<?php

error_reporting(E_ALL);

use Phalcon\Mvc\Dispatcher,
    Phalcon\Events\Event,
    Phalcon\Events\Manager as EventsManager,
    Phalcon\Mvc\Dispatcher\Exception as DispatchException;

try {
    /**
     * Read the configuration
     */
    $config = include(__DIR__ . "/../app/config/config.php");
    $loader = new \Phalcon\Loader();

    define('BASE_DIR', $config->application->baseDir);
    define('DATA_DIR', $config->application->dataDir);
    define('FILES_DIR', $config->application->filesDir);
    define('UPLOAD_DIR', $config->application->uploadDir);
    define('DOWNLOAD_DIR', $config->application->downloadDir);
    define('MAIL_DIR', $config->application->mailDir);
    define('BASE_SCL_DIR', $config->application->baseSchoolDir);
    define('LESSONPLAN_DIR', $config->application->lessonplanDir);
    define('FILES_URI', $config->application->filesUri);
    define('UPLOAD_URI', $config->application->uploadUri);
    define('ASSIGNMENT_DIR', $config->application->assignmentDir);
    define('ASSIGNMENT_URI', $config->application->assignmentUri);
    define('EVENT_DIR', $config->application->eventDir);
    define('EVENT_URI', $config->application->eventUri);
    define('EVAL_DIR', $config->application->evaluationDir);
    define('ORGTEMPL_DIR', $config->application->orgtemplateDir);
    define('SALESAPI', 'http://sales.edusparrow.com/');
    define('USERAUTHAPI', 'http://54.165.211.208/userv2.edusparrow.com/');
    define('COMMAPI', 'http://54.165.211.208/AppCMS/PHP/');
    define('CALENDARAPI', 'http://'.$config->application->subdomain.'.edusparrowcalendar.com/');
    define('MAILSERVICEAPI', 'http://'.$config->application->subdomain.'.edusparrowmail.com/');
    define('MAILCLIENT', 'http://'.$config->application->subdomain.'-cli.edusparrowmail.com/');
    define('APPKEY', '$2a$08$QDkRuR3TOoiSysOO0EKAee4ZZNiCF0/9bA0AEbeweKJuBdLGgDYWa');
    define('SUBDOMAIN', $config->application->subdomain);
    define('CASSANDRASERVERIP', '52.70.201.166');
    define('CASSANDRASERVERPORT', '9042');
    define('BUSINESSKEY', '');
    define("CONFIG_SECURITY_SALT", "ff0aa70de3348707c48fce012e594c7a");
    define("CONFIG_SERVER_BASE_URL", "http://52.23.228.66/bigbluebutton/");
    define("SCRIPTLT", '1455199009');
    /**
     * We're a registering a set of directories taken from the configuration file
     */
    $loader->registerDirs(
            array(
                $config->application->controllersDir,
                $config->application->pluginsDir,
                $config->application->modelsDir,
                $config->application->formsDir,
                $config->application->libDir,
                $config->application->libBBBDir,
                $config->application->libCassandra
            )
    )->register();

    /**
     * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
     */
    $di = new \Phalcon\DI\FactoryDefault();

    /**
     * Dispatcher use a default namespace
     */
    $di->set('dispatcher', function () {

        $eventsManager = new EventsManager();

        $eventsManager->attach("dispatch:beforeException", function($event, $dispatcher, $exception) {
//            echo '<pre>';
//            print_r($exception);
//            exit;
            //Handle 404 exceptions
            if ($exception instanceof DispatchException) {
                $dispatcher->forward(array(
                    'controller' => 'errorpage',
                    'action' => 'show404'
                ));
                return false;
            }
//Alternative way, controller or action doesn't exist
            if ($event->getType() == 'beforeException') {
                switch ($exception->getCode()) {
                    case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                    case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                        $dispatcher->forward(array(
                            'controller' => 'errorpage',
                            'action' => 'show404'
                        ));
                        return false;
                }
            }
            //Handle other exceptions
            $dispatcher->forward(array(
                'controller' => 'errorpage',
                'action' => 'show500'
            ));

            return false;
        });

        $dispatcher = new Dispatcher();

        //Bind the EventsManager to the dispatcher
        $dispatcher->setEventsManager($eventsManager);

        return $dispatcher;
    }, true);

    /**
     * Include the application routes
     */
    $di->set('router', function() {
        return include(__DIR__ . "/../app/config/routes.php");
    });

    /**
     * The URL component is used to generate all kind of urls in the application
     */
    $di->set('url', function() use ($config) {
        $url = new \Phalcon\Mvc\Url();
        $url->setBaseUri($config->application->baseUri);
        return $url;
    });

    /**
     * Mail service uses AmazonSES
     */
    $di->set('mail', function () {
        return new Mail();
    });


    $di->set('config', $config);


    /**
     * Setting up the view component
     */
    $di->set('view', function() use ($config) {
        $view = new \Phalcon\Mvc\View();
        $view->setViewsDir($config->application->viewsDir);
        return $view;
    });

    /**
     * Database connection is created based in the parameters defined in the configuration file
     */
    $di->set('db', function() use ($config) {
        return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
            "host" => $config->database->host,
            "username" => $config->database->username,
            "password" => $config->database->password,
            "dbname" => $config->database->name,
            "persistent" => $config->database->persistent
        ));
    });

    /**
     * Auth List
     */
    $di->set('auth', function () {
        return new Auth();
    });

    /**
     * vUCHER List
     */
    $di->set('voucherTrigger', function () {
        return new VoucherTrigger();
    });
     /**
     * Voucher Delete List
     */
    $di->set('voucherDeleteTrigger', function () {
        return new VoucherDeleteTrigger();
    });
     /**
     * Voucher Edit List
     */
    $di->set('voucherEditTrigger', function () {
        return new VoucherEditTrigger();
    });
    /**
     * FeeTrigger List
     */
    $di->set('feeTrigger', function () {
        return new FeeTrigger();
    });

    /**
     * Calendar List
     */
    $di->set('calendar', function () {
        return new Calendar();
    });

    /**
     * Mail service uses moduleEnabled
     */
    $di->set('moduleEnabled', function () {
        return new ModuleEnabled();
    });
    /**
     * Notification List
     */
    $di->set('notificationTrigger', function () {
        return new NotificationTrigger();
    });


    /**
     * Timiline activity List
     */
    $di->set('activityTrigger', function () {
        return new ActivityTrigger();
    });
    /**
     * Register the flash service with custom CSS classes
     */
    $di->set('flash', function() {
        return new Phalcon\Flash\Direct(array(
            'error' => 'alert alert-error',
            'success' => 'alert alert-success',
            'notice' => 'alert alert-info',
        ));
    });


    /**
     * Register the flash service with custom CSS classes
     */
    $di->set('flashSession', function() {
        return new Phalcon\Flash\Session(array(
            'error' => 'alert alert-error',
            'success' => 'alert alert-success',
            'notice' => 'alert alert-info',
        ));
    });

    /**
     * Start the session the first time some component request the session service
     */
    $di->set('session', function() {
        $session = new \Phalcon\Session\Adapter\Files();
        $session->start();
        return $session;
    });



    /**
     * initialization of models, keeping record of relations between the different models
     */
    $di->set('modelsManager', function() {
        return new Phalcon\Mvc\Model\Manager();
    });


    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application();
    $application->setDI($di);
//    setlocale(LC_MONETARY, Settings::findFirstByVariableName('app_locale')->variableValue);
//    echo 'gi';exit;
    echo $application->handle()->getContent();
} catch (Phalcon\Exception $e) {
    echo $e->getMessage();
} catch (PDOException $e) {
    echo $e->getMessage();
}
