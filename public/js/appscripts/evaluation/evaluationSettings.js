var evalSettings = {};
evalSettings.params = {};
evalSettings.downloadEvalutionExcel = function (masterid) {

    var newForm = $('<form>', {
        'action': commonSettings.evaluationUrl + "/generateEvaluationExcel",
        'target': '_blank',
        'id': 'generateEvaluationExcel',
        'method': 'POST',
    }).append($('<input>', {
        'name': 'masterid',
        'value': masterid,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};

evalSettings.addActivityToEvaluation = function (ctrl) {
    $('.loading-act').removeClass('hide')
    $('.enableActivity').addClass('hide');
    $('.disableActivity').addClass('hide')
    var activityObj = {};
    activityObj.activity_id = $(ctrl).attr('activity_id');
    activityObj.activity_name = $(ctrl).attr('activity_name');
    activityObj.activity_type = $(ctrl).attr('activity_type');
    activityObj.subject_master_id = $(ctrl).attr('subject_master_id');
    $.ajax({
        type: "POST",
        url: commonSettings.evaluationUrl + "/addActivityToEvaluation",
        data: activityObj,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                $('.loading-act').addClass('hide')
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();

                    }
                    else if (res['type'] == 'success') {
                        $('.disableActivity').attr('activity_item_id', res.activity_item_id)
                        $('.enableActivity').addClass('hide');
                        $('.disableActivity').removeClass('hide')
                    }
                });

            }

        }
    });

}

evalSettings.removeActivityToEvaluation = function (ctrl) {
    $('.loading-act').removeClass('hide')
    $('.enableActivity').addClass('hide');
    $('.disableActivity').addClass('hide')
    var activityObj = {};
    activityObj.activity_item_id = $(ctrl).attr('activity_item_id');
    $.ajax({
        type: "POST",
        url: commonSettings.evaluationUrl + "/removeActivityToEvaluation",
        data: activityObj,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                $('.loading-act').addClass('hide')
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                    }
                    else if (res['type'] == 'success') {
                        $('.disableActivity').attr('activity_item_id', '0')
                        $('.enableActivity').removeClass('hide');
                        $('.disableActivity').addClass('hide')
                    }
                });

            }

        }
    });

}
evalSettings.downloadUploadedEvalExcel = function (masterid) {

    var newForm = $('<form>', {
        'action': commonSettings.evaluationUrl + "/userDefinedExcel",
        'target': '_blank',
        'id': 'userDefinedExcel',
        'method': 'POST',
    }).append($('<input>', {
        'name': 'masterid',
        'value': masterid,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};
evalSettings.generateReport = function (masterid) {
    $.ajax({
        type: "POST",
        url: commonSettings.evaluationUrl + "/generateReport",
        data: {masterid: masterid},
        dataType: "HTML",
        success: function (res) {
            if (res) {
                $('#eval-body').html(res)
            }

        }
    });

};
evalSettings.evalSettings = function (masterid) {
    $.ajax({
        type: "POST",
        url: commonSettings.evaluationUrl + "/evalSettings",
        data: {masterid: masterid},
        dataType: "HTML",
        success: function (res) {
            if (res) {
                $('#eval-body').html(res)
            }

        }
    });

};
evalSettings.generateExcelReport = function (masterid) {

    var newForm = $('<form>', {
        'action': commonSettings.evaluationUrl + "/generateExcelReport",
        'target': '_blank',
        'id': 'downloadEvaluationExcel',
        'method': 'POST',
    }).append($('<input>', {
        'name': 'masterid',
        'value': masterid,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();

}
evalSettings.loadSubtree = function (ctrl) {

    $('#submitEvalSearchCri').addClass('hide')
    $(ctrl).parent().parent().next('.subtreediv').removeClass('hide').html(' <div class="form-group">  <span class="col-sm-8 pull-right help-block"><i class="fa fa-spin fa-spinner" ></i> Loading please wait...</span>  </div>')
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.evaluationUrl + "/loadSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $('#submitEvalSearchCri').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};

evalSettings.loadEvaldet = function () {
    var params = {}, error = '';
    $("#finalEvaluationForm").find('select').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory') && $.trim($(this).val()) == '') {
            if ($.trim($(this).val()) == '') {
                error += $(this).attr('title') + " is required!\n";
            }
        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    if (error == '') {

        $('#submitEvalSearchCri').addClass('hide');
        $('#stuEvalList').removeClass('hide')
        $('#stuEvalList').find('div.panel-body').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> This will take several minutes. Please wait...')
        evalSettings.params = params;
        $.ajax({
            type: "POST",
            url: commonSettings.evaluationUrl + "/loadStudentsEvaluationList",
            data: params,
            success: function (res) {
                $('#submitEvalSearchCri').removeClass('hide');
                if (res) {
                    $('#stuEvalList').find('div.panel-body').html(res);
                }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
        return false;
    }
};

evalSettings.calclateResult = function () {
    $('#publishres').html(' <div style="  padding: 90px; ">  <span class="col-sm-8 pull-right help-block"><i class="fa fa-spin fa-spinner" ></i> Loading please wait...</span>  </div>')
    $.ajax({
        type: "POST",
        url: commonSettings.evaluationUrl + "/calclateResult",
        data: evalSettings.params,
        dataType: "html",
        success: function (res) {
            $('#publishres').html(res)
        }
    });
};
evalSettings.generatePromotionExcel = function () {

    var newForm = $('<form>', {
        'action': commonSettings.evaluationUrl + "/generatePromotionExcel",
        'target': '_blank',
        'id': 'generatePromotionExcel',
        'method': 'POST',
    })
    $.each(evalSettings.params, function (inx, val) {
        newForm.append($('<input>', {
            'name': inx,
            'value': val,
            'type': 'hidden'
        }));
    });

    newForm.appendTo("body").submit();
};


evalSettings.downloadUploadedPromotionExcel = function () {

    var newForm = $('<form>', {
        'action': commonSettings.evaluationUrl + "/downloadUploadedPromotionExcel",
        'target': '_blank',
        'id': 'userDefinedExcel',
        'method': 'POST',
    })
    $.each(evalSettings.params, function (inx, val) {
        newForm.append($('<input>', {
            'name': inx,
            'value': val,
            'type': 'hidden'
        }));
    });

    newForm.appendTo("body").submit();
};

evalSettings.loadDistinctCombinations = function () {
    $('#loadDistinctCombinations').html('<span class="col-sm-8 pull-right help-block"><i class="fa fa-spin fa-spinner" ></i> Loading Combinations please wait...</span>')

    $.ajax({
        type: "POST",
        url: commonSettings.promotionAjaxurl + "/loadDistinctCombinations",
        dataType: "HTML",
        data: evalSettings.params,
        success: function (resArr) {
            $('#loadDistinctCombinations').html(resArr);
        }
    });
}


evalSettings.publishResultsForSelectedStu = function () {
    var tablectrl = '#promotionPubTable', params = [], stu = {},vparams ={};
    $(tablectrl).find('.select_chkbx:checked').each(function () {
        stu.id = $.trim($(this).closest('tr').attr("stuid"));
        stu.promstatus = $.trim($(this).closest('tr').attr("promstatus"));
        params.push(stu);
    });
    $('#confirm_close').unbind('click').click();
    $('.cnfBox-title').html('Confirm Message');
    if (params.length > 0) {
        $('.cnfBox-body').html("Please confirm to promote selected students ?");

    }
    vparams.students = params;
    $('#confirmMsg').click();
    $('#confirm_yes').unbind('click').click(function () {
        $('#confirm_no').click();
        console.log(vparams)
        evalSettings.promoteStudentsConfirmed(vparams)
    });
};

evalSettings.promoteStudentsConfirmed = function (params) {
    var aggregate = new Array();
    var error_msg = '';
    $('#passFailnode').find('input:hidden').each(function () {
//        console.log($(this).val())
        if ($(this).val() === '') {
            var text = $(this).hasClass('passnode') ? '<b>To be promoted</b>' : '<b>To be demoted to</b>';
            error_msg = text + ' is missing for ' + $(this).attr('title') + ' !<br/>';
        } else {
            params[$(this).attr('id')] = $.trim($(this).val());
        }


    });
    console.log(params);
    if (error_msg) {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error_msg + '</div>');
        $('#messageBox').click();
        $('#msg_ok').unbind('click').click(function () {
            $('#msg_ok').unbind('click').click();
            return false;
        });
    } else {
        myApp.showPleaseWait();
          $.ajax({
        type: "POST",
        url: commonSettings.evaluationUrl + "/promoteOrDemoteStudents",
        data: params,
        dataType: "JSON",
        success: function (res) {
            //console.log(res)
            myApp.hidePleaseWait();
            if (res) {
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                        return false;
                    }
                    else if (res['type'] == 'success') {
                       evalSettings.calclateResult()
                    }
                });

            }
        }
    });
    }

};