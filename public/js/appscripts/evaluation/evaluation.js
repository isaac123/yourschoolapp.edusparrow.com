var evaluationSettings = {};
$(document).ready(function() {
//    evaluationSettings.viewEditEvaluationSettings();
})

evaluationSettings.enableMainExamSettings = function() {
    if ($('#exam').is(':checked')) {
        $('#main_exam_div').removeClass('hide');
    }
    else {
        $('#main_exam_div').addClass('hide');
    }
};

evaluationSettings.enableAttendanceSettings = function() {
    if ($('#attendance').is(':checked')) {
        $('#attendance_div').removeClass('hide');
    }
    else {
        $('#attendance_div').addClass('hide');
    }
};

evaluationSettings.enableRatingSettings = function() {
    if ($('#rating').is(':checked')) {
        $('#rating_div').removeClass('hide');
    }
    else {
        $('#rating_div').addClass('hide');
    }
};

evaluationSettings.processRadioButton = function(val) {

    if (val == 1) {
        $('#No').prop("checked", false);
        $('#class_div').addClass('hide');
        $('#main_exam_div').addClass('hide');
        $('#attendance_div').addClass('hide');
        $('#rating_div').addClass('hide');
        $('#exam').attr('checked', false);
        $('#attendance').attr('checked', false);
        $('#rating').attr('checked', false);

        $.ajax({
            type: "POST",
            url: commonSettings.evaluationAjaxurl + "/loadMainExamAndRating",
            data: {
                classId: "",
                type: 'mainexam'
            },
            success: function(res) {
                if (res) {
                    $('#populate_main_exam').html();
                    $('#populate_main_exam').html(res);
                }

            }
        });
        $.ajax({
            type: "POST",
            url: commonSettings.evaluationAjaxurl + "/loadMainExamAndRating",
            data: {
                classId: "",
                type: 'rating'
            },
            success: function(res) {
                if (res) {
                    $('#populate_rating').html();
                    $('#populate_rating').html(res);
                }
            }
        });
    }
    else {
        $('#Yes').prop("checked", false);
        $('#class_div').removeClass('hide');
        $('#main_exam_div').addClass('hide');
        $('#attendance_div').addClass('hide');
        $('#rating_div').addClass('hide');
        $('#exam').attr('checked', false);
        $('#attendance').attr('checked', false);
        $('#rating').attr('checked', false);

        $.ajax({
            type: "POST",
            url: commonSettings.evaluationAjaxurl + "/loadMainExamAndRating",
            data: {
                classId: "",
                type: 'mainexam'
            },
            success: function(res) {
                if (res) {
                    $('#populate_main_exam').html();
                    $('#populate_main_exam').html(res);
                }

            }
        });
        $.ajax({
            type: "POST",
            url: commonSettings.evaluationAjaxurl + "/loadMainExamAndRating",
            data: {
                classId: "",
                type: 'rating'
            },
            success: function(res) {
                if (res) {
                    $('#populate_rating').html();
                    $('#populate_rating').html(res);
                }
            }
        });
    }
};

evaluationSettings.loadMainExamAndRating = function() {
    var class_id = $("#divID").val();
    if (class_id != 0)
    {
        //get the rating name and main exam name applicable for this class
        $.ajax({
            type: "POST",
            url: commonSettings.evaluationAjaxurl + "/loadMainExamAndRating",
            data: {
                classId: class_id,
                type: 'mainexam'
            },
            success: function(res) {
                if (res) {
                    $('#populate_main_exam').html();
                    $('#populate_main_exam').html(res);
                }

            }
        });
        $.ajax({
            type: "POST",
            url: commonSettings.evaluationAjaxurl + "/loadMainExamAndRating",
            data: {
                classId: class_id,
                type: 'rating'
            },
            success: function(res) {
                if (res) {
                    $('#populate_rating').html();
                    $('#populate_rating').html(res);
                }
            }
        });
    }
    else
    {
        $('#main_exam_div').addClass('hide');
        $('#attendance_div').addClass('hide');
        $('#rating_div').addClass('hide');
        $('#exam').attr('checked', false);
        $('#attendance').attr('checked', false);
        $('#rating').attr('checked', false);
    }
}


evaluationSettings.viewEditEvaluationSettings = function() {

    $.ajax({
        type: "POST",
        url: commonSettings.evaluationAjaxurl + "/viewEditEvaluationSettings",
        success: function(res) {
            if (res) {
                $('#view_setting').html(res);
            }
        }
    });
}

evaluationSettings.removeEvaluationSettings = function(val) {
    $.ajax({
        type: "POST",
        url: "removeEvaluationSettings",
        dataType: "JSON",
        data:
                {
                    eval_id: val
                },
        success: function(res) {
            if (res) {
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function(event) {
                    if (res['type'] == "success") {
                        evaluationSettings.viewEditEvaluationSettings();
                    }
                });
            }
        }
    });
}
evaluationSettings.saveEvaluationSettings = function() {
    var error = 0;
    //Check one of the radio button is clicked
    if (!(($('#No').is(':checked')) || ($('#Yes').is(':checked')))) {


        error = 1;
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Tell us whether Evaluation Setting is Common or not</div>');
        $('#messageBox').click();
    }
    //check whether radio button "No" is clicked, if so there should be class selected
    //
    else if (($('#No').is(':checked')) && ($("#divID").val() == 0)) {

        error = 1;
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Select Class</div>');
        $('#messageBox').click();

    }
    else {
        //Check whether at least one checkbox is checked, if not throw error
        if (!(($('#exam').is(':checked')) || ($('#attendance').is(':checked')) || ($('#rating').is(':checked')))) {

            $('.modal-title').html('Message');
            $('.modal-body').html('<div class="alert alert-block alert-danger fade in">No Factors Were Checked. Select the Factor affecting Promotion</div>');
            $('#messageBox').click();
        }
        else {
            //Atleast one checkbox is checked

            if ($('#exam').is(':checked')) {
                if ($("#mainexamname").val() == 0)
                {
                    error = 1;
                    $('.modal-title').html('Message');
                    $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Select One Decisive Main Exam</div>');
                    $('#messageBox').click();
                }
                else {
                    length = $('#mainexam_minimum_marks').val().length;

                    if (length > 0) {
                        minimum_marks = parseFloat($('#mainexam_minimum_marks').val());
                        if (minimum_marks < 0)
                        {
                            error = 1;
                            $('.modal-title').html('Message');
                            $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Minimum Marks Percentage Cant be Negative</div>');
                            $('#messageBox').click();
                        }
                        else if (minimum_marks > 100) {
                            error = 1;
                            $('.modal-title').html('Message');
                            $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Minimum Marks Percentage Cant exceed 100</div>');
                            $('#messageBox').click();
                        }
                    }
                    else {
                        error = 1;
                        $('.modal-title').html('Message');
                        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Enter Minimum Marks Required For Promotion</div>');
                        $('#messageBox').click();
                    }
                }

            }

            if ($('#attendance').is(':checked')) {

                length = $('#attendance_minimum_marks').val().length;

                if (length > 0) {
                    minimum_attendance = parseFloat($('#attendance_minimum_marks').val());
                    if (minimum_attendance < 0)
                    {
                        error = 1;
                        $('.modal-title').html('Message');
                        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Minimum Attendance Percentage Cant be Negative</div>');
                        $('#messageBox').click();
                    }
                    else if (minimum_attendance > 100) {
                        error = 1;
                        $('.modal-title').html('Message');
                        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Minimum Attendance Percentage Cant exceed 100</div>');
                        $('#messageBox').click();
                    }
                }
                else {
                    error = 1;
                    $('.modal-title').html('Message');
                    $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Enter Minimum Attendance Percentage Required For Promotion</div>');
                    $('#messageBox').click();
                }
            }

            if ($('#rating').is(':checked')) {

                if ($("#ratingname").val() == 0)
                {
                    error = 1;
                    $('.modal-title').html('Message');
                    $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Pick One Rating Name</div>');
                    $('#messageBox').click();
                }
                else {

                    length = $('#rating_minimum_percentage').val().length;

                    if (length > 0) {
                        minimum_rating = parseFloat($('#rating_minimum_percentage').val());
                        if (minimum_rating < 0)
                        {
                            error = 1;
                            $('.modal-title').html('Message');
                            $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Minimum Rating Percentage Cant be Negative</div>');
                            $('#messageBox').click();
                        }
                        else if (minimum_rating > 100) {
                            error = 1;
                            $('.modal-title').html('Message');
                            $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Minimum Rating Percentage Cant exceed 100</div>');
                            $('#messageBox').click();
                        }
                    }
                    else {
                        error = 1;
                        $('.modal-title').html('Message');
                        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Enter Minimum Rating Percentage Required For Promotion</div>');
                        $('#messageBox').click();
                    }
                }
            }

            if (error == 0) {
                //All inputs are validated and looks good

                $.ajax({
                    type: "POST",
                    url: commonSettings.evaluationAjaxurl + "/saveEvaluationSettings",
                    data: {
                        commonSetting: $('#Yes').is(':checked'),
                        classId: $("#divID").val(),
                        is_main: $('#exam').is(':checked'),
                        is_attendance: $('#attendance').is(':checked'),
                        is_rating: $('#rating').is(':checked'),
                        mainexam_name: $("#mainexamname option:selected").text(),
                        minimum_mark: $('#mainexam_minimum_marks').val(),
                        minimum_attendance: $('#attendance_minimum_marks').val(),
                        rating_name: $("#ratingname option:selected").text(),
                        minimum_rating: $('#rating_minimum_percentage').val()
                    },
                    success: function(res) {
                        if (res) {
                            $('.modal-title').html('Message');
                            $('.modal-body').html(res);
                            $('#messageBox').click();

                            evaluationSettings.loadevaluationSettings();
                        }

                    }
                });
            }
        }
    }

};

evaluationSettings.loadSection = function(ctrl) {

    $('.combispan').addClass('inactive')
    $(ctrl).toggleClass('inactive')
    $('#loadResults').addClass('hide')
    evaluationSettings.classmasterID = $(ctrl).attr('id').split('_')[1];

    $.ajax({
        type: "POST",
        url: commonSettings.evaluationAjaxurl + "/loadSectionsByClass",
        data: {
            classId: evaluationSettings.classmasterID
        },
        success: function(res) {
            if (res) {
                $('#loadSection').html(res);
                $('#loadSection').removeClass('hide');
            }

        }
    });

};

evaluationSettings.evaluateStudents = function() {

    if ($('#subDivVal').val() == 0)
    {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Select Section to Evaluate</div>');
        $('#messageBox').click();
    }
    else if ($('#start_date').val().length == 0) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Pick start date</div>');
        $('#messageBox').click();
    }
    else if ($('#end_date').val().length == 0) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Pick end date</div>');
        $('#messageBox').click();
    }
    else if (($('#end_date').val() < $('#start_date').val())) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">End date cannot be lesser than start date</div>');
        $('#messageBox').click();
    }
    else {
        $.ajax({
            type: "POST",
            url: commonSettings.evaluationAjaxurl + "/evaluationResults",
            data: {
                class_id: evaluationSettings.classmasterID,
                start_date: $('#start_date').val(),
                end_date: $('#end_date').val()
            },
            success: function(res) {
                if (res) {
                    $('#loadResults').html(res);
                    $('#loadResults').removeClass('hide')
                }
            }
        });
    }
};

evaluationSettings.changeResults = function(stud_id) {

    $.ajax({
        type: "POST",
        url: commonSettings.evaluationAjaxurl + "/changeResults",
        data: {
            stud_id: stud_id
        },
        success: function(res) {
            if (res) {
                $('#' + stud_id).html(res);
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html('<div class="alert alert-block alert-success fade in">Result update is successful!</div>');
                $('#messageBox').click();
            }
        }
    });


};


evaluationSettings.loadevaluationSettings = function() {

    $.ajax({
        type: "POST",
        url: commonSettings.evaluationAjaxurl + "/evaluationSettings",
        dataType: "html",
        success: function(res) {
            $('#evaluationDiv').html(res);
            $('#evaluationDiv').removeClass('hide');
            $('#promotionDiv').addClass('hide');
            
            evaluationSettings.viewEditEvaluationSettings();
        }
    });
};
evaluationSettings.loadPromotionSettings = function() {

    $.ajax({
        type: "POST",
        url: commonSettings.promotionAjaxurl + "/settings",
        dataType: "html",
        success: function(res) {
            $('#promotionDiv').html(res);
            $('#promotionDiv').removeClass('hide');
            $('#evaluationDiv').addClass('hide');
        }
    });
};

evaluationSettings.loadevaluation = function() {

    $.ajax({
        type: "POST",
        url: commonSettings.evaluationAjaxurl + "/evaluation",
        dataType: "html",
        success: function(res) {
            $('#stuevaluationDiv').html(res);
            $('#stuevaluationDiv').removeClass('hide')
        }
    });
};
evaluationSettings.loadPromotion = function() {
    $.ajax({
        type: "POST",
        url: commonSettings.promotionAjaxurl + "/publish",
        dataType: "html",
        success: function(res) {
            $('#stupromotionDiv').html(res);
            $('#stupromotionDiv').removeClass('hide')
        }
    });
};