var announcementSettings = {};
var announ_file_upload;
var ThisLoad;
announcementSettings.dialogbox = function () {
    var ids = $('#allUserIds').val();
    $.ajax({
        type: "POST",
        url: commonSettings.announcementAjaxurl + "/announcementUserList",
        data: {ids: ids},
        dataType: 'html',
        success: function (res) {
            bootbox.dialog({
                title: "Communicate To",
                message: res,
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var params, err = 0, error = '';
                            params = $('#user_list').val();
                            if (err == 0) {
                                $('#allUserIds').val(params);
                            } else {
                                bootbox.dialog({
                                    title: "Message",
                                    message: error,
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn-danger",
                                            callback: function () {
                                                announcementSettings.dialogbox();
                                            }
                                        }
                                    }
                                });
                                err = 0;
                            }
                        }
                    }
                }
            });
        }
    });
};
announcementSettings.PreviewGen = function (type, text, layout) {
    var n = noty({
        text: text,
        type: type,
        dismissQueue: true,
        layout: layout,
        closeWith: ['click'],
        theme: 'ThemeNoty',
        maxVisible: 10,
        animation: {
            open: 'noty_animated bounceInDown',
            close: 'noty_animated fadeOut',
            easing: 'swing',
            speed: 500
        }
    });
    setTimeout(function () {
        n.close();
    }, 5000);

}
announcementSettings.saveAnnouncement = function (ctrl) {
    $('body').mask("Loading");
    var Dtype = $(ctrl).data("type"),
            Dlayout = $(ctrl).data("layout");
    var params = {}, err = 0, error = '', i = 0;
    params['announcement_text'] = $('#announcement_text').val();
    params['announcement_date'] = $('#announcement_date').val();
    params['announcement_type'] = $('#announcement_type').val();
    params['allUserIds'] = $('#allUserIds').val();
    if (params['announcement_text'] == '') {
        err = 1;
        error += " Message is required !</br>";
    }
    if (params['allUserIds'] == '') {
        err = 1;
        error += " User Details is required !</br>";
    }
    if (params['announcement_type'] == '') {
        err = 1;
        error += " Type is required !</br>";
    }
    $("#annunc_multifile").find('p').each(function () {
        params['file_' + i] = $.trim($(this).text());
        i++;
    });
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.announcementAjaxurl + "/saveAnnouncement",
            data: params,
            dataType: 'JSON',
            success: function (res) {
                setTimeout($('body').unmask(), 1500);
                if (res.type == 'success') {
                    var n_dom = [];
                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a> Notification Sent <span>' + res.time + '</span> </div> </div>';
                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                    announcementSettings.communicate();
                    announcementSettings.reloadBoxes()
                } else {
                    bootbox.dialog({
                        title: "Error",
                        message: '<span class="text text-danger ">' + res.message + '</span>',
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn-danger"
                            }
                        }
                    });
                    err = 0;
                }
            }
        });
    } else {
        setTimeout($('body').unmask(), 1500);
        bootbox.dialog({
            title: "Error",
            message: '<span class="text text-danger ">' + error + '</span>',
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn-danger"
                }
            }
        });
        err = 0;
    }

};
announcementSettings.reload_outbox = function () {
//   $('#outbox_reload').mask("Loading");
    announcementSettings.outbox();
//    setTimeout($('#outbox_reload').unmask(), 2000);
}
announcementSettings.reload_inbox = function () {
//    $('#inbox_reload').mask("Loading");
    announcementSettings.inbox();
//    setTimeout($('#inbox_reload').unmask(), 2000);
}
announcementSettings.assignTastTo = function () {
    ids = $('#taskUserIds').val();
    $.ajax({
        type: "POST",
        url: commonSettings.announcementAjaxurl + "/assignTastTo",
        data: {ids: ids},
        dataType: 'html',
        success: function (res) {
            bootbox.dialog({
                title: "Assign Task To",
                message: res,
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var task, err = 0, error = '';
                            task = $('#taskUserIds').val();
                            console.log(task);
                            if (task == '') {
                                err = 1;
                                error += " User Detail is required !</br>";
                            }
                            if (err == 0) {
                                $('#taskUserIds').val(task);
                            } else {
                                bootbox.dialog({
                                    title: "Error",
                                    message: '<span class="text text-danger ">' + res.message + '</span>',
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn-danger",
                                            callback: function () {
                                                announcementSettings.assignTastTo();
                                            }
                                        }
                                    }
                                });
                                err = 0;
                            }
                        }
                    }
                }
            });
        }
    });
};
announcementSettings.saveTask = function (ctrl) {
    $('body').mask("Loading");
    var Dtype = $(ctrl).data("type"),
            Dlayout = $(ctrl).data("layout");
    var err = 0, error = '';
    var params = {};
    $("#TaskDetailsForm").find('input,select,textarea').each(function () {
        console.log($(this).hasClass('mandatory'));
        if ($(this).hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += '' + $(this).attr('title') + " is required !</br>";
            } else {
                params[$(this).attr('name')] = $(this).val();
            }
        } else {
            params[$(this).attr('name')] = $(this).val();
        }

    });
    var i = 0;
    $("#task_multifile").find('p').each(function () {
        params['file_' + i] = $.trim($(this).text());
        i++;
    });
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.announcementAjaxurl + "/saveTask",
            data: params,
            dataType: 'JSON',
            success: function (res) {
                setTimeout($('body').unmask(), 1500);
                if (res.type == 'success') {
                    var n_dom = [];
                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a> Notification Sent <span>' + res.time + '</span> </div> </div>';
                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                    announcementSettings.communicate();
                    announcementSettings.taskHistory();
                    announcementSettings.reloadBoxes()
                } else {
                    bootbox.dialog({
                        title: "Error",
                        message: '<span class="text text-danger ">' + res.message + '</span>',
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn-danger"
                            }
                        }
                    });
                    err = 0;
                }
            }
        });
    } else {
        setTimeout($('body').unmask(), 1500);
        bootbox.dialog({
            title: "Error",
            message: '<span class="text text-danger ">' + error + '</span>',
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn-danger"
                }
            }
        });
        err = 0;
    }
};
announcementSettings.acceptTask = function (taskid, status, ctrl, private) {
    var edit = $(ctrl).attr('edit');
    if (edit == 'inbox') {
        viewtask.modal('hide');
    }
    if (edit == 'outbox') {
        edittask.modal('hide');
    }
    if (status == 'Accept') {
        var url = commonSettings.announcementAjaxurl + "/acceptTask";
    } else if (status == 'Decline' || status == 'Finish') {
        var url = commonSettings.announcementAjaxurl + "/rejectTask";
    }
    bootbox.dialog({
        message: "Do you confirm to  " + status + "  this Task",
        title: "Confirm Message",
        buttons: {
            success: {
                label: "Confirm",
                className: "btn-success",
                callback: function () {

                    if (private) {
                        $('#chat').mask("Loading");
                    }
                    if (ctrl) {
                        $('body').mask("Loading");
                    }
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {taskid: taskid, status: status},
                        dataType: 'JSON',
                        success: function (res) {
                            if (ctrl) {
                                setTimeout($('body').unmask(), 1500);
                                var Dtype = 'information';
                                var Dlayout = 'topRight';
                                if (res.type == 'success') {
                                    var n_dom = [];
                                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                                    <div class="activity">  <a href="#"></a> Task Status Updated <span>' + res.time + '</span> </div> </div>';
                                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                                    announcementSettings.reloadBoxes()
                                    announcementSettings.taskHistory();
                                } else {
                                    bootbox.dialog({
                                        title: "Error",
                                        message: '<span class="text text-danger ">' + res.message + '</span>',
                                        buttons: {
                                            success: {
                                                label: "Ok",
                                                className: "btn-danger"
                                            }
                                        }
                                    });
                                    err = 0;
                                }
                            }
                            else if (private) {
                                announcementSettings.reloadBoxes()
                                announcementSettings.taskHistory();
                                setTimeout($('#chat').unmask(), 1500);
                            }
                            else {
                                bootbox.dialog({
                                    title: "Error",
                                    message: '<span class="text text-danger ">' + res.message + '</span>',
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn-success",
                                            callback: function () {
//                                                window.location.reload();
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            },
            danger: {
                label: "Cancel",
                className: "btn-default",
                callback: function () {
                }
            }
        }
    });
};
announcementSettings.forwardTask = function (taskid, ctrl) {
    var edit = $(ctrl).attr('edit');
    if (edit == 'inbox') {
        viewtask.modal('hide');
    }
    if (edit == 'outbox') {
        edittask.modal('hide');
    }
    $.ajax({
        type: "POST",
        url: commonSettings.announcementAjaxurl + "/forwardTask",
        dataType: 'html',
        data: {taskid: taskid},
        success: function (res) {
            bootbox.dialog({
                title: "Message",
                message: res,
                buttons: {
                    success: {
                        label: "Forward",
                        className: "btn-success",
                        callback: function () {
                            var task, err = 0, error = '';
                            task = $('#forwardIds').val();
                            console.log(task);
                            if (task == '') {
                                err = 1;
                                error += " User Detail is required !</br>";
                            }
                            if (err == 0) {
                                $.ajax({
                                    type: "POST",
                                    url: commonSettings.announcementAjaxurl + "/forwarAssignedTask",
                                    data: {taskid: taskid, userid: task},
                                    dataType: 'JSON',
                                    success: function (res) {
                                        if (ctrl) {
                                            var Dtype = 'information';
                                            var Dlayout = 'topRight';
                                            if (res.type == 'success') {
                                                var n_dom = [];
                                                n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a> Task Status Updated <span>' + res.time + '</span> </div> </div>';
                                                announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                                                announcementSettings.taskHistory();
                                                announcementSettings.reloadBoxes()
                                            } else {
                                                bootbox.dialog({
                                                    title: "Error",
                                                    message: '<span class="text text-danger ">' + res.message + '</span>',
                                                    buttons: {
                                                        success: {
                                                            label: "Ok",
                                                            className: "btn-danger"
                                                        }
                                                    }
                                                });
                                                err = 0;
                                            }
                                        } else {
                                            bootbox.dialog({
                                                title: "Error",
                                                message: '<span class="text text-danger ">' + res.message + '</span>',
                                                buttons: {
                                                    success: {
                                                        label: "Ok",
                                                        className: "btn-success",
                                                        callback: function () {
                                                            window.location.reload();
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                bootbox.dialog({
                                    title: "Message",
                                    message: error,
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn-danger",
                                            callback: function () {
                                                announcementSettings.forwardTask(taskid);
                                            }
                                        }
                                    }
                                });
                                err = 0;
                            }
                        }
                    }
                }
            });
        }
    });
};
announcementSettings.addComments = function (taskactid, type) {
    console.log('#comm-' + type + '-' + taskactid);
    $('#comm-' + type + '-' + taskactid).removeClass('hide');
    $('#comment-' + type + '-' + taskactid).focus()
}
announcementSettings.cancelComments = function (taskactid, ctrl) {
    var type = $(ctrl).attr('type');
    $('#comm-' + type + '-' + taskactid).addClass('hide');
    $('#comment-' + type + '-' + taskactid).val('');
};
announcementSettings.saveComments = function (taskactid, ctrl) {
    var edit = $(ctrl).attr('edit');
    var type = $(ctrl).attr('type');
//    if (type == 'task' || type == 'viewtask') {
//        if (edit == 'inbox') {
//            viewtask.modal('hide');
//        }
//        if (edit == 'outbox') {
//            edittask.modal('hide');
//        }
//    }
//    if (type == 'announc' || type == 'announcement') {
//        if (edit == 'inbox') {
//            viewannouncement.modal('hide');
//        }
//        if (edit == 'outbox') {
//            editannouncement.modal('hide');
//        }
//    }
//    if (type == 'event' || type == 'viewevent') {
//        if (edit == 'inbox') {
//            viewevent.modal('hide');
//        }
//        if (edit == 'outbox') {
//            editevent.modal('hide');
//        }
//    }
    var type = $(ctrl).attr('type');
    var err = 0, error = '';
    var comments = $('#comment-' + type + '-' + taskactid).val();
    if (comments == '') {
        err = 1;
        error += ' Comments is Required</br>';
    }
    if (err == 0) {
        $('#' + type + 'comments_' + taskactid).mask('Loading');
        $.ajax({
            type: "POST",
            url: commonSettings.announcementAjaxurl + "/saveComments",
            data: {taskactid: taskactid, comments: comments, type: type},
            dataType: 'JSON',
            success: function (res) {
                if (ctrl) {
                    var Dtype = $(ctrl).data("type"),
                            Dlayout = $(ctrl).data("layout");
                    if (res.type == 'success') {
                        $.ajax({
                            type: "POST",
                            url: commonSettings.staffAjaxurl + "/loadCommentsDiv",
                            data: {activityid: taskactid, type: type},
                            dataType: 'html',
                            success: function (res) {
                                $('#' + type + 'comments_' + taskactid).html(res);
                                var totalcnt = 0, tot = '';
                                totalcnt = $('#count_' + type + '_' + taskactid).val();
                                tot = totalcnt > 1 ? totalcnt + ' Comments' : totalcnt + ' Comment';
                                $('#' + type + '_table_' + taskactid + '  tr td a').each(function () {
                                    $(this).find('span').text(tot);
                                });
                                setTimeout($('#comm-' + type + '-' + taskactid).unmask(), 1500);
                            }
                        });

                    } else {
                        bootbox.dialog({
                            title: "Error",
                            message: '<span class="text text-danger ">' + res.message + '</span>',
                            buttons: {
                                success: {
                                    label: "Ok",
                                    className: "btn-danger"
                                }
                            }
                        });
                        err = 0;
                    }
                }
            }
        });
    } else {
        bootbox.dialog({
            title: "Message",
            message: error,
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn-danger"
                }
            }
        });
        err = 0;
    }
};
announcementSettings.anouncementAcknwldg = function (announcement_id, ctrl) {
    var edit = $(ctrl).attr('edit');
    if (edit == 'inbox') {
        viewannouncement.modal('hide');
    }
    if (edit == 'outbox') {
        editannouncement.modal('hide');
    }
    $('body').mask("Loading");
    $.ajax({
        type: "POST",
        url: commonSettings.announcementAjaxurl + "/anouncementAcknwldg",
        data: {announcement_id: announcement_id},
        dataType: 'JSON',
        success: function (res) {
            if (ctrl) {
                setTimeout($('body').unmask(), 1500);
                var Dtype = $(ctrl).data("type"),
                        Dlayout = $(ctrl).data("layout");
                if (res.type == 'success') {
                    var n_dom = [];
                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a> Circular Acknowledged...  <span>' + res.time + '</span> </div> </div>';
                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                    announcementSettings.reloadBoxes()
                } else {
                    bootbox.dialog({
                        title: "Error",
                        message: '<span class="text text-danger ">' + res.message + '</span>',
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn-danger"
                            }
                        }
                    });
                    err = 0;
                }
            } else {
                bootbox.dialog({
                    title: "success",
                    message: '<span class="text text-danger ">' + res.message + '</span>',
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn-success",
                            callback: function () {
                                announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                                announcementSettings.reloadBoxes()
                            }
                        }
                    }
                });
            }
        }
    });
};
announcementSettings.viewAnnouncement = function (announcement_id, edit) {
    $.ajax({
        type: "POST",
        url: commonSettings.announcementAjaxurl + "/viewAnnouncement",
        data: {announcement_id: announcement_id, edit: edit},
        dataType: 'html',
        success: function (res) {
            if (edit == 'outbox') {
                editannouncement = bootbox.dialog({
                    title: "Communicate",
                    animate: true,
                    message: res,
                    className: "modal70",
                    buttons: {
                        success: {
                            label: "Edit",
                            className: "btn-success",
                            callback: function () {
                                editannouncement.modal('hide');
                                $.ajax({
                                    type: "POST",
                                    url: commonSettings.announcementAjaxurl + "/editAnnouncements",
                                    data: {announcement_id: announcement_id},
                                    dataType: 'html',
                                    success: function (res) {
                                        edit_announcement = bootbox.dialog({
                                            title: " Edit Communicate",
                                            message: res,
                                            buttons: {
                                                success: {
                                                    label: "Save",
                                                    className: "btn-success",
                                                    callback: function () {
                                                        var params = {}, i = 0;
                                                        $("#AnnouncEditForm").find('input,select,textarea').each(function () {
                                                            params[$(this).attr('id')] = $(this).val();
                                                        });
                                                        $("#editfileannounc").find('li').each(function () {
                                                            params['file_' + i] = $.trim($(this).find('p').text());
                                                            i++;
                                                        });
                                                        $('body').mask("Loading");
                                                        $.ajax({
                                                            type: "POST",
                                                            url: commonSettings.announcementAjaxurl + "/saveAnnouncement",
                                                            data: params,
                                                            dataType: 'JSON',
                                                            success: function (res) {
                                                                setTimeout($('body').unmask(), 1500);
                                                                if (res.type == 'success') {
                                                                    var Dtype = 'information';
                                                                    var Dlayout = 'topRight';
                                                                    var n_dom = [];
                                                                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a>  Notification Updated <span>' + res.time + '</span> </div> </div>';
                                                                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                                                                    announcementSettings.communicate();
                                                                    announcementSettings.reloadBoxes()
                                                                } else {
                                                                    bootbox.dialog({
                                                                        message: res.message,
                                                                        buttons: {
                                                                            danger: {
                                                                                label: "Ok",
                                                                                className: "btn-danger",
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                },
                                                Close: {
                                                    label: "Close",
                                                    className: "btn-default"
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        },
                        close: {
                            label: "Close",
                            className: "btn-default",
                            callback: function () {
                                editannouncement.modal('hide');
                                announcementSettings.reloadBoxes();
                            }

                        }

                    }
                });
            } else {
                viewannouncement = bootbox.dialog({
                    title: "Communicate",
                    animate: true,
                    message: res,
                    className: "modal70",
                    buttons: {
                        Close: {
                            label: "Close",
                            className: "btn-default",
                            callback: function () {
                                viewannouncement.modal('hide');
                                announcementSettings.reloadBoxes();
                            }
                        },
                    }
                });

            }

        }

    });
};
announcementSettings.viewTasks = function (taskactivity_id, edit) {
    $.ajax({
        type: "POST",
        url: commonSettings.announcementAjaxurl + "/viewTasks",
        data: {taskactivity_id: taskactivity_id, edit: edit},
        dataType: 'html',
        success: function (res) {
            if (edit == 'outbox') {
                edittask = bootbox.dialog({
                    title: "Task",
                    animate: true,
                    message: res,
                    className: "modal70",
                    buttons: {
                        success: {
                            label: "Edit",
                            className: "btn-success",
                            callback: function () {
                                edittask.modal('hide');
                                $.ajax({
                                    type: "POST",
                                    url: commonSettings.announcementAjaxurl + "/editTask",
                                    data: {taskactivity_id: taskactivity_id},
                                    dataType: 'html',
                                    success: function (res) {
                                        edit_announcement = bootbox.dialog({
                                            title: " Edit Task",
                                            message: res,
                                            buttons: {
                                                success: {
                                                    label: "Save",
                                                    className: "btn-success",
                                                    callback: function () {
                                                        var params = {}, i = 0;
                                                        $("#taskEditForm").find('input,select,textarea').each(function () {
                                                            params[$(this).attr('id')] = $(this).val();
                                                        });
                                                        $("#editfiletask").find('li').each(function () {
                                                            params['file_' + i] = $.trim($(this).find('p').text());
                                                            i++;
                                                        });
                                                        $('body').mask("Loading");
                                                        $.ajax({
                                                            type: "POST",
                                                            url: commonSettings.announcementAjaxurl + "/saveTask",
                                                            data: params,
                                                            dataType: 'JSON',
                                                            success: function (res) {
                                                                setTimeout($('body').unmask(), 1500);
                                                                if (res.type == 'success') {
                                                                    var Dtype = 'information';
                                                                    var Dlayout = 'topRight';
                                                                    var n_dom = [];
                                                                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a>  Notification Updated <span>' + res.time + '</span> </div> </div>';
                                                                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                                                                    announcementSettings.communicate();
                                                                    announcementSettings.reloadBoxes()
                                                                } else {
                                                                    bootbox.dialog({
                                                                        message: res.message,
                                                                        buttons: {
                                                                            danger: {
                                                                                label: "Ok",
                                                                                className: "btn-danger",
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                },
                                                Close: {
                                                    label: "Close",
                                                    className: "btn-default",
                                                    callback: function () {
                                                    }

                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        },
                        close: {
                            label: "Close",
                            className: "btn-default",
                            callback: function () {
                                edittask.modal('hide');
                                announcementSettings.reloadBoxes();
                            }
                        }

                    }
                });
            } else {
                viewtask = bootbox.dialog({
                    title: "Task",
                    animate: true,
                    message: res,
                    className: "modal70",
                    buttons: {
                        Close: {
                            label: "Close",
                            className: "btn-default",
                            callback: function () {
                                viewtask.modal('hide');
                                announcementSettings.reloadBoxes();
                            }
                        }
                    }
                });

            }
        }

    });
};
announcementSettings.downloadTaskFile = function (task_id) {
    var newForm = $('<form>', {
        'action': commonSettings.announcementAjaxurl + "/downloadTaskFile",
        'method': 'POST',
        'target': '_blank',
        'id': 'downloadPhoto'
    }).append($('<input>', {
        'name': 'task_id',
        'value': task_id,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};
announcementSettings.outbox = function (ctrl) {
    $('#outbox_reload').mask("Loading");
    var params = {};
    $('#outboxAllsearch').find('input,select').each(function () {
        params[$(this).attr('id')] = $(this).val();
    });
    if (ctrl) {
        params['sort'] = $(ctrl).attr('id');
    }
    $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/outbox",
        data: params,
        dataType: 'html',
        success: function (res) {
            setTimeout($('#outbox_reload').unmask(), 2000);
            $('#outboxDetails').html(res);
        }
    });
};
announcementSettings.communicate = function (ctrl) {
    $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/communicateScreen",
        dataType: 'html',
        success: function (res) {
            $('#communicateDetails').html(res);
        }
    });
};

announcementSettings.recalculateHeight = function () {
    var $this = $('.panel-heading a.fa');
    console.log($this.hasClass("fa-chevron-down"))
    if (!$this.hasClass("fa-chevron-down")) {
        if ($("#calendarDetails  .scrollme")) {
            $("#calendarDetails  .scrollme").getNiceScroll().remove();
        }
        if ($("#outboxDetails  .scrollme")) {
            $("#outboxDetails  .scrollme").getNiceScroll().remove();
            $("#outboxDetails .scrollme").css('maxHeight', '475px');
            $("#outboxDetails  .scrollme").niceScroll();
        }
    } else {
        if ($("#calendarDetails  .scrollme")) {
            $("#calendarDetails .scrollme").niceScroll();
        }
        if ($("#outboxDetails  .scrollme")) {
            $("#outboxDetails  .scrollme").getNiceScroll().remove();
            $("#outboxDetails .scrollme").css('maxHeight', '125px');
            $("#outboxDetails  .scrollme").niceScroll();
        }
    }
};
announcementSettings.reloadBoxes = function () {
    announcementSettings.outbox();
    announcementSettings.inbox();
}
announcementSettings.inbox = function (ctrl) {
    $('#inbox_reload').mask("Loading");
    var params = {};
    $('#inboxAllsearch').find('input,select').each(function () {
        params[$(this).attr('id')] = $(this).val();
    });
    if (ctrl) {
        params['sort'] = $(ctrl).attr('id');
    }
    $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/index",
        data: params,
        dataType: 'html',
        success: function (res) {
            $('#inboxDetails').html(res);
            setTimeout($('#inbox_reload').unmask(), 1500);
        }
    });
};
announcementSettings.deleteTask = function (ctrl) {
    var taskid = $(ctrl).attr('taskid');
    var type = $(ctrl).attr('type');
    $(ctrl).parent().addClass('hide')
    $.ajax({
        type: "POST",
        url: commonSettings.announcementAjaxurl + "/deleteTask",
        data: {taskid: taskid, type: type},
        dataType: 'JSON',
        success: function (res) {
            if (res.type != 'success') {
                announcementSettings.taskHistory();
                $(ctrl).parent().removeClass('hide')
            }
        }
    });
};
announcementSettings.taskHistory = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/taskHistory",
        dataType: 'html',
        success: function (res) {
            $('.tasklist').html(res);
        }
    });
};
announcementSettings.inboxOutboxCnt = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/inboxOutboxCnt",
        dataType: 'JSON',
        success: function (res) {
            console.log(res.inbox);
            $('#inboxCnt').text(res.inbox);
            $('#outboxCnt').text(res.outbox);
        }
    });
};
announcementSettings.dashboardFileDownload = function (filename) {
    var newForm = $('<form>', {
        'action': commonSettings.announcementAjaxurl + "/dashboardFileDownload",
        'method': 'POST',
        'target': '_blank',
        'id': 'downloadFile'
    }).append($('<input>', {
        'name': 'filename',
        'value': filename,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};
announcementSettings.dashboardInboxSearch = function (ctrl) {
    var params = {};
    $('#allsearch').find('input,select').each(function () {
        params[$(this).attr('id')] = $(this).val();
    });
    if (ctrl) {
        params['sort'] = $(ctrl).attr('id');
    }
    $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/dashboardInboxSearch",
        data: params,
        dataType: 'html',
        success: function (res) {
            $('#inboxDetails').html(res);
        }
    });
};
announcementSettings.annuncFileUpload = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.announcementAjaxurl + "/annuncFileUpload",
        dataType: 'html',
        success: function (res) {
            $('#annunc_multifile').empty();
            announ_file_upload = bootbox.dialog({
                title: "File Upload",
                animate: true,
                message: res,
                buttons: {
                    Save: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var i = 0;
                            $("#announcementfileupload").find('li').each(function () {
                                i++;
                                $('#annunc_multifile').append(($.trim($(this).html())));
                            });
                            $('#announcementfilescount').text('('+i+')');
                            //console.log($('#annunc_multifile').html());
                        }
                    },
                    Close: {
                        label: "Close",
                        className: "btn-default"
                    }
                }
            });
        }
    });
};
announcementSettings.taskFileUpload = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.announcementAjaxurl + "/taskFileUpload",
        dataType: 'html',
        success: function (res) {
            $('#task_multifile').empty();
            task_file_upload = bootbox.dialog({
                title: "File Upload",
                animate: true,
                message: res,
                buttons: {
                    Save: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var i = 0;
                            $("#task_upload").find('li').each(function () {
                                i++;
                                $('#task_multifile').append(($.trim($(this).html())));
                            });
                             $('#taskfilescount').text('('+i+')');
                        }
                    },
                    Close: {
                        label: "Close",
                        className: "btn-default"
                    }
                }
            });
        }
    });
};
announcementSettings.removeUploadedFile = function (ctrl) {
    var type = $(ctrl).attr('id');
    var files = $(ctrl).parent().find('p').text();
    var name = files.split('_');
    $.ajax({
        type: "POST",
        url: commonSettings.announcementAjaxurl + "/removeUploadedFile",
        data: {name: name[0], type: type},
        dataType: 'JSON',
        success: function (res) {
            if (res == 'success') {
                $(ctrl).closest('li').remove();
            }
        }
    });
}
announcementSettings.loadCommentsDiv = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/loadCommentsDiv",
        dataType: 'html',
        success: function (res) {
            $('#announcementComments').html(res);
        }
    });
};