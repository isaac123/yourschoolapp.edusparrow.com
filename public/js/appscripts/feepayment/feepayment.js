var feepaymentSettings = {};
feepaymentSettings.searchParams = {};
feepaymentSettings.loadstudetails = '';

feepaymentSettings.callfrom = '';

feepaymentSettings.loadClass = function (ctrl) {

    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/loadClass",
        data: {acd_yr: ctrl.value},
        dataType: "html",
        success: function (res) {
            $('#class_div').html(res);

        }
    });

}
feepaymentSettings.viewForwardDetails = function (ctrl) {
    var appData = {};
    appData.itemID = $(ctrl).attr('itemID');
    appData.appTypId = $(ctrl).attr('appType');
    console.log(appData);
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/viewForwardHistory",
        data: appData,
        //dataType: "JSON",
        success: function (res) {
//            console.log(res)
            $('.msgBox-title').html('List of Escalation made for this request');
            $('.msgBox-body').html(res);
            $('#messageBoxModal').modal();
        }
    });
};
feepaymentSettings.loadStudentFees = function () {

    feepaymentSettings.loadstudetails = 'stu';
    var applnData = {};
    var err = 0;

    //node_id is a hidden variable, when node is deleted node_id is not reset. so it is done here.
    var div = $('#divID').val();
    if (div == '')
    {
        $('#node_id').val('');
    }
    $("#feePaymentForm").find('select,input').each(function () {
        console.log($(this).attr('name') + ':' + $.trim($(this).val()))
        if ($(this).attr('name') == 'academicYrId' || $(this).attr('name') == 'stu_feeselectedID') {
            if ($.trim($(this).val()) == '') {
                err = 1;
                swal({
                    title: "Error",
                    text: $(this).attr('title') + ' is required!',
                    type: "error",
                    confirmButtonText: 'ok!',
                    closeOnConfirm: true,
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                console.log(err)
                                return false;
                            }
                        });
            } else {
                applnData[$(this).attr('name')] = $.trim($(this).val());
            }
        } else {
            applnData[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    console.log(applnData)
    if (err != 1) {
        feepaymentSettings.searchParams = applnData;
        feepaymentSettings.loadStudentFeeList();
    }
};

feepaymentSettings.loadStudentFeeList = function () {

    var pathurl = '';
    if (feepaymentSettings.callfrom == 'scholarschip')
    {
        pathurl = "/getStudentFeeListForSchlr";
    }
    else
    {
        pathurl = "/getStudentFeeList";
    }

    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + pathurl,
        data: feepaymentSettings.searchParams,
        dataType: "html",
        success: function (res) {
            $('#stuFeeDet').html(res)
        }
    });
};

feepaymentSettings.loadPForm = function (ctrl) {
    var feeId = $(ctrl).closest('tr').attr('feeId');
    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/feePaymentForm",
        data: {
            feeId: feeId
        },
        dataType: "html",
        success: function (res) {
            $(ctrl).closest('section').find('.payformDiv').html(res)
        }
    });
};
feepaymentSettings.feepayrec = function (ctrl) {

    $('#app_feecancel').addClass('hide');
    $('#app_feepay').addClass('hide');
    $('.loading_feepaycancel').removeClass('hide');
    var applnData = {};
    var err = 0;
    var error = '';

    var debitledger = $('#debitledgerid').val();
    var creditledger = $('#creditledgerid').val();
    var voucheramount = parseInt($('#voucheramount').val());
    var receivedby = $('#receivedbyid').val();
    var voucheramountfor = $('#voucheramountfor').val();
    var vouchertyp = $('#vouchertyp').val();

    var tobepaid = parseInt($('#tobepaidamount').val());
    applnData.vouchertyp = vouchertyp;
    console.log(voucheramount)
    if (!$.isNumeric(voucheramount)) {
        error = error + 'Invalid Amount <br>';
        err = 1;
        console.log(err + error)
    }
    else
    {
        if (voucheramount < 1)
        {
            error = error + 'Invalid Amount <br>';
            err = 1;
        }
        else if (voucheramount > tobepaid)
        {
            error = error + 'Amount exceeds the amount to be paid <br>';
            err = 1;
        }
    }
    $("#payFeeForm").find('input,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '' || $.trim($(this).val()) == '0') {
                err = 1;
                error += $(this).attr('title') + " is required!<br>";
            } else {
                applnData[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            applnData[$(this).attr('name')] = $.trim($(this).val());

        }

    });
    if (err === 0) {
//        swal({
//            title: "Confirm",
//            text: "Are you sure to pay ?",
//            type: "warning",
//            confirmButtonText: 'Yes!',
//            showCancelButton: true,
//            closeOnCancel: true,
//            closeOnConfirm: true,
//            cancelButtonText: 'Cancel',
//        },
//                function(isConfirm) {
//ConfirmModal
        $('.cnfBox-title').html('Message');
        $('.cnfBox-body').html('<div class="alert alert-block alert-danger fade in">\n\
Do you confirm to make fee payment for amount <strong>' + voucheramount + '</strong>? </div>');
        $('#ConfirmModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#confirm_yes').unbind('click').click(function () {
            $('#ConfirmModal').modal('hide');
myApp.showPleaseWait();
            $.ajax({
                type: "POST",
                url: commonSettings.feePaymentUrl + "/payFee",
                data: applnData,
                dataType: "JSON",
                success: function (res) {
                    if (res) {
                        if (res['type'] == 'error') {
                                $('#app_feecancel').removeClass('hide');
                                $('#app_feepay').removeClass('hide');
                                $('.loading_feepaycancel').addClass('hide');
                                return false;
                            }
                            else if (res['type'] == 'success') {
                                applnData.recordItemId = res.recordItemId;
                                applnData.recordTypId = res.recordTypId;
                                voucherSettings.makeNewVoucher(applnData);
                            }
                    }
                }
            });
        });

        $('#confirm_no').unbind('click').click(function () {
            $('#app_feecancel').removeClass('hide');
            $('#app_feepay').removeClass('hide');
            $('.loading_feepaycancel').addClass('hide');
        });

        //});


    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#msg_ok').unbind('click').click();
            $('#app_feecancel').removeClass('hide');
            $('#app_feepay').removeClass('hide');
            $('.loading_feepaycancel').addClass('hide');
            return false;
        });
        err = 0;
    }
};
feepaymentSettings.feepay = function (ctrl) {
    var applnData = {};
    var err = 0;

    $('#app_feecancel').addClass('hide');
    $('#app_feepay').addClass('hide');
    $('.loading_feepaycancel').removeClass('hide');
    $("#payFeeForm").find('input,textarea').each(function () {
        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
        if ($.trim($(this).val()) == '') {
            err = 1;
            swal({
                title: "Error",
                text: $(this).attr('title') + ' is required!',
                type: "error",
                confirmButtonText: 'ok!',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            console.log(err)
                            $('#app_feecancel').removeClass('hide');
                            $('#app_feepay').removeClass('hide');
                            $('.loading_feepaycancel').addClass('hide');
                            return false;
                        }
                    });
        }
        applnData[$(this).attr('name')] = $.trim($(this).val());
    });
    if (parseInt(applnData.amount) > parseInt(applnData.tobepaidamount)) {
        err = 1;
        swal({
            title: "Error",
            text: 'Paying amount exceeds the to be paid amount!',
            type: "error",
            confirmButtonText: 'ok!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        console.log(err)
                        $('#app_feecancel').removeClass('hide');
                        $('#app_feepay').removeClass('hide');
                        $('.loading_feepaycancel').addClass('hide');
                        return false;
                    }
                });
    }
    console.log(applnData)
    if (err != 1) {

        swal({
            title: "Confirm",
            text: "Are you sure to pay ?",
            type: "warning",
            confirmButtonText: 'Yes!',
            showCancelButton: true,
            closeOnCancel: true,
            closeOnConfirm: true,
            cancelButtonText: 'Cancel',
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: commonSettings.feePaymentUrl + "/payFee",
                            data: applnData,
                            dataType: "JSON",
                            success: function (res) {
                                if (res) {
                                    console.log(res)
                                    console.log(res.type)
                                    if (res['type'] == 'error') {
                                        swal("Error", res['message'], "error");
                                        $('#app_feecancel').removeClass('hide');
                                        $('#app_feepay').removeClass('hide');
                                        $('.loading_feepaycancel').addClass('hide');
                                        return false;
                                    }
                                    else if (res['type'] == 'success') {
                                        swal({
                                            title: "Done",
                                            text: res['message'],
                                            type: "success",
                                            confirmButtonText: 'Ok!',
                                            closeOnConfirm: true,
                                        },
                                                function (isConfirm) {
                                                    if (isConfirm) {
                                                        feepaymentSettings.loadStudentFeeList()
                                                    }
                                                });
                                    }

                                }
                            }
                        });
                    } else {
                        feepaymentSettings.loadStudentFeeList()
                    }
                });

    }
};
feepaymentSettings.feeactcancel = function (ctrl) {
    $('#app_feecancel').addClass('hide');
    $('#app_feepay').addClass('hide');
    $('.loading_feepaycancel').removeClass('hide');
    feepaymentSettings.loadStudentFeeList()
};

feepaymentSettings.loadCForm = function (ctrl) {
    var feeId = $(ctrl).closest('tr').attr('feeId');
    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/feeCancelForm",
        data: {
            feeId: feeId
        },
        dataType: "html",
        success: function (res) {

            $(ctrl).closest('section').find('.payformDiv').html(res)
        }
    });
};
feepaymentSettings.loadConcessionForm = function (ctrl) {
    var feeId = $(ctrl).closest('tr').attr('feeId');
    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/feeConcessionForm",
        data: {
            feeId: feeId
        },
        dataType: "html",
        success: function (res) {

            $(ctrl).closest('section').find('.payformDiv').html(res)
        }
    });
};

feepaymentSettings.cancelFeeForm = function (ctrl) {
    var applnData = {};
    var err = 0;

    $('#app_feecancel').addClass('hide');
    $('#app_feeactcancel').addClass('hide');
    $('.loading_feepaycancel').removeClass('hide');
    $("#cancelFeeForm").find('input,textarea').each(function () {
        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
        if ($.trim($(this).val()) == '') {
            err = 1;
            swal({
                title: "Error",
                text: $(this).attr('title') + ' is required!',
                type: "error",
                confirmButtonText: 'ok!',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            console.log(err)
                            $('#app_feecancel').removeClass('hide');
                            $('#app_feeactcancel').removeClass('hide');
                            $('.loading_feepaycancel').addClass('hide');
                            return false;
                        }
                    });
        }
        applnData[$(this).attr('name')] = $.trim($(this).val());
    });
    applnData.fromPage = 'StudentFeeList';
    console.log(applnData)
    if (err != 1) {
        swal({
            title: "Confirm",
            text: "Are you sure to cancel fee ?",
            type: "warning",
            confirmButtonText: 'Yes!',
            showCancelButton: true,
            closeOnCancel: true,
            closeOnConfirm: true,
            cancelButtonText: 'Cancel',
        },
                function (isConfirm) {
                    if (isConfirm) {
                        feepaymentSettings.makeNewCancelRequest(applnData);
                    } else {
                        feepaymentSettings.loadStudentFeeList()
                    }
                });

    }
};

feepaymentSettings.changeStatus = function (statusData) {
    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/changeApprovalStatus",
        data: statusData,
        dataType: "JSON",
        success: function (res) {
            if (res['type'] == 'error') {
                swal("Error!", res['message'], "error");
                return false;
            }
            else if (res['type'] == 'success') {
                swal({
                    title: res['type'],
                    text: res['message'],
                    type: res['type'],
                    confirmButtonText: 'OK',
                    closeOnConfirm: true,
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                if (statusData.fromPage == 'FeeCancel') {
                                    if (statusData['status'] == 'Approved') {
                                        feepaymentSettings.cancelFee(statusData);
                                    } else {
                                        approvalviewSettings.loadApptovalForFeeCancel()
                                    }
                                } else if (statusData.fromPage == 'StudentFeeList') {
                                    feepaymentSettings.loadStudentFees();
                                } else {
                                    window.location.reload()
                                }
                            }
                        });
            }
        }
    });
};
feepaymentSettings.cancelFee = function (statusData) {
    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/cancelApprovedFee",
        data: statusData,
        dataType: "JSON",
        success: function (res) {
            console.log(res['type'])
            var lblcls = res.type == 'error' ? 'danger' : 'success';
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
            $('#messageBoxModal').modal();
            $('#msg_ok').unbind('click').click(function (event) {
                $('#messageBoxModal').modal('hide');
                approvalviewSettings.loadApptovalForFeeCancel();
            });
            return false;

        }
    });
};
feepaymentSettings.makeNewCancelRequest = function (applnData) {
    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/addNewCancelRequest",
        data: applnData,
        dataType: "JSON",
        success: function (res) {
            swal({
                title: res['type'],
                text: res['message'],
                type: res['type'],
                confirmButtonText: 'OK',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            if (res['type'] == 'success') {

                                applnData.type = 'Fee cancel';
                                applnData.itemID = res['stuFeeCancelId'];
                                applnData.status = 'Requested';
                                console.log(applnData)
                                approvalSettings.checkReqStatus(applnData);
                            } else {
                                return false;
                            }
                        }
                    });
        }
    });
};

feepaymentSettings.ClickHereToPrint = function () {
    try {
        document.getElementById('print').style.display = 'none';
        var oIframe = document.getElementById('ifrmPrint');
        var oContent = document.getElementById('divToPrint').innerHTML;
        var oDoc = (oIframe.contentWindow || oIframe.contentDocument);
        if (oDoc.document)
            oDoc = oDoc.document;
        oDoc.write("<html><head><title>title</title>");
        //oDoc.write(\"<link href=".$path." rel=stylesheet type=text/css  />\");
        oDoc.write("</head><body onload='this.focus(); this.print();'>");
        oDoc.write(oContent + "</body></html>");
        oDoc.close();
    }
    catch (e) {
        self.print();
    }
}


feepaymentSettings.updateConcessionForStufee = function (ctrl)
{
//    var feeId = $(ctrl).closest('tr').attr('feeId');
    var feeId = $('#stufeeId').val();
    var concession_amt = $('#voucheramount').val();

    // alert(feeId + '-' + concession_amt);
    if (concession_amt > 0)
    {
        $.ajax({
            type: "POST",
            url: commonSettings.feePaymentUrl + "/updateConcessionForStufee",
            data: {feeId: feeId,
                concession_amt: concession_amt},
            dataType: "JSON",
            success: function (resArr) {
                console.log(resArr)
                if (resArr['type'] == 'error') {
                    swal({
                        title: 'Error',
                        text: resArr['message'],
                        type: resArr['type'],
                        confirmButtonText: 'OK',
                        closeOnConfirm: true,
                    });
                }
                else if (resArr['type'] == 'success') {

                    swal({
                        title: 'Message',
                        text: resArr['message'],
                        type: resArr['type'],
                        confirmButtonText: 'OK',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    if (resArr['type'] == 'success') {

                                        resArr.type = 'Concession';
                                        resArr.itemID = resArr['ItemID'];
                                        resArr.status = 'Requested';
                                        resArr.fromPage = 'Concession';
                                        approvalSettings.checkReqStatus(resArr);

                                        // feepaymentSettings.loadStudentFees();
                                    } else {
                                        return false;
                                    }
                                }
                            });

                    // approvalSettings.changeStatusForType(resArr)
                }
            }
        });
        //alert('true')
    }
    else
    {
        swal({
            title: 'Error',
            text: 'Please enter valid amount.',
            type: 'error',
            confirmButtonText: 'OK',
            closeOnConfirm: true,
        });

    }

}


feepaymentSettings.changeConcessionStatus = function (statusData) {
    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/changeConcessionApprovalStatus",
        data: statusData,
        dataType: "JSON",
        success: function (res) {
            if (res['type'] == 'error') {
                swal("Error!", res['message'], "error");
                return false;
            }
            else if (res['type'] == 'success') {
                swal({
                    title: res['type'],
                    text: res['message'],
                    type: res['type'],
                    confirmButtonText: 'OK',
                    closeOnConfirm: true,
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                if (statusData.fromPage == 'Concession') {
//                                    feepaymentSettings.loadStudentFees();
                                    feepaymentSettings.loadStudentFeeList()
                                } else if (statusData.fromPage == 'FeeConcession')
                                {
                                    approvalviewSettings.loadApptovalForFeeConcession();
                                }
                            }
                        });
            }
        }
    });
};
feepaymentSettings.changeInstallment = function (stuid, instypid) {
    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/changeInstallment",
        data: {
            stuid: stuid,
            instypid: instypid
        },
        dataType: "html",
        success: function (res) {
            $('.formBox-title').html('Change Installment');
            $('.formBox-body').html(res);
            $('#formModal').modal('show')
            $('#formModal #confirm_save').html('Change');
            $('#confirm_save').unbind('click').click(function () {
                $('#formModal').modal('hide')
                var newinstyp = $('#newInstalTyp').val()
                var stuid = $('#newInstalTyp').attr('stuId')
                var feemas = $('#newInstalTyp').attr('feeMasId')
                if (newinstyp == '' || stuid == '' || feemas == '') {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">Select valid Type</div>');
                    $('#messageBoxModal').modal();
                    feeSettings.loadFeeLst()
                    return false;
                } else {
                    $.ajax({
                        type: "POST",
                        url: commonSettings.feePaymentUrl + "/changeInsForStuByFee",
                        data: {
                            newinstyp: newinstyp,
                            stuid: stuid,
                            feemas: feemas
                        },
                        dataType: "JSON",
                        success: function (res) {

                            var lblcls = res.type == 'error' ? 'danger' : 'success';
                            $('.msgBox-title').html('Message');
                            $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                            $('#messageBoxModal').modal();
                            $('#msg_ok').unbind('click').click(function (event) {
                                $('#messageBoxModal').modal('hide');
                                feepaymentSettings.loadStudentFeeList()
                            });
                            return false;
                        }
                    });
                }

            });
        }
    });
}
feepaymentSettings.loadForwardDetails = function (ctrl) {
    var stuFeeData = {};
    stuFeeData.type = $(ctrl).attr('type');
    stuFeeData.itemID = $(ctrl).attr('itemID');
    stuFeeData.status = 'Requested';
    stuFeeData.fromPage = $(ctrl).attr('formPage');
    console.log(stuFeeData)
    approvalSettings.checkReqStatus(stuFeeData);
};