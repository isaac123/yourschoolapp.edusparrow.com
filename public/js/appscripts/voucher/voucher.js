var voucherSettings = {};
voucherSettings.vocheradd = '';
voucherSettings.oTable = '';

//voucher change status after approvel
voucherSettings.changeStatus = function (statusData) {
    var applnData = {};
    $.ajax({
        type: "POST",
        url: commonSettings.voucherAjaxurl + "/changeApprovalStatus",
        data: statusData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res);
                if (res['type'] === 'error') {
                    swal("Error!", res['message'], "error");
                    return false;
                }
                else if (res['type'] === 'success') {
                    if (res['vouchertype'] === "JOURNAL" && res['ledgerstatus'] === "Approved") {
                        applnData.itemID = res['voucherid'];
                        applnData.fromPage = statusData.fromPage;
                        voucherSettings.closevoucher(applnData);
                    } else {
                        res.fromPage = statusData.fromPage;
                        voucherSettings.makeCallBack(res);
                        /* swal({
                         title: "Done",
                         text: res['message'],
                         type: "success",
                         confirmButtonText: 'OK',
                         closeOnConfirm: true,
                         },
                         function(isConfirm) {
                         if (isConfirm) {
                         window.location.reload();
                         }
                         });*/
                    }
                }
            }
        }
    });
};

voucherSettings.closevoucher = function (applnData) {
    console.log('closevouvher')
    console.log(applnData)
    var voucherid = applnData.itemID;
    $.ajax({
        type: "POST",
        url: commonSettings.voucherAjaxurl + "/closevoucher",
        data: 'voucherid=' + voucherid,
        dataType: "JSON",
        success: function (res) {
            res.voucherid = voucherid;
            if (res['type'] == 'error') {
                swal("Error!", res['message'], "error");
                return false;
            }
            else if (res['type'] == 'success') {
                applnData.itemID = applnData.recordItemId;

                if (applnData.fromPage == 'Application')
                {
                    swal({
                        title: "Done",
                        text: res['message'],
                        type: "success",
                        confirmButtonText: 'Yes, Forward the request!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
//                                        applnData.type = 'Application';
//                                        applnData.itemID = res['ItemID'];
//                                        applnData.status = 'Requested';

                                    console.log(applicationSettings.stuapplndata)

                                    applicationSettings.fromPage = 'Application';
                                    if (applnData.fromModule == 'Application') {
                                        approvalSettings.checkReqStatus(applicationSettings.stuapplndata);
                                    } else {
                                        applicationSettings.changeStatus(applicationSettings.stuapplndata);
                                    }

                                }
                            });

                } else if (applnData.fromPage === 'vouchers') {
                    if (applnData.suspvoucherid > 0) {
                        voucherSettings.closesuspenseVoucher(applnData.suspvoucherid, 'edit')
                    } else {
                        voucherSettings.loadvoucherList();
                    }
                } else {


                    voucherSettings.makeCallBack(res);

                    if (voucherSettings.vocheradd == 'add')
                    {
                        financecycleSettings.loadAddVoucher();
                    }
                    //for reload to view updates.

                }

            }
        }
    });
};


voucherSettings.feeReceipt = function (voucherid) {
    console.log('receipt')

//    event.preventDefault();
//    event.stopPropagation();

    var newForm = $('<form>', {
        'action': commonSettings.templateAjaxurl + "/feeReceipt",
        'target': '_blank',
        'id': 'feeReceiptSubmitForm',
        'method': 'GET'
    });
    newForm.append($('<input>', {
        'name': 'voucherid',
        'value': voucherid,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};

//view voucher list
voucherSettings.loadvoucherList = function (savestate) {
    var voucherSearchData = {};
    var params = {};
    voucherSearchData.emptySearch = 1;

    //voucherSearchForm
    $("#viewVoucherListForm").find('input,select').each(function () {
        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
        if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
            voucherSearchData[$(this).attr('name')] = $.trim($(this).val());
            voucherSearchData.emptySearch = 0
        }

    });
    params = voucherSearchData;
    params.savestate = savestate;
    console.log(params);
    commonSettings.fromPage = 'VoucherList';
    $.ajax({
        type: "POST",
        url: commonSettings.voucherAjaxurl + "/voucherTblHdr",
        dataType: "html",
        success: function (res) {
            $('#voucher_div').html(res);
            var tablectrl = '#voucherListTable';
            var column = [];
            var sortingDisabled = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
            voucherSettings.oTable = $(tablectrl).dataTable({
                "sDom": '<"top"if>rt<"bottopm"p><"clear">',
                "bProcessing": true,
                "bServerSide": true,
                "bStateSave": true,
                "sAjaxSource": commonSettings.voucherAjaxurl + '/loadVoucherData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": commonSettings.NumberOfRecords,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[0, "desc"]],
                "bLengthChange": false,
                //"bInfo": false,
                "oLanguage": {
                    "sInfo": "<i class='fa fa-info activity-icon'></i> <i class='text-muted'><b>Showing _START_ to _END_ of _TOTAL_ Vouchers</b></i>",
                    "sInfoFiltered": " ",
                },
                "fnStateLoadParams": function (oSettings, oData) {
                    // Disallow state loading by returning false  
                    if (params.savestate == 0)
                    {
                        var loadStateParams = false;
                        params.savestate = 1;
                    }
                    return loadStateParams;
                },
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function (o) {
                    $('.dataTables_filter').addClass('col-md-5');
                    $('.dataTables_filter').find('label').addClass('col-md-3 search-txt1').css('display', ' inline');
                    $('.dataTables_filter').find('input').addClass('form-control pull-right search-tbox1');
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                    var rowText = '<tr appType ="' + aData.apprv_typ_id + '" itemID="' + aData.voucher_id + '">';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >';
                    rowText += aData.voucherdet;
                    rowText += '</span>';
                    rowText += '</td>';



                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.vouchertype + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.debitledger + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.creditledger + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.date + '</span>';
                    rowText += '</td>';

                    rowText += '<td class="bg-blue text_blk1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore text-primary fee_text">' + aData.amount + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    // rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.ledgerstatus + '</span>';
                    rowText += '<b><span class="text text-' + aData.labelclass + ' label-mini mr_fltlft mr_wd90 mr_forallmore">' + aData.ledgerstatus + '</span></b>';
                    rowText += '</td>';


                    // rowText += '<td>';
                    // rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.created_by + '</span>';
                    // rowText += '</td>';
                    rowText += '<td>';
                    if (aData.apprv_typ_id !== '')
                        rowText += '<a href="javascript:void(0)" onclick="approvalSettings.viewForwardDetails(this)"> <i class="fa fa-eye fa-2x"></i>  </a>';
                    rowText += '</td>';

                    // rowText += '<td>';
                    // rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.date + '</span>';
                    // rowText += '</td>';

                    rowText += '<td>';
                    rowText += aData.Operations;
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += aData.action;
                    rowText += '</td>';

                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });//.state.clear();
// voucherSettings.oTable.destroy();
        }
    });
}

//payment and suspense payment screen
voucherSettings.payVoucher = function (data) {
    console.log(data.id);
    var voucherid = data.id;
    //$('#payment_div').removeClass('hide');
    $.ajax({
        type: "POST",
        url: commonSettings.voucherAjaxurl + "/displyPaymentScreen",
        data: 'voucherid=' + voucherid,
        dataType: "html",
        success: function (res) {
            $('#voucher_div').html(res);

        }
    });
};


//payment and suspense  voucher cancel
voucherSettings.cancelVoucher = function (data) {
    console.log(data.id);
    var voucherid = data.id;
    // $('#payment_div').removeClass('hide');
    $.ajax({
        type: "POST",
        url: commonSettings.voucherAjaxurl + "/cancelVoucher",
        data: 'voucherid=' + voucherid,
        dataType: "html",
        success: function (res) {
            $('#voucher_div').html(res);

        }
    });
}

//voucher cancel update
voucherSettings.makeCancel = function () {
    var comments = $('#cancelcomments').val();
    var voucherid = $('#cancelvouchid').val();
    var err = 0;
    var error = '';
    if (comments === '') {
        error = 'Comments is Missing';
        err = 1;
    }

    if (err === 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.voucherAjaxurl + "/vouchercancelupdate",
            data: 'voucherid=' + voucherid + '&cancelledcomments=' + comments,
            dataType: "JSON",
            success: function (response) {
                if (response) {
                    console.log(response)
                    console.log(response['type'])
                    if (response['type'] === 'error') {
                        swal("Error!", response['message'], "error");
                        return false;
                    }
                    else if (response['type'] === 'success') {
                        swal({
                            title: "Done",
                            text: response['message'],
                            type: "success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: true,
                        },
                                function (isConfirm) {
                                    if (isConfirm) {
//                                        voucherSettings.loadvoucherList();

                                        voucherSettings.makeCallBack(response);
                                    }
                                });

                    }
                }

            }
        });
    } else {
        swal("Error", error, "error");
    }
};

//make payment
voucherSettings.makepayment = function (ctrl) {
    $(ctrl).addClass('hide')
    $(ctrl).next().addClass('hide')
    
    var comments = $('#comments').val();
    var voucherid = $('#ledgervouchid').val();
    var vouchertype = $('#vouchertype').val();
    var err = 0;
    var urlpath = '';
    var error = '';
    var data = '';
    if (comments === '') {
        error = 'Comments is Missing';
        err = 1;
    }

    if (err === 0) {
        if (vouchertype === 'SUSPENSE') {
            urlpath = commonSettings.voucherAjaxurl + "/paysuspensevoucher";
            data = 'voucherid=' + voucherid + '&paycomments=' + comments;
        } else if (vouchertype === 'PAYMENT') {
            urlpath = commonSettings.voucherAjaxurl + "/closevoucher";
            data = 'voucherid=' + voucherid + '&closedcomments=' + comments;
        }else if (vouchertype === 'CONTRA') {
            urlpath = commonSettings.voucherAjaxurl + "/closevoucher";
            data = 'voucherid=' + voucherid + '&closedcomments=' + comments;
        }
        $.ajax({
            type: "POST",
            url: urlpath,
            data: data,
            dataType: "JSON",
            success: function (response) {
                if (response) {
                    console.log(response)
                    console.log(response['type'])
                    if (response['type'] === 'error') {
                        swal("Error!", response['message'], "error");
                        return false;
                    }
                    else if (response['type'] === 'success') {
                        swal({
                            title: "Done",
                            text: response['message'],
                            type: "success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: true,
                        },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        if (response.recordtype === '5' || response.recordtype === '6') {
                                            voucherSettings.makeCallBack(response);
                                        } else {
                                            voucherSettings.loadvoucherList();
                                        }


                                    }
                                });

                    }
                }

            }
        });
    } else {
        swal("Error", error, "error");
    }
};

voucherSettings.makeCallBack = function (applnData) {
    console.log('makeCallBackVouc')
    console.log(applnData)
    console.log(commonSettings.fromPage)
    var rtype = applnData.recordtypename ? applnData.recordtypename.split(' ') : '-';
    if (applnData.recordtype && applnData.recordtype > 0) {
        if (applnData.recordtype === '6') {
            staffAdvanceSettings.changeStatus(applnData)
        }
        else if (applnData.recordtype === '5') {
            staffPayrollSettings.changeStatus(applnData)
        }
        else if ((applnData.recordtype === '1') || (applnData.recordtype === '2') ||
                (applnData.recordtype === '3') || (applnData.recordtype === '4')) {
            $.ajax({
                type: "POST",
                url: commonSettings.voucherAjaxurl,
                dataType: "html",
                success: function (res) {
                    $('#vouchersection').html(res);
                }
            });
        } else if (voucherSettings.vocheradd === 'add') {
            financecycleSettings.loadAddVoucher();
        } else if (feepaymentSettings.loadstudetails == 'stu') {
myApp.hidePleaseWait();
            feepaymentSettings.loadstudetails = '';
            feepaymentSettings.loadStudentFees();
            voucherSettings.feeReceipt(applnData.voucherid);
        } else {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html(applnData['message']);
            $('#messageBoxModal').modal('show');
            $('#msg_ok').unbind('click').click(function () {
                $('#msg_ok').unbind('click').click();
            });
            //swal("Success", applnData['message'], applnData['type']);
            //setTimeout(function() {
            //location.reload(); //commented
            //}, 2000);

        }
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html(applnData['message']);
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#msg_ok').unbind('click').click();
        });
        if (voucherSettings.vocheradd === 'add') {
            financecycleSettings.loadAddVoucher();
        } else if (commonSettings.fromPage == 'VoucherApproval') {
           approvalSettings.loadFinanceApprovalList();
        } else if (commonSettings.fromPage == 'VoucherList') {
            voucherSettings.loadvoucherList();
        }
    }
};



//close suspense voucher
voucherSettings.closesuspenseVoucher = function (dataid, isedit) {
    // console.log(data.id);
    var voucherid = dataid;
    var aprvl = 'View';
    $.ajax({
        type: "POST",
        url: commonSettings.voucherAjaxurl + "/closeSuspenseVoucher",
        data: {voucherid: voucherid,
            fromPage: aprvl,
            isedit: isedit},
        dataType: "html",
        success: function (res) {
            $('#voucher_div').html(res);

        }
    });
}

voucherSettings.viewVoucherFromApprvl = function (data) {
    console.log(data.id);
    var voucherid = data.id;
    var aprvl = 'Approval';
    var isedit = '';
    $.ajax({
        type: "POST",
        url: commonSettings.voucherAjaxurl + "/closeSuspenseVoucher",
        data: {voucherid: voucherid,
            fromPage: aprvl,
            isedit: isedit},
        dataType: "html",
        success: function (res) {
            $('#voucher_div').html(res);

        }
    });
}

//display  suspense close vouchers
voucherSettings.displaysuspclsevouchers = function (approval_amount) {
    //alert(approval_amount);
    var applnData = {};
    var error = 0;
    $("#clsesuspvouchform").find('input,select').each(function () {
        if ($.trim($(this).val()) !== '0') {
            applnData[$(this).attr('name')] = $.trim($(this).val());
        } else {
            error = 1;
            swal("Error", 'Voucher Type is Missing', "error");
        }
    });

    console.log(applnData);
    if (error === 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.voucherAjaxurl + "/displaySuspcloseVouchers",
            data: applnData,
            dataType: "html",
            success: function (res) {
                // $('.msgBox-title').html('Add ' + applnData.vouchtyp);
                // $('.msgBox-body').html(res);
                // $('.modal-footer').hide();
                // $('#messageBox').click();


                //console.log($.trim(res))
                $('.formBox-title').html('Add ' + applnData.vouchtyp);
                $('.formBox-body').html(res);
                $('#formModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#formModal').modal('show')
                // $('#confirm_close').addClass('hide');
                // $('#iconclose').addClass('hide');
                //console.log('1clciked')
                $('#confirm_save').html('Add')
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    //pass the approval amount and voucher type to check the limit for receipt voucher
                    voucherSettings.addsuspclosevoucher(approval_amount, applnData.vouchtyp)
                });


                //debit ledger autocomplete
                $('#debitledgername').typeahead({
                    ajax: {url: commonSettings.searchAjaxurl + '/debitLegderAutocomplt',
                        displayField: 'id',
                        dataType: "JSON",
                        triggerLength: 2,
                        items: 15,
                        preDispatch: function (query) {
                            $('#debitledgername').addClass('loadinggif');
                            return {
                                search: query,
                                record_type_id: $('#record_typ_id').val()
                            }
                        },
                        preProcess: function (data) {
                            $('#debitledgername').removeClass('loadinggif');
                            if (data.success === false) {
                                return false;
                            } else {
                                return data;
                            }
                        }
                    },
                    itemSelected: setdebitledger,
                });


                //credit ledger autoomplete  
                $('#creditledgername').typeahead({
                    ajax: {url: commonSettings.searchAjaxurl + '/creditLegderAutocomplt',
                        displayField: 'id',
                        dataType: "JSON",
                        triggerLength: 2,
                        items: 15,
                        preDispatch: function (query) {
                            $('#creditledgername').addClass('loadinggif');
                            return {
                                search: query,
                                record_type_id: $('#record_typ_id').val()
                            }
                        },
                        preProcess: function (data) {
                            $('#creditledgername').removeClass('loadinggif');
                            if (data.success === false) {
                                return false;
                            } else {
                                return data;
                            }
                        }
                    },
                    itemSelected: setcreditledger,
                });


                //received by autocomplete
                $('#receivedbyname').typeahead({
                    ajax: {url: commonSettings.searchAjaxurl + '/stfAutocomplt',
                        dataType: "JSON",
                        triggerLength: 2,
                        preDispatch: function (query) {
                            $('#receivedbyname').addClass('loadinggif');
                            return {
                                search: query
                            }
                        },
                        preProcess: function (data) {
                            $('#receivedbyname').removeClass('loadinggif');
                            if (data.success === false) {
                                return false;
                            } else {
                                return data;
                            }
                        }
                    },
                    itemSelected: setreceivedby,
                });

            }
        });
    }

}

function setdebitledger(item, val, text) {
    $('#debitledgerid').val(val);
    $('#selcteddebtledgnm').removeClass('hide');
    $('#selcteddebtledgnm span').html(item);
}
function setcreditledger(item, val, text) {
    $('#creditledgerid').val(val);
    $('#selctedcrdtledgnm').removeClass('hide');
    $('#selctedcrdtledgnm span').html(item);
}

function setreceivedby(item, val, text) {
    $('#receivedbyid').val(val);
}

//add suspence close vouchers
voucherSettings.addsuspclosevoucher = function (approval_amount, vouchtyp) {
    var applnData = {};
    var err = 0;
    var error = '';
    var debitledger = $('#debitledgerid').val();
    var creditledger = $('#creditledgerid').val();
    var suspvoucherid = $('#susp_voucherid').val();
    var suspclsevouchtyp = $('#suspclsevouchtyp').val();
    var voucheramount = $('#voucheramount').val();
    // var paymentcomments = $('#paymentcomments').val();
    var receivedby = $('#receivedbyid').val();
    //var paymentforwardto = $('#paymentforwardto').val();
    var voucheramountfor = $('#voucheramountfor').val();
    var apprvl_req = $('#apprvl_req').val();
    var apprvlmtamt = $('#apprvlmtamt').val();
    // alert(apprvl_req+','+apprvlmtamt);
    //exit;
    applnData.apprvl_req = apprvl_req;
    applnData.apprvlmtamt = apprvlmtamt;
    applnData.suspvoucherid = suspvoucherid;
    applnData.suspclsevouchtyp = suspclsevouchtyp;
    applnData.voucheramount = voucheramount;


    if (debitledger === '') {
        error = error + 'Ledger for Debit missing"\n"';
        err = 1;
    }
    if (creditledger === '') {
        error = error + 'Ledger for Credit missing"\n"';
        err = 1;
    }
    if (voucheramount === '') {
        error = error + 'Amount missing"\n"';
        err = 1;
    }
    else
    {
        if (voucheramount < 0)
        {
            error = error + 'Amount cannot be Negative"\n"';
            err = 1;
        }
        else
        {
            if (vouchtyp == 'Receipt Voucher')
            {
                if (voucheramount > approval_amount)
                {
                    error = error + 'Amount Should Not Exceed ' + approval_amount + '"\n"';
                    err = 1;
                }
            }
        }
    }

    if (receivedby === '') {
        error = error + 'Received by missing"\n"';
        err = 1;
    }

    if (voucheramountfor === '') {
        error = error + 'Amount for is missing"\n"';
        err = 1;
    }
    $("#addSuspClosevouchers").find('input,textarea').each(function () {
        applnData[$(this).attr('name')] = $.trim($(this).val());
    });
    applnData['fromPage'] = 'vouchers';
    if (err === 0) {
        voucherSettings.makesuspclosevoucher(applnData)
    }
    else
    {
        swal({
            title: "Error",
            text: error,
            type: "error",
            confirmButtonText: 'ok',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#formModal').modal('show')
                    }
                });
    }
}

//save suspense close vouchers
voucherSettings.makesuspclosevoucher = function (applnData) {
    console.log(applnData);
    $.ajax({
        type: "POST",
        url: commonSettings.voucherAjaxurl + "/saveSuspcloseVoucher",
        data: applnData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                if (res['type'] === 'success') {
                    $('#messageBoxModal').hide();
                    applnData.itemID = res['ItemID'];
                    //alert(applnData.apprvl_req)
                    if (applnData.apprvl_req !== '0') {
                        if (parseInt(applnData.voucheramount) >= parseInt(applnData.apprvlmtamt)) {
                            swal({
                                title: "Done",
                                text: res['message'],
                                type: "success",
                                confirmButtonText: 'Yes, Forward the request!',
                                closeOnConfirm: true,
                            },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            //$('.modal-footer').hide();
                                            applnData.type = res['suspclsevouchtyp'];
                                            applnData.status = 'Forwarded';
                                            approvalSettings.checkReqStatus(applnData);
                                        }
                                    });
                        } else {
                            //Amount less then approvel limit directly close voucher
                            voucherSettings.closevoucher(applnData);
                        }
                    } else {
                        //Directly close voucher
                        voucherSettings.closevoucher(applnData);
                    }
                } else {
                    swal("Error", res['message'], res['type'])
                }
            }
        }
    });
}

//suspense close balance update
voucherSettings.suspensebalclose = function (suspvouchid) {
    var pendvouch = $('#pendvouch').val();
    if (pendvouch > 0) {
        swal("Information", "Voucher in Pendind Status", "info");
        return false;
    } else {
        var susvouchamt = $('#susvouamt').val()
        var suspayamt = $('#suspayamt').val()
        var recbilamt = $('#recbilamt').val()
        var finamt = parseInt(recbilamt) - parseInt(suspayamt);
        if (parseInt(finamt) !== parseInt(susvouchamt)) {
            swal("Information", "Amount is not equal to Suspense Voucher", "info");
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: commonSettings.voucherAjaxurl + "/suspclosestatusupdate",
                data: 'suspvouchid=' + suspvouchid,
                dataType: "JSON",
                success: function (res) {
                    if (res) {
                        console.log(res)
                        console.log(res.type)
                        if (res['type'] === 'success') {
                            swal("Success", res['message'], res['type']);

                            voucherSettings.loadvoucherList();

                        } else {
                            swal("Error", res['message'], res['type'])
                        }
                    }
                }
            });

        }
    }
}

//add vouchers
voucherSettings.addvoucher = function () {
    voucherSettings.vocheradd = 'add';
    var applnData = {};
    var err = 0;
    var error = '';
    var debitledger = $('#debitledgerid').val();
    var creditledger = $('#creditledgerid').val();
    var voucheramount = $('#voucheramount').val();
    var receivedby = $('#receivedbyid').val();
    var voucheramountfor = $('#voucheramountfor').val();
    var apprvl_req = $('#apprvl_req').val();
    var apprvlmtamt = $('#apprvlmtamt').val();
    var vouchertyp = $('#vouchertyp').val();
    applnData.apprvl_req = apprvl_req;
    applnData.apprvlmtamt = apprvlmtamt;
    applnData.vouchertyp = vouchertyp;

    if (debitledger === '') {
        error = error + 'Ledger for Debit missing"\n"';
        err = 1;
    }
    if (creditledger === '') {
        error = error + 'Ledger for Credit missing"\n"';
        err = 1;
    }
    if (voucheramount === '') {
        error = error + 'Amount missing"\n"';
        err = 1;
    }
    else
    {
        if (voucheramount < 0)
        {
            error = error + 'Amount cannot be Negative"\n"';
            err = 1;
        }
    }

    if (receivedby === '') {
        error = error + 'Received by missing"\n"';
        err = 1;
    }

    if (voucheramountfor === '') {
        error = error + 'Amount for is missing"\n"';
        err = 1;
    }
    $("#createvoucherform").find('input,textarea').each(function () {
        applnData[$(this).attr('name')] = $.trim($(this).val());
    });
    if (err === 0) {
        $('.loading_addadv').removeClass('hide')
        $('#add_payment_voucher').addClass('hide')
        voucherSettings.makeNewVoucher(applnData)
    }
    else
    {
        swal({
            title: "Error",
            text: error,
            type: "error",
            confirmButtonText: 'ok',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        //  $('#formModal').modal('show')
                    }
                });
    }
};


//save New vouchers
voucherSettings.makeNewVoucher = function (applnData) {
    console.log(applnData);
    $.ajax({
        type: "POST",
        url: commonSettings.voucherAjaxurl + "/saveNewVoucher",
        data: applnData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                if (res['type'] === 'success') {
                    $('#messageBoxModal').hide();
                    applnData.itemID = res['ItemID'];
                    applnData.voucherId = res['ItemID'];
                    if (applnData.apprvl_req !== '0') {
                        if (parseInt(applnData.voucheramount) >= parseInt(applnData.apprvlmtamt)) {
                            swal({
                                title: "Done",
                                text: res['message'],
                                type: "success",
                                confirmButtonText: 'Yes, Forward the request!',
                                closeOnConfirm: true,
                            },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            applnData.type = applnData.vouchertyp;
                                            applnData.status = 'Forwarded';
                                            applnData.comments = applnData.voucheramountfor;
                                            approvalSettings.checkReqStatus(applnData);
                                        }
                                    });
                        } else {
                            //Amount less then approvel limit directly close voucher
                            voucherSettings.closevoucher(applnData);
                        }
                    } else {
                        //Directly close voucher
                        voucherSettings.closevoucher(applnData);
                    }
                } else {
                    swal("Error", res['message'], res['type'])
                }
            }
        }
    });
};

voucherSettings.deleteVoucherByID = function (voucherid, cntrl) {

    var frompage = $(cntrl).attr('fromPage');

//    alert(frompage);

    $('.cnfBox-title').html('Message');
    $('.cnfBox-body').html('<div class="alert alert-block alert-warning fade in">\n\
                               <i></i>    <strong>Confirm To Delete Transaction Voucher..?</strong></form></div>');

//     $('.cnfBox-body').html('<div class="alert alert-block alert-danger fade in"><form id"vocherDeleteForm"\n\
//                            name="vocherDeleteForm" action="javascript:;">\n\
//                            <br><input type="checkbox" name="recalculate_transctn" id="recalculate_transctn" /> Recalculate Transaction..?\n\
//                             <br><input type="checkbox" name="change_ldgr_balnc" id="change_ldgr_balnc" /> Make Ledger balance as 0..?\n\
//                            <br>&nbsp;&nbsp;&nbsp; <strong>Confirm to delete voucher..?</strong></form></div>');
//   

    $('#ConfirmModal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#confirm_yes').unbind('click').click(function () {
        $('#ConfirmModal').modal('hide');

        var current_effect = 'bounce';
        //stuFeelist
        voucherSettings.run_waitMe(current_effect);

        var recalculate_transctn = 'yes';
        var change_ldgr_balnc = 'yes';

//        if($('#recalculate_transctn').is(":checked")){
//            recalculate_transctn = 'yes';
//        }
//        
//         if($('#change_ldgr_balnc').is(":checked")){
//            change_ldgr_balnc = 'yes';
//        }

        console.log(recalculate_transctn)
        console.log(change_ldgr_balnc)


        $.ajax({
            type: "POST",
            url: commonSettings.voucherAjaxurl + "/deleteVoucherById",
            data: {
                voucherid: voucherid,
                recalculate_transctn: recalculate_transctn,
                change_ldgr_balnc: change_ldgr_balnc
            },
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    // console.log(res)
                    console.log(res.type)
                    if (res['type'] === 'success') {
                        // swal("Success", res['message'], res['type']);
                        $('#stuFeelist').waitMe('hide');

                        $('.msgBox-title').html('Message');
                        $('.msgBox-body').html(res['message']);
                        $('#messageBoxModal').modal('show');
                        $('#msg_ok').unbind('click').click(function () {
                            $('#msg_ok').unbind('click').click();

                            if (frompage == 'stuvoucher')
                            {
                                printSettings.getStudentlist();
                            } else {
                                voucherSettings.loadvoucherList();
                            }
                        });

                    } else {
                        // swal("Error", res['message'], res['type'])
                        $('.msgBox-title').html('Message');
                        $('.msgBox-body').html(res['message']);
                        $('#messageBoxModal').modal('show');
                        $('#msg_ok').unbind('click').click(function () {
                            $('#msg_ok').unbind('click').click();
                            if (frompage == 'stuvoucher')
                            {
                                printSettings.getStudentlist();
                            } else {
                                voucherSettings.loadvoucherList();
                            }
                        });
                    }
                }
            }
        });
    });
};


voucherSettings.run_waitMe = function (effect) {
    $('#stuFeelist').waitMe({
        effect: effect,
        text: 'Please wait deleting...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: ''
    });
};


voucherSettings.editVoucherByID = function (voucherid, cntrl) {

    var frompage = $(cntrl).attr('fromPage');
    $.ajax({
        type: "POST",
        url: commonSettings.voucherAjaxurl + "/editVoucherByID",
        data: {voucherid: voucherid},
        dataType: "html",
        success: function (res) {
            //console.log(res);
            $('.formBox-title').html('Edit Voucher ' + voucherid);
            $('.formBox-body').html(res);
            // $('#modalForm').click();
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#formModal').modal('show');
            $('#confirm_save').html('Update');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                voucherSettings.updateVoucher();
            });
        }
    });
}

voucherSettings.updateVoucher = function () {
    var params = {};
    var err = 0;
    var error = '';

    $("#voucherEditForm").find('input,select,textarea').each(function () {

        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n";

            } else {
                params[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }

    });
    var current_effect = 'bounce';
    //stuFeelist
    voucherSettings.run_waitMe(current_effect);
    $.ajax({
        type: "POST",
        url: commonSettings.voucherAjaxurl + "/updateVoucherByID",
        data: params,
        dataType: "JSON",
        success: function (res) {
            $('#stuFeelist').waitMe('hide');
            if (res) {
                $('.msgBox-title').html(res.type);
                $('.msgBox-body').html(res.message);
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                        return false;
                    } else if (res['type'] == 'success') {
                        voucherSettings.loadvoucherList();

                    }
                });
            }

        }
    });
}
voucherSettings.loadForwardHistory = function (ctrl) {
    var applnData = {};
    applnData.itemID = $(ctrl).attr('id');
    applnData.voucherId = $(ctrl).attr('id');
    applnData.type = $(ctrl).attr('typeid');
    applnData.status = 'Forwarded';
    applnData.voucheramountfor = $(ctrl).attr('reason');
    console.log(applnData);
    approvalSettings.checkReqStatus(applnData);

};