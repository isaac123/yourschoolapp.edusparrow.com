var voucherviewSettings = {};
voucherviewSettings.loadvoucherDetails = function (savestate) {
    var voucherSearchData = {};
    var params = {};
    voucherSearchData.emptySearch = 1;
    $("#viewVoucherListForm").find('input,select').each(function () {
        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
        if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
            voucherSearchData[$(this).attr('name')] = $.trim($(this).val());
            voucherSearchData.emptySearch = 0
        }

    });
    params = voucherSearchData;
    params.savestate = savestate;
    console.log(params);
    $.ajax({
        type: "POST",
        url: commonSettings.vouchereditAjaxurl + "/voucherTableHeader",
        dataType: "html",
        success: function (res) {
            $('#voucherlistDiv').html(res);
            var tablectrl = '#voucherEditTable';
            var column = [];
            var sortingDisabled = [0, 1, 2, 3, 4, 5, 6, 7];
            voucherviewSettings.oTable = $(tablectrl).dataTable({
                "sDom": '<"top"if>rt<"bottopm"p><"clear">',
                "bProcessing": true,
                "bServerSide": true,
                "bStateSave": true,
                "sAjaxSource": commonSettings.vouchereditAjaxurl + '/voucherTableData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": commonSettings.NumberOfRecords,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[0, "desc"]],
                "bLengthChange": false,
                "oLanguage": {
                    "sInfo": "<i class='fa fa-info activity-icon'></i> <i class='text-muted'><b>Showing _START_ to _END_ of _TOTAL_ Vouchers</b></i>",
                    "sInfoFiltered": " ",
                },
                "fnStateLoadParams": function (oSettings, oData) {
                    if (params.savestate == 0)
                    {
                        var loadStateParams = false;
                        params.savestate = 1;
                    }
                    return loadStateParams;
                },
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function (o) {
                    $('.dataTables_filter').addClass('col-md-5');
                    $('.dataTables_filter').find('label').addClass('col-md-3 search-txt1').css('display', ' inline');
                    $('.dataTables_filter').find('input').addClass('form-control pull-right search-tbox1');
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                    var rowText = '<tr appType ="' + aData.apprv_typ_id + '" itemID="' + aData.voucher_id + '">';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >';
                    rowText += aData.voucherdet;
                    rowText += '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.vouchertype + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.debitledger + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.creditledger + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.date + '</span>';
                    rowText += '</td>';

                    rowText += '<td class="bg-blue text_blk1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore text-primary fee_text">' + aData.amount + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<b><span class="text text-' + aData.labelclass + ' label-mini mr_fltlft mr_wd90 mr_forallmore">' + aData.ledgerstatus + '</span></b>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += aData.action;
                    rowText += '</td>';

                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });
        }
    });
}
voucherviewSettings.editVoucher = function (voucherid, ctrl) {
    var frompage = $(ctrl).attr('fromPage');
    $.ajax({
        type: "POST",
        url: commonSettings.vouchereditAjaxurl + "/editVoucher",
        data: {voucherid: voucherid},
        dataType: "html",
        success: function (res) {
            $('.formBox-title').html('Edit Voucher ' + voucherid);
            $('.formBox-body').html(res);
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#formModal').modal('show');
            $('#confirm_save').html('Update');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                voucherviewSettings.updateVoucher(voucherid);
            });
        }
    });
}

voucherviewSettings.updateVoucher = function (voucherid) {
    var params = {};
    var err = 0;
    var error = '';
    $("#voucherEditForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                error  = $(this).attr('title') + ' is required'
                err = 1;
            } else {
                params[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }

    });
    console.log(params);
    if (!$.isNumeric(params['voucher_amount'])) {
        error = error + 'Invalid Amount ';
        err = 1;
    }

    if (parseFloat(params['voucher_amount']) < 1)
    {
        error = error + 'Invalid Amount ';
        err = 1;
    }
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.vouchereditAjaxurl + "/updateVoucher",
            data: params,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    swal({
                        title: res.type,
                        text: res.message,
                        type: res.type,
                        confirmButtonText: 'OK!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    voucherviewSettings.loadvoucherDetails(0);
                                }
                            });
                }

            }
        });
    } else {
        swal({
            title: "Error",
            text: error,
            type: 'error',
            confirmButtonText: 'OK!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        voucherviewSettings.editVoucher(voucherid);
                        return false;

                    }
                });
    }
}
voucherviewSettings.deleteVoucher = function (voucherid) {
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html('Deleting this Voucher will also delete related entries.<br> Do you confirm to delete this Voucher?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.vouchereditAjaxurl + "/deleteVoucher",
            data: {voucherid: voucherid},
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    swal({
                        title: res.type,
                        text: res.message,
                        type: res.type,
                        confirmButtonText: 'OK!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    if (res.item == '1') {
                                        staffAdvanceSettings.loadStaffdet();
                                    }
                                    else {
                                        voucherviewSettings.loadvoucherDetails(0);
                                    }

                                }
                            });
                }
            }
        });
    });
}