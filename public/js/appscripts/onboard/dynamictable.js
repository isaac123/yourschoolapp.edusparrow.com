var jdtableSettings = {};
jdtableSettings.tablectrl = '.divisionValuesTable';
jdtableSettings.i = {};
jdtableSettings.first_word = ['eth', 'First', 'Second', 'Third', 'Fouth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Ninth', 'Tenth', 'Eleventh', 'Twelfth', 'Thirteenth', 'Fourteenth', 'Fifteenth', 'Sixteenth', 'Seventeenth', 'Eighteenth', 'Nineteenth', 'Twentieth'];
jdtableSettings.restoreRow = function (ctrl, nRow) {
    /* Get table handle */
    var tblecurctrl = $(jdtableSettings.tablectrl).index($(ctrl).parents('table'));
    var dttablectrl = ostuTable.table(tblecurctrl);
    var dat = dttablectrl
            .rows(nRow)
            .data();
    var aData = dat[0];
    // var aData = ostuTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);

  var actnbtn = '<a href="javascript:;" onclick="jdtableSettings.editAction(this)" class=" btn btn-default btn-sm m-user-edit"><i class="zmdi zmdi-edit"></i></a>'
    if(aData[0] == (jdtableSettings.i[tblecurctrl]-1) ||  !jdtableSettings.i[tblecurctrl]){

    actnbtn += (jdtableSettings.i[tblecurctrl] == 2 || !jdtableSettings.i[tblecurctrl])  ? '' : '<a href="#"  onclick="jdtableSettings.deleteRow(this)" class="removebtn btn btn-default btn-sm m-user-delete"><i class="zmdi zmdi-minus" ></i></a>';

    actnbtn += '<a href="#" onclick="jdtableSettings.addRow(this)" class="addbtn btn btn-default btn-sm m-user-edit"><i class="zmdi zmdi-plus"></i></a>';
}
aData[3] = actnbtn;
    dttablectrl
            .row(nRow)
            .data(aData)
            .draw();
    /*for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
     ostuTable.fnUpdate(aData[i], nRow, i, false);
     
     }*/

    //ostuTable.fnDraw();

};

jdtableSettings.editRow = function (dttablectrl, nRow, option) {
    console.log(nRow)
    var dat = dttablectrl
            .rows(nRow)
            .data();
    console.log(dat)
    var aData = dat[0];
//ostuTable.fnGetData(nRow);
    console.log(aData)
    var mode = option == 'new' ? 'data-mode="new"' : '';
    var jqTds = $('>td', nRow);
    jqTds[0].innerHTML = aData[0];
    jqTds[1].innerHTML = aData[1];
    jqTds[2].innerHTML = '<input type="number" min="0" step="1" value="' + aData[2] + '" class="normal" />';
    // jqTds[3].innerHTML = '<a href="javascript:;"  onclick="jdtableSettings.deleteRow(this)" class="removebtn btn btn-default btn-sm m-user-delete"><i class="zmdi zmdi-minus" ></i></a><a href="javascript:;" onclick="jdtableSettings.addRow(this)" class="addbtn btn btn-default btn-sm m-user-edit"><i class="zmdi zmdi-plus"></i></a>'; 
    jqTds[3].innerHTML = '<a href="javascript:;"  onclick="jdtableSettings.editAction(this)" class="btn btn-default btn-sm m-user-delete" action ="Save"><i class="zmdi zmdi-save" ></i></a><a href="javascript:;" '+mode+' onclick="jdtableSettings.cancelRow(this)" class="addbtn btn btn-default btn-sm m-user-edit"><i class="zmdi zmdi-close"></i></a>';
};

var column = [];
var sortingDisabled = [0, 1, 2, 3];

var ostuTable = $(jdtableSettings.tablectrl).DataTable({
    "sDom": 'C<"clear">lrtip',
    //"bProcessing": false,
    "paging": false,
    "iDisplayLength": setupSettings.noOfRecordsToDisplay,
    "aoColumnDefs": [{
            "sDefaultContent": "",
            "aTargets": column
        }, {
            "bSortable": false,
            "sDefaultContent": "",
            "aTargets": sortingDisabled
        }],
    "bLengthChange": false,
    "bInfo": false
});

console.log(ostuTable)
jQuery('#editable-sample_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

var nEditing = null;

jdtableSettings.addRow = function (ctrl) {
//$('.addbtn').parent().append('<a href="#"  onclick="jdtableSettings.deleteRow(this)" class="btn btn-default btn-sm m-user-delete"><i class="zmdi zmdi-minus" ></i></a>');
    console.log($(jdtableSettings.tablectrl).index($(ctrl).parents('table')))
    var tblecurctrl = $(jdtableSettings.tablectrl).index($(ctrl).parents('table'));
    $(ctrl).parents('table').find('.removebtn').remove()
    $(ctrl).parents('table').find('.addbtn').remove()
    if (!jdtableSettings.i[tblecurctrl]) {
        jdtableSettings.i[tblecurctrl] = 2;
    }
    var dttablectrl = ostuTable.table(tblecurctrl);
    console.log(dttablectrl)
    var i = jdtableSettings.i[tblecurctrl];
    var ii = parseInt(i) + 1;
    //var aiNew = dttablectrl.fnAddData([ i,jdtableSettings.first_word[i]+' Language', '1', '<a class="edit" href="javascript:;">Edit</a>']);
    var aiNew = dttablectrl.row.add([i, jdtableSettings.first_word[i] + ' Language', '1', '']).draw();
    console.log(aiNew[0])
    //var nRow = dttablectrl.fnGetNodes(aiNew[0]);
    var nRow = dttablectrl
            .row(aiNew[0])
            .node();
    jdtableSettings.editRow(dttablectrl, nRow, 'new');
    nEditing = nRow;
    jdtableSettings.i[tblecurctrl] = ii;

}
jdtableSettings.deleteRow = function (ctrl) {
    var tblecurctrl = $(jdtableSettings.tablectrl).index($(ctrl).parents('table'));
    if (confirm("Are you sure to delete this row ?") == false) {
        return;
    }
    var dttablectrl = ostuTable.table(tblecurctrl);
    jdtableSettings.i[tblecurctrl]--;
    var prevRow = $(ctrl).parents('tr').prev('tr')[0];

    //dttablectrl.fnUpdate(actnbtn, prevRow, 3, false);
    var prevdata = dttablectrl.row(prevRow).data()
    console.log(prevdata)
 console.log(prevdata[0])
    console.log(jdtableSettings.i[tblecurctrl])
  var actnbtn = '<a href="javascript:;" onclick="jdtableSettings.editAction(this)" class=" btn btn-default btn-sm m-user-edit"><i class="zmdi zmdi-edit"></i></a>'
    if(prevdata[0] == (jdtableSettings.i[tblecurctrl]-1) ||  !jdtableSettings.i[tblecurctrl]){

    actnbtn += (jdtableSettings.i[tblecurctrl] == 2 || !jdtableSettings.i[tblecurctrl])  ? '' : '<a href="#"  onclick="jdtableSettings.deleteRow(this)" class="removebtn btn btn-default btn-sm m-user-delete"><i class="zmdi zmdi-minus" ></i></a>';

    actnbtn += '<a href="#" onclick="jdtableSettings.addRow(this)" class="addbtn btn btn-default btn-sm m-user-edit"><i class="zmdi zmdi-plus"></i></a>';
}
    prevdata[3] = actnbtn;
    prevdata[2] = prevdata[2];
    console.log(prevdata)
    dttablectrl
            .row(prevRow)
            .data(prevdata)
            .draw();

    var nRow = $(ctrl).parents('tr');//$(ctrl).parents('tr')[0];
    var jqInputs = $('td', nRow);
//dttablectrl.fnDeleteRow(nRow);

    dttablectrl
            .row(nRow)
            .remove()
            .draw();
};


jdtableSettings.editAction = function (ctrl) {
    /* Get table handle */
    var tblecurctrl = $(jdtableSettings.tablectrl).index($(ctrl).parents('table'));
    var dttablectrl = ostuTable.table(tblecurctrl);

    /* Get the row as a parent of the link that was clicked on */
    var nRow = $(ctrl).closest('tr')[0];
    nEditing = nRow;
    if (nEditing !== null && nEditing != nRow) {
        console.log('nEditing1')
        /* Currently editing - but not this row - restore the old before continuing to edit mode */
        //  jdtableSettings.restoreRow(ostuTable, nEditing);
        jdtableSettings.editRow(dttablectrl, nRow, 'edit');
        nEditing = nRow;
    } else if (nEditing == nRow && $(ctrl).attr('action') == "Save") {
        console.log('nEditing2')
        /* Editing this row and want to save it */
        console.log(nEditing)
        var jqInputs = $('input', nEditing);
        jdtableSettings.saveRow(ctrl, nEditing);
        nEditing = null;

    } else {
        console.log('nEditing3')
        /* No edit in progress - let's start one */
        jdtableSettings.editRow(dttablectrl, nRow, 'edit');
        nEditing = nRow;
    }
}


jdtableSettings.saveRow = function (ctrl, nRow) {
    /* Get table handle */
    var tblecurctrl = $(jdtableSettings.tablectrl).index($(ctrl).parents('table'));
    var dttablectrl = ostuTable.table(tblecurctrl);

    var jqInputs = $('input', nRow);
    console.log(jqInputs)
    if (jqInputs[0].value == '0') {
        $('.msgBox-title').html('Error');
        $('.msgBox-body').html('Invalid input!');
        $('#messageBoxModal').modal('show');
        return false;
    }

    var prevdata = dttablectrl.row(nRow).data()

    console.log(prevdata[0])
    console.log(jdtableSettings.i[tblecurctrl])
    var actnbtn = '<a href="javascript:;" onclick="jdtableSettings.editAction(this)" class=" btn btn-default btn-sm m-user-edit"><i class="zmdi zmdi-edit"></i></a>'
    if(prevdata[0] == (jdtableSettings.i[tblecurctrl]-1) ||  !jdtableSettings.i[tblecurctrl]){

    actnbtn += (jdtableSettings.i[tblecurctrl] == 2 || !jdtableSettings.i[tblecurctrl])  ? '' : '<a href="#"  onclick="jdtableSettings.deleteRow(this)" class="removebtn btn btn-default btn-sm m-user-delete"><i class="zmdi zmdi-minus" ></i></a>';

    actnbtn += '<a href="#" onclick="jdtableSettings.addRow(this)" class="addbtn btn btn-default btn-sm m-user-edit"><i class="zmdi zmdi-plus"></i></a>';
}
    
    prevdata[3] = actnbtn;
    prevdata[2] = jqInputs[0].value;
    console.log(prevdata)
    dttablectrl
            .row(nRow)
            .data(prevdata)
            .draw();


//    ostuTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
//    ostuTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
//    ostuTable.fnUpdate((jqInputs[2].value == '1' ? 'Yes' : 'No'), nRow, 2, false);
    //   ostuTable.fnUpdate('<a class="edit" href="javascript:;" onclick ="jdtableSettings.editAction(this)">Edit</a>', nRow, 1, false);
    //   ostuTable.fnUpdate('<a class="delete" href="javascript:;" onclick = "jdtableSettings.deleteRow(this)">Delete</a>', nRow, 2, false);
    //   ostuTable.fnDraw();


};


jdtableSettings.cancelRow = function (ctrl) {
    /* Get table handle */
    var tblecurctrl = $(jdtableSettings.tablectrl).index($(ctrl).parents('table'));
    var dttablectrl = ostuTable.table(tblecurctrl);
    if ($(ctrl).attr("data-mode") == "new") {
        jdtableSettings.deleteRow(ctrl)
    } else {
        jdtableSettings.restoreRow(ctrl, nEditing);
        nEditing = null;
    }

};
