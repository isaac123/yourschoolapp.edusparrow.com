var setupSettings = {};
setupSettings.noOfRecordsToDisplay = 10;
setupSettings.tablectrl = '#divisionValuesTable';
setupSettings.stftablectrl = '#stfdivisionValuesTable';
setupSettings.progressPercentPrev = 0;
setupSettings.progressPercentNext = 0;
setupSettings.addBasicInfo = function (ctrl) {
    var schoolData = {};
    var form = $('#msform'); // You need to use standart javascript object here
    var formData = new FormData(form[0]);
    var err = 0;
    var label = '';
//    formData.append('file1', $("#file")[0].files[0]);
//    schoolData.fileform = data;
    $("#msform").find('input,select,textarea').each(function () {
        var txt = $.trim($(this).val());
        if ($(this).parent().find('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                $(this).css('border-color', 'rgb(213,0,0)')
                err = 1;
                console.log($.trim($(this).attr('name')));

            } else {
                $(this).css('border-color', 'green')
            }

        } else if ($(this).hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                $(this).css('border-color', 'rgb(213,0,0)')
                err = 1;
                console.log('2');
            } else {
                $(this).css('border-color', 'green')
            }
        }
//        if (($(this).attr('name') == 'admission_prefix' || $(this).attr('name') == 'appointment_prefix')
//                || ($(this).attr('name') == 'admission_sufix' || $(this).attr('name') == 'appointment_sufix')) {
//            schoolData[$(this).attr('name')] = $.trim($(this).val());
//            schoolData[$(this).attr('name')] = $.trim($(this).val());
//            txt = $.trim($(this).val());
//            partn = /^[a-zA-Z0-9]*$/;
//            if (!partn.test(txt)) {
//                $(this).css('border-color', 'rgb(213,0,0)')
//                err = 1;
//                console.log('3');
//            }
//        }
    });
//    var admprf = schoolData.admission_prefix + '*' + schoolData.admission_sufix;
//    var appprf = schoolData.appointment_prefix + '*' + schoolData.appointment_sufix;
    console.log(err)
    if (err == 1) {
        return false;
    } else if (err == -1) {
        $('.msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('.msgBox-body').html('<span class="text text-danger fade in">Atleast one asterik(*) must be found in Admission/Appointment format</span>');
        $('#messageBoxModal').modal('show');
        return false;
    }/* else if (admprf == appprf) {
     $("#msform").find('input[name="admission_prefix"],input[name="appointment_prefix"]').css('border-color', 'rgb(213,0,0)')
     $('.msgBox-title').html('<i class="fa  fa-times"></i> Error');
     $('.msgBox-body').html('<span class="text text-danger fade in">Admission and Appointment format cannot be the same</span>');
     $('#messageBoxModal').modal('show');
     return false;
     }*/ else {
        $(ctrl).addClass('hide')
        $(ctrl).next('.loading').removeClass('hide')
        $.ajax({
            type: "POST",
            url: commonSettings.onboardUrl + "/addBasicInfo",
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    if (res['type'] == 'success') {
                        $.ajax({
                            type: "POST",
                            url: commonSettings.onboardUrl + "/checkValueTree",
                            dataType: "JSON",
                            success: function (res) {
                                if (res) {
                                    if (res.type == "orgtree") {
                                        setupSettings.loadTree();
                                    } else {
                                        setupSettings.loadTemplate();
                                    }
                                }

                            }
                        });

                    } else {
                        $(ctrl).removeClass('hide')
                        $(ctrl).next('.loading').addClass('hide')
                        if (("keys" in res)) {
                            var obj = JSON.parse(res.keys)
                            if (!$.isEmptyObject(obj)) {
                                $.each(obj, function (i, v) {
                                    $("#msform").find('input[name="' + v + '"]').css('border-color', 'rgb(213,0,0)')
                                })
                            }
                        }
                        $('.msgBox-title').html('<i class="fa  fa-times"></i> Error');
                        $('.msgBox-body').html(res.message);
                        $('#messageBoxModal').modal('show');
                        return false;
                    }

                }
            }
        });
    }
};

setupSettings.loadTree = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.orgmaster + "/valueTreeD3",
        data: {onboard: 'true'},
        dataType: "HTML",
        success: function (res) {
            $('.stepwizard-two').find('.panel-body').html(res)
            formstepSettings.nextStep($('.stepwizard-one'));
        }});
};

setupSettings.loadTemplate = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadTemp",
        dataType: "HTML",
        success: function (res) {
            $('.stepwizard-two').find('.panel-body').html(res)
            $('.stepwizard-two').find('.next').addClass('hide')
            formstepSettings.nextStep($('.stepwizard-one'));
        }});
};
/** update basic info **/
setupSettings.updateBasicInfo = function (ctrl) {
    var schoolData = {};
    var form = $('#msedtform'); // You need to use standart javascript object here
    var formData = new FormData(form[0]);
    formData.append('file1', $("#file")[0].files[0]);
//    schoolData.fileform = data;
    $("#msedtform").find('input,textarea').each(function () {
        formData.append($(this).attr('name'), $.trim($(this).val()));
    });


    console.log(formData)
    $.ajax({
        type: "POST",
        url: commonSettings.setupAjaxurl + "/updateBasicInfo",
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                if (res['type'] === 'success') {
//                        console.log($(ctrl).parents('.panel').parent().html())
                    setTimeout(function () {
                        window.location.href = window.location.pathname + window.location.search
                    }, 0);
                } else {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                    });
                }

            }
        }
    });
};

setupSettings.addAcademicYear = function (ctrl) {
    var academicYrData = {};

    $("#academicFormSection").find('input,textarea').each(function () {
        academicYrData[$(this).attr('name')] = $.trim($(this).val());
    });


    console.log(academicYrData)
    var nextTab = $(ctrl).closest(".tab-pane").next(".tab-pane").attr('id');

    $.ajax({
        type: "POST",
        url: commonSettings.setupAjaxurl + "/addacdyr",
        data: academicYrData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                if (res.type == 'success') {
                    formstepSettings.nextTab(nextTab);
                } else {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                    });

                }

            }
        }
    });
};

setupSettings.addDivision = function (ctrl) {
    var divisionData = {};
    if ($(ctrl).next('input').val() == 'student') {
        $("#StuDivFormSection").find('input').each(function () {
            divisionData[$(this).attr('name')] = $.trim($(this).val());
        });
    } else {
        $("#stfDivFormSection").find('input').each(function () {
            divisionData[$(this).attr('name')] = $.trim($(this).val());
        });

    }



    $.ajax({
        type: "POST",
        url: commonSettings.setupAjaxurl + "/addDivision",
        data: divisionData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                if (res.type == 'error') {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                    });

                }

            }
        }
    });
};

setupSettings.loadDivValues = function () {
    $element = $('#subdiv');
    $('#subdiv').removeClass('hide');
    var divisionId = $('#divID').val();
    $.ajax({
        type: "POST",
        url: commonSettings.setupAjaxurl + "/loadSubdiv",
        data: {divId: divisionId},
        success: function (res) {
            $element.html(res)
            $('.stepSubmit2').removeClass('hide')
            $('.save2').removeClass('hide')

        }
    });
}

setupSettings.addSubSection = function (ctrl) {
//    
    var subDivName = $('#newsubdivname').val();
    var subDivCount = $('#subDivName').attr('subDivCount');
    var all_values = $('#all_values').val();
    if (!subDivName) {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('Sub division name is required');
        $('#messageBoxModal').modal('show');
        return false;
    }
    var err = 1;
    var subdivisionData = {};
    $("#subDivSection").find('input,select').each(function () {
        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
//        console.log($(this).html())
        if ($.trim($(this).val()) != '' && $(this).attr('name') != 'option[]') {
            err = 0;
        }
        subdivisionData[$(this).attr('name')] = $.trim($(this).val());
    });
//    console.log(all_values);
//    return false;
    if (err == 1 && subDivName) {
        $('.msgBox-title').html('Error');
        $('.msgBox-body').html('Invalid Input!<br> SubDivision values are required');
        $('#messageBoxModal').modal('show');
        return false;

    } else {

        $.ajax({
            type: "POST",
            url: commonSettings.setupAjaxurl + "/addSubDivMaster",
            data: subdivisionData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    if (res.type == 'error') {
                        $('.msgBox-title').html('Error');
                        $('.msgBox-body').html(res['message']);
                        $('#messageBoxModal').modal('show');
                        $('#msg_ok').unbind('click').click(function () {
                            if (res['type'] == 'error') {
                                $('#msg_ok').unbind('click').click();
                                return false;
                            }
                        });

                    } else {
                        $('.msgBox-title').html('Success');
                        $('.msgBox-body').html(res['message']);
                        $('#messageBoxModal').modal('show');
//                    setupSettings.divSecReload()
                        setupSettings.loadDivValues()
                    }

                }
            }
        });
    }

}

setupSettings.downloadStaffExcel = function (ctrl) {

    var newForm = $('<form>', {
        'action': commonSettings.setupXlCreateAjaxurl + "/staffExcelGenerate",
        'target': '_blank',
        'id': 'staffExcelForm'
    });
    newForm.appendTo("body").submit();
};
setupSettings.downloadStudentExcel = function (ctrl) {

    var stuExcelData = {};

    event.preventDefault();
    event.stopPropagation();

    var newForm = $('<form>', {
        'action': commonSettings.setupXlCreateAjaxurl + "/stuExcelGenerate",
        'target': '_blank',
        'id': 'studentExcelForm',
        'method': 'POST'
    })
    $("#stuUploadFormSection").find('input,select').each(function () {
        newForm.append($('<input>', {
            'name': $(this).attr('name'),
            'value': $.trim($(this).val()),
            'type': 'hidden'
        }));
//        stuExcelData[$(this).attr('name')] = $.trim($(this).val());
    });
    newForm.appendTo("body").submit();
};
setupSettings.loadDivValtoUpload = function (ctrl) {
    $element = $('#divVal');
    $('#divVal').removeClass('hide');
    //var divisionId = $(ctrl).val();
    var divisionId = $('#LdivID').val();
    $.ajax({
        type: "POST",
        url: commonSettings.setupAjaxurl + "/loadDivVal",
        data: {divId: divisionId},
        success: function (res) {
            $element.html(res)
            setupSettings.validFormDetails(divisionId)
        }
    });
}
setupSettings.loadDivValtodownloadPdf = function () {

    $element = $('#pdfdivVal');
    $('#pdfdivVal').removeClass('hide');
    var divisionId = $('#pdfdivID').val();
    $.ajax({
        type: "POST",
        url: commonSettings.setupAjaxurl + "/loadDownloadLink",
        data: {divId: divisionId},
        success: function (res) {
            $element.html(res)

        }
    });
}
setupSettings.validFormDetails = function (divisionId) {
    $.ajax({
        type: "POST",
        url: commonSettings.setupAjaxurl + "/validStudentDetails",
        data: {divId: divisionId},
        dataType: "JSON",
        success: function (res) {
            if (res.type == 'yes') {
                $('.stepSubmit3').removeClass('hide')
                console.log('show')
            } else {
                $('.stepSubmit3').addClass('hide')
            }
        }
    });
}


setupSettings.downloadStfPdf = function () {
    console.log('got')
    var newForm = $('<form>', {
        'action': commonSettings.setupAjaxurl + "/staffPdfDownload",
        'target': '_blank',
        'id': 'staffPDF',
        'method': 'post'
    });
    newForm.appendTo("body").submit();
}
setupSettings.downloadStupdf = function () {
    console.log('got')
    var newForm = $('<form>', {
        'action': commonSettings.setupAjaxurl + "/stuPdfDownload",
        'target': '_blank',
        'id': 'stuPDF',
        'method': 'post'
    });
    newForm.appendTo("body").submit();
}

setupSettings.downloadStuPdf = function (divname) {
    if (divname) {
        var newForm = $('<form>', {
            'action': commonSettings.setupAjaxurl + "/studentPdfDownload",
            'target': '_blank',
            'id': 'studentPDF',
            'method': 'post'
        }).append($('<input>', {
            'name': 'divname',
            'value': divname,
            'type': 'hidden'
        }));
        newForm.appendTo("body").submit();
    }
}
setupSettings.CompleteSettings = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.setupAjaxurl + "/completeSettings",
        data: {iscomplete: '1'},
        success: function (res) {
            $('.modal-title').html('Message');
            $('.modal-body').html(res);
            $('#messageBox').click();
            $('#msg_ok').click(function () {
                window.location.href = commonSettings.assigningAjaxurl;
            });

        }
    });
}


setupSettings.divSecReload = function () {
    $element = $('#divSecReload');
    $.ajax({
        type: "POST",
        url: commonSettings.setupAjaxurl + "/getDivOptions",
        success: function (res) {
            $element.html(res)
            $('#subdiv').html('');
            formstepSettings.nextTab('subDiv')
        }
    });
}

setupSettings.divSecUploadReload = function () {
    $element = $('#divSecUploadReload');
    $.ajax({
        type: "POST",
        url: commonSettings.setupAjaxurl + "/getDivOptionForUpload",
        success: function (res) {
            $element.html(res)
            $('#divVal').html('');
            formstepSettings.nextTab('stuUpload')
        }
    });
}
//$('#msform ul.nav>li>a').on('click', function () {
//    console.log('clikednav')
//
//    event.preventDefault();
//    event.stopPropagation();
//
//});

setupSettings.loadSubDivByDiv = function (subdivId) {
    $element = $('#subdivVal');
    var divisionId = $('#divID').val();
    var redirectUrl = (subdivId == 0) ? "/addNewSubDiv" : "/editSubDiv";
    $.ajax({
        type: "POST",
        url: commonSettings.setupAjaxurl + redirectUrl,
        data: {subdivId: subdivId,
            divId: divisionId},
        success: function (res) {
            $element.html(res)
            $element.removeClass('hide')
        }
    });

}

setupSettings.validateDivisionForm = function (ctrl, next) {
    var divisionData = {};
    $(ctrl).find('input').each(function () {
        divisionData[$(this).attr('name')] = $.trim($(this).val());
    });

    if (divisionData.divname == '') {

        $('.msgBox-title').html('Error');
        $('.msgBox-body').html('Invalid Input!<br> Division Name is required');
        $('#messageBoxModal').modal('show');
        return false;
    } else {

        $.ajax({
            type: "POST",
            url: commonSettings.setupAjaxurl + "/checkDivision",
            data: divisionData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    if (res.type == 'error') {
                        $('.msgBox-title').html('Message');
                        $('.msgBox-body').html(res['message']);
                        $('#messageBoxModal').modal('show');
                        $('#msg_ok').unbind('click').click(function () {
                            if (res['type'] == 'error') {
                                $('#msg_ok').unbind('click').click();
                                return false;
                            }
                        });

                    } else {
                        if (next == 'subDiv')
                            setupSettings.divSecReload()
                        else
                            formstepSettings.nextTab(next)
                    }

                }
            }
        });

    }


}
/*
 setupSettings.loadDivisionValues = function() {
 var params = {};
 $.ajax({
 type: "POST",
 url: commonSettings.setupAjaxurl + "/loadDivisionForm",
 data: params,
 dataType: "html",
 success: function(res) {
 $('#StuDivFormSection').html(res)
 var column = [0, 1, 2, 3, 4];
 var sortingDisabled = [0, 1, 2, 3, 4];
 var oTable = $(setupSettings.tablectrl).dataTable({
 // "sDom": 'C<"clear">lrtip',
 //"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
 "bProcessing": true,
 "bServerSide": true,
 "sAjaxSource": commonSettings.setupAjaxurl + '/loadDivisionValueTable',
 "sPaginationType": "full_numbers",
 "iDisplayLength": setupSettings.noOfRecordsToDisplay,
 "aoColumnDefs": [{
 "sDefaultContent": "",
 "aTargets": column
 }, {
 "bSortable": false,
 "sDefaultContent": "",
 "aTargets": sortingDisabled
 }],
 "aaSorting": [[1, "desc"]],
 "bLengthChange": false,
 "bInfo": false,
 "fnServerData": function(surl, aoData, l) {
 for (var key in params) {
 aoData.push({
 "name": key,
 "value": params[key]
 });
 }
 $.ajax({
 dataType: "json",
 type: "POST",
 url: surl,
 contentType: "application/x-www-form-urlencoded;charset=UTF-8",
 data: aoData,
 success: l
 });
 },
 "fnDrawCallback": function(o) {
 },
 "fnRowCallback": function(nRow, aData, iDisplayIndex) {
 var rowText = '<tr stuid="' + aData.id + '">';
 rowText += '<td>';
 rowText += '<div class="mr_fltlft mr_wd34px mr_txtcntr">';
 rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.class_name + '</span></div>';
 rowText += '</td>';
 rowText += '<td class="new-class">';
 rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.order + '</span>';
 rowText += '</td>';
 
 rowText += '<td class="new-class">';
 rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.promotion_status + '</span>';
 rowText += '</td>';
 
 rowText += '<td><a class="edit" href="javascript:;">Edit</a></td>';
 rowText += '<td><a class="delete" href="javascript:;">Delete</a></td>';
 rowText += '</tr>';
 var newRow = $(rowText);
 $(nRow).html(newRow.html());
 commonSettings.copyAttributes(newRow, nRow);
 return nRow;
 }
 }).makeEditable({
 sUpdateURL: commonSettings.setupAjaxurl + "/UpdateData.php",
 sAddURL: commonSettings.setupAjaxurl + "/AddData.php",
 sDeleteURL: commonSettings.setupAjaxurl + "/DeleteData.php"
 });
 }
 });
 };
 
 commonSettings.restoreRow = function(oTable, nRow) {
 var aData = oTable.fnGetData(nRow);
 var jqTds = $('>td', nRow);
 
 for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
 oTable.fnUpdate(aData[i], nRow, i, false);
 }
 
 oTable.fnDraw();
 }
 
 setupSettings.editRow = function(oTable, nRow) {
 var aData = oTable.fnGetData(nRow);
 console.log(aData)
 var jqTds = $('>td', nRow);
 jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[0] + '">';
 jqTds[1].innerHTML = '<input type="text" class="form-control small" value="' + aData[1] + '">';
 jqTds[2].innerHTML = '<select class="form-control" name="promoteStatus" id="promoteStatus">\n\
 <option value="1" selected="selected">Yes</option>\n\
 <option value="2">No</option>\n\
 </select>';
 //jqTds[3].innerHTML = '<input type="text" class="form-control small" value="' + aData[3] + '">';
 jqTds[3].innerHTML = '<a class="edit" href="">Save</a>';
 jqTds[4].innerHTML = '<a class="cancel" href="">Cancel</a>';
 }
 
 setupSettings.saveRow = function(oTable, nRow) {
 var jqInputs = $('input', nRow);
 oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
 oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
 oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
 //oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
 oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 3, false);
 oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 4, false);
 oTable.fnDraw();
 }
 
 setupSettings.cancelEditRow = function(oTable, nRow) {
 var jqInputs = $('input', nRow);
 oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
 oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
 oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
 //oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
 oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 3, false);
 oTable.fnDraw();
 }
 
 setupSettings.loadDivisionValues();
 */
//Circul;ar progress
/*var progressbar = 'progress';
 //alert(progressbar);
 progressbar = new CircularProgress({
 radius: 25,
 strokeStyle: '#27AE60',
 lineJoin: 'round',
 lineWidth: 4,
 shadowBlur: 0,
 shadowColor: '#27AE60',
 text: {
 font: '500 10px arial',
 shadowBlur: 0
 },
 initial: {
 strokeStyle: '#dddddd',
 lineJoin: 'round',
 shadowBlur: 2,
 shadowColor: '#dddddd'
 }
 });
 */
//document.getElementById("topLoader").appendChild(progressbar.el);


//setupSettings.progressReDraw = function () {
//    n = setupSettings.progressPercentPrev;
//    id = setInterval(function () {
//        if (n == setupSettings.progressPercentNext)
//            clearInterval(id);
//        progressbar.update(n++);
//    }, 30);
//}
//setupSettings.progressReDraw();


setupSettings.loadDistinctCombintion = function (ctrl) {
    $(ctrl).addClass('hide')
    $(ctrl).next('.loading').removeClass('hide')
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadDistinctCombinations",
        dataType: "HTML",
        success: function (res) {
            $(ctrl).removeClass('hide')
            $(ctrl).next('.loading').addClass('hide')
            $('.stepwizard-three').find('.body').html(res)
            formstepSettings.nextStep($('.stepwizard-two'));
        }});
}
setupSettings.uploadStudent = function (filenamestu, aggreval) {
    pleaseWaitDiv.find('.text-display').html('Importing file...')
    myApp.showPleaseWait()
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/studentUpload",
        data: {filenamestu: filenamestu,
            aggreval: aggreval},
        dataType: "JSON",
        success: function (resp) {
            if (resp) {
                myApp.hidePleaseWait()

                if (resp.type == "success") {
//                    setupSettings.loadStudet();
                    assignSectionSettings.loadStudentsOnboard();
                } else {
                    $('.msgBox-title').html('<i class="fa  fa-times"></i> Error');
                    $('.msgBox-body').html('<span class="text text-danger fade in">' + resp.message + '</span>');
                    $('#messageBoxModal').modal('show');
                }
            }

        }
    });

};

setupSettings.loadStudet = function ( ) {
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadDistinctCombinations",
        dataType: "HTML",
        success: function (res) {
            $('.stepwizard-three').find('.body').html(res)
        }});

}

setupSettings.loadStudentList = function (ctrl) {
    var agg = $(ctrl).attr('aggregate_key');
    var aggname = $(ctrl).attr('aggergate_name');
    $(ctrl).parent('table').find('tr').removeClass('unread');
    $(ctrl).addClass('unread');
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadStudents",
        data: {aggreval: agg, aggname: aggname},
        dataType: "HTML",
        success: function (resp) {
            $('.stunav').html(resp)

        }
    });
};
setupSettings.downloadSampleExcel = function (usertyp) {
    $('#downloadSampleExcel').remove()
    var newForm = $('<form>', {
        'action': commonSettings.onboardUrl + "/downloadSampleExcel",
        'target': '_blank',
        'id': 'downloadSampleExcel',
        method: 'POST'
    }).append($('<input>', {
        'name': 'usertyp',
        'value': usertyp,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};
setupSettings.downloadSAppStu = function (ctrl) {

    var agg = $(ctrl).parents('tr').attr('aggregate_key');
    var aggname = $(ctrl).parents('tr').attr('aggergate_name');
    $('#downloadSAppStu').remove()
    var newForm = $('<form>', {
        'action': commonSettings.onboardUrl + "/downloadSAppStu",
        'target': '_blank',
        'id': 'downloadSAppStu',
        method: 'POST'
    }).append($('<input>', {
        'name': 'agg',
        'value': agg,
        'type': 'hidden'
    })).append($('<input>', {
        'name': 'aggname',
        'value': aggname,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};

setupSettings.loadDistinctCombintionStf = function (ctrl) {
    $(ctrl).addClass('hide')
    $(ctrl).next('.loading').removeClass('hide')

    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadDistinctCombintionStf",
        dataType: "HTML",
        success: function (res) {
            $(ctrl).removeClass('hide')
            $(ctrl).next('.loading').addClass('hide')
            $('.stepwizard-three').find('.body').html(res)
            $('.stepwizard-three').find('.body').next().find('.next').attr('onclick', "setupSettings.addClassrooms(this)")
//            $('.stepwizard-three').find('.body').next().next().html('<button type="button" class=" loading btn btn-success primary-btn processing col-md-2 pull-right hide">Processing...</button>')
        }});
};
setupSettings.uploadStaff = function (filenamestu, aggreval) {
    pleaseWaitDiv.find('.text-display').html('Importing file...')
    myApp.showPleaseWait()
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/staffUpload",
        data: {filenamestu: filenamestu,
            aggreval: aggreval},
        dataType: "JSON",
        success: function (resp) {
            if (resp) {
                myApp.hidePleaseWait()

                if (resp.type == "success") {
//                    setupSettings.staffdet()
                    appointmentSettings.loadStaffOnboard();

                } else {
                    $('.msgBox-title').html('<i class="fa  fa-times"></i> Error');
                    $('.msgBox-body').html('<span class="text text-danger fade in">' + resp.message + '</span>');
                    $('#messageBoxModal').modal('show');
                }
            }

        }
    });

};

setupSettings.staffdet = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadDistinctCombintionStf",
        dataType: "HTML",
        success: function (res) {
            $('.stepwizard-three').find('.body').html(res)
        }});
}
setupSettings.downloadSAppStf = function (ctrl) {

    var agg = $(ctrl).parents('tr').attr('aggregate_key');
    var aggname = $(ctrl).parents('tr').attr('aggergate_name');
    $('#downloadSAppStf').remove()
    var newForm = $('<form>', {
        'action': commonSettings.onboardUrl + "/downloadSAppStf",
        'target': '_blank',
        'id': 'downloadSAppStf',
        method: 'POST'
    }).append($('<input>', {
        'name': 'agg',
        'value': agg,
        'type': 'hidden'
    })).append($('<input>', {
        'name': 'aggname',
        'value': aggname,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};
setupSettings.loadStaffList = function (ctrl) {
    var agg = $(ctrl).attr('aggregate_key');
    var aggname = $(ctrl).attr('aggergate_name');
    $(ctrl).parent('table').find('tr').removeClass('unread');
    $(ctrl).addClass('unread');
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadStaffList",
        data: {aggreval: agg, aggname: aggname},
        dataType: "HTML",
        success: function (resp) {
            $('.stunav').html(resp)

        }
    });
};

setupSettings.addClassrooms = function (ctrl) {
    $(ctrl).addClass('hide')
    $(ctrl).next('.loading').removeClass('hide')
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/generateClassRooms",
        dataType: "JSON",
        success: function (resp) {
            if (resp) {
                if (resp.type == "success") {
                    $.ajax({
                        type: "GET",
                        url: commonSettings.assigningAjaxurl + "/addClassroom",
                        data: {onboard: 'true'},
                        dataType: "HTML",
                        success: function (resp) {
                            var res = resp.split('_');
                            if (res[0] == 'error') {
                                $('.msgBox-title').html('Message');
                                var msg = res[1] == '1' ? 'Upload students for atleast one class' : 'Upload teaching staff';
                                $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + msg + '</div>');
                                $('#messageBox').click();
                                $(ctrl).next('.loading').addClass('hide')
                                $(ctrl).removeClass('hide')
                                return false;
                            } else {
                                $(ctrl).removeClass('hide')
                                $(ctrl).next('.loading').addClass('hide')
                                $('.stepwizard-four').find('.body').html(resp)
                                formstepSettings.nextStep($('.stepwizard-three'))
                            }

                        }
                    });

                } else {
                    $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
                    $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">' + resp.message + '</span>');
                    $('#messageBoxModal').modal('show');
                }
            }
        }
    });

};
setupSettings.saveSettings = function (ctrl) {

    $(ctrl).addClass('hide')
    $(ctrl).next('.loading').removeClass('hide')
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/saveSettings",
        dataType: "JSON",
        success: function (resp) {
console.log(resp)
		 if (resp.type == 'error') {
                                $('.msgBox-title').html('Message'); 
                                $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + resp.msg + '</div>');
                                $('#messageBox').click();
                                return false;
                            } else {
				window.location.href = commonSettings.staffAjaxurl + "/dashboard";
                            }
        }
    });
};

setupSettings.loadUploadView = function (ctrl) {
    $(ctrl).addClass('hide')
    $(ctrl).next('.loading').removeClass('hide')
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadUploadView",
        dataType: "HTML",
        success: function (res) {
            $(ctrl).removeClass('hide')
            $(ctrl).next('.loading').addClass('hide')
            $('.stepwizard-three').find('.body').html(res)
            formstepSettings.nextStep($('.stepwizard-two'));
        }});
}

setupSettings.loadAssigningTable = function (ctrl) {
    $(ctrl).addClass('hide')
    $(ctrl).next('.loading').removeClass('hide')
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadAssigningTable",
        dataType: "HTML",
        success: function (res) {
            $(ctrl).removeClass('hide')
            $(ctrl).next('.loading').addClass('hide')
            $('.stepwizard-three').find('.body').html(res)
            formstepSettings.nextStep($('.stepwizard-two'));
        }});
}

setupSettings.loadCombinationTable = function (ctrl) {
    var selectedSchool = $(ctrl).val()
    if (selectedSchool) {
        $('#combitable').html('<span class="loading  col-md-8 pull-right " style="padding-top:50px;"><i class="fa spinner fa-spin fa-spinner "></i> Loading ...</span>')
        $.ajax({
            type: "POST",
            url: commonSettings.onboardUrl + "/loadCombiTable",
            data: {schoolid: selectedSchool},
            dataType: "HTML",
            success: function (res) {
                $('#combitable').html(res)
            }});
    }
};

setupSettings.loadStudentListByCombi = function (ctrl) {
    var agg = $(ctrl).attr('aggregate_key');
    var aggname = $(ctrl).attr('aggergate_name');
    $(ctrl).parent('table').find('tr').removeClass('unread');
    $(ctrl).addClass('unread');
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadStudentListByCombi",
        data: {aggreval: agg, aggname: aggname},
        dataType: "HTML",
        success: function (resp) {
            $('#studentsList').html(resp)

        }
    });
};

setupSettings.loadStudentsUploadData = function () {
    $('#combitable').html('<span class="loading  col-md-8 pull-right " style="padding-top:50px;"><i class="fa spinner fa-spin fa-spinner "></i> Loading ...</span>')
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/studentAssigning",
        success: function (res) {
            if (res) {
                $('#combitable').html(res)
            }

        }
    });
};


setupSettings.loadStaffUploadData = function () {
    $('#combitable').html('<span class="loading  col-md-8 pull-right " style="padding-top:50px;"><i class="fa spinner fa-spin fa-spinner "></i> Loading ...</span>')
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadStaffUploadData",
        dataType: "HTML",
        success: function (res) {
            $('#combitable').html(res)
        }
    });
};

setupSettings.basicTree = function () {
    var err = 0, error = '', stuData = {};
    $("#loadTempForms").find('input').each(function () {
        if ($(this).parents('.form-group').find('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is Required<br>";
            } else {
                stuData[$(this).attr('name')] = $.trim($(this).val());
            }
        } else if ($(this).hasClass('mandatory')) {
            stuData[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    console.log(stuData)
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.onboardUrl + "/basicTree",
            dataType: "HTML",
            data: stuData,
            success: function (res) {
                $('#generateTable').html(res)
            }});
    } else {
        $('.msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('.msgBox-body').html('<span class="text text-danger fade in">' + error + '</span>');
        $('#messageBoxModal').modal('show');
        return false;
    }
};

setupSettings.findDuplicates = function () {

    var allName = {}, error = '', dupicateName = {}, err = 0, valueData = {};
    $('#basicDyntree').find('.textfield').each(function (index, el) {
        allName[$(el).attr('nodename')] = [];
        dupicateName[$(el).attr('nodename')] = [];
    }
    );
    $('#basicDyntree').find('.textfield').each(function (index, el) {
        var name = $(el).val();
        if (name != '') {
            if (jQuery.inArray(name, allName[$(el).attr('nodename')]) === -1) {
                allName[$(el).attr('nodename')].push(name);
            } else {
                dupicateName[$(el).attr('nodename')].push(name);
            }
        }
    }
    );
    $.each(dupicateName, function (key, value) {
        $.each(value, function (vsalue, vdalue) {
            error += '"' + vdalue + '" exists already<br>';
            err = 1;
        });

    });

    $('#loadTempForms').find('input').each(function () {
        if ($.trim($(this).val()) == '') {
            error += $(this).attr('title') + '  is required<br>';
            err = 1;
        } else {
            valueData[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    $('#basicDyntree').find('input').each(function () {
        if ($.trim($(this).val()) == '') {
            error += '"Missing Values<br>';
            err = 1;
        } else {
            if (!valueData['aggr'])
            {
                valueData['aggr'] = {};
            }
            if (!valueData['aggr'][$(this).attr('trrow')]) {
                valueData['aggr'][$(this).attr('trrow')] = {};
            }
            if (!valueData['aggr'][$(this).attr('trrow')][$(this).attr('masid')]) {
                valueData['aggr'][$(this).attr('trrow')][$(this).attr('masid')] = {};
            }

            valueData['aggr'][$(this).attr('trrow')][$(this).attr('masid')] = $.trim($(this).val());
        }

    })
    console.log(valueData)
    console.log(err)
    if (err == 0) {
        myApp.showPleaseWait()
        $.ajax({
            type: "POST",
            url: commonSettings.onboardUrl + "/generateValueTree",
            dataType: "JSON",
            data: valueData,
            success: function (resp) {
                if (resp) {
                    myApp.hidePleaseWait()
                    if (resp.type == "success") {
                        //next
                        setupSettings.loadSecondaryTemp()

                    } else {
                        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
                        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">' + resp.message + '</span>');
                        $('#messageBoxModal').modal('show');
                    }
                }
            }});
    } else {
        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">' + error + '</span>');
        $('#messageBoxModal').modal('show');
        return false;
    }
};


setupSettings.loadSecondaryTemp = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/loadSecondaryTemp",
        dataType: "HTML",
        success: function (res) {
            $('.stepwizard-two').find('.panel-body').html(res)
            $('.stepwizard-two').find('.body').next().find('.next').attr('onclick', "setupSettings.loadUploadView(this)")
            formstepSettings.nextStep($('.stepwizard-one'));
        }});
};

setupSettings.generateLangCombi = function (ctrl) {
    $(ctrl).addClass('hide')
    var langData = {};
    $('#loadSecondaryTempForms').find('table').each(function () {
        var grade = $(this).attr('gradeid');
        if (!langData[grade])
            langData[grade] = {};
        var $self = $(this);
        $self.find('tbody>tr').each(function () {
            var lcnt = $(this).find('td:eq(0)').html();
            if (!langData[grade][lcnt])
                ;
            langData[grade][lcnt] = {};
            langData[grade][lcnt] = $(this).find('td:eq(2)').html();

        })
    });

    console.log(langData);
    myApp.showPleaseWait()
    $.ajax({
        type: "POST",
        url: commonSettings.onboardUrl + "/generateLangCombi",
        dataType: "JSON",
        data: langData,
        success: function (resp) {
            if (resp) {
                myApp.hidePleaseWait()
                if (resp.type == "success") {
                    $(ctrl).parents('.panel-body').next().find('.next').click();

                } else {
                    $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
                    $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">' + resp.message + '</span>');
                    $('#messageBoxModal').modal('show');
                }
            }
        }});
};


