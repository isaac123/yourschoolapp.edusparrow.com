var classroomcalSettings = {};

classroomcalSettings.loadSubjects = function (classroomid, classroomname) {

    classroomcalSettings.loadCalendar(classroomid);
    var subloading = '   <i class="fa fa-spin fa-spinner" ></i> Loading subjects ...  <br>';
    var stuloading = '   <i class="fa fa-spin fa-spinner" ></i> Loading students ...  <br>';
    var locloading = '   <i class="fa fa-spin fa-spinner" ></i> Loading locations ...  <br>';
    $('.classroomdet').html('Classroom: ' + classroomname);
    $('#subjist').html(subloading);
    $('#stuist').html(stuloading);
    $('#localist').html(locloading);
    $('#classroom_name_ontop').html('<h4 class="drg-event-title">You are now Scheduling for Classroom: ' + classroomname + '</h4>');
    $.ajax({
        type: "POST",
        url: commonSettings.calendarAjaxUrl + "/getSubjectsForClassroom",
        data: {classroomid: classroomid},
        dataType: "html",
        success: function (res) {
            if (res) {
                $('#subjist').html(res);
            }
        }
    });
    classroomcalSettings.loadStuStf(classroomid);
    classroomcalSettings.loadlocations(classroomid);
};

classroomcalSettings.iconfn = function (ctrl) {

    $(ctrl).toggleClass('fa-chevron-down');
    //   $(ctrl).addClass('fa-chevron-up').removeClass('fa-chevron-down')

}
classroomcalSettings.toggleactive = function (ctrl) {

    $('.classroombtn').addClass('label-default').removeClass('label-warning')
    $(ctrl).removeClass('label-default').addClass('label-warning')
}
classroomcalSettings.loadStuStf = function (classroomid) {
    $.ajax({
        type: "POST",
        url: commonSettings.calendarAjaxUrl + "/loadStafStu",
        data: {classroomid: classroomid},
        dataType: 'HTML',
        success: function (res) {
            $('#stuist').html(res);
        }
    });
};

classroomcalSettings.loadlocations = function (classroomid) {
    $.ajax({
        type: "POST",
        url: commonSettings.calendarAjaxUrl + "/locations",
        data: {classroomid: classroomid},
        dataType: 'HTML',
        success: function (res) {
            $('#localist').html(res);
        }
    });
};
classroomcalSettings.eventAddOnDragDrop = function (subjectid, classroomid, paticipants, start, end, duration)

{

    commonSettings.run_waitMe('#calendar')
    var eventObj = {};
    eventObj.classroomid = classroomid;
    eventObj.subjectid = subjectid;
    eventObj.paticipants = paticipants;
    eventObj.end = end;
    eventObj.start = start;
    eventObj.duration = duration;
    eventObj.phpdate = 0;
    eventObj.mandatory = 1;
    eventObj.status = 'Confirmed';
//    console.log(eventObj);
    $.ajax({
        type: "POST",
        url: commonSettings.calendarAjaxUrl + "/addEvent",
        data: eventObj,
        dataType: 'JSON',
        success: function (res) {
            $('#calendar').waitMe('hide');
            if (res.type == 'success')
                $('#calendar').fullCalendar('refetchEvents');
            else {
                var $clonedform = $('#formModal').clone().attr('id', 'addeveClone');
                $clonedform.find('.formBox-title').html(res.title);
                $clonedform.find('.formBox-body').html('<div class="alert alert-block alert-danger fade in">' + res.message + '</div>');
                $clonedform.modal('show')
                $clonedform.find('.modal-footer').html(' ');
                $clonedform.find('.modal-footer').append('<button class="btn btn-success" type="button" id="confirm_save">Schedule anyway</button>');
                $clonedform.find('.modal-footer').append('<button class="btn btn-warning" type="button" id="save_now">Save now</button>');
                $clonedform.find('.modal-footer').append(' <button data-dismiss="modal" class="btn btn-default" type="button"  id="confirm_close">Cancel</button>');
                $clonedform.find('#confirm_save').unbind('click').click(function () {
                    $clonedform.modal('hide');
                    eventObj.phpdate = 0;
                    eventObj.mandatory = 1;
                    eventObj.status = 'Confirmed';
                    eventObj.forceadd = 1;
                    classroomcalSettings.forceSaveEvent(eventObj, 'add');

                });
                $clonedform.find('#save_now').unbind('click').click(function () {
                    $clonedform.modal('hide');
                    eventObj.phpdate = 0;
                    eventObj.mandatory = 1;
                    eventObj.status = 'Needs Action';
                    classroomcalSettings.forceSaveEvent(eventObj, 'add');

                });
//                $('#messageBoxModal .modal-title').html(res.title);
//                $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-danger fade in">' + res.message + '</div>');
//                $('#messageBoxModal').modal();
                $clonedform.find('#confirm_close').unbind('click').click(function () {
                    $clonedform.modal('hide');

                    setTimeout(function () {
                        $clonedform.remove();
                    }, 2000);
                    $('#calendar').fullCalendar('refetchEvents');

                });
            }
        }

    });

};

classroomcalSettings.eventResize = function (eventObj) {
    commonSettings.run_waitMe('#calendar')
//    console.log(eventObj);
    $.ajax({
        type: "POST",
        url: commonSettings.calendarAjaxUrl + "/editEvent",
        data: eventObj,
        dataType: 'JSON',
        success: function (res) {
            $('#calendar').waitMe('hide');
            if (res.type == 'success')
                $('#calendar').fullCalendar('refetchEvents');
            else {
                var $clonedform = $('#formModal').clone().attr('id', 'editeveClone');
                $clonedform.find('.formBox-title').html(res.title);
                $clonedform.find('.formBox-body').html('<div class="alert alert-block alert-danger fade in">' + res.message + '</div>');
                $clonedform.modal('show')
                $clonedform.find('.modal-footer').html(' ');
                $clonedform.find('.modal-footer').append('<button class="btn btn-success" type="button" id="confirm_save">Schedule anyway</button>');
                $clonedform.find('.modal-footer').append('<button class="btn btn-warning" type="button" id="save_now">Save now</button>');
                $clonedform.find('.modal-footer').append(' <button data-dismiss="modal" class="btn btn-default" type="button"  id="confirm_close">Cancel</button>');
                $clonedform.find('#confirm_save').unbind('click').click(function () {
                    $clonedform.modal('hide');

                    setTimeout(function () {
                        $clonedform.remove();
                    }, 2000);
                    eventObj.phpdate = 0;
                    eventObj.mandatory = 1;
                    eventObj.status = 'Confirmed';
                    eventObj.forceadd = 1;
                    classroomcalSettings.forceSaveEvent(eventObj, 'edit');

                });
                $clonedform.find('#save_now').unbind('click').click(function () {
                    $clonedform.modal('hide');
                    eventObj.phpdate = 0;
                    eventObj.mandatory = 1;
                    eventObj.forceadd = 1;
                    eventObj.status = 'Needs Action';
                    classroomcalSettings.forceSaveEvent(eventObj, 'edit');

                });
//                $('#messageBoxModal .modal-title').html(res.title);
//                $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-danger fade in">' + res.message + '</div>');
//                $('#messageBoxModal').modal();
                $clonedform.find('#confirm_close').unbind('click').click(function () {
                    $clonedform.modal('hide');
                    setTimeout(function () {
                        $clonedform.remove();
                    }, 2000);
                    $('#calendar').fullCalendar('refetchEvents');

                });
            }
        }

    });

};
classroomcalSettings.forceSaveEvent = function (eventObj, action) {
//    console.log(eventObj);

    commonSettings.run_waitMe('#calendar')
    var urlref = action == 'edit' ? "/editEvent" : "/addEvent";
    $.ajax({
        type: "POST",
        url: commonSettings.calendarAjaxUrl + urlref,
        data: eventObj,
        dataType: 'JSON',
        success: function (res) {
            $('#calendar').waitMe('hide');
            if (res.type == 'success')
                $('#calendar').fullCalendar('refetchEvents');
            else {
                $('#messageBoxModal .modal-title').html(res.title);
                $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-danger fade in">' + res.message + '</div>');
                $('#messageBoxModal').modal();
                $('#msg_ok').unbind('click').click(function () {
                    $('#messageBoxModal').modal('hide');
                    $('#calendar').fullCalendar('refetchEvents');

                });
            }
        }

    });

};
classroomcalSettings.loadCalendar = function (classroomid) {

    $("#calendar").empty();
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#calendar').fullCalendar({
        defaultTimedEventDuration: '05:00:00',
        defaultAllDayEventDuration: {days: 1},
        forceEventDuration: false,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'

        },
        defaultView: 'agendaWeek', //globalSettings.activeview.value,,
        dropAccept: '.external-event',
        firstDay: 1,
        editable: true,
        allDayDefault: false,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        columnFormat: {
            month: 'ddd',
            week: 'ddd d',
            day: 'dddd'
        },
        titleFormat: {
            month: 'MMMM yyyy',
            week: "dS[ yyyy]{ '&#8212;' dS MMMM yyyy}",
            day: 'MMMM d , yyyy'
        },
        allDayText: ' ALL DAY',
        eventResize: function (event, delta, revertFunc) {

//            console.log(event)
//            console.log(revertFunc)

            var copiedEventObject = $.extend({}, event);

            var sd = new Date(copiedEventObject.start);
            var sdatestring = ("0" + sd.getDate()).slice(-2) + "-" + ("0" + (sd.getMonth() + 1)).slice(-2) + "-" +
                    sd.getFullYear() + " " + ("0" + sd.getHours()).slice(-2) + ":" + ("0" + sd.getMinutes()).slice(-2);
            var ed = new Date(copiedEventObject.end);
            var edatestring = ("0" + ed.getDate()).slice(-2) + "-" + ("0" + (ed.getMonth() + 1)).slice(-2) + "-" +
                    ed.getFullYear() + " " + ("0" + ed.getHours()).slice(-2) + ":" + ("0" + ed.getMinutes()).slice(-2);
            var eventObj = {};
            eventObj.eventid = copiedEventObject.id;
            eventObj.subjectid = copiedEventObject.subject;
            eventObj.classroomid = copiedEventObject.classroom;
            eventObj.start = sdatestring;
            eventObj.end = edatestring;
            eventObj.status = copiedEventObject.status;
            eventObj.mandatory = copiedEventObject.mandatory;
            classroomcalSettings.eventResize(eventObj);

        },
        drop: function (date, allDay) { // this function is called when something is dropped
            var originalEventObject = $(this).data('eventObject');
            var classroomid = $(this).attr('classroomid');
            var paticipants = $(this).attr('paticipants');
            var subjectid = $(this).attr('subjectid');
            var duration = 60;


            var sd = new Date(date);
            var sdatestring = ("0" + sd.getDate()).slice(-2) + "-" + ("0" + (sd.getMonth() + 1)).slice(-2) + "-" +
                    sd.getFullYear() + " " + ("0" + sd.getHours()).slice(-2) + ":" + ("0" + sd.getMinutes()).slice(-2);

            var copiedEventObject = $.extend({}, originalEventObject);
            copiedEventObject.start = sdatestring;
            copiedEventObject.allDay = allDay;

            var mystart = sdatestring;//new Date(date).getTime() / 1000;
            var myend = '';//new Date(copiedEventObject.end).getTime() / 1000;

            classroomcalSettings.eventAddOnDragDrop(subjectid, classroomid, paticipants, mystart, myend, duration);

        },
        events: {
            url: commonSettings.calendarAjaxUrl + "/calendarFeedCassandra",
            type: 'POST',
            data: {
                classroomid: classroomid
            },
            error: function () {

                console.log('there was an error while fetching events!');

            }
        },
        eventClick: function (calEvent, jsEvent, view) {
//            console.log(calEvent)
            var copiedEventObject = $.extend({}, calEvent);


            var sd = new Date(calEvent.start);
            var sdatestring = ("0" + sd.getDate()).slice(-2) + "-" + ("0" + (sd.getMonth() + 1)).slice(-2) + "-" +
                    sd.getFullYear() + " " + ("0" + sd.getHours()).slice(-2) + ":" + ("0" + sd.getMinutes()).slice(-2);
            var ed = new Date(calEvent.end);
            var edatestring = ("0" + ed.getDate()).slice(-2) + "-" + ("0" + (ed.getMonth() + 1)).slice(-2) + "-" +
                    ed.getFullYear() + " " + ("0" + ed.getHours()).slice(-2) + ":" + ("0" + ed.getMinutes()).slice(-2);
            var eventObj = {};
            eventObj.id = calEvent.id;
            eventObj.subjectid = calEvent.subject;
            eventObj.classroomid = calEvent.classroom;
            eventObj.start = sdatestring;
            eventObj.end = edatestring;
            eventObj.status = calEvent.status;
            eventObj.mandatory = calEvent.mandatory;
            eventObj.title = calEvent.title;
            eventObj.Attendees = calEvent.Attendees;
            eventObj.phpdate = 0;
            eventObj.location = calEvent.location;
//              console.log(eventObj)
            classroomcalSettings.eventEditModal(eventObj)
        },
        eventDrop: function (event, delta, revertFunc) {


            var copiedEventObject = $.extend({}, event);

            var sd = new Date(copiedEventObject.start);
            var sdatestring = ("0" + sd.getDate()).slice(-2) + "-" + ("0" + (sd.getMonth() + 1)).slice(-2) + "-" +
                    sd.getFullYear() + " " + ("0" + sd.getHours()).slice(-2) + ":" + ("0" + sd.getMinutes()).slice(-2);
            var ed = new Date(copiedEventObject.end);
            var edatestring = ("0" + ed.getDate()).slice(-2) + "-" + ("0" + (ed.getMonth() + 1)).slice(-2) + "-" +
                    ed.getFullYear() + " " + ("0" + ed.getHours()).slice(-2) + ":" + ("0" + ed.getMinutes()).slice(-2);
            var eventObj = {};
            eventObj.eventid = copiedEventObject.id;
            eventObj.subjectid = copiedEventObject.subject;
            eventObj.classroomid = copiedEventObject.classroom;
            eventObj.start = sdatestring;
            eventObj.end = edatestring;
            eventObj.status = copiedEventObject.status;
            eventObj.mandatory = copiedEventObject.mandatory;
            eventObj.phpdate = 0;
            classroomcalSettings.eventResize(eventObj);

        },
        eventRender: function (event, element) {
//            console.log(event)
            var content = "Course : " + event.title + '<br>'
                    + " Location : " + (event.location ? event.location_name : 'Not mentioned');
            $(element).popover({
                html: true,
                title: "Event details",
                placement: 'right',
                container: '#calendar',
                trigger: 'hover',
                content: content
            });

        }, eventAfterRender: function (event, element, view) {
            $(element).attr("id", "event_id_" + event._id);
        }, loading: function (isLoading, view) {
            if (isLoading) {// isLoading gives boolean value
                commonSettings.run_waitMe('#calendar')
            } else {
                $('#calendar').waitMe('hide');

                $('.fc-event').each(function () {
//                    console.log('accept loc')
                    $(this).droppable({
                        accept: ".location-event",
                        activeClass: "fc-state-active",
                        hoverClass: "fc-state-hover",
                        drop: function (event, ui) {
                            $(this).addClass("fc-state-highlight");
                            var eventid = $(this).attr('id').split('_');
                            var copiedobj = $.extend({}, $("#calendar").fullCalendar('clientEvents', eventid[2]));
                            var locaid = ui.draggable.attr('id').split('_')
                            var copiedEventObject = copiedobj[0];
                            var sd = new Date(copiedEventObject.start);
                            var sdatestring = ("0" + sd.getDate()).slice(-2) + "-" + ("0" + (sd.getMonth() + 1)).slice(-2) + "-" +
                                    sd.getFullYear() + " " + ("0" + sd.getHours()).slice(-2) + ":" + ("0" + sd.getMinutes()).slice(-2);
                            var ed = new Date(copiedEventObject.end);
                            var edatestring = ("0" + ed.getDate()).slice(-2) + "-" + ("0" + (ed.getMonth() + 1)).slice(-2) + "-" +
                                    ed.getFullYear() + " " + ("0" + ed.getHours()).slice(-2) + ":" + ("0" + ed.getMinutes()).slice(-2);
                            var eventObj = {};
                            eventObj.eventid = copiedEventObject.id;
                            eventObj.subjectid = copiedEventObject.subject;
                            eventObj.classroomid = copiedEventObject.classroom;
                            eventObj.start = sdatestring;
                            eventObj.end = edatestring;
                            eventObj.status = copiedEventObject.status;
                            eventObj.mandatory = copiedEventObject.mandatory;
                            eventObj.phpdate = 0;
                            eventObj.locationdet = locaid[1]
//                            console.log(eventObj)
                            classroomcalSettings.eventResize(eventObj);
                        }
                    });
                })
            }
        }
    });

    $('.fc-agenda-slots').parent().parent().niceScroll();
};

classroomcalSettings.eventEditModal = function (copiedEventObject) {

    commonSettings.run_waitMe('#calendar')
    $.ajax({
        type: "POST",
        url: commonSettings.calendarAjaxUrl + "/editEventPopup",
        data: copiedEventObject,
        dataType: "html",
        success: function (res) {
            $('#calendar').waitMe('hide');
            var $clonedform = $('#formModal').clone().attr('id', 'editevepopupClone');
            $clonedform.find('.formBox-title').html('Edit ' + copiedEventObject.title + ' details');
            $clonedform.find('.formBox-body').html(res);
            $clonedform.modal('show')
            $clonedform.find('.modal-footer').append(' <button data-dismiss="modal" class="btn btn-danger" type="button"  id="confirm_delete">Delete</button>');
            var eveid = copiedEventObject.id.replace(' ', '_').replace('.', '_');
            $clonedform.find('#confirm_delete').unbind('click').click(function () {
                $clonedform.modal('hide');
                var eventObj = {};
                $("#editEventForm_" + eveid).find('input,select').each(function () {
                    eventObj[$(this).attr('name')] = $.trim($(this).val());
                });
                eventObj.title = copiedEventObject.title;
                classroomcalSettings.deleteEvent(eventObj);

            });
            $clonedform.find('#confirm_save').unbind('click').click(function () {
                $clonedform.modal('hide');
                var eventObj = {};
                $("#editEventForm_" + eveid).find('input,select').each(function () {
                    eventObj[$(this).attr('name')] = $.trim($(this).val());
                });
                eventObj.phpdate = 1;
                //console.log(eventObj)

                setTimeout(function () {
                    $clonedform.remove();
                }, 2000);
                classroomcalSettings.eventResize(eventObj);

            });
            $clonedform.find('#confirm_close').unbind('click').click(function () {
                $clonedform.modal('hide');
                setTimeout(function () {
                    $clonedform.remove();
                }, 2000);

            });
        }
    });
}
classroomcalSettings.addLocations = function (classroomid) {
    $.ajax({
        type: "POST",
        url: commonSettings.calendarAjaxUrl + "/addLocations",
        data: {classroomid: classroomid},
        dataType: 'html',
        success: function (res) {
//            console.log('f');
            $('#formModal .formBox-title').html('Add New Locations');
            $('#formModal .formBox-body').html(res);
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#confirm_save').removeClass('hide');
            $('#confirm_save').html('Save');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                classroomcalSettings.saveClassroomLocation(classroomid);
            });
        }
    });
};
classroomcalSettings.saveClassroomLocation = function (classroomid) {
    var params = {};
    var err = 0;
    var error = '';
    $("#addNewLocationForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n";

            } else {
                params[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }

    });

    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.calendarAjaxUrl + "/saveClassroomLocation",
            data: params,
            dataType: "JSON",
            success: function (res) {
                if (res) {
//                    console.log(res)
//                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            $('#formModal').modal();
                        }
                        else if (res['type'] == 'success') {
                            classroomcalSettings.loadlocations(classroomid);
                        }
                    });

                }

            }
        });
    }
    else
    {
        $('#confirm_close').click();
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#formModal').modal('show')
        });
    }
};
classroomcalSettings.deleteEvent = function (eventObj) {
    var $clonedform = $('#ConfirmModal').clone().attr('id', 'deleveClone');
    $clonedform.find('.cnfBox-title').html('Confirm event delete');
    $clonedform.find('.cnfBox-body').html(' Do you confirm to detele ' + eventObj.title + '  event? ');
    $clonedform.modal({
        backdrop: 'static',
        keyboard: false
    });
    $clonedform.find('#confirm_yes').unbind('click').click(function () {
        $clonedform.modal('hide');
        commonSettings.run_waitMe('#calendar')
//        console.log(eventObj);
        $.ajax({
            type: "POST",
            url: commonSettings.calendarAjaxUrl + "/deleteEvent",
            data: eventObj,
            dataType: "JSON",
            success: function (res) {
                $('#calendar').waitMe('hide');
                if (res.type == 'success')
                    $('#calendar').fullCalendar('refetchEvents');
                else {
                    $('#messageBoxModal .modal-title').html(res.title);
                    $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-danger fade in">' + res.message + '</div>');
                    $('#messageBoxModal').modal();
                    $('#msg_ok').unbind('click').click(function () {
                        $('#messageBoxModal').modal('hide');
                        $('#calendar').fullCalendar('refetchEvents');

                    });
                }
            }
        });

    });
}
