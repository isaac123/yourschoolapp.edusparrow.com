var Script = function () {
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultView: 'agendaWeek', //globalSettings.activeview.value,,
        firstDay: 1,
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        columnFormat: {
            month: 'ddd',
            week: 'ddd d',
            day: 'dddd'
        },
        titleFormat: {
            month: 'MMMM yyyy',
            week: "dS[ yyyy]{ '&#8212;' dS MMMM yyyy}",
            day: 'MMMM d , yyyy'
        },
         allDayText :' ALL DAY',
        events: [
        ],
    });
$('.fc-agenda-slots').parent().parent().niceScroll();

}();