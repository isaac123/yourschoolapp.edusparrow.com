var markSettings = {};
var max = 61;
markSettings.stuCombiData = {};
markSettings.loadClassTestMark = function (ctrl) {

//    $('.combispan').addClass('inactive')
//    $(ctrl).toggleClass('inactive')

    $('.combispan').addClass('label-default').removeClass('label-warning')
        if ($(ctrl).hasClass('combispan'))
    $(ctrl).removeClass('label-default').addClass('label-warning')
    markSettings.subject_masterid = $(ctrl).attr('id').split('_')[1];

    $.ajax({
        type: "POST",
        url: commonSettings.markAjaxurl + "/loadClassTest",
        data: {
            subject_masterid: markSettings.subject_masterid
        },
        success: function (res) {
            if (res) {
                $('#loadClassTest').html(res);
                $('#loadClassTest').removeClass('hide')
                $('#loadClassTestStudents').addClass('hide')
            }
        }
    });
};

markSettings.updateClassTestTotalMark = function (mark)
{
    $('.done_totmark').addClass('hide')
    $('.loading_totmark').removeClass('hide')
    var total_mark = parseInt(mark);
    var number_pattren = /^[0-9.]*$/;
    var err = 0, errormsg = '';
    if (!number_pattren.test(mark))
    {
        err = 1;
        errormsg = 'Please enter valid numeric only!';
    }
    else if (total_mark < 0) {
        err = 1;
        errormsg = 'Total Mark Cannot Be Negative!';
    }
    $('#clsTestMark').find('input.stuMark').each(function () {
        var markstu = $.trim($(this).val());
//        console.log(markstu)
        if (markstu !== '' && parseFloat(markstu).toFixed(2) > total_mark) {
            err = 1;
            errormsg = 'Student mark exceeds the total mark!';
            return;
        }
    });

    if (err == 1)
    {
        $('#Total_Mark').val($('#Total_Mark').attr('placeholder'))
        $('#messageBoxModal .modal-title').html('Error');
        $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-danger fade in">' + errormsg + '</div>');
        $('#messageBoxModal').modal('show');
        $('.loading_totmark').addClass('hide')
    } else {
        $.ajax({
            type: "POST",
            url: commonSettings.markAjaxurl + "/updateClassTestTotMark",
            data: {
                test_id: markSettings.test_id,
                total_mark: total_mark
            },
            success: function (res) {
                if (res) {

                    $('.done_totmark').removeClass('hide')
                    $('.loading_totmark').addClass('hide')
                }

            }
        });
    }
};
markSettings.updateClassTestMark = function (mark, id)
{
    total_mark = parseInt($('#Total_Mark').val());
    var number_pattren = /^[0-9.]*$/;

    if (!number_pattren.test(mark))
    {
        $('#div_' + id).html("Please enter valid numeric only.");
        $('#div_' + id).css("color", "red")
    }
    else if ($('#Total_Mark').val().length == 0) {
        $('#div_' + id).html("Please Enter Total Mark First!");
        $('#div_' + id).css("color", "red")
    }
    else if (total_mark < 0) {
        $('#div_' + id).html("Total Mark Cannot Be Negative!");
        $('#div_' + id).css("color", "red")
    }
    else if (mark < 0) {
        $('#div_' + id).html("Entered Mark Cannot Be Negative!");
        $('#div_' + id).css("color", "red")
    }
    else if (mark <= total_mark)
    {
        $('#Total_Mark').attr("disabled", "disabled");
        $.ajax({
            type: "POST",
            url: commonSettings.markAjaxurl + "/updateClassTestMark",
            data: {
                test_id: markSettings.test_id,
                student_id: id,
                mark: mark,
                total_mark: total_mark
            },
            success: function (res) {
                if (res) {
                    $('#display').html(res);
                }

            }
        });
        $('#div_' + id).html("Mark updated");
        $('#div_' + id).css("color", "green")
    }
    else
    {
        $('#div_' + id).html("Exceeds Total Mark. Re-Enter correct mark");
        $('#div_' + id).css("color", "red")
    }
};


markSettings.loadSection = function (sval) {
    console.log(sval);
    markSettings.classID = sval;
    markSettings.className = $("#divID option:selected").text();
    $.ajax({
        type: "POST",
        url: commonSettings.markAjaxurl + "/loadSectionsByClass",
        data: {
            classId: sval
        },
        success: function (res) {
            if (res) {
                $('#loadSection').html(res);
                $('#loadSection').removeClass('hide');
                $('#loadSubject').addClass('hide');
                $('#loadClassTest').addClass('hide');
                $('#loadClassTestStudents').addClass('hide')
            }

        }
    });

};


markSettings.loadSubjectsBySection = function (val) {
    markSettings.subDivVal = val;
    markSettings.subDivName = $("#subDivVal option:selected").text();
    $.ajax({
        type: "POST",
        url: commonSettings.markAjaxurl + "/loadSubjectsBySection",
        data: {
            class_id: markSettings.classID,
            subDivVal: val
        },
        success: function (res) {
            if (res) {
                $('#loadSubject').html(res);
                $('#loadSubject').removeClass('hide');
                $('#loadClassTest').addClass('hide');
                $('#loadClassTestStudents').addClass('hide')
            }
        }
    });
};

markSettings.loadClassTest = function (val) {
    if (val != -1)
    {
        markSettings.subject_id = val;
        markSettings.subject_name = $("#subid option:selected").text();

        $return = $('#return_uri').val();
        console.log($return);
        $.ajax({
            type: "POST",
            url: $return,
            data: {
                class_id: markSettings.classID,
                subDivVal: markSettings.subDivVal,
                subject_id: val
            },
            success: function (res) {
                if (res) {
                    $('#loadClassTest').html(res);
                    $('#loadClassTest').removeClass('hide');
                    $('#loadClassTestStudents').addClass('hide')
                }

            }
        });
    }
};

markSettings.loadMainExam = function (val) {
    if (val != -1)
    {
        markSettings.subject_id = val;
        markSettings.subject_name = $("#subid option:selected").text();
        $.ajax({
            type: "POST",
            url: commonSettings.markAjaxurl + "/loadClassTest",
            data: {
                class_id: markSettings.classID,
                subDivVal: markSettings.subDivVal,
                subject_id: val
            },
            success: function (res) {
                if (res) {
                    //alert(res);
                    //$('#mainDiv').append(res);
                    $('#loadClassTest').html(res);
                    $('#loadClassTest').removeClass('hide');
                    $('#loadClassTestStudents').addClass('hide')
                }

            }
        });
    }
};

markSettings.loadMainExamStudents = function (val) {
    if (val != -1)
    {
        markSettings.test_id = val;
        markSettings.test_name = $("#testid option:selected").text();
        $return = $('#loadMainExamStudents_return_uri').val();

        $.ajax({
            type: "POST",
            url: $return,
            data: {
                class_id: markSettings.classID,
                class_name: markSettings.className,
                subDivVal: markSettings.subDivVal,
                subdiv_name: markSettings.subDivName,
                subject_id: markSettings.subject_id,
                test_id: markSettings.test_id
            },
            success: function (res) {
                if (res) {
                    //alert(res);
                    //$('#mainDiv').append(res);
                    $('#loadClassTestStudents').html(res);
                    $('#loadClassTestStudents').removeClass('hide');
                }

            }
        });
    }
};

markSettings.updateMainExamMark = function (mark, id)
{
    total_mark = parseInt($('#Total_Mark').val());
    mark = parseInt(mark);
    if (mark <= total_mark)
    {
        $.ajax({
            type: "POST",
            url: commonSettings.markAjaxurl + "/updateMainExamMark",
            data: {
                test_id: markSettings.test_id,
                student_id: id,
                mark: mark
            },
            success: function (res) {
                if (res) {
                    $('#display').html(res);
                }

            }
        });
        $('#div_' + id).html("Mark update successfull");
        $('#div_' + id).css("color", "green")
    }
    else
    {
        $('#div_' + id).html("Exceeds Total Mark. Re-Enter correct mark");
        $('#div_' + id).css("color", "red")
    }
};

markSettings.loadExams = function (ctrl) {/* notused */
    var StuCombiData = {};
//    $('.combispan').addClass('inactive')
//    $(ctrl).toggleClass('inactive')
    $('.combispan').addClass('label-default').removeClass('label-warning')
    $(ctrl).removeClass('label-default').addClass('label-warning')
    StuCombiData.masterid = $(ctrl).attr('id').split('_')[1];
    StuCombiData.type = $(ctrl).attr('teacherType');

    markSettings.stuCombiData = StuCombiData;
    $.ajax({
        type: "POST",
        url: commonSettings.markAjaxurl + "/getExamList",
        data: markSettings.stuCombiData,
    }).done(function (resHtml) {
        $("#stuExamList").removeClass('hide')
        $("#stuExamList").html(resHtml);
    });
};

markSettings.getStudentList = function (value) {
    console.log('stulist')
    markSettings.stuCombiData['mainExId'] = value ? value : markSettings.stuCombiData['mainExId'];
    markSettings.loadStudentData();
};
markSettings.loadStudentData = function () {
    console.log(markSettings.stuCombiData)
    $.ajax({
        type: "POST",
        url: commonSettings.markAjaxurl + "/getStudentList",
        data: markSettings.stuCombiData,
    }).done(function (resHtml) {
        $("#mymarkDiv").html(resHtml);
        $('#mymarkDiv').toggle(attSettings.effect, attSettings.options, attSettings.duration);
        $('#mymarkDiv1').toggle(attSettings.effect, attSettings.options, attSettings.duration);
//        $("#stuMarkList").removeClass('hide')
//        $("#stuMarkList").html(resHtml);
        markSettings.stuCombiData['MainExTotalMark'] = $('#mainexTotVal').val();


        markSettings.updateHeight(max, 'mainmrkStuTable', 'markStuTable')
    });
};

markSettings.updateMainExamMarkTotal = function (ctrl) {

    console.log($(ctrl).val())
    markSettings.stuCombiData['MainExTotalMark'] = '';
    var mark = $(ctrl).val();
    console.log(parseFloat(mark).toFixed(2))
    var number_pattren = /^[0-9.]*$/;

    if (!number_pattren.test(mark))
    {
        $('.enable_' + markSettings.stuCombiData['mainExId']).prop('disabled', true);
        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Please enter valid numeric only.</span>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#messageBoxModal').modal('hide');
            markSettings.loadStudentData();
        });
        return false;
    }
    else if (mark == '' || parseFloat(mark).toFixed(2) < 0) {

        $('.enable_' + markSettings.stuCombiData['mainExId']).prop('disabled', true);
        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Invalid Total Marks.</span>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#messageBoxModal').modal('hide');
            markSettings.loadStudentData();
        });
        return false;
    } else {
        $('.loading_' + markSettings.stuCombiData['mainExId']).removeClass('hide')
        markSettings.stuCombiData['MainExTotalMark'] = parseFloat(mark).toFixed(2);
        $('.enable_' + markSettings.stuCombiData['mainExId']).prop('disabled', false);
        console.log(markSettings.stuCombiData)
        markSettings.updateAlStuMainExamMark()
    }

};

markSettings.updateAlStuMainExamMark = function () {
    var err = 0;

    $('#markStuTable').find('input.stuMark').each(function () {
        var markstu = $.trim($(this).val());
//        console.log(parseFloat(90.10) > parseFloat(100.00))
        if (markstu != '' && parseFloat(markstu) > parseFloat(markSettings.stuCombiData['MainExTotalMark'])) {

            $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
            $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Student mark exceeds the total mark!</span>');
            $('#messageBoxModal').modal('show');
            $('#msg_ok').unbind('click').click(function () {
                $('#messageBoxModal').modal('hide');
                markSettings.loadStudentData();
            });
            return false;
            err = 1;
        }
    });
    console.log(markSettings.stuCombiData)
    if (err == 0) {

        $.ajax({
            type: "POST",
            url: commonSettings.markAjaxurl + "/updateAlStudentMark",
            data: markSettings.stuCombiData,
            dataType: 'JSON',
        }).done(function (res) {

            if (res) {
                if (res.type == 'success') {
                    $('.done_' + markSettings.stuCombiData['mainExId']).removeClass('hide')
                    $('.loading_' + markSettings.stuCombiData['mainExId']).addClass('hide')
                } else {
                    var lblcls = res.type == 'error' ? 'times' : (res.type == 'success' ? 'check' : 'info');
                    var lblstcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                    $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-' + lblcls + '"></i> ' + res.type.toUpperCase());
                    $('#messageBoxModal .msgBox-body').html('<span class="text text-' + lblstcls + ' fade in">' + res.message + '</span>');
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        $('#messageBoxModal').modal('hide');
                        if (res.type == 'success') {
                            $('.done_' + markSettings.stuCombiData['mainExId']).removeClass('hide')
                            $('.loading_' + markSettings.stuCombiData['mainExId']).addClass('hide')
                        } else {
                            markSettings.loadStudentData();
                        }

                    });
                }
            }
        });

    }


};
markSettings.updateStuMainExamMark = function (ctrl) {

//    console.log($(ctrl).val())
    var mark = $(ctrl).val();
    var stuId = $(ctrl).attr('stu_id');
    console.log(parseFloat(mark).toFixed(2) + ' > ' + markSettings.stuCombiData['MainExTotalMark']);

    console.log(parseFloat(mark) > markSettings.stuCombiData['MainExTotalMark']);

    $('.loading_' + markSettings.stuCombiData['mainExId'] + '_' + stuId).removeClass('hide');

    var number_pattren = /^[0-9.]*$/;

    if (!number_pattren.test(mark))
    {
        $('.enable_' + markSettings.stuCombiData['mainExId']).prop('disabled', true);

        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Please enter valid numeric only!</span>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#messageBoxModal').modal('hide');
            markSettings.loadStudentData();
        });
        return false;
    }
    else if (mark == '' || parseFloat(mark).toFixed(2) < 0)
    {

        $('.enable_' + markSettings.stuCombiData['mainExId']).prop('disabled', true);
        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Invalid Total Marks</span>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#messageBoxModal').modal('hide');
            markSettings.loadStudentData();
        });
        return false;
    }
    else if (parseFloat(mark) > markSettings.stuCombiData['MainExTotalMark']) {
//    console.log(parseInt(mark) > markSettings.stuCombiData['MainExTotalMark']);
        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Student mark exceeds the total mark</span>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#messageBoxModal').modal('hide');
            markSettings.loadStudentData();
        });
        return false;
    } else {
        markSettings.stuCombiData['StuMainExMark'] = mark;
        markSettings.stuCombiData['StuId'] = stuId;
        console.log(markSettings.stuCombiData)
        $.ajax({
            type: "POST",
            url: commonSettings.markAjaxurl + "/updateStudentMark",
            data: markSettings.stuCombiData,
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    if (res.type == 'success') {
                        $('.done_' + markSettings.stuCombiData['mainExId'] + '_' + stuId).removeClass('hide')
                        $('.loading_' + markSettings.stuCombiData['mainExId'] + '_' + stuId).addClass('hide')
                    } else {
                        var lblcls = res.type == 'error' ? 'times' : (res.type == 'success' ? 'check' : 'info');
                        var lblstcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-' + lblcls + '"></i> ' + res.type.toUpperCase());
                        $('#messageBoxModal .msgBox-body').html('<span class="text text-' + lblstcls + ' fade in">' + res.message + '</span>');
                        $('#messageBoxModal').modal('show');
                        $('#msg_ok').unbind('click').click(function () {
                            $('#messageBoxModal').modal('hide');
                            if (res.type == 'success') {
                                $('.done_' + markSettings.stuCombiData['mainExId'] + '_' + stuId).removeClass('hide')
                                $('.loading_' + markSettings.stuCombiData['mainExId'] + '_' + stuId).addClass('hide')
                            } else {
                                markSettings.loadStudentData();
                            }

                        });
                    }
                }
            }
        });
    }


};
markSettings.loadStudentDiv = function (subsujectid) {
    $("#stuExamList").addClass('hide')
//    markSettings.stuCombiData.masterid = masterid;
//    console.log(masterid)
    markSettings.stuCombiData.subsujectid = subsujectid;
//    markSettings.stuCombiData.examid = examid;
    markSettings.getStudentList(markSettings.stuCombiData.examid)

//    $('.updatemark').toggle(attSettings.effect, attSettings.options, attSettings.duration);
//    $('.viewmark').toggle(attSettings.effect, attSettings.options, attSettings.duration);
//    
};


markSettings.loadClassTestStudents = function (val) {
    if (val != -1)
    {
        markSettings.test_id = val;
        markSettings.subject_masterid = examSettings.subject_masterid;
        //markSettings.test_name = $("#testid option:selected").text();        
        $.ajax({
            type: "POST",
            url: commonSettings.markAjaxurl + "/loadClassTestStudents",
            data: {
                subject_masterid: markSettings.subject_masterid,
                test_id: markSettings.test_id
            },
            success: function (res) {
                if (res) {
                    //alert(res);
                    //$('#mainDiv').append(res);
//                    $('#loadClassTestStudents').html(res);
//                    $('#loadClassTestStudents').removeClass('hide');
                    $('#viewclasstest').html(res);
                    $('#viewclasstest').removeClass('hide');
                }

            }
        });
    }
    else
    {
        $('#loadClassTestStudents').addClass('hide');
    }
};

markSettings.loadStudentMarkList = function (examid) {
    markSettings.stuCombiData['current_view'] = 2;
    markSettings.stuCombiData['mainExId'] = examid ? examid : markSettings.stuCombiData['mainExId'];
    console.log(markSettings.stuCombiData)
    $.ajax({
        type: "POST",
        url: commonSettings.markAjaxurl + "/getStudentMarkList",
        data: markSettings.stuCombiData,
        dataType: 'html'
    }).done(function (resHtml) {
//        console.log(resHtml)
//        $("#myDiv").html(resHtml);
//        $('#myDiv').toggle(attSettings.effect, attSettings.options, attSettings.duration);
//        $('#myDiv1').toggle(attSettings.effect, attSettings.options, attSettings.duration);
        $("#stuMarkList").html(resHtml);
        $("#stuExamList").addClass('hide')
        $("#stuMarkList").removeClass('hide')
        $('#mymarkDiv').toggle(attSettings.effect, attSettings.options, attSettings.duration);
        $('#mymarkDiv1').toggle(attSettings.effect, attSettings.options, attSettings.duration);

        markSettings.updateHeight(max, 'mainmrkStuTable', 'getExamMArkList')
//        markSettings.stuCombiData['MainExTotalMark'] = $('#mainexTotVal').val();
    });
};

markSettings.updateHeight = function (max, t1, t2) {
    var nowmax = max;
//    console.log('max:'+nowmax)
    nowmax = Math.max($('#' + t1 + ' thead').height(), nowmax);
    nowmax = Math.max($('#' + t2 + ' thead').height(), nowmax);
    var subjHeight = $('#' + t2 + ' thead>tr').length;
    var equated = parseInt(nowmax / subjHeight);
    var i = 0;
    if (subjHeight > 1) {
        $('#' + t2 + ' thead').find('tr').each(function () {
            if (i == subjHeight) {
                $(this).height(equated + 20)
            } else {
                $(this).height(equated)
            }
        })
    } else {
        $('#' + t2 + ' thead').find("> tr:first").height(nowmax)
    }
    $('#' + t1 + ' thead').find("> tr:first").height(nowmax)

    var maxtbody = max;
    $('#' + t1 + ' tbody').find('tr').each(function () {
        maxtbody = Math.max($(this).height(), maxtbody);
    });

    $('#' + t2 + ' tbody').find('tr').each(function () {
        maxtbody = Math.max($(this).height(), maxtbody);
    });
    $('#' + t1 + ' tbody').find('tr').height(maxtbody)
    $('#' + t2 + ' tbody').find('tr').height(maxtbody)
}

markSettings.loadBackStudentMarkList = function (examid) {
    markSettings.stuCombiData['current_view'] = 2;
    markSettings.stuCombiData['mainExId'] = examid ? examid : markSettings.stuCombiData['mainExId'];
    console.log(markSettings.stuCombiData)
    $.ajax({
        type: "POST",
        url: commonSettings.markAjaxurl + "/getStudentMarkList",
        data: markSettings.stuCombiData,
        dataType: 'html'
    }).done(function (resHtml) {


        $("#stuMarkList").html(resHtml);
        $('#mymarkDiv').toggle(attSettings.effect, attSettings.options, attSettings.duration);
        $('#mymarkDiv1').toggle(attSettings.effect, attSettings.options, attSettings.duration);
        markSettings.updateHeight(max, 'mainmrkStuTable', 'getExamMArkList')
    });
};
markSettings.UpdateCurrentView = function (ctrl) {
    console.log(markSettings.stuCombiData)
//    var currentclass = $(ctrl).find('.classname').text();
    var currentclass = $(ctrl).attr('classname');
    if (markSettings.stuCombiData.current_view == 2 && (markSettings.stuCombiData.className == currentclass)) {
        var StuCombiData = {};
//        $('.combispan').addClass('inactive')
//        $(ctrl).toggleClass('inactive')
        $('.combispan').addClass('label-default').removeClass('label-warning')
        if ($(ctrl).hasClass('combispan'))
            $(ctrl).removeClass('label-default').addClass('label-warning')
        markSettings.stuCombiData.masterid = $(ctrl).attr('id').split('_')[1];
        examSettings.stuCombiData.masterid = $(ctrl).attr('id').split('_')[1];
        markSettings.loadStudentMarkList()
    } else {
        examSettings.loadExams(ctrl)

    }
};

markSettings.updateFormula = function (examid, subjectid) {

    var masterid = markSettings.stuCombiData.masterid ?
            markSettings.stuCombiData.masterid : examSettings.stuCombiData.masterid;
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/loadFormulaForm",
        dataType: "html",
        data:
                {
                    examid: examid,
                    subjectid: subjectid,
                    masterid: masterid
                },
        success: function (res) {
            if (res) {
                $('#formModal .modal-title').html('Formula');
                $('#formModal .modal-body').html(res);
//                $('#classID').val(examSettings.classID)
                //$('#modalForm').click();
                $('#formModal').modal('show');
                $('#formModal').find('.modal-footer').html(' ');
                $('#formModal').find('.modal-footer').append('<button class="btn btn-success" type="button" id="confirm_save">Update formula</button>');
                $('#formModal').find('.modal-footer').append('<button class="btn btn-warning" type="button" id="reset_now">Reset formula</button>');
                $('#formModal').find('.modal-footer').append(' <button data-dismiss="modal" class="btn btn-default" type="button"  id="confirm_close">Cancel</button>');


                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    markSettings.saveFormula();
                });


                $('#reset_now').unbind('click').click(function () {
                    $('#confirm_close').click();
                    $('.cnfBox-title').html('Confirm');
                    $('.cnfBox-body').html('<div class="alert alert-block alert-warning fade in">Do you confirm to reset contribution?</div>');
                    $('#ConfirmModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#confirm_yes').unbind('click').click(function () {
                        $('#ConfirmModal').modal('hide');
                        markSettings.resetFormula();
                    });
                });
            }
        }
    });
};

markSettings.resetFormula = function () {

    var formulaData = {};
    $('#addFormulaForm').find('input,select,textarea').each(function () {
        formulaData[$(this).attr('name')] = $.trim($(this).val());
    });
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/resetFormula",
        data: formulaData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                var lblcls = res.type == 'error' ? 'danger' : 'success';
                $('#messageBoxModal .modal-title').html(res.type);
                $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == "success") {
                        $.ajax({
                            type: "POST",
                            url: commonSettings.markAjaxurl + "/getStudentList",
                            data: markSettings.stuCombiData,
                        }).done(function (resHtml) {
                            $("#mymarkDiv").html(resHtml);
                            markSettings.stuCombiData['MainExTotalMark'] = $('#mainexTotVal').val();

                            markSettings.updateHeight(max, 'mainmrkStuTable', 'markStuTable')
                        });
                    } else {
                        $('#formModal').modal('show')
                    }

                });
            }
        }
    });
};
markSettings.saveFormula = function () {
    var formulaData = {};
    var err = 0;
    var errmsg = '';
    $('#addFormulaForm').find('input,select,textarea').each(function () {


        formulaData[$(this).attr('name')] = $.trim($(this).val());

    });

    if (formulaData['select_formula'] === 'Bestof') {

        formulaData['formula'] = formulaData['select_formula'] + '-' + formulaData['bestofval'] + '-' + formulaData['select_formula_forbestof'];

        if (formulaData['bestofval'] === '')
        {
            err = 1;
            errmsg = errmsg + 'Please enter best of val. <br>';
        }

        if (formulaData['select_formula_forbestof'] === '')
        {
            err = 1;
            errmsg = errmsg + 'Please select best of formula. <br>';
        }

    } else {
        formulaData['formula'] = formulaData['select_formula'];
    }

    formulaData['exam_ids'] = formulaData['my_multi_select2[]'];

    console.log(formulaData);

    if (formulaData['exam_ids'] === '')
    {
        err = 1;
        errmsg = errmsg + 'Please select exams,classtest & assignments. <br>';
    }

    if (formulaData['select_formula'] === '')
    {
        err = 1;
        errmsg = errmsg + 'Please select formula. <br>';
    }

    if (formulaData['contribute_outof'] === '')
    {
        err = 1;
        errmsg = errmsg + 'Please enter outof val. <br>';
    }


    if (err === 1) {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#formModal').modal('show')
        });
    } else {
        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/addFormula",
            data: formulaData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    var lblcls = res.type == 'error' ? 'danger' : 'success';
                    $('#messageBoxModal .modal-title').html(res.type);
                    $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == "success") {
//                            markSettings.loadStudentDiv( markSettings.stuCombiData.subsujectid);
                            $.ajax({
                                type: "POST",
                                url: commonSettings.markAjaxurl + "/getStudentList",
                                data: markSettings.stuCombiData,
                            }).done(function (resHtml) {
                                $("#mymarkDiv").html(resHtml);
                                markSettings.stuCombiData['MainExTotalMark'] = $('#mainexTotVal').val();

                                markSettings.updateHeight(max, 'mainmrkStuTable', 'markStuTable')
                            });
                        } else {
                            $('#formModal').modal('show')
                        }

                    });
                }
            }
        });
    }
};

markSettings.selectedFormula = function (slectval) {

    if (slectval === 'Bestof') {
        $('#bestofdiv').removeClass('hide')
        // $('#formula').val('MAX(a/10,b/10,c/10)');
    } else {
        if (!$('#bestofdiv').hasClass('hide')) {
            $('#bestofdiv').addClass('hide')
        }
    }

};