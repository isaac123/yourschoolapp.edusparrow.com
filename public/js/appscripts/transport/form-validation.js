   $(function() {
  
  $("#addVehicleForm").validate({
    
        // Specify the validation rules
        rules: {
            busno: "required",
            busregno: "required",
            seats: "required",
            
        },
        
        // Specify the validation error messages
        messages: {
           busno: "Please Enter Bus No",
            busregno: "Please Enter Bus Reg No",
            seats: "Please Enter Total No Of Seats",
			
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });
    
    
  });
