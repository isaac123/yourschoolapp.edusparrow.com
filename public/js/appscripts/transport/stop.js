


var stopSettings = {};

stopSettings.addStop = function () {
    $('.formBox-title').html('Add Stop');
    $('.formBox-body').html($('#addNewStop').html());
    $('#formModal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#formModal').modal('show');

    $('#confirm_save').html('Save');
    $('#confirm_save').unbind('click').click(function () {
        $('#confirm_close').click();
        stopSettings.addNewStop();
    });

};


stopSettings.addNewStop = function () {
    var stopData = {};
    var rest = {};
    var array = [];// new Array();
    var patchck = 0;
    var pattren = /^[a-zA-Z ]*$/;

    $("#addNewStopForm").find('input,select,textarea').each(function () {

        //if($(this).attr('name') == 'routename[]'){
        array.push($.trim($(this).val()));
        if ($.trim($(this).val()) !== "")
        {
            // alert('tert');
            if (!pattren.test($.trim($(this).val())))
            {
                patchck = 1;

                swal({
                    title: "Error",
                    text: ' Please Enter Valid Characters Only!',
                    type: "error",
                    confirmButtonText: 'ok!',
                    closeOnConfirm: true,
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                $('#formModal').modal('show');
//                            console.log(err)
//                            return false;
                            }
                        });
//                rest['type'] = 'error';
//                rest['message'] = '<div class="alert alert-block alert-danger fade in">Please Enter Valid Characters Only</div>';
//                $('.msgBox-title').html('Message');
//                $('.msgBox-body').html(rest['message']);
//                $('#messageBox').click();

            }
        }
        else
        {
            //  alert('tert');   rest['type'] ='error';
//				rest['message']='<div class="alert alert-block alert-danger fade in">Stop Name Is Required</div>';
//    			$('.msgBox-title').html('Message');
//                $('.msgBox-body').html(rest['message']);
//                $('#messageBox').click();

 patchck = 1;
            swal({
                title: "Error",
                text: ' Stop Name Is Required!',
                type: "error",
                confirmButtonText: 'ok!',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $('#formModal').modal('show');
//                            console.log(err)
//                            return false;
                        }
                    });
        }


    });

    //alert(array);
    if (patchck != 1)
    {
        console.log(stopData)
        stopData['stopname'] = array;
        $.ajax({
            type: "POST",
            url: commonSettings.routeAjaxurl + "/addNewStop",
            data: stopData,
            dataType: "JSON",
            success: function (res) {
                //alert(res['type']);
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            
                                 $('#formModal').modal('show');
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        else if (res['type'] == 'success') {
                            setTimeout(function () {
                                window.location.href = window.location.pathname + window.location.search
                            }, 0);
                        }
                    });

                }
            }
        });
    }
    //else
    //alert('not send');
};


stopSettings.editStop = function (StopID) {
//alert(StopID);
    $.ajax({
        type: "POST",
        url: commonSettings.routeAjaxurl + "/editStopByID",
        data: {
            StopID: StopID
        },
        dataType: "html",
        success: function (res) {
            $('.formBox-title').html('Edit Stop Details');
            $('.formBox-body').html(res);
            $('#modalForm').click();
            $('#confirm_save').html('Update');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                stopSettings.updateStop();
            });

        }
    });
};


stopSettings.updateStop = function () {
    var stopData = {};
    $("#editStopForm").find('input,select,textarea').each(function () {

        stopData[$(this).attr('name')] = $.trim($(this).val());
    });

    console.log(stopData)

    $.ajax({
        type: "POST",
        url: commonSettings.routeAjaxurl + "/updateStop",
        data: stopData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function () {
                    window.location.reload();
                });

            }
        }
    });
};


stopSettings.delStop = function (StopID) {

    $('#confirm_close').unbind('click').click();
    $('#msg_ok').unbind('click').click();
    stopSettings.StopID = StopID;
    $('.cnfBox-title').html('Delete Confirm');
    $('.cnfBox-body').html('Do you confirm to delete?');
    $('#confirmMsg').click();
    $('#confirm_yes').unbind('click').click(function () {
        $('#confirm_no').click();
        $.ajax({
            type: "POST",
            url: commonSettings.routeAjaxurl + "/delStop",
            data: {
                StopID: stopSettings.StopID,
            },
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        else if (res['type'] == 'success') {
                            setTimeout(function () {
                                window.location.href = window.location.pathname + window.location.search
                            }, 0);
                        }
                    });

                }
            }
        });
    });


};
