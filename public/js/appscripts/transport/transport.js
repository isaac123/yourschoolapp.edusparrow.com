var transportSettings = {};
transportSettings.noOfRecordsToDisplay = 50;
transportSettings.transSearchdata = {};
transportSettings.transSearchdatafromFee = {};

transportSettings.assignTransportToStudents = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.transportAjaxurl + "/transportSearch",
        dataType: "html",
        success: function (res) {

            $('#assigntransportDiv').html(res);
            $('#assigntransportDiv').removeClass('hide');
        }
    });
};

transportSettings.loadTransportApprvlList = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/transportApprvl",
        dataType: "html",
        success: function (res) {

            $('#trnsrtapprovaldiv').html(res);
            $('#trnsrtapprovaldiv').removeClass('hide');
        }
    });
};


transportSettings.loadSubtree = function (ctrl) {
    console.log($(ctrl).parent().parent().next('.subtreediv').html())
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.transportAjaxurl + "/loadSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};

transportSettings.loadSubtrees = function (ctrl) {
    console.log($(ctrl).parent().parent().next('.subtreediv').html())
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.transportAjaxurl + "/loadSubtrees",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};

transportSettings.loadStudents = function () {
    var searchData = {};
    console.log('ai')
    var err = 1;
    $("#transportStuForm").find('select').each(function () {
        if ($.trim($(this).val()) == '0') {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('Please select a value');
            $('#messageBox').click();
            err = 0;
            return false;
        } else {
            searchData[$(this).attr('name')] = $.trim($(this).val());

        }
    });
    console.log(err)
    searchData.err = err;
    transportSettings.transSearchdata = searchData;
    transportSettings.loadStudentsDiv(searchData)

};
transportSettings.loadStudentsDiv = function (searchData) {
    if (searchData.err == 1) {
        $.ajax({
            type: "POST",
            url: commonSettings.transportAjaxurl + "/loadStudents",
            data: searchData,
            success: function (res) {
                if (res) {
                    $('#StudentDet').html('');
                    $('#StudentDet').removeClass('hide')
                    $('#StudentDet').html(res);
                }

            }
        });
    }
};
transportSettings.updateRoute = function (ctrl) {
    var transData = {};
    var err = 1;
    $(ctrl).closest('tr').find('input').each(function () {
        if ($.trim($(this).val()) == '') {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('Please select a value');
            $('#messageBox').click();
            err = 0;
            return false;
        } else {
            transData[$(this).attr('name')] = $.trim($(this).val());

        }
    });
    if (err == 1) {
        $.ajax({
            type: "POST",
            url: commonSettings.transportAjaxurl + "/assignRoute",
            data: transData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html(res.type);
                    $('.msgBox-body').html(res.message);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        } else if (res['type'] == 'success') {
                            transData.type = 'Transport';
                            transData.itemID = res['ItemID'];
                            transData.status = 'Requested';
                            transData.fromPage = 'Transport'; //recordTypId
                            console.log(transData)
                            approvalSettings.checkReqStatus(transData);
                        }
                    });
                }

            }
        });
    }
    console.log(transData)
};
transportSettings.changeStatus = function (statusData) {
    $.ajax({
        type: "POST",
        url: commonSettings.transportAjaxurl + "/changeApprovalStatus",
        data: statusData,
        dataType: "JSON",
        success: function (res) {
            console.log(res)
            $('.msgBox-title').html(res.type);
            $('.msgBox-body').html(res.message);
            $('#messageBoxModal').modal('show');
            $('#msg_ok').unbind('click').click(function () {
                if (res['type'] == 'error') {
                    $('#msg_ok').unbind('click').click();
                    return false;
                } else if (res['type'] == 'success') {
                    if (statusData.fromPage == 'Transport') {
                        transportSettings.loadStudentsDiv(transportSettings.transSearchdata);
                    } else if (statusData.fromPage == 'TransportApprvl') {
                        approvalviewSettings.loadTransportApprvl();
                    } else
                    {
                        approvalSettings.loadApprovalList();
                    }
                }
            });

        }
    });
};

transportSettings.viewForwardDetails = function (ctrl) {
    var appData = {};
    appData.itemID = $(ctrl).attr('itemID');
    appData.appTypId = $(ctrl).attr('appType');
    appData.appitemID = $(ctrl).attr('appitemID');
    console.log(appData);
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/viewForwardHistory",
        data: appData,
        //dataType: "JSON",
        success: function (res) {
            console.log(res)
            $('.msgBox-title').html('List of Escalation made for this request');
            $('.msgBox-body').html(res);
            $('#messageBoxModal').modal();
            console.log("clicked")
        }
    });
};

transportSettings.loadTransportStudents = function () {
    var searchData = {};
    console.log('ai')
    var err = 1;
    $("#transportStuForm").find('select').each(function () {
        if ($(this).attr('name') == 'subordinate_type' && $.trim($(this).val()) == '0') {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('Please select ' + $(this).attr('title'));
            $('#messageBox').click();
            err = 0;
            return false;
        } else if ($.trim($(this).val()) == '0') {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('Please select a value');
            $('#messageBox').click();
            err = 0;
            return false;
        } else {
            searchData[$(this).attr('name')] = $.trim($(this).val());

        }
    });
    console.log(searchData)
    searchData.err = err;
    transportSettings.transSearchdatafromFee = searchData;
    //transportSettings.loadStudentsDiv(searchData)

    if (searchData.err == 1) {
        $.ajax({
            type: "POST",
            url: commonSettings.transportAjaxurl + "/loadTransportStudents",
            data: searchData,
            success: function (res) {
                if (res) {
                    $('#StudentDet').html('');
                    $('#StudentDet').removeClass('hide')
                    $('#StudentDet').html(res);
                }

            }
        });
    }

};

transportSettings.updateSubortinateVal = function (ctrl) {
    var transData = {};
    var err = 1;
    $(ctrl).closest('tr').find('select').each(function () {
        if ($.trim($(this).val()) == '') {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">Please select a value</div>');
            $('#messageBox').click();
            err = 0;
            return false;
        } else {
            transData[$(this).attr('name')] = $.trim($(this).val());

        }
    });
    if (err == 1) {
        $.ajax({
            type: "POST",
            url: commonSettings.transportAjaxurl + "/updateSubortinateValues",
            data: transData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html(res.type);
                    $('.msgBox-body').html(res.message);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        } else if (res['type'] == 'success') {
                            transportSettings.loadTransportStudents(this);

                        }
                    });
                }

            }
        });
        console.log(transData)
    }
};


transportSettings.deleteSubortinateVal = function (ctrl) {
    var transData = {};
    var err = 1;
    $(ctrl).closest('tr').find('input').each(function () {
        if ($.trim($(this).val()) == '') {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">Please select a value</div>');
            $('#messageBox').click();
            err = 0;
            return false;
        } else {
            transData[$(this).attr('name')] = $.trim($(this).val());

        }
    });
    if (err == 1) {
        $.ajax({
            type: "POST",
            url: commonSettings.transportAjaxurl + "/deleteSubortinateValues",
            data: transData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html(res.type);
                    $('.msgBox-body').html(res.message);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        } else if (res['type'] == 'success') {
                            transportSettings.loadTransportStudents(this);

                        }
                    });
                }

            }
        });
        console.log(transData)
    }
};


transportSettings.deleteAssignedRoute = function (ctrl) {
    var transData = {};
    $(ctrl).closest('tr').find('input').each(function () {
        if ($.trim($(this).val()) == ' ') {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('Please select a value');
            $('#messageBox').click();
            err = 0;
            return false;
        } else {
            transData[$(this).attr('name')] = $.trim($(this).val());

        }
    });
    $.ajax({
        type: "POST",
        url: commonSettings.transportAjaxurl + "/deleteAssignedRoute",
        data: transData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                $('.msgBox-title').html(res.type);
                $('.msgBox-body').html(res.message);
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                        return false;
                    } else if (res['type'] == 'success') {
                        transData.type = 'Transport Cancel';
                        transData.itemID = res['ItemID'];
                        transData.status = 'Requested';
                        transData.fromPage = 'Transport'; //recordTypId
                        transData.fromPageaction = 'DeleteTransport';
                        console.log(transData)
                        approvalSettings.checkReqStatus(transData);
                    }
                });
            }

        }
    });
    console.log(transData)
};

transportSettings.changeDeleteRouteStatus = function (statusData) {
    $.ajax({
        type: "POST",
        url: commonSettings.transportAjaxurl + "/changeDeleteApprovalStatus",
        data: statusData,
        dataType: "JSON",
        success: function (res) {
            console.log(res)
            $('.msgBox-title').html(res.type);
            $('.msgBox-body').html(res.message);
            $('#messageBoxModal').modal('show');
            $('#msg_ok').unbind('click').click(function () {
                if (res['type'] == 'error') {
                    $('#msg_ok').unbind('click').click();
                    return false;
                } else if (res['type'] == 'success') {
                    if (statusData.fromPage == 'Transport') {
                        transportSettings.loadStudentsDiv(transportSettings.transSearchdata);
                    } else if (statusData.fromPage == 'TransportApprvl') {
                        approvalviewSettings.loadTransportDeleteApprvl();
                    } else
                    {
                        approvalSettings.loadApprovalList();
                    }
                }
            });

        }
    });
};

transportSettings.loadTransportcanlcelApprvlList = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/transportDeleteApprvl",
        dataType: "html",
        success: function (res) {

            $('#trnsprtcanlcelApprovaldiv').html(res);
            $('#trnsprtcanlcelApprovaldiv').removeClass('hide');
        }
    });
};

transportSettings.loadTransportReportList = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.transportAjaxurl + "/transportReport",
        dataType: "html",
        success: function (res) {

            $('#transportReportdiv').html(res);
            $('#transportReportdiv').removeClass('hide');
        }
    });
};

transportSettings.loadTransportStudentList = function () {

    var transprtSearchData = {};
    var studata = {};
    var err = 0;
    var error = '';

    $("#transportStuSearchForm").find('input,select,textarea').each(function () {


        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n";

            } else {
                studata[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            studata[$(this).attr('name')] = $.trim($(this).val());
        }


//        var splitval = $(this).attr('name').split('_');
//        if (splitval[0] === 'aggregate') {
//            if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {

        studata[$(this).attr('name')] = $.trim($(this).val());
//            } else {
//                err = 1;
//            }
//        } else
//        {
//            studata[$(this).attr('name')] = $.trim($(this).val());
//        }
    });
//    var nodeid = $('#transportnodeid').val();
    console.log(studata);
    if (err === 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.transportAjaxurl + "/loadTransportReport",
            data: studata,
            dataType: "html",
            success: function (res) {

                $('#TransportStudentDet').html(res);
                $('#TransportStudentDet').removeClass('hide');
            }
        });
    }
    else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBoxModal').modal('show');
        err = 0;
        return false;
    }
};

transportSettings.sendMessageToStudents = function () {
    var params = {};
    
    var error = '';

    $("#transportStuSearchForm").find('input,select,textarea').each(function () {

        params[$(this).attr('name')] = $.trim($(this).val());
    });
      $('.cnfBox-title').html('Transport Message');
    $('.cnfBox-body').html('<div class="alert alert-block alert-danger fade in"><strong> Do you want send sms..?</strong></div>');
    $('#ConfirmModal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#confirm_yes').unbind('click').click(function () {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.transportAjaxurl + "/transportStudentMsgData",
//            data: params,
            dataType: "html",
            success: function (res) {

                $('.formBox-title').html('Transport Message');
                $('.formBox-body').html(res);
                $('#formModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#formModal').modal('show')
                $('#confirm_close').addClass('hide');
                $('#iconclose').addClass('hide');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    var err = 1;
                    $("#transportStudentMsgForm").find('input,textarea').each(function () {
                        if ($.trim($(this).val()) != '') {
                            params[$(this).attr('name')] = ($.trim($(this).val()));
                        }
                        else { 
                            err = 0;
                        }
                    });
                    if (err != 1) {
                        $('.msgBox-title').html('Message');
                        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">Messsage is required</div>');
                        $('#messageBoxModal').modal('show');
                        $('#msg_ok').unbind('click').click(function () {
                            $('#formModal').modal('show')
                        });


                    } else {
//                    
                        var current_effect = 'bounce';
                        //stuFeelist
                        transportSettings.run_waitMe(current_effect);

                        var recalculate_transctn = 'yes';
                        var change_ldgr_balnc = 'yes';
                        $.ajax({
                            type: "POST",
                            url: commonSettings.transportAjaxurl + "/sendMessageToTransportStudents",
                            data: params,
                            dataType: "JSON",
                            success: function (res) {
                                $('#other_section').waitMe('hide');
                                $('.modal-title').html("Message");
                                $('.modal-body').html(res.message);
                                $('#messageBoxModal').modal('show');
                                $('#msg_ok').unbind('click').click(function () {
                                    transportSettings.loadTransportStudentList();
                                    $('#messageBoxModal').modal('hide');

                                });
//            $('.mainexam').removeClass('hide')
//            $('.mainexam').html(res)
                            }
                        });
                    }
                });


            }
        });
    });

    $('#confirm_no').unbind('click').click(function () {

    });
};

transportSettings.run_waitMe = function (effect) {
    $('#other_section').waitMe({
        effect: effect,
        text: 'Please wait message sending...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: ''
    });
};
transportSettings.loadFeeTransportReportDet = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.transportAjaxurl + "/loadFeeTransportReportDet",
        dataType: "html",
        success: function (res) {

            $('#feetransportReportdiv').html(res);
            $('#feetransportReportdiv').removeClass('hide');
        }
    });
};
transportSettings.feeTransportReportforStud = function () {
    var transdata = {};
    var err = 0;
    var error = '';
    $("#transportReportSearchForm").find('input,select,textarea').each(function () {


        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n";
            } else {
                transdata[$(this).attr('name')] = $.trim($(this).val());
            }
        } else {
            transdata[$(this).attr('name')] = $.trim($(this).val());
        }
        transdata[$(this).attr('name')] = $.trim($(this).val());
    });
    console.log(transdata);
    if (err === 0) {
        $('#transreportDet').removeClass('hide');
         $('#transreportDet').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
        $.ajax({
            type: "POST",
            url: commonSettings.transportAjaxurl + "/feeTransportReportforStud",
            data: transdata,
            dataType: "html",
            success: function (res) {

                $('#transreportDet').html(res);
            }
        });
    }
    else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBoxModal').modal('show');
        err = 0;
        return false;
    }
};
transportSettings.feeTransportExcelReportforStud = function () {

    var params = {};
    var err = 0;
    var error = '';

    $("#transportReportSearchForm").find('input,select,textarea').each(function () {
        params[$(this).attr('name')] = $.trim($(this).val());

    });
    params['limit'] = 500;
    event.preventDefault();
    event.stopPropagation();
    var newForm = $('<form>', {
        'action': commonSettings.transportAjaxurl + "/feeTransportExcelReportforStud",
        'target': '_blank',
        'id': 'StudentList',
        'method': 'POST'
    });
    newForm.append($('<input>', {
        'name': 'params',
        'value': JSON.stringify(params),
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();

};
transportSettings.sendfeeTransportToStudents = function () {
    var params = {};
    var err = 0;
    var error = '';
    $("#transportReportSearchForm").find('input,select,textarea').each(function () {
        params[$(this).attr('name')] = $.trim($(this).val());
    });
    $('.cnfBox-title').html('Transport Message');
    $('.cnfBox-body').html('<div class="alert alert-block alert-danger fade in"><strong> Do you want send sms..?</strong></div>');
    $('#ConfirmModal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#confirm_yes').unbind('click').click(function () {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.transportAjaxurl + "/transportStudentMsgData",
            dataType: "html",
            success: function (res) {
                $('.formBox-title').html('Transport Message');
                $('.formBox-body').html(res);
                $('#formModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#formModal').modal('show')
                $('#confirm_close').addClass('hide');
                $('#iconclose').addClass('hide');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    var err = 1;
                    $("#transportStudentMsgForm").find('input,textarea').each(function () {
                        if ($.trim($(this).val()) != '') {
                            params[$(this).attr('name')] = ($.trim($(this).val()));
                        }
                        else {
                            err = 0;
                        }
                    });
                    if (err != 1) {
                        $('.msgBox-title').html('Message');
                        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">Messsage is required</div>');
                        $('#messageBoxModal').modal('show');
                        $('#msg_ok').unbind('click').click(function () {
                            $('#formModal').modal('show')
                        });
                    } else {
                        var current_effect = 'bounce';
                        transportSettings.run_waitMe(current_effect);
                        var recalculate_transctn = 'yes';
                        var change_ldgr_balnc = 'yes';
                        $.ajax({
                            type: "POST",
                            url: commonSettings.transportAjaxurl + "/sendfeeTransportToStudents",
                            data: params,
                            dataType: "JSON",
                            success: function (res) {
                                $('#other_section').waitMe('hide');
                                $('.modal-title').html("Message");
                                $('.modal-body').html(res.message);
                                $('#messageBoxModal').modal('show');
                                $('#msg_ok').unbind('click').click(function () {
                                    transportSettings.feeTransportReportforStud();
                                    $('#messageBoxModal').modal('hide');

                                });
                            }
                        });
                    }
                });
            }
        });
    });

    $('#confirm_no').unbind('click').click(function () {

    });

};