var routeSettings = {};


routeSettings.loadPromoPage = function() {
    //alert('test');
    var params = {};
    // params.classId = classid;    
    $.ajax({
        type: "POST",
        url: commonSettings.routeAjaxurl+"/routeclassSection",
        //  data: params,
        dataType: "html",
        success: function(res) {
            // alert(res);
            $('#routedet').append(res);
            var tablectrl = '#vehicledet';
            console.log(tablectrl)
            routeSettings.noOfRecordsToDisplay = 3;
            var column = [0, 1, 2, 3];
            var sortingDisabled = [4];

            $(tablectrl).dataTable({
                "sDom": 'C<"clear">lrtip',
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": 'routepromotionTable',
                "sPaginationType": "full_numbers",
                "iDisplayLength": routeSettings.noOfRecordsToDisplay,
                // 'iDisplayStart':patientsapp.startIndex,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[1, "desc"]],
                "bLengthChange": false,
                "bInfo": false,
                "fnServerData": function(surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function(o) {
                    if (o._iRecordsDisplay <= transportSettings.noOfRecordsToDisplay) {
                        $(tablectrl + '_paginate').hide();
                        //console.log(tablectrl+'_paginate');
                    }
                },
                "fnRowCallback": function(nRow, aData, iDisplayIndex) {

                    var rowText = '<tr id="' + aData.id + '">';

                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.busno + '</span>';
                    rowText += '</td>';

                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.busregno + '</span>';
                    rowText += '</td>';

                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.totalseats + '</span>';
                    rowText += '</td>';

                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.availableseats + '</span>';
                    rowText += '</td>';

                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="label label label-mini"><a href="javascript:;" title="Edit" class="glyphicon glyphicon-edit" onclick="transportSettings.editVehicle(' + aData.id + ')"></a>';
                    rowText += ' | <a href="javascript:;" title="Delete" class="glyphicon glyphicon-trash" onclick="transportSettings.delVehicle(' + aData.id + ')"></a></span>';
                    rowText += '</td>';

                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });
        }
    });
};

routeSettings.addRoute = function() {
    $('.formBox-title').html('Add Route');
    $('.formBox-body').html($('#addNewRoute').html());
    $('#modalForm').click();
    $('#confirm_save').html('Save');
    $('#confirm_save').unbind('click').click(function() {
        $('#confirm_close').click();
        routeSettings.addNewRoute();
    });

};


routeSettings.addNewRoute = function() {
    var routeData = {};
    var err = 0;
    $("#addNewRouteForm").find('input,select,textarea').each(function() {
        if ($.trim($(this).val()) == '') {
            err = 1;
            swal({
                title: "Error",
                text: $(this).attr('title') + ' is required!',
                type: "error",
                confirmButtonText: 'ok!',
                closeOnConfirm: true,
            },
                    function(isConfirm) {
                        if (isConfirm) {
                            console.log(err)
                            return false;
                        }
                    });
        } else {
            routeData[$(this).attr('name')] = $.trim($(this).val());
        }

    });
    console.log(routeData)

    if (err != 1) {
        $.ajax({
            type: "POST",
            url: commonSettings.routeAjaxurl+"/addNewRoute",
            data: routeData,
            dataType: "JSON",
            success: function(res) {
                if (res) {
                    swal({
                        title: res.type,
                        text: res.message,
                        type: res.type,
                        confirmButtonText: 'ok!',
                        closeOnConfirm: true,
                    },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location.href = window.location.pathname + window.location.search
                                }
                            });
                }
            }
        });
    }
};
routeSettings.editRoute = function(routeID) {
            console.log(routeID)
    $.ajax({
        type: "POST",
        url: commonSettings.routeAjaxurl+"/editRouteByID",
        data: {
            routeID: routeID
        },
        dataType: "html",
        success: function(res) {
            $('.formBox-title').html('Edit Route Details');
            $('.formBox-body').html(res);
            $('#confirm_save').html('Update');
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#confirm_save').unbind('click').click(function() {
                $('#formModal').modal('hide')
                routeSettings.updateRoute();
            });
        }
    });
};
routeSettings.updateRoute = function() {
    var routeData = {};
    var err = 0;
    $("#editRouteForm").find('input,select,textarea').each(function() {
        if ($.trim($(this).val()) == '') {
            err = 1;
            swal({
                title: "Error",
                text: $(this).attr('title') + ' is required!',
                type: "error",
                confirmButtonText: 'ok!',
                closeOnConfirm: true,
            },
                    function(isConfirm) {
                        if (isConfirm) {
                            console.log(err)
                            return false;
                        }
                    });
        } else {
            routeData[$(this).attr('name')] = $.trim($(this).val());
        }

    });
    console.log(routeData)

    if (err != 1) {
        $.ajax({
            type: "POST",
            url: commonSettings.routeAjaxurl+"/updateRoute",
            data: routeData,
            dataType: "JSON",
            success: function(res) {
                if (res) {

                    swal({
                        title: res.type,
                        text: res.message,
                        type: res.type,
                        confirmButtonText: 'ok!',
                        closeOnConfirm: true,
                    },
                            function(isConfirm) {
                                if (isConfirm) {
                                   transportSettings.loadRoute();  // window.location.href = window.location.pathname + window.location.search
                                }
                            });
                }
            }
        });
    }
};
routeSettings.delRoute = function(RouteID) {

    $('#confirm_close').unbind('click').click();
    $('#msg_ok').unbind('click').click();
    routeSettings.RouteID = RouteID;
    $('.cnfBox-title').html('Delete Confirm');
    $('.cnfBox-body').html('Do you confirm to delete?');
    $('#confirmMsg').click();
    $('#confirm_yes').unbind('click').click(function() {
        $('#confirm_no').click();
        $.ajax({
            type: "POST",
            url: commonSettings.routeAjaxurl+"/delRoute",
            data: {
                RouteID: routeSettings.RouteID,
            },
            dataType: "JSON",
            success: function(res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function() {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        else if (res['type'] == 'success') {
                            setTimeout(function() {
                                 transportSettings.loadRoute(); //window.location.href = window.location.pathname + window.location.search
                            }, 0);
                        }
                    });
                }
            }
        });
    });
};
 