var contentsharingSettings = {};
contentsharingSettings.saveReadingFiles = function () {
    var Data = {};
    var file = {};
    var err = 0;
    var error = '';
    $("#fileuploadreading").find('input,select,textarea').each(function () {
        Data[$(this).attr('name')] = $.trim($(this).val());
    });
    var i = 0;
    $('#readingsfile').find('a.fileretrieve').each(function () {
        file[i++] = $(this).text();
    });
    Data['filename'] = file;
    if (Data['filename'].length < 0) {
        err = 1;
        error += " Upload File is required!\n</br>";
    }
    if (Data['content_title'] == '') {
        err = 1;
        error += " Title is required!\n</br>";
    }
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.contentsharingAjaxurl + "/saveReadingFiles",
            data: Data,
            dataType: "JSON",
            success: function (res) {
                if (res.type == 'success') {
                    bootbox.dialog({
                        title: "Message",
                        message: res.message,
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn-success",
                                callback: function () {
                                    window.location.reload();
                                }
                            }
                        }
                    });
                } else {
                    bootbox.dialog({
                        title: "Message",
                        message: res.message,
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn-danger"
                            }
                        }
                    });
                    err = 0;
                }
            }
        });
    } else {
        bootbox.dialog({
            title: "Message",
            message: error,
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn-danger"
                }
            }
        });
        err = 0;
    }
};