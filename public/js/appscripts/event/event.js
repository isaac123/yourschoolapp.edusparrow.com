var eventsSettings = {};
eventsSettings.selectEventUser = function () {
    var ids = $('#eventUserIds').val();
    $.ajax({
        type: "POST",
        url: commonSettings.eventAjaxUrl + "/selectEventUser",
        data: {ids: ids},
        dataType: 'html',
        success: function (res) {
            bootbox.dialog({
                title: "Event Attendees",
                message: res,
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var event, err = 0, error = '';
                            event = $('#events_user_list').val();
                            console.log(event);
                            if (event == '') {
                                err = 1;
                                error += " User Detail is required !</br>";
                            }
                            if (err == 0) {
                                $('#eventUserIds').val(event);
                            } else {
                                bootbox.dialog({
                                    title: "Message",
                                    message: error,
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn-danger",
                                            callback: function () {
                                                eventsSettings.selectEventUser();
                                            }
                                        }
                                    }
                                });
                                err = 0;
                            }
                        }
                    }
                }
            });
        }
    });
};
eventsSettings.saveEvents = function (ctrl) {

    var Dtype = $(ctrl).data("type"),
            Dlayout = $(ctrl).data("layout");
    var params = {}, err = 0, error = '';

    $(ctrl).parents('#event').find('textarea,input,select').each(function () {
        params[$(this).attr('id')] = $(this).val();
    });
    if (params['event_text'] == '') {
        err = 1;
        error += " Event Message is required !</br>";
    }
    if (params['eventUserIds'] == '') {
        err = 1;
        error += " User Details is required !</br>";
    }
    if (params['eventtype'] == '') {
        err = 1;
        error += "Event Type is required !</br>";
    }
    if (err == 0) {

        $('body').mask("Loading");
        $.ajax({
            type: "POST",
            url: commonSettings.eventAjaxUrl + "/saveEvents",
            data: params,
            dataType: 'JSON',
            success: function (res) {
                setTimeout($('body').unmask(), 1500);
                if (res.type == 'success') {
                    var n_dom = [];
                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a> Notification Sent <span>' + res.time + '</span> </div> </div>';
                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                    announcementSettings.communicate();
                    announcementSettings.reloadBoxes()
                } else {
                    bootbox.dialog({
                        title: "Error",
                        message: '<span class="text text-danger ">' + res.message + '</span>',
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn-danger"
                            }
                        }
                    });
                    err = 0;
                }
            }
        });
    } else {
        bootbox.dialog({
            title: "Error",
            message: '<span class="text text-danger ">' + error + '</span>',
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn-danger"
                }
            }
        });
        err = 0;
    }

};
eventsSettings.acceptEvents = function (event_id, ctrl) {
    var edit = $(ctrl).attr('edit');
    if (edit == 'inbox') {
        viewevent.modal('hide');
    }
    if (edit == 'outbox') {
        editevent.modal('hide');
    }
    $.ajax({
        type: "POST",
        url: commonSettings.eventAjaxUrl + "/acceptEvents",
        data: {event_id: event_id},
        dataType: 'JSON',
        success: function (res) {
            if (ctrl) {
                var Dtype = $(ctrl).data("type"),
                        Dlayout = $(ctrl).data("layout");
                if (res.type == 'success') {
                    var n_dom = [];
                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a> Events Accepted <span>' + res.time + '</span> </div> </div>';
                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                    announcementSettings.communicate();
                    announcementSettings.reloadBoxes()
                } else {
                    bootbox.dialog({
                        title: "Message",
                        message: res.message,
                        buttons: {
                            success: {
                                label: "Ok",
                                className: "btn-danger"
                            }
                        }
                    });
                    err = 0;
                }
            } else {
                bootbox.dialog({
                    title: "Message",
                    message: res.message,
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn-success",
                            callback: function () {
                                window.location.reload();
                            }
                        }
                    }
                });
            }
        }
    });
};
eventsSettings.viewEvents = function (event_id, edit) {
    $.ajax({
        type: "POST",
        url: commonSettings.eventAjaxUrl + "/viewEvents",
        data: {event_id: event_id, edit: edit},
        dataType: 'html',
        success: function (res) {
            if (edit == 'outbox') {
                editevent = bootbox.dialog({
                    backdrop: true,
                    title: "Event",
                    animate: true,
                    width: "50%",
                    message: res,
                    buttons: {
                        success: {
                            label: "Edit",
                            className: "btn-success",
                            callback: function () {
                                editevent.modal('hide');
                                $.ajax({
                                    type: "POST",
                                    url: commonSettings.eventAjaxUrl + "/editEvents",
                                    data: {event_id: event_id},
                                    dataType: 'html',
                                    success: function (res) {
                                        edit_event = bootbox.dialog({
                                            title: " Edit Event",
                                            message: res,
                                            buttons: {
                                                success: {
                                                    label: "Save",
                                                    className: "btn-success",
                                                    callback: function () {
                                                        var params = {}, i = 0;
                                                        $("#EventsEditForm").find('input,select,textarea').each(function () {
                                                            params[$(this).attr('id')] = $(this).val();
                                                        });
                                                        $("#editfileevent").find('li').each(function () {
                                                            params['file_' + i] = $.trim($(this).find('p').text());
                                                            i++;
                                                        });
                                                        $('body').mask("Loading");
                                                        $.ajax({
                                                            type: "POST",
                                                            url: commonSettings.eventAjaxUrl + "/saveEvents",
                                                            data: params,
                                                            dataType: 'JSON',
                                                            success: function (res) {
                                                                setTimeout($('body').unmask(), 1500);
                                                                if (res.type == 'success') {
                                                                    var Dtype = 'information';
                                                                    var Dlayout = 'topRight';
                                                                    var n_dom = [];
                                                                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a>  Notification Updated <span>' + res.time + '</span> </div> </div>';
                                                                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                                                                    announcementSettings.communicate();
                                                                    announcementSettings.reloadBoxes()
                                                                } else {
                                                                    bootbox.dialog({
                                                                        message: res.message,
                                                                        buttons: {
                                                                            danger: {
                                                                                label: "Ok",
                                                                                className: "btn-danger",
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                },
                                                Close: {
                                                    label: "Close",
                                                    className: "btn-default",
                                                    callback: function () {
                                                        edit_event.modal('show');
                                                    }

                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        },
                        close: {
                            label: "Close",
                            className: "btn-default",
                            callback: function () {
                                editevent.modal('show');
                                announcementSettings.reloadBoxes();
                            }
                        }

                    }
                });

            } else {
                viewevent = bootbox.dialog({
                    title: "Event Details",
                    animate: true,
                    message: res,
                    className: "modal70",
                    buttons: {
                        Close: {
                            label: "Close",
                            className: "btn-default",
                            callback: function () {
                                viewevent.modal('hide');
                                announcementSettings.reloadBoxes();
                            }
                        }
                    }
                });
            }
        }

    });
};
eventsSettings.advanceSettings = function (ctrl) {
    var params = {};
    $(ctrl).parents('#event').find('textarea,input,select').each(function () {
        params[$(this).attr('id')] = $(this).val();
    });
    var i = 0;
    $.ajax({
        type: "POST",
        url: commonSettings.eventAjaxUrl + "/advanceSettings",
        data: params,
        dataType: 'html',
        success: function (res) {
            $('#formModal .formBox-title').html('Event Details');
            $('#formModal #confirm_save').html('Post');
            $('#formModal .formBox-body').html(res);
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#confirm_save').unbind('click').click(function () {
                $('#formModal').modal('hide')
                var form = $('#EventsForm');
                var formData = new FormData(form[0]);
                $("#Events_upload").find('li').each(function () {
                    formData.append('file_' + i, $.trim($(this).find('p').text()));
                    i++;
                });
                $('body').mask("Loading");
                $.ajax({
                    type: "POST",
                    url: commonSettings.eventAjaxUrl + "/saveEvents",
                    processData: false,
                    contentType: false,
                    data: formData,
                    dataType: 'JSON',
                    success: function (res) {
                        setTimeout($('body').unmask(), 1500);
                        if (res.type == 'success') {
                            var Dtype = 'information';
                            var Dlayout = 'topRight';
                            var n_dom = [];
                            n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a>  Notification Sent <span>' + res.time + '</span> </div> </div>';
                            announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                            announcementSettings.communicate();
                            $('#EventsForm')[0].reset();   
                            announcementSettings.reloadBoxes()
                        } else {
                            $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-info"></i> Error');
                            $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">' + res.message + '</span>');
                            $('#messageBoxModal').modal('show');
                            $('#msg_ok').unbind('click').click(function () {
                                $('#messageBoxModal').modal('hide');
                                $('#formModal').modal('show')

                            });
                            return false;
                        }
                    }
                });
            });
        }
    });
};
eventsSettings.downloadEventFile = function (event_id) {
    var newForm = $('<form>', {
        'action': commonSettings.eventAjaxUrl + "/downloadEventFile",
        'method': 'POST',
        'target': '_blank',
        'id': 'downloadPhoto'
    }).append($('<input>', {
        'name': 'event_id',
        'value': event_id,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};