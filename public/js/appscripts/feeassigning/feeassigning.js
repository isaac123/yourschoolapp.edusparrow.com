var feeassigingSettings = {};
feeassigingSettings.searchParams = {};
feeassigingSettings.loadSections = function(sval) {
    console.log(sval);
    feeassigingSettings.classID = sval;
    $.ajax({
        type: "POST",
        url: commonSettings.feeAssigningUrl + "/loadSection",
        data: {
            classId: sval
        },
        success: function(res) {
            if (res) {
                $('#secDet').removeClass('hide')
                $('#secDet').html(res);
            }

        }
    });

};

feeassigingSettings.loadStudents = function() {
    var applnData = {};
    var err = 0;
    var div = $('#divID_assign').val();
    if (div == '')
    {
        $('#node_id_assign').val('');
    }
    $("#feeAssigningSearchForm").find('input, select').each(function() {
        if (($.trim($(this).val()) == 0) || ($.trim($(this).val()) == '')) {
            err = 1;
            swal({
                title: "Error",
                text: $(this).attr('title') + ' is required!',
                type: "error",
                confirmButtonText: 'ok!',
                closeOnConfirm: true,
            },
                    function(isConfirm) {
                        if (isConfirm) {
                            console.log(err)
                            return false;
                        }
                    });
            return;
        } else {
            applnData[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    console.log(err)
    if (err != 1) {
        feeassigingSettings.searchParams = applnData;
        feeassigingSettings.loadStudentList()
    }
};
feeassigingSettings.reloadStudentList = function(feeid) {
    feeassigingSettings.searchParams.feeSelected = feeid;
    feeassigingSettings.loadStudentList()
}
feeassigingSettings.loadStudentList = function() {
    $.ajax({
        type: "POST",
        url: commonSettings.feeAssigningUrl + "/getStudentList",
        data: feeassigingSettings.searchParams,
        dataType: "Html",
        success: function(res) {
            $('#StuTableDiv').parents('section').removeClass('hide')
            $('#StuTableDiv').html(res)
        }
    });
};
feeassigingSettings.assignFeeStu = function() {
    var assignData = {};
    $("#assignFeeStuTable").find('input:checked').each(function() {
        assignData[$(this).attr('name')] = $.trim($(this).val());
    });
    if ($.isEmptyObject(assignData)) {
        swal({
            title: "Error",
            text: 'Select atleast one student!',
            type: "error",
            confirmButtonText: 'ok!',
            closeOnConfirm: true,
        },
                function(isConfirm) {
                    if (isConfirm) {
                        return false;
                    }
                });
    } else {

    assignData['fee_item_id'] = $("#assignFeeStuTable").attr('fee_id')
        console.log(assignData)
        if (assignData['select-all']) {
            swal({
                title: "Confirm",
                text: "Do you confirm to assign this fee to all students?",
                type: "warning",
                confirmButtonText: 'Yes!',
                showCancelButton: true,
                closeOnCancel: true,
                closeOnConfirm: true,
                cancelButtonText: 'Cancel',
            },
                    function(isConfirm) {
                        if (isConfirm) {
                            feeassigingSettings.assigningFeeToStudents(assignData)
                            return false;
                        }
                    });
        }else{
             feeassigingSettings.assigningFeeToStudents(assignData)
        }
    }
};
feeassigingSettings.assigningFeeToStudents = function(assignData){
        $.ajax({
        type: "POST",
        url: commonSettings.feeAssigningUrl + "/assigningFeeToStudents",
        data: assignData,
        dataType: "JSON",
        success: function(res) {
            
        swal({
            title: res.type,
            text: res.message,
            type: res.type,
            confirmButtonText: 'ok!',
            closeOnConfirm: true,
        },
                function(isConfirm) {
                    if (isConfirm) {
                      feeassigingSettings.loadStudentList()
                    }
                });
        }
    });
}