var classroomAdminSettings = {};

classroomAdminSettings.loadAttendanceSet = function() {
    $.ajax({
        type: "POST",
        url: commonSettings.attendancesettingsAjaxurl + "/studentAttendanceSettings",
        dataType: "html",
        success: function(res) {
            $('#attcombiDiv').html(res);    
            $('#attcombiDiv').removeClass('hide')
        }
    });
};

classroomAdminSettings.loadXamCombination = function() {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/listAdminCombination",
        dataType: "html",
        success: function(res) {
            $('#xamcombiDiv').html(res);
            $('#xamcombiDiv').removeClass('hide')
        }
    });
};

classroomAdminSettings.loadAddExam = function() {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/addExamByClass",
        dataType: "html",
        success: function(res) {
            $('#xamcombiDiv').html(res);
            $('#xamcombiDiv').removeClass('hide')
        }
    });
};
classroomAdminSettings.ratingSettings= function() {
    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/ratingSettings",
        dataType: "html",
        success: function(res) {
            $('#ratCategoryDiv').html(res);
            $('#ratCategoryDiv').removeClass('hide')
        }
    });
};

classroomAdminSettings.ratingCathelpLegend = function() {
    $('.msgBox-title').html('Help text');
    $('.msgBox-body').html($('.ratingCathelpLegend').html());
    $('#messageBoxModal').modal({
        backdrop: 'static',
        keyboard: false
    });
};
classroomAdminSettings.assignrating= function() {
    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/assignRating",
        dataType: "html",
        success: function(res) {
            $('#ratDivisonDiv').html(res);
            $('#ratDivisonDiv').removeClass('hide')
        }
    });
};