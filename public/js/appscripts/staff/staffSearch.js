
staffSearchSettings = {};
staffSearchSettings.searchVal = {};

staffSearchSettings.loadStaffSalary = function()
{
    var month = $('#month').val() ? $('#month').val() : '-';
    var staffid = $('#staffid').val();
    
    if(month == '-'){
         $('.msgBox-title').html('Message');
            $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in"> Please Select Month</div>');
            $('#messageBox').click();
        
    }else{
     $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/staffPayroll",
        data: {
            month: month,
            staffid:staffid
        },
        dataType: "html",
        success: function (res) {
            $('#payrollTable').html(res)
        }
    });
}
};

staffSearchSettings.loadStaffAdvanceSalary= function(staffid)
{
     $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/getStaffAdvanceDet",
        data: {
            staffid:staffid
        },
        dataType: "html",
        success: function (res) {
            $('#stfAdvanceSalarydiv').html(res)
        }
    });
};

staffSearchSettings.loadManageLeave = function() {
    $.ajax({
        type: "POST",
        url: commonSettings.leaveAjaxurl + "/manageMyLeaves",
        dataType: "html",
        success: function(res) {
            $('#leaveDatadiv').html(res);
        }
    });
};
