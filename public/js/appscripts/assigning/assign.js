var assignSettings = {};
assignSettings.loadspacetree = 0;
assignSettings.cycleID = 0;
assignSettings.groupClsTechID = 0;
assignSettings.getSubjectsByCycle = function (sval) {

    if (sval == 0)
    {
        swal("Error", "Data is missing", "error");
        return;
    }
    assignSettings.cycleID = sval;
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/getSubjectsByCycle",
        data: {
            cycleID: sval
        },
        success: function (res) {
            if (res) {
                $('#subjDet').html(res);
                $('#subjDet').removeClass('hide')
            }
        }
    });

};
assignSettings.toggleaddgroup = function (ctrl)
{
    $('#common_subjects').empty();
    $(ctrl).parents('td').find('input,select').val('')
    $('.add_group').toggleClass('hide');
    $('.select_group').toggleClass('hide');
    $('#add_group').toggleClass('hide');
    $('#select_group').toggleClass('hide');
};
assignSettings.loadClsTeacherDelete = function (clsteachId) {

    if (clsteachId == 0)
    {
        swal("Error", "Invalid entry", "error");
        return;
    }
    $('.cnfBox-title').html('Delete Confirm');
    $('.cnfBox-body').html('Do you confirm to delete?');
    $('#confirmMsg').click();
    $('#confirm_yes').unbind('click').click(function () {
        $('#confirm_no').click();

        $.ajax({
            type: "POST",
            url: commonSettings.assigningAjaxurl + "/deleteClsTeachrById",
            data: {
                clsteachId: clsteachId
            },
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        else if (res['type'] == 'success') {

                            assignSettings.displayGroupClsTeachers();
                        }
                    });
                }
            }
        });
    });

};
assignSettings.loadSubTeacherEdit = function (ctrl) {
    $(ctrl).parent('td').prev().find('span.editteach').removeClass('hide')
    $(ctrl).parent('td').prev().find('span.teachlist').addClass('hide')
}
assignSettings.loadSubTeacherDelete = function (subjteachId) {

    if (subjteachId == 0)
    {
        swal("Error", "Invalid entry", "error");
        return;
    }
    $('.cnfBox-title').html('Delete Confirm');
    $('.cnfBox-body').html('Do you confirm to delete?');
    $('#confirmMsg').click();
    $('#confirm_yes').unbind('click').click(function () {
        $('#confirm_no').click();

        $.ajax({
            type: "POST",
            url: commonSettings.assigningAjaxurl + "/deleteSubTeacherById",
            data: {
                subjteachId: subjteachId
            },
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        else if (res['type'] == 'success') {

                            assignSettings.displayGroupSubjectTeachers();
                        }
                    });
                }
            }
        });
    });

};

assignSettings.addsubject = function () {
    var subjectData = {};
    $("#addSubjectForm").find('input').each(function () {
        if ($(this).attr('name') == 'mandatory')
        {
            subjectData[$(this).attr('name')] = $.trim($("input[type='radio'][name='mandatory']:checked").val());
        }
        else
        {
            subjectData[$(this).attr('name')] = $.trim($(this).val());
        }
    });


    subjectData['classID'] = assignSettings.classID;
    alert(assignSettings.classID);

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/addSubject",
        data: subjectData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                if (res['type'] === 'error') {
                    swal("Error", res['message'], "error");
                    $('#formModal').modal('show');
                }
                if (res['type'] === 'warning') {
                    swal("Warning", res['message'], "warning");
                    $('#formModal').modal('show');
                }
                else if (res['type'] === 'success') {
                    swal({
                        title: "Done",
                        text: res['message'],
                        type: "success",
                    });

                }
                assignSettings.getSubjectsByCycle(assignSettings.cycleID)
//                assignSettings.loadSubjects(assignSettings.classID);
            }
        }
    });

};

assignSettings.showAllTable = function () {
    $('.node_table').addClass('show');

};


//assignSettings.showSelectedTable = function(node_name) {
////    alert(node_name);
////$('.node_table').addClass('active');
////$('.node_table').removeClass('show');
//    var id = "#node_"+node_name;
//    alert(id);
//  $(id).addClass('active');
//  
//};

assignSettings.loadSingleSubject = function (subjID) {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/getSubjectById",
        data: {
            subjId: subjID
        },
        success: function (res) {
            if (res) {
                //  $('#subjEdit').html(res)

                $('.modal-title').html('Edit Subject');
                $('.modal-body').html(res);
                $('#modalForm').click();
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    assignSettings.Editsubject(subjID);
                });
            }

        }
    });

};

assignSettings.Editsubject = function (subjId) {

    var subjectData = {};
    $("#updateSubjectForm").find('input').each(function () {
        if ($(this).attr('name') == 'mandatory')
        {
            subjectData[$(this).attr('name')] = $.trim($("input[type='radio'][name='mandatory']:checked").val());
        }
        else
        {
            subjectData[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    subjectData['subjId'] = subjId;
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/editSubjectById",
        data: subjectData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                //$('#addsubj_message').html('')
                //$('#addsubj_message').html(res);
                console.log(res)
                console.log(res.type)
                if (res['type'] === 'error') {
                    swal("Error", res['message'], "error");
                }
                if (res['type'] === 'warning') {
                    swal("Warning", res['message'], "warning");
                }
                else if (res['type'] === 'success') {
                    swal({
                        title: "Done",
                        text: res['message'],
                        type: "success",
                    });

                }
                assignSettings.getSubjectsByCycle(assignSettings.cycleID)
//                assignSettings.loadSubjects(assignSettings.classID);
            }
        }
    });

};

assignSettings.addSubjSubDiv = function (ctrl) {
    var formname = ctrl.id.split('_')
    console.log(formname)
    var subjectData = {};
    $("#assignSubjSubDiv_" + formname[1] + "_" + formname[2]).find('input,select').each(function () {
        subjectData[$(this).attr('name')] = $.trim($(this).val());
    });

    console.log(subjectData);

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/assignSubject",
        data: subjectData,
        success: function (res) {
            if (res) {
                //alert(res)
                //$('#assignsubj_message').html(res);
                $('.modal-title').html('Message');
                $('.modal-body').html(res);
                $('#messageBox').click();
                $('#msg_ok').click(function () {
                    window.location.reload();
                });
            }

        }
    });


};

assignSettings.removesubject = function (ctrl) {
    console.log($(ctrl).closest('div').attr("id"))
    var subDivSubjId = $(ctrl).closest('div').attr("id").split('_')
    console.log(subDivSubjId[1]);

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/deleteSecSubject",
        data: {
            subDivSubjId: subDivSubjId[1]
        },
        success: function (res) {
            if (res) {
                //alert(res)
                //$('#assignsubj_message').html(res);
                $('.modal-title').html('Message');
                $('.modal-body').html(res);
                $('#messageBox').click();
                $('#msg_ok').click(function () {
                    window.location.reload();
                });
            }

        }
    });

};
assignSettings.loadSections = function (sval) {
    console.log(sval);
    assignSettings.classID = sval;
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/getSectionByClass",
        data: {
            classId: sval
        },
        success: function (res) {
            if (res) {
                $('#subjTeachDet').html(res);
                $('#subjTeachDet').removeClass('hide')
            }

        }
    });

};

assignSettings.loadSubjectTeacher = function (ctrl) {

    var searchData = {};
    $("#assignSubjTeachForm").find('input,select').each(function () {
        if ($.trim($(this).val()) != 0) {
            searchData[$(this).attr('name')] = $.trim($(this).val());
        } else {
            $('.modal-title').html('Message');
            $('.modal-body').html('Please Select a value! ');
            $('#messageBox').click();
            return false;
        }
    });
    console.log(searchData);
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/loadSubjTeacher",
        data: searchData,
        success: function (res) {
            if (res) {
                //alert(res)
                $('#subjTechMatrix').html(res);
                $('#subjTechMatrix').removeClass('hide')
            }

        }
    });

};

assignSettings.addSubjteach = function (ctrl) {
    var formname = ctrl.id.split('_')
    console.log("#assignSubjTeachr_" + formname[1] + "_" + formname[2] + "_" + formname[3])
    var teacherData = {};
    $("#assignSubjTeachr_" + formname[1] + "_" + formname[2] + "_" + formname[3]).find('input,select').each(function () {
        teacherData[$(this).attr('name')] = $.trim($(this).val());
    });

    console.log(teacherData);

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/addSubjTeacher",
        data: teacherData,
        success: function (res) {
            if (res) {
                //alert(res)
                //$('#assignsubj_message').html(res);
                $('.modal-title').html('Message');
                $('.modal-body').html(res);
                $('#messageBox').click();
                $('#msg_ok').click(function () {
                    assignSettings.loadSubjectTeacher($('#subjteach_add'))
                });
            }

        }
    });


};

assignSettings.removeSubjTeach = function (ctrl) {
    console.log($(ctrl).closest('div').attr("id"))
    var subjTeachId = $(ctrl).closest('div').attr("id").split('_')
    console.log(subjTeachId[1]);

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/deleteSubjectTeacher",
        data: {
            subjTeachId: subjTeachId[1]
        },
        success: function (res) {
            if (res) {
                //alert(res)
                //$('#assignsubj_message').html(res);
                $('.modal-title').html('Message');
                $('.modal-body').html(res);
                $('#messageBox').click();
                $('#msg_ok').click(function () {
                    assignSettings.loadSubjectTeacher($('#subjteach_add'))
                });
            }

        }
    });

};

assignSettings.assignUserRole = function (ctrl) {

    var permissionData = {};
    var removeData = {};
    var assignData = {};
    $("#assignUserRolesForm").find('input:checked').each(function () {
        assignData[$(this).attr('name')] = $.trim($(this).val());

    });
    $("#assignUserRolesForm").find('input:not(:checked)').each(function () {
        if ($(this).attr('name') != 'assignUsrRol')
            removeData[$(this).attr('name')] = 0;

    });
    permissionData.on = assignData;
    permissionData.off = removeData;

    console.log(permissionData);
    $.ajax({
        type: "POST",
        url: commonSettings.usersroleAjaxurl + "/assignUsrRoles",
        data: permissionData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                $('.modal-title').html(res.type);
                $('.modal-body').html(res.message);
                $('#messageBox').click();
                $('#msg_ok').click(function () {
                    window.location.reload();
                });
            }

        }
    });

};

assignSettings.createSubject = function (node_id) {

    assignSettings.classID = node_id;
    $('.modal-title').html('Add Subject');
    $('.modal-body').html($('#addSubjSec').html());
    //$('#modalForm').click();
    $('#formModal').modal('show');
    $('#confirm_save').unbind('click').click(function () {
        $('#confirm_close').click();
        assignSettings.addsubject();
    });

};

assignSettings.loadGroupsSubjectTeachers = function (cycleID) {

    if (cycleID == 0)
    {
        swal("Error", "Data is missing", "error");
        return;
    }

    assignSettings.cycleID = cycleID;
    var searchData = {};
    searchData['cycleID'] = assignSettings.cycleID;

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/loadGroupsSubjectTeachers",
        data: searchData,
        success: function (res) {
            if (res) {
                $('#subjTechandteacherMatrix').html(res);
                $('#subjTechandteacherMatrix').removeClass('hide');
            }

        }
    });

    assignSettings.displayGroupSubjectTeachers();
    //assignSettings.loadSubjectsandTeachers($('#divID').val());
    //assignSettings.divvalwisloadsubjandteachrs(searchData); 

};

assignSettings.loadGroupsClsTeachers = function (cycleID) {

    if (cycleID == 0)
    {
        swal("Error", "Data is missing", "error");
        return;
    }

    assignSettings.cycleID = cycleID;
    var searchData = {};
    searchData['cycleID'] = assignSettings.cycleID;

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/loadGroupsClsTeachers",
        data: searchData,
        success: function (res) {
            if (res) {
                $('#ClsTechandteacherMatrix').html(res);
                $('#ClsTechandteacherMatrix').removeClass('hide');
            }

        }
    });

    assignSettings.displayGroupClsTeachers();
    //assignSettings.loadSubjectsandTeachers($('#divID').val());
    //assignSettings.divvalwisloadsubjandteachrs(searchData); 

};
assignSettings.loadClsTeacherForEdit = function (groupClsTechID) {

    if (groupClsTechID == 0)
    {
        swal("Error", "Data is missing", "error");
        return;
    }

    assignSettings.groupClsTechID = groupClsTechID;
    var searchData = {};
    searchData['groupClsTechID'] = assignSettings.groupClsTechID;

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/loadGroupsClsTeachersForEdit",
        data: searchData,
        success: function (res) {
            if (res) {
                $('#ClsTechandteacherMatrix').html(res);
                $('#ClsTechandteacherMatrix').removeClass('hide');
            }

        }
    });

    assignSettings.displayGroupClsTeachers();
    //assignSettings.loadSubjectsandTeachers($('#divID').val());
    //assignSettings.divvalwisloadsubjandteachrs(searchData); 

};
assignSettings.displayGroupClsTeachers = function () {
    console.log('asd')
    console.log(assignSettings.cycleID)
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/displayGroupClsTeachers",
        data: {cycle: assignSettings.cycleID},
        success: function (res) {
            if (res) {
                $('#displayGroupClsTeachers').html(res);
                $('#displayGroupClsTeachers').removeClass('hide');
            }

        }
    });
}
//assign subject and teacher
assignSettings.loadSubjectsandTeachers = function (divval) {
    //alert(divval);
    var searchData = {};
    searchData['divvalid'] = $('#divID').val();

    if (searchData['divvalid'] != '')
    {
        $("#assignSubjsandTeachsForm").find('input,select').each(function () {
            if ($.trim($(this).val()) !== 0) {
                searchData[$(this).attr('name')] = $.trim($(this).val());
            } else {
                $('.modal-title').html('Message');
                $('.modal-body').html('Please Select a value! ');
                $('#messageBox').click();
                return false;
            }
        });

        console.log(searchData);
        $.ajax({
            type: "POST",
            url: commonSettings.assigningAjaxurl + "/loadSubjectsandTeachers",
            data: searchData,
            success: function (res) {
                if (res) {
                    $('#subjTechandteacherMatrix').html(res);
                    //$('#subjTechandteacherMatrix').removeClass('hide');
                }

            }
        });
        assignSettings.divvalwisloadsubjandteachrs(searchData);
    }
    else {
        assignSettings.loadoverallsubjandteachrs();
    }
};

assignSettings.createGroup = function (ctrl) {

    var subdivids = '';
    var subdivval = '';

    $("#sectiongroups").find('select').each(function () {

        var clss = $(this).attr('name');
        var clsss = $("#" + clss + " option:selected").text();
        if (clsss != 'Select')
        {
            subdivids = +$(this).val() + ',';
            subdivval = subdivval + clsss + '-';
        }

    });

    var division = $('#divvalname').val();

    if (subdivval != '')
    {
        subdivval = subdivval.substring(0, (subdivval.length - 1));

        subdivids = subdivids.substring(0, (subdivids.length - 1));
    }
    else
    {
        subdivids = 0;
    }

    var repeat = 0;
    $('.groupspan').each(function ()
    {
        var prevsubdivids = $(this).attr('group-id');
        //delete all the existing span if the subject is added to whole class
        if (subdivids == 0)
        {
            $(this).remove();
        }
        else if (subdivids === prevsubdivids)
        {
            repeat = 1;
            swal("Error", "Group Already Created!", "error");
        }
        else if (prevsubdivids == 0)
        {
            //condition to remove the whole class if group other than whole class is added.
            $(this).remove();

        }

    });



    if (subdivval != '')
    {
        var data = '<span group-id=' + subdivids + ' class="col-md-4 label label-primary fee_text groupspan">' + division + '-' + subdivval + '</span>';
    }
    else
    {
        var data = '<span group-id=' + subdivids + ' class="col-md-4 label label-primary fee_text groupspan">' + division + '</span>';
    }

    if (repeat === 0)
        $('#groupids').append(data);

    $('#subjTechandteacherMatrix').removeClass('hide');

}
assignSettings.updtSubjsandteachrs = function (ctrl) {
    var error = 0;
    var error_msg = '';
    var updtTeach = {};
    var masterid = $(ctrl).parents('tr').attr('masterid');
    var teachers = $(ctrl).parents('td').find('.staff-edit').val();
    if (teachers.length == 0) {
        error = 2;
        error_msg = error_msg + 'Teacher is missing, please select atleast one!' + '<br/>';
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error_msg + '</div>');
        $('#messageBox').click();
        $('#msg_ok').unbind('click').click(function () {
            $('#msg_ok').unbind('click').click();
            return false;
        });
    }
    if (error == 0) {

        updtTeach.masterid = masterid;
        updtTeach.teachers = teachers;
        console.log(updtTeach)

        $.ajax({
            type: "POST",
            url: commonSettings.assigningAjaxurl + "/updateSubjTeachers",
            data: updtTeach,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] === 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        else if (res['type'] === 'success') {
                            assignSettings.displayGroupSubjectTeachers()
                            assignSettings.loadGroupsSubjectTeachers(assignSettings.cycleID)
                        }
                    });

                }

            }
        });
    }

}
assignSettings.addSubjsandteachrs = function (ctrl) {
    var Data = {};

    var aggregate_key = '';
    var concat_subdivs = '';
    var error = 0;
    var error_msg = '';


    aggregate_key = $('#aggregate_key').val();



    if (aggregate_key == '')
    {
        error = 3;
        error_msg = error_msg + 'No Group Exist, Create Group To Begin With!' + '<br/>';
    }

    $("#addSubjsandTeachrs").find('input,select').each(function () {
        if ($(this).attr('id') === 'subjID')
        {
            //Subject select box processing
            if ($.trim($(this).val()) === '0') {
                error = 1;
                error_msg = error_msg + 'Subject is missing, please select!' + '<br/>';
            } else
            {
                Data[$(this).attr('id')] = $.trim($(this).val());
            }
        }
        else if ($(this).attr('id') === 'staff') {
            //Teacher select box processing
            var staff = $('#staff').val();
            if (staff.length == 0) {
                //If no teacher is selected
                error = 2;
                error_msg = error_msg + 'Teacher is missing, please select atleast one!' + '<br/>';
            } else
            {
                Data[$(this).attr('id')] = $.trim($(this).val());
            }

        }
        Data[$(this).attr('id')] = $.trim($(this).val());
    });
    Data['aggregate_key'] = aggregate_key;
    if (error === 1 || error === 2 || error === 3) {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error_msg + '</div>');
        $('#messageBox').click();
        $('#msg_ok').unbind('click').click(function () {
            $('#msg_ok').unbind('click').click();
            return false;
        });
    } else if (error === 0) {

        $.ajax({
            type: "POST",
            url: commonSettings.assigningAjaxurl + "/saveSubjtsandTeachers",
            data: Data,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] === 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        else if (res['type'] === 'success') {
                            assignSettings.loadGroupsSubjectTeachers(assignSettings.cycleID)
                            assignSettings.displayGroupSubjectTeachers()
                        }
                    });

                }

            }
        });


    }
};

assignSettings.addClsteachrs = function (ctrl) {
    var Data = {};
    var aggregate_key = '';
    var concat_subdivs = '';
    var error = 0;
    var error_msg = '';


    aggregate_key = $('#aggregate_key').val();



    if (aggregate_key == '')
    {
        error = 3;
        error_msg = error_msg + 'No Group Exist, Create Group To Begin With!' + '<br/>';
    }

    $("#addClsTeachrs").find('input,select').each(function () {
        if ($(this).attr('id') === 'staff') {
            //Teacher select box processing
            var staff = $('#staff').val();
            if (staff.length == 0) {
                //If no teacher is selected
                error = 2;
                error_msg = error_msg + 'Teacher is missing, please select atleast one!' + '<br/>';
            } else
            {
                Data[$(this).attr('id')] = $.trim($(this).val());
            }

        }
        Data[$(this).attr('id')] = $.trim($(this).val());
    });
    Data['aggregate_key'] = aggregate_key;
    if (error === 1 || error === 2 || error === 3) {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error_msg + '</div>');
        $('#messageBox').click();
        $('#msg_ok').unbind('click').click(function () {
            $('#msg_ok').unbind('click').click();
            return false;
        });
    } else if (error === 0) {

        $.ajax({
            type: "POST",
            url: commonSettings.assigningAjaxurl + "/saveClsTeachers",
            data: Data,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBoxModal').modal();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] === 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        else if (res['type'] === 'success') {
                            assignSettings.loadGroupsClsTeachers(assignSettings.cycleID)
                            assignSettings.displayGroupClsTeachers()
                        }
                    });

                }

            }
        });


    }
};

assignSettings.editSubjectsandTeachers = function (subj_serid) {
    console.log(subj_serid);
    var err = 0;
    assignSettings.hidDivId = $('#hidDivId').val();

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/editSubjtsandTeachers",
        data: 'subj_serid=' + subj_serid,
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Edit Teacher');
            $('.formBox-body').html(res);
            $('#modalForm').click();
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();


                if ($('#teacherId').val())
                {
                    assignSettings.editSaveSubjtsandTeachers(subj_serid, $.trim($('#teacherId').val()));
                }
                else
                {

                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in"> Select at least one Teacher </div>');
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        $('#msg_ok').unbind('click').click();
                        return false;
                    });
                }
            });

        }
    });
};

assignSettings.editSaveSubjtsandTeachers = function (subj_serid, teacherid) {
    console.log(subj_serid);
    console.log(teacherid);

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/editSaveSubjtsandTeachers",
        data: {
            subj_serid: subj_serid,
            teacherid: teacherid
        },
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                $('.modal-title').html('Message');
                $('.modal-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').click(function () {
                    //setTimeout(function(){window.location.href = window.location.pathname + window.location.search},0);

                    assignSettings.displayGroupSubjectTeachers();

                });
            }

        }
    });
};

//remove subject and teachers
assignSettings.removeSubjectsandTeachers = function (subj_serid) {
    console.log(subj_serid);
    var hidDivId = $('#hidDivId').val();
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/removeSubjtsandTeachers",
        data: 'subj_serid=' + subj_serid,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                $('.modal-title').html('Message');
                $('.modal-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').click(function () {
                    //alert(hidDivId);
                    //setTimeout(function(){window.location.href = window.location.pathname + window.location.search},0);
                    assignSettings.loadSubjectsandTeachers(hidDivId);
                });
            }

        }
    });
};

assignSettings.divvalwisloadsubjandteachrs = function (searchData) {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/divvalwiseloadSubjsandTeachs",
        data: searchData,
        success: function (res) {
            if (res) {
                $('#existsujectandteacherMatrix').html(res);
                $('#existsujectandteacherMatrix').removeClass('hide');
            }

        }
    });
}

assignSettings.displayGroupSubjectTeachers = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/displayGroupSubjectTeachers",
        data: {cycle: assignSettings.cycleID},
        success: function (res) {
            if (res) {
                $('#displayGroupSubjectTeachers').html(res);
                $('#displayGroupSubjectTeachers').removeClass('hide');
            }

        }
    });
}


assignSettings.loadClassTeacher = function (divval) {
    //alert(divval);
    var searchData = {};
    $("#assignSubjsandTeachsForm").find('input,select').each(function () {
        if ($.trim($(this).val()) !== 0) {
            searchData[$(this).attr('name')] = $.trim($(this).val());
        } else {
            $('.modal-title').html('Message');
            $('.modal-body').html('Please Select a value! ');
            $('#messageBox').click();
            return false;
        }
    });
    searchData['divvalid'] = divval;

    $('#initiate_page').val(1);

    console.log(searchData);
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/loadClassTeacher",
        data: searchData,
        success: function (res) {
            if (res) {
                $('#clsteacherMatrix').html(res);
                $('#clsteacherMatrix').removeClass('hide');
            }

        }
    });
    if (searchData['divID'] !== '0') {
        assignSettings.divvalwiseLoadClassTeacher(searchData);
    } else {
        assignSettings.LoadoverallClassTeacher();
    }
};

assignSettings.saveClassTeacher = function (ctrl) {
    var Data = {};
    var concat_subdivs = '';
    var error = 0;
    var error_msg = '';
    var ack_msg = '';

    ack_msg = ack_msg + $('#divname').val() + ' ' + $('#divvalname').val();

    $("#addSubjsandTeachrs").find('input,select').each(function () {
        if ($(this).attr('name') === 'Subdiv[]') {
            var subdivvalength = $(this).attr('count');
            var subdivname = $(this).attr('subdivname');
            //alert(subdivname);
            //validation 
            if (subdivvalength > 0) {
                if ($.trim($(this).val()) != '0') {
                    //alert($(this).find( "option:selected" ).text());
                    ack_msg = ack_msg + ' - ' + subdivname + ' ' + $(this).find("option:selected").text();
                }
            }

            if ($.trim($(this).val()) !== '0') {
                concat_subdivs = concat_subdivs + $.trim($(this).val()) + ',';
            }
        } else {
            if ($(this).attr('name') === 'teacherId') {
                //Teacher select box processing
                if (!($(this).val())) {
                    error = 1;
                    error_msg = error_msg + 'Teacher is missing, please select atleast one!' + '<br/>';
                } else
                {
                    Data[$(this).attr('name')] = $.trim($(this).val());
                }
            }
            Data[$(this).attr('name')] = $.trim($(this).val());
        }

    });
    Data['concat_subdivs'] = concat_subdivs;
    if (error === 1) {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error_msg + '</div>');
        $('#messageBox').click();
        $('#msg_ok').unbind('click').click(function () {
            $('#msg_ok').unbind('click').click();
            return false;
        });
    } else if (error === 0) {

        ack_msg = 'You Are Trying To Assign ' + $('#divname').val() + ' Teacher For ' + ack_msg;
        $('.cnfBox-title').html('Confirmation');
        $('.cnfBox-body').html(ack_msg);
        $('#confirmMsg').click();
        //console.log('1clciked')
        $('#confirm_yes').unbind('click').click(function () {
            $('#confirm_no').click();
            $.ajax({
                type: "POST",
                url: commonSettings.assigningAjaxurl + "/saveClassTeacher",
                data: Data,
                dataType: "JSON",
                success: function (res) {
                    if (res) {
                        console.log(res)
                        console.log(res.type)
                        $('.msgBox-title').html('Message');
                        $('.msgBox-body').html(res['message']);
                        $('#messageBox').click();
                        $('#msg_ok').unbind('click').click(function () {
                            if (res['type'] === 'error') {
                                $('#msg_ok').unbind('click').click();
                                return false;
                            }
                            else if (res['type'] === 'success') {
                                //reload a page
                                var hidDivId = $('#hidDivId').val();
                                assignSettings.loadClassTeacher(hidDivId);
                                //setTimeout(function(){window.location.href = window.location.pathname + window.location.search},0);
                            }
                        });

                    }

                }
            });
            // }
        });

    }
};

//remove subject and teachers
assignSettings.removeClassTeacher = function (subj_serid) {
    console.log(subj_serid);
    var hidDivId = $('#hidDivId').val();
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/removeClassTeacher",
        data: 'subj_serid=' + subj_serid,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                $('.modal-title').html('Message');
                $('.modal-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').click(function () {
                    //setTimeout(function(){window.location.href = window.location.pathname + window.location.search},0);
                    assignSettings.loadClassTeacher(hidDivId);
                });
            }

        }
    });
};

assignSettings.editClassTeacher = function (subj_serid) {
    console.log(subj_serid);
    var err = 0;
    assignSettings.hidDivId = $('#hidDivId').val();

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/editClassTeacher",
        data: 'subj_serid=' + subj_serid,
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Edit Teacher');
            $('.formBox-body').html(res);
            $('#modalForm').click();
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();


                if ($('#teacherId').val())
                {
                    assignSettings.editSaveClassTeacher(subj_serid, $.trim($('#teacherId').val()));
                }
                else
                {

                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in"> Select at least one Teacher </div>');
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        $('#msg_ok').unbind('click').click();
                        return false;
                    });
                }
            });

        }
    });
};

assignSettings.editSaveClassTeacher = function (subj_serid, teacherid) {
    console.log(subj_serid);
    console.log(teacherid);

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/editSaveClassTeacher",
        data: {
            subj_serid: subj_serid,
            teacherid: teacherid
        },
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                $('.modal-title').html('Message');
                $('.modal-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').click(function () {
                    //setTimeout(function(){window.location.href = window.location.pathname + window.location.search},0);

                    assignSettings.LoadoverallClassTeacher();

                });
            }

        }
    });
};

assignSettings.LoadoverallClassTeacher = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/loadoverallClassTeacher",
        success: function (res) {
            if (res) {
                $('#existclsteacherMatrix').html(res);
                $('#existclsteacherMatrix').removeClass('hide');
            }

        }
    });
}

assignSettings.divvalwiseLoadClassTeacher = function (searchData) {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/divvalwiseLoadClassTeacher",
        data: searchData,
        success: function (res) {
            if (res) {
                $('#existclsteacherMatrix').html(res);
                $('#existclsteacherMatrix').removeClass('hide');
            }

        }
    });
};
assignSettings.loadClassteacher = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/assignClassTeachers",
        dataType: "html",
        success: function (res) {
            $('#subjectteacherDiv').html('');
            $('#addsubjectDiv').html('');
            $('#classteacherDiv').html(res);
            $('#classteacherDiv').removeClass('hide')
        }
    });
};


assignSettings.loadsubjectTeacher = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/assignSubjectandTeacher",
        dataType: "html",
        success: function (res) {
            $('#classteacherDiv').html('');
            $('#addsubjectDiv').html('');
            $('#subjectteacherDiv').html(res);
            $('#subjectteacherDiv').removeClass('hide')
        }
    });
};
assignSettings.loadaddsubject = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/addSubjectsByClass",
        dataType: "html",
        success: function (res) {
            $('#classteacherDiv').html('');
            $('#classteacherDiv').html('');
            $('#addsubjectDiv').html(res);
            $('#addsubjectDiv').removeClass('hide')
        }
    });
};

assignSettings.loadSpaceTreeForAllSubject = function (cycle, node_id, node_name) {
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectspacetree = 0;
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/spacetree",
        dataType: "html",
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Choose a Node');
            $('.formBox-body').html(res);
            $('#formModal').modal();

            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                var id = '"' + $('#' + node_id).val() + '"';


                $('#returned_node_nameall').html($('#' + node_name).val());
                var app = "<a href='javascript:void(0)' class='' onclick='assignSettings.createSubject(" + id + ")' title='Add Subject'>" +
                        "<i class='fa fa-plus activity-icon bg-red' style='margin-top: 6px;'></i></a>"
                $('#returned_node_nameall').append(app);
                $('#added_category').removeClass('hide');
            });
        }
    });
};
assignSettings.loadStuCountnSubj = function (nodeid) {
    var loading = $('.loading');
    var done = $('.done');
    if (loading && done) {
        loading.removeClass('hide')
        done.addClass('hide')
    }
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/getStudentCount",
        data: {node_id: nodeid},
        success: function (res) {
            if (loading && done) {
                loading.addClass('hide')
                done.removeClass('hide')
            }
            $('#common_subjects').html(res);
        }
    });
}
//assignSettings.loadSpaceTreeForSubject = function (cycle, node_id, node_name) {
//    spacetreeutilsSettings.cycle = cycle;
//    spacetreeutilsSettings.node_id = node_id;
//    spacetreeutilsSettings.node_name = node_name;
//    spacetreeutilsSettings.loadspacetree = 1;
//    spacetreeutilsSettings.multiselectspacetree = 0;
//    $.ajax({
//        type: "POST",
//        url: commonSettings.assigningAjaxurl + "/spacetree",
//        dataType: "html",
//        success: function (res) {
//            $('#msg_ok').unbind('click').click();
//            $('#confirm_close').unbind('click').click();
//            $('.formBox-title').html('Choose a Node');
//            $('.formBox-body').html(res);
//            $('#formModal').modal();
//
//            $('#confirm_save').unbind('click').click(function () {
//                $('#confirm_close').click();
//                var id = '"' + $('#' + node_id).val() + '"';
//
//
//                $('#returned_node_nameall').html($('#' + node_name).val());
//                var app = "<a href='javascript:void(0)' class='' onclick='assignSettings.createSubject(" + id + ")' title='Add Subject'>" +
//                        "<i class='fa fa-plus activity-icon bg-red' style='margin-top: 6px;'></i></a>"
//                $('#returned_node_nameall').append(app);
//                $('#added_category').removeClass('hide');
//            });
//        }
//    });
//};
//assignSettings.loadSpaceTreeForSubjectTeachers = function (cycle, node_id, node_name) {
//    spacetreeutilsSettings.cycle = cycle;
//    spacetreeutilsSettings.node_id = node_id;
//    spacetreeutilsSettings.node_name = node_name;
//    spacetreeutilsSettings.loadspacetree = 1;
//    spacetreeutilsSettings.multiselectspacetree = 1;
//    spacetreeutilsSettings.fromAssigning=1;
//    $.ajax({
//        type: "POST",
//        url: commonSettings.spacetreeutilsAjaxurl + "/spacetreeForSubjTeach",
//        dataType: "html",
//        success: function (res) {
//            $('#msg_ok').unbind('click').click();
//            $('#confirm_close').unbind('click').click();
//            $('.formBox-title').html('Choose a Node');
//            $('.formBox-body').html(res);
//            $('#formModal').modal();
//
//            $('#confirm_save').unbind('click').click(function () {
//                $('#confirm_close').click();
//                var id = '"' + $('#' + node_id).val() + '"';
//
//                $('#returned_node_name').html($('#' + node_name).val());
//                $('#added_category').removeClass('hide');
//
//                if ($('#' + node_id).val() == '')
//                {
//                    //No Node is selected
//                    alert('No Nodes has been selected');
//                }
//                else
//                {
//                    //Get the count of students
//
//                    //Get the subjects
//
//                    $.ajax({
//                        type: "POST",
//                        url: commonSettings.assigningAjaxurl + "/getStudentCountAndSubjects",
//                        data: {node_id: $('#' + node_id).val()},
//                        success: function (res) {
//                            $('#common_subjects').html(res);
//                        }
//                    });
//                }
//            });
//        }
//    });
//};
//
//assignSettings.loadSpaceTreeForClassTeachers = function (cycle, node_id, node_name) {
//    spacetreeutilsSettings.cycle = cycle;
//    spacetreeutilsSettings.node_id = node_id;
//    spacetreeutilsSettings.node_name = node_name;
//    spacetreeutilsSettings.loadspacetree = 1;
//    spacetreeutilsSettings.multiselectspacetree = 1;
//    spacetreeutilsSettings.fromAssigning=1;
//    $.ajax({
//        type: "POST",
//        url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
//        dataType: "html",
//        success: function (res) {
//            $('#msg_ok').unbind('click').click();
//            $('#confirm_close').unbind('click').click();
//            $('.formBox-title').html('Choose a Node');
//            $('.formBox-body').html(res);
//            $('#formModal').modal();
//
//            $('#confirm_save').unbind('click').click(function () {
//                $('#confirm_close').click();
//                var id = '"' + $('#' + node_id).val() + '"';
//
//                $('#returned_node_name').html($('#' + node_name).val());
//                $('#added_category').removeClass('hide');
//
//                if ($('#' + node_id).val() == '')
//                {
//                    //No Node is selected
//                    alert('No Nodes has been selected');
//                }
//                else
//                {
//                    //Get the count of students
//
//                    //Get the subjects
//
//                    $.ajax({
//                        type: "POST",
//                        url: commonSettings.assigningAjaxurl + "/getStudentCount",
//                        data: {node_id: $('#' + node_id).val()},
//                        success: function (res) {
//                            $('#common_subjects').html(res);
//                        }
//                    });
//                }
//            });
//        }
//    });
//};