var assignSectionSettings = {};
assignSectionSettings.serarchAggregate = {};
assignSectionSettings.loadStudents = function () {
    var searchData = {};
    var err = 1;
    $("#assignSecSearchForm").find('select').each(function () {
        if ($.trim($(this).val()) == '0') {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('Please select a value');
            $('#messageBox').click();
            err = 0;
            return false;
        } else {
            searchData[$(this).attr('name')] = $.trim($(this).val());

        }
    });
    searchData.err = err;
    assignSectionSettings.serarchAggregate = searchData;
    assignSectionSettings.loadStudentsDiv(searchData)

};
assignSectionSettings.loadStudentsOnboard = function () {
    var searchData = {};
    var err = 0, error = '';
    $("#assignSecSearchForm").find('input,select,textarea').each(function () {
        if ($(this).attr('includekey') == '1') {
            if ($(this).parent().prev('label').hasClass('mandatory')) {
                if ($.trim($(this).val()) == '') {
                    err = 1;
                    error += $(this).attr('title') + " is required!\n";
                } else {
                    searchData[$(this).attr('name')] = $.trim($(this).val());
                }
            } else {
                searchData[$(this).attr('name')] = $.trim($(this).val());
            }
        }
    });
    if (err == 0) {
        assignSectionSettings.serarchAggregate = searchData;
        $.ajax({
            type: "POST",
            url: commonSettings.onboardUrl + "/loadStudentsOnboard",
            data: searchData,
            success: function (res) {
                if (res) {
                    $('#StudentDet').html('');
                    $('#StudentDet').removeClass('hide')
                    $('#StudentDet').html(res);
                }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
        return false;
    }

};
assignSectionSettings.loadStudentsDiv = function (searchData) {
    if (searchData.err == 1) {
        $.ajax({
            type: "POST",
            url: commonSettings.assigningAjaxurl + "/loadStudents",
            data: searchData,
            success: function (res) {
                if (res) {
                    $('#StudentDet').html('');
                    $('#StudentDet').removeClass('hide')
                    $('#StudentDet').html(res);
                }

            }
        });
    }
}

assignSectionSettings.saveSection = function (ctrl) {
    var stuSecData = {};
    stuSecData.id = $(ctrl).closest('tr').attr('stuId');
    stuSecData.subDivId = $(ctrl).val();
    stuSecData.acyrID = $(ctrl).closest('tr').attr('acyrID');
    stuSecData.divValId = $(ctrl).closest('tr').attr('divValId');
    $('.loading_' + stuSecData.id).removeClass('hide')
    console.log(stuSecData)
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/saveStudentsSubDiv",
        data: stuSecData,
        dataType: 'JSON',
        success: function (res) {
            if (res) {

                console.log(res)
                $('.message_' + stuSecData.id).html('');
                $('.message_' + stuSecData.id).removeClass('hide')
                $('.message_' + stuSecData.id).html(res['message']);
                $('.loading_' + stuSecData.id).addClass('hide')

                setTimeout(function () {
                    var searchData = {};
                    searchData.academicYrId = stuSecData.acyrID;
                    searchData.divID = stuSecData.divValId;
                    searchData.err = 1;
                    assignSectionSettings.loadStudents(searchData)
                }, 1000);

            }

        }
    });


};


assignSectionSettings.saveRollNo = function (ctrl) {
    var stuSecData = {};
    stuSecData.id = $(ctrl).closest('tr').attr('stuId');
    stuSecData.rollno = $(ctrl).val();
    stuSecData.acyrID = $(ctrl).closest('tr').attr('acyrID');
    stuSecData.divValId = $(ctrl).closest('tr').attr('divValId');
    $('.loading1_' + stuSecData.id).removeClass('hide')
    console.log(stuSecData)
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/saveStudentRollno",
        data: stuSecData,
        dataType: 'JSON',
        success: function (res) {
            if (res) {

                console.log(res)
                $('.message1_' + stuSecData.id).html('');
                $('.message1_' + stuSecData.id).removeClass('hide')
                $('.message1_' + stuSecData.id).html(res['message']);
                $('.loading1_' + stuSecData.id).addClass('hide')

                setTimeout(function () {
                    var searchData = {};
                    searchData.academicYrId = stuSecData.acyrID;
                    searchData.divID = stuSecData.divValId;
                    searchData.err = 1;
                    assignSectionSettings.loadStudents(searchData)
                }, 1000);

            }

        }
    });


};


//save section and rollno to students
assignSectionSettings.addesectandrollno = function (stud_id, stud_name, acadmicyrid, opeartion) {
    var Data = {};
    var concat_subdivs = '';
    var error = 0;
    var error_msg = '';
    var ack_msg = '';
    ack_msg = ack_msg + stud_name + ' ' + $('#divname').val() + ' ' + $('#divvalname').val();

    $("#assignSecrollnoForm").find('input,select').each(function () {
        if ($(this).attr('name') === 'subDivVal[]' && $(this).attr('stud_id') === stud_id) {
            var subdivvalength = $(this).attr('count');
            var subdivname = $(this).attr('subdivname');

            //validation 
            if (subdivvalength > 0) {
                if ($.trim($(this).val()) === '0') {
                    error = 1;
                    error_msg = error_msg + stud_name + ' ' + subdivname + ' is missing, please select!' + '<br/>';
                    $(this).focus();
                }
            }

            if ($.trim($(this).val()) !== '0') {
                concat_subdivs = concat_subdivs + $.trim($(this).val()) + ',';
            }
        } else {
            if ($(this).attr('name') === 'RollNo' && $(this).attr('stud_id') === stud_id)
            {
                if ($.trim($(this).val()) === '') {
                    error = 2;
                    error_msg = error_msg + 'RollNo is missing, please Enter!' + '<br/>';
                } else
                {
                    Data[$(this).attr('name')] = $.trim($(this).val());
                }
            }
        }
    });
    Data['stud_id'] = stud_id;
    Data['acadmicyrid'] = acadmicyrid;
    Data['concat_subdivs'] = concat_subdivs;
    if (error === 1 || error === 2) {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error_msg + '</div>');
        $('#messageBox').click();
        $('#msg_ok').unbind('click').click(function () {
            $('#msg_ok').unbind('click').click();
            return false;
        });
    }
    else if (error === 0) {
        //ack_msg = 'You Are Trying To Assign Section and Roll No For ' + ack_msg;
        //  $('.cnfBox-title').html('Confirmation');
        //  $('.cnfBox-body').html(ack_msg);
        //$('#confirmMsg').click();
        // $('#confirm_yes').unbind('click').click(function() {
        //  $('#confirm_no').click();
        if (opeartion === 'update' || opeartion === 'insert') {
            $.ajax({
                type: "POST",
                url: commonSettings.assigningAjaxurl + "/checkrollnoexists",
                data: Data,
                dataType: "JSON",
                success: function (res) {
                    if (res) {
                        console.log(res.type);
                        if (res['type'] === 'error') {
                            $('.msgBox-title').html('Message');
                            $('.msgBox-body').html(res['message']);
                            $('#messageBox').click();
                            $('#msg_ok').unbind('click').click(function () {
                                if (res['type'] === 'error') {
                                    $('#msg_ok').unbind('click').click();
                                    //assignSectionSettings.loadStudents();
                                    var extrollno = $('#RollNo_' + stud_id).attr('value');
                                    $('#RollNo_' + stud_id).val(extrollno);
                                    $('.' + stud_id + '_action_data').html('Added');

                                    return false;
                                }
                            });
                        }
                        else if (res['type'] === 'success') {
                            $.ajax({
                                type: "POST",
                                url: commonSettings.assigningAjaxurl + "/savesectionandrollno",
                                data: Data,
                                dataType: "JSON",
                                success: function (res) {
                                    if (res) {
                                        console.log(res)
                                        console.log(res.type)
                                        //$('.msgBox-title').html('Message');
                                        // $('.msgBox-body').html(res['message']);
                                        // $('#messageBox').click();
                                        //$('#msg_ok').unbind('click').click(function() {
                                        if (res['type'] === 'error') {
                                            $('#msg_ok').unbind('click').click();
                                            return false;
                                        }
                                        else if (res['type'] === 'success') {
                                            //assignSectionSettings.loadStudents();
                                            $('.' + stud_id + '_action_data').html('Added');
                                        }
                                        // });

                                    }

                                }
                            });
                        }

                    }
                }
            });
        }


        //});

    }
};

//generate roll no for students
assignSectionSettings.genstudrollno = function (studcount) {
    var rollformat = $('#rollno_format').val();
    var rollnextval = $('#rollstartval').val();
    var rollnostart = rollnextval.replace(/^0+/, '');
    var error = 0;
    var error_msg = '';
    var prefix = rollformat.replace(/\*/g, '');
    var asterikcount = countInstances(rollformat, '*');
    //alert(rollnostart);
    var err = 0;
    if (rollformat === '' || rollformat === '0') {
        swal("Error", 'Please enter RollNo Format value', "error");
        err = 1;
        return false;
    }
    if (err === 0) {
//        $('#dynamicStuList').find('select').each(function () {
//            if ($(this).attr('name') === 'subDivVal[]') {
//                var subdivvalength = $(this).attr('count');
//                var subdivname = $(this).attr('subdivname');
//
//                //validation 
//                if (subdivvalength > 0) {
//                    if ($.trim($(this).val()) === '0') {
//                        error = 1;
//                        error_msg = error_msg + subdivname + ' is missing, please select!' + '<br/>';
//
//                    }
//                }
//
//            }
//        });
        if (error === 1) {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error_msg + '</div>');
            $('#messageBox').click();
            $('#msg_ok').unbind('click').click(function () {
                $('#msg_ok').unbind('click').click();
                return false;
            });
        } else if (error === 0) {
            for (var i = 0; i < studcount; i++) {
                var rollno = rollnostart;
                var rollnoval = pad(rollno, asterikcount);
                var finalrollno = prefix + rollnoval;
                $('#dynamicStuList tr td .rollno:eq(' + i + ')').val(finalrollno);
                var rollnostart = parseInt(rollnostart) + 1;
            }
        }
    }
};



assignSectionSettings.get_rollnostartval = function (ctrl) {
    var rollformat = $(ctrl).val();
    var err = 0;
    if (rollformat === '' || rollformat === '0') {
        swal("Error", 'Please enter RollNo Format value', "error");
        err = 1;
        return false;
    }
    if (err === 0) {
        var prefix = rollformat.replace(/\*/g, '');
        var asterikcount = countInstances(rollformat, '*');
        var rollno = pad('1', asterikcount);
        $('#rollstartval').val(rollno);
        var prefixval = prefix + rollno;
        $('#rollnostart_val').val(prefixval)
    }
};
assignSectionSettings.change_rollnostartval = function (ctrl) {
    var startrollno = $(ctrl).val();
    var rollformat = $('#rollno_format').val();
    var prefix = rollformat.replace(/\*/g, '');
    var startval = startrollno.replace(prefix, '');
    $('#rollstartval').val(startval);
}

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}
function countInstances(string, word) {
    var substrings = string.split(word);
    return substrings.length - 1;
}

assignSectionSettings.upadterollno = function (ctrl) {
    var extrollno = $(ctrl).attr('value');
    var stud_id = $(ctrl).attr('stud_id');
    var stud_nm = $(ctrl).attr('stud_nm');
    var yearid = $(ctrl).attr('yearid');
    if (extrollno !== '') {
        var html = '<input type="button" id="addsubj_1" onclick="assignSectionSettings.addesectandrollno(\'' + stud_id + '\',\'' + stud_nm + '\',\'' + yearid + '\',\'update\')" value="Update" name="assignSubj" class="btn btn-round btn-primary">';
        $('.' + stud_id + '_action_data').html(html);
    }
};
assignSectionSettings.loadSubtree = function (ctrl) {
    var orgvalueid = $(ctrl).val();
    $(ctrl).parent().parent().next('.subtreediv')
            .removeClass('hide')
            .html(' <div class="form-group">  <span class="col-sm-8 pull-right help-block"><i class="fa fa-spin fa-spinner" ></i> Loading please wait...</span>  </div>')
    $('#stu_list').addClass('hide');
    assignSectionSettings.loadsubtree = $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/loadSubtree",
        data: {orgvalueid: orgvalueid},
        beforeSend: function () {
            if (assignSectionSettings.loadsubtree != null) {
                assignSectionSettings.loadsubtree.abort()
            }
        },
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
            $('#stu_list').removeClass('hide');
        }
    });

};

//save section and rollno to students
assignSectionSettings.updateRollno = function (ctrl) {
    var rollnoData = {};
    rollnoData.stuid = $(ctrl).closest('tr').attr('stuId');
    rollnoData.rollno = $(ctrl).closest('tr').find('input.rollno').val()
    var stud_name = $(ctrl).closest('tr').attr('stuname')
    var error_msg = '';
    var aggregate = new Array();
    $(ctrl).closest('tr').find('select').each(function () {
//        console.log($(this).val())
        if ($(this).val() === '') {
            error_msg = error_msg + stud_name + ' ' + $(this).attr('divname') + ' is missing, please select!' + '<br/>';
        } else {
            aggregate.push($(this).val());
        }


    });
    if (error_msg) {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error_msg + '</div>');
        $('#messageBox').click();
        $('#msg_ok').unbind('click').click(function () {
            $('#msg_ok').unbind('click').click();
            return false;
        });
    } else {
        rollnoData.aggregateKey = aggregate.join(',')
        rollnoData.action = $(ctrl).attr('action');
        rollnoData.managgre = assignSectionSettings.serarchAggregate;
        console.log(rollnoData)
        var error = 0;
        if (rollnoData.action === 'update' || rollnoData.action === 'insert') {
            if (rollnoData.rollno != '') {
                assignSectionSettings.checkIfRollNoExists(rollnoData)
            } else {
                assignSectionSettings.savechanges(rollnoData)
            }
        }
    }
};

assignSectionSettings.checkIfRollNoExists = function (rollnoData) {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/checkrollnoexists",
        data: rollnoData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                if (res['type'] === 'error') {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] === 'error') {
                            $('#msg_ok').unbind('click').click();
                            //assignSectionSettings.loadStudents();
                            var extrollno = $('#RollNo_' + rollnoData.stuid).attr('value');
                            $('#RollNo_' + rollnoData.stuid).val(extrollno);
//                            $('.' + rollnoData.stuid + '_action_data').html('Added');

                            return false;
                        }
                    });
                }
                else if (res['type'] === 'success') {
                    assignSectionSettings.savechanges(rollnoData)
                }
            }
        }
    });
}
assignSectionSettings.savechanges = function (rollnoData) {

    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/savesectionandrollno",
        data: rollnoData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                //$('.msgBox-title').html('Message');
                // $('.msgBox-body').html(res['message']);
                // $('#messageBox').click();
                //$('#msg_ok').unbind('click').click(function() {
                if (res['type'] === 'error') {
                    $('#msg_ok').unbind('click').click();
                    return false;
                }
                else if (res['type'] === 'success') {
                    //assignSectionSettings.loadStudents();
                    $('.' + rollnoData.stuid + '_action_data').html('Added');
//                   assignSectionSettings.loadStudents()
                }
                // });

            }

        }
    });
}

//assignSectionSettings.updateRollno = function (ctrl) {
//    var rollnoData = {};
//    rollnoData.stuid = $(ctrl).closest('tr').attr('stuId');
//    rollnoData.rollno = $(ctrl).closest('tr').find('input.rollno').val()
//    rollnoData.aggregateKey = $(ctrl).closest('tr').find('input.aggrgate_val').val()
//    rollnoData.action = $(ctrl).attr('action');
//    console.log(rollnoData)
//    var error = 0;
//    if (rollnoData.rollno == '') {
//        $('.msgBox-title').html('Message');
//        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">Rollno field cannot be empty</div>');
//        $('#messageBox').click();
//        $('#msg_ok').unbind('click').click(function () {
//            $('#msg_ok').unbind('click').click();
//            return false;
//        });
//        error = 1;
//
//    }
//
//
//    else if (error === 0) {
//        if (rollnoData.action === 'update' || rollnoData.action === 'insert') {
//            $.ajax({
//                type: "POST",
//                url: commonSettings.assigningAjaxurl + "/checkrollnoexists",
//                data: rollnoData,
//                dataType: "JSON",
//                success: function (res) {
//                    if (res) {
//                        console.log(res.type);
//                        if (res['type'] === 'error') {
//                            $('.msgBox-title').html('Message');
//                            $('.msgBox-body').html(res['message']);
//                            $('#messageBox').click();
//                            $('#msg_ok').unbind('click').click(function () {
//                                if (res['type'] === 'error') {
//                                    $('#msg_ok').unbind('click').click();
//                                    //assignSectionSettings.loadStudents();
//                                    var extrollno = $('#RollNo_' + rollnoData.stuid).attr('value');
//                                    $('#RollNo_' + rollnoData.stuid).val(extrollno);
//                                    $('.' + rollnoData.stuid + '_action_data').html('Added');
//
//                                    return false;
//                                }
//                            });
//                        }
//                        else if (res['type'] === 'success') {
//                            $.ajax({
//                                type: "POST",
//                                url: commonSettings.assigningAjaxurl + "/savesectionandrollno",
//                                data: rollnoData,
//                                dataType: "JSON",
//                                success: function (res) {
//                                    if (res) {
//                                        console.log(res)
//                                        console.log(res.type)
//                                        //$('.msgBox-title').html('Message');
//                                        // $('.msgBox-body').html(res['message']);
//                                        // $('#messageBox').click();
//                                        //$('#msg_ok').unbind('click').click(function() {
//                                        if (res['type'] === 'error') {
//                                            $('#msg_ok').unbind('click').click();
//                                            return false;
//                                        }
//                                        else if (res['type'] === 'success') {
//                                            //assignSectionSettings.loadStudents();
//                                            $('.' + rollnoData.stuid + '_action_data').html('Added');
//                                        }
//                                        // });
//
//                                    }
//
//                                }
//                            });
//                        }
//
//                    }
//                }
//            });
//        }
//
//
//        //});
//
//    }
//};