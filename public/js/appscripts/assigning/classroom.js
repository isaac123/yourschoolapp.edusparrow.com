var classroomAssignSettings = {};
classroomAssignSettings.openValue = function (classmasid) {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/openValues",
        dataType: 'HTML',
        data: {
            classmasid: classmasid
        },
        success: function (res) {
            $('#loadStaffIncharge').html(res)
            classroomAssignSettings.loadStafStu(classmasid);
        }
    });
};
classroomAssignSettings.closepopover = function (ctrl) {
    $('[data-toggle="popover"]').popover('hide')

};
classroomAssignSettings.addOrEditClassroom = function (ctrl) {
    var orgValues = {};
    var err = 0;
    $('#addnewClassroom').find('input').each(function () {
        if ($.trim($(this).val()) == '') {
            console.log($(this).parent().html())
            err = 1;

            $('.msgBox-title').html('<i class="fa  fa-times"></i> Error');
            $('.msgBox-body').html('<span class="text text-danger fade in">' + $(this).attr('title') + ' is required' + '</span>');
            $('#messageBoxModal').modal('show');
            return false;
        }
        orgValues[$(this).attr('name')] = $.trim($(this).val());
    });
    console.log(orgValues)
    orgValues['actions'] = $(ctrl).attr('action');
    console.log(orgValues)
    if (err == 0) {

        if (orgValues.actions == 'delete') {
            $('.cnfBox-title').html('Confirm Delete');
            $('.cnfBox-body').html('Deleting this classroom will also delete related entries.<br> Do you confirm to delete Classroom?');
            $('#ConfirmModal').modal();
            $('#confirm_yes').unbind('click').click(function (event) {
                $('#ConfirmModal').modal('hide');
                classroomAssignSettings.addOrEditClassroomCofirm(orgValues);
            });
        } else {
            classroomAssignSettings.addOrEditClassroomCofirm(orgValues);
        }
    }
};
classroomAssignSettings.addOrEditClassroomCofirm = function (orgValues) {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/addOrEditClassroom",
        dataType: 'JSON',
        data: orgValues,
        success: function (res) {
            if (res) {
                var lblcls = res.type == 'error' ? 'danger' : 'success';
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                $('#messageBoxModal').modal();
                $('#msg_ok').unbind('click').click(function (event) {
                    $('#messageBoxModal').modal('hide');
                    if (res.message == 'Deleted successfully') {
                        classroomAssignSettings.allClassroom();
                        classroomAssignSettings.addnewClassroom();
                    } else {
                        classroomAssignSettings.allClassroom();
                        classroomAssignSettings.openValue(res.id);
                    }
                });
                return false;

            }

        }
    });
};
classroomAssignSettings.loadStafStu = function (grpsubjid) {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/loadStafStu",
        dataType: 'HTML',
        data: {
            classmasterid: grpsubjid
        },
        success: function (res) {
            $('#loadstaffandstu').html(res)
        }
    });
};

classroomAssignSettings.addStaffIncharge = function (ctrl) {
    var orgValues = {};
    var err = 0;
    $(ctrl).closest('form').find('input').each(function () {
        if ($.inArray($(this).attr('name'), ["staff_list", "classroomid"]) > -1) {
            if ($.trim($(this).val()) == '') {
                console.log($(this).parent().html())
                swal({
                    title: "Error",
                    text: $(this).attr('title') + ' is required',
                    type: 'error',
                    confirmButtonText: 'OK!',
                    closeOnConfirm: true,
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                return false;
                                err = 1;
                            }
                        });
            }
            orgValues[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    console.log(orgValues)
    orgValues['actions'] = $(ctrl).attr('action');
    console.log(orgValues)
    if (err == 0) {

        if (orgValues.actions == 'delete') {
            $('.cnfBox-title').html('Confirm Delete');
            $('.cnfBox-body').html('Deleting this classroom will also delete related entries.<br> Do you confirm to delete Classroom?');
            $('#ConfirmModal').modal();
            $('#confirm_yes').unbind('click').click(function (event) {
                $('#ConfirmModal').modal('hide');
                classroomAssignSettings.addStaffInchargeConfirm(orgValues);
            });
        } else {
            classroomAssignSettings.addStaffInchargeConfirm(orgValues);
        }
    }
};
classroomAssignSettings.addStaffInchargeConfirm = function (orgValues) {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/addStaffInchargeConfirm",
        dataType: 'JSON',
        data: orgValues,
        success: function (res) {
            if (res) {
var cls = (res.type == "success")?'success':'danger';
			var msg ='<span class="text text-'+cls+' fade in">' +  res.message + '</span>'
 			bootbox.dialog({
                                    title: res.type.toUpperCase(),
                                    message: msg,
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn-success",
                                            callback: function () {
                                                 classroomAssignSettings.openValue(orgValues.classroomid)
                                            }
                                        }
                                    }
                                });
            }

        }
    });
};
classroomAssignSettings.addSubjTeach = function (ctrl) {
//    alert('sd')
    var orgValues = {};
    var err = 0;
    $(ctrl).closest('form').find('input,select').each(function () {
        if ($.inArray($(this).attr('name'), ["staff_list", "subjclasid", "subjID"]) > -1) {
            if ($.trim($(this).val()) == '') {
                err = 1;
//        console.log($(this).parent().html())
                swal({
                    title: "Error",
                    text: $(this).attr('title') + ' is required',
                    type: 'error',
                    confirmButtonText: 'OK!',
                    closeOnConfirm: true,
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                return false;

                            }
                        });
            }
            orgValues[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    orgValues['actions'] = $(ctrl).attr('action');
    console.log(orgValues)
    if (err == 0) {

        if (orgValues.actions == 'delete') {
            $('.cnfBox-title').html('Confirm Delete');
            $('.cnfBox-body').html('Deleting this classroom will also delete related entries.<br> Do you confirm to delete Classroom?');
            $('#ConfirmModal').modal();
            $('#confirm_yes').unbind('click').click(function (event) {
                $('#ConfirmModal').modal('hide');
                classroomAssignSettings.addSubjTeachConfirm(orgValues);
            });
        } else {
            classroomAssignSettings.addSubjTeachConfirm(orgValues);
        }
    }
};
classroomAssignSettings.addSubjTeachConfirm = function (orgValues) {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/addSubjTeachConfirm",
        dataType: 'JSON',
        data: orgValues,
        success: function (res) {
            if (res) {
//                if (res.type == 'error') {
			var cls = (res.type == "success")?'success':'danger';
			var msg ='<span class="text text-'+cls+' fade in">' +  res.message + '</span>'
 			bootbox.dialog({
                                    title: res.type.toUpperCase(),
                                    message: msg,
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn-success",
                                            callback: function () {
                                                 classroomAssignSettings.openValue(orgValues.subjclasid)
                                            }
                                        }
                                    }
                                });
             
            }
//            }

        }
    });
};
classroomAssignSettings.removeClassTeachers = function (teacherid, clsmasid) {
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html('Do you want to delete this Class Teacher and also the related entries ?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.assigningAjaxurl + "/removeClassTeachers",
            dataType: 'JSON',
            data: {
                teacherid: teacherid,
                classmasterid: clsmasid
            },
            success: function (res) {
                if (res) {
var cls = (res.type == "success")?'success':'danger';
			var msg ='<span class="text text-'+cls+' fade in">' +  res.message + '</span>'
 			bootbox.dialog({
                                    title: res.type.toUpperCase(),
                                    message: msg,
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn-success",
                                            callback: function () {
                                                 classroomAssignSettings.openValue(clsmasid)
                                            }
                                        }
                                    }
                                });
                }
            }
        });
    });
};
classroomAssignSettings.removeSubjectTeachers = function (clsmasid, sub_id, teacherid) {
    var relatedentriesdel = teacherid ? '' : ' and also the related entries ';
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html('Do you want to delete this Subject Teacher ' + relatedentriesdel + ' ?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.assigningAjaxurl + "/removeSubjectTeachers",
            dataType: 'JSON',
            data: {
                teacherid: teacherid,
                classmasterid: clsmasid,
                subject_id: sub_id
            },
            success: function (res) {
                if (res) {
             

var cls = (res.type == "success")?'success':'danger';
			var msg ='<span class="text text-'+cls+' fade in">' +  res.message + '</span>'
 			bootbox.dialog({
                                    title: res.type.toUpperCase(),
                                    message: msg,
                                    buttons: {
                                        success: {
                                            label: "Ok",
                                            className: "btn-success",
                                            callback: function () {
                                                classroomAssignSettings.openValue(clsmasid)
                                            }
                                        }
                                    }
                                });
                }
            }
        });
    });
};
classroomAssignSettings.addnewClassroom = function (selectednode) {
var selectednode = selectednode?selectednode:0;
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/addnewClassroom",
        dataType: 'HTML',
data:{ selectednode:selectednode},
        success: function (res) {
            $('#loadStaffIncharge').html(res)
        }
    });
};
classroomAssignSettings.allClassroom = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/allClassroom",
        dataType: 'HTML',
        success: function (res) {
            $('#addclassroomdiv').html(res)
        }
    });
};
classroomAssignSettings.toggleactive = function (ctrl) {

    //$('.classroombtn').addClass('label-default').removeClass('label-warning')
    //$(ctrl).removeClass('label-default').addClass('label-warning')
$('.nobackgrnd').css('background-color','#fff');
$(ctrl).css('background-color','rgba(0, 0, 0, 0.05)');
}
classroomAssignSettings.iconfn = function (ctrl) {

    $('.subicon_' + ctrl).toggleClass('fa-chevron-down');
    //   $(ctrl).addClass('fa-chevron-up').removeClass('fa-chevron-down')

}
classroomAssignSettings.loadClassroomEditDet = function () {
    $('.clslab').addClass('hide');
    $('#name').addClass('hide');
    $('.clsdelete').addClass('hide');
    $('.clsedit').addClass('hide');
    $('.clstxt').removeClass('hide');
    $('.clsupdate').removeClass('hide');
    $('.clscancel').removeClass('hide');
};
classroomAssignSettings.editClassroomCancelDet = function () {
    $('.clslab').removeClass('hide');
    $('#name').removeClass('hide');
    $('.clsdelete').removeClass('hide');
    $('.clsedit').removeClass('hide');
    $('.clstxt').addClass('hide');
    $('.clsupdate').addClass('hide');
    $('.clscancel').addClass('hide');
};
classroomAssignSettings.deleteSelectClassroom = function (val) {
    console.log(val);
    var orgValues = {};
    orgValues['nodeid'] = val;
    orgValues['actions'] = 'delete';
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html('Deleting this classroom will also delete related entries.<br> Do you confirm to delete Classroom?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        classroomAssignSettings.addOrEditClassroomCofirm(orgValues);
    });

}
classroomAssignSettings.editClassroomUpdateDet = function (val) {
    console.log(val);
    var orgValues = {};
    orgValues['nodeid'] = val;
    orgValues['nodevalue'] = $('#nodevalue').val();
    orgValues['actions'] = 'edit';
    classroomAssignSettings.addOrEditClassroomCofirm(orgValues);
};
classroomAssignSettings.loadSubjTeach = function (clsmasid, name) {
    var orgValues = {};
    var err = 0;
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/loadSubjTeacher",
        dataType: 'html',
        data: {
            classmasterid: clsmasid
        },
        success: function (res) {
            $('#formModal .formBox-title').html('Add Subject (' + name + ')');
            $('#formModal .formBox-body').html(res);
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#confirm_save').removeClass('hide');
            $('#confirm_save').html('Save');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                $('#addSubjectTeachForm').find('input,select').each(function () {
                    if ($.inArray($(this).attr('name'), ["staff_list", "subjclasid", "subjID"]) > -1) {
                        if ($.trim($(this).val()) == '') {
                            err = 1;
                            swal({
                                title: "Error",
                                text: $(this).attr('title') + ' is required',
                                type: 'error',
                                confirmButtonText: 'OK!',
                                closeOnConfirm: true,
                            },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            classroomAssignSettings.loadSubjTeach(clsmasid, name);
                                            return false;

                                        }
                                    });
                        }
                        orgValues[$(this).attr('name')] = $.trim($(this).val());
                    }
                });
                orgValues['actions'] = "add";
                console.log(orgValues);
                if (err == 0) {
                    classroomAssignSettings.addSubjTeachConfirm(orgValues);
                }
            });

        }
    });
};
classroomAssignSettings.loadSubjTeacherOnly = function (clsmasid, subid, name) {
    var orgValues = {};
    var err = 0;
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/loadSubjTeacherOnly",
        dataType: 'html',
        data: {
            classmasterid: clsmasid,
            subid: subid
        },
        success: function (res) {
            $('#formModal .formBox-title').html('Add Subject Teacher(' + name + ')');
            $('#formModal .formBox-body').html(res);
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#confirm_save').removeClass('hide');
            $('#confirm_save').html('Save');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                $('#addSubjectTeachOnlyForm').find('input,select').each(function () {
                    if ($.inArray($(this).attr('name'), ["staff_list", "subjclasid", "subjID"]) > -1) {
                        if ($.trim($(this).val()) == '') {
                            err = 1;
                            swal({
                                title: "Error",
                                text: $(this).attr('title') + ' is required',
                                type: 'error',
                                confirmButtonText: 'OK!',
                                closeOnConfirm: true,
                            },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            classroomAssignSettings.loadSubjTeacherOnly(clsmasid, subid, name);
                                            return false;

                                        }
                                    });
                        }
                        orgValues[$(this).attr('name')] = $.trim($(this).val());
                    }
                });
                orgValues['actions'] = "edit";
                console.log(orgValues);
                if (err == 0) {
                    classroomAssignSettings.addSubjTeachConfirm(orgValues);
                }
            });

        }
    });
};
classroomAssignSettings.loadStaffIncharge = function (clsmasid, name) {
    var orgValues = {};
    var err = 0;
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/loadStaffIncharge",
        dataType: 'html',
        data: {
            classmasterid: clsmasid
        },
        success: function (res) {
            $('#formModal .formBox-title').html('Staff Incharge (' + name + ')');
            $('#formModal .formBox-body').html(res);
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#confirm_save').removeClass('hide');
            $('#confirm_save').html('Save');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                $('#addStaffInchargeForm').find('input,select').each(function () {
                    if ($.inArray($(this).attr('name'), ["staff_list", "classroomid"]) > -1) {
                        if ($.trim($(this).val()) == '') {
                            console.log($(this).parent().html())
                            swal({
                                title: "Error",
                                text: $(this).attr('title') + ' is required',
                                type: 'error',
                                confirmButtonText: 'OK!',
                                closeOnConfirm: true,
                            },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            classroomAssignSettings.loadStaffIncharge(clsmasid, name);
                                            return false;
                                            err = 1;
                                        }
                                    });
                        }
                        orgValues[$(this).attr('name')] = $.trim($(this).val());
                    }
                });
                orgValues['actions'] = "add";
                console.log(orgValues);
                if (err == 0) {
                    classroomAssignSettings.addStaffInchargeConfirm(orgValues);
                }
            });

        }
    });
};


classroomAssignSettings.updateClassgroupStudents = function (classroomid) {
    var selectedStu = {};
    selectedStu.sid = [];
    selectedStu.classroomid = classroomid;
    $('#draggable li').find('.event-select').each(function () {
        selectedStu.sid.push($(this).parent().attr('stuId'))
    })
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/updateClassgroupStudents",
        dataType: 'JSON',
        data: selectedStu,
        success: function (res) {
            if (res) {
                var lblcls = res.type == 'error' ? 'danger' : 'success';
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                $('#messageBoxModal').modal();
                $('#msg_ok').unbind('click').click(function (event) {
                    $('#messageBoxModal').modal('hide');
                    classroomAssignSettings.openValue(classroomid)
                });
                return false;

            }

        }
    });
};
