var studentCycleSettings = {};

studentCycleSettings.loadApplications = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.applnAjaxurl + "/getAllApplications",
        dataType: "html",
        success: function (res) {
            $('#application').html(res);
        }
    });
};
studentCycleSettings.loadApplApproval = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl,
        dataType: "html",
        success: function (res) {
            $('#application').html(res);
        }
    });
};

studentCycleSettings.input = '';
studentCycleSettings.loadProfilesettings = function () {
    studentCycleSettings.input = 'student';
    staffCycleSettings.input = '';
//    console.log('ff')
    $('#application').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')

    $.ajax({
        type: "POST",
        url: commonSettings.profileSettingsAjaxurl + "/index",
        dataType: "html",
        success: function (res) {
            $('#application').html(res);
        }
    });
};
studentCycleSettings.loadClassroom = function () {
    $('#classroom').prev().find('.cycle-title').html('Classroom Activities')
    $('#classroom').prev().find('.cycle-desc').html(' Manage student\'s academic progress ')
    $('#classroom').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.classRoomAjaxurl,
        dataType: "html",
        success: function (res) {
            $('#classroom').html(res);
        }
    });
};
studentCycleSettings.loadClassroomadmin = function () {
    $('#classroom').prev().find('.cycle-title').html('Classroom Settings')
    $('#classroom').prev().find('.cycle-desc').html(' Manage classroom prior settings ')
    $('#classroom').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.classRoomAdminAjaxurl,
        dataType: "html",
        success: function (res) {
            $('#classroom').html(res);
        }
    });
};
studentCycleSettings.loadstudentAssigning = function () {
    $('#classroom').prev().find('.cycle-title').html('Student Assigning')
    $('#classroom').prev().find('.cycle-desc').html('Manage grouping of students ')
    $('#classroom').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/assignSection",
        dataType: "html",
        success: function (res) {
            $('#classroom').html(res);
        }
    });
};

studentCycleSettings.loadfeeAssigning = function () {
    $('#feesection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.feeAssigningUrl,
        dataType: "html",
        success: function (res) {
            $('#feesection').html(res);
        }
    });
};
studentCycleSettings.loadfeeCollection = function () {
    $('#feesection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    feepaymentSettings.callfrom = '';
    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/studentFeePayment",
        dataType: "html",
        success: function (res) {
            $('#feesection').html(res);
        }
    });
};

studentCycleSettings.loadfeeCancel = function () {
    $('#feesection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    feepaymentSettings.callfrom = '';
    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/feeCancelApprvl",
        dataType: "html",
        success: function (res) {
            $('#feesection').html(res);
        }
    });
};
studentCycleSettings.loadfeeDemand = function () {
    $('#feesection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.feeDemandUrl,
        dataType: "html",
        success: function (res) {
            $('#feesection').html(res);
        }
    });
};
studentCycleSettings.loadfeeSettings = function () {
    $('#feesection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl,
        dataType: "html",
        success: function (res) {
            $('#feesection').html(res);
        }
    });
};

studentCycleSettings.loadFeeCancellationApproval = function () {
    $('#feesection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl,
        dataType: "html",
        success: function (res) {
            $('#feesection').html(res);
        }
    });
};

studentCycleSettings.loadPromotionSettings = function () {
//    $.ajax({
//        type: "POST",
//        url: commonSettings.promotionAjaxurl+'/settings' ,
//        dataType: "html",
//        success: function (res) {
//            $('#lvloutsection').html(res);
//        }
//    });

    var newForm = $('<form>', {
        'action': commonSettings.promotionAjaxurl + '/settings',
        'target': '_blank',
        'id': 'loadPromotionSettingsForm'
    });
    newForm.appendTo("body").submit();

}
studentCycleSettings.loadStudentPromotion = function () {
    $('#lvloutsection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
//        url: commonSettings.promotionAjaxurl + "/listAllStudent",
        url: commonSettings.evaluationUrl + "/finalEvaluation",
        dataType: "html",
        success: function (res) {
            $('#lvloutsection').html(res);
        }
    });

}
studentCycleSettings.loadStudentReleiv = function () {
    $('#lvloutsection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/student",
        dataType: "html",
        success: function (res) {
            $('#lvloutsection').html(res);
        }
    });

};

studentCycleSettings.loadStudentReleivList = function () {
    $('#lvloutsection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/studentRelive",
        dataType: "html",
        success: function (res) {
            $('#lvloutsection').html(res);
        }
    });

}


studentCycleSettings.loadgeneralprofile = function (stuid) {
    $('#application').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.studentProfAjaxurl + "/studentGeneralProfile?studentId=" + stuid,
        dataType: "html",
        success: function (res) {
            $('#application').html(res);
        }
    });

}
studentCycleSettings.loadacademicprofile = function (stuid) {
    $('#classroom').prev().find('.cycle-title').html('Student Profile')
    $('#classroom').prev().find('.cycle-desc').html('Manage student general profile  ')
    $('#classroom').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.studentProfAjaxurl + "/academic?studentId=" + stuid,
        dataType: "html",
        success: function (res) {
            $('#classroom').html(res);
        }
    });

}
studentCycleSettings.loadStudentFeeList = function (stuid) {
    $('#feesection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/getStudentFeeList",
        data: {
            stu_feeselectedID: stuid,
            academicYrId: ''
        },
        dataType: "html",
        success: function (res) {
            $('#feesection').html(res)
        }
    });
};

studentCycleSettings.loadGeneralprofileal = function (cntrl) {
    $('#profilesection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    var frmreset = $(cntrl).attr('from-action') ? $(cntrl).attr('from-action') : '';

    $.ajax({
        type: "POST",
        url: commonSettings.profileAjaxurl + "/studentProfile",
        data: {frmreset: frmreset},
        dataType: "html",
        success: function (res) {
            $('#profilesection').html(res);

        }
    });
};

studentCycleSettings.loadstudent = function (val) {
    $.ajax({
        type: "POST",
        url: commonSettings.cycleAjaxurl + "/loadStudentDetails",
        data: {stuId: val},
        dataType: "json",
        success: function (res) {
//            console.log(res)
            $('#stu_admission_icon').attr('class', 'white_font')
                    .attr('onclick', "")
            //.attr('onclick', "studentCycleSettings.loadgeneralprofile(" + val + ")")
            $('#stupicurl').attr({'href': '#page-2',
                'class': 'pageload-link white_font',
                'onclick': "studentCycleSettings.loadgeneralprofile(" + val + ")"})


            $('#admission_icon').attr('class', 'hide')
            $('#menu-wrapper1').attr('class', 'hide')
            $('#stu_academic_icon').attr('class', 'pageload-link white_font')
                    .attr('onclick', "studentCycleSettings.loadacademicprofile(" + val + ")")
            $('#academic_icon').attr('class', 'hide')

            $('#stu_fee_icon').attr('class', 'pageload-link white_font')
                    .attr('onclick', "studentCycleSettings.loadStudentFeeList(" + val + ")")
            $('#fee_icon').attr('class', 'hide')

            $('#stu_relieve_icon').attr('class', '')
            $('#menu-wrapper').attr('class', 'hide')
            $('#relieve_icon').attr('class', 'hide')
            $('#menu-wrapper2').attr('class', 'hide')
            $('#studb').attr('class', 'hide')
            $('#menu-wrapper3').attr('class', 'hide')
            $.each(res, function (key, value) {
                $('#' + key).html(value)
            });
        }
    });
};

studentCycleSettings.loadstudentProf = function () {
    $('#stuprofsection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "GET",
        url: commonSettings.studentDashboardAjaxurl + "/index",
        dataType: "html",
        success: function (res) {
            $('#stuprofsection').html(res)
        }
    });
};

studentCycleSettings.loadApplicationApproval = function ()
{

    $('#application').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "GET",
        url: commonSettings.approvalViewAjaxurl + "/loadApprovedAppln",
        dataType: "html",
        success: function (res) {
            $('#application').html(res)
        }
    });
};

studentCycleSettings.loadStuApplicationReleivApproval = function ()
{
    $('#lvloutsection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "GET",
        url: commonSettings.approvalViewAjaxurl + "/studentReliveApprvl",
        dataType: "html",
        success: function (res) {
            $('#lvloutsection').html(res)
        }
    });

};

studentCycleSettings.loadApprovals = function () {
    $('#feesection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + '/studentApprovals',
        dataType: "html",
        success: function (res) {
            $('#feesection').html(res);
        }
    });

};
studentCycleSettings.loadStuApplicationReleivSettings = function ()
{
    $('#lvloutsection').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "GET",
        url: commonSettings.relievingAjaxurl + "/studentReliveSettings",
        dataType: "html",
        success: function (res) {
            $('#lvloutsection').html(res)
        }
    });

};
studentCycleSettings.loadAcademicRepotrts = function () {
    $('#classroom').prev().find('.cycle-title').html('Academic Progress')
    $('#classroom').prev().find('.cycle-desc').html('Report of student\'s academic progress')
    $('#classroom').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.reportAjaxurl + "/loadAcademicReports",
        dataType: "html",
        success: function (res) {
            $('#classroom').html(res);
        }
    });
};