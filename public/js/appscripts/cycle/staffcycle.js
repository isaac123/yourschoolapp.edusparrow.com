var staffCycleSettings = {};

staffCycleSettings.loadAssigning = function () {
    $('#classactivity').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.assigningAjaxurl + "/assignStafftoClass",
        dataType: "html",
        success: function (res) {
            $('#classactivity').html(res);
        }
    });
};
staffCycleSettings.loadEvaluation = function () {
    $('#classactivity').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.stfratingAjaxurl + "/staffEvaluation",
        dataType: "html",
        success: function (res) {
            $('#classactivity').html(res);
        }
    });
};

staffCycleSettings.loadStafflist = function () {
    $('#appointment').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + '/getAllStaffList', //   newAppointment
        dataType: "html",
        success: function (res) {
            $('#appointment').html(res);
        }
    });
};


staffCycleSettings.loadAppointment = function () {
    $('#appointment').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.appointementAjaxurl + '/newAppointment', // appointementAjaxurl  newAppointment
        dataType: "html",
        success: function (res) {
            $('#appointment').html(res);
        }
    });
};


staffCycleSettings.loadStfReleiv = function () {
    $('#relievstf').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/relieveStaff",
        dataType: "html",
        success: function (res) {
            $('#relievstf').html(res);
        }
    });

};
staffCycleSettings.loadAttendace = function () {
    $('#staffactivity').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl,
        dataType: "html",
        success: function (res) {
            $('#staffactivity').html(res);
        }
    });

};

staffCycleSettings.loadApprovals = function () {
    $('#staffactivity').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + '/staffApprovals',
        dataType: "html",
        success: function (res) {
            $('#staffactivity').html(res);
        }
    });

};

staffCycleSettings.loadPayroll = function () {
    $('#staffactivity').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl,
        dataType: "html",
        success: function (res) {
            $('#staffactivity').html(res);
        }
    });

};
staffCycleSettings.loadStaffProfile = function (stfid, input) {
    $('#appointment').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    //console.log('ins')
    $.ajax({
        type: "GET",
        url: commonSettings.staffAjaxurl + "/profile",
        data: {
            input: input,
            staffId: stfid
        },
        dataType: "html",
        success: function (res) {
            $('#appointment').html(res)
        }
    });
};

staffCycleSettings.loadStaffAppointmentdet = function (stfid, input) {
    $('#appointment').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    //console.log('ins')
    $.ajax({
        type: "GET",
        url: commonSettings.staffAjaxurl + "/loadAppointmentdetails",
        data: {
            input: input,
            staffId: stfid
        },
        dataType: "html",
        success: function (res) {
            $('#appointment').html(res)
        }
    });
};

staffCycleSettings.loadStaffPayroll = function (stfid) {
    $('#staffactivity').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    //console.log('ins')
    $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/getPayrollList",
        data: {
            staffId: stfid
        },
        dataType: "html",
        success: function (res) {
            $('#staffactivity').html(res)
        }
    });
};

staffCycleSettings.loadstaff = function (val, type) {
    $.ajax({
        type: "POST",
        url: commonSettings.cycleAjaxurl + "/loadStaffDetails",
        data: {stfId: val},
        dataType: "json",
        success: function (res) {
            //console.log(res)
            $('#appoint_stf').attr('class', 'white_font')
                    .attr('onclick', '')
            // .attr('onclick', 'staffCycleSettings.loadStaffAppointmentdet(' + val + ',"general")')
//            $('#appoint').attr('class', 'hide')


            $('#stfloadfun').attr('class', 'pageload-link white_font')
                    .attr('onclick', 'staffCycleSettings.loadStaffProfile(' + val + ',"general")')

            $('#appoint_stf_indv').attr('class', 'hide')

            $('#menu-wrapper').attr('class', 'hide')
//                    $('#appoint_stf').attr('onclick', 'staffCycleSettings.loadStaffProfile(' + val + ',"general")')
            $('#class_activity_stf').attr('class', 'pageload-link white_font')
                    .attr('onclick', "staffCycleSettings.loadStaffProfile(" + val + ",'schedule')")
            $('#class_activity').attr('class', 'hide')
            $('#menu-wrapper1').attr('class', 'hide')
            $('#stf_relieve_icon').attr('class', '')
            $('#relieve_icon').attr('class', 'hide')

            $('#payroll_stf').attr('class', 'pageload-link white_font')
                    .attr('onclick', "staffCycleSettings.loadStaffActivity(" + val + ",'" + type + "' )")
            $('#payroll').attr('class', 'hide')

            $('#studb').attr('class', 'hide')
            $('#menu-wrapper2').attr('class', 'hide')
            $.each(res, function (key, value) {
                $('#' + key).html(value)
            });
        }
    });
};

staffCycleSettings.loadstaffProf = function () {
    $('#staffprof').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "GET",
        url: commonSettings.staffDashboardAjaxurl + "/index?input=generalprofile_section",
        dataType: "html",
        success: function (res) {
            $('#staffprof').html(res)
        }
    });
};

staffCycleSettings.loadScheduling = function () {
    $('#classactivity').html(' Academic calendar goes here..');
};

staffCycleSettings.input = '';
staffCycleSettings.loadProfilesettings = function () {
    $('#appointment').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')

    staffCycleSettings.input = 'staff';
    studentCycleSettings.input = '';
    //alert('test');
    //console.log('ff')
    $.ajax({
        type: "POST",
        url: commonSettings.profileSettingsAjaxurl + "/staff",
        dataType: "html",
        success: function (res) {
            $('#appointment').html(res);
        }
    });
};

staffCycleSettings.loadStaffdet = function (cntrl)
{
    $('#staffprof').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')

    var frmreset = $(cntrl).attr('from-action') ? $(cntrl).attr('from-action') : '';

    $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/getAllStaff",
        data: {frmreset: frmreset},
        dataType: "html",
        success: function (res) {
            $('#staffprof').html(res);
        }
    });

};

staffCycleSettings.loadApplicationApproval = function ()
{
    $('#appointment').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "GET",
        url: commonSettings.approvalViewAjaxurl + "/loadStaffAppln",
        dataType: "html",
        success: function (res) {
            $('#appointment').html(res)
        }
    });

};

staffCycleSettings.loadStaffReleiv = function ()
{
    $('#relievstf').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "GET",
        url: commonSettings.approvalViewAjaxurl + "/loadStaffRelvAppln",
        dataType: "html",
        success: function (res) {
            $('#relievstf').html(res)
        }
    });

};

staffCycleSettings.loadManageLeave = function () {
    $('#staffactivity').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.leaveAjaxurl + "/manageMyLeaves",
        dataType: "html",
        success: function (res) {
            $('#staffactivity').html(res);
            //   $('#leavediv').removeClass('hide')
        }
    });
};

staffCycleSettings.loadStaffActivity = function (stfid, type) {
    $('#staffactivity').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    //console.log('ins')
    $.ajax({
        type: "POST",
        url: commonSettings.staffAjaxurl + "/staffActivities",
        data: {
            staffId: stfid,
            type: type
        },
        dataType: "html",
        success: function (res) {
            $('#staffactivity').html(res)
        }
    });
};