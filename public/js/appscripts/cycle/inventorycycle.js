var inventorycycleSettings = {};

inventorycycleSettings.loadVendorList = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.vendorAjaxurl + "/vendorManagement",
        dataType: "html",
        success: function (res) {
            $('#vendor').html(res);
        }
    });
};
inventorycycleSettings.loadPurchaseList = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.inventoryAjaxurl + "/purchaseManagement",
        dataType: "html",
        success: function (res) {
            $('#purchase').html(res);
              inventorySettings.loadPurchaseDet();
        }
    });
};

inventorycycleSettings.loadMaterialinList = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.movementinAjaxurl + '/movementinManagement', //   newAppointment
        dataType: "html",
        success: function (res) {
            $('#materialin').html(res);
             movementinSettings.mvmntinwithoutPo();
           
        }
    });
};
inventorycycleSettings.loadMaterialoutList = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.movementoutAjaxurl + '/movementinitial', //   newAppointment
        dataType: "html",
        success: function (res) {
            $('#materialout').html(res);
        }
    });
};
inventorycycleSettings.loadStockList = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.inventoryAjaxurl + '/inventoryManagement', //   newAppointment
        dataType: "html",
        success: function (res) {
            $('#stock').html(res);
             inventorySettings.loadMaterialPanel();
        }
    });
};
inventorycycleSettings.movementinReport = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.movementinAjaxurl + '/movementinReport', //   newAppointment
        dataType: "html",
        success: function (res) {
            $('#vendor').html(res);
            movementinSettings.movementinHeader();
        }
    });
};
inventorycycleSettings.movementoutReport = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.movementoutAjaxurl + '/movementoutReport', //   newAppointment
        dataType: "html",
        success: function (res) {
            $('#vendor').html(res);
            movementoutSettings.movementoutHeader();
        }
    });
};



