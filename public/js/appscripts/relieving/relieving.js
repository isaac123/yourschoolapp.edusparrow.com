var relievingSettings = {};
relievingSettings.loadStaffList = function (stfList) {
    //console.log(stfList);
    relievingSettings.stfList = stfList;
    relievingSettings.pageno = 0;
    _loadStfRelievDiv(relievingSettings.pageno)
};
function _loadStfRelievDiv(pageno) {
    relievingSettings.pageno = pageno;
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/staffListFormat?page=" + pageno,
        data: {
            stfListJson: relievingSettings.stfList
        }
    }).done(function (resHtml) {
        $("#stfFormatList").html(resHtml);
    });

}
relievingSettings.loadRelievForm = function (staffId) {

    var stfId = staffId;
    var relieveData = {};
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/staffRelievModal",
        data: {
            stfId: stfId
        },
        success: function (resHtml) {
            $('.formBox-title').html('Confirm to Relieve Staff');
            $('.formBox-body').html(resHtml);
            //    $('#formModal').modal('show')
            $('#confirm_save').html('Initiate relieving')
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                $("#relieveStaffForm").find('input,textarea').each(function () {
                    relieveData[$(this).attr('name')] = ($.trim($(this).val()));
                });
                $.ajax({
                    type: "POST",
                    url: commonSettings.relievingAjaxurl + "/relieveStaffByReason",
                    data: relieveData,
                    dataType: "JSON",
                }).done(function (res) {
                    if (res) {
                        $('.msgBox-title').html('Message');
                        $('.msgBox-body').html(res['message']);
                        $('#messageBoxModal').modal('show');
                        $('#msg_ok').unbind('click').click(function () {
                            if (res['type'] == 'error') {
                                $('#msg_ok').unbind('click').click();
                                return false;
                            }
                            else if (res['type'] == 'success') {
//                                window.location.reload()

                                relieveData.type = 'Relieving(staff)';
                                relieveData.itemID = res['ItemID'];
                                relieveData.status = 'Requested';
                                relieveData.fromPage = 'Relieving(staff)'; //recordTypId
                                console.log(relieveData)
                                approvalSettings.checkReqStatus(relieveData);
                            }
                        });

                    }
                });
            });
        }
    })
};

relievingSettings.loadStudent = function (student) {
    relievingSettings.student = student;
    var relieveData = {};
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/studentRelievModal",
        data: {
            studentJson: relievingSettings.student
        },
        success: function (resHtml) {
            $("#stureliForm").html(resHtml);
        }
    })
};



relievingSettings.relievStudent = function () {

    var stuRelievData = {},
            valid = 1;
    var error = '';
    $("#relieveStudentForm").find('input,select,textarea').each(function () {

        stuRelievData[$(this).attr('name')] = $.trim($(this).val());
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                valid = 0;
                error += $(this).attr('title') + ' is required!<br>';
            }

        }
    });
    console.log(stuRelievData);
    if (valid == 1) {

        $.ajax({
            type: "POST",
            url: commonSettings.relievingAjaxurl + "/relieveStudent",
            data: stuRelievData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        else if (res['type'] == 'success') {

                            stuRelievData.type = 'Relieving(student)';
                            stuRelievData.itemID = res['ItemID'];
                            stuRelievData.status = 'Requested';
                            stuRelievData.fromPage = 'Relieving(student)'; //recordTypId
                            console.log(stuRelievData)
                            approvalSettings.checkReqStatus(stuRelievData);
                        }
                    });

                }
            }
        });
    } else {
        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">' + error + '</span>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function (event) {
            $('#messageBoxModal').modal('hide');
        });
        return false;
    }
};


relievingSettings.loadClassByAcademicYr = function (acvalue) {
    if (acvalue != 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.relievingAjaxurl + "/loadClassByAcademicYear",
            data: {
                academicYr: acvalue
            },
            success: function (res) {
                if (res) {
                    ////        console.log(resHtml).log(res)                    
                    $('#StudentClass').html(res);
                    $('#StudentClass').removeClass('hide')
                }

            }
        });
    } else {

        $('.modal-title').html('Message');
        $('.modal-body').html("Select valid Academic Year");
        $('#messageBox').click();
        return false;
    }
};

relievingSettings.loadSectionsByClass = function (clsvalue) {
    if (clsvalue != 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.relievingAjaxurl + "/loadSectionsByClass",
            data: {
                classId: clsvalue,
                academicYr: $('#academicYrId').val()
            },
            success: function (res) {
                if (res) {
                    ////        console.log(resHtml).log(res)                    
                    $('#StudentSection').html(res);
                    $('#StudentSection').removeClass('hide')
                }

            }
        });
    } else {
        $('.modal-title').html('Message');
        $('.modal-body').html("Select valid Class");
        $('#messageBox').click();
        return false;
    }
};

relievingSettings.loadRelieveStuList = function () {

    var stuSearchData = {};
    var params = {};
    stuSearchData.emptySearch = 1;
    $("#studentGroupSearchForm").find('input,select').each(function () {
        if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
            stuSearchData.emptySearch = 0;
            stuSearchData[$(this).attr('name')] = $.trim($(this).val());
        }
    });


    params = stuSearchData;

    console.log(params);
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/loadRelieveStuTblHdr",
        dataType: "html",
        success: function (res) {
            $('#ApplicationTableDiv').html(res);
            var tablectrl = '#studentApplicationList';
            relievingSettings.NumberOfRecords = 10;

            var column = [0, 1, 2, 3, 4, 5, 6];
            var sortingDisabled = [7, 8];
            /*$('#studentApplicationList tfoot th').each(function() {
             var title = $('#studentApplicationList thead th').eq($(this).index()).text();
             $(this).html('<input type="text" placeholder="Search ' + title + '" />');
             });*/
            $(tablectrl).dataTable({
                "sDom": 'C<"clear">lrtip',
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": commonSettings.relievingAjaxurl + '/loadRelieveStuData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": relievingSettings.NumberOfRecords,
                // 'iDisplayStart':patientsapp.startIndex,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[0, "desc"]],
                "bLengthChange": false,
                "bInfo": false,
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function (o) {
                    if (o._iRecordsDisplay <= relievingSettings.NumberOfRecords) {
                        $(tablectrl + '_paginate').hide();
                    }
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    var rowText = '<tr>';

                    rowText += '<td>';
                    rowText += '<span class="tooltip tooltip-effect-2"><span class="tooltip-item">';
                    rowText += aData.academicYr;
                    rowText += '</span></td>';



                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.admission_no + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.Student_Name + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.gender + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.class + '</span>';
                    rowText += '</td>';


                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.dateofjoin + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.relievdate + '</span>';
                    rowText += '</td>';

//                    rowText += '<td>';
//                    rowText += '<span class="label label-' + aData.labelclass + ' label-mini">' + aData.status + '</span>';
//                    rowText += '</td>';
//
//
//                    rowText += '<td>';
//                    rowText += '<span class="label label-' + aData.dataclass + ' label-mini">' + aData.key_in_status + '</span>';
//                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="label label-warning label-mini">' + aData.status + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<a href="javascript:void(0)" onclick=""> <i class="fa fa-eye fa-2x"></i>  </a>';
                    rowText += '</td>';



//                    rowText += '<td>';
//                    rowText += aData.Actions;
//                    rowText += '</td>';

                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });
        }
    });
};



relievingSettings.loadjSubtree = function (ctrl)
{
    console.log($(ctrl).parent().parent().next('.subtreediv').html())
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/loadjSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};
relievingSettings.loadrSubtree = function (ctrl)
{
    console.log($(ctrl).parent().parent().next('.subtreediv').html())
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/loadrSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};


relievingSettings.changeStatusStudent = function (statusData) {
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/changeApprovalStatusStudent",
        data: statusData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                if (statusData.fromPage == 'allapproval' || statusData.fromPage == 'ApprovalView') {
                    if(statusData.fromPage == 'ApprovalView'){
                        viewapproval.modal('hide'); 
                    }
                    var n_dom = [], Dtype = 'information';
                    Dlayout = 'topRight';
                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a> Approval Status Updated <span> </span> </div> </div>';
                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                    announcementSettings.reload_inbox();
                } else {
                    var lblcls = res.type == 'error' ? 'danger' : 'success';
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                    $('#messageBoxModal').modal();
                    $('#msg_ok').unbind('click').click(function (event) {
                        $('#messageBoxModal').modal('hide');
                        if (res['type'] == 'success') {
                            if (statusData.fromPage == 'Relieving(student)') {
                                studentCycleSettings.loadStudentReleiv()
                            } else if (statusData.fromPage == 'StuRelievApplication') {
                                approvalviewSettings.loadReliveStundentdet();
                            } else {
                                window.location.reload();
                            }
                        }
                    });
                    return false;
                }
            }
        }
    });
};

relievingSettings.viewForwardDetails = function (ctrl) {
    var appData = {};
    appData.itemID = $(ctrl).attr('itemID');
    appData.appTypId = $(ctrl).attr('appType');
    appData.appitemID = $(ctrl).attr('appitemID');
    console.log(appData);
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/viewForwardHistory",
        data: appData,
        //dataType: "JSON",
        success: function (res) {
            console.log(res)
            $('.msgBox-title').html('List of Escalation made for this request');
            $('.msgBox-body').html(res);
            $('#messageBoxModal').modal();
            console.log("clicked")
        }
    });
};
relievingSettings.relievStudentPermanently = function (ctrl) {
    var appData = {};
    appData.itemID = $(ctrl).attr('itemID');
    console.log(appData);
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/relieveStudentPermanently",
        data: appData,
        dataType: "JSON",
        success: function (res) {
            if (res['type'] == 'error') {
                swal("Error!", res['message'], "error");
                return false;
            }
            else if (res['type'] == 'success') {
                swal({
                    title: "Done",
                    text: res['message'],
                    type: "success",
                    confirmButtonText: 'OK',
                    closeOnConfirm: true,
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                studentCycleSettings.loadStudentReleiv()
                            }
                        });
            }
        }
    });
};

relievingSettings.staffRelievPermanently = function (ctrl) {
    var appData = {};
    appData.staffID = $(ctrl).attr('itemID');
    console.log(appData);
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/relieveStaffPermanently",
        data: appData,
        dataType: "JSON",
        success: function (res) {
            if (res['type'] == 'error') {
                swal("Error!", res['message'], "error");
                return false;
            }
            else if (res['type'] == 'success') {
                swal({
                    title: "Done",
                    text: res['message'],
                    type: "success",
                    confirmButtonText: 'OK',
                    closeOnConfirm: true,
                },
                        function (isConfirm) {
                            if (isConfirm) {

                                relievingSettings.getStaff();
                            }
                        });
            }
        }
    });
};
relievingSettings.changeStatusStaff = function (statusData) {
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/changeApprovalStatusStaff",
        data: statusData,
        dataType: "JSON",
        success: function (res) {

            if (res) {
               if (statusData.fromPage == 'allapproval' || statusData.fromPage == 'ApprovalView') {
                    if(statusData.fromPage == 'ApprovalView'){
                        viewapproval.modal('hide'); 
                    }
                    var n_dom = [], Dtype = 'information';
                    Dlayout = 'topRight';
                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a> Approval Status Updated <span> </span> </div> </div>';
                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                    announcementSettings.reload_inbox();
                } else {
                    var lblcls = res.type == 'error' ? 'danger' : 'success';
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                    $('#messageBoxModal').modal();
                    $('#msg_ok').unbind('click').click(function (event) {
                        $('#messageBoxModal').modal('hide');
                        if (res['type'] == 'success') {
                            if (statusData.fromPage == 'Relieving(staff)') {
                                relievingSettings.getStaff();
                            } else if (statusData.fromPage == 'StfRelievApplication') {
                                approvalviewSettings.loadStaffApplicationList();
                            } else {
                                window.location.reload();
                            }
                        }
                    });
                    return false;
                }
            }
        }
    });
};
relievingSettings.viewRelieveDetails = function (stuid) {
    var appData = {};
    appData.stuid = stuid;
    console.log(appData);
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/viewRelieveDetails",
        data: appData,
        //dataType: "JSON",
        success: function (res) {

            $('#formModal .formBox-title').html('Relieve status check ');
            $('#formModal .formBox-body').html(res);
            $('#confirm_save').html('Refresh')
            $('#formModal').modal('show')
            $('#confirm_save').unbind('click').click(function () {
                $('#formModal').modal('hide')
                relievingSettings.refresh(stuid)
            });
        }
    });
};
relievingSettings.refresh = function (stuid) {
    var appData = {};
    appData.stuid = stuid;
    console.log(appData);
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/refreshRelieveDetails",
        data: appData,
        dataType: "JSON",
        success: function (res) {
            if (res['type'] == 'success') {
                approvalviewSettings.loadReliveStundentdet()
            }
        }
    });
};

relievingSettings.updateRelievingStatus = function (ctrl) {
    var relievingSettings = {};
    relievingSettings.itemID = $(ctrl).closest('tr').attr('itemid');
    relievingSettings.value = '0';
    if ($(ctrl).is(':checked') == true) {
        relievingSettings.value = '1';
    }
    console.log(relievingSettings);
    $.ajax({
        type: "POST",
        url: commonSettings.relievingAjaxurl + "/updateRelievingStatus",
        data: relievingSettings,
        dataType: "JSON",
        success: function (res) {
            if (res['type'] == 'error') {
                console.log(res['message'])
                return false;
            }
            else if (res['type'] == 'success') {
                $.gritter.add({
                    // (string | mandatory) the heading of the notification
                    title: 'Profile Settings Changed!',
                    // (string | mandatory) the text inside the notification
                    text: res['message']
                });
            }



        }
    });
}
relievingSettings.loadForwardDetails = function (itemid) {
    var stuRelievData = {};
    stuRelievData.type = 'Relieving(student)';
    stuRelievData.itemID = itemid;
    stuRelievData.status = 'Requested';
    stuRelievData.fromPage = 'Relieving(student)';
    console.log(stuRelievData)
    approvalSettings.checkReqStatus(stuRelievData);
};
relievingSettings.loadForwardforRelieve = function (ctrl) {
    var relieveData = {};
    relieveData.type = 'Relieving(staff)';
    relieveData.itemID = $(ctrl).attr('itemID');
    ;
    relieveData.status = 'Forwarded';
    relieveData.fromPage = 'Relieving(staff)'; //recordTypId
    console.log(relieveData)
    approvalSettings.checkReqStatus(relieveData);
};