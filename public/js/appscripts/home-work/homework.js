var homeworkSettings = {};
homeworkSettings.stuparams = {};
homeworkSettings.loadHomeworkName = function (ctrl) {

//    $('.combispan').addClass('inactive')
//    $(ctrl).toggleClass('inactive')
    $('.combispan').addClass('label-default').removeClass('label-warning')
    if ($(ctrl).hasClass('combispan'))
        $(ctrl).removeClass('label-default').addClass('label-warning')

    homeworkSettings.subject_masterid = $(ctrl).attr('id').split('_')[1];

    $.ajax({
        type: "POST",
        url: commonSettings.homeworkAjaxurl + "/loadHomeWorkSubject",
        data: {
            subject_masterid: homeworkSettings.subject_masterid,
            return_uri: $("#return_uri").val()
        },
        success: function (res) {
            if (res) {
//                console.log(res)
//                $('.loadRatingName').html('');
//                $('.loadStudenRating').html('');
                $('.loadHomeworkName').html(res);
                $('.loadHomeworkName').removeClass('hide');

                homeworkSettings.loadHomeworDet();
//                $('.loadStudenRating').addClass('hide');
            }
        }
    });

};

homeworkSettings.saveHomework = function () {

    var params = {};
    var err = 0;
    var error = '';
    $("#addHomeworkForm").find('input,select,textarea').each(function () {


        if ($.trim($(this).val()) == '' || $.trim($(this).val()) == '0') {
            err = 1;
            error += $(this).attr('name') + " is required!<br>";
        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }


    });
    if (err == 0) {
        params['subject_masterid'] = homeworkSettings.subject_masterid;
        console.log(params);

        $.ajax({
            type: "POST",
            url: commonSettings.homeworkAjaxurl + "/saveHomework",
            data: params,
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    var lblcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                    $('.modal-title').html("Message");
                    $('.modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        $('#homeworkcontnt').val('');
                        if (res.type == 'success')
                        {
                            homeworkSettings.loadHomeworDet();
                        }
                        $('#messageBoxModal').modal('hide');

                    });
                }
            }
        });


    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
        return false;
    }


//    window.open(commonSettings.templateAjaxurl + '/overallFeereceipt?studid=' + stuid + '&&fromdate=' + fromdate + '&&todate=' + todate, '_blank');
};


homeworkSettings.editHomework = function (id) {

    $.ajax({
        type: "POST",
        url: commonSettings.homeworkAjaxurl + "/editHomework",
        data: {id: id},
        dataType: "html",
        success: function (res) {
            //console.log(res);
            $('.formBox-title').html('Edit Home Work');
            $('.formBox-body').html(res);
            // $('#modalForm').click();
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#formModal').modal('show');
            $('#confirm_save').html('Save');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                homeworkSettings.updateHomework();
            });
        }
    });

};

homeworkSettings.loadHomeWorkBYStudent = function (params) {
    $.ajax({
        type: "POST",
        url: commonSettings.homeworkAjaxurl + "/loadHomeWorkBYStudentDate",
        data: params,
        dataType: "html",
        success: function (res) {
            //console.log(res);
            $('#homeworkdiv').html(res);
            $('#homeworkdiv').removeClass('hide')
        }
    });

};

homeworkSettings.loadHomeWorkBYStudentBydate = function () {

    var searchdate = $('#hdate').val() ? $('#hdate').val() : '';
    var searchsubject = $('#subjectid_homewrk :selected').val() ? $('#subjectid_homewrk :selected').val() : '';
//alert(searchsubject);
    homeworkSettings.stuparams['searchdate'] = searchdate;
    homeworkSettings.stuparams['searchsubject'] = searchsubject;

    console.log(homeworkSettings.stuparams);

    $.ajax({
        type: "POST",
        url: commonSettings.homeworkAjaxurl + "/loadHomeWorkBYStudentDate",
        data: homeworkSettings.stuparams,
        dataType: "html",
        success: function (res) {
            //console.log(res);
            $('#homeworksearch').html(res);
//            $('#homeworkdiv').removeClass('hide')
        }
    });

};

homeworkSettings.updateHomework = function () {

    var params = {};
    var err = 0;
    var error = '';
    $("#editHomeworkForm").find('input,select,textarea').each(function () {


        if ($.trim($(this).val()) == '' || $.trim($(this).val()) == '0') {
            err = 1;
            error += $(this).attr('name') + " is required!<br>";
        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }


    });
    if (err == 0) {
        params['subject_masterid'] = homeworkSettings.subject_masterid;
        console.log(params);

        $.ajax({
            type: "POST",
            url: commonSettings.homeworkAjaxurl + "/updateeditHomework",
            data: params,
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    var lblcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                    $('.modal-title').html("Message");
                    $('.modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
//                        $('#messageBox').click();
                        if (res.type === 'success') {
                            homeworkSettings.loadHomeworDet();
                        }
                        $('#messageBoxModal').modal('hide');

                    });
                }
            }
        });


    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
        return false;
    }


//    window.open(commonSettings.templateAjaxurl + '/overallFeereceipt?studid=' + stuid + '&&fromdate=' + fromdate + '&&todate=' + todate, '_blank');
};

homeworkSettings.loadHomeworDet = function () {
    var SearchData = {};
    var params = {};

    SearchData['grpsubject_id'] = homeworkSettings.subject_masterid;
    SearchData['subject_modules_id'] = $('#aggregate_subjectid').val();

    params = SearchData;
    console.log(params);

    $.ajax({
        type: "POST",
        url: commonSettings.homeworkAjaxurl + "/homeworkTblHdr",
        dataType: "html",
        success: function (res) {
//            console.log(res);
            $('.loadHomeworkdata').html(res);
            $('.loadHomeworkdata').removeClass('hide');
            var tablectrl = '#homeworkListTable';
            var column = [];
            var sortingDisabled = [0, 1, 2,3,4];
//console.log('tes');
            $(tablectrl).dataTable({
                "sDom": '<"top"if>rt<"bottopm"p><"clear">',
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": commonSettings.homeworkAjaxurl + '/loadHomeworkData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": commonSettings.NumberOfRecords,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[0, "desc"]],
                "bLengthChange": false,
                //"bInfo": false,
                "oLanguage": {
                    "sInfo": "Showing _START_ to _END_ of _TOTAL_ Home Works",
                    "sInfoFiltered": " ",
                },
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function (o) {
                    $('#headerlist').html($('.dataTables_info').html())
                    $('.dataTables_filter').addClass('col-md-5');
                    $('.dataTables_info').addClass('col-md-6 hide');
                    $('.dataTables_filter').find('label').css('display', ' inline');
                    $('.dataTables_filter').find('input').addClass('form-control').css('width', ' 50%');
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                    var rowText = '<tr >';
                    rowText += '<td>';
                    rowText += aData.date;
                    rowText += '</td>';

                    rowText += '<td >';
                    rowText += aData.title;
                    rowText += '</td>';

                    rowText += '<td class="td-left">';
                    rowText += aData.homework;
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += aData.action; 
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += aData.actiond; 
                    rowText += '</td>';


                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });


        }
    });
}

homeworkSettings.loadHomeworkReport = function () {

    var params = {};
    var err = 0;
    var error = '';
    $("#homeworkSearchForm").find('input,select,textarea').each(function () {

        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n";

            }

        }

        params[$(this).attr('name')] = $.trim($(this).val());



    });
    if (err == 0) {

        console.log(params);

        $.ajax({
            type: "POST",
            url: commonSettings.homeworkAjaxurl + "/loadHomeWorkBYClass",
            data: params,
            dataType: 'html',
            success: function (res) {
                if (res) {
                    $('#loadHomeworkReport').html(res);
                    $('#loadHomeworkReport').removeClass('hide');
                }
            }
        });


    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
        return false;
    }


//    window.open(commonSettings.templateAjaxurl + '/overallFeereceipt?studid=' + stuid + '&&fromdate=' + fromdate + '&&todate=' + todate, '_blank');
};


homeworkSettings.sendHomeworkReport = function () {

    var params = {};
    var err = 0;
    var error = '';
    $("#homeworkSearchForm").find('input,select,textarea').each(function () {

        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n";

            }

        }

        params[$(this).attr('name')] = $.trim($(this).val());



    });
    if (err == 0) {

        console.log(params);

        $('.cnfBox-title').html('Home Work Message');
        $('.cnfBox-body').html('<div class="alert alert-block alert-danger fade in"><strong> Do you want send sms..?</strong></div>');
        $('#ConfirmModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#confirm_yes').unbind('click').click(function () {
            $('#ConfirmModal').modal('hide');

            var current_effect = 'bounce';
            //stuFeelist
            homeworkSettings.run_waitMe(current_effect);

            var recalculate_transctn = 'yes';
            var change_ldgr_balnc = 'yes';

            $.ajax({
                type: "POST",
                url: commonSettings.homeworkAjaxurl + "/sendHomeWorkReport",
                data: params,
                dataType: 'JSON',
                success: function (res) {
                    if (res) {
                        $('#other_section').waitMe('hide');
                        var lblcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                        $('.modal-title').html("Message");
                        $('.modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                        $('#messageBoxModal').modal('show');
                        $('#msg_ok').unbind('click').click(function () {
                            $('#messageBox').click();
                        });
                    }
                }
            });
        });

        $('#confirm_no').unbind('click').click(function () {

        });

    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
        return false;
    }


//    window.open(commonSettings.templateAjaxurl + '/overallFeereceipt?studid=' + stuid + '&&fromdate=' + fromdate + '&&todate=' + todate, '_blank');
};

homeworkSettings.run_waitMe = function (effect) {
    $('#other_section').waitMe({
        effect: effect,
        text: 'Please wait message sending...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: ''
    });
};


homeworkSettings.loadSubtree = function (ctrl) {
    console.log($(ctrl).parent().parent().next('.subtreediv').html())
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.homeworkAjaxurl + "/loadSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};

homeworkSettings.deleteSelectedHomework = function (homeworkid) {
    if (homeworkid) {
        $.ajax({
            type: "POST",
            url: commonSettings.homeworkAjaxurl + "/deleteSelectedHomework",
            data: {homeworkid: homeworkid},
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    var lblcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                    $('.modal-title').html("Message");
                    $('.modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
//                            $('#messageBox').click();
                        if (res.type === 'success') {
                            homeworkSettings.loadHomeworDet();
                        }
                        $('#messageBoxModal').modal('hide');

                    });
                }
            }
        });
    }
};