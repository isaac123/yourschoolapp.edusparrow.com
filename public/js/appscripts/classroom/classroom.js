var classroomSettings = {};

var attSettings = {};
attSettings.effect = 'slide';
attSettings.options = {direction: "right"};
attSettings.duration = 500;

classroomSettings.loadCombination = function() {
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/studentAttendance",
        dataType: "html",
        success: function(res) {
            $('#attcombiDiv').html(res);
            $('#attcombiDiv').removeClass('hide')
        }
    });
};
classroomSettings.loadXamCombination = function() {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/listCombination",
        dataType: "html",
        success: function(res) {
            $('#xamcombiDiv').html(res);
            $('#xamcombiDiv').removeClass('hide')
        }
    });
};
classroomSettings.loadTestCombination = function() {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/listClassTestCombination",
        dataType: "html",
        success: function(res) {
            $('#tstcombiDiv').html(res);
            $('#tstcombiDiv').removeClass('hide')
        }
    });
};
classroomSettings.loadAssignCombination = function() {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.assignmentAjaxurl + "/addAssignment",
        dataType: "html",
        success: function(res) {
            $('#assigncombiDiv').html(res);
            $('#assigncombiDiv').removeClass('hide')
        }
    });
};
classroomSettings.loadclsRatCombination = function() {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/addClassteacherRating",
        dataType: "html",
        success: function(res) {
            $('#classRating').html(res);
            $('#classRating').removeClass('hide')
        }
    });
};
classroomSettings.loadsubjRatCombination = function() {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/addStudentRating",
        dataType: "html",
        success: function(res) {
            $('#subjRating').html(res);
            $('#subjRating').removeClass('hide')
        }
    });
};

classroomSettings.loadsubjHomeworkCombination = function() {
    $.ajax({
        type: "POST",
        url: commonSettings.homeworkAjaxurl + "/addStudentHomework",
        dataType: "html",
        success: function(res) {
            $('#subjhomeworkcombiDiv').html(res);
            $('#subjhomeworkcombiDiv').removeClass('hide')
        }
    });
};

classroomSettings.loadclsHomeworkCombination = function() {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.homeworkAjaxurl + "/addClassteacherHomework",
        dataType: "html",
        success: function(res) {
            $('#classhomeworkcombiDiv').html(res);
            $('#classhomeworkcombiDiv').removeClass('hide')
        }
    });
};

classroomSettings.loadVideoCombination = function() {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.videosAjaxUrl + "/studentClass",
        dataType: "html",
        success: function(res) {
            $('#videoscombiDiv').html(res);
            $('#videoscombiDiv').removeClass('hide')
        }
    });
};

classroomSettings.loadExamReport = function() {
    console.log('examReport')
    $.ajax({
        type: "POST",
        url: commonSettings.examReportAjaxUrl + "/mainExamReport",
        dataType: "html",
        success: function(res) {
            $('#examReportDiv').html(res);
            $('#examReportDiv').removeClass('hide')
        }
    });
};
classroomSettings.loadClasTestReport = function() {
    console.log('clasReport')
    $.ajax({
        type: "POST",
        url: commonSettings.examReportAjaxUrl + "/clsTestReport",
        dataType: "html",
        success: function(res) {
            $('#clstestReportDiv').html(res);
            $('#clstestReportDiv').removeClass('hide')
        }
    });
};
classroomSettings.loadRatingReport = function() {
    $.ajax({
        type: "POST",
        url: commonSettings.examReportAjaxUrl + "/ratingReport",
        dataType: "html",
        success: function(res) {
            $('#ratingReportDiv').html(res);
            $('#ratingReportDiv').removeClass('hide')
        }
    });
};

classroomSettings.loadhomeworkReport = function() {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.homeworkAjaxurl + "/stuSearch",
        dataType: "html",
        success: function(res) {
            $('#homeworkDiv').html(res);
            $('#homeworkDiv').removeClass('hide')
        }
    });
};
classroomSettings.loadHomeworkReport = function() {
    console.log('homeworkReport')
    $.ajax({
        type: "POST",
        url: commonSettings.homeworkAjaxurl + "/stuSearch",
        dataType: "html",
        success: function(res) {
            $('#homeworkreportDiv').html(res);
            $('#homeworkreportDiv').removeClass('hide')
        }
    });
};
classroomSettings.loadClassRatingReport = function() {
    $.ajax({
        type: "POST",
        url: commonSettings.academicAjaxurl + "/loadClassRatingReport",
        dataType: "html",
        success: function(res) {
            $('#classReportDetDiv').html(res);
            $('#classReportDetDiv').removeClass('hide');
        }
    });
};
attSettings.closeDailyReport = function(ctrl) {
    $("#report_btn").click()
};
attSettings.closeMonthlyReport = function(ctrl) {
    $("#report_btn").click()
};

classroomSettings.closeClasstestReport = function(ctrl) {
    $("#test_report_btn").click()
};
classroomSettings.closeAssignmtReport = function(ctrl) {
    $("#assignment_report_btn").click()
};
classroomSettings.closeRatingReport = function(ctrl) {
    $("#rating_report_btn").click()
};