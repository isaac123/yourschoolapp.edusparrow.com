var applicationSettings = {};
applicationSettings.fromPage = '';
applicationSettings.stuapplndata = {};


applicationSettings.changeAppointmntNo = function (acdmcyrid)
{
    //alert(acdmcyrid);
    if (acdmcyrid != 0)
    {
        $.ajax({
            type: "POST",
            url: commonSettings.applnAjaxurl + "/changeAppointmntNo",
            data: {acdmcyrid: acdmcyrid},
            dataType: "html",
            success: function (res) {
                //  alert(res);
                $('#application_no').val(res);

            }
        });
    }
    else
    {
        swal({
            title: "Error",
            text: "Please select academic year",
            type: "error",
            confirmButtonText: 'OK',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.reload();
                    }
                });
    }
};

applicationSettings.changeStatus = function (statusData) {
    $.ajax({
        type: "POST",
        url: commonSettings.applnAjaxurl + "/changeApprovalStatus",
        data: statusData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                if (statusData.fromPage == 'allapproval' || statusData.fromPage == 'ApprovalView') {
                    if(statusData.fromPage == 'ApprovalView'){
                        viewapproval.modal('hide'); 
                    }
                    var n_dom = [], Dtype = 'information';
                    Dlayout = 'topRight';
                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a> Approval Status Updated <span> </span> </div> </div>';
                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                    announcementSettings.reload_inbox();
                } else {
                    var lblcls = res.type == 'error' ? 'danger' : 'success';
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                    $('#messageBoxModal').modal();
                    $('#msg_ok').unbind('click').click(function (event) {
                        $('#messageBoxModal').modal('hide');
                        if (res['type'] == 'success') {
                            if (statusData.fromPage == 'Application') {
                                manageAppSettings.loadApplicationList();
                            }
                            else if (statusData.fromPage == 'ApprovalView') {
                                window.location.reload();
                            } else {
                                approvalviewSettings.loadApplicationList();
                            }
                        }
                    });
                    return false;
                }
            }
        }
    });
};


applicationSettings.viewAppDetails = function (appID) {

    $.ajax({
        type: "POST",
        url: commonSettings.applnAjaxurl + "/viewApplicationByID",
        data: {
            appID: appID
        },
        dataType: "html",
        success: function (res) {
            $('.msgBox-title').html('View Application Details');
            $('.msgBox-body').html(res);
            $('#messageBox').click();

        }
    });
};

applicationSettings.loadSections = function (sval) {
    console.log(sval);
    applicationSettings.classID = sval;
    $.ajax({
        type: "POST",
        url: commonSettings.applnAjaxurl + "/loadSection",
        data: {
            classId: sval
        },
        success: function (res) {
            if (res) {
                $('#secDet').removeClass('hide')
                $('#secDet').html(res);
            }

        }
    });

};

applicationSettings.admitStudent = function (ctrl) {
    var appData = {};
    appData.itemID = $(ctrl).closest('tr').attr('applicationId');
    admissionSettings.admitStudent(appData)
};
applicationSettings.loadApplicationForm = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.applnAjaxurl + "/index",
        success: function (res) {
            if (res) {
                $('#formModal .formBox-title').html('Issue Application');
                $('#formModal #confirm_save').html('Issue');
                $('#formModal .formBox-body').html(res);
                $('#formModal .modal-dialog').css('width', '1000px')
                $('#formModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#confirm_save').unbind('click').click(function () {
                    $('#formModal').modal('hide')
//                    applicationSettings.issueNewApplication()
                    applicationSettings.issueApplication()
                });
            }
        }
    });

};

applicationSettings.loadPayForm = function (applicationid)
{
    var applnData = {};

    $.ajax({
        type: "POST",
        url: commonSettings.applnAjaxurl + "/isCheckModule",
        data: {applicationid: applicationid},
        dataType: 'JSON',
        success: function (res) {
            if (res.type === 'yes') {
                applnData.type = 'Application';
                applnData.itemID = res['ItemID'];
                applnData.status = res['status'];
                applnData.recordItemId = res.ItemID;
                applnData.recordTypId = res.recordTypId;
                applnData.fromPage = 'Application';
                applnData.fromModule = 'ApplicationPayFee';

                applicationSettings.stuapplndata.type = 'Application';
                applicationSettings.stuapplndata.itemID = res['ItemID'];
                applicationSettings.stuapplndata.status = res['status'];
                applicationSettings.stuapplndata.recordItemId = res.ItemID;
                applicationSettings.stuapplndata.recordTypId = res.recordTypId;
                applicationSettings.stuapplndata.fromPage = 'Application';
                applicationSettings.stuapplndata.fromModule = 'ApplicationPayFee';

                $.ajax({
                    type: "POST",
                    url: commonSettings.applnAjaxurl + "/payApplicationFeeForm",
                    success: function (res) {
                        if (res) {
                            $('#formModal .formBox-title').html('Application Fee');
                            $('#formModal #confirm_save').html('Pay');
                            $('#formModal .formBox-body').html(res);
                            // $('#formModal .modal-dialog').css('width', '1000px')
                            $('#formModal').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            $('#confirm_close').addClass('hide');
                            $('#iconclose').addClass('hide');

                            $('#confirm_save').unbind('click').click(function () {
                                $('#confirm_close').click();
                                console.log(applnData);
                                applicationSettings.payApplicationFeeSave(applnData);
                            });
                        }
                    }
                });
            } else {
                $('.modal-title').html('Message');
                $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Please upgrate your package.</div>');
                $('#messageBox').click();
            }
        }
    });
};

applicationSettings.payApplicationFee = function (applctndata)
{
    $.ajax({
        type: "POST",
        url: commonSettings.applnAjaxurl + "/isCheckModule",
        dataType: 'JSON',
        success: function (res) {
            if (res.type === 'yes') {
                $.ajax({
                    type: "POST",
                    url: commonSettings.applnAjaxurl + "/payApplicationFeeForm",
                    success: function (res) {
                        if (res) {
                            $('#formModal .formBox-title').html('Application Fee');
                            $('#formModal #confirm_save').html('Pay');
                            $('#formModal .formBox-body').html(res);
                            // $('#formModal .modal-dialog').css('width', '1000px')
                            $('#formModal').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            $('#confirm_close').addClass('hide');
                            $('#iconclose').addClass('hide');

                            $('#confirm_save').unbind('click').click(function () {
                                $('#confirm_close').click();
                                applicationSettings.payApplicationFeeSave(applctndata);
                            });
                        }
                    }
                });
            } else {
                approvalSettings.checkReqStatus(applctndata);
            }
        }
    });
};

applicationSettings.payApplicationFeeSave = function (applctndata)
{
    var appFeeData = {};

    var err = 0;
    var error = '';
    var debitledger = $('#debitledgerid').val();
    var creditledger = $('#creditledgerid').val();
    var voucheramount = $('#voucheramount').val();
    var receivedby = $('#receivedbyid').val();
    var voucheramountfor = $('#voucheramountfor').val();
    var vouchertyp = $('#vouchertyp').val();

//alert('tesg');

    $("#payFeeForm").find('input,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '' || $.trim($(this).val()) == '0') {
                err = 1;
                error += $(this).attr('title') + " is required!<br>";
            } else {
                appFeeData[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            appFeeData[$(this).attr('name')] = $.trim($(this).val());

        }
    });
    if (!$.isNumeric(voucheramount) || voucheramount < 1) {
        error = error + 'Invalid Amount <br>';
        err = 1;
    }

    if (err == 1) {
        $('#messageBoxModal .msgBox-title').html('Message');
        $('#messageBoxModal .msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#msg_ok').unbind('click').click();
            $('#formModal').modal('show')
            err = 0;
            return false;
        });
    } else {

        appFeeData.recordItemId = applctndata.recordItemId;
        appFeeData.recordTypId = applctndata.recordTypId;
        applicationSettings.stuapplndata.voucheramount = voucheramount;
        applicationSettings.stuapplndata.voucheramountfor = voucheramountfor;

//        appFeeData.type = 'Application';
        appFeeData.itemID = applctndata.itemID;
        appFeeData.status = 'Requested';
        appFeeData.fromPage = 'Application';
        appFeeData.fromModule = applctndata.fromModule;

        voucherSettings.makeNewVoucher(appFeeData);
    }
};
applicationSettings.loadSubtree = function (ctrl)
{
    $(ctrl).parent().parent().next('.subtreediv')
            .removeClass('hide')
            .html(' <div class="form-group">  <span class="col-sm-8 pull-right help-block"><i class="fa fa-spin fa-spinner" ></i> Loading please wait...</span>  </div>')

    var orgvalueid = $(ctrl).val();
    if (orgvalueid != '') {
        $.ajax({
            type: "POST",
            url: commonSettings.applnAjaxurl + "/loadSubtree",
            data: {orgvalueid: orgvalueid},
            dataType: "html",
            success: function (res) {
                $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
                $(ctrl).parent().parent().next('.subtreediv').html(res)
            }
        });
    } else
    {
        //alert('test')
        $(ctrl).parent().parent().next('.subtreediv').addClass('hide')
    }
};

applicationSettings.issueApplication = function () {
    var applnData = {};
    var alphabet_pattren = /^[a-zA-Z ]*$/;
    var error = '';
    var valid = 0;
    $("#applicationForm").find('input,select,textarea').each(function () {
        applnData[$(this).attr('name')] = $.trim($(this).val());

        applicationSettings.stuapplndata[$(this).attr('name')] = $.trim($(this).val());

        if ($(this).attr('name') == 'studentName')
        {
            if (!alphabet_pattren.test($.trim($(this).val())))
            {
                alphabedcheck = 1;
                valid = 1;
                error += "Student name is invalid!\nhint: Only Alphabets are allowed";

            }

        }
//        console.log($(this).parent().prev('label').html())
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                valid = 1;
                error += $(this).attr('title') + " is required!\n";

            }

        }
    });


    console.log(error)
    console.log(applnData)
    if (valid == 0)
    {
        $.ajax({
            type: "POST",
            url: commonSettings.applnAjaxurl + "/issueApplication",
            data: applnData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    if (res['type'] == 'error') {
                        $('.msgBox-title').html('<i class="fa  fa-times"></i> Error');
                        $('.msgBox-body').html('<span class="text text-danger fade in">' + res.message + '</span>');
                        $('#messageBoxModal').modal('show');
                        $('#msg_ok').unbind('click').click(function (event) {
                            $('#messageBoxModal').modal('hide');
                            $('#formModal').modal('show')
                        });
                        return false;

                    }
                    else if (res['type'] == 'success') {
                        applicationSettings.fromPage = 'Application';
                        applnData.type = 'Application';
                        applnData.itemID = res['ItemID'];
                        applnData.status = 'Requested';
                        applnData.fromPage = 'Application'; //recordTypId
                        console.log(applnData)
                        approvalSettings.checkReqStatus(applnData);
                    }


                }
            }
        });
    } else {

        $('.msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('.msgBox-body').html('<span class="text text-danger fade in">' + error + '</span>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function (event) {
            $('#messageBoxModal').modal('hide');
            $('#formModal').modal('show')
        });
        return false;

    }

};

applicationSettings.loadeditprofile = function (stuid) {
    $.ajax({
        type: "GET",
        url: commonSettings.profileAjaxurl + "/editStudentProfile?student_id=" + stuid,
        dataType: "html",
        success: function (res) {
            $('#application').html(res);
        }
    });
};
applicationSettings.forwardDetails = function (app_id) {
    console.log(app_id);
    var applnData = {};
    applicationSettings.fromPage = 'Application';
    applnData.type = 'Application';
    applnData.itemID = app_id;
    applnData.status = 'Requested';
    applnData.fromPage = 'Application';
    console.log(applnData)
    approvalSettings.checkReqStatus(applnData);
};
applicationSettings.generateExcel = function (params) {
//    conole.log(params)
    var newForm = $('<form>', {
        'action': commonSettings.applnAjaxurl + "/generateApplExcel",
        'target': '_blank',
        'id': 'generateApplExcel',
        'method': 'POST'
    });
    for (var i = 0; i < params.length; i++) {
        newForm.append($('<input>', {
            'name': params[i]['name'],
            'value': params[i]['value'],
            'type': 'hidden'
        }));
    }
    newForm.appendTo("body").submit();
};