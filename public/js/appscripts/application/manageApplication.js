var manageAppSettings = {};
 manageAppSettings.request='';

manageAppSettings.viewOrEditApplication = function(ctrl) {
    var appData = {};
    appData.applicationId = $(ctrl).closest('tr').attr('applicationId');
    $.ajax({
        type: "POST",
        url: commonSettings.applnAjaxurl + "/manageApplication",
        data: appData,
        dataType: "JSON",
        success: function(res) {
            $('.formBox-title').html('Detailed view of the Applicant');
            $('.formBox-body').html(res);
            $('#formModal').modal('show');
            $('#confirm_save').unbind('click').click(function() {
                $('#confirm_close').click();
            });

        }
    });
}

//manageAppSettings.loadApplicationList = function()
//{
//    console.log('im here')
//    var applnSearchData = {};
//    var params = {};
//    var academic = "";
//    applnSearchData.emptySearch = 1;
//    //studentProfileSearchForm
//    
////    manageAppSettings.request='call';
//
//    $("#studentApplicationForm").find('input,select,textarea').each(function() {
//        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
//        if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
//            applnSearchData[$(this).attr('name')] = $.trim($(this).val());
//            applnSearchData.emptySearch = 0
//        }
//
//    });
//
//    params = applnSearchData;
//
//    console.log(params);
//    $.ajax({
//        type: "POST",
//        url: commonSettings.applnAjaxurl + "/applicationTblHdr",
//        dataType: "html",
//        success: function(res) {
//            $('#ApplicationTableDiv').html(res);
//            var tablectrl = '#studentApplicationList';
//            var column = [0, 1, 2, 3, 4, 5, 6];
//            var sortingDisabled = [7];
//            var appdynamicTabl = $(tablectrl).dataTable({
////                "sDom": 'C<"clear">lrtip',
//                "sDom": '<"top"if>rt<"bottopm"p><"clear">',
//                "bProcessing": true,
//                "bServerSide": true,
//                "sAjaxSource": commonSettings.applnAjaxurl + "/loadApplicationData",
//                "sPaginationType": "full_numbers",
//                "iDisplayLength": commonSettings.NumberOfRecords,
//                // 'iDisplayStart':patientsapp.startIndex,
//                "aoColumnDefs": [{
//                        "sDefaultContent": "",
//                        "aTargets": column
//                    }, {
//                        "bSortable": false,
//                        "sDefaultContent": "",
//                        "aTargets": sortingDisabled
//                    }],
//                "aaSorting": [[0, "desc"]],
//                "bLengthChange": false,
////                "bInfo": false,
//                "fnServerData": function(surl, aoData, l) {
//                    for (var key in params) {
//                        aoData.push({
//                            "name": key,
//                            "value": params[key]
//                        });
//                    }
//                    $.ajax({
//                        dataType: "json",
//                        type: "POST",
//                        url: surl,
//                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
//                        data: aoData,
//                        success: l
//                    });
//                },
//                "fnDrawCallback": function(o) {
//                    if (o._iRecordsDisplay <= commonSettings.NumberOfRecords) {
//                        $(tablectrl + '_paginate').hide();
//                    }
//                    $('.dataTables_filter').addClass('col-md-5');
//                    $('.dataTables_filter').find('label').addClass('col-md-3').css('display', ' inline');
//                    $('.dataTables_filter').find('input').addClass('form-control pull-right');
//                },
//                "fnRowCallback": function(nRow, aData, iDisplayIndex) {
//
//                    var rowText = '<tr applicationId="' + aData.application_id + '">';
//
//                    rowText += '<td><a href= "javascript:;" onclick="applicationSettings.viewAppDetails(' + aData.application_id + ')">';
//                    rowText += aData.application_no;
//                    rowText += '</a></td>';
//                    rowText += '<td>';
//                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.Student_Name + '</span>';
//                    rowText += '</td>';
//
//                    rowText += '<td>';
//                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.gender + '</span>';
//                    rowText += '</td>';
//
//                    rowText += '<td>';
//                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.dob + '</span>';
//                    rowText += '</td>';
//
//                    rowText += '<td>';
//                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.doj + '</span>';
//                    rowText += '</td>';
//
//                    rowText += '<td>';
//                    rowText += '<span class="label label-' + aData.labelclass + ' label-mini">' + aData.status + '</span>';
//                    rowText += '</td>';
//
//
//                    rowText += '<td>';
//                    rowText += '<span class="label label-' + aData.dataclass + ' label-mini">' + aData.key_in_status + '</span>';
//                    rowText += '</td>';
//
//
//
//                    rowText += '<td>';
//                    rowText += aData.Actions;
//                    rowText += '</td>';
//
//                    rowText += '</tr>';
//                    var newRow = $(rowText);
//                    $(nRow).html(newRow.html());
//                    commonSettings.copyAttributes(newRow, nRow);
//                    return nRow;
//                }
//            });
//        }
//    });
//
//
//};
