var feedemandSettings = {};

feedemandSettings.getStudentList = function (feeItemId, classcombi) {
//    $.ajax({
//        type: "GET",
//        url: commonSettings.feeDemandUrl + "/getStudentList?fee_item_id=" + feeItemId + "&class_combi=" + classcombi,
//        dataType: "html",
//        success: function (res) {
//            $('#classroom').html(res);
//        }
//    });
    
    var newForm = $('<form>', {
        'action': commonSettings.feeDemandUrl + "/getStudentList",
        'target': '_blank',
        'id': 'StufeeDemand',
        'method': 'GET'
    })
    newForm.append($('<input>', {
        'name': 'fee_item_id',
        'value': feeItemId,
        'type': 'hidden'
    }));
    newForm.append($('<input>', {
        'name': 'class_combi',
        'value': classcombi,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
}
feedemandSettings.getClassWiseDemand = function (feenodes) {
    var newForm = $('<form>', {
        'action': commonSettings.feeDemandUrl + "/getClassWiseDemand",
        'target': '_blank',
        'id': 'feeDaemand',
        'method': 'POST'
    })
    newForm.append($('<input>', {
        'name': 'feenodes',
        'value': feenodes,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
}
feedemandSettings.loadOverallFeeDemand = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.feeDemandUrl + "/childTable",
        dataType: "html",
        success: function (res) {
            $('#feedemandDiv').html(res);
            $('#feedemandDiv').removeClass('hide')
        }
    });
};
feedemandSettings.loadDaywiseFeeReport = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.feeDemandUrl + "/loadDaywiseFeeReport",
        dataType: "html",
        success: function (res) {
            $('#feereportDiv').html(res);
            $('#feereportDiv').removeClass('hide')
        }
    });
};
feedemandSettings.loadStuFeeReport = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/loadFeeReport",
        dataType: "html",
        success: function (res) {

            $('#stufeereportDiv').html(res);
            $('#stufeereportDiv').removeClass('hide');
        }
    });
};

feedemandSettings.loadSubtree = function (ctrl) {
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.feeDemandUrl + "/loadSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};
feedemandSettings.loadFeeAssSubtree = function (ctrl) {
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.feeDemandUrl + "/loadFeeAssSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.feeDetdiv').removeClass('hide')
            $(ctrl).parent().parent().next('.feeDetdiv').html(res)
        }
    });
};
feedemandSettings.loadDailyFeeReportStudents = function () {
    var searchData = {};
    var err = 0;
    var error = '';
    $("#StuFeeReportForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n";

            } else {
                searchData[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            searchData[$(this).attr('name')] = $.trim($(this).val());
        }

        searchData[$(this).attr('name')] = $.trim($(this).val());
    });
    if (err == 0) {
        var current_effect = 'bounce';
        feedemandSettings.run_waitMe(current_effect);
        var recalculate_transctn = 'yes';
        var change_ldgr_balnc = 'yes';
        $.ajax({
            type: "POST",
            url: commonSettings.feeDemandUrl + "/loadDailyFeeReportStudents",
            data: searchData,
            dataType: "html",
            success: function (res) {
                $('#other_section').waitMe('hide');
                $('#StudentFeeReportDetdaywise').html(res);
                $('#StudentFeeReportDetdaywise').removeClass('hide');
            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        return false;
    }
};


feedemandSettings.run_waitMe = function (effect) {
    $('#other_section').waitMe({
        effect: effect,
        text: 'Please wait loading...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: ''
    });
};
feedemandSettings.dailyFeeExcelReportStudents = function () {
    var params = {};
    var err = 0;
    var error = '';

    $("#StuFeeReportForm").find('input,select,textarea').each(function () {
        params[$(this).attr('name')] = $.trim($(this).val());

    });
    params['limit'] = 500;
    event.preventDefault();
    event.stopPropagation();
    var newForm = $('<form>', {
        'action': commonSettings.feeDemandUrl + "/dailyFeeExcelReportStudents",
        'target': '_blank',
        'id': 'StudentList',
        'method': 'POST'
    });
    newForm.append($('<input>', {
        'name': 'params',
        'value': JSON.stringify(params),
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();

};
feedemandSettings.loadClasswiseExcelReport = function (fee_node) {
    var params = {};
    params['feenode'] = fee_node;
    params['limit'] = 500;
    event.preventDefault();
    event.stopPropagation();
    var newForm = $('<form>', {
        'action': commonSettings.feeDemandUrl + "/loadClasswiseExcelReport",
        'target': '_blank',
        'id': 'FeeReport',
        'method': 'POST'
    });
    newForm.append($('<input>', {
        'name': 'params',
        'value': JSON.stringify(params),
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();

};
feedemandSettings.loadStudentwiseExcelReport = function (feeitem_id, classcombin_id) {
    var params = {};
    params['feeitem_id'] = feeitem_id;
    params['classcombin_id'] = classcombin_id;
    params['limit'] = 500;
    console.log(params);
    event.preventDefault();
    event.stopPropagation();
    var newForm = $('<form>', {
        'action': commonSettings.feeDemandUrl + "/loadStudentwiseExcelReport",
        'target': '_blank',
        'id': 'FeeReportdet',
        'method': 'POST'
    });
    newForm.append($('<input>', {
        'name': 'params',
        'value': JSON.stringify(params),
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();

};