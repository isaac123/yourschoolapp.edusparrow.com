var roleResSettings={};
roleResSettings.assignRoleMenuRes = function (ctrl) {
    
    var permissionData = {};
    var removeData = {};
    var assignData  ={};
    $("#assignRolesMenuResForm").find('input:checked').each(function () {      
        assignData[$(this).attr('name')] = $.trim($(this).val());
       
    });
    $("#assignRolesMenuResForm").find('input:not(:checked)').each(function () {      
        if($(this).attr('name')!='assignRolMenRes')
        removeData[$(this).attr('name')] = 0;
       
    });
    permissionData.on = assignData;
    permissionData.off = removeData;
    
    console.log(permissionData);
     $.ajax({
            type: "POST",
            url:"assignRolesMenu",
            data: permissionData,
            success: function(res) {
                if (res) {
                    //alert(res)
                     $('.modal-title').html('Message');
                            $('.modal-body').html(res);
                            $('#messageBox').click();
                            $('#msg_ok').click(function(){
                                window.location.reload();
                            });
                } 

            }
        });
    
};

roleResSettings.loadAlRolesByStaffId = function () {
    var stflogin =  $('#stflogin').val()
    
     $.ajax({
            type: "POST",
            url:"loadStaffRoles",
            data: {stflogin:stflogin},
            dataType:'HTML',
            success: function(res) {
                if (res) {
                    $('#staffUserRole').removeClass('hide')
                    $('#staffUserRole').html(res)
                } 

            }
        });
};
roleResSettings.assignUserRole  = function () {
    var permissionData = {};
    var removeData = {};
    var assignData = {};
    $("#assignRoleForm").find('input.indv:checked').each(function () {
        assignData[$(this).attr('name')] = $.trim($(this).val());

    });
    $("#assignRoleForm").find('input.indv:not(:checked)').each(function () {
        if ($(this).attr('name') != 'assignUsrRol')
            removeData[$(this).attr('name')] = 0;

    });
    permissionData.on = assignData;
    permissionData.off = removeData;

    console.log(permissionData);
    $.ajax({
        type: "POST",
        url: commonSettings.usersroleAjaxurl + "/assignUsrRoles",
        data: permissionData,
        dataType:"JSON",
        success: function (res) {
            if (res) {
               console.log(res)
                $('.modal-title').html(res.type);
                $('.modal-body').html(res.message);
                $('#messageBox').click();
                $('#msg_ok').click(function () {
                   roleResSettings.loadAlRolesByStaffId()
                });
            }

        }
    });
};