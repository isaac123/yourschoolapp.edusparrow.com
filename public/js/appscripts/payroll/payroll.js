var staffPayrollSettings = {};
staffPayrollSettings.loadStaffdet = function () {
    var applnData = {};
    $("#staffPayrollGenForm").find('input').each(function () {
        applnData[$(this).attr('name')] = $.trim($(this).val());
    });
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/getPayrollList",
        data: applnData,
        dataType: "Html",
        success: function (res) {
            $('#payformSal').addClass('hide')
            $('#loadPayReport').parents('section').removeClass('hide')
            $('#loadPayReport').html(res)
        }
    });
};
staffPayrollSettings.loadStaffSalaryExcel = function () {
    var applnData = {};
    $("#staffPayrollGenForm").find('input').each(function () {
        applnData[$(this).attr('name')] = $.trim($(this).val());
    });
    event.preventDefault();
    event.stopPropagation();
    var newForm = $('<form>', {
        'action': commonSettings.salaryUrl + "/loadStaffSalaryExcel",
        'target': '_blank',
        'id': 'salaryReport',
        'method': 'POST'
    });
    newForm.append($('<input>', {
        'name': 'applnData',
        'value': JSON.stringify(applnData),
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();

};
staffPayrollSettings.loadFinanceCycleStaffdet = function () {
    var applnData = {};
    $("#staffPayrollGenForm").find('input').each(function () {
        applnData[$(this).attr('name')] = $.trim($(this).val());
    });
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/financeCyclegetPayrollList",
        data: applnData,
        dataType: "Html",
        success: function (res) {
            $('#loadPayReport').parents('section').removeClass('hide')
            $('#loadPayReport').html(res)
        }
    });
};
staffPayrollSettings.saveSalaryByMonth = function (ctrl, stfid) {

    var applnData = {};
    var elem = $(ctrl).parents('tr')
    elem.find('td.salmonthlyitems').each(function () {
        applnData[$(this).attr('fieldname')] = $.trim($(this).html());
    });
    applnData['stfid'] = stfid;
    applnData['monthyear'] = $(ctrl).parents('table').attr('monthyr');
    console.log(applnData)
    $('.loading_addfee_' + applnData.staffid).removeClass('hide')
    $('.done_loading_addfee_' + applnData.staffid).addClass('hide')

    $(ctrl).addClass('hide')
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/saveStaffSalaryBtMonth",
        data: applnData,
        dataType: "JSON",
        success: function (res) {

            $('.loading_addfee_' + applnData.staffid).addClass('hide')
            $('.done_loading_addfee_' + applnData.staffid).removeClass('hide')

            if (res['type'] === 'success') {
                swal("Success", res['message'], res['type']);
                setTimeout(function () {
                    staffPayrollSettings.loadStaffdet()
                }, 2000);
            } else {
                swal("Error", res['message'], res['type'])
            }
        }
    });
};

staffPayrollSettings.submitVoucherSal = function () {
    var applnData = {};
    $("#StaffPayrollPermonth").find(':checked').each(function () {
        applnData[$(this).attr('id')] = $.trim($(this).val());
    });
    applnData['monthyr'] = $("#StaffPayrollPermonth").attr('monthyr');
    $('#payformSal').html('')
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/makeSalVoucher",
        data: applnData,
        dataType: "Html",
        success: function (res) {
            $('#payformSal').removeClass('hide')
            $('#payformSal').html(res)
        }
    });
};
staffPayrollSettings.addNewSalVoucher = function () {

    var applnData = {};
    var err = 0;
    var error = '';


    var debitledger = $('#debitledgerid').val();
    var creditledger = $('#creditledgerid').val();
    var voucheramount = $('#voucheramount').val();
    var receivedby = $('#receivedbyid').val();
    var voucheramountfor = $('#voucheramountfor').val();
    var apprvl_req = $('#apprvl_req').val();
    var apprvlmtamt = $('#apprvlmtamt').val();
    var vouchertyp = $('#vouchertyp').val();
    applnData.apprvl_req = apprvl_req;
    applnData.apprvlmtamt = apprvlmtamt;
    applnData.vouchertyp = vouchertyp;

//    applnData.paymentamount = paymentamount;

    if (debitledger === '') {
        error = error + 'Ledger for Debit missing"\n"';
        err = 1;
    }
    if (creditledger === '') {
        error = error + 'Ledger for Credit missing"\n"';
        err = 1;
    }
    if (voucheramount === '') {
        error = error + 'Amount missing"\n"';
        err = 1;
    }
    else
    {
        if (voucheramount < 0)
        {
            error = error + 'Amount cannot be Negative"\n"';
            err = 1;
        }
    }

    if (receivedby === '') {
        error = error + 'Received by missing"\n"';
        err = 1;
    }

    if (voucheramountfor === '') {
        error = error + 'Amount for is missing"\n"';
        err = 1;
    }

    $("#addNewSalPaymentvouchers").find('input,textarea').each(function () {
        applnData[$(this).attr('name')] = $.trim($(this).val());
    });
    if (err === 0) {
        $('.loading_addsalVou   ').removeClass('hide')
        $('#app_addsalVou').addClass('hide')
        $("#addNewSalPaymentvouchers :input").prop('readonly', true);

        applnData.recordItemId = $("#sal_ref_ids").val();
        applnData.recordTypId = '5';
        applnData.fromPage = 'Payroll';
        voucherSettings.makeNewVoucher(applnData);
    }
    else
    {
        swal("Error", error, "error");
    }
};
staffPayrollSettings.changeStatus = function (applnData) {
    console.log(applnData)
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/updatePayrollStatus",
        data: applnData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                if (res) {

                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
    console.log(commonSettings.fromPage)
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        else if (res['type'] == 'success') {
                            if (commonSettings.fromPage == 'Payroll') {
                                staffPayrollSettings.loadStaffdet()
                            } else if (commonSettings.fromPage == 'VoucherApproval') {
                                approvalSettings.loadFinanceApprovalList();
                            } else if (commonSettings.fromPage == 'VoucherList') {
                                voucherSettings.loadvoucherList();
                            }
                        }
                    });
                }
            }
        }
    });
};

staffPayrollSettings.savPayrollAccountSett = function () {
    var rowvalues = new Array();
    $("#salaryAccountSett > tbody > ").each(function () {
        var data = {};
        data.paytype = $(this).find('.paytype').val();
        data.creditledgersalid = $(this).find('.creditledgersalid').val();
        data.debitledgerid = $(this).find('.debitledgerid').val();
        data.formula_sal = $(this).find('.formula_sal').val();
        rowvalues.push(data)

    });
    console.log(rowvalues)
    if (rowvalues.length > 0) {

        $.ajax({
            type: "POST",
            url: commonSettings.payrollUrl + "/savePayrollAccountSettings",
            data: {rowvalues: rowvalues},
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            //  $('#formModal').modal();
                        }
                        else if (res['type'] == 'success') {
                            salarySettings.loadSalarySettings();
                        }
                    });

                }

            }
        });
    } else {

        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('Add atleast a row');
        $('#messageBoxModal').modal();
        return false;
    }
};

staffPayrollSettings.viewAttendace= function (monthyr, stafid) {
     
    var newForm = $('<form>', {
        'action': commonSettings.payrollUrl + "/viewStaffAttByMonth",
        'target': '_blank',
        'id': 'staffAttExcelForm',
        'method':'POST',
    }).append($('<input>', {
            'name': 'monthyr',
            'value': monthyr,
            'type': 'hidden'
        })).append($('<input>', {
            'name': 'stafid',
            'value': stafid,
            'type': 'hidden'
        }));
    newForm.appendTo("body").submit();
}
staffPayrollSettings.loadFinanceCycleStaffExcel = function () {
    var params = {};
    $("#staffPayrollGenForm").find('input').each(function () {
        params[$(this).attr('name')] = $.trim($(this).val());
    });
     event.preventDefault();
            event.stopPropagation();

            var newForm = $('<form>', {
                'action': commonSettings.salaryUrl + "/loadFinanceCycleStaffExcel",
                'target': '_blank',
                'id': 'stuRatingForm',
                'method': 'POST'
            });
            newForm.append($('<input>', {
                'name': 'params',
                'value': JSON.stringify(params),
                'type': 'hidden'
            }));
            newForm.appendTo("body").submit();
};