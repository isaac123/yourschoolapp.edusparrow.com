var salarySettings = {};
salarySettings.calculatetotal = function () {
    var specialallowances = $('#Special_Allowances').val();
    var PF = $('#PF').val();
    var HRA = $('#HRA').val();
    var basic = $('#Basic_Pay').val();
    var conveyance = $('#conveyance').val();
    if (PF == '')
        PF = 0;
    if (specialallowances == '')
        specialallowances = 0;
    if (HRA == '')
        HRA = 0;
    if (basic == '')
        basic = 0;
    if (conveyance == '')
        conveyance = 0;
    //alert('out'+specialallowances+PF+HRA+basic+conveyance);
    if ((specialallowances >= 0) && (PF >= 0) && (HRA >= 0) && (basic >= 0) && (conveyance >= 0))
    {
        var gross = $('#Gross_Pay').val();
        var sum = parseFloat(basic) + parseFloat(specialallowances) + parseFloat(PF) + parseFloat(HRA) + parseFloat(conveyance);
        var rem = parseFloat(gross) - parseFloat(sum);

        $('#total').html('<p class="text-success">' + sum + '</p>');
        if (parseFloat(rem) < 0 || parseFloat(rem) > 0) {
            $('#remain').html('<p class="text-danger">' + rem + '</p>');
        }
        if (parseFloat(rem) == 0) {
            $('#remain').html('<p class="text-success">' + rem + '</p>');
        }

    }


};
salarySettings.calculatetotal();


$('.amountfield').each(function () {
    var $this = $(this);
    var newValue = $this.val();
    var changedValue = newValue.replace(/[^0-9.]+/g, "");
    changedValue = parseFloat(changedValue).toFixed(2);
    if (isNaN(changedValue)) {
        $this.val("");
    } else if (newValue != changedValue) {
        $this.val(changedValue);
    }
});
salarySettings.calculatesal = function () {
    var gross = $('#Gross_Pay').val();
    if (gross == '') {
        swal({
            title: "Error",
            text: 'Enter Gross pay to calculate!',
            type: "error",
            confirmButtonText: 'ok!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        return false;
                    }
                });
    } else {
        console.log(parseFloat(gross) * 0.45)
        var basic = parseFloat(gross) * 0.45;
        var HRA = parseFloat(basic) * 0.60;
        var PF = parseFloat(basic) * 0.12;
        var conveyance = parseFloat($('#conveyance').val());
        var newt = basic + PF + HRA + conveyance;
        var remt = parseFloat(gross) - parseFloat(newt);
        $('#HRA').val(HRA.toFixed(2));
        $('#PF').val(PF.toFixed(2));
        $('#Basic_Pay').val(basic.toFixed(2));
        $('#Special_Allowances').val(remt.toFixed(2));
        if (remt > 0)
            remt = 0;
        $('#total').html('<p class="text-success">' + gross + '</p>');
        if (parseFloat(remt) < 0 || parseFloat(remt) > 0) {
            $('#remain').html('<p class="text-danger">' + remt + '</p>');
        }
        if (parseFloat(remt) == 0) {
            $('#remain').html('<p class="text-success">' + remt + '</p>');
        }
    }
}

salarySettings.saveSalary = function () {
    var applnData = {};
    var err = 0;
    $('#assignSalaryForm').find('input').each(function () {
        if (( $.trim($(this).val()) == '')) {
            err = 1;
            swal({
                title: "Error",
                text: $(this).attr('title') + ' is required!',
                type: "error",
                confirmButtonText: 'ok!',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            console.log(err)
                            return false;
                        }
                    });
        } else {
            applnData[$(this).attr('name')] = $.trim($(this).val());
        }

    });
    console.log(applnData)
    if (err != 1) {

        $.ajax({
            type: "POST",
            url: commonSettings.salaryUrl + "/assignsalary",
            data: applnData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    swal({
                        title: res.type,
                        text: res.message,
                        type: res.type,
                        confirmButtonText: 'ok!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
//                                    window.location.reload();
                                    salarySettings.loadsalary()
                                }
                            });
                }
            }
        });
    }


};
salarySettings.loadassignsalary = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/staffAutocomplete",
        dataType: "html",
        success: function (res) {
            $('#assignSalaryDiv').html(res);
            $('#assignSalaryDiv').removeClass('hide')
        }
    });
};


salarySettings.loadsalary = function () {
    var staffid = $('#staffID').val()
    $.ajax({
        type: "GET",
        url: commonSettings.payrollUrl + "/assignSalary?staffId=" + staffid,
        dataType: "html",
        success: function (res) {
            $('#staffAssignSal').html(res);
            $('#staffAssignSal').removeClass('hide')
        }
    });
};

salarySettings.loadadvance = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/makeAdvance",
        dataType: "html",
        success: function (res) {
            $('#advanceediv').html(res);
            $('#advanceediv').removeClass('hide')
        }
    });
};

salarySettings.loadPayroll = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/payroll",
        dataType: "html",
        success: function (res) {
            $('#payrolldiv').html(res);
            $('#payrolldiv').removeClass('hide')
        }
    });
};

salarySettings.loadSalarySettings = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.payrollUrl + "/loadSalarySettings",
        dataType: "html",
        success: function (res) {
            $('#salarySettingsDiv').html(res);
            $('#salarySettingsDiv').removeClass('hide')
        }
    });
};
salarySettings.calculate = function () {
    var applnData = {};
    $('#assignSalaryForm').find(':input:not([readonly])').each(function () {
        if (( $.trim($(this).val()) == '')) {
            err = 1;
            swal({
                title: "Error",
                text: $(this).attr('title') + ' is required!',
                type: "error",
                confirmButtonText: 'ok!',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            return false;
                        }
                    });
        } else {
            applnData[$(this).attr('name')] = $.trim($(this).val());
        }
    });

    $.ajax({
        type: "POST",
        url: commonSettings.payrollUrl + "/calculatesal",
        data: applnData,
        dataType: "JSON",
        success: function (res) {
            console.log(res);
            for (var obj in res) {
                var ptyp = res[obj];
                $('#' + ptyp.A + '_val').val(ptyp.B)
                console.log(ptyp.A + " -> " + ptyp.B);
//                for (var paytyp in ptyp) {
//                    console.log(paytyp + " -> " + ptyp[paytyp]);
//                }
            }
        }
    });
};
salarySettings.downloadStaffPayrollExcel = function () {

    var newForm = $('<form>', {
        'action': commonSettings.payrollUrl + "/staffPayrollExcelGenerate",
        'target': '_blank',
        'id': 'staffPayrollExcelForm'
    });
    newForm.appendTo("body").submit();
};
salarySettings.downloadStaffPayrollExcelUploaded = function () {

    var newForm = $('<form>', {
        'action': commonSettings.payrollUrl + "/userDefinedExcel",
        'target': '_blank',
        'id': 'userDefinedExcel'
    });
    newForm.appendTo("body").submit();
};