
staffPayrollSettings.paytype = [];
$('#salaryAccountSett').on('click', '.add_data', function () {

    var $self = $(this);
    var err = 0;
    $self.parents('tr').find('input, select').each(function () {
        var $each = $(this);
        console.log($each.attr('name'))
        if ($each.val() == '') {
            err = 1;
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('Please fill all fields');
            $('#messageBoxModal').modal();
            return false;
        }
    });
    if (err == 0) {
        staffPayrollSettings.paytype.push($self.parents('tr').find('.paytype').val())
        console.log(staffPayrollSettings.paytype)
        $self.addClass('hide');
        $self.next('.remove_data').removeClass('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.payrollUrl + "/loadAccountSetting",
            data: {paylist: staffPayrollSettings.paytype, },
            dataType: "Html",
            success: function (res) {
                var wrapper = document.createElement('tr');
                $(wrapper).addClass('add_removedata')
                wrapper.innerHTML = res;
                $(wrapper).appendTo($self.parents('tbody'));
            }
        });

    }
}).on('click', '.remove_data', function () {
    var $row = $(this).closest('.add_removedata');

    var $self = $(this);
    var index = staffPayrollSettings.paytype.indexOf($self.parents('tr').find('.paytype').val());
    if (index > -1) {
        staffPayrollSettings.paytype.splice(index, 1);
    }
    // Remove element containing the option
    $row.remove();
})
