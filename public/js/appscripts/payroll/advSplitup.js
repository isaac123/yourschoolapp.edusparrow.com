var advSplitupSettings = {};
advSplitupSettings.restoreRow = function(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);

    for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
        oTable.fnUpdate(aData[i], nRow, i, false);
    }

    oTable.fnDraw();

};

advSplitupSettings.editRow = function(oTable, nRow, option) {
    var aData = oTable.fnGetData(nRow);
    var mode = option == 'new' ? 'data-mode="new"' : '';
    var jqTds = $('>td', nRow);
    jqTds[0].innerHTML = '<input type="text"  name="advreturnmotnh" readonly class="form-control advreturnmotnh form_date" value="' + aData[0] + '">';
    jqTds[1].innerHTML = '<input type="text" class="form-control small advsplamount"  onkeyup="advSplitupSettings.validateAmount(this)"  value="' + aData[1] + '">';
    jqTds[2].innerHTML = '<a class="edit" href="javascript:;" onclick ="advSplitupSettings.editAction(this)">Save</a>';
    jqTds[3].innerHTML = '<a class="cancel" href="javascript:;" ' + mode + ' onclick= "advSplitupSettings.cancelRow (this)">Cancel</a>';

};

advSplitupSettings.saveRow = function(oTable, nRow) {
    var jqInputs = $('input', nRow);
    if (jqInputs[0].value == '' || jqInputs[1].value == '') {
        $('.msgBox-title').html('Error');
        $('.msgBox-body').html('Invalid Input');
        $('#messageBoxModal').modal('show');
        oTable.fnDeleteRow(nRow);
        return false;
    }
    //console.log(nRow)
    $(nRow).find('td:eq( 0 )').addClass('advsplmonyr')
    $(nRow).find('td:eq( 1 )').addClass('advsplamount')
    oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
    oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
    oTable.fnUpdate('<a class="edit" href="javascript:;" onclick ="advSplitupSettings.editAction(this)">Edit</a>', nRow, 2, false);
    oTable.fnUpdate('<a class="delete" href="javascript:;" onclick = "advSplitupSettings.deleteRow(this)">Delete</a>', nRow, 3, false);
    oTable.fnDraw();
};

var column = [0, 1];
var sortingDisabled = [2, 3];
//console.log((staffAdvanceSettings.addavdTablectrl))
var oTable = $(staffAdvanceSettings.addavdTablectrl).dataTable({
    "sDom": 'C<"clear">lrtip',
    //"bProcessing": false,
    "sPaginationType": "full_numbers",
    "iDisplayLength": commonSettings.jqueryDynmicTable,
    "aoColumnDefs": [{
            "sDefaultContent": "",
            "aTargets": column
        }, {
            "bSortable": false,
            "sDefaultContent": "",
            "aTargets": sortingDisabled
        }],
    "bLengthChange": false,
    "bInfo": false
});
var nEditing = null;

$(staffAdvanceSettings.addavdTablectrl + '_new').click(function(e) {
//    //console.log(staffAdvanceSettings.advTotalAmount + ' = ' + staffAdvanceSettings.itemAdvTotalAmount)
    if (staffAdvanceSettings.advTotalAmount > 1 && staffAdvanceSettings.itemAdvTotalAmount < parseInt(staffAdvanceSettings.advTotalAmount)) {
        if (staffAdvanceSettings.itemAdvTotalAmount !== staffAdvanceSettings.advTotalAmount) {
            $('#match').addClass('hide')
            $('#notmatch').removeClass('hide')
            $('#itemTotalSpan').html(staffAdvanceSettings.itemAdvTotalAmount.toFixed(2));
        }
        else
        {
            $('#notmatch').addClass('hide')
            $('#match').removeClass('hide')
            $('#itemTotalSpan').html(staffAdvanceSettings.itemAdvTotalAmount.toFixed(2));
        }
        e.preventDefault();
        var aiNew = oTable.fnAddData(['', '', '', '',
            '<a class="edit" href="javascript:;">Edit</a>', '<a class="cancel" data-mode="new" href="javascript:;" onclick= "advSplitupSettings.cancelRow (this)">Cancel</a>'
        ]);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        advSplitupSettings.editRow(oTable, nRow, 'new');
        nEditing = nRow;
        $('.advreturnmotnh').datetimepicker({
            format: 'mm-yyyy',
            language: 'en',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 3,
            minView: 3,
            forceParse: 0,
            startDate: '-1d',
        });
    } else {
        swal({
            title: "Error",
            text: "Items' total exceeds the advance amount!",
            type: "error",
            confirmButtonText: 'Ok!',
            closeOnConfirm: true,
        },
                function(isConfirm) {
                    if (isConfirm) {
                        return false;
                    }
                });
    }
});

advSplitupSettings.deleteRow = function(ctrl) {
//    e.preventDefault();
    console.log('hais')

    swal({
        title: "Confirm",
        text: "Are you sure to delete this row ?",
        type: "warning",
        confirmButtonText: 'Yes!',
        showCancelButton: true,
        closeOnCancel: true,
        closeOnConfirm: true,
        cancelButtonText: 'Cancel',
    },
            function(isConfirm) {
                if (isConfirm) {
                    var nRow = $(ctrl).parents('tr')[0];
                    var jqInputs = $('td', nRow);
                    //console.log(jqInputs)

                    oTable.fnDeleteRow(nRow);

                    var splAmountSum = 0;
                    $(staffAdvanceSettings.addavdTablectrl).find('input.advsplamount').each(function() {
                        splAmountSum += $.trim($(this).val());
                    });
                    $(staffAdvanceSettings.addavdTablectrl).find('td.advsplamount').each(function() {
                        splAmountSum += parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0;
                    });
                    staffAdvanceSettings.itemAdvTotalAmount = parseInt(splAmountSum);
//    //console.log((staffAdvanceSettings.itemAdvTotalAmount + ' ' + staffAdvanceSettings.advTotalAmount))
                    if (staffAdvanceSettings.itemAdvTotalAmount < staffAdvanceSettings.advTotalAmount) {
                        $('#match').addClass('hide')
                        $('#notmatch').removeClass('hide')
                        $('#itemTotalSpan').html(staffAdvanceSettings.itemAdvTotalAmount.toFixed(2));
                    }
                    else
                    {
                        $('#notmatch').addClass('hide')
                        $('#match').removeClass('hide')
                        $('#itemTotalSpan').html(staffAdvanceSettings.itemAdvTotalAmount.toFixed(2));
                    }
                } else {
                    return false;
                }
            });


};


advSplitupSettings.editAction = function(ctrl) {
    var nRow = $(ctrl).closest('tr')[0];
    nEditing = nRow;
    if (nEditing !== null && nEditing != nRow) {
        advSplitupSettings.editRow(oTable, nRow, 'edit');
        nEditing = nRow;
    } else if (nEditing == nRow && ctrl.innerHTML == "Save") {
        var jqInputs = $('input', nEditing);
        //console.log(jqInputs)
        if (jqInputs[0].value == '') {
            $('.msgBox-title').html('Error');
            $('.msgBox-body').html('Invalid Input');
            $('#messageBoxModal').modal('show');
            oTable.fnDeleteRow(nRow);
            return false;
        }

        if (staffAdvanceSettings.itemAdvTotalAmount > staffAdvanceSettings.advTotalAmount)
        {
            $('#match').addClass('hide')
            $('#notmatch').removeClass('hide')
            $('#itemTotalSpan').html(staffAdvanceSettings.itemAdvTotalAmount.toFixed(2));
            swal({
                title: "Error",
                text: 'Items\' total exceeds the Fee total!',
                type: "error",
                confirmButtonText: 'Ok!',
                closeOnConfirm: true,
            },
                    function(isConfirm) {
                        if (isConfirm) {
                            return false;
                        }
                    });
        } else {
            advSplitupSettings.saveRow(oTable, nEditing);
            nEditing = null;
            //console.log(params)
        }
    } else {
        /* No edit in progress - let's start one */
        advSplitupSettings.editRow(oTable, nRow, 'edit');
        nEditing = nRow;




        $('.advreturnmotnh').datetimepicker({
            format: 'mm-yyyy',
            language: 'en',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 3,
            minView: 3,
            forceParse: 0,
            startDate: '-1d',
        });
    }
}

advSplitupSettings.cancelRow = function(ctrl) {
    if ($(ctrl).attr("data-mode") == "new") {
        var nRow = $(ctrl).parents('tr')[0];
        oTable.fnDeleteRow(nRow);

        var splAmountSum = 0;
        $(staffAdvanceSettings.addavdTablectrl).find('input.advsplamount').each(function() {
            splAmountSum += parseInt($.trim($(this).val())) > 0 ? parseInt($.trim($(this).val())) : 0;
        });

        $(staffAdvanceSettings.addavdTablectrl).find('td.advsplamount').each(function() {
            splAmountSum += parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0;
        });
        staffAdvanceSettings.itemAdvTotalAmount = parseInt(splAmountSum);
        if (staffAdvanceSettings.itemAdvTotalAmount != staffAdvanceSettings.advTotalAmount) {
            $('#match').addClass('hide')
            $('#notmatch').removeClass('hide')
            $('#itemTotalSpan').html(staffAdvanceSettings.itemAdvTotalAmount.toFixed(2));
        }
        else
        {
            $('#notmatch').addClass('hide')
            $('#match').removeClass('hide')
            $('#itemTotalSpan').html(staffAdvanceSettings.itemAdvTotalAmount.toFixed(2));
        }
    } else {
        advSplitupSettings.restoreRow(oTable, nEditing);
        nEditing = null;
    }


};
advSplitupSettings.validateAmount = function(ctrl) {
    var $this = $(ctrl);
    var splamount = $this.val().replace(/,/g, "");
    //console.log(parseInt(splamount))
    if (parseInt(splamount) > 0) {

        var splAmountSum = 0;
        $(staffAdvanceSettings.addavdTablectrl).find('input.advsplamount').each(function() {
            splAmountSum += parseInt($.trim($(this).val())) ? parseInt($.trim($(this).val())) : 0;
        });

        $(staffAdvanceSettings.addavdTablectrl).find('td.advsplamount').each(function() {
            //console.log('splitamt:' + parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0)
            splAmountSum += parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0;
        });
        staffAdvanceSettings.itemAdvTotalAmount = parseInt(splAmountSum);
        //console.log((staffAdvanceSettings.itemAdvTotalAmount + ' ' + staffAdvanceSettings.advTotalAmount))
        if (staffAdvanceSettings.itemAdvTotalAmount != staffAdvanceSettings.advTotalAmount) {
            $('#match').addClass('hide')
            $('#notmatch').removeClass('hide')
            $('#itemTotalSpan').html(staffAdvanceSettings.itemAdvTotalAmount.toFixed(2));
        }
        else
        {
            $('#notmatch').addClass('hide')
            $('#match').removeClass('hide')
            $('#itemTotalSpan').html(staffAdvanceSettings.itemAdvTotalAmount.toFixed(2));
        }

    } else {
        swal({
            title: "Error",
            text: "Enter a valid item fee amount",
            type: "error",
            confirmButtonText: 'Ok!',
            closeOnConfirm: true,
        },
                function(isConfirm) {
                    if (isConfirm) {
                        $this.val('')
                    }
                });
    }

};

(function() {
})();

