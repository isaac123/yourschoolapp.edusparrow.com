var staffAdvanceSettings = {};
staffAdvanceSettings.addavdTablectrl = "#advSplitupFormTable";
staffAdvanceSettings.advTotalAmount = 0;
staffAdvanceSettings.itemAdvTotalAmount = 0;
staffAdvanceSettings.loadStaffdet = function () {
    var applnData = {};
    $("#staffAdvSearchForm").find('input,select').each(function () {
        applnData[$(this).attr('name')] = $.trim($(this).val());
    });
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/getStaffList",
        data: applnData,
        dataType: "Html",
        success: function (res) {
            $('#advStaflst').removeClass('hide')
            $('#advloadReport').html(res)
        }
    });
};
//staffAdvanceSettings.loadStaffdet();
staffAdvanceSettings.addNewAdvance = function () {

    console.log('asd')
    var applnData = {};
    var totsplnData = {};
    var err = 0;
    var error = '';
    var i = 0;
    //Splitup check start
    console.log(staffAdvanceSettings.itemAdvTotalAmount + ' ' + staffAdvanceSettings.advTotalAmount)
    if (staffAdvanceSettings.itemAdvTotalAmount != staffAdvanceSettings.advTotalAmount) {
        swal("Error", "Items' total doesn't match with Advance amount total!", "error");
//        error = error + "Items' total doesn't match with Advance amount total!\n";
        err = 1;
        return false;
    }
    console.log(error)
    $('#advSplitupFormTable').find('tr').each(function () {

        var splnData = {};
        $(this).find('td').each(function () {
            if ($(this).attr('class') == 'dataTables_empty') {
                swal("Error", 'No Split-ups found!', "error");
//                error = error + "No Split-ups found!\n";
                err = 1;
                return false;
            }
            $(this).find('input').each(function () {
                swal("Error", 'Splitup items are not saved!', "error");
//                error = error + "Splitup items are not saved!\n";
                err = 1;
                return false;
            });
            var attr = $(this).attr('class');
            if (typeof attr !== typeof undefined && attr !== false) {
                console.log($(this).attr('class').split(' '))
                var name = $(this).attr('class').split(' ')[0];
                splnData[name] = $.trim($(this).text());
            }
        });
        if (!$.isEmptyObject(splnData)) {
            totsplnData[i] = splnData;
            i++;
        }
    });
    console.log(totsplnData);
    applnData['splitup'] = totsplnData;
//    exit;
    //Splitup ceck end

    var debitledger = $('#debitledgerid').val();
    var creditledger = $('#creditledgerid').val();
    var voucheramount = $('#voucheramount').val();
    var receivedby = $('#receivedbyid').val();
    var voucheramountfor = $('#voucheramountfor').val();
    var apprvl_req = $('#apprvl_req').val();
    var apprvlmtamt = $('#apprvlmtamt').val();
    var vouchertyp = $('#vouchertyp').val();
    applnData.apprvl_req = apprvl_req;
    applnData.apprvlmtamt = apprvlmtamt;
    applnData.vouchertyp = vouchertyp;

//    applnData.paymentamount = paymentamount;

    if (debitledger === '') {
        error = error + "Ledger for Debit missing\n";
        err = 1;
    }
    if (creditledger === '') {
        error = error + "Ledger for Credit missing\n";
        err = 1;
    }
    if (voucheramount === '') {
        error = error + "Amount missing\n";
        err = 1;
    }
    else
    {
        if (voucheramount < 0)
        {
            error = error + "Amount cannot be Negative\n";
            err = 1;
        }
    }

    if (receivedby === '') {
        error = error + "Received by missing\n";
        err = 1;
    }

    if (voucheramountfor === '') {
        error = error + "Amount for is missing\n";
        err = 1;
    }

    $("#addPaymentvouchers").find('input,textarea').each(function () {
        applnData[$(this).attr('name')] = $.trim($(this).val());
    });
    console.log(err)
    console.log(error)
    if (err === 0) {
        $('.loading_addadv').removeClass('hide')
        $('#app_adv').addClass('hide')
        $("#addPaymentvouchers :input").prop('readonly', true);
        staffAdvanceSettings.saveAdvance(applnData)
    }
    else
    {
        swal("Error", error, "error");
        return false;
    }
};
staffAdvanceSettings.saveAdvance = function (applnData) {
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/saveNewAdvance",
        data: applnData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                if (res['type'] === 'success') {
                    applnData.recordItemId = res.recordItemId;
                    applnData.recordTypId = res.recordTypId;
                    voucherSettings.makeNewVoucher(applnData);
                } else {
                    swal("Error", res['message'], res['type'])
                }
            }
        }
    });
};


$('.amountfield').on('blur', function () {
    var $this = $(this);
    var newValue = $this.val();
    var changedValue = newValue.replace(/[^0-9.]+/g, "");
    changedValue = parseFloat(changedValue).toFixed(2);
    if (isNaN(changedValue)) {
        $this.val("");
    } else if (newValue != changedValue) {
        $this.val(changedValue);
    }
});

$('.amountfield').each(function () {
    var $this = $(this);
    var newValue = $this.val();
    var changedValue = newValue.replace(/[^0-9.]+/g, "");
    changedValue = parseFloat(changedValue).toFixed(2);
    if (isNaN(changedValue)) {
        $this.val("");
    } else if (newValue != changedValue) {
        $this.val(changedValue);
    }
});
staffAdvanceSettings.changeStatus = function (applnData) {
    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/updateAdvance",
        data: applnData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                if (res['type'] === 'success') {
                    //swal("Success", res['message'], res['type']);
//                    setTimeout(function() {
//                        location.reload()
//                    }, 2000);
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        
    console.log(commonSettings.fromPage)
                        if (commonSettings.fromPage == 'StaffAdvanceAprvl') {
                            approvalviewSettings.loadStaffAdvanceApproval();
                        } else if (commonSettings.fromPage == 'StfSalaryApprvl') {
                            approvalviewSettings.loadStaffSalaryApprovalList();
                        } else if (commonSettings.fromPage == 'VoucherList') {
                            voucherSettings.loadvoucherList();
                        } else if (commonSettings.fromPage == 'VoucherApproval') {
                                approvalSettings.loadFinanceApprovalList();
                        }  else {
                            salarySettings.loadadvance()
                        }
                    });

                } else {
                    swal("Error", res['message'], res['type'])
                }
            }
        }
    });
};
staffAdvanceSettings.loadAdvTotal = function () {

    var amount = $('#voucheramount').val()
    console.log(parseInt(amount))
    if (parseInt(amount) > 0) {
        staffAdvanceSettings.advTotalAmount = parseInt(amount);
        viewAdvSplSettings.calculateAmount();
    }
    else {
        swal({
            title: "Error",
            text: "Enter the fee amount",
            type: "error",
            confirmButtonText: 'Ok!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                    }
                });
    }
    return false;

};

staffAdvanceSettings.loadcreateadvForm = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.salaryUrl + "/createadvance",
        dataType: "html",
        success: function (res) {
            $('#advanceediv').html(res);
            $('#advanceediv').removeClass('hide')
        }
    });

};
staffAdvanceSettings.advanceForwardDet = function (ctrl) {
    var applnData = {};
    applnData.itemID = $(ctrl).attr('itemID');
    applnData.type = $(ctrl).attr('typeid') + " " + "Voucher";
    applnData.status = 'Forwarded';
    applnData.voucheramountfor = $(ctrl).attr('comments');
    console.log(applnData);
    approvalSettings.checkReqStatus(applnData);

};
staffAdvanceSettings.deleteAdvVoucher = function (voucherid) {
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html('Deleting this Voucher will also delete related entries.<br> Do you confirm to delete this Voucher?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.vouchereditAjaxurl + "/deleteVoucher",
            data: {voucherid: voucherid},
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    swal({
                        title: res.type,
                        text: res.message,
                        type: res.type,
                        confirmButtonText: 'OK!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    staffAdvanceSettings.loadStaffdet();

                                }
                            });
                }
            }
        });
    });
}