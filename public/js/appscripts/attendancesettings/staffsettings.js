var settings = {};


settings.updateStaffSettings = function() {
    var i = 0;
    var name = {};
    var attendanceid = {};
    var weightage = {};
    var color = {};
    var candtlev = {};
    var input = {};
    //var total_weightage = 0;
    var error = 0;
    var hidden_count = 0;
    var hidden = {};
    $(".name").each(function() {
        
     //   console.log($(this).attr('name'));

        if ($(this).parent('div').parent('div').is(":visible"))
        {
            if ($(this).val().length > 0)
            {
                name[i++] = ($(this).val());
            } else {
                error = 1;
            }
        } else {


            hidden[hidden_count++] = ($(this).val());

        }
    })

    i = 0;
    if (!error) {
        $(".attendanceid").each(function() {
            if ($(this).parent('div').parent('div').is(":visible"))
            {
                if ($(this).val().length > 0)
                {
                    if (parseInt($(this).val().length) > 3)
                    {
                        error = 5;
                    }
                    else
                    {
                        attendanceid[i] = ($(this).val());
                        i++;
                    }

                } else {
                    error = 2;
                }
            }
        })
    }

    i = 0;

    if (!error) {
        $(".attendance").each(function() {
            if ($(this).parent('div').parent('div').is(":visible"))
            {
                if ($(this).val().length > 0)
                {
                    if ((parseFloat(($(this).val())) < -5) || (parseFloat(($(this).val())) > 10))
                    {
                        error = 6;
                    }
                    else
                    {
                        weightage[i] = parseFloat(($(this).val()));
                        i++;
                    }
                } else {
                    error = 3;
                }
            }
        })
    }

    i = 0;

    if (!error) {
        $(".pickcolor").each(function() {
            if ($(this).parent('div').parent('div').is(":visible"))
            {
                if ($(this).val().length > 0)
                {
                    color[i] = (($(this).val()));
                    i++;
                } else {
                    error = 4;
                }
            }
        })
    }
    
     i = 0;
    if (!error) {
        $(".selctlevtyp").each(function() {
           // console.log('call')
            if ($(this).parent('div').parent('div').is(":visible"))
            {
               // console.log('call')
                if($(this).is(':checked'))
                {
                   //  console.log('checked')
                     
                   candtlev[i] = $(this).val();
                    i++;
                } else {
                    error = 7;
                }
            }
        })
    }

console.log(candtlev);

    if (error === 1) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Attendance Name is missing</div>');
                            $('#messageBoxModal').modal('show'); 
    }
    else if (error === 2) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Attendance Id is missing</div>');
                            $('#messageBoxModal').modal('show');
    }
    else if (error === 3) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Attendance Value is missing</div>');
                            $('#messageBoxModal').modal('show');
    }
    else if (error === 4) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Color Code is missing</div>');
                            $('#messageBoxModal').modal('show');
    }
    else if (error === 5) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Attendance legend has greater then 3</div>');
                            $('#messageBoxModal').modal('show');
    }
    else if (error === 6) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Attendance Value is out of Range(-5,10)</div>');
                            $('#messageBoxModal').modal('show');
    }
    else {

        for (j = 0; j < i; j++) {
            input[j] = new Array(name[j], attendanceid[j], weightage[j], color[j],candtlev[j]);
        }
        input["count"] = i;
        input["hidden"] = hidden;
        input["hidden_count"] = hidden_count;

        //alert(JSON.stringify(input));

        $.ajax({
            type: "POST",
            url: commonSettings.attendancesettingsAjaxurl + "/updateStaffSettings",
            data: input,
            success: function(res) {
                if (res) {
                    $('#Rating').addClass('hide');
                    $('.modal-title').html('Message');
                    $('.modal-body').html(res);
                                        $('#messageBoxModal').modal('show');
                    $('#msg_ok').click(function() {
//                        window.location.reload();
                        attendanceSettings.loadattSett();
                    });
                }
            }
        });
        //alert(JSON.stringify(input));
        //alert(JSON.stringify(weightage));
    }

};

settings.updateStaffFrequency = function() {
    var frequency = {};
    var class_ids = {};
    var data = {};
    //var total_weightage = 0;
    var exceeds_limit = 0;
    var field_empty = 0;
    var i = 0;

    var message = "";

    var value = 0;
    $(".frequencyval").each(function() {

        if (($(this).val().length == 0))
        {
            field_empty = 1;
        }
        else
        {
            value = parseInt(($(this).val()));
            if ((value <= 0) || (value > 10))
            {
                exceeds_limit = 1;
            }
            else
            {
                class_ids[i] = $(this).attr("id");
                frequency[i] = value;
                i++;
            }
        }
    });

    if (exceeds_limit == 1) {
        message = message + '<div class="alert alert-block alert-danger fade in">No of Periods should be within range 1 - 10</div>'
    }
    if (field_empty == 1) {
        message = message + '<div class="alert alert-block alert-danger fade in">No of Periods cannot be empty</div>'
    }

    if (!exceeds_limit && !field_empty) {
        data['count'] = i;
        data['class_ids'] = class_ids;
        data['frequency'] = frequency;

        $.ajax({
            type: "POST",
            url: commonSettings.attendancesettingsAjaxurl + "/updateStaffFrequency",
            data: data,
            success: function(res) {
                if (res) {
                    $('.modal-title').html('Message');
                    $('.modal-body').html(res);
                                        $('#messageBoxModal').modal('show');
                }
            }
        });
    }
    else {
        $('.modal-title').html('Message');
        $('.modal-body').html(message);
                            $('#messageBoxModal').modal('show');
    }

};

settings.addNumofDays = function(id, leavetype)
{
    var leavetype = leavetype;
    var attendanceid = id;
    $.ajax({
        type: "POST",
        url: commonSettings.attendancesettingsAjaxurl + "/addNumofDays",
        data: {"leavetype": leavetype,
            "id": attendanceid},
        dataType: "html",
        success: function(res) {
            $('.formBox-title').html(leavetype);
            $('.formBox-body').html(res);
            // $('#modalForm').click();
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#formModal').modal('show');
            $('#confirm_save').html('Save');
            $('#confirm_save').unbind('click').click(function() {

                settings.updateAttendance(attendanceid);
            });
        }
    });

};

settings.viewSalDetect = function()
{
    $('.salary_detect').toggleClass('hide');
};

settings.enableAllwForm = function(ctrl)
{
    //    alert('d')
    //          console.log($('#allowed_count_form').html())
    $('#allowed_count_form').find('select').each(function(){
          var $this = $(ctrl).prop('checked');
        $(this).prop('disabled',!$this)
    });
};
settings.slctDayorInsts = function(type)
{
    if (type == '1')
        $('.daysorinsts').html('days');
    else if (type == '0')
        $('.daysorinsts').html('instances');
};

settings.viewLeaveType = function()
{
    //alert('test');
    $('.show_payslip').toggleClass('hide');
};

settings.updateAttendance = function(attendanceid)
{
    var attendancedata = {};
    var saldtchck = 0;
    var payslpchk = 1;
    var errmsg = '';
    attendancedata['id'] = attendanceid;
    $("#addNumofDaysForm").find('input,select').each(function() {
        if ($(this).attr('name') == 'is_allowed_by_count')
        {
            if ($(this).is(':checked')) {
                 attendancedata[$(this).attr('name')] = 1;

            }else{
                  attendancedata[$(this).attr('name')] = 0;
            }
        }else{
            
            attendancedata[$(this).attr('name')] = $.trim($(this).val());
        }
//        else if ($(this).attr('name') == 'showinpayslip')
//        {
//            if ($(this).is(':checkbox')) {
//                if ($(this).is(':checked')) {
//                    attendancedata[$(this).attr('name')] = 1;
//                }
//                else
//                {
//                    payslpchk = 0;
//                    attendancedata[$(this).attr('name')] = 0;
//                }
//
//            }
//        }
//        else
//        {
//            if ($(this).attr('name') == 'allowed_days')
//            {
//                if ($.trim($(this).val()) == '')
//                {
//                    errmsg += 'Please Select Allowed Days.' + '<br>';
//                }
//            }
//
//            if ($(this).attr('name') == 'allowed_month')
//            {
//                if ($.trim($(this).val()) == '')
//                {
//                    errmsg += 'Please Select Allowed Month.' + '<br>';
//                }
//            }
//
//            if (saldtchck == 1)
//            {
//                if ($(this).attr('name') == 'sal_deduct')
//                {
//                    if ($.trim($(this).val()) == '')
//                    {
//                        errmsg += 'Please Select Salary Deduction values.' + '<br>';
//                    }
//                }
//
//                if ($(this).attr('name') == 'deduct_days')
//                {
//                    if ($.trim($(this).val()) == '')
//                    {
//                        errmsg += 'Please Select Salary Deduction days.' + '<br>';
//                    }
//                }
//
//                if ($(this).attr('name') == 'deduct_type')
//                {
//                    if ($.trim($(this).val()) == '')
//                    {
//                        errmsg += 'Please Select Salary Deduction type.' + '<br>';
//                    }
//                }
//
//                if ($(this).attr('name') == 'deduct_max_days')
//                {
//                    if ($.trim($(this).val()) == '')
//                    {
//                        errmsg += 'Please Select Salary Deduction Maximum days.' + '<br>';
//                    }
//                }
//
//            }
//
//            if (payslpchk == 0)
//            {
//                if ($(this).attr('name') == 'attendacetype')
//                {
//                    if ($.trim($(this).val()) == '')
//                    {
//                        errmsg += 'Please Select Attendance Type.' + '<br>';
//                    }
//                }
//
//            }

//        }


    });
    console.log(attendancedata);
    console.log(errmsg);
//    if (errmsg == '')
//    {
        $('#confirm_close').click();
        $('.errmsg').addClass('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.attendancesettingsAjaxurl + "/updateAttendance",
            data: attendancedata,
            dataType: "html",
            success: function(res) {
                console.log(res);
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res);
                                    $('#messageBoxModal').modal('show');
            }

        });
//    }
//    else
//    {
//        $('.errmsg').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
//        $('.errmsg').removeClass('hide');
//        //<div class="alert alert-block alert-danger fade in">
//    }
};
