var settings = {};

settings.updateStudentSettings = function() {
    var i = 0;
    var name = {};
    var attendanceid={};
    var weightage = {};
    var color = {};
    var input = {};
    //var total_weightage = 0;
    var error = 0;
    var hidden_count = 0;
    var hidden = {};
    $(".name").each(function() {

        if ($(this).parent('div').parent('div').is(":visible"))
        {
            if ($(this).val().length > 0)
            {
                name[i++] = ($(this).val());
            } else {
                error = 1;
            }
        } else {


            hidden[hidden_count++] = ($(this).val());

        }
    })
   i = 0;
  if (!error) {
     $(".attendanceid").each(function() {
       if ($(this).parent('div').parent('div').is(":visible"))
        {
            if ($(this).val().length > 0)
            {
                 if (parseInt($(this).val().length)>3)
                    {
                        error = 5;
                    }
                    else
                    {
                  attendanceid[i] = ($(this).val());
                  i++;
                    }
                
            } else {
                error = 2;
            }
        } 
    })
  }
    i = 0;
    if (!error) {
        $(".attendance").each(function() {
            if ($(this).parent('div').parent('div').is(":visible"))
            {
                if ($(this).val().length > 0)
                {
                    if ((parseFloat(($(this).val())) < -5) || (parseFloat(($(this).val())) > 10))
                    {
                        error = 6;
                    }
                    else
                    {
                        weightage[i] = parseFloat(($(this).val()));
                        i++;
                    }
                } else {
                    error = 3;
                }
            }
        })
    }

    i = 0;
   if (!error) {
        $(".pickcolor").each(function() {
            if ($(this).parent('div').parent('div').is(":visible"))
            {
                if ($(this).val().length > 0)
                {
                    color[i] = (($(this).val()));
                    i++;
                } else {
                    error = 4;
                }
            }
        })
    }

    if (error === 1) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Attendance Name is missing</div>');
        $('#messageBox').click();
    }
     else if (error === 2) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Attendance Id is missing</div>');
        $('#messageBox').click();
    }
    else if (error === 3) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Attendance Value is missing</div>');
        $('#messageBox').click();
    }
    else if (error === 4) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Color Code is missing</div>');
        $('#messageBox').click();
    }
    else if (error === 5) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Attendance legend has greater then 3</div>');
        $('#messageBox').click();
    }    
    else if (error === 6) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Attendance Value is out of Range(-5,10)</div>');
        $('#messageBox').click();
    }    
    else {

        for (j = 0; j < i; j++) {
            input[j] = new Array(name[j],attendanceid[j], weightage[j], color[j]);
        }
        input["count"] = i;
        input["hidden"] = hidden;
        input["hidden_count"] = hidden_count;

        //alert(JSON.stringify(input));

        $.ajax({
            type: "POST",
            url: commonSettings.attendancesettingsAjaxurl + "/updateStudentSettings",
            data: input,
            success: function(res) {
                if (res) {
                $('#Rating').addClass('hide');
                $('.modal-title').html('Message');
                $('.modal-body').html(res);
                $('#messageBox').click();
                $('#msg_ok').click(function() {
                    //window.location.reload();
                    studentCycleSettings.loadClassroomadmin();
                });
                 }
            }
        });
        //alert(JSON.stringify(input));
        //alert(JSON.stringify(weightage));
    }

};

settings.updateStudentFrequency = function() {
    var frequency = {};
    var class_ids = {};
    var data = {};
    //var total_weightage = 0;
    var exceeds_limit = 0;
    var field_empty = 0;
    var i = 0;

    var message = "";

    var value = 0;
    $(".frequencyval").each(function() {

        if (($(this).val().length == 0))
        {
            field_empty = 1;
        }
        else
        {
            value = parseInt(($(this).val()));
            if ((value <= 0) || (value > 10))
            {
                exceeds_limit = 1;
            }
            else
            {
                class_ids[i] = $(this).attr("id");
                frequency[i] = value;
                i++;
            }
        }
    });

    if (exceeds_limit == 1) {
        message = message + '<div class="alert alert-block alert-danger fade in">No of Periods should be within range 1 - 10</div>'
    }
    if (field_empty == 1) {
        message = message + '<div class="alert alert-block alert-danger fade in">No of Periods cannot be empty</div>'
    }

    if (!exceeds_limit && !field_empty) {
        data['count'] = i;
        data['class_ids'] = class_ids;
        data['frequency'] = frequency;

        $.ajax({
            type: "POST",
            url: commonSettings.attendancesettingsAjaxurl + "/updateStudentFrequency",
            data: data,
            success: function(res) {
                if (res) {
                    $('.modal-title').html('Message');
                    $('.modal-body').html(res);
                    $('#messageBox').click();
                }
            }
        });
    }
    else {
        $('.modal-title').html('Message');
        $('.modal-body').html(message);
        $('#messageBox').click();
    }

};

settings.getAttendancePeriods = function (orgvalueid) {
    $.ajax({
        type: "POST",
        url: commonSettings.attendancesettingsAjaxurl + "/getAttendancePeriods",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $('#attendance_period').html(res);
            $('#attendance_period').removeClass('hide');
        }
    });
};
settings.assignPeriods = function (node_id) {
    $.ajax({
        type: "POST",
        url: commonSettings.attendancesettingsAjaxurl + "/assignPeriods",
        data: {node_id: node_id},
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('.modal-title').html('Add Periods');
                $('.modal-body').html(res);
                $('#formModal').modal('show');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    settings.addPeriods(node_id);
                });
            }
        }
    });
};
settings.addPeriods = function (node_id) {
    var params = {};
    var err = 0;
    var error = '';
    $("#createPeriodsForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n<br>";

            } else {
                params[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.attendancesettingsAjaxurl + "/updatePeriods",
            data: params,
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    swal({
                        title: res.type,
                        text: res.message,
                        type: res.type,
                        confirmButtonText: 'OK!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    settings.getAttendancePeriods(res.ID);
                                }
                            });
                }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        $('#msg_ok').unbind('click').click(function () {
            settings.assignPeriods(node_id);
        });
        err = 0;

    }
};
settings.editAttnPeriods = function (id) {
    $.ajax({
        type: "POST",
        url: commonSettings.attendancesettingsAjaxurl + "/editAttnPeriods",
        data: {id: id},
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('.modal-title').html('Edit Periods');
                $('.modal-body').html(res);
                $('#formModal').modal('show');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    settings.updatePeriods(id);
                });
            }
        }
    });
};
settings.updatePeriods = function (id) {
    var params = {};
    var err = 0;
    var error = '';
    $("#editAttPeriodsForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n<br>";

            } else {
                params[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    console.log(params);
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.attendancesettingsAjaxurl + "/updatePeriods",
            data: params,
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    swal({
                        title: res.type,
                        text: res.message,
                        type: res.type,
                        confirmButtonText: 'OK!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    settings.getAttendancePeriods(res.ID);
                                }
                            });
                }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        $('#msg_ok').unbind('click').click(function () {
            settings.editAttnPeriods(id);
        });
        err = 0;
    }
};
settings.deleteAttnPeriods = function (id) {
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html('Do you want to delete this period ?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.attendancesettingsAjaxurl + "/deleteAttnPeriods",
            data: {id: id},
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    swal({
                        title: res.type,
                        text: res.message,
                        type: res.type,
                        confirmButtonText: 'OK!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    settings.getAttendancePeriods(res.ID);
                                }
                            });
                }
            }
        });
    });
};