var approvalviewSettings = {};
approvalviewSettings.request='';

approvalviewSettings.loadLeaveapprvl = function () {
    
    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/manageLeavesApp",
        dataType: "html",
        success: function (res) {
            $('#leaveapprovaldiv').html(res);
            $('#leaveapprovaldiv').removeClass('hide');
        }
    });
};

approvalviewSettings.loadStaffSalryapprvl = function () {
    
    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/staffSalaryManag",
        dataType: "html",
        success: function (res) {
            $('#stfsalaryapprovaldiv').html(res);
            $('#stfsalaryapprovaldiv').removeClass('hide');
        }
    });
};

approvalviewSettings.loadAdvanceApprvl = function () {
    
    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/manageStaffAdvncApp",
        dataType: "html",
        success: function (res) {
            $('#advanceapprvlDiv').html(res);
            $('#advanceapprvlDiv').removeClass('hide');
        }
    });
};

approvalviewSettings.loadFeecancelApprvl = function () {
    
    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/feeCancelApprvl",
        dataType: "html",
        success: function (res) {
            $('#feecancelapprvlDiv').html(res);
            $('#feecancelapprvlDiv').removeClass('hide');
        }
    });
};

approvalviewSettings.loadFeeConcessionapprvl = function () {
    
    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/feeConcessionApprvl",
        dataType: "html",
        success: function (res) {
            $('#feeconcessionapprovaldiv').html(res);
            $('#feeconcessionapprovaldiv').removeClass('hide');
        }
    });
};

approvalviewSettings.loadFeeReqapprvl = function () {
    
    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/feeRequestApprvl",
        dataType: "html",
        success: function (res) {
            $('#feeReqApprvldiv').html(res);
            $('#feeReqApprvldiv').removeClass('hide');
        }
    });
};

    
approvalviewSettings.loadSubtree = function (ctrl) {
    console.log($(ctrl).parent().parent().next('.subtreediv').html())
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/loadSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};


approvalviewSettings.loadStaffSalaryApprovalList = function ()
{
    var approvalSearchData = {};
    var params = {};
    approvalSearchData.emptySearch = 1;
    $("#searchapprovalList").find('input,select').each(function () {
        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
        if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
            approvalSearchData[$(this).attr('name')] = $.trim($(this).val());
            approvalSearchData.emptySearch = 0
        }

    });

    params = approvalSearchData;
    commonSettings.fromPage = 'StfSalaryApprvl';
    console.log(params);
    $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/staffSalaryHeader",
        dataType: "html",
        success: function (res) {
            $('#AppList').html(res);
            var tablectrl = '#ApprovalList';
            approvalviewSettings.noOfRecordsToDisplay = 10;
            var column = [0, 2, 3, 4, 5];
            var sortingDisabled = [1, 6, 7];

            $(tablectrl).dataTable({
              //  "sDom": 'C<"clear">lrtip',
               "sDom": '<"top"if>rt<"bottopm"p><"clear">',
               "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": commonSettings.approvalViewAjaxurl + '/loadstaffSalaryData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": approvalviewSettings.noOfRecordsToDisplay,
                // 'iDisplayStart':patientsapp.startIndex,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[0, "desc"]],
                "bLengthChange": false,
                //"bInfo": false,
                 "oLanguage":{
                        "sInfo":"<i class='fa fa-info activity-icon'></i> <i class='text-muted'><b>Showing _START_ to _END_ of _TOTAL_ applications</b></i>",
                        "sInfoFiltered": " ",
                    },
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function (o) {
//                    if (o._iRecordsDisplay <= approvalviewSettings.noOfRecordsToDisplay) {
//                        $(tablectrl + '_paginate').hide();
//                    }
                         $('.dataTables_filter').addClass('col-md-5');
                        $('.dataTables_filter').find('label').addClass('col-md-3').css('display', ' inline');
                        $('.dataTables_filter').find('input').addClass('form-control pull-right');
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                    var rowText = '<tr fromPage="StfSalaryApprvl" appitemID = "' + aData.approval_id + '" appType ="' + aData.approval_type_id + '" itemID ="' + aData.item_id + '">';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.voucherid + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<a href="javascript:void(0)" onclick="approvalSettings.viewAppDetails(this)">' + aData.approval_item + '</a>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.voucher_crdt + '</span>';
                    rowText += '</td>';


                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.voucher_dbt + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.voucher_amt + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="label label-' + aData.labelclass + ' label-mini">' + aData.approval_status + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<a href="javascript:void(0)" onclick="approvalSettings.viewForwardDetails(this)"> <i class="fa fa-eye fa-2x"></i>  </a>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += aData.Actions;
                    rowText += '</td>';

                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });


        }
    });


};
approvalviewSettings.viewApprovalDetails = function (approve_masid) {
     $.ajax({
        type: "POST",
        url: commonSettings.approvalViewAjaxurl + "/viewApprovalDetails",
        data:{approve_masid:approve_masid},
        dataType: 'html',
        success: function (res) {
            viewapproval = bootbox.dialog({
                title: "Approval View",
                animate: true,
                message: res,
                buttons: {
                    Close: {
                        label: "Close",
                        className: "btn-default"
                    }
                }
            });
        }
    });
};