var appointmentSettings = {};


appointmentSettings.loadSubtree = function (ctrl)
{
    var orgvalueid = $(ctrl).val();
    $(ctrl).parent().parent().next('.subtreediv')
            .removeClass('hide')
            .html(' <div class="form-group">  <span class="col-sm-8 pull-right help-block"><i class="fa fa-spin fa-spinner" ></i> Loading please wait...</span>  </div>')
    $("#stf_list").addClass('hide');
    var orgvalueid = $(ctrl).val();
    appointmentSettings.loadsubtree = $.ajax({
        type: "POST",
        url: commonSettings.appointAjaxurl + "/loadSubtree",
        data: {orgvalueid: orgvalueid},
        beforeSend: function () {
            if (appointmentSettings.loadsubtree != null) {
                appointmentSettings.loadsubtree.abort()
            }
        },
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
            $('#stf_list').removeClass('hide');
        }
    });
};

appointmentSettings.loadStaffOnboard = function () {
    var searchData = {};
    var err = 0, error = '';
    $("#assignStaffSearchForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n";
            } else {
                searchData[$(this).attr('name')] = $.trim($(this).val());
            }
        } else {
            searchData[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.onboardUrl + "/loadStaffOnboard",
            data: searchData,
            success: function (res) {
                if (res) {
                    $('#staffDet').removeClass('hide')
                    $('#staffDet').html(res);
                }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
        return false;
    }

};
appointmentSettings.changeStatus = function (statusData) {
    $.ajax({
        type: "POST",
        url: commonSettings.appointAjaxurl + "/changeApprovalStatus",
        data: statusData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
               if (statusData.fromPage == 'allapproval' || statusData.fromPage == 'ApprovalView') {
                    if(statusData.fromPage == 'ApprovalView'){
                        viewapproval.modal('hide'); 
                    }
                    var n_dom = [], Dtype = 'information';
                    Dlayout = 'topRight';
                    n_dom[0] = '<div class="activity-item"> <i class="zmdi zmdi-check-all"></i> \n\
                    <div class="activity">  <a href="#"></a> Approval Status Updated <span> </span> </div> </div>';
                    announcementSettings.PreviewGen(Dtype, n_dom[0], Dlayout);
                    announcementSettings.reload_inbox();
                } else {
                    var lblcls = res.type == 'error' ? 'danger' : 'success';
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                    $('#messageBoxModal').modal();
                    $('#msg_ok').unbind('click').click(function (event) {
                        $('#messageBoxModal').modal('hide');
                        if (res['type'] == 'success') {
                            if (statusData.fromPage == 'Appointment') {
                                staffCycleSettings.loadAppointment();
                            } else if (statusData.fromPage == 'StfApplication') {
                                staffCycleSettings.loadStafflist();
                            } else {
                                window.location.reload()
                            }
                        }
                    });
                    return false;
                }
            }
        }
    });
};

appointmentSettings.viewForwardDetails = function (ctrl) {
    var appData = {};
    appData.itemID = $(ctrl).attr('itemID');
    appData.appTypId = $(ctrl).attr('appType');
    appData.appitemID = $(ctrl).attr('appitemID');
    console.log(appData);
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/viewForwardHistory",
        data: appData,
        //dataType: "JSON",
        success: function (res) {
//            console.log(res)
            $('#messageBoxModal .msgBox-title').html('List of Escalation made for this request');
            $('#messageBoxModal .msgBox-body').html(res);
            $('#messageBoxModal').modal();
        }
    });
};

appointmentSettings.appointStaff = function (ctrl) {
    var appData = {};
    appData.itemID = $(ctrl).attr('itemID');
    console.log(appData);
    $.ajax({
        type: "POST",
        url: commonSettings.appointAjaxurl + "/appointStaff",
        data: appData,
        dataType: "JSON",
        success: function (res) {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html(res['message']);
            $('#messageBox').click();
            $('#msg_ok').unbind('click').click(function () {
                if (res['type'] == 'error') {
                    $('#msg_ok').unbind('click').click();
                    return false;
                }
                else if (res['type'] == 'success') {
                    // window.location.reload();     
                    staffSearchSettings.loadStaffListdet();
                    //staffCycleSettings.loadStaffdet();
                }
            });
        }
    });
};

appointmentSettings.loadStaffdetbyid = function (appointment_no)
{
    $.ajax({
        type: "GET",
        url: commonSettings.staffAjaxurl + "/edit",
        data: {appointment_no: appointment_no},
        dataType: "html",
        success: function (res) {
            $('#staffprof').html(res);
        }
    });

};
appointmentSettings.requestForwardDetails = function (ctrl) {
    var staffData = {};
    staffData.type = 'Appointment';
    staffData.itemID = $(ctrl).attr('ItemID');
    staffData.status = 'Forwarded';
    staffData.fromPage = 'Appointment_forward';
    console.log(staffData)
    approvalSettings.checkReqStatus(staffData);
};