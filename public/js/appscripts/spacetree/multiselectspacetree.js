var labelType, useGradients, nativeTextSupport, animate;
var array_id = Array();
var array_name = Array();
var idx = new Array();
var $nameArr = Array
var map = {};

var newname = [];



var Log = {
    elem: false,
    write: function (text) {
        if (!this.elem)
            this.elem = document.getElementById('log');
        this.elem.innerHTML = text;
//        this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
    }
};


function multiselectinittree(tree, node_id, node_name, nodesel) {

    //init Spacetree
    //Create a new ST instance

    var json = tree;

    var first_time = 1;
    var st = new $jit.ST({
        //id of viz container element
        injectInto: 'infovis',
        //set duration for the animation
        duration: 800,
        //set animation transition type
        transition: $jit.Trans.Quart.easeInOut,
        //set distance between node and its children
        levelDistance: 50,
        orientation: "left",
        //enable panning
        Navigation: {
            enable: true,
            panning: true
        },
        //set node and edge styles
        //set overridable=true for styling individual
        //nodes or edges
        Node: {
            height: 40,
            width: 120,
            color: '#FFFFFF',
            overridable: true,
            type: "adv-rect",
            CanvasStyles: {
//                fillStyle: "#1fb5ac", // background color
//                strokeStyle: "#1fb5ac", // border color
                lineWidth: 0.5,
                radius: 4,
                padding: 6
            }
        },
        Edge: {
            type: 'bezier',
            overridable: true
        },
        onBeforeCompute: function (node) {

            if (first_time == 0)
            {
                var pars = node.getParents();
                var name = node.name;
                var id = node.id;
                while (pars.length > 0)
                {
                    id = pars[0].id + '-' + id;
                    name = pars[0].name + '>>' + name;
                    pars = pars[0].getParents();
                }
                var start = array_id.length;
                array_id = jQuery.grep(array_id, function (a) {
                    return a !== id;
                });
                var end = array_id.length;

                var start_name = array_name.length;
                array_name = jQuery.grep(array_name, function (a) {
                    return a !== name;
                });
                var end_name = array_name.length;

                if (start == end)
                {
                    array_id.push(id);
                    array_name.push(name);
                    var checkisreplaced = '', i = 0;
                    if (newname.length > 0) {
                        $.each(newname, function (ii, vv) {
                            checkisreplaced = name.replace(vv, '');
                            if (checkisreplaced != name) {
                                newname[ii] = name;
                                i = 1;
                                return;
                            }
                        });
                    }
                    if (i == 0) {
                        newname.push(name);
                    }
                    id = array_id.join(':');
                    name = array_name.join(':');
//                    $.each(array_name, function (i, v) {

//                    }) 
//                    id = $('#' + node_id).val() + ':' + id;
//                    name = $('#' + node_name).val() + ':' + name;
                    $('#' + node_id).val(id);
                    $('#' + node_name).val(newname.join('<br>'));

                    Log.write('  <div class="col-md-12"> <h4>Selected Node List :</h4>'
                            + name.split(':').join('<br>') + '  </div>');
                    // alert($('#' + node_name).val());
//node.data.$color = "#1fb5ac";
                    $('#' + node.id).css('font-weight', '600');
                }
                else
                {
                    $('#' + node.id).css('font-weight', '400');
                    id = array_id.join(':');
                    name = array_name.join(':');

                    $('#' + node_id).val(id);
                    $('#' + node_name).val(name);

                    Log.write('  <div class="col-md-12"> <h4>Selected Node List :</h4>'
                            + name.split(':').join('<br>') + '  </div>');
                    alert($('#' + node_name).val());
                }
            }
            else
            {
                first_time = 0;
            }



        },
        onAfterCompute: function () {
        },
        //This method is called on DOM label creation.
        //Use this method to add event handlers and styles to
        //your node.
        onCreateLabel: function (label, node) {

            label.id = node.id;
            label.innerHTML = node.name;
            label.onclick = function () {
//            	if(normal.checked) {
                st.onClick(node.id, {selectionType: 'Multiple'});
//            	} else {
//                st.setRoot(node.id, 'animate');
//            	}
            };
            //set label styles
            var style = label.style;
//            style.width = 'auto !important';
//            style.minWidth = 100 +'px';
//            style.height = 25 + 'px';
            style.cursor = 'pointer';
//            style.backgroundColor = '#eee';
            style.color = '#FFFFFF';
            style.fontSize = '0.8em';
            style.textAlign = 'center';
            style.fontWeight = '400';
            style.padding = '6px';
            style.textAlign = 'center';
            style.wordBreak = 'break-word';
            style.maxWidth = 120 + 'px';
//            style.borderColor = '1px solid #555';

//            style.paddingTop = '3px';
        },
        //This method is called right before plotting
        //a node. It's useful for changing an individual node
        //style properties before plotting it.
        //The data properties prefixed with a dollar
        //sign will override the global node style properties.
        onBeforePlotNode: function (node) {
            //add some color to the nodes in the path between the
            //root node and the selected node
            if (node.selected) {
                node.data.$color = "#1fb5ac";
            }
            else {
                delete node.data.$color;
                //if the node belongs to the last plotted level
                if (!node.anySubnode("exist")) {
                    //count children number
                    var count = 0;
                    node.eachSubnode(function (n) {
                        count++;
                    });
                    //assign a node color based on
                    //how many children it has
                    node.data.$color = ['#aaa', '#aaa', '#aaa', '#aaa', '#aaa', '#aaa'][count];
                }
            }
        },
        //This method is called right before plotting
        //an edge. It's useful for changing an individual edge
        //style properties before plotting it.
        //Edge data proprties prefixed with a dollar sign will
        //override the Edge global style properties.
        onBeforePlotLine: function (adj) {
            if (adj.nodeFrom.selected && adj.nodeTo.selected) {
                adj.data.$color = "#eed";
                adj.data.$lineWidth = 3;
            }
            else {
                delete adj.data.$color;
                delete adj.data.$lineWidth;
            }
        }
    });
    //load json data
    st.loadJSON(json);

    //compute node positions and layout
    st.compute();

    //optional: make a translation of the tree
    st.geom.translate(new $jit.Complex(-200, 0), "current");
    //emulate a click on the root node.
    st.onClick(st.root);
    $.each(nodesel, function (i, n) {
        st.select(n);
    });
    //end
    //Add event handlers to switch spacetree orientation.
//    var top = $jit.id('r-top'), 
//        left = $jit.id('r-left'), 
//        bottom = $jit.id('r-bottom'), 
//        right = $jit.id('r-right'),
//        normal = $jit.id('s-normal');


//    function changeHandler() {
//        if(this.checked) {
//            top.disabled = bottom.disabled = right.disabled = left.disabled = true;
//            st.switchPosition(this.value, "animate", {
//                onComplete: function(){
//                    top.disabled = bottom.disabled = right.disabled = left.disabled = false;
//                }
//            });
//        }
//    };

//    top.onchange = left.onchange = bottom.onchange = right.onchange = changeHandler;
    //end

}


function multiselectinittreemodified(tree, node_id, node_name) {

    //init Spacetree
    //Create a new ST instance

    var json = tree;

    var first_time = 1;

    var st = new $jit.ST({
        //id of viz container element
        injectInto: 'infovis',
        //set duration for the animation
        duration: 800,
        //set animation transition type
        transition: $jit.Trans.Quart.easeInOut,
        //set distance between node and its children
        levelDistance: 50,
        orientation: "left",
        //enable panning
        Navigation: {
            enable: true,
            panning: true
        },
        //set node and edge styles
        //set overridable=true for styling individual
        //nodes or edges
        Node: {
            height: 20,
            width: 60,
            type: 'rectangle',
            color: '#aaa',
            overridable: true
        },
        Edge: {
            type: 'bezier',
            overridable: true
        },
        onBeforeCompute: function (node) {

            if (first_time == 0)
            {
                var pars = node.getParents();
                var name = node.name;
                var id = node.id;
                var obj = {idx: id, id: id, name: name, namex: name};
//                console.log(obj)
                while (pars.length > 0)
                {
//                    console.log('obj.id'+obj.id)
//                    console.log('pid'+pid)
                    var pid = pars[0].id;
                    var pname = pars[0].name;
                    if (!map[pid]) {
                        map[pid] = {
                            idx: pid + '-' + obj.idx,
                            id: pid,
                            namex: pname + '>>' + obj.namex,
                            name: pname,
                            child: {}
                        };
                    } else {
//                        map[pid].idx = map[pid].idx + '-' + obj.id;
                    }
                    console.log(map[pid])
                    map[pid].child[obj.id] = obj;
                    obj = map[pid];
                    pars = pars[0].getParents();
                }
                var check = '';
//                    console.log(map)
                $.each(map, function (val, i) {
                    check = objLoop(val)

                });
                console.log(check)
//                    $('#' + node_id).val(id);
//                    $('#' + node_name).val(name);

            }
            else
            {
                first_time = 0;
            }



        },
        onAfterCompute: function () {
        },
        //This method is called on DOM label creation.
        //Use this method to add event handlers and styles to
        //your node.
        onCreateLabel: function (label, node) {

            label.id = node.id;
            label.innerHTML = node.name;
            label.onclick = function () {
//            	if(normal.checked) {
                st.onClick(node.id);
//            	} else {
//                st.setRoot(node.id, 'animate');
//            	}
            };
            //set label styles
            var style = label.style;
            style.width = 60 + 'px';
            style.height = 17 + 'px';
            style.cursor = 'pointer';
            style.color = '#333';
            style.fontSize = '0.8em';
            style.textAlign = 'center';
            style.paddingTop = '3px';
        },
        //This method is called right before plotting
        //a node. It's useful for changing an individual node
        //style properties before plotting it.
        //The data properties prefixed with a dollar
        //sign will override the global node style properties.
        onBeforePlotNode: function (node) {
            //add some color to the nodes in the path between the
            //root node and the selected node.
            if (node.selected) {
                node.data.$color = "#ff7";
            }
            else {
                delete node.data.$color;
                //if the node belongs to the last plotted level
                if (!node.anySubnode("exist")) {
                    //count children number
                    var count = 0;
                    node.eachSubnode(function (n) {
                        count++;
                    });
                    //assign a node color based on
                    //how many children it has
                    node.data.$color = ['#aaa', '#baa', '#caa', '#daa', '#eaa', '#faa'][count];
                }
            }
        },
        //This method is called right before plotting
        //an edge. It's useful for changing an individual edge
        //style properties before plotting it.
        //Edge data proprties prefixed with a dollar sign will
        //override the Edge global style properties.
        onBeforePlotLine: function (adj) {
            if (adj.nodeFrom.selected && adj.nodeTo.selected) {
                adj.data.$color = "#eed";
                adj.data.$lineWidth = 3;
            }
            else {
                delete adj.data.$color;
                delete adj.data.$lineWidth;
            }
        }
    });
    //load json data
    st.loadJSON(json);

    //compute node positions and layout
    st.compute();

    //optional: make a translation of the tree
    st.geom.translate(new $jit.Complex(-200, 0), "current");
    //emulate a click on the root node.
    st.onClick(st.root);


    //end
    //Add event handlers to switch spacetree orientation.
//    var top = $jit.id('r-top'), 
//        left = $jit.id('r-left'), 
//        bottom = $jit.id('r-bottom'), 
//        right = $jit.id('r-right'),
//        normal = $jit.id('s-normal');


//    function changeHandler() {
//        if(this.checked) {
//            top.disabled = bottom.disabled = right.disabled = left.disabled = true;
//            st.switchPosition(this.value, "animate", {
//                onComplete: function(){
//                    top.disabled = bottom.disabled = right.disabled = left.disabled = false;
//                }
//            });
//        }
//    };

//    top.onchange = left.onchange = bottom.onchange = right.onchange = changeHandler;
    //end

}
function objLoop(obj) {
    $.each(map, function (val, i) {
        idx.push(i)
        if (_.size(val) > 0) {
            objLoop(val)
        } else {
            return idx.join(',');
        }

    });
}
