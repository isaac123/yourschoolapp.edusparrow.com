var spacetreeutilsSettings = {};
spacetreeutilsSettings.loadspacetree = 0;
spacetreeutilsSettings.defaultSeleted = [];

spacetreeutilsSettings.loadSpaceTreeForAllExam = function (cycle, node_id, node_name) {
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectspacetree = 0;
    spacetreeutilsSettings.singleManNode = 1
    $.ajax({
        type: "POST",
        url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
        data: {cycle: cycle},
        dataType: "html",
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Choose a Node');
            $('.formBox-body').html(res);
            $('#confirm_close').removeClass('hide');
            $('#iconclose').removeClass('hide');
            $('#formModal #confirm_save').html('Save');
            $('#formModal').modal();

            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                var id = '"' + $('#' + node_id).val() + '"';


                $('#returned_node_nameall').html($('#' + node_name).val());
                var app = "<a href='javascript:void(0)' class='' onclick='examSettings.createExam(" + id + ")' title='Add Subject'>" +
                        "<i class='fa fa-plus activity-icon bg-red' style='margin-top: 6px;'></i></a>"
                $('#returned_node_nameall').append(app);
                $('#added_category').removeClass('hide');
            });
        }
    });
};

spacetreeutilsSettings.loadSpaceTreeForSubject = function (cycle, node_id, node_name) {
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectinittree = 0;
    assignSettings.fromAssigning = 1;
    if (cycle != '')
    {
        $.ajax({
            type: "POST",
            url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
            data: {cycle: cycle},
            dataType: "html",
            success: function (res) {
                $('#msg_ok').unbind('click').click();
                $('#confirm_close').unbind('click').click();
                $('.formBox-title').html('Choose a Node');
                $('.formBox-body').html(res);
                $('#confirm_close').removeClass('hide');
                $('#iconclose').removeClass('hide');
                $('#formModal #confirm_save').html('Save');
                $('#formModal').modal();

                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    var id = '"' + $('#' + node_id).val() + '"';


                    $('#returned_node_nameall').html($('#' + node_name).val());
                    var app = "<a href='javascript:void(0)' class='' onclick='assignSettings.createSubject(" + id + ")' title='Add Subject'>" +
                            "<i class='fa fa-plus activity-icon bg-red' style='margin-top: 6px;'></i></a>"
                    $('#returned_node_nameall').append(app);
                    $('#added_category').removeClass('hide');
                });
            }
        });
    }
    else
    {
        swal("Error", "Academic Year Is Missing!", "error");
    }
};

spacetreeutilsSettings.loadSpaceTreeForSubjectTeachers = function (cycle, node_id, node_name) {
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectinittree = 1;
    spacetreeutilsSettings.fromFeeMap = 1;
//    assignSettings.fromAssigning = 1;
    $.ajax({
        type: "POST",
        url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
        data: {cycle: spacetreeutilsSettings.cycle},
        dataType: "html",
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Choose a Node');
            $('.formBox-body').html(res);
            $('#confirm_close').removeClass('hide');
            $('#iconclose').removeClass('hide');
            $('#formModal #confirm_save').html('Save');
            $('#formModal').modal();

            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                var id = '"' + $('#' + node_id).val() + '"';

                $('#returned_node_name').html($('#' + node_name).val());
                $('#added_category').removeClass('hide');

                if ($('#' + node_id).val() == '')
                {
                    //No Node is selected
                    alert('No Nodes has been selected');
                }
                else
                {
                    //Get the count of students

                    //Get the subjects
                    assignSettings.loadStuCountnSubj($('#' + node_id).val())
//                    $.ajax({
//                        type: "POST",
//                        url: commonSettings.assigningAjaxurl + "/getStudentCountAndSubjects",
//                        data: {node_id: $('#' + node_id).val()},
//                        success: function (res) {
//                            $('#common_subjects').html(res);
//                        }
//                    });
                }
            });
        }
    });
};

spacetreeutilsSettings.loadSpaceTreeForClassTeachers = function (cycle, node_id, node_name) {
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectspacetree = 1;
    $.ajax({
        type: "POST",
        url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
        data: {cycle: spacetreeutilsSettings.cycle},
        dataType: "html",
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Choose a Node');
            $('.formBox-body').html(res);
            $('#formModal').modal();

            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                var id = '"' + $('#' + node_id).val() + '"';

                $('#returned_node_name').html($('#' + node_name).val());
                $('#added_category').removeClass('hide');

                if ($('#' + node_id).val() == '')
                {
                    //No Node is selected
                    alert('No Nodes has been selected');
                }
                else
                {
                    //Get the count of students

                    //Get the subjects
                    $.ajax({
                        type: "POST",
                        url: commonSettings.assigningAjaxurl + "/getStudentCount",
                        data: {node_id: $('#' + node_id).val()},
                        success: function (res) {
                            $('#common_subjects').html(res);
                        }
                    });
                }
            });
        }
    });
};
spacetreeutilsSettings.loadSpaceTreeForSectionAssigning = function (cycle, node_id, node_name) {
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectspacetree = 1;
    assignSettings.fromAssigning = 1;

    console.log(spacetreeutilsSettings)
    $.ajax({
        type: "POST",
        url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
        data: {cycle: spacetreeutilsSettings.cycle},
        dataType: "html",
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Choose a Node');
            $('.formBox-body').html(res);
            $('#confirm_close').removeClass('hide');
            $('#iconclose').removeClass('hide');
            $('#formModal #confirm_save').html('Save');
            $('#formModal').modal();

            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                var id = '"' + $('#' + node_id).val() + '"';

                $('#returned_node_name').html($('#' + node_name).val());
                $('#added_category').removeClass('hide');

                if ($('#' + node_id).val() == '')
                {
                    //No Node is selected
                    alert('No Nodes has been selected');
                }
                else
                {
                }
            });
        }
    });
};

spacetreeutilsSettings.loadSpaceTreeForFee = function (cycle, node_id, node_name) {
    console.log('loadSpaceTreeForFee')
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectspacetree = 1;
    spacetreeutilsSettings.fromFeeMap = 1;
    spacetreeutilsSettings.fromFeeNode = 0;
    if (cycle != '')
    {
        $.ajax({
            type: "POST",
            url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
            data: {cycle: cycle},
            dataType: "html",
            success: function (res) {
                $('.formBox-title').html('Choose a Node');
                $('.formBox-body').html(res);
                $('#confirm_close').removeClass('hide');
                $('#iconclose').removeClass('hide');
                $('#formModal #confirm_save').html('Save');
                $('#formModal').modal();
                $('#confirm_close').unbind().on('click', function () {
                    $('#' + node_id).val('')
                    $('#' + node_name).val('')
                    $('#formModal .formBox-body').html('');
                });
                $('#confirm_save').unbind('click').click(function () {
                    $('#formModal').modal('hide')
                    $('#formModal .formBox-body').html('');
                    var id = '"' + $('#' + node_id).val() + '"';

                    $('#returned_node_name').html($('#' + node_name).val());
                    $('#added_category').removeClass('hide');

                    if ($('#' + node_id).val() == '')
                    {
                        //No Node is selected
                        alert('No Nodes has been selected');
                    }
                });
            }
        });

        //Get the list of fee for each available node
//        $.ajax({
//            type: "POST",
//            url: commonSettings.feeAjaxurl + "/getNodesAndFees",
//            data: {
//                cycle: spacetreeutilsSettings.cycle
//            },
//            success: function (res) {
//                if (res) {
//                    $('#allFeedetails').removeClass('hide')
//                    $('#allFeedetails').html(res);
//                }
//
//            }
//        });
    }
    else
    {
        swal("Error", "Academic Year Is Missing!", "error");
    }
};

spacetreeutilsSettings.loadSpaceTreeForFeeNames = function (cycle, node_id, node_name) {
    console.log('loadSpaceTreeForFeeNames')
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectspacetree = 0;
    spacetreeutilsSettings.fromFeeMap = 0;
    spacetreeutilsSettings.fromFeeNode = 1;

    if (cycle != '')
    {
        $.ajax({
            type: "POST",
            url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
            data: {cycle: cycle},
            dataType: "html",
            success: function (res) {
                $('.formBox-title').html('Choose a Node');
                $('.formBox-body').html(res);
                $('#confirm_close').removeClass('Hide');
                $('#iconclose').removeClass('Hide');
                $('#formModal #confirm_save').html('Save');
                $('#formModal').modal();

                $('#confirm_close').unbind('click').click(function () {
                    $('#' + node_id).val('')
                    $('#' + node_name).val('')
                    $('#formModal .formBox-body').html('');
                });
                $('#confirm_save').unbind('click').click(function () {
                    $('#formModal').modal('hide')
                    $('#formModal .formBox-body').html('');
                    var id = '"' + $('#' + node_id).val() + '"';

                    $('#returned_node_name').html($('#' + node_name).val());
                    $('#added_category').removeClass('hide');

                    if ($('#' + node_id).val() == '')
                    {
                        //No Node is selected
                        alert('No Nodes has been selected');
                    }
                });
            }
        });
    }
    else
    {
        swal("Error", "Fee Missing!", "error");
    }
};
spacetreeutilsSettings.loadSpaceTreeForPromotion = function (cycle, node_id, node_name) {
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectspacetree = 1;
    spacetreeutilsSettings.fromPromotion = 1;
    console.log(spacetreeutilsSettings)
    $.ajax({
        type: "POST",
        url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
        data: {cycle: spacetreeutilsSettings.cycle},
        dataType: "html",
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Choose a Node');
            $('.formBox-body').html(res);
            $('#confirm_close').removeClass('hide');
            $('#iconclose').removeClass('hide');
            $('#formModal #confirm_save').html('Save');
            $('#formModal').modal();

            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                var id = '"' + $('#' + node_id).val() + '"';

                $('#returned_node_name').html($('#' + node_name).val());
                $('#added_category').removeClass('hide');

                if ($('#' + node_id).val() == '')
                {
                    //No Node is selected
                    alert('No Nodes has been selected');
                }
                else
                {
                }
            });
        }
    });
};
spacetreeutilsSettings.loadSpaceTreeForTransport = function (cycle, node_id, node_name) {
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectspacetree = 1;
    spacetreeutilsSettings.fromTransport = 1;
    console.log(spacetreeutilsSettings)
    $.ajax({
        type: "POST",
        url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
        data: {cycle: spacetreeutilsSettings.cycle},
        dataType: "html",
        success: function (res) {
            $('#msg_ok').unbind().click();
            $('#confirm_close').unbind().click();
            $('.formBox-title').html('Choose a Node');
            $('.formBox-body').html(res);
            $('#confirm_close').removeClass('hide');
            $('#iconclose').removeClass('hide');
            $('#formModal #confirm_save').html('Save');
            $('#formModal').modal('show');

            $('#confirm_save').unbind().on('click', function () {
                $('#confirm_close').click();
                var id = '"' + $('#' + node_id).val() + '"';

                $('#returned_node_name').html($('#' + node_name).val());
                $('#added_category').removeClass('hide');

                if ($('#' + node_id).val() == '')
                {
                    //No Node is selected
                    alert('No Nodes has been selected');
                }
                else
                {
                }
            });
        }
    });
};
$('#formModal').on('shown.bs.modal', function (e) {
    console.log('Fee1')
    console.log(spacetreeutilsSettings)
    if (spacetreeutilsSettings.loadspacetree == 1) {
        console.log(spacetreeutilsSettings.loadspacetree)
        if (spacetreeutilsSettings.fromFeeNode == 1)
        {
            console.log('1')
            $.ajax({
                type: "POST",
                url: commonSettings.spacetreeutilsAjaxurl + "/getTree",
                data: {cycle: spacetreeutilsSettings.cycle},
                dataType: "JSON",
                success: function (res) {
                    spacetreeutilsSettings.spacetree = res;
                    feeinittree(spacetreeutilsSettings.spacetree, spacetreeutilsSettings.node_id, spacetreeutilsSettings.node_name);

                }
            });
        } else if (spacetreeutilsSettings.fromFeeMap == 1) {
            console.log('2')
            $.ajax({
                type: "POST",
                url: commonSettings.spacetreeutilsAjaxurl + "/getTreeForMainNodes",
                data: {cycle: spacetreeutilsSettings.cycle},
                dataType: "JSON",
                success: function (res) {
                    spacetreeutilsSettings.spacetree = res;
                    multiselectinittree(spacetreeutilsSettings.spacetree, spacetreeutilsSettings.node_id, spacetreeutilsSettings.node_name);
                }
            });
        } else if (spacetreeutilsSettings.singleManNode == 1) {
            console.log('3')
            $.ajax({
                type: "POST",
                url: commonSettings.spacetreeutilsAjaxurl + "/getTreeForMainNodes",
                data: {cycle: spacetreeutilsSettings.cycle},
                dataType: "JSON",
                success: function (res) {
                    spacetreeutilsSettings.spacetree = res;
                    inittree(spacetreeutilsSettings.spacetree, spacetreeutilsSettings.node_id, spacetreeutilsSettings.node_name);
                }
            });
            spacetreeutilsSettings.loadspacetree = 0;
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: commonSettings.spacetreeutilsAjaxurl + "/getTree",
                data: {cycle: spacetreeutilsSettings.cycle},
                dataType: "JSON",
                success: function (res) {
                    spacetreeutilsSettings.spacetree = res;
                    if (spacetreeutilsSettings.multiselectspacetree)
                    {
                        multiselectinittree(spacetreeutilsSettings.spacetree, spacetreeutilsSettings.node_id, spacetreeutilsSettings.node_name, spacetreeutilsSettings.defaultSeleted);

                    }
                    else
                    {
                        inittree(spacetreeutilsSettings.spacetree, spacetreeutilsSettings.node_id, spacetreeutilsSettings.node_name);
                    }
                }
            });
        }
    }

});
spacetreeutilsSettings.loadSpaceTreeForEnquiryNames = function (cycle, node_id, node_name, selectedval) {
    var $self = $('#' + node_name);
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectspacetree = 1;
    spacetreeutilsSettings.fromTransport = 1;
    spacetreeutilsSettings.defaultSeleted = selectedval ? (selectedval.split(',')) : [];
    console.log(spacetreeutilsSettings)
    $.ajax({
        type: "POST",
        url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
        data: {cycle: spacetreeutilsSettings.cycle},
        dataType: "html",
        success: function (res) {
            $('#msg_ok').unbind().click();
            $('#confirm_close').unbind().click();
            $('#formModal .formBox-title').html('Choose a Node');
            $('#formModal .formBox-body').html(res);
            $('#confirm_close').removeClass('hide');
            $('#iconclose').removeClass('hide');
            $('#formModal #confirm_save').html('Save');
            $('#formModal').modal('show');
            $('#confirm_close').unbind().on('click', function () {
                $('#' + node_id).val('')
                $('#' + node_name).val('')
            });

            $('#confirm_save').unbind().on('click', function () {
                $('#formModal').modal('hide');
                var id = '"' + $('#' + node_id).val() + '"';

                $('#returned_node_name').html($('#' + node_name).val());
                $('#added_category').removeClass('hide');

                if ($('#' + node_id).val() == '')
                {
                    //No Node is selected
                    alert('No Nodes has been selected');
                }
                else
                {

                    enquiryProfileSettings.updateSubtree($self)
                }
            });
        }
    });
};
spacetreeutilsSettings.loadSpaceTreeForAttendance = function (cycle, node_id, node_name, staff) {
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectspacetree = 0;
    spacetreeutilsSettings.singleManNode = 1
    $.ajax({
        type: "POST",
        url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
        data: {cycle: cycle},
        dataType: "html",
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Choose a Node');
            $('.formBox-body').html(res);
            $('#confirm_close').removeClass('hide');
            $('#iconclose').removeClass('hide');
            $('#formModal #confirm_save').html('Save');
            $('#formModal').modal();
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                var id = '"' + $('#' + node_id).val() + '"';
                $('#returned_node_nameall').html($('#' + node_name).val());
                if (staff == "staff") {
                    var app = "<a href='javascript:void(0)' class='' onclick='staffratingSettings.assignPeriods(" + id + ")' title='Add Periods'>" +
                            "<i class='fa fa-plus activity-icon bg-red' style='margin-top: 6px;'></i></a>"
                } else {
                    var app = "<a href='javascript:void(0)' class='' onclick='settings.assignPeriods(" + id + ")' title='Add Periods'>" +
                            "<i class='fa fa-plus activity-icon bg-red' style='margin-top: 6px;'></i></a>"
                }
                $('#returned_node_nameall').append(app);
                $('#added_category').removeClass('hide');
            });
        }
    });
};