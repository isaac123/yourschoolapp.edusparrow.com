var admissionSettings = {};
admissionSettings.admitStudent = function(studentData) {
    $.ajax({
        type: "POST",
        url: commonSettings.admissionAjaxurl + "/admitStudent",
        data: studentData,
        dataType: "JSON",
        success: function(res) {
            $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function() {  
                    if (res['type'] == 'error'){
                        $('#msg_ok').unbind('click').click();    
                        return false;
                    }
                    else if (res['type'] == 'success'){
                       // window.location.reload();
                       
                       manageAppSettings.loadApplicationList();
                    }
                });

        }
    });
};