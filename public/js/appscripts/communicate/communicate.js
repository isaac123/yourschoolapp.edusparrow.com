var communicateSettings = {};

communicateSettings.mailpager = {};
communicateSettings.mailactionData = {};

communicateSettings.loadSMS = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.mailAjaxurl + "/index",
        dataType: "html",
        success: function (res) {
            $('#smsDiv').html(res);
            $('#smsDiv').removeClass('hide')
        }
    });
};
communicateSettings.loadMessage = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.mailAjaxurl + "/index",
        dataType: "html",
        success: function (res) {

            $("#SENTMAIL").removeClass("active");
            $("#INBOX").addClass("active");
            $("#IMPORTANT").removeClass("active");
            $('#mail_content').html(res);
            $('#mail_content').removeClass('hide')
        }
    });
};

communicateSettings.loadMessageSidePanel = function (activeTab) {

    console.log('loadsidepanel')
    $.ajax({
        type: "POST",
        url: commonSettings.mailAjaxurl + "/loadSidePanel",
        data: {activeTab: activeTab},
        dataType: "html",
        success: function (res) {
            $('.mail-sidenav').html(res);
            $('.mail-sidenav').removeClass('hide')
        }
    });
};
communicateSettings.loadAnnouncement = function () {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.announcementAjaxurl + "/index",
        dataType: "html",
        success: function (res) {
            $('#announcementDiv').html(res);
            $('#announcementDiv').removeClass('hide')
        }
    });
};
communicateSettings.loadEvent = function () {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.eventAjaxUrl + "/eventManagement",
        dataType: "html",
        success: function (res) {
            $('#eventDiv').html(res);
            $('#eventDiv').removeClass('hide')
        }
    });
};
communicateSettings.loadEnquiry = function () {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.enquiryAjaxUrl + "/enquiryManagement",
        dataType: "html",
        success: function (res) {
            $('#enquiryDiv').html(res);
            $('#enquiryDiv').removeClass('hide')
        }
    });
};
communicateSettings.loadEnquiryAllTypes = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.enquiryallAjaxurl + "/enquiryallManag",
        dataType: "html",
        success: function (res) {
            $('#enquiryDiv').html(res);
            $('#enquiryDiv').removeClass('hide')
        }
    });
};

communicateSettings.addEvent = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.communicateAjaxUrl + "/addEvent",
        dataType: "html",
        success: function (res) {
            $('#eventDiv').html(res);
            $('#eventDiv').removeClass('hide')
        }
    });
};
communicateSettings.loadSmsPanel = function () {
    console.log('cliekced')
    $.ajax({
        type: "POST",
        url: commonSettings.smsAjaxUrl + "/schoolSms",
        dataType: "html",
        success: function (res) {
            $('#smsDiv').html(res);
            $('#smsDiv').removeClass('hide')
        }
    });
};


communicateSettings.usertype = function (usrtyp)
{
    $.ajax({
        type: "POST",
        url: commonSettings.communicateAjaxUrl + "/slctUserForm",
        data: {slctduser: usrtyp},
        dataType: "html",
        success: function (res) {
            $('#formModal  .formBox-title').html('Add User');

            $('#formModal .formBox-body').html(res);
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#confirm_save').html('Save');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                communicateSettings.saveNewUser();
            });
        }
    });

};


communicateSettings.addNewUser = function (usrtyp)
{
    $.ajax({
        type: "POST",
        url: commonSettings.communicateAjaxUrl + "/newUsers",
        data: {slctduser: usrtyp},
        dataType: "html",
        success: function (res) {

            $('#userform').html(res);

        }
    });

};

communicateSettings.saveNewUser = function ()
{
    var userData = {};
    var usrtype = '';
    $("#addNewUserForm").find('input,select,textarea').each(function () {

        userData[$(this).attr('name')] = $.trim($(this).val());

        if ($(this).attr('name') == 'usrtype')
            usrtype = $.trim($(this).val());
    });

    console.log(userData);
    //saveNewUser
    $.ajax({
        type: "POST",
        url: commonSettings.communicateAjaxUrl + "/saveNewUser",
        data: userData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                $('#messageBoxModal  .msgBox-title').html('Message');
                $('#messageBoxModal .msgBox-body').html(res['message']);
                $('#messageBoxModal ').modal();
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                        $('#formModal').modal();
                    }
                    else if (res['type'] == 'success') {
                        communicateSettings.loadUserdet();
//                         window.location.reload();
//                        setTimeout(function () {
//                            window.location.href = window.location.pathname + window.location.search
//                        }, 0);
                    }
                });

            }

        }
    });
};


communicateSettings.editusers = function (userid, cntrl)
{
    var slctduser = $(cntrl).attr('slctd-user');
    //  alert(slctduser);

    $.ajax({
        type: "POST",
        url: commonSettings.communicateAjaxUrl + "/editUserByID",
        data: {
            userid: userid,
            slctduser: slctduser
        },
        dataType: "html",
        success: function (res) {
            $('#formModal  .formBox-title').html('Edit User Details');
            $('#formModal .formBox-body').html(res);
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });

            $('#confirm_save').html('Update');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                communicateSettings.updateUser(userid, slctduser);
            });

        }
    });
};

communicateSettings.updateUser = function (userid, slctduser)
{
    //alert(userid +'-'+slctduser)

    var userData = {};

    $("#editUserForm").find('input,select,textarea').each(function () {

        userData[$(this).attr('name')] = $.trim($(this).val());

    });
    userData['id'] = userid;
    userData['slctduser'] = slctduser;
    console.log(userData);
    //editUser
    $.ajax({
        type: "POST",
        url: commonSettings.communicateAjaxUrl + "/updateUser",
        data: userData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                $('#messageBoxModal .msgBox-title').html('Message');
                $('#messageBoxModal .msgBox-body').html(res['message']);
                $('#messageBoxModal').modal();
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                        $('#formModal').modal();

                    }
                    else if (res['type'] == 'success') {
                        communicateSettings.loadUserdet();
//                        window.location.reload();
//                        setTimeout(function () {
//                            window.location.href = window.location.pathname + window.location.search
//                        }, 0);
                    }
                });

            }

        }
    });
};

communicateSettings.resetPassword = function (userid)
{
    $.ajax({
        type: "POST",
        url: commonSettings.communicateAjaxUrl + "/resetPasswordByUserID",
        data: {
            userid: userid,
        },
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();

                    }
                    else if (res['type'] == 'success') {
                        communicateSettings.loadUserdet();
                        // window.location.reload();
//                        setTimeout(function () {
//                            window.location.href = window.location.pathname + window.location.search
//                        }, 0);
                    }
                });
            }

        }
    });
};

communicateSettings.loadUserdet = function ()
{

    var params = {};
    $.ajax({
        type: "POST",
        url: commonSettings.communicateAjaxUrl + "/loadtableHeader",
        dataType: "html",
        success: function (res) {
            //loadReport
            //alert(res);
            $('#usersDiv').html(res);
            $('#usersDiv').removeClass('hide');

            var tablectrl = '#userdet';
            console.log(tablectrl)
            communicateSettings.noOfRecordsToDisplay = 10;
            var column = [1, 3];
            var sortingDisabled = [0, 2, 4];

            $(tablectrl).dataTable({
                "sDom": 'C<"clear">lrtip',
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": 'loadtableData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": communicateSettings.noOfRecordsToDisplay,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[1, "desc"]],
                "bLengthChange": false,
                "bInfo": false,
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function (o) {
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                    var rowText = '<tr >';

                    rowText += '<td class="student_name "> ';
                    rowText += aData.name;
                    rowText += '</td>';

                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.loginid + '</span>';
                    rowText += '</td>';

                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.utype + '</span>';
                    rowText += '</td>';

                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.email + '</span>';
                    rowText += '</td>';

//                        rowText += '<td class="new-patient invoiceview sorting_1">';
//                        rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.dob + ' </span>';
//                        rowText += '</td>';


                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mini-stat-icon-action pink"><a href="javascript:;" title="Edit" class="fa fa-edit white_font" slctd-user=' + aData.stforstu + ' onclick="communicateSettings.editusers(' + aData.id + ',this)"></a></span>';
                    rowText += '<span class="mini-stat-icon-action red-bg"><a href="javascript:;" title="Delete" class="fa fa-trash white_font"  onclick="communicateSettings.deleteusers(' + aData.id + ')"></a></span>';
                    rowText += '<span class="mini-stat-icon-action tar"><a href="javascript:;" title="Reset Password" class="fa fa-refresh white_font"  onclick="communicateSettings.resetPassword(' + aData.id + ')"></a></span>';
//                    rowText += '| <a href="javascript:;" title="Edit" onclick="communicateSettings.resetPassword(' + aData.id + ')" class="label label-danger label-mini"> Reset Password </a>';
                    rowText += '</td>';
                    rowText += '</tr>';

                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });

        }
    });

};
communicateSettings.deleteusers = function (userID) {
    swal({
        title: "Confirm",
        text: "Do you confirm to delete this user?",
        type: "warning",
        confirmButtonText: 'Yes!',
        showCancelButton: true,
        closeOnCancel: true,
        closeOnConfirm: true,
        cancelButtonText: 'Cancel',
    },
            function (isConfirm) {
                if (isConfirm) {
                    communicateSettings.deleteConfirmUser(userID)
                    return false;
                }
            });
};
communicateSettings.deleteConfirmUser = function (userID) {

    $.ajax({
        type: "POST",
        url: commonSettings.communicateAjaxUrl + "/deleteByUserId",
        data: {
            userid: userID,
        },
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();

                    }
                    else if (res['type'] == 'success') {
                        communicateSettings.loadUserdet();
                    }
                });
            }

        }
    });
};

communicateSettings.loadMessageSetting = function ()
{
    $.ajax({
        type: "POST",
        url: commonSettings.communicateAjaxUrl + "/mailSetting",
        dataType: "html",
        success: function (res) {

            $('#mail_content').html(res);
            $('#mail_content').removeClass('hide');

        }
    });

};

communicateSettings.loadWebMail = function ()
{
    $.ajax({
        type: "POST",
        url: commonSettings.communicateAjaxUrl + "/loadWebMail",
        dataType: "html",
        success: function (res) {
//            console.log(res)
            $('#mail_contentdata').removeClass('hide');

            //$('#mail_content').show();
            $('#mail_contentdata').html(res);

        }
    });

};

communicateSettings.run_waitMe = function (effect) {
    $('#loadStudet').waitMe({
        effect: effect,
        text: 'Please wait message sending...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: ''
    });
};
