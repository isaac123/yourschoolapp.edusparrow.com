var ratingSettings = {};
ratingSettings.mainCatRatinInitial = function (studentData) {
    console.log(studentData)
    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/ratingView",
        data: studentData,
        dataType: "html",
        success: function (res) {
            $('#ratingTable').html(res);
//            $('#ratingTable').parent().css('display','block')     
            $("#subratlink").addClass('tab-current');
            $("#subjRating").addClass('content-current');
            $("#clsratlink").removeClass('tab-current');
            $("#classRating").removeClass('content-current');
        }
    });

};
ratingSettings.clsCatRatinInitial = function (studentData) {
    console.log(studentData)

    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/classRatingView",
        data: studentData,
        dataType: "html",
        success: function (res) {
            $('#classRatTbl').html(res);
//            $('#classRatTbl').parent().css('display','block')
            $("#subratlink").removeClass('tab-current');
            $("#subjRating").removeClass('content-current');
            $("#clsratlink").addClass('tab-current');
            $("#classRating").addClass('content-current');
        }
    });

};
ratingSettings.mainCatRating = function (ctrl) {
    console.log($(ctrl).html())
    var params = {};
    params.student = $(ctrl).parent().attr('student_id');
    params.aggregate_key = $(ctrl).parent().attr('aggregate_key');
    params.mainCatId = ($(ctrl).val() != 0) ? $(ctrl).val() : '';
    console.log(params)

    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/ratingView",
        data: params,
        dataType: "html",
        success: function (res) {
            $('#loadRatingdatamain').html(res);
        }
    });

};

ratingSettings.clsCatCatRating = function (ctrl) {
    console.log($(ctrl).html())
    var params = {};
    params.student = $(ctrl).parent().attr('student_id');
    params.aggregate_key = $(ctrl).parent().attr('aggregate_key');
    params.mainCatId = ($(ctrl).val() != 0) ? $(ctrl).val() : '';
    console.log(params)

    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/classRatingView",
        data: params,
        dataType: "html",
        success: function (res) {
            $('#classRatTbl').html(res);
        }
    });

};

