var classtestSettings = {};
classtestSettings.subjwise = function(ctrl) {
    var params = {};
    params.student = $(ctrl).parent().attr('student_id');
    params.divValId = $(ctrl).parent().attr('divValId');
    params.subDivValId = $(ctrl).parent().attr('subDivValId');
    params.academicYrId = $(ctrl).parent().attr('academicYrId');
    params.subject_id = ($(ctrl).val()!=0)?$(ctrl).val():'';
console.log(params)

    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/subjwiseClstestStatistics",
        data: params,
        dataType: "html",
        success: function(res) {
//            $('.msgBox-title').html('Exam-wise Statistics');
//            $('.msgBox-body').html(res);
            $('#clssubjwiseChart').html(res);            
classtestSettings.loadClssubjwiseTable (ctrl)
        }
    });
    
};
classtestSettings.loadClssubjwiseTable = function(ctrl) {
    var params = {};
    params.student = $(ctrl).parent().attr('student_id');
    params.divValId = $(ctrl).parent().attr('divValId');
    params.subDivValId = $(ctrl).parent().attr('subDivValId');
    params.academicYrId = $(ctrl).parent().attr('academicYrId');
    params.subject_id = ($(ctrl).val()!=0)?$(ctrl).val():'';
console.log(params)

    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/subjwiseClstestTable",
        data: params,
        dataType: "html",
        success: function(res) {
//            $('.msgBox-title').html('Main Exam List');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
   $('#clssubjwiseTable').html(res);
        }
    });
};