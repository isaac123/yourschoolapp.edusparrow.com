var dashboardSettings = {};
console.log(commonSettings.studentDashboardAjaxurl);
dashboardSettings.loadOverViewChart = function (studentData) {

//    $.ajax({
//        type: "POST",
//        url: commonSettings.studentDashboardAjaxurl + "/overallchart",
//        data: studentData,
//        dataType: "html"
//    }).done(function(res) {
//        $('#overviewChart').html(res);
//    });
};

dashboardSettings.loadAttendanceChart = function (studentData) {
    //console.log(studentData)
    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/attendancechart",
        data: studentData,
        dataType: "html",
        success: function (res) {
//            $('#messageBoxModal').modal('hide');
//            $('.msgBox-title').html('Attendance Statistics');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
            $('#attendanceChart').html(res);
        }
    });
};

dashboardSettings.loadAttendanceTable = function (studentData) {
    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/attendanceTable",
        data: studentData,
        dataType: "html",
        success: function (res) {

//            $('#messageBoxModal').modal('hide');
//            $('.msgBox-title').html('Attendance List');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
            $('#attendanceTable').html(res);
        }
    });
};

dashboardSettings.loadMainexamChart = function (studentData) {
    //console.log(studentData)
    $.ajax({
        type: "POST",
        url: commonSettings.studentDetailsAjaxurl + "/mainExmStudentMarkDets", //"/mainExamStudentMarkDets", //"/mainexamchart",
        data: studentData,
        dataType: "html",
        success: function (res) {
//            $('.msgBox-title').html('Main Exam Statistics');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
//examwiseChart
            $("#other_section").show();
            $('#examwiseChart').html(res);
            // $('#mainexamChart').html(res);
        }
    });
};

dashboardSettings.loadMainexamTable = function (studentData) {
    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/mainexamTable",
        data: studentData,
        dataType: "html",
        success: function (res) {
//            $('.msgBox-title').html('Main Exam List');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
            $('#mainexamTable').html(res);
        }
    });
};

dashboardSettings.loadClassTestChart = function (studentData) {
    //console.log(studentData)
    $.ajax({
        type: "POST",
        url: commonSettings.studentDetailsAjaxurl + "/loadClasstestDet",  //"/classTestStudentMarkDets", //"/classTestChart",
        data: studentData,
        dataType: "html",
        success: function (res) {
//            $('.msgBox-title').html('Class Test Statistics');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
            $("#other_section").show();
            $('#classtestChart').html(res);
        }
    });
};

dashboardSettings.loadsubjRating = function (studentData) {
    //console.log(studentData)
    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/subwiseRating",
        data: studentData,
        dataType: "html",
        success: function (res) {
//            $('.msgBox-title').html('Rating');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
            $('#ratingChart').html(res);
        }
    });
};
dashboardSettings.loadmainCatRating = function (studentData) {
    //console.log(studentData)
    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/mainCatRating",
        data: studentData,
        dataType: "html",
        success: function (res) {
//            $('.msgBox-title').html('Rating');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
            $('#mainCatRatingChart').html(res);
        }
    });
};

dashboardSettings.changePassword = function () {
    var passwordData = {};
    $("#changePassword").find('input').each(function () {
        passwordData[$(this).attr('name')] = $.trim($(this).val());
    });

    //console.log(passwordData)
    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/changePassword",
        data: passwordData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                //console.log(res)
                //console.log(res.type)
                if (res['type'] == 'error') {
                    swal("Error", res['message'], "error");
                    return false;
                }
                else if (res['type'] == 'success') {
                    swal({
                        title: "Done",
                        text: res['message'],
                        type: "success",
                        confirmButtonText: 'Ok',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    $('#oldpassword').val('');
                                    $('#confirmPassword').val('');
                                    $('#password').val('');
                                }
                            });
                }
                /*$('.msgBox-title').html('Message');
                 $('.msgBox-body').html(res['message']);
                 $('#messageBox').click();
                 $('#msg_ok').unbind('click').click(function() {
                 if (res['type'] == 'error') {
                 $('#msg_ok').unbind('click').click();
                 return false;
                 }
                 else if (res['type'] == 'success') {
                 $('#oldpassword').val('');
                 $('#confirmPassword').val('');
                 $('#password').val('');
                 }
                 });*/

            }
        }
    });
};


dashboardSettings.changePasswordStaff = function () {
//alert('test');
    var passwordData = {};
    $("#changePassword").find('input').each(function () {
        passwordData[$(this).attr('name')] = $.trim($(this).val());
    });

    //console.log(passwordData)
    $.ajax({
        type: "POST",
        url: commonSettings.staffDashboardAjaxurl + "/changePassword",
        data: passwordData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                //console.log(res)
                //console.log(res.type)
                if (res['type'] == 'error') {
                    swal("Error", res['message'], "error");
                    return false;
                }
                else if (res['type'] == 'success') {
                    swal({
                        title: "Done",
                        text: res['message'],
                        type: "success",
                        confirmButtonText: 'Ok',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    $('#oldpassword').val('');
                                    $('#confirmPassword').val('');
                                    $('#password').val('');
                                }
                            });
                }
                /*$('.msgBox-title').html('Message');
                 $('.msgBox-body').html(res['message']);
                 $('#messageBox').click();
                 $('#msg_ok').unbind('click').click(function() {
                 if (res['type'] == 'error') {
                 $('#msg_ok').unbind('click').click();
                 return false;
                 }
                 else if (res['type'] == 'success') {
                 $('#oldpassword').val('');
                 $('#confirmPassword').val('');
                 $('#password').val('');
                 }
                 });*/

            }
        }
    });
};


dashboardSettings.updateProfile = function () {
    var studentData = {};
    $("#profileStudentForm").find('input').each(function () {
        studentData[$(this).attr('name')] = $.trim($(this).val());
    });

    var data = new FormData();
    data.append('file1', $("#photo")[0].files[0]);
    data.append('file2', $("#family_photo")[0].files[0]);
    //console.log(data)
    studentData.fileform = data;
    //console.log(studentData)
    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/updateProfile",
        processData: false,
        contentType: false,
        data: studentData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                //console.log(res)
                //console.log(res.type)
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                        return false;
                    }
                    else if (res['type'] == 'success') {
                        $('#oldpassword').val('');
                        $('#confirmPassword').val('');
                        $('#password').val('');
                    }
                });

            }
        }
    });
};

dashboardSettings.assignmentPopup = function (assignment_id) {
//    alert(assignment_id);

    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + '/assignmentPopup',
        data: {
            assignment_id: assignment_id
        },
        success: function (res) {
            $('.formBox-title').html('Upload Assignment');
            $('.formBox-body').html(res);

            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#formModal').modal('show');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                dashboardSettings.storeAssignment(assignment_id);

            });
        }
    });
};

dashboardSettings.storeAssignmentOld = function (assignment_id) {

    var studentData = {};

    var data = new FormData();
    data.append('upload_file_name', $("#assign")[0].files[0]);
    data.append('assignment_id', assignment_id);
    if (!($("#assign")[0].files[0]))
    {
        alert('Please select a file!');
    }
    else
    {
        studentData.fileform = data;
        //console.log($("#assign")[0].files[0]);
        $.ajax({
            type: "POST",
            url: commonSettings.studentDashboardAjaxurl + '/storeAssignment',
            processData: false,
            contentType: false,
            data: data,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        location.reload();
                    });

                }
            }
        });
    }
};

dashboardSettings.storeAssignment = function (assignment_id,stuid) {


    var studentData = {};

    var data = new FormData();
    data.append('upload_file_name', $("#assign_" + assignment_id)[0].files[0]);
    data.append('assignment_id', assignment_id);
    if (!($("#assign_" + assignment_id)[0].files[0]))
    {
        alert('Please select a file!');
    }
    else
    {
        studentData.fileform = data;
        //console.log($("#assign")[0].files[0]);
        $.ajax({
            type: "POST",
            url: commonSettings.assignmentAjaxurl + '/storeAssignment',
            processData: false,
            contentType: false,
            data: data,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        studentprofileSettings.loadAssignments(stuid)
                    });

                }
            }
        });
    }

}