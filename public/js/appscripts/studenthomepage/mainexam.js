var mainexamSettings = {};
mainexamSettings.examwise = function(ctrl) {
    var params = {};
    params.student = $(ctrl).parent().attr('student_id');
    params.divValId = $(ctrl).parent().attr('divValId');
    params.subDivValId = $(ctrl).parent().attr('subDivValId');
    params.academicYrId = $(ctrl).parent().attr('academicYrId');
    params.mainexamName = ($(ctrl).val()!=0)?$(ctrl).val():'';
console.log(params)

    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/examwiseStatistics",
        data: params,
        dataType: "html",
        success: function(res) {
//            $('.msgBox-title').html('Exam-wise Statistics');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
            $('#examwiseChart').html(res);
mainexamSettings.loadMainexamwiseTable(ctrl)
        }
    });
    
};


mainexamSettings.loadMainexamwiseTable = function(ctrl) {
    var params = {};
    params.student = $(ctrl).parent().attr('student_id');
    params.divValId = $(ctrl).parent().attr('divValId');
    params.subDivValId = $(ctrl).parent().attr('subDivValId');
    params.academicYrId = $(ctrl).parent().attr('academicYrId');
    params.mainexamName = ($(ctrl).val()!=0)?$(ctrl).val():'';
console.log(params)

    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/mainexamwiseTable",
        data: params,
        dataType: "html",
        success: function(res) {
//            $('.msgBox-title').html('Main Exam List');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
   $('#mainExamWiseTable').html(res);
        }
    });
};

mainexamSettings.subjwise = function(ctrl) {
  var params = {};
    params.student = $(ctrl).parent().attr('student_id');
    params.divValId = $(ctrl).parent().attr('divValId');
    params.subDivValId = $(ctrl).parent().attr('subDivValId');
    params.academicYrId = $(ctrl).parent().attr('academicYrId');
    params.subject_id = ($(ctrl).val()!=0)?$(ctrl).val():'';
console.log(params)


    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/subjwiseStatistics",
        data: params,
        dataType: "html",
        success: function(res) {
//            $('.msgBox-title').html('Subject-wise Statistics');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
//console.log($('#subjwiseChart').html())
            $('#subjwiseChart').html(res);
            mainexamSettings.loadmainsubjwiseTable(ctrl)
            
        }
    });
    
};


mainexamSettings.loadmainsubjwiseTable = function(ctrl) {
    var params = {};
    params.student = $(ctrl).parent().attr('student_id');
    params.divValId = $(ctrl).parent().attr('divValId');
    params.subDivValId = $(ctrl).parent().attr('subDivValId');
    params.academicYrId = $(ctrl).parent().attr('academicYrId');
    params.subject_id = ($(ctrl).val()!=0)?$(ctrl).val():'';
console.log(params)

    $.ajax({
        type: "POST",
        url: commonSettings.studentDashboardAjaxurl + "/mainsubjwiseTable",
        data: params,
        dataType: "html",
        success: function(res) {
//            $('.msgBox-title').html('Main Exam List');
//            $('.msgBox-body').html(res);
//            $('#messageBoxModal').modal('show');
   $('#subjwiseTable').html(res);
        }
    });
};
