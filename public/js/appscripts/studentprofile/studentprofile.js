studentprofileSettings = {};
studentprofileSettings.stuparams = {};
studentprofileSettings.searchVal = {};

studentprofileSettings.loadSection = function (sval) {
    //console.log(sval);
    if (sval != 0)
    {

        $.ajax({
            type: "POST",
            url: commonSettings.studentprofileAjaxurl + "/loadSection",
            data: {
                classId: sval
            },
            success: function (res) {
                //alert(res);

                if (res) {
                    $('#loadSection').html(res);
                    $('#loadSection').removeClass('hide');

                }

            }
        });
    }
    else {
        $('#loadSection').addClass('hide');

        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Please Select Class.</div>');
        $('#messageBox').click();
    }

};

studentprofileSettings.loadStudentdet = function (savestate)
{
    var studata = {};
    var subdivval = [];
    var params = {};
    var academic = "";
    var i = 0;
    studata.emptySearch = 1;
    //studentProfileSearchForm

    $("#studentProfileSearchForm").find('input,select,textarea').each(function () {

        if ($(this).attr('name') == 'subDivVal')
        {
            if ($(this).val() != 0)
            {
                //i++;
                subdivval.push($(this).val());
            }
        }
        else
        {
            studata.emptySearch = 0;
            studata[$(this).attr('name')] = $.trim($(this).val());
        }

    });
    studata['subDivVal'] = subdivval;

    console.log('i value=' + i);
    console.log(subdivval);
    console.log(studata);
    params = studata;
    params.savestate = savestate;
    console.log(params);
    /*if(i !=0)
     {*/
    $.ajax({
        type: "POST",
        url: commonSettings.profileAjaxurl + "/loadtableHeader",
        data: studata,
        dataType: "html",
        success: function (res) {
            $('#loadReport').html(res);
            $('#loadReport').removeClass('hide');
            var tablectrl = '#studetails';
            var column = [0, 1, 2, 3, 4, 5, 6];
            var sortingDisabled = [7];

            $(tablectrl).dataTable({
                "sDom": '<"top"if>rt<"bottopm"p><"clear">',
                "bProcessing": true,
                "bServerSide": true,
                stateSave: true,
                "sAjaxSource": commonSettings.profileAjaxurl + '/loadtableData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": commonSettings.NumberOfRecords,
                // 'iDisplayStart':patientsapp.startIndex,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[1, "desc"]],
                "bLengthChange": false,
//                "bInfo": false,
                "fnStateLoadParams": function (oSettings, oData) {
                    // Disallow state loading by returning false  
                    if (params.savestate == 0)
                    {
                        var loadStateParams = false;
                        params.savestate = 1;
                    }
                    return loadStateParams;
                },
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function (o) {
                    if (o._iRecordsDisplay <= commonSettings.NumberOfRecords) {
                        $(tablectrl + '_paginate').hide();
                    }
                    $('.dataTables_filter').addClass('col-md-5');
                    $('.dataTables_filter').find('label').addClass('col-md-3 search-txt1').css('display', ' inline');
                    $('.dataTables_filter').find('input').addClass('form-control pull-right search-tbox1');
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    var rowText = '<tr id="' + aData.id + '">';
                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.Admission_no + '</span>';
                    rowText += '</td>';
                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.Date_of_Birth + '</span>';
                    rowText += '</td>';
                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.Student_Name + '</span>';
                    rowText += '</td>';
                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.gender + '</span>';
                    rowText += '</td>';
                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.stulogin + '</span>';
                    rowText += '</td>';
                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.parntlogin + '</span>';
                    rowText += '</td>';
                    rowText += '<td class="">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore label label-info label-mini">' + aData.status + '</span>';
                    rowText += '</td>';
                    rowText += '<td class="new-patient invoiceview sorting_1">';
                    rowText += aData.Actions;
                    rowText += '</td>';
//                    rowText += '<td class=" ">';
//                    rowText += aData.view;
//                    rowText += '</td>';
                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });
        }
    });
    /* }
     else
     {
     $('.modal-title').html('Message');
     $('.modal-body').html('<div class="alert alert-block alert-danger fade in">You Should Fill Atleast One Field.</div>');
     $('#messageBox').click();
     }*/
};
studentprofileSettings.loadacademicprofile = function (stuid) {
    $.ajax({
        type: "GET",
        url: commonSettings.studentProfAjaxurl + "?studentId=" + stuid,
        dataType: "html",
        success: function (res) {
            $('#profilesection').html(res);
        }
    });
};
studentprofileSettings.loadeditprofile = function (stuid) {
    $.ajax({
        type: "GET",
        url: commonSettings.profileAjaxurl + "/editStudentProfile?student_id=" + stuid,
        dataType: "html",
        success: function (res) {
            $('#profilesection').html(res);
        }
    });
};


studentprofileSettings.loadAssignments = function (stuid)
{

    var subjectid = $('#assignment_subject').val();
    var assignmentdate = $('#assignmentdate').val();
    console.log(subjectid);

    $.ajax({
        type: "POST",
        url: commonSettings.studentProfAjaxurl + "/loadAssignment",
        data: {subjectid: subjectid,
            assignmentdate: assignmentdate,
            studentId: stuid},
        dataType: "html",
        success: function (res) {
            $('#assignment_content').html(res);
        }
    });
};

studentprofileSettings.loadClasTest = function ()
{
    var searchtest = $('#classtestids option:selected').val() ? $('#classtestids option:selected').val() : '';
    var searchsubject = $('#subjectid :selected').val() ? $('#subjectid :selected').val() : '';
//alert(searchtest);
    studentprofileSettings.stuparams['searchtest'] = searchtest;
    studentprofileSettings.stuparams['searchsubject'] = searchsubject;

    console.log(studentprofileSettings.stuparams);

    $.ajax({
        type: "POST",
        url: commonSettings.studentDetailsAjaxurl + "/loadClasTestBySubjAndClsTst",
        data: studentprofileSettings.stuparams,
        dataType: "html",
        success: function (res) {
            //console.log(res);
            $('#stuClstestTabl').html(res);
//            $('#homeworkdiv').removeClass('hide')
        }
    });
};


studentprofileSettings.loadMainExam = function ()
{
    var searchtest = $('#mainexmids option:selected').val() ? $('#mainexmids option:selected').val() : '';
    var searchsubject = $('#subjectid :selected').val() ? $('#subjectid :selected').val() : '';
//alert(searchtest);
    studentprofileSettings.stuparams['searchtest'] = searchtest;
    studentprofileSettings.stuparams['searchsubject'] = searchsubject;

    console.log(studentprofileSettings.stuparams);

    $.ajax({
        type: "POST",
        url: commonSettings.studentDetailsAjaxurl + "/loaMainExmBySubjAndClsTst",
        data: studentprofileSettings.stuparams,
        dataType: "html",
        success: function (res) {
            //console.log(res);
            $('#stuMaintestTabl').html(res);
//            $('#homeworkdiv').removeClass('hide')
        }
    });
};



studentprofileSettings.loadSubtree = function (ctrl) {
    console.log($(ctrl).parent().parent().next('.subtreediv').html())
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.profileAjaxurl + "/loadSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};