var searchSettings = {};
searchSettings.stuSearch = function(ctrl) {
    var stuSearchData = {};
    $("#searchStudentSingle").find('input').each(function() {
        stuSearchData[$(this).attr('name')] = $.trim($(this).val());
    });
    var callBackFn= stuSearchData['callBackFn'];
    //        console.log(resHtml).log(stuSearchData);
    $.ajax({
        type: "POST",
        url: commonSettings.searchAjaxurl+"/studentAutoCmpltSearch",
        data: stuSearchData,
        success: function(res) {
            if (res) {
               eval(callBackFn)(res) 
            }

        }
    });
};

searchSettings.loadClassByAcademicYr = function(acvalue) {
    if (acvalue != 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.searchAjaxurl+"/loadClassByAcademicYear",
            data: {
                academicYr: acvalue
            },
            success: function(res) {
                if (res) {
                    ////        console.log(resHtml).log(res)                    
                    $('#StudentClass').html(res);
                    $('#StudentClass').removeClass('hide')
                }

            }
        });
    } else {

        $('.modal-title').html('Message');
        $('.modal-body').html("Select valid Academic Year");
        $('#messageBox').click();
        return false;
    }
};

searchSettings.loadSectionsByClass= function(clsvalue) {
    if (clsvalue != 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.searchAjaxurl+"/loadSectionsByClass",
            data: {
                classId: clsvalue,
                academicYr: $('#academicYrId').val()
            },
            success: function(res) {
                if (res) {
                    ////        console.log(resHtml).log(res)                    
                    $('#StudentSection').html(res);
                    $('#StudentSection').removeClass('hide')
                }

            }
        });
    } else {
        $('.modal-title').html('Message');
        $('.modal-body').html("Select valid Class");
        $('#messageBox').click();
        return false;
    }
};

searchSettings.stuGrpSearch = function(ctrl) {
    var stuSearchData = {};
    $("#studentGroupSearchForm").find('input,select').each(function() {
        stuSearchData[$(this).attr('name')] = $.trim($(this).val());
    });
    var callBackFn= stuSearchData['callBackFn'];
    //        console.log(resHtml).log(eval(callBackFn))
    //        console.log(resHtml).log(stuSearchData);
    $.ajax({
        type: "POST",
        url: commonSettings.searchAjaxurl+"/studentGrpSearch",
        data: stuSearchData,
        success: function(res) {
            if (res) {
               eval(callBackFn)(res) 
           }

        }
    });
};

searchSettings.stfSearch= function(ctrl) {
    var stfSearchData = {};
    $("#searchStaffSingle").find('input').each(function() {
        stfSearchData[$(this).attr('name')] = $.trim($(this).val());
    });
    var callBackFn= stfSearchData['callBackFn'];
    //        console.log(resHtml).log(stuSearchData);
    $.ajax({
        type: "POST",
        url: commonSettings.searchAjaxurl+"/staffAutoCmpltSearch",
        data: stfSearchData,
        success: function(res) {
            if (res) {
               eval(callBackFn)(res) 
            }

        }
    });
};

searchSettings.loadDeptByAcademicYr= function(acvalue) {
    if (acvalue != 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.searchAjaxurl+"/loadDeptByAcademicYr",
            data: {
                academicYr: acvalue
            },
            success: function(res) {
                if (res) {
                    ////        console.log(resHtml).log(res)                    
                    $('#StaffDept').html(res);
                    $('#StaffDept').removeClass('hide')
                }

            }
        });
    } else {

        $('.modal-title').html('Message');
        $('.modal-body').html("Select valid Academic Year");
        $('#messageBox').click();
        return false;
    }
};

searchSettings.stfGrpSearch= function(ctrl) {
    var stfSearchData = {};
    $("#staffGroupSearchForm").find('input,select').each(function() {
        stfSearchData[$(this).attr('name')] = $.trim($(this).val());
    });
    var callBackFn= stfSearchData['callBackFn'];
    //        console.log(resHtml).log(eval(callBackFn))
     //  console.log(stfSearchData);
    $.ajax({
        type: "POST",
        url: commonSettings.searchAjaxurl+"/staffGrpSearch",
        data: stfSearchData,
        success: function(res) {
            if (res) {
               eval(callBackFn)(res) 
           }

        }
    });
};