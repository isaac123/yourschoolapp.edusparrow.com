var timelineSettings = {};
timelineSettings.loadTasks = function (masterid, typ) {
    $('#taskdiv').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    var type = $('#type').val();
    timelineSettings.type = type;
    $.ajax({
        type: "POST",
        url: commonSettings.timelineAjaxUrl + "/loadTasks",
        data: {
            masterid: masterid,
            typ: typ
        },
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('#taskdiv').html(res);
                //timelineSettings.loadTimeline(masterid);
            }
        }
    });
};
timelineSettings.iconfn = function (ctrl) {

    $(ctrl).toggleClass('fa-chevron-down');
    //   $(ctrl).addClass('fa-chevron-up').removeClass('fa-chevron-down')

}
timelineSettings.loadTimeline = function (masterid) {
    $.ajax({
        type: "POST",
        url: commonSettings.timelineAjaxUrl + "/loadTimeline",
        data: {
            masterid: masterid
        },
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('#timeline').html(res);

            }
        }
    });
};

timelineSettings.addnewTasks = function (masterid)
{
    $.ajax({
        type: "POST",
        url: commonSettings.timelineAjaxUrl + "/addnewTasks",
        data: {
            masterid: masterid
        },
        dataType: "html",
        success: function (res) {
            $('#formModal .formBox-title').html('Add New Activities');
            $('#formModal .formBox-body').html(res);
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#confirm_save').removeClass('hide');
            $('#confirm_save').html('Save');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                timelineSettings.saveDetAll();
            });

        }
    });

};
timelineSettings.loadActivityType = function (ctrl) {
    var commentsData = {};
    $("#addNewActivityForm").find('input').each(function () {
        commentsData[$(this).attr('name')] = $.trim($(this).val());
    });
    if (ctrl == '1') {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/loadAssignmentTasks",
            data: {commentsData: commentsData, activity_id: ctrl},
            dataType: 'html',
            success: function (res) {
                if (res) {
                    $('#assignment').html(res);
                    $('#assignment').removeClass('hide');
                    $('#homework').addClass('hide');
                    $('#classtest').addClass('hide');
                    $('#comments').addClass('hide');
                }
            }
        });
    }
    else if (ctrl == '2') {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/loadClassTestTasks",
            data: {commentsData: commentsData, activity_id: ctrl},
            dataType: 'html',
            success: function (res) {
                if (res) {
                    $('#classtest').html(res);
                    $('#classtest').removeClass('hide');
                    $('#assignment').addClass('hide');
                    $('#homework').addClass('hide');
                    $('#comments').addClass('hide');
                }
            }
        });

    }
    else if (ctrl == '3') {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/loadHomeworkTasks",
            data: {commentsData: commentsData, activity_id: ctrl},
            dataType: 'html',
            success: function (res) {
                if (res) {
                    $('#homework').html(res);
                    $('#homework').removeClass('hide');
                    $('#assignment').addClass('hide');
                    $('#classtest').addClass('hide');
                    $('#comments').addClass('hide');
                }
            }
        });

    }
    else if (ctrl == '4') {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/loadComments",
            dataType: 'html',
            success: function (res) {
                if (res) {
                    $('#comments').html(res);
                    $('#comments').removeClass('hide');
                    $('#assignment').addClass('hide');
                    $('#classtest').addClass('hide');
                    $('#homework').addClass('hide');
                }
            }
        });

    }
};
timelineSettings.newAssignment = function () {
    var AssignmentData = {};
    var assdate = 0;
    var asstopic = 0;
    var valid = 0;
    var errmsg = '';
    var AssignmentData = new FormData();
    $("#addAssignmentTaskForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                valid = 1;
                console.log($(this))
                errmsg += $(this).attr('title') + " is required!<br>";

            } else {
                AssignmentData.append($(this).attr('name'), $.trim($(this).val()));
            }
        } else {
            if ($(this).attr('name') !== 'files')
            {
                if ($(this).is(':checkbox'))
                {
                    if ($(this).is(':checked') == true) {
                        AssignmentData.append($(this).attr('name'), 'on');
                    }
                    else {
                        AssignmentData.append($(this).attr('name'), 'off');
                    }
                }
                else {
                    AssignmentData.append($(this).attr('name'), $.trim($(this).val()));
                }

            }
            else
            {
                AssignmentData.append('files', $("#files")[0].files[0]);

            }
        }
    });
    console.log(AssignmentData);

    if (valid == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/saveAssignment",
            processData: false,
            contentType: false,
            data: AssignmentData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            // $('#formModal').modal();

                        }
                        else if (res['type'] == 'success') {
                            window.close();
                            window.opener.reloadParent();
                        }
                    });

                }

            }
        });
    }
    else
    {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
        $('#messageBox').click();
        valid = 0;
    }
};
timelineSettings.saveDetAll = function () {
    var commentsData = {};
    $("#addNewActivityForm").find('input,select').each(function () {
        commentsData[$(this).attr('name')] = $.trim($(this).val());
    });
    console.log(commentsData);
    if (commentsData['activity_type'] == '1') {
        timelineSettings.newAssignment();
    }
    else if (commentsData['activity_type'] == '2') {
        timelineSettings.newClassTest();
    }
    else if (commentsData['activity_type'] == '3') {
        timelineSettings.newHomeWork();
    }

};
timelineSettings.newHomeWork = function () {
    var AssignmentData = {};
    var assdate = 0;
    var asstopic = 0;
    var valid = 0;
    var errmsg = '';
    $("#addHomeworkForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                valid = 1;
                errmsg += $(this).attr('title') + " is required!<br>";

            } else {
                AssignmentData[$(this).attr('name')] = $.trim($(this).val());
            }
        } else {
            if ($(this).is(':checkbox'))
            {
                if ($(this).is(':checked') == true)
                    AssignmentData[$(this).attr('name')] = 'on';
                else
                    AssignmentData[$(this).attr('name')] = 'off';
            }
            AssignmentData[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    console.log(AssignmentData);

    if (valid == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/newHomeWork",
            data: AssignmentData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                        }
                        else if (res['type'] == 'success') {
                            window.close();
                            window.opener.reloadParent();
                        }
                    });

                }

            }
        });
    }
    else
    {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
        $('#messageBox').click();
        valid = 0;
    }
};
timelineSettings.updateDetails = function (masid, assid) {
    timelineSettings.master_id = masid;
    timelineSettings.selectedAssignment = assid;
    if (arr[0] == 'assignment') {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/updateassignmentDetails",
            data: {
                test_id: assid, master_id: masid
            },
            dataType: "html",
            success: function (res) {
                $('#formModal .formBox-title').html('Update Marks');
                $('#formModal .formBox-body').html(res);
                $('#formModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#confirm_save').removeClass('hide');
                $('#confirm_save').html('Save');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                });

            }
        });
    }
    if (arr[0] == 'mainexam') {

        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/getStudentMainExamMarkList",
            data: {
                exam_id: arr[2], master_id: arr[1]
            },
            dataType: "html",
            success: function (res) {
                $('#formModal .formBox-title').html('Update Marks');
                $('#formModal .formBox-body').html(res);
                $('#formModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#confirm_save').removeClass('hide');
                $('#confirm_save').html('Save');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                });

            }
        });
    }
    if (arr[0] == 'classtest') {

        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/loadClassTestStudents",
            data: {
                test_id: arr[2], master_id: arr[1]
            },
            dataType: "html",
            success: function (res) {
                $('#formModal .formBox-title').html('Update Marks');
                $('#formModal .formBox-body').html(res);
                $('#formModal').css('width', '800px;');
                $('#formModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#confirm_save').removeClass('hide');
                $('#confirm_save').html('Save');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                });

            }
        });
    }
};
timelineSettings.updateAssignmentTotalMark = function (mark, assid)
{
    $('.done_totmark').addClass('hide')
    $('.loading_totmark').removeClass('hide')
    var total_mark = parseInt(mark);
    var number_pattren = /^[0-9.]*$/;
    var err = 0, errormsg = '';
    if (!number_pattren.test(mark))
    {
        err = 1;
        errormsg = 'Please enter valid numeric only!';
    }
    else if (total_mark < 0) {
        err = 1;
        errormsg = 'Total Mark Cannot Be Negative!';
    }
    $('#assigMrk').find('input.stuMark').each(function () {
        var markstu = $.trim($(this).val());
//        console.log(markstu)
        if (markstu !== '' && parseFloat(markstu).toFixed(2) > total_mark) {
            err = 1;
            errormsg = 'Student mark exceeds the total mark!';
            return;
        }
    });

    if (err == 1)
    {
        $('#Total_Mark').val($('#Total_Mark').attr('placeholder'))
        $('#messageBoxModal .modal-title').html('Error');
        $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-danger fade in">' + errormsg + '</div>');
        $('#messageBoxModal').modal('show');
        $('.loading_totmark').addClass('hide')
    } else {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/updateAssTestTotMark",
            data: {
                test_id: assid,
                total_mark: total_mark
            },
            success: function (res) {
                if (res) {

                    $('.done_totmark').removeClass('hide')
                    $('.loading_totmark').addClass('hide')
                }

            }
        });
    }
};
timelineSettings.updateAssignmentMark = function (mark, studid, assid)
{
    var total_mark = parseInt($('#Total_Mark').val());
    var number_pattren = /^[0-9.]*$/;

    if (!number_pattren.test(mark))
    {
        swal('Error', 'Please enter valid numeric only', 'error');
    }
    else if ($('#Total_Mark').val().length == 0) {
        swal('Error', 'Please Enter Total Mark First!', 'error');
    }
    else if (total_mark < 0) {
        swal('Error', 'Total Mark Cannot Be Negative!', 'error');
    }
    else if (mark < 0) {
        swal('Error', 'Entered Mark Cannot Be Negative!', 'error');
    }
    else if (mark <= total_mark)
    {
        $('#Total_Mark').attr("disabled", "disabled");
        $('.done_' + studid).addClass('hide')
        $('.loading_' + studid).removeClass('hide')
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/updateAssignmentMark",
            data: {
                test_id: assid,
                student_id: studid,
                mark: mark,
                total_mark: total_mark
            },
            dataType: "JSON",
            success: function (res) {
                console.log(res);
                if (res.type == 'success') {
                    $('.done_' + studid).removeClass('hide');
                    $('.loading_' + studid).addClass('hide');

                } else {
                    swal('Error', res.message, 'error');
                }

            }
        });
        $('#div_' + studid).html("Mark updated");
        $('#div_' + studid).css("color", "green")
    }
    else
    {
        $('#div_' + studid).html("Exceeds Total Mark. Re-Enter correct mark");
        $('#div_' + studid).css("color", "red")
    }
};
timelineSettings.updateFormula = function (masterid, examid, subjectid) {
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/loadFormulaForm",
        dataType: "html",
        data:
                {
                    examid: examid,
                    subjectid: subjectid,
                    masterid: masterid
                },
        success: function (res) {
            if (res) {
                $('.modal-title').html('Formula');
                $('.modal-body').html(res);
                $('#formModal').modal('show');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    timelineSettings.saveFormula(masterid, examid, subjectid);
                });
            }
        }
    });
};
timelineSettings.saveFormula = function (masterid, examid, subjectid) {
    var formulaData = {};
    var err = 0;
    var errmsg = '';
    $('#addFormulaForm').find('input,select,textarea').each(function () {
        formulaData[$(this).attr('name')] = $.trim($(this).val());

    });
    formulaData['master_id'] = masterid;
    formulaData['exam_id'] = examid;
    formulaData['subject_id'] = subjectid;
    if (formulaData['select_formula'] === 'Bestof') {

        formulaData['formula'] = formulaData['select_formula'] + '-' + formulaData['bestofval'] + '-' + formulaData['select_formula_forbestof'];

        if (formulaData['bestofval'] === '')
        {
            err = 1;
            errmsg = errmsg + 'Please enter best of val. <br>';
        }

        if (formulaData['select_formula_forbestof'] === '')
        {
            err = 1;
            errmsg = errmsg + 'Please select best of formula. <br>';
        }

    } else {
        formulaData['formula'] = formulaData['select_formula'];
    }

    formulaData['exam_ids'] = formulaData['my_multi_select2[]'];

    console.log(formulaData);

    if (formulaData['exam_ids'] === '')
    {
        err = 1;
        errmsg = errmsg + 'Please select exams,classtest & assignments. <br>';
    }

    if (formulaData['select_formula'] === '')
    {
        err = 1;
        errmsg = errmsg + 'Please select formula. <br>';
    }

    if (formulaData['contribute_outof'] === '')
    {
        err = 1;
        errmsg = errmsg + 'Please enter outof val. <br>';
    }
    if (err === 1) {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#formModal').modal('show')
        });
    } else {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/addFormula",
            data: formulaData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    var lblcls = res.type == 'error' ? 'danger' : 'success';
                    $('#messageBoxModal .modal-title').html(res.type);
                    $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == "success") {
                            $.ajax({
                                type: "POST",
                                url: commonSettings.timelineAjaxUrl + "/getStudentList",
                                data: {
                                    exam_id: examid,
                                    master_id: masterid,
                                    subject_id: subjectid
                                },
                            }).done(function (resHtml) {
                                $("#mymarkDiv").html(resHtml);
                                // markSettings.stuCombiData['MainExTotalMark'] = $('#mainexTotVal').val();
                            });
                        } else {
                            $('#formModal').modal('show')
                        }

                    });
                }
            }
        });
    }
};
timelineSettings.addnewActivity = function (masterid)
{
    $('#formModal .formBox-title').html('Add New Activities');
    $('#formModal .formBox-body').html($('#addactivity').html());
    $('#formModal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#confirm_save').removeClass('hide');
    $('#confirm_save').html('Save');
    $('#confirm_save').unbind('click').click(function () {
        $('#confirm_close').click();
    });

};
timelineSettings.addnewAssignment = function (masterid) {
    var url = commonSettings.timelineAjaxUrl + "/loadAssignmentTasks?masterid=" + masterid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.loadContentSharing = function (masterid) {
    var url = commonSettings.timelineAjaxUrl + "/loadContentSharing?masterid=" + masterid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.editAssignment = function (assignmentid) {
    var url = commonSettings.timelineAjaxUrl + "/viewEditAssignment?assignmentid=" + assignmentid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.validateAssignment = function (assid, masid) {
    var url = commonSettings.timelineAjaxUrl + "/updateassignmentDetails?master_id=" + masid + "&test_id=" + assid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.editComments = function (actid, typ) {
    $('#' + typ + '-comm-' + actid).removeClass('hide');
    $('#comment-' + typ + '-' + actid).focus()
}
timelineSettings.cancelComments = function (actid, typ) {
    $('#' + typ + '-comm-' + actid).addClass('hide');
    $('#comment-' + typ + '-' + actid).val('')
};
timelineSettings.savComments = function (ctrl) {
    var commobj = {};
    var commentctrl = $(ctrl).parents('.card-block').find('textarea');
    commobj.comment = commentctrl.val()
    commobj.activitytyp = commentctrl.attr('activity_type')
    commobj.activity_id = commentctrl.attr('activity_id')
    commobj.master_id = commentctrl.attr('master_id')
    commobj.display_time = commentctrl.attr('display_time')
    console.log(commobj)
    if (commobj.comment) {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/savenewComments",
            data: commobj,
            dataType: "json",
            success: function (res) {
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function () {
                    timelineSettings.loadTasks(commobj.master_id);
                });
            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('enter commments to add');
        $('#messageBox').click();
        return false;
    }
};
timelineSettings.addnewHomework = function (masterid) {
    var url = commonSettings.timelineAjaxUrl + "/loadHomeworkTasks?masterid=" + masterid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.editHomework = function (homeworkid) {
    var url = commonSettings.timelineAjaxUrl + "/editHomework?homeworkid=" + homeworkid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.deleteHomework = function (homeworkid, masterid, hmwrk) {
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html(' Do you confirm to delete this ' + hmwrk + ' Details?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/deleteHomework",
            data: {homeworkid: homeworkid, masterid: masterid},
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                        }
                        else if (res['type'] == 'success') {
                            timelineSettings.loadTasks(masterid);
                        }
                    });

                }
            }
        });
    });
};
timelineSettings.openEditor = function (combinationid, docname, planid) {
    var url = commonSettings.videosAjaxUrl + "/lessonPlan?planid=" + planid + "&docname=" + docname + "&combiid=" + combinationid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.downloadFile = function (planid) {
    $('#downloadLessonPlan').remove()
    var newForm = $('<form>', {
        'action': commonSettings.timelineAjaxUrl + "/downloadLessonPlan",
        'method': 'POST',
        'target': '_blank',
        'id': 'downloadLessonPlan'
    }).append($('<input>', {
        'name': 'planid',
        'value': planid,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};
timelineSettings.deleteFile = function (planid) {
    $.ajax({
        type: "POST",
        url: commonSettings.timelineAjaxUrl + "/deleteFile",
        data: {planid: planid},
        dataType: "json",
        success: function (res) {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html(res['message']);
            $('#messageBox').click();
            $('#msg_ok').unbind('click').click(function () {
                timelineSettings.loadTasks();
            });
        }
    });
};
timelineSettings.uploadDocuments = function (masterid) {
    var url = commonSettings.timelineAjaxUrl + "/uploadDocuments?masterid=" + masterid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.saveDocuments = function () {
    var Data = {};
    var valid = 0;
    var errmsg = '';
    var Data = new FormData();
    $("#uploadFileForm").find('input,select,textarea,text').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                valid = 1;
                console.log($(this))
                errmsg += $(this).attr('title') + " is required!<br>";

            } else {
                if ($(this).attr('name') == 'files')
                {
                    Data.append('files', $("#files")[0].files[0]);

                }
                else
                {
                    Data.append($(this).attr('name'), $.trim($(this).val()));
                }
            }
        } else {

            Data.append($(this).attr('name'), $.trim($(this).val()));
        }
    });
    console.log(Data);
    if (valid == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/saveDocuments",
            processData: false,
            contentType: false,
            data: Data,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                        }
                        else if (res['type'] == 'success') {
                            window.close();
                            window.opener.reloadParent();
                        }
                    });

                }

            }
        });
    }
    else
    {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
        $('#messageBox').click();
        valid = 0;
    }
};
timelineSettings.editReadings = function (masterid, readingsid) {
    var url = commonSettings.timelineAjaxUrl + "/editReadings?masterid=" + masterid + "&readingsid=" + readingsid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.deleteReadings = function (masterid, readingsid, name) {
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html(' Do you confirm to delete this ' + name + ' file?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/deleteReadings",
            data: {readingsid: readingsid, masterid: masterid},
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                        }
                        else if (res['type'] == 'success') {
                            timelineSettings.loadTasks(masterid);
                        }
                    });

                }
            }
        });
    });
};
timelineSettings.downloadReadingsFile = function (readingsid) {
    console.log(readingsid);
    var newForm = $('<form>', {
        'action': commonSettings.timelineAjaxUrl + "/downloadReadingsFile",
        'method': 'POST',
        'target': '_blank',
        'id': 'downloadReadingsFile'
    }).append($('<input>', {
        'name': 'readingsid',
        'value': readingsid,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};
timelineSettings.loadContentSharing = function (masterid) {
    var url = commonSettings.timelineAjaxUrl + "/loadContentSharing?masterid=" + masterid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.downloadContentFile = function (contentid) {
    var newForm = $('<form>', {
        'action': commonSettings.timelineAjaxUrl + "/downloadContentFile",
        'method': 'POST',
        'target': '_blank',
        'id': 'downloadReadingsFile'
    }).append($('<input>', {
        'name': 'contentid',
        'value': contentid,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};
timelineSettings.deleteContentFile = function (masterid, contentid, name) {
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html(' Do you confirm to delete this ' + name + ' file?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/deleteContentFile",
            data: {contentid: contentid, masterid: masterid},
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                        }
                        else if (res['type'] == 'success') {
                            timelineSettings.loadTasks(masterid);
                        }
                    });

                }
            }
        });
    });
};
timelineSettings.loadMainExam = function (masterid) {
    var url = commonSettings.timelineAjaxUrl + "/loadMainExam?masterid=" + masterid + "&type=" + "subjectTeacher"
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.loadStudentMarkList = function (examid, masterid) {
    $.ajax({
        type: "POST",
        url: commonSettings.timelineAjaxUrl + "/loadStudentMarkList",
        data: {masterid: masterid, mainExId: examid},
        dataType: 'html'
    }).done(function (resHtml) {
        $("#marklist").removeClass('hide');
        $("#marklist").html(resHtml);
        $("#examlist").addClass('hide');
        $('#mymarkDiv').toggle(attSettings.effect, attSettings.options, attSettings.duration);
        $('#mymarkDiv1').toggle(attSettings.effect, attSettings.options, attSettings.duration);
        var max = 100;
        markSettings.updateHeight(max, 'mainmrkStuTable', 'getExamMArkList');
    });
};
timelineSettings.enterStudentMark = function (subid, masid, examid) {
    $.ajax({
        type: "POST",
        url: commonSettings.timelineAjaxUrl + "/getStudentList",
        data: {master_id: masid, exam_id: examid, subject_id: subid},
        dataType: 'html'
    }).done(function (resHtml) {
        $("#mymarkDiv").html(resHtml);
        $('#mymarkDiv').toggle(attSettings.effect, attSettings.options, attSettings.duration);
        $('#mymarkDiv1').toggle(attSettings.effect, attSettings.options, attSettings.duration);
        var max = 100;
        markSettings.updateHeight(max, 'mainmrkStuTable', 'markStuTable')
    });
};
timelineSettings.updateMainExamMarkTotal = function (ctrl, masid, examid, subid) {
    var mark = $(ctrl).val();
    var number_pattren = /^[0-9.]*$/;
    if (!number_pattren.test(mark))
    {
        swal({
            title: 'Error',
            text: 'Please enter valid numeric only.',
            type: 'error',
            confirmButtonText: 'OK!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        timelineSettings.enterStudentMark(subid, masid, examid);
                    }
                });
    }
    else if (mark == '' || parseFloat(mark).toFixed(2) < 0) {
        swal({
            title: 'Error',
            text: 'Invalid Total Marks',
            type: 'error',
            confirmButtonText: 'OK!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        timelineSettings.enterStudentMark(subid, masid, examid);
                    }
                });
    } else {
        $('.loading_' + examid).removeClass('hide')
        $('.enable_' + examid).prop('disabled', false);
        var MainExTotalMark = parseFloat(mark).toFixed(2);
        timelineSettings.updateAlStuMainExamMark(masid, examid, subid, MainExTotalMark)
    }
};
timelineSettings.updateAlStuMainExamMark = function (masid, examid, subid, MainExTotalMark) {
    var err = 0;
    var params = {};
    $('#markStuTable').find('input.stuMark').each(function () {
        var markstu = $.trim($(this).val());
        if (markstu != '' && parseFloat(markstu) > parseFloat(MainExTotalMark)) {
            err = 1;
            swal({
                title: 'Error',
                text: 'Student mark exceeds the total mark!',
                type: 'error',
                confirmButtonText: 'OK',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            timelineSettings.enterStudentMark(subid, masid, examid);
                        }
                    });
        }
    });
    if (err == 0) {
        params['totalmark'] = MainExTotalMark;
        params['master_id'] = masid;
        params['exam_id'] = examid;
        params['subject_id'] = subid;
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/updateAlStudentMark",
            data: params,
            dataType: 'JSON',
        }).done(function (resHtml) {
            if (resHtml['type'] != 'error') {
                $('.done_' + examid).removeClass('hide');
                $('.loading_' + examid).addClass('hide');
            } else {

                swal({
                    title: resHtml['type'],
                    text: resHtml.message,
                    type: resHtml['type'],
                    confirmButtonText: 'OK!',
                    closeOnConfirm: true,
                },
                        function (isConfirm) {
                            if (isConfirm) {

                                if (resHtml['type'] == 'error')
                                    timelineSettings.enterStudentMark(subid, masid, examid);

                            }
                        });
            }
        });

    }
};
timelineSettings.updateStuMainExamMark = function (ctrl, masterid, mainexamid, subid) {
    var params = {};
    var mark = $(ctrl).val();
    var stuId = $(ctrl).attr('stu_id');
    var totalmark = $('.totalMark').val();
    console.log(parseFloat(mark).toFixed(2) + ' > ' + totalmark);
    console.log(parseFloat(mark) > parseFloat(totalmark));
    $('.loading_' + mainexamid + '_' + stuId).removeClass('hide');
    var number_pattren = /^[0-9.]*$/;
    if (!number_pattren.test(mark))
    {
        $('.enable_' + mainexamid).prop('disabled', true);
        swal({
            title: 'Error',
            text: 'Please enter valid numeric only.',
            type: 'error',
            confirmButtonText: 'OK!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        timelineSettings.enterStudentMark(subid, masterid, mainexamid);
                    }
                });
    }
    else if (mark == '' || parseFloat(mark).toFixed(2) < 0)
    {
        $('.enable_' + mainexamid).prop('disabled', true);
        swal({
            title: 'Error',
            text: 'Invalid Total Marks',
            type: 'error',
            confirmButtonText: 'OK!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        timelineSettings.enterStudentMark(subid, masterid, mainexamid);
                    }
                });
    }
    else if (parseFloat(mark) > totalmark) {
        swal({
            title: 'Error',
            text: 'Student mark exceeds the total mark!',
            type: 'error',
            confirmButtonText: 'OK',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        timelineSettings.enterStudentMark(subid, masterid, mainexamid);
                    }
                });
    } else {
        params['StuMainExMark'] = mark;
        params['totalMark'] = totalmark;
        params['master_id'] = masterid;
        params['exam_id'] = mainexamid;
        params['subject_id'] = subid;
        params['StuId'] = stuId;
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/updateStudentMark",
            data: params,
            dataType: 'JSON',
            success: function (resHtml) {
                console.log(resHtml);
                if (resHtml['type'] != 'error') {
                    $('.done_' + mainexamid + '_' + stuId).removeClass('hide');
                    $('.loading_' + mainexamid + '_' + stuId).addClass('hide');
                } else {
                    swal({
                        title: resHtml['type'],
                        text: resHtml.message,
                        type: resHtml['type'],
                        confirmButtonText: 'OK!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {

                                    if (resHtml['type'] == 'error')
                                        timelineSettings.enterStudentMark(subid, masterid, mainexamid);
                                }
                            });
                }
            }
        });
    }

};
timelineSettings.loadClassTest = function (masterid) {
    var url = commonSettings.timelineAjaxUrl + "/loadClassTest?masterid=" + masterid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.saveNewClasstest = function () {
    var datavalues = {};
    var err = 0;
    var errmsg = "";
    $('#addNewClassTestForm').find('input,select,textarea').each(function () {
        var splitval = $(this).attr('name').split('_');
        if (splitval[0] === 'aggregate') {
            if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
                console.log('in')
                datavalues[$(this).attr('name')] = $.trim($(this).val());
            } else {
                err = 1;
                errmsg = errmsg + "Please Select " + $(this).attr('title') + '<br/>';
                console.log('err')
            }
        } else {
            datavalues[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    if ($("#classtestname").val().length == 0)
    {
        err = 1;
        errmsg = errmsg + "Please Enter the Class Test Name" + '<br/>';
    }
    console.log(datavalues);
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/saveNewClasstest",
            data: datavalues,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                        }
                        else if (res['type'] == 'success') {
                            window.close();
                            window.opener.reloadParent();
                        }
                    });

                }

            }
        });
    }
    else
    {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
        $('#messageBox').click();
        err = 0;
    }
};
timelineSettings.editClasstest = function (classtestid) {
    var url = commonSettings.timelineAjaxUrl + "/editClasstest?classtestid=" + classtestid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.addClasstestMarks = function (classtestid, masterid) {
    var url = commonSettings.timelineAjaxUrl + "/addClasstestMarks?masterid=" + masterid + "&classtestid=" + classtestid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.updateClassTestTotalMark = function (mark, testid)
{
    $('.done_totmark').addClass('hide')
    $('.loading_totmark').removeClass('hide')
    var total_mark = parseInt(mark);
    var number_pattren = /^[0-9.]*$/;
    var err = 0, errormsg = '';
    if (!number_pattren.test(mark))
    {
        err = 1;
        errormsg = 'Please enter valid numeric only!';
    }
    else if (total_mark < 0) {
        err = 1;
        errormsg = 'Total Mark Cannot Be Negative!';
    }
    $('#clsTestMark').find('input.stuMark').each(function () {
        var markstu = $.trim($(this).val());
        if (markstu !== '' && parseFloat(markstu).toFixed(2) > total_mark) {
            err = 1;
            errormsg = 'Student mark exceeds the total mark!';
            return;
        }
    });
    if (err == 1)
    {
        $('#Total_Mark').val($('#Total_Mark').attr('placeholder'))
        $('#messageBoxModal .modal-title').html('Error');
        $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-danger fade in">' + errormsg + '</div>');
        $('#messageBoxModal').modal('show');
        $('.loading_totmark').addClass('hide')
    } else {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/updateClassTestTotMark",
            data: {
                test_id: testid,
                total_mark: total_mark
            },
            success: function (res) {
                if (res) {

                    $('.done_totmark').removeClass('hide')
                    $('.loading_totmark').addClass('hide')
                }

            }
        });
    }
};
timelineSettings.updateClassTestMark = function (mark, id, testid)
{
    total_mark = parseInt($('#Total_Mark').val());
    var number_pattren = /^[0-9.]*$/;

    if (!number_pattren.test(mark))
    {
        $('#div_' + id).html("Please enter valid numeric only.");
        $('#div_' + id).css("color", "red")
    }
    else if ($('#Total_Mark').val().length == 0) {
        $('#div_' + id).html("Please Enter Total Mark First!");
        $('#div_' + id).css("color", "red")
    }
    else if (total_mark < 0) {
        $('#div_' + id).html("Total Mark Cannot Be Negative!");
        $('#div_' + id).css("color", "red")
    }
    else if (mark < 0) {
        $('#div_' + id).html("Entered Mark Cannot Be Negative!");
        $('#div_' + id).css("color", "red")
    }
    else if (mark <= total_mark)
    {
        $('#Total_Mark').attr("disabled", "disabled");
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/updateClassTestMark",
            data: {
                test_id: testid,
                student_id: id,
                mark: mark,
                total_mark: total_mark
            },
            success: function (res) {
                if (res) {
                    $('#display').html(res);
                }

            }
        });
        $('#div_' + id).html("Mark updated");
        $('#div_' + id).css("color", "green")
    }
    else
    {
        $('#div_' + id).html("Exceeds Total Mark. Re-Enter correct mark");
        $('#div_' + id).css("color", "red")
    }
};
timelineSettings.deleteClasstest = function (classtestid, masterid, test) {
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html(' Do you confirm to delete this ' + test + ' Details?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/checkLinkageForTest",
            data: {testId: classtestid},
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    if (res['type'] == "success") {
                        timelineSettings.deleteTest(classtestid, masterid);
                    } else {
                        var lblcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                        $('#ConfirmModal .modal-title').html("Confirm Delete");
                        $('#ConfirmModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                        $('#ConfirmModal').modal('show');
                        $('#confirm_yes').unbind('click').click(function () {
                            $('#confirm_no').click()
                            timelineSettings.deleteTest(classtestid, masterid);

                        });
                    }

                }
            }
        });
    });
};
timelineSettings.deleteTest = function (testId, masterid) {

    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/removeClassTest",
        dataType: "JSON",
        data:
                {
                    test_id: testId
                },
        success: function (res) {
            if (res) {
                if (res['type'] == "success") {
                    $.ajax({
                        type: "POST",
                        url: commonSettings.timelineAjaxUrl + "/deleteClasstest",
                        data: {classtestid: testId, masterid: masterid},
                        dataType: 'JSON',
                        success: function (res) {
                            if (res) {
                                $('.msgBox-title').html('Message');
                                $('.msgBox-body').html(res['message']);
                                $('#messageBox').click();
                                $('#msg_ok').unbind('click').click(function () {
                                    if (res['type'] == 'error') {
                                        $('#msg_ok').unbind('click').click();
                                    }
                                    else if (res['type'] == 'success') {
                                        timelineSettings.loadTasks(masterid);
                                    }
                                });

                            }
                        }
                    });
                }
            }
        }
    });
};
timelineSettings.addMainExam = function () {
    var params = {};
    var err = 0;
    var error = '';
    $("#addMainexamForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n" + '<br/>';

            } else {
                params[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }

    });
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/addMainExam",
            data: params,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            // $('#formModal').modal();

                        }
                        else if (res['type'] == 'success') {
                            window.close();
                            window.opener.reloadParent();
                        }
                    });

                }

            }
        });
    }
    else
    {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
    }
};
timelineSettings.addMainexamMarks = function (examid, masterid) {
    var url = commonSettings.timelineAjaxUrl + "/addMainexamMarks?masterid=" + masterid + "&examid=" + examid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.deleteMainExamDet = function (examid, masterid) {
    $.ajax({
        type: "POST",
        url: commonSettings.timelineAjaxUrl + "/deleteMainExam",
        data: {examid: examid, masterid: masterid},
        dataType: 'JSON',
        success: function (res) {
            if (res) {
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                    }
                    else if (res['type'] == 'success') {
                        timelineSettings.loadTasks(masterid);
                    }
                });

            }
        }
    });
};
timelineSettings.deleteMainExam = function (examid, masterid, exam) {
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html(' Do you confirm to delete this ' + exam + ' Details ?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/checkMainExamMarks",
            data: {examid: examid, masterid: masterid},
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    if (res['type'] == "success") {
                        timelineSettings.deleteMainExamDet(examid, masterid);
                    } else {
                        var lblcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                        $('#ConfirmModal .modal-title').html("Confirm Delete");
                        $('#ConfirmModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                        $('#ConfirmModal').modal('show');
                        $('#confirm_yes').unbind('click').click(function () {
                            $('#confirm_no').click()
                            timelineSettings.deleteMainExamDet(examid, masterid);
                        });
                    }
                }
            }
        });
    });
};
timelineSettings.deleteAssignment = function (assid, masterid, name) {
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html(' Do you confirm to delete this ' + name + ' Details ?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.assignmentAjaxurl + "/checkLinkageForAssignment",
            data: {assignmentId: assid},
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    if (res['type'] == "success") {
                        timelineSettings.deleteAssignCompletely(assid, masterid);
                    } else {
                        var lblcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                        $('#ConfirmModal .modal-title').html("Confirm Delete");
                        $('#ConfirmModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                        $('#ConfirmModal').modal('show');
                        $('#confirm_yes').unbind('click').click(function () {
                            $('#confirm_no').click()
                            timelineSettings.deleteAssignCompletely(assid, masterid);

                        });
                    }

                }

            }
        });
    });
};
timelineSettings.deleteAssignCompletely = function (assignment, masterid) {
    $.ajax({
        type: "POST",
        url: commonSettings.assignmentAjaxurl + "/deleteAssignment",
        dataType: "JSON",
        data:
                {
                    assignment_id: assignment
                },
        success: function (res) {
            if (res) {
                if (res['type'] == "success") {
                    $.ajax({
                        type: "POST",
                        url: commonSettings.timelineAjaxUrl + "/deleteAssignmentAll",
                        data: {assignmentid: assignment, masterid: masterid},
                        dataType: 'JSON',
                        success: function (res) {
                            if (res) {
                                $('.msgBox-title').html('Message');
                                $('.msgBox-body').html(res['message']);
                                $('#messageBox').click();
                                $('#msg_ok').unbind('click').click(function () {
                                    if (res['type'] == 'error') {
                                        $('#msg_ok').unbind('click').click();
                                    }
                                    else if (res['type'] == 'success') {
                                        timelineSettings.loadTasks(masterid);
                                    }
                                });

                            }
                        }
                    });
                }
            }
        }
    });
};
timelineSettings.editMainExam = function (examid, masterid) {
    var url = commonSettings.timelineAjaxUrl + "/editMainExam?examid=" + examid + "&masterid=" + masterid
    popup = window.open(url, "Popup",
            "toolbar=no,scrollbars=no,location=no,statusbar=no,menubar=no,resizable=0,width=800,height=600,left = 490,top = 262");
    popup.focus();
};
timelineSettings.shareVideo = function (cntrl, sharedornt)
{
    timelineSettings.video_id = $(cntrl).attr('video_id');
    timelineSettings.topic_id = $(cntrl).attr('sub_lessonid');
    $('.tog_active').removeClass('fa fa-share-square');
    $('.tog_active').removeClass('fa fa-times');
    $('.tog_active').addClass('fa fa-share-square');
    $('.tog_active').html('Share');
    $(cntrl).find('i').html(' Shared');
    $(cntrl).find('i').removeClass(' fa fa-share-square');
    $(cntrl).find('i').addClass(' fa fa-times');

}
timelineSettings.saveMultipleFiles = function () {
    var Data = {};
    var file = {};
    var err = 0;
    var error = '';
    $("#fileuploadvideo").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n</br>";

            } else {
                Data[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            Data[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    var i = 0;
    $('#getfilename').find('a.fileretrieve').each(function () {
        file[i++] = $(this).text();
    });
    Data['filename'] = file;
    Data['video_id'] = timelineSettings.video_id;
    Data['topic_id'] = timelineSettings.topic_id;
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/saveMultipleFiles",
            data: Data,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                        }
                        else if (res['type'] == 'success') {
                            window.close();
                            window.opener.reloadParent();
                        }
                    });
                }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
    }
};
timelineSettings.saveReadingFiles = function () {
    var Data = {};
    var file = {};
    var err = 0;
    var error = '';
    $("#fileuploadreading").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n</br>";

            } else {
                Data[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            Data[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    var i = 0;
    $('#readingsfile').find('a.fileretrieve').each(function () {
        file[i++] = $(this).text();
    });
    Data['filename'] = file;
    if (!Data['filename']) {
        err = 1;
        error += " Upload File is required!\n</br>";
    }
    console.log(Data);
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.timelineAjaxUrl + "/saveReadingFiles",
            data: Data,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                        }
                        else if (res['type'] == 'success') {
                            window.close();
                            window.opener.reloadParent();
                        }
                    });
                }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
    }
};
timelineSettings.downloadAssignmentFile = function (assignmentid) {
    var newForm = $('<form>', {
        'action': commonSettings.timelineAjaxUrl + "/downloadAssignmentFile",
        'method': 'POST',
        'target': '_blank',
        'id': 'downloadAssignmentFile'
    }).append($('<input>', {
        'name': 'assignmentid',
        'value': assignmentid,
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};

timelineSettings.toggleWorkspace = function (masterid) {
    $('#taskdiv').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.evaluationUrl + "/loadEvaluation",
        data: {masterid: masterid},
        dataType: "HTML",
        success: function (res) {
            if (res) {
                $('#taskdiv').html(res);
            }

        }
    });
};
