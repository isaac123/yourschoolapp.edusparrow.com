
var orgvalSettings = {};
orgvalSettings.reqVar = [];
var ICON_UNICODE = {
    "500px": "",
    "adjust": "",
    "adn": "",
    "align-center": "",
    "align-justify": "",
    "align-left": "",
    "align-right": "",
    "amazon": "",
    "ambulance": "",
    "anchor": "",
    "android": "",
    "angellist": "",
    "angle-double-down": "",
    "angle-double-left": "",
    "angle-double-right": "",
    "angle-double-up": "",
    "angle-down": "",
    "angle-left": "",
    "angle-right": "",
    "angle-up": "",
    "apple": "",
    "archive": "",
    "area-chart": "",
    "arrow-circle-down": "",
    "arrow-circle-left": "",
    "arrow-circle-o-down": "",
    "arrow-circle-o-left": "",
    "arrow-circle-o-right": "",
    "arrow-circle-o-up": "",
    "arrow-circle-right": "",
    "arrow-circle-up": "",
    "arrow-down": "",
    "arrow-left": "",
    "arrow-right": "",
    "arrow-up": "",
    "arrows": "",
    "arrows-alt": "",
    "arrows-h": "",
    "arrows-v": "",
    "asterisk": "",
    "at": "",
    "automobile": "",
    "backward": "",
    "balance-scale": "",
    "ban": "",
    "bank": "",
    "bar-chart": "",
    "bar-chart-o": "",
    "barcode": "",
    "bars": "",
    "battery-0": "",
    "battery-1": "",
    "battery-2": "",
    "battery-3": "",
    "battery-4": "",
    "battery-empty": "",
    "battery-full": "",
    "battery-half": "",
    "battery-quarter": "",
    "battery-three-quarters": "",
    "bed": "",
    "beer": "",
    "behance": "",
    "behance-square": "",
    "bell": "",
    "bell-o": "",
    "bell-slash": "",
    "bell-slash-o": "",
    "bicycle": "",
    "binoculars": "",
    "birthday-cake": "",
    "bitbucket": "",
    "bitbucket-square": "",
    "bitcoin": "",
    "black-tie": "",
    "bold": "",
    "bolt": "",
    "bomb": "",
    "book": "",
    "bookmark": "",
    "bookmark-o": "",
    "briefcase": "",
    "btc": "",
    "bug": "",
    "building": "",
    "building-o": "",
    "bullhorn": "",
    "bullseye": "",
    "bus": "",
    "buysellads": "",
    "cab": "",
    "calculator": "",
    "calendar": "",
    "calendar-check-o": "",
    "calendar-minus-o": "",
    "calendar-o": "",
    "calendar-plus-o": "",
    "calendar-times-o": "",
    "camera": "",
    "camera-retro": "",
    "car": "",
    "caret-down": "",
    "caret-left": "",
    "caret-right": "",
    "caret-square-o-down": "",
    "caret-square-o-left": "",
    "caret-square-o-right": "",
    "caret-square-o-up": "",
    "caret-up": "",
    "cart-arrow-down": "",
    "cart-plus": "",
    "cc": "",
    "cc-amex": "",
    "cc-diners-club": "",
    "cc-discover": "",
    "cc-jcb": "",
    "cc-mastercard": "",
    "cc-paypal": "",
    "cc-stripe": "",
    "cc-visa": "",
    "certificate": "",
    "chain": "",
    "chain-broken": "",
    "check": "",
    "check-circle": "",
    "check-circle-o": "",
    "check-square": "",
    "check-square-o": "",
    "chevron-circle-down": "",
    "chevron-circle-left": "",
    "chevron-circle-right": "",
    "chevron-circle-up": "",
    "chevron-down": "",
    "chevron-left": "",
    "chevron-right": "",
    "chevron-up": "",
    "child": "",
    "chrome": "",
    "circle": "",
    "circle-o": "",
    "circle-o-notch": "",
    "circle-thin": "",
    "clipboard": "",
    "clock-o": "",
    "clone": "",
    "close": "",
    "cloud": "",
    "cloud-download": "",
    "cloud-upload": "",
    "cny": "",
    "code": "",
    "code-fork": "",
    "codepen": "",
    "coffee": "",
    "cog": "",
    "cogs": "",
    "columns": "",
    "comment": "",
    "comment-o": "",
    "commenting": "",
    "commenting-o": "",
    "comments": "",
    "comments-o": "",
    "compass": "",
    "compress": "",
    "connectdevelop": "",
    "contao": "",
    "copy": "",
    "copyright": "",
    "creative-commons": "",
    "credit-card": "",
    "crop": "",
    "crosshairs": "",
    "css3": "",
    "cube": "",
    "cubes": "",
    "cut": "",
    "cutlery": "",
    "dashboard": "",
    "dashcube": "",
    "database": "",
    "dedent": "",
    "delicious": "",
    "desktop": "",
    "deviantart": "",
    "diamond": "",
    "digg": "",
    "dollar": "",
    "dot-circle-o": "",
    "download": "",
    "dribbble": "",
    "dropbox": "",
    "drupal": "",
    "edit": "",
    "eject": "",
    "ellipsis-h": "",
    "ellipsis-v": "",
    "empire": "",
    "envelope": "",
    "envelope-o": "",
    "envelope-square": "",
    "eraser": "",
    "eur": "",
    "euro": "",
    "exchange": "",
    "exclamation": "",
    "exclamation-circle": "",
    "exclamation-triangle": "",
    "expand": "",
    "expeditedssl": "",
    "external-link": "",
    "external-link-square": "",
    "eye": "",
    "eye-slash": "",
    "eyedropper": "",
    "facebook": "",
    "facebook-f": "",
    "facebook-official": "",
    "facebook-square": "",
    "fast-backward": "",
    "fast-forward": "",
    "fax": "",
    "feed": "",
    "female": "",
    "fighter-jet": "",
    "file": "",
    "file-archive-o": "",
    "file-audio-o": "",
    "file-code-o": "",
    "file-excel-o": "",
    "file-image-o": "",
    "file-movie-o": "",
    "file-o": "",
    "file-pdf-o": "",
    "file-photo-o": "",
    "file-picture-o": "",
    "file-powerpoint-o": "",
    "file-sound-o": "",
    "file-text": "",
    "file-text-o": "",
    "file-video-o": "",
    "file-word-o": "",
    "file-zip-o": "",
    "files-o": "",
    "film": "",
    "filter": "",
    "fire": "",
    "fire-extinguisher": "",
    "firefox": "",
    "flag": "",
    "flag-checkered": "",
    "flag-o": "",
    "flash": "",
    "flask": "",
    "flickr": "",
    "floppy-o": "",
    "folder": "",
    "folder-o": "",
    "folder-open": "",
    "folder-open-o": "",
    "font": "",
    "fonticons": "",
    "forumbee": "",
    "forward": "",
    "foursquare": "",
    "frown-o": "",
    "futbol-o": "",
    "gamepad": "",
    "gavel": "",
    "gbp": "",
    "ge": "",
    "gear": "",
    "gears": "",
    "genderless": "",
    "get-pocket": "",
    "gg": "",
    "gg-circle": "",
    "gift": "",
    "git": "",
    "git-square": "",
    "github": "",
    "github-alt": "",
    "github-square": "",
    "gittip": "",
    "glass": "",
    "globe": "",
    "google": "",
    "google-plus": "",
    "google-plus-square": "",
    "google-wallet": "",
    "graduation-cap": "",
    "gratipay": "",
    "group": "",
    "h-square": "",
    "hacker-news": "",
    "hand-grab-o": "",
    "hand-lizard-o": "",
    "hand-o-down": "",
    "hand-o-left": "",
    "hand-o-right": "",
    "hand-o-up": "",
    "hand-paper-o": "",
    "hand-peace-o": "",
    "hand-pointer-o": "",
    "hand-rock-o": "",
    "hand-scissors-o": "",
    "hand-spock-o": "",
    "hand-stop-o": "",
    "hdd-o": "",
    "header": "",
    "headphones": "",
    "heart": "",
    "heart-o": "",
    "heartbeat": "",
    "history": "",
    "home": "",
    "hospital-o": "",
    "hotel": "",
    "hourglass": "",
    "hourglass-1": "",
    "hourglass-2": "",
    "hourglass-3": "",
    "hourglass-end": "",
    "hourglass-half": "",
    "hourglass-o": "",
    "hourglass-start": "",
    "houzz": "",
    "html5": "",
    "i-cursor": "",
    "ils": "",
    "image": "",
    "inbox": "",
    "indent": "",
    "industry": "",
    "info": "",
    "info-circle": "",
    "inr": "",
    "instagram": "",
    "institution": "",
    "internet-explorer": "",
    "intersex": "",
    "ioxhost": "",
    "italic": "",
    "joomla": "",
    "jpy": "",
    "jsfiddle": "",
    "key": "",
    "keyboard-o": "",
    "krw": "",
    "language": "",
    "laptop": "",
    "lastfm": "",
    "lastfm-square": "",
    "leaf": "",
    "leanpub": "",
    "legal": "",
    "lemon-o": "",
    "level-down": "",
    "level-up": "",
    "life-bouy": "",
    "life-buoy": "",
    "life-ring": "",
    "life-saver": "",
    "lightbulb-o": "",
    "line-chart": "",
    "link": "",
    "linkedin": "",
    "linkedin-square": "",
    "linux": "",
    "list": "",
    "list-alt": "",
    "list-ol": "",
    "list-ul": "",
    "location-arrow": "",
    "lock": "",
    "long-arrow-down": "",
    "long-arrow-left": "",
    "long-arrow-right": "",
    "long-arrow-up": "",
    "magic": "",
    "magnet": "",
    "mail-forward": "",
    "mail-reply": "",
    "mail-reply-all": "",
    "male": "",
    "map": "",
    "map-marker": "",
    "map-o": "",
    "map-pin": "",
    "map-signs": "",
    "mars": "",
    "mars-double": "",
    "mars-stroke": "",
    "mars-stroke-h": "",
    "mars-stroke-v": "",
    "maxcdn": "",
    "meanpath": "",
    "medium": "",
    "medkit": "",
    "meh-o": "",
    "mercury": "",
    "microphone": "",
    "microphone-slash": "",
    "minus": "",
    "minus-circle": "",
    "minus-square": "",
    "minus-square-o": "",
    "mobile": "",
    "mobile-phone": "",
    "money": "",
    "moon-o": "",
    "mortar-board": "",
    "motorcycle": "",
    "mouse-pointer": "",
    "music": "",
    "navicon": "",
    "neuter": "",
    "newspaper-o": "",
    "object-group": "",
    "object-ungroup": "",
    "odnoklassniki": "",
    "odnoklassniki-square": "",
    "opencart": "",
    "openid": "",
    "opera": "",
    "optin-monster": "",
    "outdent": "",
    "pagelines": "",
    "paint-brush": "",
    "paper-plane": "",
    "paper-plane-o": "",
    "paperclip": "",
    "paragraph": "",
    "paste": "",
    "pause": "",
    "paw": "",
    "paypal": "",
    "pencil": "",
    "pencil-square": "",
    "pencil-square-o": "",
    "phone": "",
    "phone-square": "",
    "photo": "",
    "picture-o": "",
    "pie-chart": "",
    "pied-piper": "",
    "pied-piper-alt": "",
    "pinterest": "",
    "pinterest-p": "",
    "pinterest-square": "",
    "plane": "",
    "play": "",
    "play-circle": "",
    "play-circle-o": "",
    "plug": "",
    "plus": "",
    "plus-circle": "",
    "plus-square": "",
    "plus-square-o": "",
    "power-off": "",
    "print": "",
    "puzzle-piece": "",
    "qq": "",
    "qrcode": "",
    "question": "",
    "question-circle": "",
    "quote-left": "",
    "quote-right": "",
    "ra": "",
    "random": "",
    "rebel": "",
    "recycle": "",
    "reddit": "",
    "reddit-square": "",
    "refresh": "",
    "registered": "",
    "remove": "",
    "renren": "",
    "reorder": "",
    "repeat": "",
    "reply": "",
    "reply-all": "",
    "retweet": "",
    "rmb": "",
    "road": "",
    "rocket": "",
    "rotate-left": "",
    "rotate-right": "",
    "rouble": "",
    "rss": "",
    "rss-square": "",
    "rub": "",
    "ruble": "",
    "rupee": "",
    "safari": "",
    "save": "",
    "scissors": "",
    "search": "",
    "search-minus": "",
    "search-plus": "",
    "sellsy": "",
    "send": "",
    "send-o": "",
    "server": "",
    "share": "",
    "share-alt": "",
    "share-alt-square": "",
    "share-square": "",
    "share-square-o": "",
    "shekel": "",
    "sheqel": "",
    "shield": "",
    "ship": "",
    "shirtsinbulk": "",
    "shopping-cart": "",
    "sign-in": "",
    "sign-out": "",
    "signal": "",
    "simplybuilt": "",
    "sitemap": "",
    "skyatlas": "",
    "skype": "",
    "slack": "",
    "sliders": "",
    "slideshare": "",
    "smile-o": "",
    "soccer-ball-o": "",
    "sort": "",
    "sort-alpha-asc": "",
    "sort-alpha-desc": "",
    "sort-amount-asc": "",
    "sort-amount-desc": "",
    "sort-asc": "",
    "sort-desc": "",
    "sort-down": "",
    "sort-numeric-asc": "",
    "sort-numeric-desc": "",
    "sort-up": "",
    "soundcloud": "",
    "space-shuttle": "",
    "spinner": "",
    "spoon": "",
    "spotify": "",
    "square": "",
    "square-o": "",
    "stack-exchange": "",
    "stack-overflow": "",
    "star": "",
    "star-half": "",
    "star-half-empty": "",
    "star-half-full": "",
    "star-half-o": "",
    "star-o": "",
    "steam": "",
    "steam-square": "",
    "step-backward": "",
    "step-forward": "",
    "stethoscope": "",
    "sticky-note": "",
    "sticky-note-o": "",
    "stop": "",
    "street-view": "",
    "strikethrough": "",
    "stumbleupon": "",
    "stumbleupon-circle": "",
    "subscript": "",
    "subway": "",
    "suitcase": "",
    "sun-o": "",
    "superscript": "",
    "support": "",
    "table": "",
    "tablet": "",
    "tachometer": "",
    "tag": "",
    "tags": "",
    "tasks": "",
    "taxi": "",
    "television": "",
    "tencent-weibo": "",
    "terminal": "",
    "text-height": "",
    "text-width": "",
    "th": "",
    "th-large": "",
    "th-list": "",
    "thumb-tack": "",
    "thumbs-down": "",
    "thumbs-o-down": "",
    "thumbs-o-up": "",
    "thumbs-up": "",
    "ticket": "",
    "times": "",
    "times-circle": "",
    "times-circle-o": "",
    "tint": "",
    "toggle-down": "",
    "toggle-left": "",
    "toggle-off": "",
    "toggle-on": "",
    "toggle-right": "",
    "toggle-up": "",
    "trademark": "",
    "train": "",
    "transgender": "",
    "transgender-alt": "",
    "trash": "",
    "trash-o": "",
    "tree": "",
    "trello": "",
    "tripadvisor": "",
    "trophy": "",
    "truck": "",
    "try": "",
    "tty": "",
    "tumblr": "",
    "tumblr-square": "",
    "turkish-lira": "",
    "tv": "",
    "twitch": "",
    "twitter": "",
    "twitter-square": "",
    "umbrella": "",
    "underline": "",
    "undo": "",
    "university": "",
    "unlink": "",
    "unlock": "",
    "unlock-alt": "",
    "unsorted": "",
    "upload": "",
    "usd": "",
    "user": "",
    "user-md": "",
    "user-plus": "",
    "user-secret": "",
    "user-times": "",
    "users": "",
    "venus": "",
    "venus-double": "",
    "venus-mars": "",
    "viacoin": "",
    "video-camera": "",
    "vimeo": "",
    "vimeo-square": "",
    "vine": "",
    "vk": "",
    "volume-down": "",
    "volume-off": "",
    "volume-up": "",
    "warning": "",
    "wechat": "",
    "weibo": "",
    "weixin": "",
    "whatsapp": "",
    "wheelchair": "",
    "wifi": "",
    "wikipedia-w": "",
    "windows": "",
    "won": "",
    "wordpress": "",
    "wrench": "",
    "xing": "",
    "xing-square": "",
    "y-combinator": "",
    "y-combinator-square": "",
    "yahoo": "",
    "yc": "",
    "yc-square": "",
    "yelp": "",
    "yen": "",
    "youtube": "",
    "youtube-play": "",
    "youtube-square": ""
};
d3.contextMenu = function (menu, openCallback) {
    console.log(menu);
    // create the div element that will hold the context menu
    d3.selectAll('.d3-context-menu').data([1])
            .enter()
            .append('div')
            .attr('class', 'd3-context-menu');

    // close menu
    d3.select('body').on('click.d3-context-menu', function () {
        d3.select('.d3-context-menu').style('display', 'none');
    });

    // this gets executed when a contextmenu event occurs
    return function (data, index) {
        var elm = this;

        d3.selectAll('.d3-context-menu').html('');
        var list = d3.selectAll('.d3-context-menu').append('ul');
        list.selectAll('li').data(menu).enter()
                .append('li')
                .html(function (d) {
                    return d.title;
                })
                .on('click', function (d, i) {
                    d.action(elm, data, index);
                    d3.select('.d3-context-menu').style('display', 'none');
                });

        // the openCallback allows an action to fire before the menu is displayed
        // an example usage would be closing a tooltip
        if (openCallback)
            openCallback(data, index);

        // display context menu
        d3.select('.d3-context-menu')
                .style('left', (d3.event.pageX - 2) + 'px')
                .style('top', (d3.event.pageY - 2) + 'px')
                .style('display', 'block');

        d3.event.preventDefault();
    };
};

var menu = [
    {
        title: '<a tabindex="-1" href="javascript:;"  >View </a>',
        action: function (elm, d, i) {
            console.log(d);
            orgvalSettings.loadViewModal(d.nodeid, d.name);
        }
    },
    {
        title: '<a tabindex="-1" href="#">Edit</a>',
        action: function (elm, d, i) {
            orgvalSettings.loadEditModal(d.nodeid, d.name);
        }
    },
    {
        title: '<a tabindex="-1" href="#">Delete</a>',
        action: function (elm, d, i) {
            orgvalSettings.confirmDelete(d.nodeid, d.name);
        }
    },
    {
        title: '<a tabindex="-1" href="#">Add node below</a>',
        action: function (elm, d, i) {
            orgvalSettings.loadAddNodeModal(d.nodeid, d.name);
        }
    }
];
orgvalSettings.loadWithSubmodules = function (val) {
    $('.suboor').css('color', '#cbcbcb')
    $("#boog").html('<div class=" well"><br><br><div  style="margin:0 10%"><i class="fa fa-spinner fa-spin"></i> Loading...</div><br><br></div>')
    if (val) {
        $('#suboor_' + val).css('color', '#1fb5ad')
        orgvalSettings.reqVar.push("module=" + val);
    }
    treeJSON = d3.json(commonSettings.orgmaster + "/valueTreeJson?" + orgvalSettings.reqVar.join("&"), function (error, treeData) {

        var margin = {
            top: 20,
            right: 120,
            bottom: 20,
            left: 120
        },
        width = 960 - margin.right - margin.left,
                height = 800 - margin.top - margin.bottom;

        var tree = d3.layout.tree().nodeSize([70, 40]);

        var root = treeData;

        var viewerWidth = 1000;//$(document).width();
        var viewerHeight = 700;//$(document).height();
        var i = 0,
                duration = 750,
                rectW = 150,
                rectH = 30;

        var tree = d3.layout.tree().nodeSize([180, 40]);
        var diagonal = d3.svg.diagonal()
                .projection(function (d) {
                    return [d.x + rectW / 2, d.y + rectH / 2];
                });
        $("#boog").empty()
        var svg = d3.select("#boog").append("svg").attr("width", viewerWidth).attr("height", viewerHeight)
                .call(zm = d3.behavior.zoom().scaleExtent([0.1, 2]).on("zoom", redraw)).append("g")
                .attr("transform", "translate(" + 350 + "," + 20 + ")");

//necessary so that zoom knows where to zoom and unzoom from
        zm.translate([350, 20]);

        root.x0 = 0;
        root.y0 = height / 2;

        function collapse(d) {
            if (d.children) {
                d._children = d.children;
                d._children.forEach(collapse);
                d.children = null;
            }
        }

        root.children.forEach(collapse);
        update(root);

//d3.select("#boog").style("height", "800px");

        function update(source) {

            // Compute the new tree layout.
            var nodes = tree.nodes(root).reverse(),
                    links = tree.links(nodes);

            // Normalize for fixed-depth.

            nodes.forEach(function (d) {
                d.y = d.depth * 100;
            });
            var levelWidth = [1];

            var newHeight = d3.max(levelWidth) * 180; // 25 pixels per line  
            tree = tree.nodeSize([newHeight, viewerWidth]);
            // Update the nodes…
            var node = svg.selectAll("g.node")
                    .data(nodes, function (d) {
                        return d.id || (d.id = ++i);
                    });

            // Enter any new nodes at the parent's previous position.
            var nodeEnter = node.enter().append("g")
                    .attr("class", "node")
                    .attr("transform", function (d) {
                        return "translate(" + source.x0 + "," + source.y0 + ")";
                    })
                    .on("click", click);

            nodeEnter.append("rect")
                    .attr("width", rectW)
                    .attr("height", rectH)
                    .attr("stroke", "black")
                    .attr("stroke-width", 1)
                    .on('contextmenu', d3.contextMenu(menu))
                    .style("fill", function (d) {
                        return d._children ? "#fff" : "#fff";
                    });


            nodeEnter.append('text')
                    .attr("x", "3")
                    .attr("y", rectH / 2)
                    .attr('dominant-baseline', 'central')
                    .attr('font-family', 'FontAwesome')
                    .text(function (d) {
                        return ICON_UNICODE[d.font] ? ICON_UNICODE[d.font] : d.font;
                    })
                    .style('font-size', '18px')
                    .style("fill", function (d) {
                        return d._children ? d.fillcolor : d.fillcolor;
                    })
                    .attr('class', 'main')
                    .attr('valnodeid', function (d) {
                        return d.nodeid;
                    })
                    .on('contextmenu', d3.contextMenu(menu))
                    .attr('valnodeid', function (d) {
                        return d.nodeid;
                    });
            nodeEnter.append("text")
                    .attr("x", rectW / 2)
                    .attr("y", rectH / 2)
                    .attr("dy", ".35em")
                    .attr("text-anchor", "middle")
                    .text(function (d) {
                        return d.name;
                    })
                    .on('contextmenu', d3.contextMenu(menu))
                    .attr('valnodeid', function (d) {
                        return d.nodeid;
                    });

            // Transition nodes to their new position.
            var nodeUpdate = node.transition()
                    .duration(duration)
                    .attr("transform", function (d) {
                        return "translate(" + d.x + "," + d.y + ")";
                    });

            nodeUpdate.select("rect")
                    .attr("width", rectW)
                    .attr("height", rectH)
                    .attr("stroke", "black")
                    .attr("stroke-width", 1)
                    .style("fill", function (d) {
                        return d._children ? "#fff" : "#fff";
                    });

            nodeUpdate.select("text")
                    .style("fill-opacity", 1);

            // Transition exiting nodes to the parent's new position.
            var nodeExit = node.exit().transition()
                    .duration(duration)
                    .attr("transform", function (d) {
                        return "translate(" + source.x + "," + source.y + ")";
                    })
                    .remove();

            nodeExit.select("rect")
                    .attr("width", rectW)
                    .attr("height", rectH)
                    //.attr("width", bbox.getBBox().width)""
                    //.attr("height", bbox.getBBox().height)
                    .attr("stroke", "black")
                    .attr("stroke-width", 1);

            nodeExit.select("text");

            // Update the links…
            var link = svg.selectAll("path.link")
                    .data(links, function (d) {
                        return d.target.id;
                    });

            // Enter any new links at the parent's previous position.
            link.enter().insert("path", "g")
                    .attr("class", "link")
                    .attr("x", rectW / 2)
                    .attr("y", rectH / 2)
                    .attr("d", function (d) {
                        var o = {
                            x: source.x0,
                            y: source.y0
                        };
                        return diagonal({
                            source: o,
                            target: o
                        });
                    });

            // Transition links to their new position.
            link.transition()
                    .duration(duration)
                    .attr("d", diagonal);

            // Transition exiting nodes to the parent's new position.
            link.exit().transition()
                    .duration(duration)
                    .attr("d", function (d) {
                        var o = {
                            x: source.x,
                            y: source.y
                        };
                        return diagonal({
                            source: o,
                            target: o
                        });
                    })
                    .remove();

            // Stash the old positions for transition.
            nodes.forEach(function (d) {
                d.x0 = d.x;
                d.y0 = d.y;
            });
        }

// Toggle children on click.
        function click(d) {
            if (d.children) {
                d._children = d.children;
                d.children = null;
            } else {
                d.children = d._children;
                d._children = null;
            }
            update(d);
        }

//Redraw for zoom
        function redraw() {
            //console.log("here", d3.event.translate, d3.event.scale);
            svg.attr("transform",
                    "translate(" + d3.event.translate + ")"
                    + " scale(" + d3.event.scale + ")");
        }
    });
};
orgvalSettings.loadWithSubmodules();

orgvalSettings.loadViewModal = function (valueid, name) {
    if (valueid) {
        $.ajax({
            type: "POST",
            url: commonSettings.orgmaster + "/viewValueTree",
            data: {valueid: valueid},
            dataType: "html",
            success: function (res) {
                $('.msgBox-title').html('View ' + name + ' details');
                $('.msgBox-body').html(res);
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function () {
                    $('#messageBoxModal').modal('hide');
                });
            }
        });
    }
};

orgvalSettings.confirmDelete = function (valueid, name) {
    if (valueid) {
        $('.cnfBox-title').html('Confirm Delete');
        $('.cnfBox-body').html('<div class="alert alert-block alert-warning fade in">Do you confirm to delete this node? </div>');
        $('#ConfirmModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#confirm_yes').unbind('click').click(function () {
            $('#ConfirmModal').modal('hide');
            $.ajax({
                type: "POST",
                url: commonSettings.orgmaster + "/deleteValueNode",
                data: {valueid: valueid},
                dataType: "json",
                success: function (res) {
                    if (res) {
                        var lblcls = res.type == 'error' ? 'danger' : 'success';
                        $('.msgBox-title').html('Message');
                        $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                        $('#messageBoxModal').modal();
                        $('#msg_ok').unbind('click').click(function (event) {
                            $('#messageBoxModal').modal('hide');
                            orgvalSettings.loadWithSubmodules();
                        });
                        return false;
                    }
                }
            });
        });
    }
};
orgvalSettings.loadEditModal = function (valueid, name) {
    if (valueid) {
        $.ajax({
            type: "POST",
            url: commonSettings.orgmaster + "/editValueTree",
            data: {valueid: valueid},
            dataType: "html",
            success: function (res) {
                $('.formBox-title').html('Edit ' + name + ' details');
                $('.formBox-body').html(res);
                $('#formModal').modal('show')
                $('#confirm_save').unbind('click').click(function () {
                    $('#formModal').modal('hide');
                    orgvalSettings.updateValueTree('edit');

                });
            }
        });
    }
};


orgvalSettings.updateValueTree = function (action) {
    var params = {};
    var err = 0;
    var error = '';

    $("#valueSubTreeForm").find('select,input,textarea').each(function () {
        if ($(this).is(':visible')) {
            if ($(this).parent().parent().find('label').hasClass('mandatory')) {
                if ($.trim($(this).val()) == '') {
                    err = 1;
                    error += $(this).attr('title') + " is required!<br>";

                } else {
                    params[$(this).attr('name')] = $.trim($(this).val());
                }

            } else {
                params[$(this).attr('name')] = $.trim($(this).val());
            }
        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }

    });
    params['actions'] = action;
    console.log(error)
    if (err == 0) {

        $.ajax({
            type: "POST",
            url: commonSettings.orgmaster + "/updateOrgValues",
            data: params,
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    var lblcls = res.type == 'error' ? 'times' : (res.type == 'success' ? 'check' : 'info');
                    var lblstcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                    $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-' + lblcls + '"></i> ' + res.type.toUpperCase());
                    $('#messageBoxModal .msgBox-body').html('<span class="text text-'+lblstcls+' fade in">' + res.message + '</span>');
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        $('#messageBoxModal').modal('hide');
                        if (res.type == 'success') {
                            orgvalSettings.loadWithSubmodules();
                        } else {
                            $('#formModal').modal('show')
                        }

                    });
                }
            }
        });


    } else {
        $('.msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('.msgBox-body').html('<span class="text text-danger fade in">' + error + '</span>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#messageBoxModal').modal('hide');
            $('#formModal').modal('show')
        });
        err = 0;
//        return false;
    }

};

orgvalSettings.loadAddNodeModal = function (valueid, name) {
    if (valueid) {
        $.ajax({
            type: "POST",
            url: commonSettings.orgmaster + "/addValueTree",
            data: {valueid: valueid},
            dataType: "html",
            success: function (res) {
                $('.formBox-title').html('Add node below ' + name);
                $('.formBox-body').html(res);
                $('#formModal').modal('show')
                $('#confirm_save').unbind('click').click(function () {
                    $('#formModal').modal('hide');
                    orgvalSettings.updateValueTree('add');

                });
            }
        });
    }
};


