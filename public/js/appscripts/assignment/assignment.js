var assignmentSettings = {};

assignmentSettings.loadSection = function (sval) {
    console.log(sval);
    assignmentSettings.classID = sval;
    $.ajax({
        type: "POST",
        url: commonSettings.assignmentAjaxurl + "/getSectionByClass",
        data: {
            classId: sval
        },
        success: function (res) {
            if (res) {
                $('#subjTeachDet').html(res);
                $('#subjTeachDet').removeClass('hide');
                $('#subjDet').addClass('hide');
                $('#subjTechMatrix').addClass('hide')
            }

        }
    });

};

assignmentSettings.loadSubtrees = function (ctrl) {
    console.log($(ctrl).parent().parent().next('.subtreediv').html())
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.assignmentAjaxurl + "/loadSubtrees",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};
assignmentSettings.loadSubjects = function (val) {
    assignmentSettings.subDivVal = val;
    console.log(val);
    $.ajax({
        type: "POST",
        url: commonSettings.assignmentAjaxurl + "/getSubjectsByClass",
        data: {
            class_id: assignmentSettings.classID,
            subDivVal: val
        },
        success: function (res) {
            if (res) {
                //alert(res);
                //$('#mainDiv').append(res);
                $('#subjDet').html(res);
                $('#subjDet').removeClass('hide')
                $('#subjTechMatrix').addClass('hide')
            }

        }
    });

};


assignmentSettings.Subjectsloaded = function (ctrl) {
//    $('.combispan').addClass('inactive')
//    $(ctrl).toggleClass('inactive')
    $('.combispan').addClass('label-default').removeClass('label-warning')
        if ($(ctrl).hasClass('combispan'))
    $(ctrl).removeClass('label-default').addClass('label-warning')

    assignmentSettings.subject_masterid = $(ctrl).attr('id').split('_')[1];

    var now = new Date();
    console.log(now);
    var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
    var timestamp = startOfDay / 1000;
    assignmentSettings.currentDate = timestamp;
    console.log(assignmentSettings.currentDate);

    _loadStuAssDiv();


};

assignmentSettings.SubjectsloadedAfterMarks = function (subject_masterid) {

    assignmentSettings.subject_masterid = subject_masterid;

    var now = new Date();
    console.log(now);
    var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
    var timestamp = startOfDay / 1000;
    assignmentSettings.currentDate = timestamp;
    console.log(assignmentSettings.currentDate);

    _loadStuAssDiv();


};


function _loadStuAssDiv() {

    $.ajax({
        type: "POST",
        url: commonSettings.assignmentAjaxurl + "/loadAssignmentAddPanel",
        data: {
            subject_masterid: assignmentSettings.subject_masterid,
            currentDate: assignmentSettings.currentDate

        },
        success: function (res) {
            if (res) {
                $('#subjTechMatrix').html(res);
                $('#subjTechMatrix').removeClass('hide')
            }

        }
    });

}

assignmentSettings.changeDate = function (currentDate, ctrl) {

    var UType = $(ctrl).attr('utype');
    var date = new Date(currentDate * 1000);
    console.log(UType)
    assignmentSettings.currentDate = currentDate;
    if (UType == 'student')
        _loadStuAssDiv();


};



assignmentSettings.addnewAssignment = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.assignmentAjaxurl + "/addNewAssignment",
        dataType: "html",
        data: {subject_masterid: assignmentSettings.subject_masterid},
        success: function (res) {
            $('.formBox-title').html('Add Assignment');
            $('.formBox-body').html(res);

            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#formModal').modal('show');
            $('#confirm_save').unbind('click').click(function () {

                assignmentSettings.newAssignment();
            });
        }
    });

};



assignmentSettings.newAssignment = function () {
    var AssignmentData = {};
    var assdate = 0;
    var asstopic = 0;
    var valid = 0;
    var errmsg = '';
    console.log(AssignmentData);
    $("#addNewAssignmentForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                valid = 1;
                errmsg += $(this).attr('title') + " is required!<br>";

            } else {
                AssignmentData[$(this).attr('name')] = $.trim($(this).val());
            }
        } else {
            if ($(this).is(':checkbox'))
            {
                if ($(this).is(':checked') == true)
                    AssignmentData[$(this).attr('name')] = 'on';
                else
                    AssignmentData[$(this).attr('name')] = 'off';
            } else {
                AssignmentData[$(this).attr('name')] = $.trim($(this).val());
            }
        }
    });
    console.log(AssignmentData);
    AssignmentData['subject_masterid'] = assignmentSettings.subject_masterid;

    if (valid == 0)
    {
        $('#confirm_close').click();
        assignmentSettings.saveAssignment(AssignmentData);
        $('.errmsgAssignmentTopic').addClass('hide');
        $('.errmsgAssignmentDeadline').addClass('hide');
    }
    else
    {
        $('#confirm_close').click();
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#formModal').modal('show')
        });
    }
};



assignmentSettings.viewassignment = function (assignment) {
    //alert(assignment);
    assignmentSettings.selectedAssignment = assignment;
    $.ajax({
        type: "POST",
        url: commonSettings.assignmentAjaxurl + "/viewEditAssignment",
        data: {
            selectedAssignment: assignmentSettings.selectedAssignment,
            action: 'View'
        },
        success: function (res) {
            $('.msgBox-title').html('View Assignment');
            $('.msgBox-body').html(res);
            $('#messageBox').click();

        }
    });


};



assignmentSettings.editassignment = function (assignment) {
    //  console.log(assignment)
    var AssignmentData = {};
    assignmentSettings.selectedAssignment = assignment;
    $.ajax({
        type: "POST",
        url: commonSettings.assignmentAjaxurl + "/viewEditAssignment",
        data: {
            selectedAssignment: assignmentSettings.selectedAssignment,
            action: 'Edit'
        },
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Edit Assignment');
            $('.formBox-body').html(res);
            $('#modalForm').click();
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                var valid = 0;
                var errmsg = '';
                $("#viewAssignmentForm").find('input,select,textarea').each(function () {
                    if ($(this).parent().prev('label').hasClass('mandatory')) {
                        if ($.trim($(this).val()) == '') {
                            valid = 1;
                            errmsg += $(this).attr('title') + " is required!<br>";

                        } else {
                            AssignmentData[$(this).attr('name')] = $.trim($(this).val());
                        }
                    } else {
                        if ($(this).is(':checkbox'))
                        {
                            if ($(this).is(':checked') == true)
                                AssignmentData[$(this).attr('name')] = 'on';
                            else
                                AssignmentData[$(this).attr('name')] = 'off';
                        } else {
                            AssignmentData[$(this).attr('name')] = $.trim($(this).val());
                        }
                    }
                });
                AssignmentData['subject_masterid'] = assignmentSettings.subject_masterid;
                if (valid == 0)
                {
                    assignmentSettings.saveAssignment(AssignmentData)
                }
                else
                {
                    $('#confirm_close').click();
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        $('#formModal').modal('show')
                    });
                }

            });

        }
    });


};

assignmentSettings.viewAssignment = function () {
    $('.formBox-title').html('View Assignment');
    $('.formBox-body').html($('#viewAssignment').html());
    $('#modalForm').click();
    $('#confirm_save').unbind('click').click(function () {
        $('#confirm_close').click();
        assignmentSettings.saveAssignment();
    });
}

assignmentSettings.saveAssignment = function (AssignmentData) {

    $.ajax({
        type: "POST",
        url: commonSettings.assignmentAjaxurl + "/saveAssignment",
        data: AssignmentData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                        return false;
                    }
                    else if (res['type'] == 'success') {
                        _loadStuAssDiv()
                    }
                });

            }
        }
    });
}

assignmentSettings.deleteassignment = function (assignment) {
    $('#ConfirmModal .cnfBox-title').html('Confirm Delete');
    $('#ConfirmModal .cnfBox-body').html('<div class="alert alert-block alert-warning fade in">Do you confirm to delete this assignment? </div>');
    $('#ConfirmModal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#confirm_yes').unbind('click').click(function () {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.assignmentAjaxurl + "/checkLinkageForAssignment",
            data: {assignmentId: assignment},
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    if (res['type'] == "success") {
                        assignmentSettings.deleteAssignCompletely(assignment)
                    } else {

                        var lblcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                        $('#ConfirmModal .modal-title').html("Confirm Delete");
                        $('#ConfirmModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                        $('#ConfirmModal').modal('show');
                        $('#confirm_yes').unbind('click').click(function () {
                            $('#confirm_no').click()
                            assignmentSettings.deleteAssignCompletely(assignment)

                        });
                    }

                }

            }
        });
    });

};

assignmentSettings.deleteAssignCompletely = function (assignment) {

    $.ajax({
        type: "POST",
        url: commonSettings.assignmentAjaxurl + "/deleteAssignment",
        dataType: "JSON",
        data:
                {
                    assignment_id: assignment
                },
        success: function (res) {
            if (res) {
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function (event) {
                    if (res['type'] == "success") {
//                        examSettings.viewClassTests(examSettings.subject_masterid);
                        _loadStuAssDiv();
                    }
                });
            }
        }
    });
};
assignmentSettings.loadAssignmentDet = function (ctrl) {
//       $('.assDet').addClass('hide');
    $(ctrl).next('tr.assDet').toggleClass('hide');
};

assignmentSettings.mainexamlink = function () {
    if ($('#is_evaluation').is(':checked')) {
        $('#mainexamlink').removeClass('hide');
        $('#selectmainexam').addClass('hide');
    }
    else
    {
        $('#mainexamlink').addClass('hide');
        $('#selectmainexam').addClass('hide');
        $('#linkmainexam').attr('checked', false);
    }
};

assignmentSettings.selectmainexam = function () {
    if ($('#linkmainexam').is(':checked')) {
        $('#selectmainexam').removeClass('hide');
    }
    else
    {
        $('#selectmainexam').addClass('hide');
    }
};

assignmentSettings.loadAssignmentMark = function (val) {
    if (val != -1)
    {
        assignmentSettings.selectedAssignment = val;

        //markSettings.test_name = $("#testid option:selected").text();        
        $.ajax({
            type: "POST",
            url: commonSettings.assignmentAjaxurl + "/loadAssignmentStudents",
            data: {
                subject_masterid: assignmentSettings.subject_masterid,
                assignment_id: val
            },
            success: function (res) {
                if (res) {
                    $('#subjTechMatrix').html(res);
                    //$('#viewclasstest').removeClass('hide');
                }

            }
        });
    }
    else
    {
        //$('#loadClassTestStudents').addClass('hide');
    }
};


assignmentSettings.updateAssignmentTotalMark = function (mark)
{
    $('.done_totmark').addClass('hide')
    $('.loading_totmark').removeClass('hide')
    var total_mark = parseInt(mark);
    var number_pattren = /^[0-9.]*$/;
    var err = 0, errormsg = '';
    if (!number_pattren.test(mark))
    {
        err = 1;
        errormsg = 'Please enter valid numeric only!';
    }
    else if (total_mark < 0) {
        err = 1;
        errormsg = 'Total Mark Cannot Be Negative!';
    }
    $('#assigMrk').find('input.stuMark').each(function () {
        var markstu = $.trim($(this).val());
//        console.log(markstu)
        if (markstu !== '' && parseFloat(markstu).toFixed(2) > total_mark) {
            err = 1;
            errormsg = 'Student mark exceeds the total mark!';
            return;
        }
    });

    if (err == 1)
    {
        $('#Total_Mark').val($('#Total_Mark').attr('placeholder'))
        $('#messageBoxModal .modal-title').html('Error');
        $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-danger fade in">' + errormsg + '</div>');
        $('#messageBoxModal').modal('show');
        $('.loading_totmark').addClass('hide')
    } else {
        $.ajax({
            type: "POST",
            url: commonSettings.assignmentAjaxurl + "/updateAssTestTotMark",
            data: {
                test_id: assignmentSettings.selectedAssignment,
                total_mark: total_mark
            },
            success: function (res) {
                if (res) {

                    $('.done_totmark').removeClass('hide')
                    $('.loading_totmark').addClass('hide')
                }

            }
        });
    }
};

assignmentSettings.updateAssignmentMark = function (mark, id)
{
    var total_mark = parseInt($('#Total_Mark').val());
    var number_pattren = /^[0-9.]*$/;

    if (!number_pattren.test(mark))
    {
        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Please enter valid numeric only.</span>');
        $('#messageBoxModal').modal('show');
        return false;
    }
    else if ($('#Total_Mark').val().length == 0) {
        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Please Enter Total Mark First.</span>');
        $('#messageBoxModal').modal('show');
        return false;
    }
    else if (total_mark < 0) {
        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Total Mark Cannot Be Negative.</span>');
        $('#messageBoxModal').modal('show');
        return false;
    }
    else if (mark < 0) {
        $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
        $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Entered Mark Cannot Be Negative!.</span>');
        $('#messageBoxModal').modal('show');
        return false;
    }
    else if (mark <= total_mark)
    {
        $('#Total_Mark').attr("disabled", "disabled");
        $('.done_' + id).addClass('hide')
        $('.loading_' + id).removeClass('hide')
        $.ajax({
            type: "POST",
            url: commonSettings.assignmentAjaxurl + "/updateAssignmentMark",
            data: {
                test_id: assignmentSettings.selectedAssignment,
                student_id: id,
                mark: mark,
                total_mark: total_mark
            },
            dataType: "JSON",
            success: function (res) {

                console.log(res)
                if (res.type == 'success') {
                    $('.done_' + id).removeClass('hide')
                    $('.loading_' + id).addClass('hide')

                } else {
                    $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
                    $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">' + res.message + '</span>');
                    $('#messageBoxModal').modal('show');
                    return false;
                }

            }
        });
        $('#div_' + id).html("Mark updated");
        $('#div_' + id).css("color", "green")
    }
    else
    {
        $('#div_' + id).html("Exceeds Total Mark. Re-Enter correct mark");
        $('#div_' + id).css("color", "red")
    }
};

assignmentSettings.assignmenthelpLegend = function () {
    $('.msgBox-title').html('Help text');
    $('.msgBox-body').html($('.assignmenthelpLegend').html());
    $('#messageBoxModal').modal({
        backdrop: 'static',
        keyboard: false
    });
};


assignmentSettings.storeAssignment = function (assignment_id) {


    var studentData = {};

    var data = new FormData();
    data.append('upload_file_name', $("#assign_" + assignment_id)[0].files[0]);
    data.append('assignment_id', assignment_id);
    if (!($("#assign_" + assignment_id)[0].files[0]))
    {
        alert('Please select a file!');
    }
    else
    {
        studentData.fileform = data;
        //console.log($("#assign")[0].files[0]);
        $.ajax({
            type: "POST",
            url: commonSettings.assignmentAjaxurl + '/storeAssignment',
            processData: false,
            contentType: false,
            data: data,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        studentprofileSettings.loadAssignments(res['stuid']);

//                        location.reload();
                    });

                }
            }
        });
    }

}
