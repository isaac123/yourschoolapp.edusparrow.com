var viewexamSettings = {};

viewexamSettings.viewmainexam = function() {

    var ac_yr = $("#academic_year_name").val();
    viewexamSettings.acd_id = ac_yr;

    $.ajax({
        type: "POST",
        url: "viewMainExamByAcdYear",
        data: {
            academic_year_name: ac_yr
        },
        success: function(res) {
            if (res) {
                $('#viewMainExamByAcdYear').html(res);
                $('#viewMainExamByAcdYear').removeClass('hide');
            }

        }
    });
};

viewexamSettings.viewMainExamByClass = function(val){
    if(val != -1)
    {      
        viewexamSettings.classID = val;
        $.ajax({
            type: "POST",
            url: "viewMainExamByClass",
            data: {
                acd_id: viewexamSettings.acd_id,
                classID: viewexamSettings.classID
            },
            success: function(res) {
                if (res) {
                    $('#viewMainExamByAcdYear').html(res);
                    $('#viewMainExamByAcdYear').removeClass('hide');
                }

            }
        });   
    }
    else
    {
        viewexamSettings.viewmainexam();
    }    
}

viewexamSettings.loadSection = function(sval) {
    console.log(sval);
    viewexamSettings.classID = sval;
    $.ajax({
        type: "POST",
        url: "viewSectionByClass",
        data: {
            classId: sval
        },
        success: function(res) {
            if (res) {
                $('#subjTeachDet').html(res);
                $('#subjTeachDet').removeClass('hide');
                $('#subjDet').addClass('hide')
            }

        }
    });

};

viewexamSettings.viewClassTestTable = function(val)
{
    viewexamSettings.subDivVal = val;
    $.ajax({
        type: "POST",
        url: "viewClassTestTable",
        data: {
            class_id: viewexamSettings.classID,
            subDivVal: val
        },
        success: function(res) {
            if (res) {
                //alert(res);
                //$('#mainDiv').append(res);
                $('#subjDet').html(res);
                $('#subjDet').removeClass('hide');
            }

        }
    });    
};

viewexamSettings.viewClassTestTableSubjects = function(val)
{
    if(val != -1)
    {
        viewexamSettings.subject_id = val;
        $.ajax({
            type: "POST",
            url: "viewClassTestTableSubjects",
            data: {
                class_id: viewexamSettings.classID,
                subDivVal: viewexamSettings.subDivVal,
                subject_id: val
            },
            success: function(res) {
                if (res) {
                    //alert(res);
                    //$('#mainDiv').append(res);
                    $('#subjDet').html(res);
                    $('#subjDet').removeClass('hide')
                }

            }
        });   
    }
    else
    {
        viewexamSettings.viewClassTestTable(viewexamSettings.subDivVal);
    }
};

viewexamSettings.loadSubjects = function(val) {
    viewexamSettings.subDivVal = val;
    $.ajax({
        type: "POST",
        url: "viewSubjectsByClass",
        data: {
            class_id: viewexamSettings.classID,
            subDivVal: val
        },
        success: function(res) {
            if (res) {
                $('#subjDet').html(res);
                $('#subjDet').removeClass('hide')
            }

        }
    });

};



