var reportSettings = {};

reportSettings.getStudentReports = function () {

//   $.ajax({
//        type: "POST",
//        url: commonSettings.reportAjaxurl + "/getStudentsReport",
//        data:{limit:500},
//        dataType: "JSON",
//        success: function (res) {
//            commonSettings.studentListByLoggedUser['stu_ids'] = res.stu_ids;
//            console.log(res);
//        }
//    });
    var studata = {};
    var subdivval = [];

    $("#studentProfileSearchForm").find('input,select,textarea').each(function () {

        if ($(this).attr('name') == 'subDivVal')
        {
            if ($(this).val() != 0)
            {
                //i++;
                subdivval.push($(this).val());
            }
        }
        else
        {
            studata.emptySearch = 0;
            studata[$(this).attr('name')] = $.trim($(this).val());
        }

    });
    studata['subDivVal'] = subdivval;
    studata['limit'] = 500;

    console.log(studata);

    event.preventDefault();
    event.stopPropagation();

    var newForm = $('<form>', {
        'action': commonSettings.reportAjaxurl + "/getStudentsReport",
        'target': '_blank',
        'id': 'studentListForm',
        'method': 'POST'
    });
    newForm.append($('<input>', {
        'name': 'studata',
        'value': JSON.stringify(studata),
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};

reportSettings.getStaffReports = function () {

    var staffdata = {};

    $("#staffSearchForm").find('input,select,textarea').each(function () {
        staffdata.emptySearch = 0;
        staffdata[$(this).attr('name')] = $.trim($(this).val());
    });
    staffdata['limit'] = 500;
    event.preventDefault();
    event.stopPropagation();

    console.log(staffdata);

    var newForm = $('<form>', {
        'action': commonSettings.reportAjaxurl + "/getStaffsReport",
        'target': '_blank',
        'id': 'staffListForm',
        'method': 'POST'
    });
    newForm.append($('<input>', {
        'name': 'stfdata',
        'value': JSON.stringify(staffdata),
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};


reportSettings.getSmsReports = function (fileid) {

    var smsdata = {};

    smsdata['fileid'] = fileid;
    event.preventDefault();
    event.stopPropagation();

    console.log(smsdata);

    var newForm = $('<form>', {
        'action': commonSettings.reportAjaxurl + "/getSmsReport",
        'target': '_blank',
        'id': 'getSmsReportForm',
        'method': 'POST'
    });
    newForm.append($('<input>', {
        'name': 'smsdata',
        'value': JSON.stringify(smsdata),
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};

reportSettings.getGropupSmsReports = function (fileid) {

    var smsdata = {};

    smsdata['fileid'] = fileid;
    event.preventDefault();
    event.stopPropagation();

    console.log(smsdata);

    var newForm = $('<form>', {
        'action': commonSettings.reportAjaxurl + "/getGropupSmsReports",
        'target': '_blank',
        'id': 'getSmsReportForm',
        'method': 'POST'
    });
    newForm.append($('<input>', {
        'name': 'smsdata',
        'value': JSON.stringify(smsdata),
        'type': 'hidden'
    }));
    newForm.appendTo("body").submit();
};


reportSettings.getTransportStudentsReports = function () {


    var transprtSearchData = {};

    var err = 0;
    var error = '';

    $("#transportStuSearchForm").find('input,select,textarea').each(function () {


        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n";

            } else {
                transprtSearchData[$(this).attr('name')] = $.trim($(this).val());
            }
        } else
        {
            transprtSearchData[$(this).attr('name')] = $.trim($(this).val());
        }
    });
   transprtSearchData['limit'] = 500;
   
    event.preventDefault();
    event.stopPropagation();
    console.log(transprtSearchData);
    
    if (err === 0) {
        var newForm = $('<form>', {
            'action': commonSettings.reportAjaxurl + "/getTransportStudentsReports",
            'target': '_blank',
            'id': 'getTransprtReportForm',
            'method': 'POST'
        });
        newForm.append($('<input>', {
            'name': 'trnsprtnode',
            'value': JSON.stringify(transprtSearchData),
            'type': 'hidden'
        }));
        newForm.appendTo("body").submit();
    }
    else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBoxModal').modal('show');
        err = 0;
        return false;
    }



};

reportSettings.getFeeReports = function () {


    var feeSearchData = {};

    var err = 0;

      $("#StuFeeReportForm").find('input,select,textarea').each(function () {
        feeSearchData[$(this).attr('name')] = $.trim($(this).val());
    });
   feeSearchData['limit'] = 500;
   
    event.preventDefault();
    event.stopPropagation();
    console.log(feeSearchData);
    
   
        var newForm = $('<form>', {
            'action': commonSettings.reportAjaxurl + "/getFeeStudentsReports",
            'target': '_blank',
            'id': 'getFeeReportForm',
            'method': 'POST'
        });
        newForm.append($('<input>', {
            'name': 'feeSearchData',
            'value': JSON.stringify(feeSearchData),
            'type': 'hidden'
        }));
        newForm.appendTo("body").submit();
    

};
