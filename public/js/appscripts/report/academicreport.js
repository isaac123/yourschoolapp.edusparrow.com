var academicReportSettings = {};
academicReportSettings.loadActivityList = function (masterid) {
    $('#actDiv').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.reportAjaxurl + "/getActivityList",
        data: {
            masterid: masterid
        },
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('#actDiv').html(res);
            }
        }
    });
};

academicReportSettings.loadAttendance = function (params) {

    $('.sub-panel').getNiceScroll().remove();
    $('#attpanelbody').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.reportAjaxurl + "/loadMonthlyReport",
        data: params,
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('#attpanelbody').html(res);
            }
        }
    });
};


academicReportSettings.loadDailyReport = function (ctrl) {
    $('.sub-panel').getNiceScroll().remove();
    var month = $(ctrl).attr('month');
    var year = $(ctrl).attr('year');
    var reptyp = $(ctrl).attr('reptyp');
    var masterid = $(ctrl).attr('masterid');
    $('#attpanelbody').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.reportAjaxurl + "/loadDailyReport",
        dataType: 'html',
        data: {
            masterid: masterid,
            reptyp: reptyp,
            month: month,
            year: year
        },
        success: function (res) {
            if (res) {
                $('#attpanelbody').html(res);
            }
        }
    });
};

academicReportSettings.attendanceMonReportPrint = function (params) {
    var newForm = $('<form>', {
        'action': commonSettings.reportAjaxurl + "/attendanceMonReportPrint",
        'target': '_blank',
        'id': 'attendanceMonReportPrint',
        'method': 'POST'
    })
    for (var key in params) {
//        console.log(key +':'+params[key])
        if (params.hasOwnProperty(key)) {
            newForm.append($('<input>', {
                'name': key,
                'value': params[key],
                'type': 'hidden'
            }));
        }
    }
    newForm.appendTo("body").submit();
};
academicReportSettings.attendanceDlyReportPrint = function (params) {
    var newForm = $('<form>', {
        'action': commonSettings.reportAjaxurl + "/attendanceDailyReportPrint",
        'target': '_blank',
        'id': 'attendanceDailyReportPrint',
        'method': 'POST'
    })
    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            newForm.append($('<input>', {
                'name': key,
                'value': params[key],
                'type': 'hidden'
            }));
        }
    }
    newForm.appendTo("body").submit();
};

academicReportSettings.loadExamById = function (params) {
    $('.sub-panel').getNiceScroll().remove();
    $('.todo-checkbox-xam').prop('checked', false);
    $('#todo-checkbox-xam-' + params.examid).prop('checked', true);
    $('#exampanelbody').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.reportAjaxurl + "/loadExamReport",
        data: params,
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('#exampanelbody').html(res);
            }
        }
    });
};
academicReportSettings.loadTestBySubject = function (params) {

    $('.sub-panel').getNiceScroll().remove();
    $('.todo-checkbox-clstst').prop('checked', false);
    $('#todo-checkbox-clstst-' + params.subjid).prop('checked', true);
    $('#clststpanelbody').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.reportAjaxurl + "/loadClassTestReport",
        data: params,
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('#clststpanelbody').html(res);
            }
        }
    });
};
academicReportSettings.loadAssBySubject = function (params) {

    $('.sub-panel').getNiceScroll().remove();
    $('.todo-checkbox-ass').prop('checked', false);
    $('#todo-checkbox-ass-' + params.subjid).prop('checked', true);
    $('#asspanelbody').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.reportAjaxurl + "/loadAssignmentReport",
        data: params,
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('#asspanelbody').html(res);
            }
        }
    });
};
academicReportSettings.loadClassRating = function (params) {

    $('.sub-panel').getNiceScroll().remove();
    $('.todo-checkbox-clsrat').prop('checked', false);
    $('#todo-checkbox-clsrat-' + params.catid).prop('checked', true);
    $('#clsratpanelbody').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.reportAjaxurl + "/loadClassRatingReport",
        data: params,
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('#clsratpanelbody').html(res);
            }
        }
    });
};
academicReportSettings.loadSubjectRating = function (params) {

    $('.sub-panel').getNiceScroll().remove();
    var err = 0, error = '';
    $('#subjratpanelbody').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $('.todo-checkbox-ratsubj:checked').each(function () {
        params.subjid = JSON.parse($(this).attr('params')).subjid;
        $('.todo-checkbox-ratsubj').prop('checked', false);
        $('#todo-checkbox-ratsubj-' + params.subjid).prop('checked', true);
    })
    $('.todo-checkbox-subjrat:checked').each(function () {
        params.catid = JSON.parse($(this).attr('params')).catid;
        $('.todo-checkbox-subjrat').prop('checked', false);
        $('#todo-checkbox-subjrat-' + params.catid).prop('checked', true);
    })
    if (!params.catid) {
        error += "Please Select atleast one category\n";
        err = 1;
    } else if (!params.subjid) {
        error += "Please Select atleast one subject\n";
        err = 1;
    }
    console.log(params)
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.reportAjaxurl + "/loadSubjectRatingReport",
            data: params,
            dataType: 'html',
            success: function (res) {
                if (res) {
                    $('#subjratpanelbody').html(res);
                }
            }
        });
    } else {
        $('#subjratpanelbody').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        err = 0;
    }
};

academicReportSettings.loadHomeWorksBySubject = function (params) {

    $('.sub-panel').getNiceScroll().remove();
    $('.todo-checkbox-homework').prop('checked', false);
    $('#todo-checkbox-homework-' + params.subjid).prop('checked', true);
    $('#homewrkpanelbody').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
    $.ajax({
        type: "POST",
        url: commonSettings.reportAjaxurl + "/loadHomeWorksBySubject",
        data: params,
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('#homewrkpanelbody').html(res);
            }
        }
    });
};
