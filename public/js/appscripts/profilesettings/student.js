var studentProfileSettings = {};

studentProfileSettings.admit_student = 0;
$(function () {

    $('.label-switch1').on('switch-change', function (e, data) {
        var $element = $(data.el),
                value = data.value;
        if (value) {
            studentProfileSettings.admit_student = 1;
        } else {

            studentProfileSettings.admit_student = 0;
        }
        studentProfileSettings.updateSettingsValue(studentProfileSettings.admit_student);
       // console.log(studentProfileSettings.admit_student);
    });
    $('.label-switch').on('switch-change', function (e, data) {
        var $element = $(data.el),
                value = data.value;
        studentProfileSettings.updateMandatory($element)
    });
    $('.hideSwitch').on('switch-change', function (e, data) {
        var $element = $(data.el),
                value = data.value;
        studentProfileSettings.updateHideorShow($element)
    });
    
    
});

studentProfileSettings.updateSettingsValue = function(admit_val){
    
    var settingValue = {};
    settingValue.admit_student = admit_val;
    $.ajax({
        type: "POST",
        url: commonSettings.profileSettingsAjaxurl + "/updateSettingsValue",
        data: settingValue,
        dataType: "JSON",
        success: function (res) {
            if (res['type'] == 'error') {
                console.log(res['message'])
                return false;
            }
            else if (res['type'] == 'success') {
                $.gritter.add({
                    // (string | mandatory) the heading of the notification
                    title: 'Profile Settings Changed!',
                    // (string | mandatory) the text inside the notification
                    text: res['message']
                });
            }

            setTimeout(function () {
                studentCycleSettings.loadProfilesettings();
              
            }, 1000);


        }
    });
};


studentProfileSettings.updateMandatory = function (ctrl) {
    var profileSettingData = {};
    profileSettingData.itemID = $(ctrl).closest('tr').attr('itemid');
    profileSettingData.value = '0';
    profileSettingData.userType = $(ctrl).closest('tr').attr('userType');
    if ($(ctrl).is(':checked') == true) {
        profileSettingData.value = '1';
    }
    console.log(profileSettingData)
    $.ajax({
        type: "POST",
        url: commonSettings.profileSettingsAjaxurl + "/updateMandatory",
        data: profileSettingData,
        dataType: "JSON",
        success: function (res) {
            if (res['type'] == 'error') {
                console.log(res['message'])
                return false;
            }
            else if (res['type'] == 'success') {
                $.gritter.add({
                    // (string | mandatory) the heading of the notification
                    title: 'Profile Settings Changed!',
                    // (string | mandatory) the text inside the notification
                    text: res['message']
                });
            }

            setTimeout(function () {

                if (studentCycleSettings.input != '')
                {
                    studentCycleSettings.loadProfilesettings();
                    //studentCycleSettings.input = '';
                }

                if (staffCycleSettings.input != '')
                {
                    staffCycleSettings.loadProfilesettings();
                    //staffCycleSettings.input = '';
                }
                //window.location.reload();
            }, 1000);


        }
    });
}
studentProfileSettings.updateHideorShow = function (ctrl) {
    var profileSettingData = {};
    profileSettingData.itemID = $(ctrl).closest('tr').attr('itemid');
    profileSettingData.value = '0';
    profileSettingData.userType = $(ctrl).closest('tr').attr('userType');
    if ($(ctrl).is(':checked') == true) {
        profileSettingData.value = '1';
    }
    console.log(profileSettingData)
    $.ajax({
        type: "POST",
        url: commonSettings.profileSettingsAjaxurl + "/updateHideorShow",
        data: profileSettingData,
        dataType: "JSON",
        success: function (res) {
            if (res['type'] == 'error') {
                console.log(res['message'])
                return false;
            }
            else if (res['type'] == 'success') {
                $.gritter.add({
                    // (string | mandatory) the heading of the notification
                    title: 'Profile Settings Changed!',
                    // (string | mandatory) the text inside the notification
                    text: res['message']
                });
            }

            setTimeout(function () {
                if (studentCycleSettings.input != '')
                {
                    studentCycleSettings.loadProfilesettings();
                    //  studentCycleSettings.input = '';
                }

                if (staffCycleSettings.input != '')
                {
                    staffCycleSettings.loadProfilesettings();
                    //  staffCycleSettings.input = '';
                }
                //window.location.reload();
            }, 1000);


        }
    });
}