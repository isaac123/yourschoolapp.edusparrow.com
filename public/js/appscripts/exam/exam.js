var examSettings = {};
examSettings.stuCombiData = {};
examSettings.loadspacetree = 0;
examSettings.cycleID = 0;
examSettings.groupClsTechID = 0;
examSettings.getExamByCycle = function (sval) {

    if (sval == 0)
    {
        swal("Error", "Data is missing", "error");
        return;
    }
    examSettings.cycleID = sval;
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/getExamByCycle",
        data: {
            cycleID: sval
        },
        success: function (res) {
            if (res) {
                $('#subjDet').html(res);
                $('#subjDet').removeClass('hide')
            }
        }
    });

};

//$('#formModal').on('shown.bs.modal', function (e) {
//console.log('Exam')
//    if (examSettings.loadspacetree == 1 )
//    {
//        $.ajax({
//            type: "POST",
//            url: commonSettings.assigningAjaxurl + "/getTree",
//            data: {cycle: examSettings.cycle},
//            dataType: "JSON",
//            success: function (res) {
//                examSettings.spacetree = res;
//                if (examSettings.multiselectspacetree)
//                {
//                    multiselectinittree(examSettings.spacetree, examSettings.node_id, examSettings.node_name);
//
//                }
//                else
//                {
//                    inittree(examSettings.spacetree, examSettings.node_id, examSettings.node_name);
//                }
//            }
//        });
//    }
//    examSettings.loadspacetree = 0;
//
//});

//examSettings.loadSpaceTreeForAllSubject = function (cycle, node_id, node_name) {
//    spacetreeutilsSettings.cycle = cycle;
//    spacetreeutilsSettings.node_id = node_id;
//    spacetreeutilsSettings.node_name = node_name;
//    spacetreeutilsSettings.loadspacetree = 1;
//    spacetreeutilsSettings.multiselectspacetree = 0;
//    $.ajax({
//        type: "POST",
//        url: commonSettings.examAjaxurl + "/spacetree",
//        dataType: "html",
//        success: function (res) {
//            $('#msg_ok').unbind('click').click();
//            $('#confirm_close').unbind('click').click();
//            $('.formBox-title').html('Choose a Node');
//            $('.formBox-body').html(res);
//            $('#formModal').modal();
//
//            $('#confirm_save').unbind('click').click(function () {
//                $('#confirm_close').click();
//                var id = '"' + $('#' + node_id).val() + '"';
//
//
//                $('#returned_node_nameall').html($('#' + node_name).val());
//                var app = "<a href='javascript:void(0)' class='' onclick='examSettings.createExam(" + id + ")' title='Add Subject'>" +
//                        "<i class='fa fa-plus activity-icon bg-red' style='margin-top: 6px;'></i></a>"
//                $('#returned_node_nameall').append(app);
//                $('#added_category').removeClass('hide');
//            });
//        }
//    });
//};

examSettings.createExam = function (node_id) {

    examSettings.classID = node_id;
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/addExamForm",
        data: {
            classID: examSettings.classID
        },
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('.modal-title').html('Add Exam');
                $('.modal-body').html(res);
//                $('#classID').val(examSettings.classID)
                //$('#modalForm').click();
                $('#formModal').modal('show');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    examSettings.addExam();
                });
            }
        }
    });


};

examSettings.editExam = function (id) {

    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/editExamForm",
        data: {
            examid: id
        },
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('.modal-title').html('Edit Exam');
                $('.modal-body').html(res);
//                $('#classID').val(examSettings.classID)
                //$('#modalForm').click();
                $('#formModal').modal('show');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    examSettings.addExam();
                });
            }
        }
    });


};
examSettings.addExam = function () {
    var examData = {};

    $('#addExamForm').find('input,select,textarea').each(function () {
        if ($(this).is(':checkbox')) {
            examData[$(this).attr('name')] = $(this).is(':checked');
        } else if ($(this).is('input:radio')) {
            if ($(this).is(':checked'))
                examData[$(this).attr('name')] = $.trim($(this).val());
        } else {
            examData[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/addExam",
        data: examData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                var lblcls = res.type == 'error' ? 'danger' : 'success';
                $('#messageBoxModal .modal-title').html(res.type);
                $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == "success") {
                        examSettings.getExamByCycle(examSettings.cycleID)
                    } else {
                        $('#formModal').modal('show')
                    }

                });
//                assignSettings.getSubjectsByCycle(assignSettings.cycleID)
            }
        }
    });

};

examSettings.loadMainExLnk = function () {
    if ($('#link_check_box').is(':checked')) {
        var examData = {};
        $('#addExamForm').find('input,select').each(function () {
            if ($(this).is(':checkbox')) {
                examData[$(this).attr('name')] = $(this).is(':checked');
            } else if ($(this).is('input:radio')) {
                if ($(this).is(':checked'))
                    examData[$(this).attr('name')] = $.trim($(this).val());
            } else {
                examData[$(this).attr('name')] = $.trim($(this).val());
            }
        });
        console.log(examData)
        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/loadExamByClass",
            data: examData,
            success: function (res) {
                if (res) {
                    $('#mainexamlink').removeClass('hide');
                    $('#mainexamlink').html(res)
                }
            }
        });
    } else {
        $('#mainexamlink').html(' ');
    }

};
examSettings.addmainexam = function (linked) {
    var error = '';
    var enteredOf = $('#weightage').val()
    var enterable = $('#availWeightage').html()
    var diff = parseInt(enterable) - parseInt(enteredOf);
    if ($('#exam_text').val().length == 0)
    {
        error += '<div class="alert alert-block alert-danger fade in">Please Enter the Main Exam Name</div><br>';
    }
    else if ($('#mark_record_start').val().length == 0) {
        error += '<div class="alert alert-block alert-danger fade in">Please enter mark recording start date</div>';
    }
    else if ($('#mark_record_end').val().length == 0) {
        error += '<div class="alert alert-block alert-danger fade in">Please enter mark recording end date</div>';
    }
    else if ((new Date($('#mark_record_end').val()) < new Date($('#mark_record_start').val()))) {
        error += '<div class="alert alert-block alert-danger fade in">End date cannot be lesser than start date</div>';
    }
    else if ((linked == 1) && ($('#weightage').val() == 0))
    {
        //Exam is linked to another main exam. so evaluate few more inputs

        error += '<div class="alert alert-block alert-danger fade in">Enter Weightage to the selected exam</div>';
    }

    if (error) {

        $('#messageBoxModal .msgBox-title').html('Message');
        $('#messageBoxModal .msgBox-body').html(error);
        $('#messageBoxModal').modal({
            backdrop: 'static',
            keyboard: false
        });

        $('#msg_ok').unbind('click').click(function () {
            $('#formModal').modal('show')
        });
    } else
    {
        var examData = {};
        /*examData['linked'] = linked;
         if (linked == 1)
         {
         examData['mainexam'] = $('#mainexam option:selected').text();
         }*/
        $('#addMainExamForm').find('input,select').each(function () {
            if ($(this).is(':checkbox')) {
                examData[$(this).attr('name')] = $(this).is(':checked');
            } else if ($(this).is('input:radio')) {
                if ($(this).is(':checked'))
                    examData[$(this).attr('name')] = $.trim($(this).val());
            } else {
                examData[$(this).attr('name')] = $.trim($(this).val());
            }
        });
        console.log(examData)

        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/add_main_exam",
            data: examData,
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    $('#messageBoxModal .msgBox-title').html('Message');
                    $('#messageBoxModal .msgBox-body').html(res.message);
                    $('#messageBoxModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });

                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == "success") {

                            examSettings.loadBackExams()
                        } else {
                            $('#formModal').modal('show')
                        }

                    });

                }
            }
        });
    }

};

examSettings.linkmainexamtomainexam = function () {

    if ($('#link_check_box').is(':checked')) {
        examSettings.loadAvailWeight($('#mainexam'))
        $('#mainexamlink').removeClass('hide');
        $('#exam_add').addClass('hide');
    }
    else
    {
        $('#mainexamlink').removeClass('hide');
        $('#exam_add').removeClass('hide');
    }

};

examSettings.addmainexambyclassloadSubjects = function (ctrl) {
    if (ctrl != 0)
    {
        var formname = ctrl.id.split('_');
        console.log(formname)
        var subdivData = {};
        $("#addMainExamByClass_" + formname[1]).find('input,select').each(function () {
            subdivData[$(this).attr('name')] = $.trim($(this).val());
        });
        $('#addMainExamForm').find('input').each(function () {
            subdivData[$(this).attr('name')] = $.trim($(this).val());
        });

        subdivData[formname[0]] = subdivData[ctrl.id];
        subdivData["formname"] = formname[1];
        console.log(subdivData);

        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/addMainExamLoadSubjects",
            data: subdivData,
            success: function (res) {
                if (res) {
                    $('#showsubjects_' + formname[1]).html(res);
                    $('#showsubjects_' + formname[1]).removeClass('hide')
                }

            }
        });
    } else
    {
        $('#showmainexams_' + formname[1]).addClass('hide')
    }
};

examSettings.hidetextbox = function (ctrl) {
    if ($('#link_main_exam').is(':checked')) {
        $("#linkmain").removeClass('hide');
        $("#linkweight").removeClass('hide');
    }
    else {
        $("#linkmain").addClass('hide');
        $("#linkweight").addClass('hide');
    }
}

examSettings.loadmainexamlink = function (ctrl) {
    if (ctrl != -1)
    {
        var formname = ctrl.id.split('_');
        console.log(formname)
        var subdivData = {};
        $("#addMainExamByClass_" + formname[1]).find('input,select').each(function () {
            subdivData[$(this).attr('name')] = $.trim($(this).val());
        });
        $('#addMainExamForm').find('input').each(function () {
            subdivData[$(this).attr('name')] = $.trim($(this).val());
        });

        subdivData[formname[0]] = subdivData[ctrl.id];
        subdivData["formname"] = formname[1];
        console.log(subdivData);

        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/addMainExamLink",
            data: subdivData,
            success: function (res) {
                if (res) {
                    $('#showmainexams_' + formname[1]).html(res);
                    $('#showmainexams_' + formname[1]).removeClass('hide')
                }

            }
        });
    }
};

examSettings.addmainexambyclass = function (ctrl) {

    if ($('#exam_text').val().length == 0)
    {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Please Enter the Main Exam Name</div>');
        $('#messageBox').click();
    }
    else if ($('#mark_record_start').val().length == 0) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Please enter mark recording start date</div>');
        $('#messageBox').click();
    }
    else if ($('#mark_record_end').val().length == 0) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Please enter mark recording end date</div>');
        $('#messageBox').click();
    }
    else if (($('#mark_record_end').val() < $('#mark_record_start').val())) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">End date cannot be lesser than start date</div>');
        $('#messageBox').click();
    }

    else if ((($('#link_main_exam').is(':checked'))) && ($('#linkweightage').val() == 0))
    {
        //Exam is linked to another main exam. so evaluate few more inputs

        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Enter Weightage to the selected exam</div>');
        $('#messageBox').click();

    }
    else
    {
        var formname = ctrl.id.split('_');
        console.log(formname)
        var subdivData = {};
        $("#addMainExamByClass_" + formname[1]).find('input,select').each(function () {
            subdivData[$(this).attr('name')] = $.trim($(this).val());
        });
        $('#addMainExamForm').find('input').each(function () {
            subdivData[$(this).attr('name')] = $.trim($(this).val());
        });

        subdivData["formname"] = formname[1];

        if ($("#link_main_exam").is(':checked')) {
            subdivData["linked"] = 1;
        }
        else {
            subdivData["linked"] = 0;
        }

        console.log(subdivData);

        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/addMainExamByClass",
            data: subdivData,
            success: function (res) {
                if (res) {
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        window.location.reload();
                    });


                }

            }
        });
    }
};

examSettings.loadSubdivisionSubjects = function (sval) {
    console.log(sval);
    $(".btn-round").addClass('inactive');
    $("#span_" + sval).removeClass('inactive');

    if (sval != 0) {
        examSettings.classID = sval;
        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/loadSubdivisionSubjects",
            data: {
                classId: sval
            },
            success: function (res) {
                if (res) {
                    $('#subjTeachDet').html(res);
                    $('#subjTeachDet').removeClass('hide');
                    $('#subjDet').addClass('hide');
                    $('#loadmainexam').addClass('hide');
                }

            }
        });
    } else {
        $('#subjTeachDet').addClass('hide');
        $('#subjDet').addClass('hide');
        $('#loadmainexam').addClass('hide');
    }

};

examSettings.loadSection = function (sval) {
    console.log(sval);
    $(".mini-stat-icon").addClass('inactive');
    $("#span_" + sval).removeClass('inactive');

    if (sval != 0) {
        examSettings.classID = sval;
        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/getSectionByClass",
            data: {
                classId: sval
            },
            success: function (res) {
                if (res) {
                    $('#subjTeachDet').html(res);
                    $('#subjTeachDet').removeClass('hide');
                    $('#subjDet').addClass('hide');
                    $('#loadmainexam').addClass('hide');
                }

            }
        });
    } else {
        $('#subjTeachDet').addClass('hide');
        $('#subjDet').addClass('hide');
        $('#loadmainexam').addClass('hide');
    }

};

examSettings.loadSubjects = function (val) {

    if (val != 0) {
        examSettings.subDivVal = val;
        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/getSubjectsByClass",
            data: {
                class_id: examSettings.classID,
                subDivVal: val
            },
            success: function (res) {
                if (res) {

                    $('#subjDet').html(res);
                    $('#subjDet').removeClass('hide');
                    $('#loadmainexam').addClass('hide');
                }

            }
        });
    } else {
        $('#subjDet').addClass('hide');
        $('#loadmainexam').addClass('hide');
    }

};

examSettings.loadClassTestForm = function (ctrl) {

//    $('.combispan').addClass('inactive')
//    $(ctrl).toggleClass('inactive')

    $('.combispan').addClass('label-default').removeClass('label-warning')
    if ($(ctrl).hasClass('combispan'))
        $(ctrl).removeClass('label-default').addClass('label-warning')
    examSettings.subject_masterid = $(ctrl).attr('id').split('_')[1];

    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/getMainExamBySubjects",
        data: {
            subject_masterid: examSettings.subject_masterid
        },
        success: function (res) {
            if (res) {
                $('#loadmainexam').html(res);
                $('#loadmainexam').removeClass('hide')
            }

            examSettings.viewClassTests(examSettings.subject_masterid);
        }
    });
};
examSettings.activeTest = function (ctrl) {
    console.log($(ctrl).attr("id").split("_"))
//    $('.combispan').addClass('inactive')
//    $(ctrl).toggleClass('inactive')
    $('.combispan').addClass('label-default').removeClass('label-warning')
    if ($(ctrl).hasClass('combispan'))
        $(ctrl).removeClass('label-default').addClass('label-warning')
    var masterId = $(ctrl).attr("id").split("_");
    examSettings.viewClassTests(masterId[1])
};
examSettings.viewClassTests = function (val) {
    examSettings.subject_masterid = val;
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/loadoverallclasstests",
        data:
                {
                    subject_master_id: val
                },
        success: function (res) {
            if (res) {
                $('#viewclasstest').html(res);
                $('#viewclasstest').removeClass('hide');
            }

        }
    });
}

examSettings.removeClassTest = function (testId) {
    $('#ConfirmModal .cnfBox-title').html('Confirm Delete');
    $('#ConfirmModal .cnfBox-body').html('<div class="alert alert-block alert-warning fade in">Do you confirm to delete this test? </div>');
    $('#ConfirmModal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#confirm_yes').unbind('click').click(function () {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/checkLinkageForTest",
            data: {testId: testId},
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    if (res['type'] == "success") {
                        examSettings.deleteTest(testId);
                    } else {
                        var lblcls = res.type == 'error' ? 'danger' : (res.type == 'success' ? 'success' : 'warning');
                        $('#ConfirmModal .modal-title').html("Confirm Delete");
                        $('#ConfirmModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                        $('#ConfirmModal').modal('show');
                        $('#confirm_yes').unbind('click').click(function () {
                            $('#confirm_no').click()
                            examSettings.deleteTest(testId);

                        });
                    }

                }

            }
        });

    });
}

examSettings.hidemainexamlink = function () {
    if ($('#linkmainexam').is(':checked')) {
        $('#mainexamlink').removeClass('hide');
        $('#add_class_test').addClass('hide');
    }
    else
    {
        $('#mainexamlink').addClass('hide');
        $('#add_class_test').removeClass('hide');
    }

};

examSettings.evaluate_class_test_form = function (e) {
    var datavalues = {};

    var err = 0;
    var errmsg = "";

    $('#addNewClassTestForm').find('input,select,textarea').each(function () {

        var splitval = $(this).attr('name').split('_');
        if (splitval[0] === 'aggregate') {
            if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
                console.log('in')
                datavalues[$(this).attr('name')] = $.trim($(this).val());
            } else {
                err = 1;
                errmsg = errmsg + "Please Select " + $(this).attr('name') + '<br/>';
                console.log('err')
            }
        }
    });

    if ($("#classtestname").val().length == 0)
    {
        err = 1;
        errmsg = errmsg + "Please Enter the Class Test Name" + '<br/>';

    }

    //console.log($("#classtestname").val().length);
    if (err == 1)
    {

        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#formModal').modal('show')
        });

    }
    else
    {
        datavalues['test_name'] = $("#classtestname").val();
        datavalues['subject_masterid'] = examSettings.subject_masterid;
        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/addTestByClassLinkedMainExam",
            dataType: "JSON",
            data: datavalues,
            success: function (res) {
                if (res) {

                    $('#messageBoxModal .msgBox-title').html('Message');
                    $('#messageBoxModal .msgBox-body').html(res['message']);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function (event) {
                        console.log(res['type'])
                        if (res['type'] == "success") {
                            //window.location.reload();
                            $("#classtestname").val("");
                            examSettings.viewClassTests(examSettings.subject_masterid);
                        } else {
                            console.log('yes')
                            $('#formModal').modal('show')
                        }
                    });
                }
            }
        });
    }
};

examSettings.evaluate_class_test_linked_exam_form = function (e) {

    var datavalues = {};
    var err = 0;
    var errmsg = "";

    $('#addNewClassTestForm').find('input,select,textarea').each(function () {

        var splitval = $(this).attr('name').split('_');
        if (splitval[0] === 'aggregate') {
            if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
                console.log('in')
                datavalues[$(this).attr('name')] = $.trim($(this).val());
            } else {
                err = 1;
                errmsg = errmsg + "Please Select " + $(this).attr('name') + '<br/>';
                console.log('err')
            }
        }
    });


    //console.log($("#classtestname").val().length);
    if ($("#classtestname").val().length == 0)
    {
        err = 1;
        errmsg = errmsg + "Please Enter the Class Test Name" + '<br/>';

    }
    else if ($("#mainexam").val().length == 0)
    {
        err = 1;
        errmsg = errmsg + "Select Main Exam from the List" + '<br/>';
    }
    else if ($("#weightage").val().length == 0)
    {
        err = 1;
        errmsg = errmsg + "Weightage can't be empty or invalid" + '<br/>';
    }
    else if ((($("#weightage").val()) < 1))
    {
        err = 1;
        errmsg = errmsg + "Weightage cannot be lesser than 1" + '<br/>';
    }

    if (err == 1)
    {

        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#formModal').modal('show')
        });

//        $('#messageBox .modal-title').html('Message');
//        $('#messageBox .modal-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
//        $('#messageBoxModal').modal('show');
//        $('#msg_ok').unbind('click').click(function () {
//            $('#formModal').modal('show')
//        });
    }
    else
    {
        datavalues['test_name'] = $("#classtestname").val();
        datavalues['subject_masterid'] = examSettings.subject_masterid;
        datavalues['mainexam_id'] = $("#mainexam").val();
        datavalues['weightage'] = $("#weightage").val();
        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/addTestByClassLinkedMainExam",
            data: datavalues,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('#messageBoxModal .msgBox-title').html('Message');
                    $('#messageBoxModal .msgBox-body').html(res['message']);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function (event) {
                        if (res['type'] == "success") {
                            //window.location.reload();
                            $("#classtestname").val("");
                            $("#mainexam").prop('defaultSelected');
                            $("#weightage").val("");
                            examSettings.viewClassTests(examSettings.subject_masterid);
                        } else {
                            $('#formModal').modal('show')
                        }
                    });

                }
                //examSettings.viewClassTests(examSettings.subject_masterid);

            }
        });

    }


};


examSettings.edit_hidemainexamlink = function () {
    if ($('#editlinkmainexam').is(':checked')) {
        $('#editmainexamlink').removeClass('hide');
        $('#edit_add_class_test').addClass('hide');
    }
    else
    {
        $('#editmainexamlink').addClass('hide');
        $('#edit_add_class_test').removeClass('hide');
    }

};

examSettings.edit_evaluate_class_test_form = function (val) {
    var datavalues = {};

    //console.log($("#classtestname").val().length);
    if ($("#editclasstestname").val().length == 0)
    {
        $('#messageBoxModal .modal-title').html('Message');
        $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-danger fade in">Please Enter the Class Test Name</div>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#formModal').modal('show')
        });
    }
    else
    {
        datavalues['test_name'] = $("#editclasstestname").val();
        datavalues['subject_masterid'] = examSettings.subject_masterid;
        datavalues['test_id'] = val;
        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/editTestByClassLinkedMainExam",
            dataType: "JSON",
            data: datavalues,
            success: function (res) {
                if (res) {

                    $('#messageBoxModal .msgBox-title').html('Message');
                    $('#messageBoxModal .msgBox-body').html(res['message']);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function (event) {
                        if (res['type'] == "success") {
                            //window.location.reload();
                            $("#editclasstestname").val("");
                            examSettings.viewClassTests(examSettings.subject_masterid);
                        } else {
                            $('#formModal').modal('show')
                        }
                    });
                }
                //examSettings.viewClassTests(examSettings.subject_masterid);
            }
        });
    }
};

examSettings.edit_evaluate_class_test_linked_exam_form = function (val) {

    var datavalues = {};
    err = 0;
    errmsg = "";


    //console.log($("#classtestname").val().length);
    if ($("#editclasstestname").val().length == 0)
    {
        err = 1;
        errmsg = errmsg + "Please Enter the Class Test Name" + '<br/>';

    }
    else if ($("#editmainexam").val().length == 0)
    {
        err = 1;
        errmsg = errmsg + "Select Main Exam from the List" + '<br/>';
    }
    else if ($("#editweightage").val().length == 0)
    {
        err = 1;
        errmsg = errmsg + "Weightage can't be empty or invalid" + '<br/>';
    }
    else if ((($("#editweightage").val()) < 1))
    {
        err = 1;
        errmsg = errmsg + "Weightage cannot be lesser than 1" + '<br/>';
    }

    if (err == 1)
    {
        $('#messageBoxModal messageBoxModal.modal-title').html('Message');
        $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
        $('#messageBoxModal').modal('show');
        $('#msg_ok').unbind('click').click(function () {
            $('#formModal').modal('show')
        });
    }
    else
    {
        datavalues['test_name'] = $("#editclasstestname").val();
        datavalues['subject_masterid'] = examSettings.subject_masterid;
        datavalues['mainexam_id'] = $("#editmainexam").val();
        datavalues['weightage'] = $("#editweightage").val();
        datavalues['test_id'] = val;
        $.ajax({
            type: "POST",
            url: commonSettings.examAjaxurl + "/editTestByClassLinkedMainExam",
            data: datavalues,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    $('#messageBoxModal .msgBox-title').html('Message');
                    $('#messageBoxModal .msgBox-body').html(res['message']);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function (event) {
                        if (res['type'] == "success") {
                            //window.location.reload();
                            $("#editclasstestname").val("");
                            $("#editmainexam").prop('defaultSelected');
                            $("#editweightage").val("");
                            examSettings.viewClassTests(examSettings.subject_masterid);
                        } else {
                            $('#formModal').modal('show')
                        }
                    });

                }

            }
        });

    }


};


examSettings.loadValues = function (ctrl) {
    var newForm = $('#addMainExamForm').append($('<input>', {
        'name': 'examId',
        'value': $(ctrl).val(),
        'type': 'hidden'
    }));
    document.addMainExamForm.action = commonSettings.examAjaxurl + '/addExam'
//    console.log(document.addMainExamForm)
    document.addMainExamForm.submit();
}
examSettings.loadAvailWeight = function (ctrl) {
    var linkname = $(ctrl).val()
    var enteredId = $('#exam_type_id').val()
    console.log($('#exam_type_id').val())
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/loadAvailWeight",
        data: {
            linkname: linkname,
            enteredId: enteredId
        },
        success: function (res) {
            if (res) {
                $('#availWeightage').html(parseInt(res))

            }

        }
    });
}
examSettings.validOutOf = function (ctrl) {
    var enteredOf = $(ctrl).val()
    var enterable = $('#availWeightage').html()
    var diff = parseInt(enterable) - parseInt(enteredOf);
    if (diff < 0) {

        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('Entered weightage of linked main exam exceeds the maximum weightage!');
        $('#messageBoxModal').modal('show');
        return false;
    }
    $('#availWeightage').html(diff)

};

examSettings.loadExams = function (ctrl) {
    var StuCombiData = {};
//    $('.combispan').addClass('inactive')
//    $(ctrl).toggleClass('inactive')

    $('.combispan').addClass('label-default').removeClass('label-warning')
    if ($(ctrl).hasClass('combispan'))
        $(ctrl).removeClass('label-default').addClass('label-warning')
    var currentclass = $(ctrl).attr('classname');
    StuCombiData.masterid = $(ctrl).attr('id').split('_')[1];
    StuCombiData.type = $(ctrl).attr('teacherType');
    StuCombiData.className = currentclass;

    examSettings.stuCombiData = StuCombiData;
    markSettings.stuCombiData = StuCombiData;

    examSettings.loadBackExams()
//    $.ajax({
//        type: "POST",
//        url: commonSettings.examAjaxurl + "/getExamList",
//        data: examSettings.stuCombiData,
//    }).done(function(resHtml) {
//        $("#stuExamList").removeClass('hide')
//        $("#stuMarkList").addClass('hide')
//        $("#stuExamList").html(resHtml);
//    });
};

examSettings.loadBackExams = function () {
    console.log(examSettings.stuCombiData)
    markSettings.stuCombiData['current_view'] = 1;
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/getExamList",
        data: examSettings.stuCombiData,
    }).done(function (resHtml) {
        $("#stuExamList").removeClass('hide')
        $("#stuMarkList").addClass('hide')
        $("#stuExamList").html(resHtml);
    });
}
examSettings.loadClassTestAddForm = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/getMainExamBySubjects",
        data: {
            subject_masterid: examSettings.subject_masterid
        },
        success: function (res) {
            if (res) {
                $('#formModal .formBox-title').html('Add New Test');
                $('#formModal .formBox-body').html(res);
                $('#formModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#formModal').modal('show')
                $('#confirm_save').unbind('click').click(function () {
                    $('#formModal').modal('hide')
                    if ($('#linkmainexam').is(':checked'))
                        examSettings.evaluate_class_test_linked_exam_form()
                    else
                        examSettings.evaluate_class_test_form()
                });
            }
        }
    });
};

examSettings.editClassTest = function (val) {

    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/editClassTest",
        data:
                {
                    test_id: val
                },
        success: function (res) {
            if (res) {
                $('#formModal .formBox-title').html('Edit Test');
                $('#formModal .formBox-body').html(res);
                $('#formModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#formModal').modal('show')
                $('#confirm_save').unbind('click').click(function () {
                    $('#formModal').modal('hide')
                    var testid = $('#test_id').val()
                    if ($('#editlinkmainexam').is(':checked'))
                        examSettings.edit_evaluate_class_test_linked_exam_form(testid)
                    else
                        examSettings.edit_evaluate_class_test_form(testid)
                });
            }
        }
    });
};
examSettings.loadExamAddForm = function () {

    console.log(examSettings.stuCombiData)
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/addExam",
        success: function (res) {

            console.log(examSettings.stuCombiData)
            if (res) {
                $('#formModal .formBox-title').html('Add New Exam');
                $('#formModal .formBox-body').html(res);
                $('#formModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#confirm_save').unbind('click').click(function () {
                    $('#formModal').modal('hide')
                    if ($('#link_check_box').is(':checked'))
                        examSettings.addmainexam(1)
                    else
                        examSettings.addmainexam(0)



                });
            }
        }
    });
};

examSettings.loadExamEditForm = function (examId) {

    console.log(examSettings.stuCombiData)
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/editExam",
        data: {examId: examId},
        success: function (res) {

            console.log(examSettings.stuCombiData)
            if (res) {
                $('#formModal .formBox-title').html('Edit Exam');
                $('#formModal .formBox-body').html(res);
                $('#formModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });

                examSettings.loadMainExLnk()
                $('#confirm_save').unbind('click').click(function () {
                    $('#formModal').modal('hide')
                    if ($('#link_check_box').is(':checked'))
                        examSettings.addmainexam(1)
                    else
                        examSettings.addmainexam(0)



                });
            }
        }
    });
};
examSettings.deleteExam = function (examId) {
    $('#ConfirmModal .cnfBox-title').html('Confirm Delete');
    $('#ConfirmModal .cnfBox-body').html('<div class="alert alert-block alert-warning fade in">Do you confirm to delete this exam?</div>');
    $('#ConfirmModal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#confirm_yes').unbind('click').click(function () {
        $('#ConfirmModal').modal('hide');
        examSettings.confirmDeleteExam(examId);
    });
};

examSettings.confirmDeleteExam = function (examId) {
    console.log('yes')
    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/confirmDeleteExam",
        data: {examId: examId},
        dataType: "JSON",
        success: function (res) {
            if (res) {

                console.log(res)
                var lblcls = res.type == 'error' ? 'danger' : 'success';
                $('#messageBoxModal .modal-title').html(res.type);
                $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == "success") {
                        examSettings.getExamByCycle(examSettings.cycleID)
                    } else {
                        $('#formModal').modal('show')
                    }

                });

            }

        }
    });
};

examSettings.deleteTest = function (testId) {

    $.ajax({
        type: "POST",
        url: commonSettings.examAjaxurl + "/removeClassTest",
        dataType: "JSON",
        data:
                {
                    test_id: testId
                },
        success: function (res) {
            if (res) {
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function (event) {
                    if (res['type'] == "success") {
                        examSettings.viewClassTests(examSettings.subject_masterid);
                    }
                });
            }
        }
    });
};

examSettings.classshelpLegend = function () {
    $('.msgBox-title').html('Help text');
    $('.msgBox-body').html($('.classhelpLegend').html());
    $('#messageBoxModal').modal({
        backdrop: 'static',
        keyboard: false
    });
};

examSettings.mainexamhelpLegend = function () {
    $('.msgBox-title').html('Help text');
    $('.msgBox-body').html($('.mainexamhelpLegend').html());
    $('#messageBoxModal').modal({
        backdrop: 'static',
        keyboard: false
    });
};


