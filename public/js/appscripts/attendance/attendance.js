var attendanceSettings = {};
attendanceSettings.stuCombiData = {};
attendanceSettings.stuSessionData = {};
attendanceSettings.stfCombiData = {};
attendanceSettings.timeout;
attendanceSettings.loadStudentAttendance = function (stulist) {
    //console.log(stulist);
    attendanceSettings.stulist = stulist;
    var now = new Date();
    var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
    var timestamp = startOfDay / 1000;
    attendanceSettings.currentDate = timestamp;
    _loadStuAttDiv()
};

/*attendanceSettings.changeDate = function(currentDate, ctrl) {
 var UType = $(ctrl).attr('utype');
 var date = new Date(currentDate * 1000);
 console.log(UType)
 attendanceSettings.currentDate = currentDate;
 if (UType == 'student')
 _loadStuAttDiv();
 else if (UType == 'staff')
 _loadStfAttDiv();
 
 };*/

attendanceSettings.selectday = function () {

//    $(".dayattend-menu").toggleClass('open_attend-menu');
//    $('#daynav_btn').toggleClass('menu-expanded');
//    $('#daynav_btn').find('i').toggleClass('fa-pencil');
//    $('#daynav_btn').find('i').toggleClass('fa-plus');

};

attendanceSettings.selecthrs = function () {

    $(".hrsattend-menu").toggleClass('open_attend-menu');

};
attendanceSettings.selectattend = function (num) {

    $(".attend-menu_" + num).toggleClass('open_attend-menu');

};

function _loadStuAttDiv() {

    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/studentListFormat",
        data: {
            stulistJson: attendanceSettings.stulist,
            currentDate: attendanceSettings.currentDate,
        }
        ,
        success: function (res) {
            $("#stuResultSet").html(res);
        }
    });



}



//attendanceSettings.stfAttDiv = 
function _loadStfAttDiv() {
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/staffListFormat",
        data: {
            stfListJson: attendanceSettings.stfList,
            currentDate: attendanceSettings.currentDate,
        }
    }).done(function (resHtml) {
        $("#stfFormatList").html(resHtml);
    });
}
;


attendanceSettings.loadStaffAttendance = function (stfList) {
    //console.log(stulist);
    attendanceSettings.stfList = stfList;
    var now = new Date();
    var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0);
    var timestamp = startOfDay / 1000;
    attendanceSettings.currentDate = timestamp;
    _loadStfAttDiv()
};


attendanceSettings.getStudentList = function (ctrl) {
    var StuCombiData = {};
//    $('.combispan').addClass('inactive')
//    $(ctrl).toggleClass('inactive')
    $('.combispan').addClass('label-default').removeClass('label-warning')
    if ($(ctrl).hasClass('combispan'))
        $(ctrl).removeClass('label-default').addClass('label-warning')

    StuCombiData.masterid = $(ctrl).attr('id').split('_')[1];
    StuCombiData.type = $(ctrl).attr('teacherType');

    attendanceSettings.stuCombiData = StuCombiData;
    attendanceSettings.stuCombiData['currentDate'] = $(ctrl).attr('cdate');
    attendanceSettings.stuCombiData['UType'] = 'student'

    attendanceSettings.loadStudentData();
};
attendanceSettings.loadStudentData = function () {
    console.log(attendanceSettings.stuCombiData)
//    console.log(attendanceSettings.stfCombiData)
    attendanceSettings.stuCombiData['fromPage'] = 'academic';
//    $("#stuAttList").removeClass('hide')
//    $("#stuAttList").html('<div class= "col-md-8 pull-right" style="padding: 50px;"><i class="fa fa-spinner fa-spin fa-3x"></i></div><div class= "col-md-6"></div>')
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/classroomAttForStu",
        data: attendanceSettings.stuCombiData,
    }).done(function (resHtml) {
        $("#stuAttList").removeClass('hide')
        $("#stuAttList").html(resHtml);
    });
};


attendanceSettings.changeDate = function (ctrl) {
    var UType = $(ctrl).attr('utype');
    console.log(UType)

    if ($.isEmptyObject(attendanceSettings.stfCombiData)) {
        attendanceSettings.stuCombiData['currentDate'] = $(ctrl).val();
    } else {
        attendanceSettings.stfCombiData['currentDate'] = $(ctrl).val();
    }

    console.log(attendanceSettings.stuCombiData)
    console.log(attendanceSettings.stfCombiData)
    if (UType == 'student')
        attendanceSettings.loadStudentData();
    else if (UType == 'staff')
        attendanceSettings.loadStaffData();

};


attendanceSettings.addOrUpdateSlot = function (value) {
//    $(ctrl).next('.loading').removeClass('hide');
    console.log(attendanceSettings.stuCombiData)
    console.log(attendanceSettings.stfCombiData)
    var slotData = {};
    if ($.isEmptyObject(attendanceSettings.stfCombiData)) {
        attendanceSettings.stuCombiData['dayValue'] = value;
        slotData = attendanceSettings.stuCombiData
    } else {
        attendanceSettings.stfCombiData['dayValue'] = value;
        slotData = attendanceSettings.stfCombiData
    }

    console.log(slotData)
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/addOrUpdateSlot",
        data: slotData,
        dataType: 'JSON'
    }).done(function (resHtml) {
//        $(ctrl).next('.loading').addClass('hide');
        swal({
            title: resHtml['type'].charAt(0).toUpperCase() + resHtml['type'].substr(1).toLowerCase(),
            text: resHtml.message,
            type: resHtml['type'],
            confirmButtonText: 'OK!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.isEmptyObject(attendanceSettings.stfCombiData) ?
                                attendanceSettings.loadStudentData() :
                                attendanceSettings.loadStaffData();
                    }
                });

    });
};

attendanceSettings.addOrUpdateAttnValFull = function (ctrl) {
    $(ctrl).parent('nav').prev('span').toggleClass('menu-expanded');
    $(ctrl).parent('nav').toggleClass('open_attend-menu');
    var userids = [];
    var periodid = $(ctrl).attr('periodId');
    var attId = $(ctrl).attr('attId');

    $('#attenStuTable').find('tbody').find('td').each(function () {
        userids.push($(this).attr('userId'));
    })
    var attValData = {};
    if ($.isEmptyObject(attendanceSettings.stfCombiData)) {
        if ($.isEmptyObject(attendanceSettings.stuSessionData)) {
            attendanceSettings.stuCombiData['periods'] = $(ctrl).attr('periodId');
            attendanceSettings.stuCombiData['attVal'] = $(ctrl).attr('attId');
            attendanceSettings.stuCombiData['userIds'] = $.unique(userids);
            attValData = attendanceSettings.stuCombiData;
        } else {
            attendanceSettings.stuSessionData['periods'] = $(ctrl).attr('periodId');
            attendanceSettings.stuSessionData['attVal'] = $(ctrl).attr('attId');
            attendanceSettings.stuSessionData['userIds'] = $.unique(userids);
            attValData = attendanceSettings.stuSessionData;
        }
    } else {
        attendanceSettings.stfCombiData['periods'] = $(ctrl).attr('periodId');
        attendanceSettings.stfCombiData['attVal'] = $(ctrl).attr('attId');
        attendanceSettings.stfCombiData['userIds'] = $.unique(userids);
        attValData = attendanceSettings.stfCombiData;
    }
    console.log(attValData)
    $('.loading_' + periodid).removeClass('hide')
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/addOrUpdateAttnValFull",
        data: attValData,
        dataType: "JSON"
    }).done(function (resHtml) {
        console.log(resHtml)

        if (resHtml.type != 'error') {

            $('.done_' + periodid).removeClass('hide')
            $('.loading_' + periodid).addClass('hide')
            clearTimeout(attendanceSettings.timeout);
            attendanceSettings.timeout = setTimeout(function () {
                $.isEmptyObject(attendanceSettings.stfCombiData) ?
                        ($.isEmptyObject(attendanceSettings.stuSessionData) ?
                                attendanceSettings.loadStudentData() : attendanceSettings.loadStudentDataBySession())
                        : attendanceSettings.loadStaffData();
            }, 30);
        } else {
            swal({
                title: resHtml['type'].charAt(0).toUpperCase() + resHtml['type'].substr(1).toLowerCase(),
                text: resHtml.message,
                type: resHtml['type'],
                confirmButtonText: 'OK!',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.isEmptyObject(attendanceSettings.stfCombiData) ?
                                    ($.isEmptyObject(attendanceSettings.stuSessionData) ?
                                            attendanceSettings.loadStudentData() : attendanceSettings.loadStudentDataBySession())
                                    : attendanceSettings.loadStaffData();
                        }
                    });
        }
    });

}
attendanceSettings.selectedperiods = function (periodval) {
    $('.loading_' + periodval).removeClass('hide')
    $('#periodval_' + periodval).toggleClass('attend-taken');
    $('#periodval_' + periodval).toggleClass('not-taken');
    /*var periods = new Array();
     $('.periodVal').each(function() {
     if ($(this).hasClass('attend-taken'))
     {
     var hr = $(this).attr('id').split('_')
     periods.push(hr[1])
     }
     });
     var energy = periods.join()*/
    console.log(attendanceSettings.stuCombiData)
    console.log(attendanceSettings.stfCombiData)
    var periodData = {};
    if ($.isEmptyObject(attendanceSettings.stfCombiData)) {
        attendanceSettings.stuCombiData['periods'] = periodval;
        attendanceSettings.stuCombiData['period_value'] = $('#periodval_' + periodval).hasClass('attend-taken') ? '1' : '0';
        periodData = attendanceSettings.stuCombiData;
    } else {
        attendanceSettings.stfCombiData['periods'] = periodval;
        attendanceSettings.stfCombiData['period_value'] = $('#periodval_' + periodval).hasClass('attend-taken') ? '1' : '0';
        periodData = attendanceSettings.stfCombiData;
    }

    console.log(periodData)
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/addOrUpdatePeriod",
        data: periodData,
        dataType: 'JSON'
    }).done(function (resHtml) {
        console.log(resHtml)
        if (resHtml.type != 'error') {

            $('.done_' + periodval).removeClass('hide')
            $('.loading_' + periodval).addClass('hide')
            clearTimeout(attendanceSettings.timeout);
            attendanceSettings.timeout = setTimeout(function () {
                $.isEmptyObject(attendanceSettings.stfCombiData) ?
                        attendanceSettings.loadStudentData() :
                        attendanceSettings.loadStaffData();
            }, 50);
        } else {
            swal({
                title: resHtml['type'].charAt(0).toUpperCase() + resHtml['type'].substr(1).toLowerCase(),
                text: resHtml.message,
                type: resHtml['type'],
                confirmButtonText: 'OK!',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.isEmptyObject(attendanceSettings.stfCombiData) ?
                                    attendanceSettings.loadStudentData() :
                                    attendanceSettings.loadStaffData();
                        }
                    });
        }
    });

};


attendanceSettings.makeAttendance = function (statusData) {

    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/makeAttendance",
        data: statusData,
        dataType: "JSON"
    }).done(function (res) {
        console.log(res)
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html(res['message']);
        $('#messageBox').click();
        $('#msg_ok').unbind('click').click(function () {
            if (res['type'] == 'error') {
                $('#msg_ok').unbind('click').click();
                return false;
            }
            else if (res['type'] == 'success') {
                window.location.reload();
            }
        });

    });
};
attendanceSettings.addOrUpdateAttnVal = function (ctrl) {
    var attValData = {};
//    console.log($(ctrl).parent().prev('span').html())
    $(ctrl).parent('nav').prev('span').toggleClass('menu-expanded');
    $(ctrl).parent('nav').toggleClass('open_attend-menu');
    var attValData = {};
    if ($.isEmptyObject(attendanceSettings.stfCombiData)) {
        if ($.isEmptyObject(attendanceSettings.stuSessionData)) {
            attendanceSettings.stuCombiData['periods'] = $(ctrl).attr('periodId');
            attendanceSettings.stuCombiData['attVal'] = $(ctrl).attr('attId');
            attendanceSettings.stuCombiData['userId'] = $(ctrl).attr('userId');
            attValData = attendanceSettings.stuCombiData;
        } else {
            attendanceSettings.stuSessionData['periods'] = $(ctrl).attr('periodId');
            attendanceSettings.stuSessionData['attVal'] = $(ctrl).attr('attId');
            attendanceSettings.stuSessionData['userId'] = $(ctrl).attr('userId');
            attValData = attendanceSettings.stuSessionData;
        }
    } else {
        attendanceSettings.stfCombiData['periods'] = $(ctrl).attr('periodId');
        attendanceSettings.stfCombiData['attVal'] = $(ctrl).attr('attId');
        attendanceSettings.stfCombiData['userId'] = $(ctrl).attr('userId');
        attValData = attendanceSettings.stfCombiData;
    }

//    var period_value = $('#periodval_' + $(ctrl).attr('periodId')).hasClass('attend-taken') ? '1' : '0';
//    if (period_value == 0) {
//        swal("Cancelled", "Please select  Period in the Header", "error");
//        return false;
//    }

    var currentObj = attValData;
    $('.loading_' + currentObj.userId + '_' + currentObj.periods).removeClass('hide')

    console.log(attendanceSettings.stuSessionData)
    console.log(attendanceSettings.stfCombiData)
    console.log(attendanceSettings.stuCombiData)
    console.log('.loading_' + currentObj.userId + '_' + currentObj.periods)
//    $(ctrl).next('.loading').removeClass('hide');
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/addOrUpdateAttnVal",
        data: attValData,
        dataType: "JSON"
    }).done(function (resHtml) {
        console.log(resHtml)

        if (resHtml.type != 'error') {

            $('.done_' + currentObj.userId + '_' + currentObj.periods).removeClass('hide')
            $('.loading_' + currentObj.userId + '_' + currentObj.periods).addClass('hide')
            clearTimeout(attendanceSettings.timeout);
            attendanceSettings.timeout = setTimeout(function () {
                $.isEmptyObject(attendanceSettings.stfCombiData) ?
                        ($.isEmptyObject(attendanceSettings.stuSessionData) ?
                                attendanceSettings.loadStudentData() : attendanceSettings.loadStudentDataBySession())
                        : attendanceSettings.loadStaffData();
            }, 30);
        } else {
            swal({
                title: resHtml['type'].charAt(0).toUpperCase() + resHtml['type'].substr(1).toLowerCase(),
                text: resHtml.message,
                type: resHtml['type'],
                confirmButtonText: 'OK!',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.isEmptyObject(attendanceSettings.stfCombiData) ?
                                    ($.isEmptyObject(attendanceSettings.stuSessionData) ?
                                            attendanceSettings.loadStudentData() : attendanceSettings.loadStudentDataBySession())
                                    : attendanceSettings.loadStaffData();
                        }
                    });
        }
    });
};

attendanceSettings.helpLegend = function () {
    $('.msgBox-title').html('Help text');
    $('.msgBox-body').html($('.helpLegend').html());
    $('#messageBoxModal').modal({
        backdrop: 'static',
        keyboard: false
    });
};


attendanceSettings.getStaffList = function (ctrl) {
    var StfCombiData = {};
    $('.combispan').addClass('inactive')
    $(ctrl).toggleClass('inactive')
    StfCombiData.masterid = $(ctrl).attr('id').split('_')[1];

    attendanceSettings.stfCombiData = StfCombiData;
    attendanceSettings.stfCombiData['currentDate'] = $(ctrl).attr('cdate');
    attendanceSettings.stfCombiData['UType'] = 'staff'

    attendanceSettings.loadStaffData();
};

attendanceSettings.loadStaffData = function () {

    console.log(attendanceSettings.stuCombiData)
    console.log(attendanceSettings.stfCombiData)
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/classroomAttForStf",
        data: attendanceSettings.stfCombiData,
    }).done(function (resHtml) {
        $("#stfAttList").removeClass('hide')
        $("#stfAttList").html(resHtml);
    });
};

attendanceSettings.loadattSett = function () {
    $('#classactivity').empty()
    $.ajax({
        type: "POST",
        url: commonSettings.attendancesettingsAjaxurl + "/staffAttendanceSettings",
        dataType: "html",
        success: function (res) {
            $('#attendancesettingsDiv').html(res);
            $('#attendancesettingsDiv').removeClass('hide')

            attendanceSettings.loadAttendanceSettings();
        }
    });
};
attendanceSettings.loadAttendanceSettings = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.stfratingAjaxurl + "/loadAttendanceSettings",
        dataType: "html",
        success: function (res) {
            $('#attendance-frequency').html(res);
            $('#attendance-frequency').removeClass('hide')
        }
    });
};
attendanceSettings.loadStaffatt = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/staffAttendance",
        dataType: "html",
        success: function (res) {
            $('#attendancediv').html(res);
            $('#attendancediv').removeClass('hide')
        }
    });
};
attendanceSettings.loadStaffleaves = function () {
    $.ajax({
        type: "POST",
        url: commonSettings.leaveAjaxurl + "/manageMyLeaves",
        dataType: "html",
        success: function (res) {
            $('#leavediv').html(res);
            $('#leavediv').removeClass('hide')
        }
    });
};
attendanceSettings.loadStaffAttReport = function () {
    $('#staffAttndiv')
            .find('.att-panel')
            .find('.panel-body')
            .html('<span class="col-sm-8 pull-right help-block"><i class="fa fa-spin fa-spinner" ></i> Loading please wait...</span>')
    $('#staffAttndiv')
            .find('.att-panel').next().addClass('hide')
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/loadStaffAttReport",
        dataType: "html",
        success: function (res) {
            $('#staffAttndiv').html(res);
            $('#staffAttndiv').removeClass('hide')
        }
    });
};
attendanceSettings.loadSubtree = function (ctrl) {
    $(ctrl).parent().parent().next('.subtreediv').removeClass('hide').html(' <div class="form-group">  <span class="col-sm-8 pull-right help-block"><i class="fa fa-spin fa-spinner" ></i> Loading please wait...</span>  </div>')
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/loadSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};
attendanceSettings.viewStaffAttendance = function (ctl) {
    var params = {}, err = 0, error = '';
    $(ctl).addClass('hide');
    $("#staffAttendanceReport").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n</br>";

            } else {
                params[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }

    });
    if (err == 0) {
        $('#staffAttDiv').removeClass('hide');
        $('#staffAttDiv').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...')
        $.ajax({
            type: "POST",
            url: commonSettings.attendanceAjaxurl + "/viewStaffAttendance",
            data: params,
            success: function (res) {
                $(ctl).removeClass('hide')
                if (res) {
                    $('#staffAttDiv').html(res);
                }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
    }
};
attendanceSettings.loadStfDailyAttendance = function (ctrl) {
    var month = $(ctrl).attr('month');
    var year = $(ctrl).attr('year');
    var reptyp = $(ctrl).attr('reptyp');
    var aggregateids = $(ctrl).attr('aggregateids');
    $('#downloadStaffMnthlyAtt').addClass('hide');
    $('#downloadStaffDailyAtt').removeClass('hide');
    $('#goback').removeClass('hide');
    $('#monthlystaffAtt').addClass('hide');
    $('#stfDailyAttBody').removeClass('hide');
    $('#stfDailyAttBody').html('<i class="fa fa-spin fa-spinner" style="  padding: 90px; "></i> Loading please wait...');
    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/loadStfDailyAttendance",
        dataType: 'html',
        data: {
            aggregateids: aggregateids,
            reptyp: reptyp,
            month: month,
            year: year
        },
        success: function (res) {
            if (res) {
                $('#stfDailyAttBody').html(res);
            }
        }
    });
};

attendanceSettings.getStudentListBySession = function (ctrl) {

    attendanceSettings.stuSessionData['masterid'] = $(ctrl).attr('id').split('_')[1];
    attendanceSettings.stuSessionData['currentDate'] = $(ctrl).attr('cdate');
    attendanceSettings.loadStudentDataBySession();
}
attendanceSettings.loadStudentDataBySession = function (ctrl) {

    $.ajax({
        type: "POST",
        url: commonSettings.attendanceAjaxurl + "/subjectAttForStu",
        data: attendanceSettings.stuSessionData,
    }).done(function (resHtml) {
        $("#stuAttList").removeClass('hide')
        $("#stuAttList").html(resHtml);
    });
};

attendanceSettings.changeDateBySession = function (ctrl) {
    attendanceSettings.stuSessionData['currentDate'] = $(ctrl).val();
    attendanceSettings.loadStudentDataBySession();

};
