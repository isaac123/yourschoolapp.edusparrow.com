$('.select-menu').on('click', function() {
    console.log('clicked')
    $(this).next('nav').toggleClass('open_attend-menu');
    $(this).toggleClass('menu-expanded');
    $(this).find('i').toggleClass('fa-pencil');
    $(this).find('i').toggleClass('fa-plus');
});


$('.form_date').datetimepicker({
    format: 'dd-mm-yyyy',
    language: 'en',
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0,
    endDate: '+0d',
});