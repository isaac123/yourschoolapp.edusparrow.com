var ratingSettings = {};
ratingSettings.loadspacetree = 0;
ratingSettings.cycleID = 0;
ratingSettings.groupClsTechID = 0;
ratingSettings.getRatingsByCycle = function (sval) {

    if (sval == 0)
    {
        swal("Error", "Data is missing", "error");
        return;
    }
    ratingSettings.cycleID = sval;
    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/getRatingsByCycle",
        data: {
            cycleID: sval
        },
        success: function (res) {
            if (res) {
                $('#subjDet').html(res);
                $('#subjDet').removeClass('hide')
            }
        }
    });

};

//$('#formModal').on('shown.bs.modal', function (e) {
//console.log('Rat')
//    if (ratingSettings.loadspacetree == 1)
//    {
//        $.ajax({
//            type: "POST",
//            url: commonSettings.assigningAjaxurl + "/getTree",
//            data: {cycle: ratingSettings.cycle},
//            dataType: "JSON",
//            success: function (res) {
//                ratingSettings.spacetree = res;
//                if (ratingSettings.multiselectspacetree)
//                {
//                    multiselectinittree(ratingSettings.spacetree, ratingSettings.node_id, ratingSettings.node_name);
//
//                }
//                else
//                {
//                    inittree(ratingSettings.spacetree, ratingSettings.node_id, ratingSettings.node_name);
//                }
//            }
//        });
//    }
//    ratingSettings.loadspacetree = 0;
//
//});
ratingSettings.loadSpaceTreeForAllSubject = function (cycle, node_id, node_name) {
    spacetreeutilsSettings.cycle = cycle;
    spacetreeutilsSettings.node_id = node_id;
    spacetreeutilsSettings.node_name = node_name;
    spacetreeutilsSettings.loadspacetree = 1;
    spacetreeutilsSettings.multiselectspacetree = 0;
    spacetreeutilsSettings.singleManNode = 1
    $.ajax({
        type: "POST",
        url: commonSettings.spacetreeutilsAjaxurl + "/spacetree",
        data: {cycle: cycle},
        dataType: "html",
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Choose a Node');
            $('.formBox-body').html(res);
            $('#formModal').modal();

            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                var id = '"' + $('#' + node_id).val() + '"';


                $('#returned_node_nameall').html($('#' + node_name).val());
                var app = "<a href='javascript:void(0)' class='' onclick='ratingSettings.createRating(" + id + ")' title='Add Rating'>" +
                        "<i class='fa fa-plus activity-icon bg-red' style='margin-top: 6px;'></i></a>"
                $('#returned_node_nameall').append(app);
                $('#added_category').removeClass('hide');
            });
        }
    });
};

ratingSettings.createRating = function (node_id) {

    ratingSettings.classID = node_id;
    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/addRatingForm",
        data: {
            classID: ratingSettings.classID
        },
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('.modal-title').html('Add Exam');
                $('.modal-body').html(res);
                $('#formModal').modal('show');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    ratingSettings.addRating();
                });
            }
        }
    });


};
ratingSettings.addRating = function () {
    var examData = {};

    $('#addRatingForm').find('input').each(function () {
        examData[$(this).attr('name')] = $.trim($(this).val());
    });
    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/addRating",
        data: examData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                var lblcls = res.type == 'error' ? 'danger' : 'success';
                $('#messageBoxModal .modal-title').html(res.type);
                $('#messageBoxModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == "success") {
                        ratingSettings.getRatingsByCycle(ratingSettings.cycleID)
                    } else {
                        $('#formModal').modal('show')
                    }

                });
            }
        }
    });

};

ratingSettings.editRating = function (id) {
    console.log('asd')
    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/editRatingForm",
        data: {
            ratid: id
        },
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('.modal-title').html('Add Exam');
                $('.modal-body').html(res);
                $('#formModal').modal('show');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    ratingSettings.addRating();
                });
            }
        }
    });


};
ratingSettings.processRadioButton = function (val) {

    if (val == 1) {
        $('#No').prop("checked", false);
        $('#class_div').addClass('hide');
    }
    else {
        $('#Yes').prop("checked", false);
        $('#class_div').removeClass('hide');
    }
};

ratingSettings.saveAssignRatings = function () {
    var error = 0;
    //Check one of the radio button is clicked
    if (!(($('#No').is(':checked')) || ($('#Yes').is(':checked')))) {


        error = 1;
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Tell us whether you would like to assign Rating to entire school or not!</div>');
        $('#messageBox').click();
    }
    //check whether radio button "No" is clicked, if so there should be class selected
    //
    else if (($('#No').is(':checked')) && ($("#divID").val() == 0)) {

        error = 1;
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Select Class</div>');
        $('#messageBox').click();

    }
    else if (($("#rating_name").val() == 0)) {

        error = 1;
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Please Enter A Name For Rating</div>');
        $('#messageBox').click();

    }
    else {
        if (error == 0) {
            //All inputs are validated and looks good

            $.ajax({
                type: "POST",
                url: commonSettings.ratingAjaxurl + "/saveAssignRatings",
                data: {
                    commonSetting: $('#Yes').is(':checked'),
                    classId: $("#divID").val(),
                    rating_name: $('#rating_name').val()
                },
                success: function (res) {
                    if (res) {
                        //$('.modal-title').html('Message');
                        //$('.modal-body').html(res);
                        //$('#messageBox').click();

                        $('.msgBox-title').html('Message');
                        $('.msgBox-body').html(res);
                        $('#messageBoxModal').modal('show');
                        $('#msg_ok').unbind('click').click(function () {
                            classroomAdminSettings.assignrating()
                        });
                    }

                }
            });
        }

    }

};

ratingSettings._assignrating = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/assignrating",
        success: function (res) {
            classroomAdminSettings.assignrating()
//            if (res) {
//                //  console.log(res);
//                //$('#mainDiv').append(res);
//                $('#').html(res);
//                $('#subjTechMatrix').removeClass('hide')
//            }

        }
    });

}

ratingSettings.viewassignrating = function (rating) {
    //alert(assignment);
    ratingSettings.rating = rating;
    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/viewAssignRating",
        data: {
            selectedrating: ratingSettings.rating,
            action: 'View'
        },
        success: function (res) {
            $('.msgBox-title').html('View Rating');
            $('.msgBox-body').html(res);
            $('#messageBox').click();

        }
    });
};

ratingSettings.editrating = function (rating) {
    //  console.log(assignment)
    var RatingData = {};
    ratingSettings.rating = rating;
    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/viewAssignRating",
        data: {
            selectedrating: ratingSettings.rating,
            action: 'Edit'
        },
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('Edit Rating');
            $('.formBox-body').html(res);
            $('#modalForm').click();
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                $("#viewAssignmentForm").find('input,select,textarea').each(function () {
                    RatingData[$(this).attr('name')] = $.trim($(this).val());
                });

                ratingSettings.saveRating(RatingData)
                //ratingSettings._assignrating();
            });

        }
    });

};

ratingSettings.saveRating = function (RatingData) {

    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/saveRating",
        data: RatingData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function () {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                        return false;
                    }
                    else if (res['type'] == 'success') {
                        ratingSettings._assignrating();
                    }
                });

            }
        }
    });
}

ratingSettings.deleterating = function (rating) {

    $('#confirm_close').unbind('click').click();
    $('#msg_ok').unbind('click').click();
    ratingSettings.rating = rating;
    $('.cnfBox-title').html('Delete Confirm');
    $('.cnfBox-body').html('Do you confirm to delete?');
    $('#confirmMsg').click();
    $('#confirm_yes').unbind('click').click(function () {
        $('#confirm_no').click();
        $.ajax({
            type: "POST",
            url: commonSettings.ratingAjaxurl + "/deleteRating",
            data: {
                rating: ratingSettings.rating,
            },
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res['message']);
                    $('#messageBox').click();
                    $('#msg_ok').unbind('click').click(function () {
                        if (res['type'] == 'error') {
                            $('#msg_ok').unbind('click').click();
                            return false;
                        }
                        else if (res['type'] == 'success') {
                            ratingSettings._assignrating();
                        }
                    });

                }
            }
        });
    });
}
ratingSettings.updateRatingCategory = function () {
    var i = 0;
    var name = {};
    var weightage = {};
    var input = {};
    var total_weightage = 0;
    var error = 0;
    var hidden_count = 0;
    var hidden = {};
    $(".category_name").each(function () {

        if ($(this).parent('div').parent('div').is(":visible"))
        {
            if ($(this).val().length > 0)
            {
                name[i++] = ($(this).val());
            } else {
                error = 1;
            }
        } else {


            hidden[hidden_count++] = ($(this).val());

        }
    })

    i = 0;

    if (!error) {
        $(".category_weightage").each(function () {
            if ($(this).parent('div').parent('div').is(":visible"))
            {
                if ($(this).val().length > 0)
                {
                    weightage[i] = parseInt(($(this).val()));
                    total_weightage = total_weightage + weightage[i];
                    i++;
                } else {
                    error = 2;
                }
            }
        })
    }
    if (!error) {
        if (total_weightage != 100)
        {
            error = 3;
        }
    }
    console.log(total_weightage)
    if (error == 1) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Category Name is missing</div>');
        $('#messageBox').click();
    }
    else if (error == 2) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Weightage is missing</div>');
        $('#messageBox').click();
    }
    else if (error == 3) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Sum of all the weightage values should be 100</div>');
        $('#messageBox').click();
    }
    else {

        for (j = 0; j < i; j++) {
            input[j] = new Array(name[j], weightage[j]);
        }
        input["count"] = i;
        input["hidden"] = hidden;
        input["hidden_count"] = hidden_count;

        //alert(JSON.stringify(input));

        $.ajax({
            type: "POST",
            url: commonSettings.ratingAjaxurl + "/updateRatingCategory",
            data: input,
            success: function (res) {
//                console.log(res)
                if (res) {
                    if (res.type == 'error') {
                        $('.modal-title').html('Message');
                        $('.modal-body').html(res.message);
                        $('#messageBox').click();
                        return false;
                    } else {
                        $('#ratValDiv').removeClass('hide');
                        $('#ratValDiv').html(res);
                        $('#c-slide').carousel('next');
                    }
                }
            }
        });
        //alert(JSON.stringify(input));
        //alert(JSON.stringify(weightage));
    }

};

ratingSettings.createRatingLevel = function (id) {
    var wid = "#wrapper_" + id;

    $(wid).append('<div class = "col-md-12" style=" padding: 10px; ">\n\
<div class = "col-md-3"></div>\n\
<div class = "col-md-3"><input type="text" name="rating_name" class = "ratingname form-control" placeholder = "Rating Name"/></div>\n\
<div class = "col-md-3"><input type="number" name="rating_weightage" class = "ratingweightage form-control" placeholder = "0" min ="0" \n\
max="100"/></div>\n\
<div class="col-md-1 remove_field_val" style="cursor: pointer;" title="Remove" \n\
onclick = "ratingSettings.removeRatingLevel(' + "#wrapper_" + id + ')"><i class="fa fa-times-circle fa-2x "></i></div></div>');

};



ratingSettings.updateRatingLevel = function () {

    var total_count = 0;
    var error = 0;
    var split_id = "";
    var input = {};
    var hidden_count = 0;
    var hidden = {};
    var names = {};

    $(".wrapper_class_val").each(function () {
//        alert(($(this).val()));
        if (!error) {
            var name = {};
            var weightage = {};

            var i = 0;
            split_id = $(this).find('.categoryname').val();


            $(this).find('.ratingname').each(function () {
//                    alert(($(this).is(":visible")));
                if ($(this).is(":visible"))
                {
                    if ($(this).val().length > 0)
                    {
                        name[i++] = ($(this).val());
                    } else {
                        error = 1;
                    }
                } else {
                    hidden[hidden_count] = $(this).val();
                    names[hidden_count] = split_id;
//                    alert(hidden[hidden_count]);
//                    alert(names[hidden_count]);
                    hidden_count++;
                }
            });

            i = 0;

            if (!error) {
                $(this).find('.ratingweightage').each(function () {
                    if ($(this).is(":visible"))
                    {
                        if ($(this).val().length > 0)
                        {
                            var weight = parseInt(($(this).val()));
                            if (weight >= 0 && weight <= 100)
                            {
                                weightage[i++] = parseInt(($(this).val()));
                            }
                            else
                            {
                                error = 3;
                            }
                        } else {
                            error = 2;
                        }
                    }
                });
            }


            if (error == 1) {
                $('.modal-title').html('Message');
                $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Rating Name is Missing for Category: ' + split_id + '</div>');
                $('#messageBox').click();
            }
            else if (error == 2) {
                $('.modal-title').html('Message');
                $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Rating Weightage is Missing for Category: ' + split_id + '</div>');
                $('#messageBox').click();
            }
            else if (error == 3) {
                $('.modal-title').html('Message');
                $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Rating Weightage is not between 0 to 100 for Category: ' + split_id + '</div>');
                $('#messageBox').click();
            }
            else {

                for (j = total_count; j < total_count + i; j++) {
                    input[j] = new Array(split_id, name[j - total_count], weightage[j - total_count]);
                }
                total_count = total_count + i;
                //alert(total_count);
                //alert(JSON.stringify(input));
            }
        }
    });

    if (!error) {

        input["total_count"] = total_count;
        input["hidden"] = hidden;
        input["names"] = names;
        input["hidden_count"] = hidden_count;
        //alert(hidden_count);
        //input["hidden"] = hidden;
        //input["hidden_count"] = hidden_count;

        //alert(JSON.stringify(input));

        $.ajax({
            type: "POST",
            url: commonSettings.ratingAjaxurl + "/updateRatingLevel",
            data: input,
            success: function (res) {
                if (res) {

                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
//                        window.location.reload();

                        classroomAdminSettings.ratingSettings()
                    });
                }
            }
        });
    }

};

ratingSettings.loadRatingName = function (ctrl) {

//    $('.combispan').addClass('inactive')
//    $(ctrl).toggleClass('inactive')
    $('.combispan').addClass('label-default').removeClass('label-warning')
    if ($(ctrl).hasClass('combispan'))
        $(ctrl).removeClass('label-default').addClass('label-warning')

    ratingSettings.subject_masterid = $(ctrl).attr('id').split('_')[1];
    var return_uri = $(ctrl).attr('return_uri');

    $.ajax({
        type: "POST",
        url: commonSettings.ratingAjaxurl + "/loadRatingName",
        data: {
            subject_masterid: ratingSettings.subject_masterid,
            return_uri: return_uri
        },
        success: function (res) {
            if (res) {
//                console.log(res)
//                $('.loadRatingName').html('');
//                $('.loadStudenRating').html('');
                $('.loadRatingName').html(res);
                $('.loadRatingName').removeClass('hide');
                $('.loadStudenRating').addClass('hide');
            }
        }
    });

};

ratingSettings.loadStudentsRating = function (val) {
    if (val != -1)
    {
        var input = {};
        var err = 0;
        var errmsg = '';
        $("#addRatingForm").find('input,select,textarea').each(function () {
            if ($(this).parent().prev('label').hasClass('mandatory')) {
                if ($.trim($(this).val()) == '') {
                    err = 1;
                    errmsg += $(this).attr('title') + " is required!<br>";

                } else {
                    input[$(this).attr('name')] = $.trim($(this).val());
                }
            }

        });
        console.log(input)
        if (err == 0) {
            input['subject_masterid'] = ratingSettings.subject_masterid;
            ratingSettings.rating_id = input.rating_id = val;
            ratingSettings.rating_name = $("#ratingid option:selected").text();
            $return_uri = input.return_uri = $("#return_uri").val();

            console.log(input)
            $.ajax({
                type: "POST",
                url: commonSettings.ratingAjaxurl + "/loadStudentsRating",
                data: input,
                success: function (res) {
                    if (res) {
                        $('.loadStudenRating').html(res);
                        $('.loadStudenRating').removeClass('hide');
                    }

                }
            });
        } else {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + errmsg + '</div>');
            $('#messageBox').click();
            err = 0;
            return false;
        }
    } else {
        $('.loadStudenRating').addClass('hide');
    }
};

ratingSettings.editStudentRating = function (stud_id, ctrl, category_count) {

    var row_id = $(ctrl).closest('tr').index() + 1;
    console.log(row_id)
    var table = document.getElementById("stuRatng");
    var row = table.rows[row_id];

    for (var i = 0; i < category_count; i++) {
        var e = row.getElementsByTagName("select")[i];
        $('#' + e.id).prop('disabled', false);
    }

    $(ctrl).closest('div').html("<a href='javascript:;' onclick='ratingSettings.submitStudentRating(" +
            stud_id + ",this," + category_count + ")'><span class='mini-stat-icon-action tar' title='Submit'><i class='fa fa-check'></i></span></a>");
    $(ctrl).closest('td').next('td').html("");
};

ratingSettings.submitStudentRating = function (stud_id, ctrl, category_count) {

    var row_id = $(ctrl).closest('tr').index() + 1;
    var table = document.getElementById("stuRatng");
    var row = table.rows[row_id];
    var error = 0;
    var input = {};

    $("#addRatingForm").find('input,select,textarea').each(function () {


        if ($.trim($(this).val()) == '' || $.trim($(this).val()) == '0') {
            err = 1;
            error += $(this).attr('name') + " is required!<br>";
        } else {
            input[$(this).attr('name')] = $.trim($(this).val());
        }


    });
    for (var i = 0; i < category_count; i++) {
        var e = row.getElementsByTagName("select")[i];
        var rating_level_id = e.options[e.selectedIndex].value;
        var split_id = e.id.split('_');
        var rating_category_id = split_id[0];

        if (rating_level_id == -1) {
            error = 1;
            $('.modal-title').html('Message');
            $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Rating is missing value.</div>');
            $('#messageBox').click();
            break;
        }
        else {
            input[i] = new Array(rating_category_id, rating_level_id);
        }
    }

    if (!error) {
        input['subject_masterid'] = ratingSettings.subject_masterid;
        input['rating_id'] = ratingSettings.rating_id;
        input['stud_id'] = stud_id;
        input['category_count'] = category_count;
        input['return_uri'] = $("#return_uri").val();
        console.log(input)


        $.ajax({
            type: "POST",
            url: commonSettings.ratingAjaxurl + "/updateStudentsRating",
            data: input,
            success: function (res) {
                if (res == 1) {
                    $(ctrl).closest('div').html("<a href='javascript:;' onclick='ratingSettings.editStudentRating(" + stud_id + ",this," + category_count + ")'><span class='mini-stat-icon-action pink' title='Edit'><i class='fa fa-edit'></i></span></a>");
                    $(ctrl).closest('td').next('td').html("Updated!");
                    $(ctrl).closest('td').next('td').css("color", "green");


                    for (var i = 0; i < category_count; i++) {
                        var e = row.getElementsByTagName("select")[i];
                        $('#' + e.id).prop('disabled', true);
                    }
                }

            }
        });
    }

};

ratingSettings.ratinghelpLegend = function () {
    $('.msgBox-title').html('Help text');
    $('.msgBox-body').html($('.ratinghelpLegend').html());
    $('#messageBoxModal').modal({
        backdrop: 'static',
        keyboard: false
    });
};
ratingSettings.addCatrow = function () {
    $('#input_wrapper_rat').append('<div class="form-group"><div class = "col-md-12">\n\
 <div class = "col-md-3"><input type="text" name="category_name" class = "category_name form-control" placeholder = "Category Name"/></div>\n\
    <div class = "col-md-3"><input type="number" name="category_weightage" class = "category_weightage form-control"\n\
 placeholder = "0" min ="0" max="100"/></div>\n\
    <div class = "col-md-2"><a href="#" class="remove_field"><i class="fa fa-trash-o fa-2x"></a></div></div></div>'); //add input box

};

ratingSettings.ClickHereToPrint = function () {
    try {
        document.getElementById('print').style.display = 'none';
        var oIframe = document.getElementById('ifrmPrint');
        var oContent = document.getElementById('divToPrint').innerHTML;
        var oDoc = (oIframe.contentWindow || oIframe.contentDocument);
        if (oDoc.document)
            oDoc = oDoc.document;
        oDoc.write("<html><head><title>title</title>");
        //oDoc.write(\"<link href=".$path." rel=stylesheet type=text/css  />\");
        oDoc.write("</head><body onload='this.focus(); this.print();'>");
        oDoc.write(oContent + "</body></html>");
        oDoc.close();
    }
    catch (e) {
        self.print();
    }
}
