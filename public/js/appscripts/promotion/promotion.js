var promotionSettings = {};
promotionSettings.applychanges = function() {
        var promotionData = {};
    $("#promotionSettings").find('input,select').each(function() {
        promotionData[$(this).attr('name')] = $.trim($(this).val());
    });

    $.ajax({
        type: "POST",
        url: commonSettings.promotionAjaxurl  + "/saveSettings",
        data: promotionData,
        dataType: "JSON",
        success: function(res) {
            $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function() {  
                    if (res['type'] == 'error'){
                        $('#msg_ok').unbind('click').click();    
                        return false;
                    }
                    else if (res['type'] == 'success'){
                         evaluationSettings.loadPromotionSettings();
                    }
                });

        }
    });
};


/*$(document).ready(function() {
    var classId = '1';
   promotionSettings.loadPromoPage(classId)
});*/

promotionSettings.loadPromoPage = function(classid) {	
    console.log("caled :"+classid)
    var params ={};
    params.classId = classid;    
    $.ajax({
        type: "POST",
        url: commonSettings.promotionAjaxurl  + "/classSection",
        data: params,
        dataType: "html",
        success: function(res) {
            $('#promoSection'+params.classId).append(res)  
                var tablectrl = '#promoClass'+params.classId;   
                console.log(tablectrl)
                promotionSettings.noOfRecordsToDisplay =10;    
                var  column = [ 1, 2,3];
                var sortingDisabled= [0];
      $(tablectrl).dataTable({
            "sDom": 'C<"clear">lrtip',
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": commonSettings.promotionAjaxurl  + "/promotionTable",
            "sPaginationType": "full_numbers",
            "iDisplayLength": promotionSettings.noOfRecordsToDisplay,
           // 'iDisplayStart':patientsapp.startIndex,
            "aoColumnDefs": [{
                "sDefaultContent": "",
                "aTargets": column
            }, {
                "bSortable": false,
                "sDefaultContent": "",
                "aTargets": sortingDisabled
            }],
           "aaSorting":  [[ 1, "desc" ]],
            "bLengthChange": false,
            "bInfo": false,
            "fnServerData": function (surl, aoData, l) {
            	for (var key in params) {
                    aoData.push({
                        "name": key,
                        "value": params[key]
                    });
                }
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: surl,
                    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                    data: aoData,
                    success: l
                });
            },
            "fnDrawCallback": function (o) {
                if (o._iRecordsDisplay <= promotionSettings.noOfRecordsToDisplay) {
                    $(tablectrl+'_paginate').hide();
                }
                
                var enableIds = "#publishres"+params.classId;
                $(".select_all_chkbx"+params.classId).unbind('click').on('click', function () {
                    console.log("clicked"+params.classId)
                    var ctrl = this;
                    if ($(ctrl).is(':checked') == false) {
                    console.log($(ctrl).is(':checked') )
                        $(tablectrl).find(".select_chkbx"+params.classId).prop("checked", false);
                    } else {
                    console.log($(ctrl).is(':checked') )
                        $(tablectrl).find(".select_chkbx"+params.classId).prop("checked", true);
                    }
                    if ($('.select_chkbx'+params.classId+':checked').length === 0) {
                    	$(enableIds).addClass("disabled");
                    	$(enableIds).attr('title', 'You must select one or more checkboxes to use this action');
                        $(tablectrl).find('.select_all_chkbx'+params.classId).attr("isselectall", "no");
                        $(tablectrl).find('.selectallmsg'+params.classId).remove();
                        $(ctrl).closest("table").find('.select_all_chkbx'+params.classId).prop("checked", false);
                    } else {
                    	$(enableIds).removeClass("disabled");
                    	$(enableIds).removeAttr('title');
                        var totalColumn = $(tablectrl).find('th').length;
                        var totalRecords = o._iRecordsDisplay;
                        var displayRecords = o._iRecordsTotal;
                        var isAllselected = false;
                        if ($(tablectrl).find('.select_all_chkbx'+params.classId).attr("isselectall") == "yes") {
                            isAllselected = true;
                        }
                        if (displayRecords < totalRecords) {
                            var trhtml = '<tr class="selectallmsg'+params.classId+' info">';
                            trhtml += '<td colspan="' + totalColumn + '" class="mr_hg26 mr_posrel">';
                            trhtml += '	<div class=" mr_lndg mr_pad5p0p mr_chkcenter"';
                            trhtml += '		style="left: -15px;">';
                            trhtml += '		<span class="selected_records_div'+params.classId+'">All <b>' + displayRecords + '</b> objects on this page selected. </span>';
                            trhtml += '		<span class="all_records_div'+params.classId+'" style="display: none;">All <b>' + totalRecords + ' </b>';
                            trhtml += '			matching objects selected. </span>';
                            trhtml += '			<strong><a href="javascript:void(0);"';
                            trhtml += '			class="delselectall mr_cncl" id="select_all_link'+params.classId+'" isselectall="no"> Select all ' + totalRecords + ' objects matching</a></strong>';
                            trhtml += '		<strong><a href="javascript:void(0);" class="delclrsct mr_cncl"';
                            trhtml += '			style="display: none;" id="clear_select_all_link'+params.classId+'"> Clear selection</a></strong>';
                            trhtml += '	</div></td>';
                            trhtml += '</tr>';
                            $(trhtml).insertBefore($(tablectrl).find('tbody tr:first'));
                            if (isAllselected) {
                                $('.selected_records_div'+params.classId).hide();
                                $('.all_records_div'+params.classId).show();
                                $('#clear_select_all_link'+params.classId).show();
                                $(tablectrl).find('#select_all_link'+params.classId).hide();
                                $(tablectrl).find('#select_all_link'+params.classId).attr("isselectall", "yes");
                                $(tablectrl).find('.select_all_chkbx'+params.classId).attr("isselectall", "yes");
                                $(tablectrl).find(".select_chkbx"+params.classId).attr("checked", true);
                                $(tablectrl).find(".select_all_chkbx"+params.classId).attr("checked", true);
                            }
                    }
                    $('#clear_select_all_link'+params.classId).unbind();
                        $('#clear_select_all_link'+params.classId).click(function () {
                            console.log("cleareventTriggered")
                        	$(enableIds).addClass("ggraybtn").removeClass("gbluebtn");
                        	$(enableIds).attr('title', 'You must select one or more checkboxes to use this action');
                            $(tablectrl).find('.selectallmsg'+params.classId).remove();
                            $(tablectrl).find('.select_all_chkbx'+params.classId).attr("isselectall", "no");
                            $(tablectrl).find(".select_chkbx"+params.classId).attr("checked", false);
                            $(tablectrl).find(".select_all_chkbx"+params.classId).attr("checked", false);
                        });
                        $('#select_all_link'+params.classId).unbind();
                        $('#select_all_link'+params.classId).click(function () {
                            $('.selected_records_div'+params.classId).hide();
                            $('.all_records_div'+params.classId).show();
                            $('#clear_select_all_link'+params.classId).show();
                            $(tablectrl).find('#select_all_link'+params.classId).hide();
                            $(tablectrl).find('#select_all_link'+params.classId).attr("isselectall", "yes");
                            $(tablectrl).find('.select_all_chkbx'+params.classId).attr("isselectall", "yes");
                            $(tablectrl).find(".select_chkbx"+params.classId).attr("checked", true);
                        });
                        
                        
                        $('.paginate_button').click(function () {
                            console.log("clearevent")
                            var event = jQuery.Event("click");
                            $(tablectrl).find("#clear_select_all_link"+params.classId).trigger(event);
                        });
                }
                    
                });
                
                 $(".select_chkbx"+params.classId).unbind().on('click', function () {
                     console.log('select_chkbx+clicked')
                    var ctrl = this;
                    if ($(ctrl).is(':checked') == false) {
                         $(tablectrl).find('.select_all_chkbx'+params.classId).attr("isselectall", "no");
                        $(tablectrl).find('.selectallmsg'+params.classId).remove();
                        $(tablectrl).find(".select_all_chkbx"+params.classId).prop("checked", false);
                    }
                    if ($('.select_chkbx'+params.classId+':checked').length === 0) {
                    	$(enableIds).addClass("disabled");
                    	$(enableIds).attr('title', 'You must select one or more checkboxes to use this action');
                        $(tablectrl).find('.select_all_chkbx'+params.classId).attr("isselectall", "no");
                        $(tablectrl).find('.selectallmsg'+params.classId).remove();
                        $(ctrl).closest("table").find('.select_all_chkbx'+params.classId).prop("checked", false);
                    } else {
                    	$(enableIds).removeClass("disabled");
                    	$(enableIds).removeAttr('title');
                    }

                });
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                
                var rowText = '<tr stuid="' + aData.id +'">';
                    rowText += '<td>';
                    rowText += '<div class="mr_fltlft mr_wd34px mr_txtcntr">';
                    rowText += !aData.disableCheck?'<input type="checkbox" class="mr_tbltdiprdbtn select_chkbx'+params.classId+'" ></div>':'';
                    rowText += '</td>';
                rowText += '<td class="new-patient invoiceview sorting_1">';
                rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.Student_Name +'</span>';
                rowText += '</td>';
                
                rowText += '<td class="new-patient invoiceview sorting_1">';
                rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.sub_div +'</span>';
                rowText += '</td>';
                
                rowText += '<td class="new-patient invoiceview sorting_1">';
                rowText += '<span class="label label-'+ aData.resultlbl+' label-mini">' + aData.resultNm +'</span>';
                rowText += '</td>';
                rowText += '</tr>';
                var newRow = $(rowText);
                $(nRow).html(newRow.html());
                commonSettings.copyAttributes(newRow, nRow);
                return nRow;
            }
        });
        }
    });
};

promotionSettings.publishAction  = function(ctrl) {	
    var classId = $(ctrl).attr('id').substring(10,11);
    console.log(classId)
    var tablectrl = '#promoClass'+classId;
    
    var params = {};
    var stuids = [];
    $(tablectrl).find('.select_chkbx'+classId+':checked').each(function () {
        stuids.push($.trim($(this).closest('tr').attr("stuid")));
    });
    
    if ((stuids == '') || ($('#select_all_link'+classId).attr("isselectall") == "yes")) {
        params['stuids'] = '';  
        params.classId = classId;
        $('#confirm_close').unbind('click').click();
     $('.cnfBox-title').html('Confirm Message');
    $('.cnfBox-body').html("Do you want to publish results all student in this class ?");
    $('#confirmMsg').click();
    $('#confirm_yes').unbind('click').click(function() {
        $('#confirm_no').click();
        $.ajax({
        type: "POST",
        url: commonSettings.promotionAjaxurl  + "/promoteStudents",
        data: params,
        dataType: "JSON",
        success: function(res) {
            //console.log(res)
            if (res) {
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function() {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                        return false;
                    }
                    else if (res['type'] == 'success'){
//                     window.location.reload();
                    }
                });

            }
        }
    });
    });
    } else {
        params['stuids'] = stuids;      
        $('#confirm_close').unbind('click').click();
     $('.cnfBox-title').html('Confirm Message');
    $('.cnfBox-body').html("Please confirm to publish results of selected records ?");
    $('#confirmMsg').click();
    $('#confirm_yes').unbind('click').click(function() {
        $('#confirm_no').click();
         $.ajax({
        type: "POST",
        url: commonSettings.promotionAjaxurl  + "/promoteStudents",
        data: params,
        dataType: "JSON",
        success: function(res) {
            //console.log(res)
            if (res) {
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html(res['message']);
                $('#messageBox').click();
                $('#msg_ok').unbind('click').click(function() {
                    if (res['type'] == 'error') {
                        $('#msg_ok').unbind('click').click();
                        return false;
                    }
                    else if (res['type'] == 'success'){
                     window.location.reload();
                    }
                });

            }
        }
    });
    });
    }

}