var feeclubSettings = {};
feeclubSettings.loadFeeGroup = function(){
       $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/manageFeeGroup",
        dataType: "html",
        success: function(res) {
            $('#feegroupDiv').html(res);    
            $('#feegroupDiv').removeClass('hide')
        }
    });
};
feeclubSettings.loadaddFee = function(){
    
       $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/addFee",
        dataType: "html",
        success: function(res) {
            $('#addFeeDiv').html(res);    
            $('#addFeeDiv').removeClass('hide')
        }
    });
};

feeclubSettings.loadaddFeeInst = function(){
    
       $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/addFeeIns",
        dataType: "html",
        success: function(res) {
            $('#addFeeDiv').html(res);    
            $('#addFeeDiv').removeClass('hide')
        }
    });
};

feeclubSettings.loadAllFee = function(){
    
       $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/viewAllFees",
        dataType: "html",
        success: function(res) {
            $('#viewFeeDiv').html(res);    
            $('#viewFeeDiv').removeClass('hide')
        }
    });
};


feeclubSettings.loadViewFee = function(){    
       $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/viewAllFees",
        dataType: "HTML",
        success: function(res) {
            $('#viewFeeDiv').html(res);    
            $('#viewFeeDiv').removeClass('hide')
        }
    });
};

feeclubSettings.loadassignfee = function(){    
       $.ajax({
        type: "POST",
        url: commonSettings.feeAssigningUrl ,
        dataType: "html",
        success: function(res) {
              $('#assignFeeDiv').html(res);    
            $('#assignFeeDiv').removeClass('hide')
        }
    });
};

feeclubSettings.loadfeecancelapprvl = function(){    
       $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/feeManag",
        dataType: "HTML",
        success: function(res) {
            $('#feecancelappvlDiv').html(res);    
            $('#feecancelappvlDiv').removeClass('hide')
        }
    });
};

 feeclubSettings.loadscholarships = function()
 {
     feepaymentSettings.callfrom= 'scholarschip';
      $.ajax({
        type: "POST",
        url: commonSettings.feePaymentUrl + "/studentFeePaymentScholarship",
        dataType: "HTML",
        success: function(res) {
            $('#feecancelappvlDiv').html(res);    
            $('#feecancelappvlDiv').removeClass('hide')
        }
    });
 };
  feeclubSettings.loadTransportdet = function()
 {
     
     $.ajax({
        type: "POST",
        url: commonSettings.transportAjaxurl + "/transportStuSearch",
        dataType: "html",
        success: function (res) {
            
           $('#transportdetDiv').html(res);
            $('#transportdetDiv').removeClass('hide');
        }
    });
 };
 
  feeclubSettings.loadFeeAssigning = function() {
     
     $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/loadFeeAssigning",
        dataType: "html",
        success: function (res) {
            
           $('#feeAssignDiv').html(res);
            $('#feeAssignDiv').removeClass('hide');
        }
    });
};

feeclubSettings.loadStuFeeReport = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/loadFeeReport",
        dataType: "html",
        success: function (res) {

            $('#stufeereportDiv').html(res);
            $('#stufeereportDiv').removeClass('hide');
        }
    });
};


