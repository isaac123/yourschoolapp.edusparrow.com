var feeSplitupSettings = {};
feeSplitupSettings.restoreRow = function (oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);

    for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
        oTable.fnUpdate(aData[i], nRow, i, false);
    }

    oTable.fnDraw();

};

feeSplitupSettings.editRow = function (oTable, nRow, option) {
    var aData = oTable.fnGetData(nRow);
    var mode = option == 'new' ? 'data-mode="new"' : '';
    var jqTds = $('>td', nRow);
    jqTds[0].innerHTML = '<input type="text"  name="splname"  class="form-control splname" value="' + aData[0] + '">';
    jqTds[1].innerHTML = '<input type="text" class="form-control small advsplamount"  onkeyup="feeSplitupSettings.validateAmount(this)"  value="' + aData[1] + '">';
    jqTds[2].innerHTML = '<a class="edit" href="javascript:;" onclick ="feeSplitupSettings.editAction(this)">Save</a>';
    jqTds[3].innerHTML = '<a class="cancel" href="javascript:;" ' + mode + ' onclick= "feeSplitupSettings.cancelRow (this)">Cancel</a>';

};

feeSplitupSettings.saveRow = function (oTable, nRow) {
    var jqInputs = $('input', nRow);
    if (jqInputs[0].value == '' || jqInputs[1].value == '') {
        $('.msgBox-title').html('Error');
        $('.msgBox-body').html('Invalid Input');
        $('#messageBoxModal').modal('show');
        oTable.fnDeleteRow(nRow);
        return false;
    }
    //console.log(nRow)
    $(nRow).find('td:eq( 0 )').addClass('advsplmonyr')
    $(nRow).find('td:eq( 1 )').addClass('advsplamount')
    oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
    oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
    oTable.fnUpdate('<a class="edit" href="javascript:;" onclick ="feeSplitupSettings.editAction(this)">Edit</a>', nRow, 2, false);
    oTable.fnUpdate('<a class="delete" href="javascript:;" onclick = "feeSplitupSettings.deleteRow(this)">Delete</a>', nRow, 3, false);
    oTable.fnDraw();
};

var column = [0, 1];
var sortingDisabled = [2, 3];
var oTable = $(feeSettings.addfeeTablectrl).dataTable({
    "sDom": 'C<"clear">lrtip',
    //"bProcessing": false,
    "destroy": true,
    "sPaginationType": "full_numbers",
    "iDisplayLength": commonSettings.jqueryDynmicTable,
    "aoColumnDefs": [{
            "sDefaultContent": "",
            "aTargets": column
        }, {
            "bSortable": false,
            "sDefaultContent": "",
            "aTargets": sortingDisabled
        }],
    "bLengthChange": false,
    "bInfo": false
});
var nEditing = null;
$(feeSettings.addfeeTablectrl + '_new').click(function (e) {
//    console.log(oTable)
//    console.log($(this).closest('.fee-table').find('.feeSplitupFormTable').attr('id').split('_')[2])
    var tblindex = $(this).closest('.fee-table').find('.feeSplitupFormTable').attr('id').split('_')[2];
    $.fn.dataTableExt.iApiIndex = tblindex;
//    console.log( $(this).parents().prev('.instalment').find('input.feeamount').val())
    feeSettings.feeTotalAmount = $(this).parents().prev('.instalment').find('input.feeamount').val()
    if (feeSettings.feeTotalAmount > 1 && feeSettings.itemfeeTotalAmount < parseInt(feeSettings.feeTotalAmount)) {
            if (feeSettings.itemfeeTotalAmount != feeSettings.feeTotalAmount)
    {
        $($(this)).closest('.fee-table').find('#match').addClass('hide')
        $($(this)).closest('.fee-table').find('#notmatch').removeClass('hide')
        $($(this)).closest('.fee-table').find('#itemTotalSpan').html(feeSettings.itemfeeTotalAmount.toFixed(2));
    }
    else
    {
        $($(this)).closest('.fee-table').find('#notmatch').addClass('hide')
        $($(this)).closest('.fee-table').find('#match').removeClass('hide')
        $($(this)).closest('.fee-table').find('#itemTotalSpan').html(feeSettings.itemfeeTotalAmount.toFixed(2));
    }
        e.preventDefault();
        var aiNew = oTable.fnAddData(['', '', '', '',
            '<a class="edit" href="javascript:;">Edit</a>', '<a class="cancel" data-mode="new" href="javascript:;" onclick= "feeSplitupSettings.cancelRow (this)">Cancel</a>'
        ]);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        feeSplitupSettings.editRow(oTable, nRow, 'new');
        nEditing = nRow;
    } else {
        swal({
            title: "Error",
            text: "Items' total exceeds the advance amount!",
            type: "error",
            confirmButtonText: 'Ok!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        return false;
                    }
                });
    }
});

feeSplitupSettings.deleteRow = function (ctrl) {
//    e.preventDefault();
//    console.log('hais')

    var tblindex = $(ctrl).closest('.fee-table').find('.feeSplitupFormTable').attr('id').split('_')[2];
    $.fn.dataTableExt.iApiIndex = tblindex;
    swal({
        title: "Confirm",
        text: "Are you sure to delete this row ?",
        type: "warning",
        confirmButtonText: 'Yes!',
        showCancelButton: true,
        closeOnCancel: true,
        closeOnConfirm: true,
        cancelButtonText: 'Cancel',
    },
            function (isConfirm) {
                if (isConfirm) {
                    var nRow = $(ctrl).parents('tr')[0];
                    var jqInputs = $('td', nRow);
                    //console.log(jqInputs)

                    oTable.fnDeleteRow(nRow);

                    var splAmountSum = 0;
                    $(feeSettings.addfeeTablectrl).find('input.advsplamount').each(function () {
                        splAmountSum += $.trim($(this).val());
                    });
                    $(feeSettings.addfeeTablectrl).find('td.advsplamount').each(function () {
                        splAmountSum += parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0;
                    });
                    feeSettings.itemfeeTotalAmount = parseInt(splAmountSum);
    feeSettings.feeTotalAmount = $(ctrl).parents().prev('.instalment').find('input.feeamount').val()
//    //console.log((feeSettings.itemfeeTotalAmount + ' ' + feeSettings.feeTotalAmount))
                    if (feeSettings.itemfeeTotalAmount != feeSettings.feeTotalAmount)
                    {
                        $(ctrl).closest('.fee-table').find('#match').addClass('hide')
                        $(ctrl).closest('.fee-table').find('#notmatch').removeClass('hide')
                        $(ctrl).closest('.fee-table').find('#itemTotalSpan').html(feeSettings.itemfeeTotalAmount.toFixed(2));
                    }
                    else
                    {
                        $(ctrl).closest('.fee-table').find('#notmatch').addClass('hide')
                        $(ctrl).closest('.fee-table').find('#match').removeClass('hide')
                        $(ctrl).closest('.fee-table').find('#itemTotalSpan').html(feeSettings.itemfeeTotalAmount.toFixed(2));
                    }
                } else {
                    return false;
                }
            });


};


feeSplitupSettings.editAction = function (ctrl) {
    var tblindex = $(ctrl).closest('.fee-table').find('.feeSplitupFormTable').attr('id').split('_')[2];
    $.fn.dataTableExt.iApiIndex = tblindex;
    var nRow = $(ctrl).closest('tr')[0];
    nEditing = nRow;
    if (nEditing !== null && nEditing != nRow) {
        feeSplitupSettings.editRow(oTable, nRow, 'edit');
        nEditing = nRow;
    } else if (nEditing == nRow && ctrl.innerHTML == "Save") {
        var jqInputs = $('input', nEditing);
        //console.log(jqInputs)
        if (jqInputs[0].value == '') {
            $('.msgBox-title').html('Error');
            $('.msgBox-body').html('Invalid Input');
            $('#messageBoxModal').modal('show');
            oTable.fnDeleteRow(nRow);
            return false;
        }
    feeSettings.feeTotalAmount = $(ctrl).parents().prev('.instalment').find('input.feeamount').val()
        if (feeSettings.itemfeeTotalAmount > feeSettings.feeTotalAmount)
        {
            $(ctrl).closest('.fee-table').find('#match').addClass('hide')
        $(ctrl).closest('.fee-table').find('#notmatch').removeClass('hide')
        $(ctrl).closest('.fee-table').find('#itemTotalSpan').html(feeSettings.itemfeeTotalAmount.toFixed(2));
            swal({
                title: "Error",
                text: 'Items\' total exceeds the Fee total!',
                type: "error",
                confirmButtonText: 'Ok!',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            return false;
                        }
                    });
        } else {
            feeSplitupSettings.saveRow(oTable, nEditing);
            nEditing = null;
            //console.log(params)
        }
    } else {
        /* No edit in progress - let's start one */
        feeSplitupSettings.editRow(oTable, nRow, 'edit');
        nEditing = nRow;




        $('.advreturnmotnh').datetimepicker({
            format: 'mm-yyyy',
            language: 'en',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 3,
            minView: 3,
            forceParse: 0,
            startDate: '-1d',
        });
    }
}

feeSplitupSettings.cancelRow = function (ctrl) {

    var tblindex = $(ctrl).closest('.fee-table').find('.feeSplitupFormTable').attr('id').split('_')[2];
    $.fn.dataTableExt.iApiIndex = tblindex;
    if ($(ctrl).attr("data-mode") == "new") {
        var nRow = $(ctrl).parents('tr')[0];
        oTable.fnDeleteRow(nRow);

        var splAmountSum = 0;
        $(feeSettings.addfeeTablectrl).find('input.advsplamount').each(function () {
            splAmountSum += parseInt($.trim($(this).val())) > 0 ? parseInt($.trim($(this).val())) : 0;
        });

        $(feeSettings.addfeeTablectrl).find('td.advsplamount').each(function () {
            splAmountSum += parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0;
        });
        feeSettings.itemfeeTotalAmount = parseInt(splAmountSum);
    feeSettings.feeTotalAmount = $(ctrl).parents().prev('.instalment').find('input.feeamount').val()
        if (feeSettings.itemfeeTotalAmount != feeSettings.feeTotalAmount)
        {
            $(ctrl).closest('.fee-table').find('#match').addClass('hide')
            $(ctrl).closest('.fee-table').find('#notmatch').removeClass('hide')
            $(ctrl).closest('.fee-table').find('#itemTotalSpan').html(feeSettings.itemfeeTotalAmount.toFixed(2));
        }
        else
        {
            $(ctrl).closest('.fee-table').find('#notmatch').addClass('hide')
            $(ctrl).closest('.fee-table').find('#match').removeClass('hide')
            $(ctrl).closest('.fee-table').find('#itemTotalSpan').html(feeSettings.itemfeeTotalAmount.toFixed(2));
        }
    } else {
        feeSplitupSettings.restoreRow(oTable, nEditing);
        nEditing = null;
    }


};
feeSplitupSettings.validateAmount = function (ctrl) {
    var $this = $(ctrl);
    var splamount = $this.val().replace(/,/g, "");
    //console.log(parseInt(splamount))
    if (parseInt(splamount) > 0) {
//var tblindex = $(ctrl).closest('.fee-table').find('.feeSplitupFormTable').attr('id').split('_')[2];
        var splAmountSum = 0;
        $(ctrl).closest('.fee-table').find(feeSettings.addfeeTablectrl).find('input.advsplamount').each(function () {
            splAmountSum += parseInt($.trim($(this).val())) ? parseInt($.trim($(this).val())) : 0;
        });

        $(ctrl).closest('.fee-table').find(feeSettings.addfeeTablectrl).find('td.advsplamount').each(function () {
            //console.log('splitamt:' + parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0)
            splAmountSum += parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0;
        });
        feeSettings.itemfeeTotalAmount = parseInt(splAmountSum);
    feeSettings.feeTotalAmount = $(ctrl).parents().prev('.instalment').find('input.feeamount').val()
        //console.log((feeSettings.itemfeeTotalAmount + ' ' + feeSettings.feeTotalAmount))
        if (feeSettings.itemfeeTotalAmount != feeSettings.feeTotalAmount)
        {
            $(ctrl).closest('.fee-table').find('#match').addClass('hide')
            $(ctrl).closest('.fee-table').find('#notmatch').removeClass('hide')
            $(ctrl).closest('.fee-table').find('#itemTotalSpan').html(feeSettings.itemfeeTotalAmount.toFixed(2));
        }
        else
        {
            $(ctrl).closest('.fee-table').find('#notmatch').addClass('hide')
            $(ctrl).closest('.fee-table').find('#match').removeClass('hide')
            $(ctrl).closest('.fee-table').find('#itemTotalSpan').html(feeSettings.itemfeeTotalAmount.toFixed(2));
        }

    } else {
        swal({
            title: "Error",
            text: "Enter a valid item fee amount",
            type: "error",
            confirmButtonText: 'Ok!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $this.val('')
                    }
                });
    }

};
