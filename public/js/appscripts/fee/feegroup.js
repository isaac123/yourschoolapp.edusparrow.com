var feegroupSettings = {};
feegroupSettings.restoreRow = function(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);

    for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
        oTable.fnUpdate(aData[i], nRow, i, false);
    }

    oTable.fnDraw();

};

feegroupSettings.editRow = function(oTable, nRow, option) {
    var aData = oTable.fnGetData(nRow);
    var mode = option == 'new' ? 'data-mode="new"' : '';
    var jqTds = $('>td', nRow);
    jqTds[0].innerHTML = '<input type="text" class="form-control small " value="' + aData[0] + '">';
    jqTds[1].innerHTML = '<input type="text" class="form-control small " value="' + aData[1] + '">';
    jqTds[2].innerHTML =  '<select class="form-control"  >\n\
     <option value = "1" ' +( (aData[2]=='Yes')?"selected":"")+ ' >Yes</option>\n\
     <option value = "0" ' +( (aData[2]=='No')?"selected":"")+ ' >No</option>\n\
     </select>';
    jqTds[3].innerHTML = '<a class="edit" href="javascript:;" onclick ="feegroupSettings.editAction(this)">Save</a>';
    jqTds[4].innerHTML = '<a class="cancel" href="javascript:;" ' + mode + ' onclick= "feegroupSettings.cancelRow (this)">Cancel</a>';

};

feegroupSettings.saveRow = function(oTable, nRow) {
    var jqInputs = $('input,select', nRow);
    if (jqInputs[0].value == '' || jqInputs[1].value == '') {
        $('.msgBox-title').html('Error');
        $('.msgBox-body').html('Invalid Input');
        $('#messageBoxModal').modal('show');
        oTable.fnDeleteRow(nRow);
        return false;
    }
    console.log(nRow)
    oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
    oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
    oTable.fnUpdate((jqInputs[2].value == '1' ? 'Yes' : 'No'), nRow, 2, false);
    oTable.fnUpdate('<a class="edit" href="javascript:;" onclick ="feegroupSettings.editAction(this)">Edit</a>', nRow, 3, false);
    oTable.fnUpdate('<a class="delete" href="javascript:;" onclick = "feegroupSettings.deleteRow(this)">Delete</a>', nRow, 4, false);
    oTable.fnDraw();


};

var column = [0, 1, 2];
var sortingDisabled = [4, 3];
console.log((feeSettings.addfeegrpTablectrl))
var oTable = $(feeSettings.addfeegrpTablectrl).dataTable({
    "sDom": 'C<"clear">lrtip',
    "sPaginationType": "full_numbers",
    "iDisplayLength": commonSettings.NumberOfRecords,
    "aoColumnDefs": [{
            "sDefaultContent": "",
            "aTargets": column
        }, {
            "bSortable": false,
            "sDefaultContent": "",
            "aTargets": sortingDisabled
        }],
    "bLengthChange": false,
    "bInfo": false
});

var nEditing = null;

$(feeSettings.addfeegrpTablectrl + '_new').click(function(e) {
    e.preventDefault();
    var aiNew = oTable.fnAddData(['', '', '', '',
        '<a class="edit" href="javascript:;">Edit</a>', '<a class="cancel" data-mode="new" href="javascript:;" onclick= "feegroupSettings.cancelRow (this)">Cancel</a>'
    ]);
    var nRow = oTable.fnGetNodes(aiNew[0]);
    feegroupSettings.editRow(oTable, nRow, 'new');
    nEditing = nRow;
});

feegroupSettings.deleteRow = function(ctrl) {
    swal({
        title: "Confirm",
        text: "Are you sure to delete this row ?",
        type: "warning",
        confirmButtonText: 'Yes!',
        showCancelButton: true,
        closeOnCancel: true,
        closeOnConfirm: true,
        cancelButtonText: 'Cancel',
    },
            function(isConfirm) {
                if (isConfirm) {

                    var nRow = $(ctrl).parents('tr')[0];
                    var jqInputs = $('td', nRow);
                    console.log(jqInputs)
                    var params = {};
                    params.feeGroupId = $(nRow).attr('feeGroupId')
                    console.log(params)
                    $.ajax({
                        type: "POST",
                        url: commonSettings.feeAjaxurl + "/deleteFeeGroup",
                        data: params,
                        dataType: "JSON",
                        success: function(res) {
                            console.log(res)

                            if (res.type == 'success') {
                                oTable.fnDeleteRow(nRow);
                            } else {
                                swal({
                                    title: "Error",
                                    text: res['message'],
                                    type: "error",
                                    confirmButtonText: 'Ok!',
                                    closeOnConfirm: true,
                                },
                                        function(isConfirm) {
                                            if (isConfirm) {
                                                return false;
                                            }
                                        });
                            }
                        }
                    });
                }
            });

};


feegroupSettings.editAction = function(ctrl) {
    /* Get the row as a parent of the link that was clicked on */
    var nRow = $(ctrl).closest('tr')[0];
    nEditing = nRow;
   feegroupSettings.err=0;
    if (nEditing !== null && nEditing != nRow) {
        /* Currently editing - but not this row - restore the old before continuing to edit mode */
        feegroupSettings.editRow(oTable, nRow, 'edit');
        nEditing = nRow;
    } else if (nEditing == nRow && ctrl.innerHTML == "Save") {
        /* Editing this row and want to save it */
        var jqInputs = $('input,select', nEditing);
        console.log(jqInputs)
        if (jqInputs[0].value == '' || jqInputs[1].value== '') {
              feegroupSettings.err=1;
              console.log('assign');
            $('.msgBox-title').html('Error');
            $('.msgBox-body').html('Invalid Input');
            $('#messageBoxModal').modal('show');
           // oTable.fnDeleteRow(nRow);
          
            //return false;
            
        }
        var params = {};
        params.feeGrpName = jqInputs[0].value;
        params.feedesc = jqInputs[1].value;
        params.isactive = jqInputs[2].value;
        params.feeGroupId = $(nEditing).attr('feeGroupId');

        console.log(params)
        
      if( feegroupSettings.err == 0){
        $.ajax({
            type: "POST",
            url: commonSettings.feeAjaxurl + "/addOrUpdateNewFeeGrp",
            data: params,
            dataType: "JSON",
            success: function(res) {
                if (res) {
                    if (res.type != 'success') {

                        swal({
                            title: "Error",
                            text: res.message,
                            type: res.type,
                            confirmButtonText: 'Ok!',
                            closeOnConfirm: true,
                        },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        if (res['type'] == 'error') {
                                           // oTable.fnDeleteRow(nEditing);
                                            return false;
                                        } else {
                                            $(nEditing).attr('feeGroupId', res.feeGroupId)
                                            nEditing = null;
                                        }
                                    }
                                });


                    } else {
                        $(nEditing).attr('feeGroupId', res.feeGroupId)
                        feegroupSettings.saveRow(oTable, nEditing);
                        nEditing = null;
                    }


                }
            }
        });
    }
    } else {
        /* No edit in progress - let's start one */
        feegroupSettings.editRow(oTable, nRow, 'edit');
        nEditing = nRow;
    }
}

feegroupSettings.cancelRow = function(ctrl) {
    console.log("cancelit")
    console.log($(ctrl).html())
    if ($(ctrl).attr("data-mode") == "new") {
        var nRow = $(ctrl).parents('tr')[0];
        console.log($(ctrl).parents('tr').html())
        oTable.fnDeleteRow(nRow);

    } else {
        feegroupSettings.restoreRow(oTable, nEditing);
        // alert("Deleted! Do not forget to do some ajax to sync with backend restore:)");
        nEditing = null;
    }


};