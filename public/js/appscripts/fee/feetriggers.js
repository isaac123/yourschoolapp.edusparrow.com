var feetriggersSettings = {};
feetriggersSettings.restoreRow = function(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);

    for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
        oTable.fnUpdate(aData[i], nRow, i, false);
    }

    oTable.fnDraw();

};

feetriggersSettings.editRow = function(oTable, nRow, option) {
    var aData = oTable.fnGetData(nRow);
    var mode = option == 'new' ? 'data-mode="new"' : '';
    var jqTds = $('>td', nRow);
//    console.log( '< option value = "1" ' +( (aData[3]=='Yes')?"selected":"")+ ' > Yes < /option>')
    jqTds[0].innerHTML = '<input type="text" class="form-control small " value="' + aData[0] + '">';
    jqTds[1].innerHTML = '<input type="text" class="form-control small " value="' + aData[1] + '">';
    jqTds[2].innerHTML = '<input type="text" class="form-control small " value="' + aData[2] + '">';
    jqTds[3].innerHTML = '<select class="form-control"  >\n\
     <option value = "1" ' +( (aData[3]=='Yes')?"selected":"")+ ' >Yes</option>\n\
     <option value = "0" ' +( (aData[3]=='No')?"selected":"")+ ' >No</option>\n\
     </select>';
    jqTds[4].innerHTML = '<a class="edit" href="javascript:;" onclick ="feetriggersSettings.editAction(this)">Save</a>';
    jqTds[5].innerHTML = '<a class="cancel" href="javascript:;" ' + mode + ' onclick= "feetriggersSettings.cancelRow (this)">Cancel</a>';

};

feetriggersSettings.saveRow = function(oTable, nRow) {
    var jqInputs = $('input,select', nRow);
    if (jqInputs[0].value == '' || jqInputs[1].value == '') {
        $('.msgBox-title').html('Error');
        $('.msgBox-body').html('Invalid Input');
        $('#messageBoxModal').modal('show');
        oTable.fnDeleteRow(nRow);
        return false;
    }
    console.log(nRow)
    oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
    oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
    oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
    oTable.fnUpdate((jqInputs[3].value == '1' ? 'Yes' : 'No'), nRow, 3, false);
    oTable.fnUpdate('<a class="edit" href="javascript:;" onclick ="feetriggersSettings.editAction(this)">Edit</a>', nRow, 4, false);
    oTable.fnUpdate('<a class="delete" href="javascript:;" onclick = "feetriggersSettings.deleteRow(this)">Delete</a>', nRow, 5, false);
    oTable.fnDraw();


};

var column = [0, 1, 2 ,3];
var sortingDisabled = [4, 5];
console.log((feeSettings.addfeetriggerTablectrl))
var oTable = $(feeSettings.addfeetriggerTablectrl).dataTable({
    "sDom": 'C<"clear">lrtip',
    "sPaginationType": "full_numbers",
    "iDisplayLength": commonSettings.NumberOfRecords,
    "aoColumnDefs": [{
            "sDefaultContent": "",
            "aTargets": column
        }, {
            "bSortable": false,
            "sDefaultContent": "",
            "aTargets": sortingDisabled
        }],
    "bLengthChange": false,
    "bInfo": false
});

var nEditing = null;

$(feeSettings.addfeetriggerTablectrl + '_new').click(function(e) {
    e.preventDefault();
    var aiNew = oTable.fnAddData(['', '', '', '',
        '<a class="edit" href="javascript:;">Edit</a>', '<a class="cancel" data-mode="new" href="javascript:;" onclick= "feetriggersSettings.cancelRow (this)">Cancel</a>'
    ]);
    var nRow = oTable.fnGetNodes(aiNew[0]);
    feetriggersSettings.editRow(oTable, nRow, 'new');
    nEditing = nRow;
});

feetriggersSettings.deleteRow = function(ctrl) {
    swal({
        title: "Confirm",
        text: "Are you sure to delete this row ?",
        type: "warning",
        confirmButtonText: 'Yes!',
        showCancelButton: true,
        closeOnCancel: true,
        closeOnConfirm: true,
        cancelButtonText: 'Cancel',
    },
            function(isConfirm) {
                if (isConfirm) {

                    var nRow = $(ctrl).parents('tr')[0];
                    var jqInputs = $('td', nRow);
                    console.log(jqInputs)
                    var params = {};
                    params.triggerId = $(nRow).attr('triggerId')
                    console.log(params)
                    $.ajax({
                        type: "POST",
                        url: commonSettings.feeAjaxurl + "/deleteFeeTrigger",
                        data: params,
                        dataType: "JSON",
                        success: function(res) {
                            console.log(res)

                            if (res.type == 'success') {
                                oTable.fnDeleteRow(nRow);
                            } else {
                                swal({
                                    title: "Error",
                                    text: res['message'],
                                    type: "error",
                                    confirmButtonText: 'Ok!',
                                    closeOnConfirm: true,
                                },
                                        function(isConfirm) {
                                            if (isConfirm) {
                                                return false;
                                            }
                                        });
                            }
                        }
                    });
                }
            });

};


feetriggersSettings.editAction = function(ctrl) {
    /* Get the row as a parent of the link that was clicked on */
    var nRow = $(ctrl).closest('tr')[0];
    nEditing = nRow;
    if (nEditing !== null && nEditing != nRow) {
        /* Currently editing - but not this row - restore the old before continuing to edit mode */
        feetriggersSettings.editRow(oTable, nRow, 'edit');
        nEditing = nRow;
    } else if (nEditing == nRow && ctrl.innerHTML == "Save") {
        /* Editing this row and want to save it */
        var jqInputs = $('input,select', nEditing);
        console.log(jqInputs)
        if (jqInputs[0].value == '') {
            $('.msgBox-title').html('Error');
            $('.msgBox-body').html('Invalid Input');
            $('#messageBoxModal').modal('show');
            oTable.fnDeleteRow(nRow);
            return false;
        }
        var params = {};
        params.feetrgName = jqInputs[0].value;
        params.feetrgController = jqInputs[1].value;
        params.feetrgAction = jqInputs[2].value;
        params.isactive = jqInputs[3].value;
        console.log(jqInputs[3].value)
        params.triggerId = $(nEditing).attr('triggerId');

        console.log(params)
        $.ajax({
            type: "POST",
            url: commonSettings.feeAjaxurl + "/addOrUpdateNewFeeTrigger",
            data: params,
            dataType: "JSON",
            success: function(res) {
                if (res) {
                    if (res.type != 'success') {

                        swal({
                            title: "Error",
                            text: res.message,
                            type: res.type,
                            confirmButtonText: 'Ok!',
                            closeOnConfirm: true,
                        },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        if (res['type'] == 'error') {
                                            oTable.fnDeleteRow(nEditing);
                                            return false;
                                        } else {
                                            $(nEditing).attr('triggerId', res.triggerId)
                                            nEditing = null;
                                        }
                                    }
                                });


                    } else {
                        $(nEditing).attr('triggerId', res.triggerId)
                        feetriggersSettings.saveRow(oTable, nEditing);
                        nEditing = null;
                    }


                }
            }
        });
    } else {
        /* No edit in progress - let's start one */
        feetriggersSettings.editRow(oTable, nRow, 'edit');
        nEditing = nRow;
    }
}

feetriggersSettings.cancelRow = function(ctrl) {
    console.log("cancelit")
    console.log($(ctrl).html())
    if ($(ctrl).attr("data-mode") == "new") {
        var nRow = $(ctrl).parents('tr')[0];
        console.log($(ctrl).parents('tr').html())
        oTable.fnDeleteRow(nRow);

    } else {
        feetriggersSettings.restoreRow(oTable, nEditing);
        // alert("Deleted! Do not forget to do some ajax to sync with backend restore:)");
        nEditing = null;
    }


};