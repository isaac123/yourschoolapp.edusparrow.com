
var manageFeeSettings = {};
manageFeeSettings.loadFeeList = function() {

    var feeSearchData = {};
    var params = {};
    feeSearchData.emptySearch = 1;

    //node_id is a hidden variable, when node is deleted node_id is not reset. so it is done here.

    var div = $('#divVal').val();
    if (div == '')
    {
        $('#node_id').val('');
    }

//    $("#addFeeNewForm").find('input,select,textarea').each(function() {
//        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
//        if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
//            feeSearchData[$(this).attr('name')] = $.trim($(this).val());
//            feeSearchData.emptySearch = 0
//        }
//
//    });

    params = feeSearchData;

    console.log(params);
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/feeTblHdr",
        dataType: "html",
        success: function(res) {
            $('#ApplicationTableDiv').html(res);
            var tablectrl = '#feeListTblHdr';
            var column = [0, 1, 2];
            var sortingDisabled = [3];

            manageFeeSettings.noOfRecordsToDisplay = 20;

            manageFeeSettings.startIndex = manageFeeSettings.startIndex ? manageFeeSettings.startIndex : 0;
            $(tablectrl).dataTable({
                "sDom": 'C<"clear">lrtip',
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": commonSettings.feeAjaxurl + "/loadFeeData",
                "sPaginationType": "full_numbers",
                "iDisplayLength": manageFeeSettings.noOfRecordsToDisplay,
                'iDisplayStart': manageFeeSettings.startIndex,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[0, "desc"]],
                "bLengthChange": false,
                "bInfo": false,
                "fnServerData": function(surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function(o) {

                    manageFeeSettings.startIndex = o._iDisplayStart;
                    if (o._iRecordsDisplay <= manageFeeSettings.noOfRecordsToDisplay) {
                        $(tablectrl + '_paginate').hide();
                    }
                },
                "fnRowCallback": function(nRow, aData, iDisplayIndex) {

                    var rowText = '<tr applicationId="' + aData.fee_master_id + '">';


                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" ><span class="label label-primary">' + aData.node_name + '</span></span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.fee_name + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.fee_man + '</span>';
                    rowText += '</td>';

                    rowText += '<td class="col-md-3">';
                    rowText += aData.Actions;
                    rowText += '</td>';

                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });


        }
    });


};
//manageFeeSettings.loadFeeList();