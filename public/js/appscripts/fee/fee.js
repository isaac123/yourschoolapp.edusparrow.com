var feeSettings = {};
feeSettings.addfeegrpTablectrl = '#feeGroupTbl'
feeSettings.addfeetriggerTablectrl = '#autoAssignTrgTbl'
feeSettings.feeTotalAmount = 0;
feeSettings.itemfeeTotalAmount = 0;
feeSettings.addfeeTablectrl = '.feeSplitupFormTable';
feeSettings.feeAssSearchdata = {};
feeSettings.loadSections = function (sval) {

    console.log(sval);
    if (!sval) {
        $('#secDet').addClass('hide')
        $('#secDet').html('');
    } else {
        feeSettings.classID = sval;

        $.ajax({
            type: "POST",
            url: commonSettings.feeAjaxurl + "/loadSection",
            data: {
                classId: sval
            },
            success: function (res) {
                if (res) {
                    $('#secDet').removeClass('hide')
                    $('#secDet').html(res);
                }

            }
        });
    }

};

feeSettings.enableAllwForm = function (ctrl)
{
    //    alert('d')
    //          console.log($('#allowed_count_form').html())
    $(ctrl).parents('div.newoldstu').find('select,input[type=text]').each(function () {
        var $this = $(ctrl).prop('checked');
        $(this).val('')
        $(this).prop('disabled', !$this)
    });
};
feeSettings.addNewFee = function () {
    var amount = $('#amount').val();
    var damount = $('#duplicate-amount').val();
    var pamount = $('#duplicate-amount').val();
    if (parseInt(amount) < 0 || parseInt(damount) < 0 || parseInt(pamount) < 0) {
        swal({
            title: "Error",
            text: "Please Select a valid amount",
            type: "error",
            confirmButtonText: 'Ok!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#amount').val('')
                        $('#app_feeadd').removeClass('hide');
                        $('.loading_addfee').addClass('hide');
                        return false;
                    }
                });
    }
    var err = 0;
    var message = '';
    var applnData = {};
    $("#addFeeForm").find('input,select').each(function () {

        applnData[$(this).attr('name')] = $.trim($(this).val());
    });

    $("#addFeeNewForm").find('input[type=text],input[type=number],input[type=hidden],input[type=checkbox],select').each(function () {
        //console.log($.trim($(this).val()))
        var $this = $(this);
//        console.log(($this.is(":checkbox")))
        if ($this.is(":checkbox")) {
            if ($this.is(":checked")) {
                $this.parents('div.newoldstu').find('select,input[type=number]').each(function () {
                    var $self = $(this);
                    console.log($self.val())
                    if ($.trim($self.val()) == '') {
                        err = 1;
                        message = 'Invalid value advance settings'
                    }
                });
            }
            applnData[$this.attr('name')] = $this.is(":checked") ? 1 : 0;

        }
        else
            applnData[$this.attr('name')] = $.trim($this.val());
    });


    console.log(err)
    console.log(applnData)
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.feeAjaxurl + "/addNewFee",
            data: applnData,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    if (res['type'] == 'error') {
                        swal("Error", res['message'], "error");
                        $('#app_feeadd').removeClass('hide');
                        $('.loading_addfee').addClass('hide');
                        return false;
                    }
                    else if (res['type'] == 'success') {
                        swal({
                            title: "Done",
                            text: res['message'],
                            type: "success",
                            confirmButtonText: 'Ok!',
                            closeOnConfirm: true,
                        },
                                function (isConfirm) {
                                    if (isConfirm) {
//                                    window.location.href = commonSettings.feeAjaxurl + "/makeSplitup?fee_item_id=" + res.feeItemId;
                                        feeSettings.loadSplitup(res.feeItemId)
                                    }
                                });
                    }

                }
            }
        });
    } else {
        $('.msgBox-title').html('Error');
        $('.msgBox-body').html(message);
        $('#messageBoxModal').modal('show');
        return false;
    }

};


feeSettings.viewselectedFee = function (feeItemId) {
    $.ajax({
        type: "GET",
        url: commonSettings.feeAjaxurl + "/viewselectedFee?fee_item_id=" + feeItemId,
        dataType: "html",
        success: function (res) {
            $('#msg_ok').unbind('click').click();
            $('#confirm_close').unbind('click').click();
            $('.formBox-title').html('View Fee');
            $('.formBox-body').html(res);
            // $('#modalForm').click();
            $('#confirm_save').addClass('hide');
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#formModal').modal('show');

        }
    });
};

feeSettings.editselectedFee = function (feeItemId) {
    $.ajax({
        type: "GET",
        url: commonSettings.feeAjaxurl + "/editselectedFee?fee_item_id=" + feeItemId,
        dataType: "html",
        success: function (res) {

            $('.formBox-title').html('Edit Fee');
            $('.formBox-body').html(res);
            //$('#modalForm').click();
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#formModal').modal('show');

//            if ($('#addFeeDiv').is(':visible')) {
//              $('#addFeeDiv').html(res);
//            } else {
//                 $('#viewFeeDiv').html(res);
//            }

//feeSettings.addNewFee();
//editFeeNewForm
            $('#confirm_save').html('Update');
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                feeSettings.editNewFee();
            });

        }
    });
};

feeSettings.editNewFee = function () {
    var applnData = {};
    applnData['feeName'] = '';

    $("#editFeeNewForm").find('input,select').each(function () {
        //console.log($.trim($(this).val()))
        if ($(this).attr('name') === 'newfeeName' || $(this).attr('name') === 'feeName')
        {
            //console.log('in')
            if ($.trim($(this).val()) != '')
                applnData['feeName'] = $.trim($(this).val());
        }
        else
            applnData[$(this).attr('name')] = $.trim($(this).val());
    });


    //console.log(applnData)

    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/editFee",
        data: applnData,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                console.log(res)
                console.log(res.type)
                if (res['type'] == 'error') {
                    swal("Error", res['message'], "error");
                    $('#app_feeadd').removeClass('hide');
                    $('.loading_addfee').addClass('hide');
                    return false;
                }
                else if (res['type'] == 'success') {
                    swal({
                        title: "Done",
                        text: res['message'],
                        type: "success",
                        confirmButtonText: 'Ok!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
//                                    window.location.href = commonSettings.feeAjaxurl + "/makeSplitup?fee_item_id=" + res.feeItemId;
                                    feeSettings.loadSplitup(res.feeItemId)
                                }
                            });
                }

            }
        }
    });

};


feeSettings.loadSplitup = function (feeItemId) {
    $.ajax({
        type: "GET",
        url: commonSettings.feeAjaxurl + "/makeSplitup?fee_item_id=" + feeItemId,
        dataType: "html",
        success: function (res) {
            if ($('#addFeeDiv').is(':visible')) {
                $('#addFeeDiv').html(res);
            } else {
                $('#viewFeeDiv').html(res);
            }
            ;
        }
    });
};


feeSettings.loadback = function () {
    console.log($('#addFeeDiv').is(':visible'))
    if ($('#addFeeDiv').is(':visible')) {

        feeclubSettings.loadaddFee();
    } else {
        console.log($('#viewFeeDiv').is(':visible'))
        feeclubSettings.loadViewFee();
    }
};
feeSettings.loadSplitupTbl = function () {
    console.log('changed')
    $.ajax({
        type: "POST",
        dataType: 'html',
        url: commonSettings.feeAjaxurl + "/loadSplitupTbl",
        success: function (res) {
            if (res) {
                $('#splitupTbl').removeClass('hide')
                $('#splitupTbl').html(res);

                (feeSettings.itemfeeTotalAmount != feeSettings.feeTotalAmount) ?
                        $('#splmatch').html('<font color="red">Items\' total doesn\'t match with Fee total!</font>')
                        : $('#splmatch').html(' <font color="green">Fee total and Items\' total are equal. Proceed to save the splitup!</font>');
            }

        }
    });

};

$('.square-green input:checkbox').on('ifToggled', function (event) {
    console.log($(this).val())
    console.log($(this).is(':checked'))
    if ($(this).is(':checked') == true) {
        var amount = $('#amount').val()
        if (parseInt(amount) > 1) {
            feeSettings.feeTotalAmount = parseInt(amount);
            $('#amount').prop('disabled', true)
            feeSettings.loadSplitupTbl();
        }
        else {
            swal({
                title: "Error",
                text: "Enter the fee amount",
                type: "error",
                confirmButtonText: 'Ok!',
                closeOnConfirm: true,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            console.log($('.square-green input:checkbox').iCheck('uncheck'))
                        }
                    });
        }
        return false;
    } else {

        $('#amount').prop('disabled', false)
        $('#splitupTbl').html('')
        $('#splitupTbl').addClass('hide')
    }
});
feeSettings.validate = function () {
    var to = $('#fee-to').val().split("-"),
            from = $('#fee-from').val().split("-"),
            t1 = new Date(from[2], from[1] - 1, from[0]),
            t2 = new Date(to[2], to[1] - 1, to[0]),
            now = new Date(),
            month = (now.getMonth() + 1) < 10 ? ('0' + (now.getMonth() + 1)) : now.getMonth(),
            date = (now.getDate() < 10) ? ('0' + now.getDate()) : now.getDate(),
            nDate = date + '-' + month + '-' + now.getFullYear(),
            diff_day = parseInt((t2 - t1) / (24 * 3600 * 1000));

    if (diff_day < 0)
    {
        swal({
            title: "Error",
            text: "Please Select a valid date Range",
            type: "error",
            confirmButtonText: 'Ok!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $('#fee-to').val(nDate)
                        $('#fee-from').val(nDate)
                        return false;
                    }
                });
    }

};

feeSettings.loadTransField = function (ctrl) {
    var appdata = {};
    appdata[$(ctrl).attr('name')] = $.trim($(ctrl).val());
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/loadTransportFormField",
        data: appdata,
        success: function (res) {
            if (res) {
                $('#trnsField').removeClass('hide')
                $('#trnsField').html(res);
            } else {
                console.log('in')
                feeSettings.showorhideStuTyp()
                $('#trnsField').html('');
                $('#trnsField').addClass('hide')
            }

        }
    });

};
feeSettings.showorhideStuTyp = function (ctrl) {
    if ($(ctrl).val() == 'staff') {
        $('#stypeDiv').addClass('hide')
        $('#divVal').attr('onchange', '')
        $('#secDet').addClass('hide')

    } else {
        $('#stypeDiv').removeClass('hide');
        $('#divVal').attr('onchange', 'feeSettings.loadSections(this.value)')
        $('#secDet').addClass('hide')
    }
    var cval = $(ctrl).val() ? $(ctrl).val() : 'student';
    //feeSettings.getDivOptions(cval)


};
feeSettings.getDivOptions = function (utype) {
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/getDivisions",
        data: {
            utype: utype
        },
        dataType: 'JSON',
        success: function (res) {
            $('#divVal').parent('div').prev('label').text(res[0].text);
            $('#divVal').empty();

            /* Insert the new ones from the array above */
            $.each(res[1], function (key, options) {
                console.log(options)
                var $div = $("<option>", options);
                $("#divVal").append($div);
            });

        }
    });
};
feeSettings.updatevehicle = function (ctrl) {
    var appdata = {};
    appdata[$(ctrl).attr('name')] = $.trim($(ctrl).val());
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/loadVehicleOptions",
        data: appdata,
        dataType: 'JSON',
        success: function (res) {
            if (res) {
                $('#vehicle').empty();

                /* Insert the new ones from the array above */
                $.each(res, function (key, options) {
                    console.log(options)
                    var $div = $("<option>", options);
                    $("#vehicle").append($div);
                });
            }

        }
    });
};
feeSettings.loadfeeField = function () {
//    $('#feeselct select').attr('disabled','disabled')
//    console.log($('#feeselct').html())
    $('#feeselct').addClass('hide')
    $('#feeselct select').val('')
    console.log($('#newfeeName').val())
    console.log($('#feeselct select').val())
    $('#feetxt').removeClass('hide')
    $('#addnewFeebtn').addClass('hide')
    $('#addslctFeebtn').removeClass('hide')
};
feeSettings.loadbackfeeField = function () {
//    $('#feeselct select').attr('disabled','disabled')
//    console.log($('#feeselct').html())
    $('#feeselct').removeClass('hide')
    $('#newfeeName').val('')
    console.log($('#newfeeName').val())
    console.log($('#feeselct select').val())
    $('#feetxt').addClass('hide')
    $('#addnewFeebtn').removeClass('hide')
    $('#addslctFeebtn').addClass('hide')
};

feeSettings.fetchFeeDetails = function (feename, feeid) {

    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/fetchFeeDetails",
        data: {
            feeid: feeid
        },
        dataType: "JSON",
        success: function (res) {
            if (res) {
                $('#feeName').val(res.fee_name);
                //$('#divVal').val(feename);
                $('#fee-from').val(res.fee_collection_start);
                $('#fee-to').val(res.fee_collection_end);
                $('#amount').val(res.fee_amount);
                $('#penalty-amount').val(res.penalty_amount);
                //$('#node_id').val(res.node_id);
            }
        }
    });
};

feeSettings.deleteSelectedFee = function (feeItemId) {

    // alert(feeItemId);
    //exit;
//    $('.cnfBox-title').html('Message');
//    $('.cnfBox-body').html('<div class="alert alert-block alert-danger fade in"><strong>If delete this fee, it will affect student fee assign too. <br>Confirm to delete..?</strong></div>');
//    $('#ConfirmModal').modal({
//        backdrop: 'static',
//        keyboard: false
//    });
//    $('#confirm_yes').unbind('click').click(function () {
//        $('#ConfirmModal').modal('hide');

    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/selectedFeeStudentsList",
        data: {
            feeid: feeItemId
        },
        dataType: "JSON",
        success: function (res) {

            if (res.type === 'success') {
                $('.cnfBox-title').html('Message');
                $('.cnfBox-body').html('<div class="alert alert-block alert-warning fade in"> Deleting the fee will lead to following actions. <br><br>1.Fee assigned to the students will be deleted. <br> 2. Total no of students assigned to this fee - <strong> ' + res['message'] + '</strong></div>');
                $('#ConfirmModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#confirm_yes').unbind('click').click(function () {
                    $('#ConfirmModal').modal('hide');

                    $.ajax({
                        type: "POST",
                        url: commonSettings.feeAjaxurl + "/deleteSelectedFeeByID",
                        data: {
                            feeid: feeItemId
                        },
                        dataType: "JSON",
                        success: function (res) {

                            $('.msgBox-title').html('Message');
                            $('.msgBox-body').html(res['message']);
                            $('#messageBoxModal').modal('show');
                            $('#msg_ok').unbind('click').click(function () {
                                $('#msg_ok').unbind('click').click();
                                manageFeeSettings.loadFeeList();
                            });

                        }
                    });
                });
            } else
            {
                $('.cnfBox-title').html('Message');
                $('.cnfBox-body').html(res.message);
            }
        }
    });
};

feeSettings.loadFeeTotal = function (ctrl) {

    var amount = $(ctrl).val()
    console.log(parseInt(amount))
    if (parseInt(amount) > 0) {
        feeSettings.feeTotalAmount = parseInt(amount);
        viewFeeSplSettings.calculateAmount(ctrl);
    }
    else {
        swal({
            title: "Error",
            text: "Enter the fee amount",
            type: "error",
            confirmButtonText: 'Ok!',
            closeOnConfirm: true,
        },
                function (isConfirm) {
                    if (isConfirm) {
                    }
                });
    }
    return false;

};

feeSettings.addNewFeeWitIns = function () {

    var err = 0, i = 0, j = 0;
    var message = '';
    var applnData = {};
    var formElement = document.getElementById("addFeeNewForm");
    var formData = new FormData(formElement);//$("#addFeeNewForm").serialize();//
    console.log(formData)

    $("#addFeeNewForm").find('input,select').each(function () {
        var $this = $(this);
        if ($.trim($this.val()) == '') {
            message = "Missing fields ";
            err = 1;
        }
    });
    if (feeSettings.itemfeeTotalAmount != feeSettings.feeTotalAmount) {
        swal("Error", "Items' total doesn't match with Advance amount total!", "error");
        err = 1;
        return false;
    }
    var typc = 0;
    var insTypData = {};
    $('.instalmentTyp').each(function () {
        var insc = 0;
        var insData = {};
        $(this).find('.instypSet').each(function () {
            var i = 0;
            var totsplnData = {};
            $(this).find(feeSettings.addfeeTablectrl).find('tr').each(function () {
                var splnData = {};
                $(this).find('td').each(function () {
                    var attr = $(this).attr('class');
                    if (typeof attr !== typeof undefined && attr !== false) {
                        console.log($(this).attr('class').split(' '))
                        var name = $(this).attr('class').split(' ')[0];
                        splnData[name] = $.trim($(this).text());
                    }
                });
                if (!$.isEmptyObject(splnData)) {
                    totsplnData[i] = splnData;
                    i++;
                }
            });
//console.log(totsplnData);
            if (!$.isEmptyObject(totsplnData)) {
//                console.log(insc)
                insData[insc] = totsplnData;
                insc++;
                i = 0;
                totsplnData = {};
            }
        });
//console.log(insData); return false;
        if (!$.isEmptyObject(insData)) {
            insTypData[typc] = insData;
            typc++;
            insc = 0;
            insData = {};
        }
    });
    formData.append('splitup', JSON.stringify(insTypData));
//    applnData['splitup'] = insTypData;
    console.log(insTypData);
//    formData.append( 'splitup[]',insTypData);
    if ($('#is_mand').is(':checked')) {

        if (err == 0) {
            $.ajax({
                type: "POST",
                url: commonSettings.feeAjaxurl + "/checkIfStudentExists",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "JSON",
                success: function (res) {
                    if (res) {
                        console.log(res)
                        if (res['type'] == "success") {
                            feeSettings.insertFeeWitIns(0)
                        } else {

                            var lblcls = res.type == 'warning' ? 'warning' : 'success';
                            $('#ConfirmModal .modal-title').html("Confirm Delete");
                            $('#ConfirmModal .modal-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                            $('#ConfirmModal').modal('show');
                            $('#confirm_yes').unbind('click').click(function () {
                                $('#ConfirmModal').modal('hide');
                                feeSettings.insertFeeWitIns(1)
                            });
                            $('#confirm_no').unbind('click').click(function () {
                                feeSettings.insertFeeWitIns(0)
                            });
                        }

                    }
                }
            });
        } else {
            $('.msgBox-title').html('Error');
            $('.msgBox-body').html(message);
            $('#messageBoxModal').modal('show');
            return false;
        }
    } else {
        feeSettings.insertFeeWitIns(0)
    }

}
feeSettings.insertFeeWitIns = function (confirmAssign) {

    var err = 0, i = 0, j = 0;
    var message = '';
    var applnData = {};
    var formElement = document.getElementById("addFeeNewForm");
    var formData = new FormData(formElement);//$("#addFeeNewForm").serialize();//
    console.log(formData)

    $("#addFeeNewForm").find('input,select').each(function () {
        var $this = $(this);
        if ($.trim($this.val()) == '') {
            message = "Missing fields ";
            err = 1;
        }
    });
    if (feeSettings.itemfeeTotalAmount != feeSettings.feeTotalAmount) {
        swal("Error", "Items' total doesn't match with Advance amount total!", "error");
        err = 1;
        return false;
    }
    var typc = 0;
    var insTypData = {};
    $('.instalmentTyp').each(function () {
        var insc = 0;
        var insData = {};
        $(this).find('.instypSet').each(function () {
            var i = 0;
            var totsplnData = {};
            $(this).find(feeSettings.addfeeTablectrl).find('tr').each(function () {
                var splnData = {};
                $(this).find('td').each(function () {
                    var attr = $(this).attr('class');
                    if (typeof attr !== typeof undefined && attr !== false) {
                        console.log($(this).attr('class').split(' '))
                        var name = $(this).attr('class').split(' ')[0];
                        splnData[name] = $.trim($(this).text());
                    }
                });
                if (!$.isEmptyObject(splnData)) {
                    totsplnData[i] = splnData;
                    i++;
                }
            });
//console.log(totsplnData);
            if (!$.isEmptyObject(totsplnData)) {
//                console.log(insc)
                insData[insc] = totsplnData;
                insc++;
                i = 0;
                totsplnData = {};
            }
        });
//console.log(insData); return false;
        if (!$.isEmptyObject(insData)) {
            insTypData[typc] = insData;
            typc++;
            insc = 0;
            insData = {};
        }
    });
    formData.append('splitup', JSON.stringify(insTypData));
    formData.append('confirmAssign', confirmAssign);
    console.log(formData)
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.feeAjaxurl + "/addNewFeeWithIns",
            data: formData,
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function (res) {
                if (res) {
                    console.log(res)
                    console.log(res.type)
                    if (res['type'] == 'error') {
                        $('.msgBox-title').html('Error');
                        $('.msgBox-body').html(res.message);
                        $('#messageBoxModal').modal('show');
                        $('#app_feeadd').removeClass('hide');
                        $('.loading_addfee').addClass('hide');
                        return false;
                    }
                    else if (res['type'] == 'success') {
                        swal({
                            title: "Done",
                            text: res['message'],
                            type: "success",
                            confirmButtonText: 'Ok!',
                            closeOnConfirm: true,
                        },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        $('#addFeeDiv').html('');
                                        feeclubSettings.loadaddFeeInst();
                                    }
                                });
                    }

                }
            }
        });
    } else {
        $('.msgBox-title').html('Error');
        $('.msgBox-body').html(message);
        $('#messageBoxModal').modal('show');
        return false;
    }

};
feeSettings.loadInstallments = function (ctrl) {
    var feemasterid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/loadInsNodes",
        data: {feemasterid: feemasterid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent('td').next('td').removeClass('hide')
            $(ctrl).parent('td').next('td').html(res)
        }
    });
};
feeSettings.loadSubtree = function (ctrl) {
//    console.log($(ctrl).parent().parent().next('.subtreediv').html())
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/loadSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.subtreediv').removeClass('hide')
            $(ctrl).parent().parent().next('.subtreediv').html(res)
        }
    });
};

feeSettings.loadFeeAssSubtree = function (ctrl) {
//    console.log($(ctrl).parent().parent().next('.feeDetdiv').html())
    var orgvalueid = $(ctrl).val();
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/loadFeeAssSubtree",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $(ctrl).parent().parent().next('.feeDetdiv').removeClass('hide')
            $(ctrl).parent().parent().next('.feeDetdiv').html(res)
        }
    });
};

feeSettings.loadFeeStudents = function () {
    var searchData = {};
    var error = '';
    console.log('ai')
    var err = 1;
    var stuname = '';
    $("#FeeAsigningForm").find('select,input').each(function () {
//        console.log($(this).attr('name')+' : '+$(this).val())
        stuname = $('#stu_feeAssname').val();
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '' || $.trim($(this).val()) == '0') {
                err = 0;
                error += $(this).attr('title') + " is required!<br>";
            } else {
                searchData[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
//              console.log(searchData)
            searchData[$(this).attr('name')] = $.trim($(this).val());

        }

    });
    console.log(searchData)
    searchData.err = err;
    feeSettings.feeAssSearchdata = searchData;
    console.log(feeSettings.feeAssSearchdata)

    if (searchData.err == 1 || searchData.stu_feeAssselectedID != '') {
        $.ajax({
            type: "POST",
            url: commonSettings.feeAjaxurl + "/loadFeeStudents",
            data: searchData,
            success: function (res) {
                if (res) {
                    $('#StudentDet').html('');
                    $('#StudentDet').removeClass('hide')
                    $('#StudentDet').html(res);

//                    feeSettings.loadFeeLst();
                }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
        return false;

    }

};

feeSettings.loadFeeLst = function () {
    var searchData = {};
    var error = '';
    var err = 1;
    var feename = '', stuname = '';
    $("#FeeSelectForm").find('select,input').each(function () {
        searchData[$(this).attr('name')] = $.trim($(this).val());

    });
    $("#FeeAsigningForm").find('select,input').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '' || $.trim($(this).val()) == '0') {
                err = 0;
                error += $(this).attr('title') + " is required!<br>";
            } else {
                searchData[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
//              console.log(searchData)
            searchData[$(this).attr('name')] = $.trim($(this).val());

        }

    });
    console.log(searchData)
    searchData.err = err;
    feeSettings.feeAssSearchdata = searchData;
    console.log(feeSettings.feeAssSearchdata)

    if (searchData.err == 1) {
        $.ajax({
            type: "POST",
            url: commonSettings.feeAjaxurl + "/loadFeeLst",
            data: searchData,
            success: function (res) {
                if (res) {
                    $('#FeeDet').html('');
                    $('#FeeDet').removeClass('hide')
                    $('#FeeDet').html(res);
                }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        err = 0;
        return false;

    }

};

feeSettings.assignFeeToSelectedStudents = function () {
    var stuIds = [];
    var params = {};
    var feeid = '0';
    $("#assignStuoForm").find('input[type=checkbox]').each(function () {
        if ($(this).is(':checked'))
        {
            var chckval = $(this).closest('tr').attr('stuId') ? $(this).closest('tr').attr('stuId') : '';
            if (chckval !== '')
            {
                stuIds.push($(this).closest('tr').attr('stuId'));
            }
        }

    });
    $("#assignFeeForm").find('input[type=radio]').each(function () {
        if ($(this).is(':checked'))
            feeid = $(this).attr('feeId');

    });

    params['stuids'] = stuIds;
    params['feeid'] = feeid;
    console.log(params)
    if (stuIds.length > 0) {
        if (feeid != 0) {
            $.ajax({
                type: "POST",
                url: commonSettings.feeAjaxurl + "/isCheckFeeApproval",
                data: {feeid: feeid},
                dataType: 'JSON',
                success: function (res) {
                    if (res.type === 'yes') {
                        console.log('true')
                        $.ajax({
                            type: "POST",
                            url: commonSettings.approvalAjaxurl + "/approvalForwad",
                            success: function (res) {
                                //console.log($.trim(res))
                                $('.formBox-title').html('Forward request to');
                                $('.formBox-body').html(res);
                                $('#formModal').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                                $('#formModal').modal('show')
                                $('#confirm_close').addClass('hide');
                                $('#iconclose').addClass('hide');
                                $('#formModal #confirm_save').html('Forward');
                                console.log('1clciked')
                                $('#confirm_save').unbind('click').click(function () {
                                    $('#confirm_close').click();

                                    var err = 1;
                                    $("#searchStaffSingle").find('input,textarea').each(function () {
                                        if ($.trim($(this).val()) != '') {
                                            params[$(this).attr('name')] = ($.trim($(this).val()));
                                        }
                                        else {
                                            err = 0;
                                        }
                                    });
                                    if (err != 1) {
                                        swal({
                                            title: "Error",
                                            text: "Fill all fields!",
                                            type: "error",
                                            confirmButtonText: 'OK!',
                                            closeOnConfirm: true,
                                        },
                                                function (isConfirm) {
                                                    if (isConfirm) {
                                                        $('#formModal').modal('show')
                                                    }
                                                });
                                    } else {
                                        feeSettings.apprvlFeeRequest(params);
                                    }
                                });
                            }
                        });
                    } else {
                        var current_effect = 'bounce';
                        //stuFeelist
                        feeSettings.run_waitMe(current_effect);

                        var recalculate_transctn = 'yes';
                        var change_ldgr_balnc = 'yes';
                        $.ajax({
                            type: "POST",
                            url: commonSettings.feeAjaxurl + "/assignFeeToSelectedStu",
                            data: params,
                            dataType: "JSON",
                            success: function (res) {
                                if (res) {
                                    $('#other_section').waitMe('hide');
                                    var lblcls = res.type == 'error' ? 'danger' : 'success';
                                    $('.msgBox-title').html('Message');
                                    $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                                    $('#messageBox').click();
//                                    feeSettings.loadFeeLst()
                                    feeclubSettings.loadFeeAssigning();
                                    return false;
                                }

                            }
                        });

                    }
                }
            });
        } else {
            $('.msgBox-title').html('Message');
            $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">Select valid fee</div>');
            $('#messageBox').click();
            err = 0;
            return false;
        }
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">Select atleast one student</div>');
        $('#messageBox').click();
        err = 0;
        return false;
    }
};



feeSettings.apprvlFeeRequest = function (params) {
    var current_effect = 'bounce';
    //stuFeelist
    feeSettings.run_waitMe(current_effect);

    var recalculate_transctn = 'yes';
    var change_ldgr_balnc = 'yes';
    params['appType'] = 'Fee Assign';
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/saveStuFeeRequest",
        data: params,
        dataType: "JSON",
        success: function (res) {
            if (res) {
                $('#other_section').waitMe('hide');
                var lblcls = res.type == 'error' ? 'danger' : 'success';
                $('.msgBox-title').html('Message');
                $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                $('#messageBox').click();
                feeclubSettings.loadFeeAssigning();
                return false;
            }

        }
    });
};


feeSettings.changeFeeAssignReqStatus = function (statusData) {
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/changeFeeReqApprovalStatus",
        data: statusData,
        dataType: "JSON",
        success: function (res) {
            if (res['type'] == 'error') {
                swal("Error!", res['message'], "error");
                return false;
            }
            else if (res['type'] == 'success') {
                swal({
                    title: res['type'],
                    text: res['message'],
                    type: res['type'],
                    confirmButtonText: 'OK',
                    closeOnConfirm: true,
                },
                        function (isConfirm) {
                            if (isConfirm) {
                                if (statusData.fromPage == 'FeeRequestApprvl') {
                                    approvalviewSettings.loadApptovalForFeeReqApprvl()
                                }
                            }
                        });
            }
        }
    });
};

feeSettings.loadFeeReportStudents = function () {

    var searchData = {};
    $("#StuFeeReportForm").find('input,select,textarea').each(function () {
        searchData[$(this).attr('name')] = $.trim($(this).val());

//         if ($.trim($(this).val()) == '' || $.trim($(this).val()) == '0') {
//                err = 0;
//                error += $(this).attr('title') + " is required!<br>";
//            } else {
//                searchData[$(this).attr('name')] = $.trim($(this).val());
//            }

    });
    
     var current_effect = 'bounce';
    //stuFeelist
    feeSettings.run_waitMe(current_effect);

    var recalculate_transctn = 'yes';
    var change_ldgr_balnc = 'yes';
    
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/loadFeeReportStuList",
        data: searchData,
        dataType: "html",
        success: function (res) {
              $('#other_section').waitMe('hide');
            $('#StudentFeeReportDet').html(res);
            $('#StudentFeeReportDet').removeClass('hide');
        }
    });
};


feeSettings.run_waitMe = function (effect) {
    $('#other_section').waitMe({
        effect: effect,
        text: 'Please wait loading...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: ''
    });
};