var feeSplitupSettings = {};
feeSplitupSettings.restoreRow = function(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);

    for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
        oTable.fnUpdate(aData[i], nRow, i, false);
    }

    oTable.fnDraw();

};

feeSplitupSettings.editRow = function(oTable, nRow, option) {
    var aData = oTable.fnGetData(nRow);
    var mode = option == 'new' ? 'data-mode="new"' : '';
    var jqTds = $('>td', nRow);
    jqTds[0].innerHTML = '<input type="text" class="form-control small feesplname" value="' + aData[0] + '">';
    jqTds[1].innerHTML = '<input type="text" class="form-control small feesplamount"  onkeyup="feeSplitupSettings.validateAmount(this)"  value="' + aData[1] + '">';
    jqTds[2].innerHTML = '<a class="edit" href="javascript:;" onclick ="feeSplitupSettings.editAction(this)">Save</a>';
    jqTds[3].innerHTML = '<a class="cancel" href="javascript:;" ' + mode + ' onclick= "feeSplitupSettings.cancelRow (this)">Cancel</a>';

};

feeSplitupSettings.saveRow = function(oTable, nRow) {
    var jqInputs = $('input', nRow);
    if (jqInputs[0].value == '' || jqInputs[1].value == '') {
        $('.msgBox-title').html('Error');
        $('.msgBox-body').html('Invalid Input');
        $('#messageBoxModal').modal('show');
        oTable.fnDeleteRow(nRow);
        return false;
    }
    console.log(nRow)
    $(nRow).find('td:eq( 1 )').addClass('feesplamount')
    oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
    oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
    oTable.fnUpdate('<a class="edit" href="javascript:;" onclick ="feeSplitupSettings.editAction(this)">Edit</a>', nRow, 2, false);
    oTable.fnUpdate('<a class="delete" href="javascript:;" onclick = "feeSplitupSettings.deleteRow(this)">Delete</a>', nRow, 3, false);
    oTable.fnDraw();


};

var column = [0, 1];
var sortingDisabled = [2, 3];
console.log((feeSettings.addfeeTablectrl))
var oTable = $(feeSettings.addfeeTablectrl).dataTable({
    "sDom": 'C<"clear">lrtip',
    //"bProcessing": false,
    "sPaginationType": "full_numbers",
    "iDisplayLength": commonSettings.NumberOfRecords,
    "aoColumnDefs": [{
            "sDefaultContent": "",
            "aTargets": column
        }, {
            "bSortable": false,
            "sDefaultContent": "",
            "aTargets": sortingDisabled
        }],
    "bLengthChange": false,
    "bInfo": false
});

//jQuery('#editable-sample_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
//jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

var nEditing = null;

$(feeSettings.addfeeTablectrl + '_new').click(function(e) {
    console.log(feeSettings.feeTotalAmount + ' = ' + feeSettings.itemfeeTotalAmount)
    if (feeSettings.feeTotalAmount > 1 && feeSettings.itemfeeTotalAmount < parseInt(feeSettings.feeTotalAmount)) {
        if (feeSettings.itemfeeTotalAmount != feeSettings.feeTotalAmount)
            $('#splmatch').html('<font color="red">Items\' total doesn\'t match with Fee total!</font> ' + '&nbsp;&nbsp;&nbsp;<b>SplitUp Items Total : ' + feeSettings.itemfeeTotalAmount);
        else
            $('#splmatch').html(' <font color="green">Fee total and Items\' total are equal. Proceed to save the splitup!</font> ' + '&nbsp;&nbsp;&nbsp;<b>SplitUp Items Total : ' + feeSettings.itemfeeTotalAmount);

        e.preventDefault();
        var aiNew = oTable.fnAddData(['', '', '', '',
            '<a class="edit" href="javascript:;">Edit</a>', '<a class="cancel" data-mode="new" href="javascript:;" onclick= "feeSplitupSettings.cancelRow (this)">Cancel</a>'
        ]);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        feeSplitupSettings.editRow(oTable, nRow, 'new');
        nEditing = nRow;
    } else {
        swal({
            title: "Error",
            text: "Items' total exceeds the Fee amount!",
            type: "error",
            confirmButtonText: 'Ok!',
            closeOnConfirm: true,
        },
                function(isConfirm) {
                    if (isConfirm) {
                        return false;
                    }
                });
    }
});

feeSplitupSettings.deleteRow = function(ctrl) {
//    e.preventDefault();

    if (confirm("Are you sure to delete this row ?") == false) {
        return;
    }

    var nRow = $(ctrl).parents('tr')[0];
    var jqInputs = $('td', nRow);
    console.log(jqInputs)
//    console.log(jqInputs[0].innerHTML + '' + jqInputs[1].innerHTML + '' + jqInputs[2].innerHTML + " " + $(nRow).attr('divValId'))
    var params = {};
    params.splitUpName = jqInputs[0].innerHTML;
    params.splitUpNameAmount = jqInputs[1].innerHTML;
    params.splitUpId = $(nRow).attr('splitUpId')
    console.log(params)
    $.ajax({
        type: "POST",
        url: commonSettings.feeAjaxurl + "/deletesplitup",
        data: params,
        dataType: "JSON",
        success: function(res) {
            console.log(res)
            if (res.type == 'success') {
                oTable.fnDeleteRow(nRow);

                var splAmountSum = 0;
                $(feeSettings.addfeeTablectrl).find('input.feesplamount').each(function() {
                    splAmountSum += $.trim($(this).val());
                });
                $(feeSettings.addfeeTablectrl).find('td.feesplamount').each(function() {
                    splAmountSum += parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0;
                });
                feeSettings.itemfeeTotalAmount = parseInt(splAmountSum);
                console.log((feeSettings.itemfeeTotalAmount + ' ' + feeSettings.feeTotalAmount))
                if (feeSettings.itemfeeTotalAmount < feeSettings.feeTotalAmount)
                    $('#splmatch').html('<font color="red">Items\' total doesn\'t match with Fee total!</font> ' + '&nbsp;&nbsp;&nbsp;<b>SplitUp Items Total : ' + feeSettings.itemfeeTotalAmount);
                else
                    $('#splmatch').html(' <font color="green">Fee total and Items\' total are equal. Proceed to save the splitup!</font> ' + '&nbsp;&nbsp;&nbsp;<b>SplitUp Items Total : ' + feeSettings.itemfeeTotalAmount);

            } else {

                swal({
                    title: "Error",
                    text: res['message'],
                    type: "error",
                    confirmButtonText: 'Ok!',
                    closeOnConfirm: true,
                },
                        function(isConfirm) {
                            if (isConfirm) {
                                return false;
                            }
                        });
            }

        }
    });
};


feeSplitupSettings.editAction = function(ctrl) {
    /* Get the row as a parent of the link that was clicked on */
    var nRow = $(ctrl).closest('tr')[0];
    nEditing = nRow;
    if (nEditing !== null && nEditing != nRow) {
        /* Currently editing - but not this row - restore the old before continuing to edit mode */
        feeSplitupSettings.editRow(oTable, nRow, 'edit');
        nEditing = nRow;
    } else if (nEditing == nRow && ctrl.innerHTML == "Save") {
        /* Editing this row and want to save it */
        var jqInputs = $('input', nEditing);
        console.log(jqInputs)
        if (jqInputs[0].value == '') {
            $('.msgBox-title').html('Error');
            $('.msgBox-body').html('Invalid Input');
            $('#messageBoxModal').modal('show');
            oTable.fnDeleteRow(nRow);
            return false;
        }
        var params = {};
        params.splitUpName = jqInputs[0].value;
        params.splitUpNameAmount = jqInputs[1].value;
        params.splitUpId = $(nEditing).attr('splitUpId');
        params.feeItemId = $('#feeSplitupFormTable').attr('feeItemId');

        if (feeSettings.itemfeeTotalAmount > feeSettings.feeTotalAmount)
        {
            $('#splmatch').html('<font color="red">Items\' total exceeds the Fee total!</font> ' + '&nbsp;&nbsp;&nbsp;<b>SplitUp Items Total : ' + feeSettings.itemfeeTotalAmount);
            swal({
                title: "Error",
                text: 'Items\' total exceeds the Fee total!',
                type: "error",
                confirmButtonText: 'Ok!',
                closeOnConfirm: true,
            },
                    function(isConfirm) {
                        if (isConfirm) {
                            return false;
                        }
                    });
        } else {

            console.log(params)
            $.ajax({
                type: "POST",
                url: commonSettings.feeAjaxurl + "/saveSplitup",
                data: params,
                dataType: "JSON",
                success: function(res) {
                    if (res) {
                        if (res.type != 'success') {

                            swal({
                                title: "Error",
                                text: res.message,
                                type: res.type,
                                confirmButtonText: 'Ok!',
                                closeOnConfirm: true,
                            },
                                    function(isConfirm) {
                                        if (isConfirm) {
                                            if (res['type'] == 'error') {
                                                oTable.fnDeleteRow(nEditing);
                                                return false;
                                            } else {

                                                $(nEditing).attr('feeItemId', res.feeItemId)
                                                $(nEditing).attr('splitUpId', res.splitUpId)
                                                nEditing = null;
                                            }
                                        }
                                    });


                        } else {
                            $(nEditing).attr('splitUpId', res.splitUpId)
                            $(nEditing).attr('feeItemId', res.feeItemId)
                            feeSplitupSettings.saveRow(oTable, nEditing);
                            nEditing = null;
                        }


                    }
                }
            });
        }
    } else {
        /* No edit in progress - let's start one */
        feeSplitupSettings.editRow(oTable, nRow, 'edit');
        nEditing = nRow;
    }
}

feeSplitupSettings.cancelRow = function(ctrl) {
    console.log("cancelit")
    console.log($(ctrl).html())
//    e.preventDefault();
    if ($(ctrl).attr("data-mode") == "new") {
        var nRow = $(ctrl).parents('tr')[0];
        console.log($(ctrl).parents('tr').html())
        oTable.fnDeleteRow(nRow);

        var splAmountSum = 0;
        $(feeSettings.addfeeTablectrl).find('input.feesplamount').each(function() {
            splAmountSum += parseInt($.trim($(this).val())) > 0 ? parseInt($.trim($(this).val())) : 0;
        });

//        console.log('isplAmountSum:'+splAmountSum)
        $(feeSettings.addfeeTablectrl).find('td.feesplamount').each(function() {
            splAmountSum += parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0;
        });
//        console.log('tsplAmountSum:'+ssplAmountSum)
        feeSettings.itemfeeTotalAmount = parseInt(splAmountSum);
        console.log((feeSettings.itemfeeTotalAmount + ' ' + feeSettings.feeTotalAmount))
        if (feeSettings.itemfeeTotalAmount != feeSettings.feeTotalAmount)
            $('#splmatch').html('<font color="red">Items\' total doesn\'t match with Fee total!</font> ' + '&nbsp;&nbsp;&nbsp;<b>SplitUp Items Total : ' + feeSettings.itemfeeTotalAmount);
        else
            $('#splmatch').html(' <font color="green">Fee total and Items\' total are equal. Proceed to save the splitup!</font> ' + '&nbsp;&nbsp;&nbsp;<b>SplitUp Items Total : ' + feeSettings.itemfeeTotalAmount);

        //alert("Deleted! Do not forget to do some ajax to sync with backend :)");
    } else {
        feeSplitupSettings.restoreRow(oTable, nEditing);
        // alert("Deleted! Do not forget to do some ajax to sync with backend restore:)");
        nEditing = null;
    }


};
feeSplitupSettings.validateAmount = function(ctrl) {
    var $this = $(ctrl);
    var splamount = $this.val().replace(/,/g, "");
    console.log(parseInt(splamount))
    if (parseInt(splamount) > 0) {

        var splAmountSum = 0;
        $(feeSettings.addfeeTablectrl).find('input.feesplamount').each(function() {
            splAmountSum += parseInt($.trim($(this).val())) ? parseInt($.trim($(this).val())) : 0;
        });

        $(feeSettings.addfeeTablectrl).find('td.feesplamount').each(function() {
            console.log('splitamt:' + parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0)
            splAmountSum += parseInt($.trim($(this).html())) > 0 ? parseInt($.trim($(this).html())) : 0;
        });
        feeSettings.itemfeeTotalAmount = parseInt(splAmountSum);
        console.log((feeSettings.itemfeeTotalAmount + ' ' + feeSettings.feeTotalAmount))
        if (feeSettings.itemfeeTotalAmount != feeSettings.feeTotalAmount)
            $('#splmatch').html('<font color="red">Items\' total doesn\'t match with Fee total!</font> ' + '&nbsp;&nbsp;&nbsp;<b>SplitUp Items Total : ' + feeSettings.itemfeeTotalAmount);
        else
            $('#splmatch').html(' <font color="green">Fee total and Items\' total are equal. Proceed to save the splitup!</font> ' + '&nbsp;&nbsp;&nbsp;<b>SplitUp Items Total : ' + feeSettings.itemfeeTotalAmount);


    } else {
        swal({
            title: "Error",
            text: "Enter a valid item fee amount",
            type: "error",
            confirmButtonText: 'Ok!',
            closeOnConfirm: true,
        },
                function(isConfirm) {
                    if (isConfirm) {
                        $this.val('')
                    }
                });
    }

};