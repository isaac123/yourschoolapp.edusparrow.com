var staffratingSettings = {};


staffratingSettings.updateStaffRatingCategory = function() {
    var i = 0;
    var name = {};
    var weightage = {};
    var input = {};
    var total_weightage = 0;
    var error = 0;
    var hidden_count = 0;
    var hidden = {};
    $(".name").each(function() {

        if ($(this).parent('div').parent('div').is(":visible"))
        {
            if ($(this).val().length > 0)
            {
                name[i++] = ($(this).val());
            } else {
                error = 1;
            }
        } else {
            hidden[hidden_count++] = ($(this).val());

        }
    })

    i = 0;

    if (!error) {
        $(".weightage").each(function() {
            if ($(this).parent('div').parent('div').is(":visible"))
            {
                if ($(this).val().length > 0)
                {
                    weightage[i] = parseInt(($(this).val()));
                    total_weightage = total_weightage + weightage[i];
                    i++;
                } else {
                    error = 2;
                }
            }
        })
    }
    if (!error) {
        if (total_weightage != 100)
        {
            error = 3;
        }
    }

    if (error == 1) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Category Name is missing</div>');
        $('#messageBox').click();
    }
    else if (error == 2) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Weightage is missing</div>');
        $('#messageBox').click();
    }
    else if (error == 3) {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Sum of all the weightage values should be 100</div>');
        $('#messageBox').click();
    }
    else {

        for (j = 0; j < i; j++) {
            input[j] = new Array(name[j], weightage[j]);
        }
        input["count"] = i;
        input["hidden"] = hidden;
        input["hidden_count"] = hidden_count;

        //alert(JSON.stringify(input));

        $.ajax({
            type: "POST",
            url: commonSettings.stfratingAjaxurl + "/updateStaffRatingCategory",
            data: input,
            success: function(res) {
                if (res) {
                    $('#Rating').addClass('hide');
                    $('#RatingLevel').html(res);
                }
            }
        });
        //alert(JSON.stringify(input));
        //alert(JSON.stringify(weightage));
    }

};

staffratingSettings.createRatingLevel = function(id) {
    var wid = "#wrapper_" + id;

    $(wid).append('<div class="form-group"><div class = "col-md-12">\n\
<div class = "col-md-3"></div><div class = "col-md-3"><input type="text" name="rating_name" class = "ratingname form-control" placeholder = "Rating Name"/></div>\n\
<div class = "col-md-3"><input type="number" name="rating_weightage" class = "ratingweightage form-control" placeholder = "0" min ="0" max="100"/></div>\n\
<a href="javascript:staffratingSettings.removeRatingLevel(' + "#wrapper_" + id + ')" class="remove_field">Remove</a></div></div>');

};



staffratingSettings.updateStaffRatingLevel = function() {

    var total_count = 0;
    var error = 0;
    var split_id = "";
    var input = {};
    var hidden_count = 0;
    var hidden = {};
    var names = {};

    $(".wrapper_class").each(function() {
        //alert(($(this).val()));
        if (!error) {
            var name = {};
            var weightage = {};

            var i = 0;
            split_id = $(this).children('div').children('.categoryname').val();


            $(this).children('div').children('div').children('div').children('.ratingname').each(function() {
                if ($(this).parent('div').parent('div').is(":visible"))
                {
                    if ($(this).val().length > 0)
                    {
                        name[i++] = ($(this).val());
                    } else {
                        error = 1;
                    }
                } else {
                    hidden[hidden_count] = $(this).val();
                    names[hidden_count] = split_id;
                    //alert(hidden[hidden_count]);
                    //alert(names[hidden_count]);
                    hidden_count++;
                }
            });

            i = 0;

            if (!error) {
                $(this).children('div').children('div').children('div').children('.ratingweightage').each(function() {
                    if ($(this).parent('div').parent('div').is(":visible"))
                    {
                        if ($(this).val().length > 0)
                        {
                            weightage[i++] = parseInt(($(this).val()));
                        } else {
                            error = 2;
                        }
                    }
                });
            }




            if (error == 1) {
                $('.modal-title').html('Message');
                $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Rating Name is Missing for Category: ' + split_id + '</div>');
                $('#messageBox').click();
            }
            else if (error == 2) {
                $('.modal-title').html('Message');
                $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Rating Weightage is Missing for Category: ' + split_id + '</div>');
                $('#messageBox').click();
            }
            else {

                for (j = total_count; j < total_count + i; j++) {
                    input[j] = new Array(split_id, name[j - total_count], weightage[j - total_count]);
                }
                total_count = total_count + i;
                //alert(total_count);
                //alert(JSON.stringify(input));
            }
        }
    });

    if (!error) {

        input["total_count"] = total_count;
        input["hidden"] = hidden;
        input["names"] = names;
        input["hidden_count"] = hidden_count;
        //alert(hidden_count);
        //input["hidden"] = hidden;
        //input["hidden_count"] = hidden_count;

        //alert(JSON.stringify(input));

        $.ajax({
            type: "POST",
            url: commonSettings.stfratingAjaxurl + "/updateStaffRatingLevel",
            data: input,
            success: function(res) {
                if (res) {

                    $('.msgBox-title').html('Message');
                    $('.msgBox-body').html(res);
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function() {
//                        window.location.reload();
                        staffratingSettings.loadartSett();
                    });
                }
            }
        });
    }

};

staffratingSettings.loadSection = function(sval) {
    //console.log(sval);
    staffratingSettings.classID = sval;
    staffratingSettings.className = $("#divID option:selected").text();
    $.ajax({
        type: "POST",
        url: commonSettings.stfratingAjaxurl + "/loadSectionsByClass",
        data: {
            classId: sval
        },
        success: function(res) {
            if (res) {
                $('#loadSection').html(res);
                $('#loadSection').removeClass('hide');
                $('#loadStaffRating').addClass('hide');

            }

        }
    });

};
staffratingSettings.loadStaffListForRating = function() {
     $.ajax({
            type: "POST",
            url: commonSettings.stfratingAjaxurl + '/loadStaffRating',
            success: function(res) {
                if (res) {
                  
                    
                       $('#ratingdiv').html(res);
                        $('#ratingdiv').removeClass('hide')
                }

            }
        });
};

staffratingSettings.loadStaffRating = function(val) {
    if (val != -1)
    {
        staffratingSettings.subDivVal = val;
        staffratingSettings.subDivName = $("#subDivVal option:selected").text();        
        staffratingSettings.classID = val;
        staffratingSettings.className = $("#divID option:selected").text();

        $return_uri = $("#return_uri").val();
        $.ajax({
            type: "POST",
            url: commonSettings.stfratingAjaxurl + '/' + $return_uri,
            data: {
                class_id: staffratingSettings.classID,
                class_name: staffratingSettings.className
                        //subDivVal: staffratingSettings.subDivVal,
                        //subdiv_name: staffratingSettings.subDivName,
            },
            success: function(res) {
                if (res) {
                    //alert(res);
                    //$('#mainDiv').append(res);
                    $('#loadStaffRating').html(res);
                    $('#loadStaffRating').removeClass('hide');
                }

            }
        });
    }
};

staffratingSettings.editStaffRating = function(stud_id, row_id, category_count) {

    var table = document.getElementById("staff_rating");
    var row = table.rows[row_id];

    for (var i = 0; i < category_count; i++) {
        var e = row.getElementsByTagName("select")[i];
        $('#' + e.id).prop('disabled', false);
    }

    $('#' + row_id).html("<a href='javascript:staffratingSettings.submitStaffRating(" +
            stud_id + "," + row_id + "," + category_count + ")'><u>Submit</u></a>");
    $('#div_' + row_id).html("");
};

staffratingSettings.submitStaffRating = function(stud_id, row_id, category_count) {
    var table = document.getElementById("staff_rating");
    var row = table.rows[row_id];
    var error = 0;
    var input = {};

    for (var i = 0; i < category_count; i++) {
        var e = row.getElementsByTagName("select")[i];
        var rating_level_id = e.options[e.selectedIndex].value;
        var split_id = e.id.split('_');
        var rating_category_id = split_id[0];

        if (rating_level_id == -1) {
            error = 1;
            $('.modal-title').html('Message');
            $('.modal-body').html('<div class="alert alert-block alert-danger fade in">One of the Rating is missing.</div>');
            $('#messageBox').click();
            break;
        }
        else {
            input[i] = new Array(rating_category_id, rating_level_id);
        }
    }

    if (!error) {
       // input['class_id'] = staffratingSettings.classID;
        input['stud_id'] = stud_id;
        input['category_count'] = category_count;
        input['return_uri'] = $("#return_uri").val();

        $.ajax({
            type: "POST",
            url: commonSettings.stfratingAjaxurl + "/updateStaffRating",
            data: input,
            success: function(res) {
                if (res == 1) {
                    $('#' + row_id).html("<a href='javascript:staffratingSettings.editStaffRating(" + stud_id + "," + row_id + "," + category_count + ")'><u>Edit</u></a>");
                    $('#div_' + row_id).html("Rating update successfull");
                    $('#div_' + row_id).css("color", "green");


                    for (var i = 0; i < category_count; i++) {
                        var e = row.getElementsByTagName("select")[i];
                        $('#' + e.id).prop('disabled', true);
                    }
                }

            }
        });
    }

};


staffratingSettings.loadartSett = function() {
    $('#staffactivity').empty()
    $.ajax({
        type: "POST",
        url: commonSettings.stfratingAjaxurl + "/staffRatingSettings",
        dataType: "html",
        success: function(res) {
            $('#ratingsettingsDiv').html(res);
            $('#ratingsettingsDiv').removeClass('hide')
        }
    });
};
staffratingSettings.loadStaffratn = function() {
    $.ajax({
        type: "POST",
        url: commonSettings.stfratingAjaxurl + "/addStaffRating",
        dataType: "html",
        success: function(res) {
            $('#ratingdiv').html(res);
            $('#ratingdiv').removeClass('hide')
        }
    });
};
staffratingSettings.getAttendancePeriods = function (orgvalueid) {
    $.ajax({
        type: "POST",
        url: commonSettings.stfratingAjaxurl + "/getAttendancePeriods",
        data: {orgvalueid: orgvalueid},
        dataType: "html",
        success: function (res) {
            $('#attendance_period').html(res);
            $('#attendance_period').removeClass('hide');
        }
    });
};
staffratingSettings.assignPeriods = function (node_id) {
    $.ajax({
        type: "POST",
        url: commonSettings.stfratingAjaxurl + "/assignPeriods",
        data: {node_id: node_id},
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('.modal-title').html('Add Periods');
                $('.modal-body').html(res);
                $('#formModal').modal('show');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    staffratingSettings.addPeriods(node_id);
                });
            }
        }
    });
};
staffratingSettings.addPeriods = function (node_id) {
    var params = {};
    var err = 0;
    var error = '';
    $("#createPeriodsForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n<br>";

            } else {
                params[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.stfratingAjaxurl + "/updatePeriods",
            data: params,
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                        var lblcls = res.type == 'error' ? 'danger' : 'success';
                        $('.msgBox-title').html('Message');
                        $('.msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                        $('#messageBoxModal').modal();
                        $('#msg_ok').unbind('click').click(function (event) {
                            $('#messageBoxModal').modal('hide');
                           if (res['type'] == 'success') { staffratingSettings.getAttendancePeriods(res.ID);}
                        });
                        return false;
                    }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
         $('#msg_ok').unbind('click').click(function () {                 
                    staffratingSettings.assignPeriods(node_id);
                });
        err = 0;
    }
};
staffratingSettings.editAttnPeriods = function (id) {
    $.ajax({
        type: "POST",
        url: commonSettings.stfratingAjaxurl + "/editAttnPeriods",
        data: {id: id},
        dataType: 'html',
        success: function (res) {
            if (res) {
                $('.modal-title').html('Edit Periods');
                $('.modal-body').html(res);
                $('#formModal').modal('show');
                $('#confirm_save').unbind('click').click(function () {
                    $('#confirm_close').click();
                    staffratingSettings.updatePeriods(id);
                });
            }
        }
    });
};
staffratingSettings.updatePeriods = function (id) {
    var params = {};
    var err = 0;
    var error = '';
    $("#editAttPeriodsForm").find('input,select,textarea').each(function () {
        if ($(this).parent().prev('label').hasClass('mandatory')) {
            if ($.trim($(this).val()) == '') {
                err = 1;
                error += $(this).attr('title') + " is required!\n<br>";

            } else {
                params[$(this).attr('name')] = $.trim($(this).val());
            }

        } else {
            params[$(this).attr('name')] = $.trim($(this).val());
        }
    });
    console.log(params);
    if (err == 0) {
        $.ajax({
            type: "POST",
            url: commonSettings.stfratingAjaxurl + "/updatePeriods",
            data: params,
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    swal({
                        title: res.type,
                        text: res.message,
                        type: res.type,
                        confirmButtonText: 'OK!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    staffratingSettings.getAttendancePeriods(res.ID);
                                }
                            });
                }

            }
        });
    } else {
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('<div class="alert alert-block alert-danger fade in">' + error + '</div>');
        $('#messageBox').click();
        $('#msg_ok').unbind('click').click(function () {
            staffratingSettings.editAttnPeriods(id);
        });
        err = 0;
    }
};
staffratingSettings.deleteAttnPeriods = function (id) {
    $('.cnfBox-title').html('Confirm Delete');
    $('.cnfBox-body').html('Do you want to delete this period ?');
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');
        $.ajax({
            type: "POST",
            url: commonSettings.stfratingAjaxurl + "/deleteAttnPeriods",
            data: {id: id},
            dataType: 'JSON',
            success: function (res) {
                if (res) {
                    swal({
                        title: res.type,
                        text: res.message,
                        type: res.type,
                        confirmButtonText: 'OK!',
                        closeOnConfirm: true,
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    staffratingSettings.getAttendancePeriods(res.ID);
                                }
                            });
                }
            }
        });
    });
};