var approvalSettings = {};
approvalSettings.request = '';
approvalSettings.listqueryparams = {};
approvalSettings.checkReqStatus = function (appTypeData) {
    console.log(appTypeData);
    if (appTypeData['type']) {
        $.ajax({
            type: "POST",
            url: commonSettings.approvalAjaxurl + "/isApprovalRequired",
            data: {
                type: appTypeData['type']
            },
            dataType: "JSON",
            success: function (res) {
                console.log(res)
                if (res.message === 'yes') {
                    console.log(appTypeData)
                    $.ajax({
                        type: "POST",
                        url: commonSettings.approvalAjaxurl + "/approvalForwad",
                        data: appTypeData,
                        success: function (res) {
                            //console.log($.trim(res))
                            $('.formBox-title').html('Forward request to');
                            $('.formBox-body').html(res);
                            $('#formModal').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            $('#formModal').modal('show')
                            $('#formModal').find('#comments').val('');
                            $('#formModal').find('#comments').val(appTypeData.voucheramountfor)
                            $('#confirm_close').addClass('hide');
                            $('#iconclose').addClass('hide');
                            $('#formModal #confirm_save').html('Forward');
                            console.log('1clciked')
                            $('#confirm_save').unbind('click').click(function () {
                                $('#confirm_close').click();
                                var err = 1;
                                $("#searchStaffSingle").find('input,textarea').each(function () {
                                    if ($.trim($(this).val()) != '') {
                                        appTypeData[$(this).attr('name')] = ($.trim($(this).val()));
                                    }
                                    else {
                                        err = 0;
                                    }
                                });
                                if (err != 1) {

                                    $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
                                    $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Fill all fields!</span>');
                                    $('#messageBoxModal').modal('show');
                                    $('#msg_ok').unbind('click').click(function (event) {
                                        $('#messageBoxModal').modal('hide');
                                        $('#formModal').modal('show')
                                    });
                                    return false;
                                } else {
                                    approvalSettings.addNewApproval(appTypeData);
                                }
                            });
                        }
                    });
                } else if (res.message == 'default') {
                    console.log(res)
                    appTypeData['forwardTo'] = res.forwardto;
                    appTypeData['comments'] = 'Forwarded for approval';
                    approvalSettings.addNewApproval(appTypeData);
                } else if (res.message == 'no') {
                    appTypeData.status = 'Approved';
                    appTypeData.appTypeId = res.appTypeid;
                    approvalSettings.changeStatusForType(appTypeData);
                } else {

                    $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
                    $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">' + res.message + '</span>');
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function (event) {
                        $('#messageBoxModal').modal('hide');
                        $('#formModal').modal('show')
                    });
                    return false;
                }

            }
        });
    }
}

approvalSettings.addNewApproval = function (appData) {
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/addNewApproval",
        data: appData,
        dataType: "JSON",
        success: function (resArr) {
            resArr.fromPage = appData.fromPage;
            resArr.voucheramount = appData.voucheramount;

            resArr.fromPageaction = appData.fromPageaction ? appData.fromPageaction : '';
//            resArr['voucheramount'] = appData.voucheramount;
            console.log(resArr)
            if (resArr['type'] == 'error') {
                return false;
            }
            else if (resArr['type'] == 'success') {
                approvalSettings.changeStatusForType(resArr)
            }
        }
    });
};
approvalSettings.viewAppDetails = function (ctrl) {
    var itemID = $(ctrl).closest('tr').attr('itemID');
    var appTypId = $(ctrl).closest('tr').attr('appType');

    if (appTypId == 1) {
        leaveSettings.viewAppDetails(itemID);
    }
    if (appTypId == 2) {
        applicationSettings.viewAppDetails(itemID);
    }
};
approvalSettings.forward = function (ctrl) {
    var action = $(ctrl).attr('action');
    if(action == 'view'){
        viewapproval.modal('hide'); 
    }
    var appData = {};

    appData.appitemID = $(ctrl).closest('tr').attr('appitemID');
    appData.appType = $(ctrl).closest('tr').attr('appType');
    appData.frompage = $(ctrl).closest('tr').attr('frompage');
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/approvalForwad",
        success: function (res) {
            //console.log($.trim(res))
            $('.formBox-title').html('Forward request to');
            $('.formBox-body').html(res);
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#formModal').modal('show')
            $('#confirm_close').addClass('hide');
            $('#confirm_save').html('Forward');
            $('#iconclose').addClass('hide');
            console.log('2clciked')
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                var err = 1;
                $("#searchStaffSingle").find('input,textarea').each(function () {
                    if ($.trim($(this).val()) != '') {
                        appData[$(this).attr('name')] = ($.trim($(this).val()));
                    }
                    else {
                        err = 0;
                    }
                });
                if (err != 1) {

                    $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
                    $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Fill all fields!</span>');
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function (event) {
                        $('#messageBoxModal').modal('hide');
                        $('#formModal').modal('show')
                    });
                    return false;
                } else {
                    approvalSettings.forwardApproval(appData);
                }
            });
        }
    });
};
approvalSettings.forwardApproval = function (appData) {
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/escalateAppItem",
        data: appData,
        dataType: "JSON",
        success: function (resArr) {
            console.log(resArr)
            if (resArr['type'] == 'error') {
                return false;
            }
            else if (resArr['type'] == 'success') {
                resArr.fromPage = appData.frompage;
                approvalSettings.changeStatusForType(resArr)
            }
        }
    });
};
approvalSettings.approve = function (ctrl) {
     var action = $(ctrl).attr('action');
    if(action == 'view'){
        viewapproval.modal('hide'); 
    }
    var appData = {};
    appData.appitemID = $(ctrl).closest('tr').attr('appitemID');
    appData.appType = $(ctrl).closest('tr').attr('appType');
    appData.fromPage = $(ctrl).closest('tr').attr('fromPage');
    $('#ConfirmModal').find('.modal-title').html('Confirm');
    $('#ConfirmModal').find('.modal-body').html("Are you sure to approve ?");
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');

        $('#formModal .formBox-title').html('Add Comments Approve');
        $('#confirm_close').addClass('hide');
        $('#iconclose').addClass('hide');
        $('#confirm_save').html('submit');
        $('.formBox-body').html('<form class="form-horizontal" ><div class="form-group"><label for="" class="col-sm-3 control-label">Enter Comments:</label><div class="col-sm-6"><textarea id="comments" name="comments" class="form-control" cols="10" rows="4"></textarea></div></div></form>');
        $('#formModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#formModal').modal('show')
        $('#confirm_save').html('submit');
        $('#confirm_save').unbind('click').click(function () {
            $('#confirm_close').click();
            if ($('#comments').val() === '') {

                $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
                $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Comment is required</span>');
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function (event) {
                    $('#messageBoxModal').modal('hide');
                    $('#formModal').modal('show')
                });
            } else {
                appData.comments = $('#comments').val()
                $('#formModal').modal('hide')
                $.ajax({
                    type: "POST",
                    url: commonSettings.approvalAjaxurl + "/approveByAppItem",
                    data: appData,
                    dataType: "JSON",
                    success: function (resArr) {
                        console.log(resArr)
                        if (resArr['type'] == 'error') {
                            return false;
                        }
                        else if (resArr['type'] == 'success') {

                            resArr.fromPage = appData.fromPage;
                            approvalSettings.changeStatusForType(resArr)
                        }
                    }
                });
            }
        });

    });
};
approvalSettings.reject = function (ctrl) {
     var action = $(ctrl).attr('action');
    if(action == 'view'){
        viewapproval.modal('hide'); 
    }
    var appData = {};
    appData.appitemID = $(ctrl).closest('tr').attr('appitemID');
    appData.appType = $(ctrl).closest('tr').attr('appType');
    appData.fromPage = $(ctrl).closest('tr').attr('fromPage');
    $('#ConfirmModal').find('.modal-title').html('Confirm');
    $('#ConfirmModal').find('.modal-body').html("Are you sure to reject this request ?");
    $('#ConfirmModal').modal();
    $('#confirm_yes').unbind('click').click(function (event) {
        $('#ConfirmModal').modal('hide');

        $('.formBox-title').html('Add Comments Reject');
        $('#confirm_close').addClass('hide');
        $('#confirm_save').html('submit');
        $('#iconclose').addClass('hide');
        $('.formBox-body').html('<form class="form-horizontal" ><div class="form-group"><label for="" class="col-sm-3 control-label">Enter Comments:</label><div class="col-sm-6"><textarea id="comments" name="comments" class="form-control" cols="10" rows="4"></textarea></div></div></form>');
        $('#formModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#formModal').modal('show')
        $('#confirm_save').unbind('click').click(function () {
            $('#confirm_close').click();
            if ($('#comments').val() === '') {
                $('#messageBoxModal .msgBox-title').html('<i class="fa  fa-times"></i> Error');
                $('#messageBoxModal .msgBox-body').html('<span class="text text-danger fade in">Comment is required</span>');
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function (event) {
                    $('#messageBoxModal').modal('hide');
                    $('#formModal').modal('show')
                });
            } else {
                appData.comments = $('#comments').val()
                $('#formModal').modal('hide')
                approvalSettings.rejectReq(appData)
            }
        });

    });
};
approvalSettings.rejectReq = function (appData) {

    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/rejectByAppItem",
        data: appData,
        dataType: "JSON",
        success: function (resArr) {
            console.log(resArr)
            if (resArr['type'] == 'error') {
                return false;
            }
            else if (resArr['type'] == 'success') {

                resArr.fromPage = appData.fromPage;
                approvalSettings.changeStatusForType(resArr)
            }
        }
    });
}
approvalSettings.viewForwardDetails = function (ctrl) {
    var appData = {};
    appData.itemID = $(ctrl).closest('tr').attr('itemID');
    appData.appTypId = $(ctrl).closest('tr').attr('appType');
    appData.appitemID = $(ctrl).closest('tr').attr('appitemID');
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/viewForwardHistory",
        data: appData,
        //dataType: "JSON",
        success: function (res) {
//            console.log(res)
            $('#messageBoxModal .msgBox-title').html('Approval Request History');
            $('#messageBoxModal .msgBox-body').html(res);
            $('##messageBoxModal').click();
            console.log("clicked")
        }
    });
};

approvalSettings.getlist = function (ctrl) {
    var queryData = {};
    $("#searchapprovalList").find('select').each(function () {
        queryData[$(this).attr('name')] = ($.trim($(this).val()));
    });
    // if(queryData['appType'] ==1){
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/getAllApprovalsByTypeId",
        data: queryData,
        success: function (resArr) {
            $('#AppList').html(resArr)
        }
    });
    // }

};


approvalSettings.loadSections = function (sval) {
    console.log(sval);
    approvalSettings.classID = sval;
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/loadSection",
        data: {
            classId: sval
        },
        success: function (res) {
            if (res) {
                $('#secDet').removeClass('hide')
                $('#secDet').html(res);
            }

        }
    });

};

approvalSettings.loadApprovalList = function (savestate)
{
    var approvalSearchData = {};
    var params = {};
    approvalSearchData.emptySearch = 1;
    $("#searchapprovalList").find('input,select').each(function () {
        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
        if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
            approvalSearchData[$(this).attr('name')] = $.trim($(this).val());
            approvalSearchData.emptySearch = 0
        }

    });

    params = approvalSearchData;
    params.savestate = savestate;
    console.log(params);
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/approvalTblHdr",
        dataType: "html",
        success: function (res) {
            $('#AppList').html(res);
            var tablectrl = '#ApprovalList';
            var column = [0, 1, 2, 3, 4, 5, 6];
            var sortingDisabled = [7, 8];

            $(tablectrl).dataTable({
                "sDom": 'C<"clear">lrtip',
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": commonSettings.approvalAjaxurl + '/loadApprovalData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": commonSettings.NumberOfRecords,
                // 'iDisplayStart':patientsapp.startIndex,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[0, "desc"]],
                "bLengthChange": false,
                "bInfo": false,
                "fnStateLoadParams": function (oSettings, oData) {
                    // Disallow state loading by returning false  
                    if (params.savestate == 0)
                    {
                        var loadStateParams = false;
                        params.savestate = 1;
                    }
                    return loadStateParams;
                },
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function (o) {
                    if (o._iRecordsDisplay <= commonSettings.NumberOfRecords) {
                        $(tablectrl + '_paginate').hide();
                    }
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                    var rowText = '<tr appitemID = "' + aData.approval_id + '" appType ="'
                            + aData.approval_type_id + '" itemID ="' + aData.item_id + '">';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.approval_id + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<a href="javascript:void(0)" onclick="approvalSettings.viewAppDetails(this)">' + aData.approval_item + '</a>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.requested_by + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.requested_date + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.Approved_by + '</span>';
                    rowText += '</td>';


                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.Approval_date + '</span>';
                    rowText += '</td>';


                    rowText += '<td>';
                    rowText += '<span class="label label-' + aData.labelclass + ' label-mini">' + aData.approval_status + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<a href="javascript:void(0)" onclick="approvalSettings.viewForwardDetails(this)"> <i class="fa fa-eye fa-2x"></i>  </a>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += aData.Actions;
                    rowText += '</td>';

                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });


        }
    });


};
approvalSettings.changeStatusForType = function (resArr)
{
    console.log(commonSettings.fromPage)
    console.log(resArr)
//itemID
//status
    if (resArr['appTypeId'] === '1') {
        console.log('fow to leave')
        leaveSettings.changeStatus(resArr);
    }
    if (resArr['appTypeId'] === '2') {
        console.log('fow to appln')
        applicationSettings.changeStatus(resArr);
    }
    if (resArr['appTypeId'] === '3') {
        console.log('fow to fee')
        feepaymentSettings.changeStatus(resArr);
    }
    if (resArr['appTypeId'] === '4') {
        console.log('fow to trans fee')
        studentrouteSettings.changeStatus(resArr);
    }
    if ((resArr['appTypeId'] === '5') || (resArr['appTypeId'] === '6') ||
            (resArr['appTypeId'] === '7') || (resArr['appTypeId'] === '19')) {
        console.log('Suspense Voucher')
        voucherSettings.changeStatus(resArr);
    }
    if (resArr['appTypeId'] === '8') {
        console.log('Relieving(student)')
        relievingSettings.changeStatusStudent(resArr);
    }
    if (resArr['appTypeId'] === '9') {
        console.log('Relieving(staff)')
        relievingSettings.changeStatusStaff(resArr);
    }
    if (resArr['appTypeId'] === '10') {
        console.log('Appointment')
        appointmentSettings.changeStatus(resArr);
    }
    if (resArr['appTypeId'] === '11') {
        console.log('Transport')
        transportSettings.changeStatus(resArr);
    }


    if (resArr['appTypeId'] === '12') {
        console.log('Advance')
        staffAdvanceSettings.changeStatus(resArr)
    }
    if (resArr['appTypeId'] === '13') {
        console.log('Payroll')
        staffPayrollSettings.changeStatus(resArr)
    }
    if (resArr['appTypeId'] === '14') {
        console.log('Concession')
        feepaymentSettings.changeConcessionStatus(resArr);
    }
    if (resArr['appTypeId'] === '15') {
        console.log('Transport Cancel')
        transportSettings.changeDeleteRouteStatus(resArr);
    }


    if (resArr['appTypeId'] === '17') {
        console.log('Fee Assign Approval')
        feeSettings.changeFeeAssignReqStatus(resArr);
    }
    if (resArr['appTypeId'] === '18') {
        console.log('StockAdjustment')
        inventorySettings.changeStatusStock(resArr);
    }

};


approvalSettings.loadApplicationList = function () {
    var applnSearchData = {};
    var params = {};
    var academic = "";
    applnSearchData.emptySearch = 1;
    //studentProfileSearchForm

    console.log('im here12')
    approvalSettings.request = 'call';

    $("#studentApplicationForm").find('input,select,textarea').each(function () {
        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
        if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
            applnSearchData[$(this).attr('name')] = $.trim($(this).val());
            applnSearchData.emptySearch = 0
        }

    });

    params = applnSearchData;

    console.log(params);
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/applicationTblHdr",
        dataType: "html",
        success: function (res) {
            $('#ApplicationTableDiv').html(res);
            var tablectrl = '#studentApplicationList';
            var column = [0, 1, 2, 3, 4, 5, 6];
            var sortingDisabled = [7, 8, 9];
            /*$('#studentApplicationList tfoot th').each(function() {
             var title = $('#studentApplicationList thead th').eq($(this).index()).text();
             $(this).html('<input type="text" placeholder="Search ' + title + '" />');
             });*/
            var appdynamicTabl = $(tablectrl).dataTable({
                "sDom": 'C<"clear">lrtip',
                "bProcessing": true,
                "bServerSide": true,
                "destroy": true,
                "sAjaxSource": commonSettings.approvalAjaxurl + '/loadApplicationData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": approvalSettings.NumberOfRecords,
                // 'iDisplayStart':patientsapp.startIndex,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[0, "desc"]],
                "bLengthChange": false,
                "bInfo": false,
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function (o) {
                    if (o._iRecordsDisplay <= approvalSettings.NumberOfRecords) {
                        $(tablectrl + '_paginate').hide();
                    }
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    var rowText = '<tr applicationId="' + aData.application_id + '" appitemID = "'
                            + aData.approval_id + '" appType ="' + aData.approval_type_id + '" itemID ="' + aData.item_id + '">';

                    rowText += '<td>';
                    rowText += '<span class="tooltip tooltip-effect-2"><span class="tooltip-item">';
                    rowText += aData.application_no;
                    rowText += '<span class="tooltip-content clearfix" style="display:none;">\n\
                                <section class="panel">\n\
                                <header class="panel-heading">\n\
                                Application in brief</header>\n\
                                <div class="panel-body">';

                    rowText += ' <table border="4"  class="table general-table">\n\
                    <tbody><tr><td><font size="2%">Name</font></td>\n\
                            <td><font size="2%">' + aData.Student_Name + '</font></td>\n\
                        </tr> <tr>\n\
                            <td><font size="2%">Address</font> </td>\n\
                            <td><font size="2%">' + aData.address + '</font></td>\n\
                        </tr> <tr>\n\
                            <td><font size="2%">Mobile</font></td> \n\
                            <td><font size="2%">' + aData.mobile + '</font></td>\n\
                        </tr>  <tr>\n\
                            <td><font size="2%">Email</font> </td>\n\
                            <td><font size="2%">' + aData.Email + '</font></td>\n\
                        </tr></tbody></table>';
                    rowText += '</div></section></span></span>';
                    rowText += '</td>';



                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.academicYr + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.Student_Name + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.gender + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.class + '</span>';
                    rowText += '</td>';


                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.dob + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.doj + '</span>';
                    rowText += '</td>';

//                    rowText += '<td>';
//                    rowText += '<span class="label label-' + aData.labelclass + ' label-mini">' + aData.status + '</span>';
//                    rowText += '</td>';
//
//
//                    rowText += '<td>';
//                    rowText += '<span class="label label-' + aData.dataclass + ' label-mini">' + aData.key_in_status + '</span>';
//                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="label label-' + aData.labelclass + ' label-mini">' + aData.approval_status + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<a href="javascript:void(0)" onclick="approvalSettings.viewForwardDetails(this)"> <i class="fa fa-eye fa-2x"></i>  </a>';
                    rowText += '</td>';



                    rowText += '<td>';
                    rowText += aData.Actions;
                    rowText += '</td>';

                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });
            /*console.log( appdynamicTabl.columns())
             
             appdynamicTabl.columns().eq(0).each(function(colIdx) {
             console.log(colIdx)
             $('input', appdynamicTabl.column(colIdx).footer()).on('keyup change', function() {
             appdynamicTabl
             .column(colIdx)
             .search(this.value)
             .draw();
             });
             });*/
        }
    });

};

approvalSettings.loadLeavedet = function ()
{
    var leavedata = {};

    var params = {};
    var i = 0;
    var actarr = new Array('Escalated', 'Approved', 'Rejected');
    var frmdate = '';
    var todate = '';
    //leaveSearchForm
    leavedata.emptySearch = 0;

    $("#leaveSearchForm").find('input,select,textarea').each(function () {

        if ($(this).attr('name') == 'fromDate')
        {
            if ($(this).val().length > 0)
                frmdate = 1;
        }

        if ($(this).attr('name') == 'toDate')
        {
            if ($(this).val().length > 0)
                todate = 1;
        }
        leavedata.emptySearch = 1;
        leavedata[$(this).attr('name')] = $.trim($(this).val());


    });


    console.log(leavedata);
    params = leavedata;

    console.log(params);

    if ((frmdate == '' && todate == '') || (frmdate != '' && todate != ''))
    {
        $.ajax({
            type: "POST",
            url: commonSettings.approvalAjaxurl + "/loadLeavetableHeader",
            data: leavedata,
            dataType: "html",
            success: function (res) {
                //loadReport
                //alert(res);
                $('#loadReport').html(res);
                $('#loadReport').removeClass('hide');

                var tablectrl = '#leavedetails';
                console.log(tablectrl)
                approvalSettings.noOfRecordsToDisplay = 10;
                var column = [];
                var sortingDisabled = [0, 1, 2, 3, 4, 5, 6, 7, 8];

                $(tablectrl).dataTable({
                    "sDom": 'C<"clear">lrtip',
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": commonSettings.approvalAjaxurl + "/loadLeavetableData",
                    "sPaginationType": "full_numbers",
                    "iDisplayLength": approvalSettings.noOfRecordsToDisplay,
                    "aoColumnDefs": [{
                            "sDefaultContent": "",
                            "aTargets": column
                        }, {
                            "bSortable": false,
                            "sDefaultContent": "",
                            "aTargets": sortingDisabled
                        }],
                    "aaSorting": [[1, "desc"]],
                    "bLengthChange": false,
                    "bInfo": false,
                    "fnServerData": function (surl, aoData, l) {
                        for (var key in params) {
                            aoData.push({
                                "name": key,
                                "value": params[key]
                            });
                        }
                        $.ajax({
                            dataType: "json",
                            type: "POST",
                            url: surl,
                            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                            data: aoData,
                            success: l
                        });
                    },
                    "fnDrawCallback": function (o) {
                        if (o._iRecordsDisplay <= approvalSettings.noOfRecordsToDisplay) {
                            $(tablectrl + '_paginate').hide();
                            //console.log(tablectrl+'_paginate');
                        }
                    },
                    "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                        var rowText = '<tr appitemID="' + aData.aprvlmasid + '" appType="' + aData.aprvltypid + '" itemID="' + aData.id + '">';

                        rowText += '<td class="new-patient invoiceview sorting_1">';
                        rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.levtype + '</span>';
                        rowText += '</td>';

                        rowText += '<td class="new-patient invoiceview sorting_1">';
                        rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.frmdate + '</span>';
                        rowText += '</td>';


                        rowText += '<td class="new-patient invoiceview sorting_1">';
                        rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.todate + '</span>';
                        rowText += '</td>';

                        rowText += '<td class="new-patient invoiceview sorting_1">';
                        rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.duration + '</span>';
                        rowText += '</td>';

                        rowText += '<td class="new-patient invoiceview sorting_1">';
                        rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.apprvdby + '</span>';
                        rowText += '</td>';

                        rowText += '<td class="new-patient invoiceview sorting_1">';
                        rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.apprvdate + '</span>';
                        rowText += '</td>';



                        rowText += '<td class="new-patient invoiceview sorting_1">';
                        rowText += '<span class="label label-' + aData.statuscls + ' label-mini">' + aData.status + '</span>';
                        rowText += '</td>';

                        rowText += '<td class="new-patient invoiceview sorting_1">';
                        rowText += '<a href="javascript:void(0)" onclick="approvalSettings.viewForwardDetails(this)"> <i class="fa fa-eye fa-2x"></i> </a>';
                        rowText += '</td>';

                        rowText += '<td>';
                        rowText += aData.Actions;
                        rowText += '</td>';


//                    rowText += '<td class="new-patient invoiceview sorting_1">';
//                    if (aData.editture === 1)
//                    {
//                        rowText += '<span class="label label label-mini"><a href="javascript:;" title="Edit" class="glyphicon glyphicon-edit" onclick="leaveSettings.editLeave(' + aData.id + ')"></a>';
//                         rowText +='| <a href="javascript:;" title="Delete" class="glyphicon glyphicon-trash" onclick="leaveSettings.delLeave(' + aData.id +')"></a></span>';
//                    }
//                    
//                                        
//    /*                if(aData.statuscls !== 'success')
//                    { 
//                      if (aData.status !== 1) 
//                        rowText +=' <a href="javascript:;" title="Delete" class="glyphicon glyphicon-trash" onclick="leaveSettings.delLeave(' + aData.id +')"></a></span>';
//                      else
//                        rowText +=' <a href="javascript:;" title="Delete" class="glyphicon glyphicon-trash" onclick="leaveSettings.delLeave(' + aData.id +')"></a></span>';
//                    }*/
//                    
//                    rowText += '</td>';

                        rowText += '</tr>';
                        var newRow = $(rowText);
                        $(nRow).html(newRow.html());
                        commonSettings.copyAttributes(newRow, nRow);
                        return nRow;
                    }
                });

            }
        });
    }
    else
    {
        $('.modal-title').html('Message');
        $('.modal-body').html('<div class="alert alert-block alert-danger fade in">Please select from and to date.</div>');
        $('#messageBox').click();
    }

};



approvalSettings.loadFinanceApprovalList = function (savestate)
{

    var approvalSearchData = {};
    var params = {};
    approvalSearchData.emptySearch = 1;
    $("#searchapprovalList").find('input,select').each(function () {
        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
        if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
            approvalSearchData[$(this).attr('name')] = $.trim($(this).val());
            approvalSearchData.emptySearch = 0
        }

    });

    params = approvalSearchData;
    params.savestate = savestate;
    console.log(params);
    approvalSettings.listqueryparams = params;
    commonSettings.fromPage = 'VoucherApproval';
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/loadFinanceTbl",
        dataType: "html",
        success: function (res) {
            $('#voucher_div').html(res);
            var tablectrl = '#ApprovalList';
            approvalSettings.noOfRecordsToDisplay = 10;
            var column = [1, 3, 4, 5, 6, 7];
            var sortingDisabled = [0, 2, 8];

            $(tablectrl).dataTable({
                "sDom": '<"top"if>rt<"bottopm"p><"clear">',
                // "sDom": 'C<"clear">lrtip',
                "bProcessing": true,
                "bServerSide": true,
                stateSave: true,
                "sAjaxSource": commonSettings.approvalAjaxurl + '/loadFinanceData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": approvalSettings.noOfRecordsToDisplay,
                // 'iDisplayStart':patientsapp.startIndex,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[1, "desc"]],
                "bLengthChange": false,
                //"bInfo": false,
                "oLanguage": {
                    "sInfo": "<i class='fa fa-info activity-icon'></i> <i class='text-muted'><b>Showing _START_ to _END_ of _TOTAL_ Vouchers</b></i>",
                    "sInfoFiltered": " ",
                },
                "fnStateLoadParams": function (oSettings, oData) {
                    // Disallow state loading by returning false  
                    if (params.savestate == 0)
                    {
                        var loadStateParams = false;
                        params.savestate = 1;
                    }
                    return loadStateParams;
                },
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });

                    approvalSettings.listqueryparams = aoData;
                },
                "fnDrawCallback": function (o) {
                    if (o._iRecordsDisplay <= approvalSettings.noOfRecordsToDisplay) {
                        $(tablectrl + '_paginate').show();
                    }
//                            console.log('length:'+$(tablectrl).find('tr>td:first').length)
                    var enableIds = "#bulk_reject_adjustment, #bulk_approve_adjustment";

                    $('.dataTables_filter').addClass('col-md-5');
                    $('.dataTables_filter').find('label').addClass('col-md-3 search-txt1').css('display', ' inline');
                    $('.dataTables_filter').find('input').addClass('form-control pull-right search-tbox1');

                    $(".select_chkbx").unbind();
                    $('.select_chkbx').click(function () {
                        var ctrl = this;
                        if ($(ctrl).prop("checked") == false) {
                            $(tablectrl).find('.selectallmsg').remove();
                            $(tablectrl).find('.select_all_chkbx').attr("isselectall", "no");
                            $(tablectrl).find(".select_all_chkbx").prop("checked", false);
                            $(ctrl).closest("tr").find("td").css('background-color', '');
                        }
                        if ($('.select_chkbx:checked').length === 0) {
                            $(enableIds).addClass("disabled");
                            $(enableIds).attr('title', 'You must select one disabled more checkboxes to use this action');
                            $(ctrl).closest("tr").find("td").css('background-color', '');
                            $(ctrl).closest("table").find('.select_all_chkbx').prop("checked", false);
                        } else {
                            $(enableIds).removeClass("disabled");
                            $(enableIds).removeAttr('title');
                            $('.select_chkbx:checked').closest("tr").find("td").css('background-color', '#fdfddd');
                        }

                    });

                    $(".select_all_chkbx").unbind();
                    $('.select_all_chkbx').click(function () {
                        var ctrl = this;
                        $(tablectrl).find(".select_chkbx").prop('checked', $(ctrl).prop("checked"));
                        if ($('.select_chkbx:checked').length === 0) {
                            $(enableIds).addClass("disabled");
                            $(enableIds).attr('title', 'You must select one or more checkboxes to use this action');
                            $(tablectrl).find('.select_all_chkbx').attr("isselectall", "no");
                            $(tablectrl).find('.selectallmsg').remove();
                            $(tablectrl).find('td').css('background-color', '');
                        } else {
                            $(enableIds).removeClass("disabled");
                            $(enableIds).removeAttr('title');
                            $(tablectrl).find('td.enabledcheckbx').css('background-color', '#fdfddd');
                            var totalColumn = $(tablectrl).find('th').length;
                            var totalRecords = o._iRecordsDisplay;
                            var displayRecords = o._iRecordsTotal;
                            var isAllselected = false;
                            if ($(tablectrl).find('.select_all_chkbx').attr("isselectall") == "yes") {
                                isAllselected = true;
                            }
                            if (displayRecords < totalRecords) {
                                var trhtml = '<tr class="selectallmsg">';
                                trhtml += '<td colspan="' + totalColumn + '" class="mr_hg26 mr_posrel">';
                                trhtml += '	<div class=" mr_lndg mr_pad5p0p mr_chkcenter"';
                                trhtml += '		style="left: -15px;">';
                                trhtml += '		<span class="selected_records_div">All <b>' + displayRecords + '</b> objects on this page selected.</span>';
                                trhtml += '		<span class="all_records_div" style="display: none;">All <b>' + totalRecords + '</b>';
                                trhtml += '			matching objects selected.</span><a href="javascript:void(0);"';
                                trhtml += '			class="delselectall mr_cncl" id="select_all_link" isselectall="no">Select all ' + totalRecords + ' objects matching</a>';
                                trhtml += '		<a href="javascript:void(0);" class="delclrsct mr_cncl"';
                                trhtml += '			style="display: none;" id="clear_select_all_link">Clear selection</a>';
                                trhtml += '	</div></td>';
                                trhtml += '</tr>';
                                $(trhtml).insertBefore($(tablectrl).find('tbody tr:first'));
                                if (isAllselected) {
                                    $('.selected_records_div').hide();
                                    $('.all_records_div').show();
                                    $('#clear_select_all_link').show();
                                    $(tablectrl).find('#select_all_link').hide();
                                    $(tablectrl).find('#select_all_link').attr("isselectall", "yes");
                                    $(tablectrl).find('.select_all_chkbx').attr("isselectall", "yes");
                                    $(tablectrl).find(".select_chkbx").prop("checked", true);
                                    $(tablectrl).find(".select_all_chkbx").prop("checked", true);
                                    $(tablectrl).find('td.enabledcheckbx').css('background-color', '#fdfddd');
                                }
                            }

                            $('#clear_select_all_link').unbind();
                            $('#clear_select_all_link').click(function () {
                                $(enableIds).addClass("disabled");
                                $(enableIds).attr('title', 'You must select one or more checkboxes to use this action');
                                $(tablectrl).find('.selectallmsg').remove();
                                $(tablectrl).find('.select_all_chkbx').attr("isselectall", "no");
                                $(tablectrl).find(".select_chkbx").prop("checked", false);
                                $(tablectrl).find(".select_all_chkbx").prop("checked", false);
                                $(tablectrl).find('td').css('background-color', '');
                            });
                            $('#select_all_link').unbind();
                            $('#select_all_link').click(function () {
                                $('.selected_records_div').hide();
                                $('.all_records_div').show();
                                $('#clear_select_all_link').show();
                                $(tablectrl).find('#select_all_link').hide();
                                $(tablectrl).find('#select_all_link').attr("isselectall", "yes");
                                $(tablectrl).find('.select_all_chkbx').attr("isselectall", "yes");
                                $(tablectrl).find(".select_chkbx").prop("checked", true);
                                $(tablectrl).find('td.enabledcheckbx').css('background-color', '#fdfddd');
                            });
                        }
                    });
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                    var rowText = '<tr appitemID = "' + aData.approval_id + '" appType ="' + aData.approval_type_id + '" itemID ="' + aData.item_id + '">';

                    rowText += '<td class="' + aData.enabledcheckbx + '">';
                    rowText += aData.checkapprv;
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += aData.voucherdet;// rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.voucherid + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<a href="javascript:void(0)" onclick="approvalSettings.viewAppDetails(this)">' + aData.approval_item + '</a>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.voucher_crdt + '</span>';
                    rowText += '</td>';


                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.voucher_dbt + '</span>';
                    rowText += '</td>';

                    rowText += '<td class="bg-blue text_blk1">';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore text-primary fee_text">' + aData.voucher_amt + '</span>';
                    rowText += '</td>';


                    rowText += '<td>';
                    rowText += '<span class="label label-' + aData.labelclass + ' label-mini">' + aData.approval_status + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<a href="javascript:void(0)" onclick="approvalSettings.viewForwardDetails(this)"> <i class="fa fa-eye fa-2x"></i>  </a>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += aData.Actions;
                    rowText += '</td>';

                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });


        }
    });


};

approvalSettings.bulkApprvalRejection = function (status) {
    if ($("#bulk_reject_adjustment").hasClass("disabled")) {
        return false;
    } else {
        $('.cnfBox-title').html('Confirm');
        $('.cnfBox-body').html('<div class="alert alert-block alert-warning fade in">Do confirm to ' + (status == 'Rejected' ? 'Reject' : 'Approve') + ' the selected items ?</div>');
        $('#ConfirmModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#confirm_yes').unbind('click').click(function () {
            $('#ConfirmModal').modal('hide');
            var commentname = status == 'Rejected' ? 'Reject' : 'Approve';
            // if (status == 'Rejected') {
            $('.formBox-title').html('Add Comments to ' + commentname);
            $('#confirm_close').addClass('hide');
            $('#iconclose').addClass('hide');
            $('.formBox-body').html('<form class="form-horizontal" ><div class="form-group"><label for="" class="col-sm-3 control-label">Enter Comments:</label><div class="col-sm-6"><textarea id="comments" name="comments" class="form-control" cols="10" rows="4"></textarea></div></div></form>');
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#confirm_save').html('submit')
            $('#formModal').modal('show')
            $('#confirm_save').unbind('click').click(function () {
                $('#confirm_close').click();
                if ($('#comments').val() === '') {
                    $('#messageBoxModal .msgBox-title').html('Error');
                    $('#messageBoxModal .msgBox-body').html('Enter comments to reject');
                    $('#messageBoxModal').modal('show');
                    $('#msg_ok').unbind('click').click(function () {
                        $('#messageBoxModal').modal('hide');
                        $('#formModal').modal('show')
                    });
                    return false;
                } else {
                    var rejcomments = $('#comments').val();
                    approvalSettings.apprvrejConfirm(status, rejcomments);
                }
            });
//            } else {
//                approvalSettings.apprvrejConfirm(status, '');
//            }
        });

    }
};
approvalSettings.apprvrejConfirm = function (status, rejcomments) {

    var tablectrl = '#ApprovalList';
    var ids = [];
    $(tablectrl).find('.select_chkbx:checked').each(function () {
        ids.push($.trim($(this).closest('tr').attr("appitemID")));
    });
    var params = {};
    if ($('#select_all_link').attr("isselectall") == "yes") {
        params = approvalSettings.listqueryparams;
        params.push({name: 'statusApproval', value: status});
        params.push({name: 'rejectionComments', value: rejcomments});

    } else {
        params['ids'] = ids;
        params['statusApproval'] = status;
        params['rejectionComments'] = rejcomments;
    }
//        console.log(params)
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/bulkApprovalReject",
        data: params,
        dataType: "json",
        success: function (res) {
            if (res) {
//                $('#other_section').waitMe('hide');
                var lblcls = res.type == 'error' ? 'danger' : 'success';
                $('#messageBoxModal .msgBox-title').html('Message');
                $('#messageBoxModal .msgBox-body').html('<div class="alert alert-block alert-' + lblcls + ' fade in">' + res.message + '</div>');
                $('#messageBoxModal').modal('show');
                $('#msg_ok').unbind('click').click(function () {
                    $('#messageBoxModal').modal('hide');
                    approvalSettings.loadFinanceApprovalList();
                });
            }

        }
    });
}
approvalSettings.loadFeeApprovalList = function ()
{
    var approvalSearchData = {};
    var params = {};
    approvalSearchData.emptySearch = 1;
    $("#searchapprovalList").find('input,select').each(function () {
        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
        if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
            approvalSearchData[$(this).attr('name')] = $.trim($(this).val());
            approvalSearchData.emptySearch = 0
        }

    });

    params = approvalSearchData;
    console.log(params);
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/loadFeeTbl",
        dataType: "html",
        success: function (res) {
            $('#AppList').html(res);
            var tablectrl = '#ApprovalList';
            approvalSettings.noOfRecordsToDisplay = 10;
            var column = [];
            var sortingDisabled = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

            $(tablectrl).dataTable({
                "sDom": 'C<"clear">lrtip',
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": commonSettings.approvalAjaxurl + '/loadFeeData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": approvalSettings.noOfRecordsToDisplay,
                // 'iDisplayStart':patientsapp.startIndex,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[0, "desc"]],
                "bLengthChange": false,
                "bInfo": false,
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function (o) {
                    if (o._iRecordsDisplay <= approvalSettings.noOfRecordsToDisplay) {
                        $(tablectrl + '_paginate').hide();
                    }
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                    var rowText = '<tr appitemID = "' + aData.approval_id + '" appType ="' + aData.approval_type_id + '" itemID ="' + aData.item_id + '">';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.approval_id + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.stuname + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.class_sec + '</span>';
                    rowText += '</td>';


                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.feetype + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.feename + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.feeamt + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.requested_by + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.requested_date + '</span>';
                    rowText += '</td>';


                    rowText += '<td>';
                    rowText += '<span class="label label-' + aData.labelclass + ' label-mini">' + aData.approval_status + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<a href="javascript:void(0)" onclick="approvalSettings.viewForwardDetails(this)"> <i class="fa fa-eye fa-2x"></i>  </a>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += aData.Actions;
                    rowText += '</td>';

                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });


        }
    });


};

approvalSettings.loadTransportApprovalList = function ()
{
    var approvalSearchData = {};
    var params = {};
    approvalSearchData.emptySearch = 1;
    $("#searchapprovalList").find('input,select').each(function () {
        console.log($(this).attr('name') + ' : ' + $.trim($(this).val()))
        if ($.trim($(this).val()) != 0 && $.trim($(this).val()) != '' && $.trim($(this).val()).length > 0) {
            approvalSearchData[$(this).attr('name')] = $.trim($(this).val());
            approvalSearchData.emptySearch = 0
        }

    });

    params = approvalSearchData;
    console.log(params);
    $.ajax({
        type: "POST",
        url: commonSettings.approvalAjaxurl + "/loadTransportTbl",
        dataType: "html",
        success: function (res) {
            $('#AppList').html(res);
            var tablectrl = '#ApprovalList';
            approvalSettings.noOfRecordsToDisplay = 10;
            var column = [];
            var sortingDisabled = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

            $(tablectrl).dataTable({
                "sDom": 'C<"clear">lrtip',
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": commonSettings.approvalAjaxurl + '/loadTransportData',
                "sPaginationType": "full_numbers",
                "iDisplayLength": approvalSettings.noOfRecordsToDisplay,
                // 'iDisplayStart':patientsapp.startIndex,
                "aoColumnDefs": [{
                        "sDefaultContent": "",
                        "aTargets": column
                    }, {
                        "bSortable": false,
                        "sDefaultContent": "",
                        "aTargets": sortingDisabled
                    }],
                "aaSorting": [[0, "desc"]],
                "bLengthChange": false,
                "bInfo": false,
                "fnServerData": function (surl, aoData, l) {
                    for (var key in params) {
                        aoData.push({
                            "name": key,
                            "value": params[key]
                        });
                    }
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: surl,
                        contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                        data: aoData,
                        success: l
                    });
                },
                "fnDrawCallback": function (o) {
                    if (o._iRecordsDisplay <= approvalSettings.noOfRecordsToDisplay) {
                        $(tablectrl + '_paginate').hide();
                    }
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                    var rowText = '<tr appitemID = "' + aData.approval_id + '" appType ="' + aData.approval_type_id + '" itemID ="' + aData.item_id + '">';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.approval_id + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.stuname + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.class_sec + '</span>';
                    rowText += '</td>';


                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.feetype + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.feename + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore">' + aData.feeamt + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.requested_by + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<span class="mr_fltlft mr_wd90 mr_forallmore" >' + aData.requested_date + '</span>';
                    rowText += '</td>';


                    rowText += '<td>';
                    rowText += '<span class="label label-' + aData.labelclass + ' label-mini">' + aData.approval_status + '</span>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += '<a href="javascript:void(0)" onclick="approvalSettings.viewForwardDetails(this)"> <i class="fa fa-eye fa-2x"></i>  </a>';
                    rowText += '</td>';

                    rowText += '<td>';
                    rowText += aData.Actions;
                    rowText += '</td>';

                    rowText += '</tr>';
                    var newRow = $(rowText);
                    $(nRow).html(newRow.html());
                    commonSettings.copyAttributes(newRow, nRow);
                    return nRow;
                }
            });


        }
    });


};
