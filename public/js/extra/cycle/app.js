var calendarSettings ={};
(function($) {
    var now = new Date();
    var month = (now.getMonth()+1)<10? ('0'+(now.getMonth()+1)):(now.getMonth()+1);
    var date = (now.getDate()<10)? ('0'+now.getDate()):now.getDate();
    "use strict";
    var options = {
//        events_source: commonSettings.studentDashboardAjaxurl + '/getStudentAttendanceForMonth?student_id=1',
        view: 'month',
        tmpl_path: commonSettings.tmplUrl + '/',
        tmpl_cache: false,
        day:  now.getFullYear()+'-'+month+'-'+date,
        onAfterEventsLoad: function(events) {
            if (!events) {
                return;
            }
            var list = $('#eventlist');
            list.html('');

            $.each(events, function(key, val) {
                $(document.createElement('li'))
                        .html('<a href="' + val.url + '">' + val.title + '</a>')
                        .appendTo(list);
            });
        },
        onAfterViewLoad: function(view) {
            $('.monthname').text(this.getTitle());
            $('.btn-group button').removeClass('active');
            $('button[data-calendar-view="' + view + '"]').addClass('active');
        },
        classes: {
            months: {
                general: 'label'
            }
        },
        merge_holidays:     true
    };
    
calendarSettings.options = options;

    $('.clndr-controls .clndr-control-button[data-calendar-nav]').each(function() {
        //console.log($(this).data('calendar-nav'))
       var $this = $(this);
        $this.click(function() {
            calendarSettings.calendar.navigate($this.data('calendar-nav'));
        });
    });

    $('.btn-group button[data-calendar-view]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendarSettings.calendar.view($this.data('calendar-view'));
        });
    });

    $('#first_day').change(function() {
        var value = $(this).val();
        value = value.length ? parseInt(value) : null;
        calendarSettings.calendar.setOptions({first_day: value});
        calendarSettings.calendar.view();
    });

    $('#language').change(function() {
        calendarSettings.calendar.setLanguage($(this).val());
        calendarSettings.calendar.view();
    });

    $('#events-in-modal').change(function() {
        var val = $(this).is(':checked') ? $(this).val() : null;
        calendarSettings.calendar.setOptions({modal: val});
    });
    $('#events-modal .modal-header, #events-modal .modal-footer').click(function(e) {
        //e.preventDefault();
        //e.stopPropagation();
    });
}(jQuery));