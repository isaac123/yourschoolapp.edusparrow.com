//$(document).ready(function() {
//    var $this = '';
//    var menuItemNum = $(".menu-item").length;
//    var angle = 90;
//    var distance = 120;
//    var startingAngle = 150 + (-angle / 2);
//    var slice = angle / (menuItemNum - 1);
//    TweenMax.globalTimeScale(0.8);
//    $(".menu-item").each(function(i) {
//        var angle = startingAngle + (slice * i);
//        $(this).css({
//            transform: "rotate(" + (angle) + "deg)"
//        })
//        $(this).find(".menu-item-icon").css({
//            transform: "rotate(" + (-angle) + "deg)"
//        })
//    })
//    var on = false;
//
//    $(".menu-toggle-button").mousedown(function() {
//        TweenMax.to($(".menu-toggle-icon"), 0.1, {
//            scale: 0.65
//        })
//    })
//    $(document).mouseup(function() {
//        TweenMax.to($(".menu-toggle-icon"), 0.1, {
//            scale: 1
//        })
//    });
//    $(document).on("touchend", function() {
//        $(document).trigger("mouseup")
//    })
//    $(".menu-toggle-button").on("mousedown", pressHandler);
//    $(".menu-toggle-button").on("touchstart", function(event) {
//        $(this).trigger("mousedown");
//        event.preventDefault();
//        event.stopPropagation();
//    });
//
//    function pressHandler(event) {
//        on = !on;
//        element = event.target;
////                $this = $(element).parents('menu-wrapper');   
////            console.log($this.html())             
//        TweenMax.to($(this).children('.menu-toggle-icon'), 0.4, {
//            rotation: on ? 45 : 0,
//            ease: Quint.easeInOut,
//            force3D: true
//        });
//
//        on ? openMenu() : closeMenu();
//
//    }
//    function openMenu() {
//        console.log($(".menu-item").length)
////            $('.menu-wrapper').removeClass('hide')
//        $(".menu-item").each(function(i) {
//            $('.menu-wrapper').animate({'z-index': "", }, 10);
//            console.log($(this).children(".menu-item-bounce").length)
//            var delay = i * 0.08;
//
//            var $bounce = $(".menu-toggle-button");//$(this).children(".menu-item-bounce");//
//
//            TweenMax.fromTo($bounce, 0.2, {textShadow: "10px 10px 15px rgb(210, 199, 199)",
//                color: "#fff"
//            }, {css: {'fill': '#000'},
//                delay: delay,
//                scaleX: 0.8,
//                scaleY: 1.2,
//                force3D: true,
//                ease: Quad.easeInOut,
//                onComplete: function() {
//                    TweenMax.to($bounce, 0.15, {
//                        // scaleX:1.2,,
//                        css: {'fill': '#504F61'},
//                        scaleY: 0.7,
//                        force3D: true,
//                        ease: Quad.easeInOut,
//                        onComplete: function() {
//                            TweenMax.to($bounce, 3, {
//                                // scaleX:1,
//                                scaleY: 0.8,
//                                force3D: true,
//                                ease: Elastic.easeOut,
//                                easeParams: [1.1, 0.12]
//                            })
//                        }
//                    })
//                }
//            });
//
//            TweenMax.to($(this).children(".menu-item-button"), 0.5, {
//                delay: delay,
//                y: distance,
//                force3D: true,
//                ease: Quint.easeInOut
//            });
//        })
//    }
//    function closeMenu() {
//        $(".menu-item").each(function(i) {
//            var delay = i * 0.08;
//
//            var $bounce = $(".menu-toggle-button");//$(this).children(".menu-item-bounce");//
//
//            TweenMax.fromTo($bounce, 0.2, {
//                textShadow: "0px",
//                color: "#504F61"
//            }, {
//                css: {'fill': '#000'},
//                delay: delay,
//                scaleX: 1,
//                scaleY: 0.8,
//                force3D: true,
//                ease: Quad.easeInOut,
//                onComplete: function() {
//                    TweenMax.to($bounce, 0.15, {
//                        // scaleX:1.2,
//                        scaleY: 1.2, css: {'fill': '#504F61'},
//                        force3D: true,
//                        ease: Quad.easeInOut,
//                        onComplete: function() {
//                            TweenMax.to($bounce, 3, {
//                                // scaleX:1,
//                                scaleY: 1,
//                                force3D: true,
//                                ease: Elastic.easeOut,
//                                easeParams: [1.1, 0.12]
//                            })
//
//                            $('.menu-wrapper').animate({'z-index': "-9999", }, 10);
//                        }
//                    })
//
////            $('.menu-wrapper').toggleClass('hide')
//                }
//            });
//
//
//            TweenMax.to($(this).children(".menu-item-button"), 0.3, {
//                delay: delay,
//                y: 0,
//                force3D: true,
//                ease: Quint.easeIn
//            });
//        })
//
//    }
//})
//
$(document).ready(function() {
    var $this = '';
    var menuItemNum = $(".menu-item1").length;
    var angle = 90;
    var distance = 120;
    var startingAngle = -150 + (-angle / 2);
    var slice = angle / (menuItemNum - 1);
    TweenMax.globalTimeScale(0.8);
    $(".menu-item1").each(function(i) {
        var angle = startingAngle + (slice * i);
        $(this).css({
            transform: "rotate(" + (angle) + "deg)"
        })
        $(this).find(".menu-item-icon1").css({
            transform: "rotate(" + (-angle) + "deg)"
        })
    })
    var on = false;

    $(".menu-toggle-button").mouseover(function(){
        $(".menu-toggle-button").css("fill", "#0097A7");
    });

     $(".menu-toggle-button").mouseout(function(){
        $(".menu-toggle-button").css("fill", "#00BCD4");
    });
  
    $(".menu-toggle-button1").mouseover(function(){
        $(".menu-toggle-button1").css("fill", "#00796B");
    });

     $(".menu-toggle-button1").mouseout(function(){
        $(".menu-toggle-button1").css("fill", "#32B196");
    });

  
    $(".menu-toggle-button2").mouseover(function(){
        $(".menu-toggle-button2").css("fill", "#757575");
    });

    $(".menu-toggle-button2").mouseout(function(){
        $(".menu-toggle-button2").css("fill", "#98AAA9");
    });

    $(".menu-toggle-button1").mousedown(function() {
        TweenMax.to($(".menu-toggle-icon1"), 0.1, {
            scale: 0.65
        })
    })
    $(document).mouseup(function() {
        TweenMax.to($(".menu-toggle-icon1"), 0.1, {
            scale: 1
        })
    });
    $(document).on("touchend", function() {
        $(document).trigger("mouseup")
    })
    $(".menu-toggle-button1").on("mousedown", pressHandler);
    $(".menu-toggle-button1").on("touchstart", function(event) {
        $(this).trigger("mousedown");
        event.preventDefault();
        event.stopPropagation();
    });

    function pressHandler(event) {
        on = !on;
        element = event.target;
//                $this = $(element).parents('menu-wrapper');   
//            console.log($this.html())             
        TweenMax.to($(this).children('.menu-toggle-icon1'), 0.4, {
            rotation: on ? 45 : 0,
            ease: Quint.easeInOut,
            force3D: true
        });

        on ? openMenu() : closeMenu();

    }
    function openMenu() {
        console.log($(".menu-item1").length)
//            $('.menu-wrapper').removeClass('hide')
        $(".menu-item1").each(function(i) {
            $('.menu-wrapper1').animate({'z-index': "", }, 10);
            console.log($(this).children(".menu-item-bounce1").length)
            var delay = i * 0.08;

            var $bounce = $(".menu-toggle-button1");//$(this).children(".menu-item-bounce");//

            TweenMax.fromTo($bounce, 0.2, {textShadow: "10px 10px 15px rgb(210, 199, 199)",
                color: "#fff"
            }, {css: {'fill': '#000 '},
                delay: delay,
                scaleX: 0.8,
                scaleY: 1.2,
                force3D: true,
                ease: Quad.easeInOut,
                onComplete: function() {
                    TweenMax.to($bounce, 0.15, {
                        // scaleX:1.2,,
                        css: {'fill': 'rgb(50, 177, 150)'},
                        scaleY: 0.7,
                        force3D: true,
                        ease: Quad.easeInOut,
                        onComplete: function() {
                            TweenMax.to($bounce, 3, {
                                // scaleX:1,
                                scaleY: 0.8,
                                force3D: true,
                                ease: Elastic.easeOut,
                                easeParams: [1.1, 0.12]
                            })
                        }
                    })
                }
            });

            TweenMax.to($(this).children(".menu-item-button1"), 0.5, {
                delay: delay,
                y: distance,
                force3D: true,
                ease: Quint.easeInOut
            });
        })
    }
    function closeMenu() {
        $(".menu-item1").each(function(i) {
            var delay = i * 0.08;

            var $bounce = $(".menu-toggle-button1");//$(this).children(".menu-item-bounce");//

            TweenMax.fromTo($bounce, 0.2, {
                textShadow: "0px",
                color: "rgb(50, 177, 150)"
            }, {
                css: {'fill': '#000'},
                delay: delay,
                scaleX: 1,
                scaleY: 0.8,
                force3D: true,
                ease: Quad.easeInOut,
                onComplete: function() {
                    TweenMax.to($bounce, 0.15, {
                        // scaleX:1.2,
                        scaleY: 1.2, css: {'fill': 'rgb(50, 177, 150)'},
                        force3D: true,
                        ease: Quad.easeInOut,
                        onComplete: function() {
                            TweenMax.to($bounce, 3, {
                                // scaleX:1,
                                scaleY: 1,
                                force3D: true,
                                ease: Elastic.easeOut,
                                easeParams: [1.1, 0.12]
                            })

                            $('.menu-wrapper1').animate({'z-index': "-9999", }, 10);
                        }
                    })

//            $('.menu-wrapper').toggleClass('hide')
                }
            });


            TweenMax.to($(this).children(".menu-item-button1"), 0.3, {
                delay: delay,
                y: 0,
                force3D: true,
                ease: Quint.easeIn
            });
        })

    }
})

$(document).ready(function() {
    var $this = '';
    var menuItemNum = $(".menu-item2").length;
    //var angle = 60;
    var angle = 180;
    //var distance = 100;
    var distance = 100;
    var startingAngle = 0 + (-angle / 2);
    var slice = angle / (menuItemNum);
    TweenMax.globalTimeScale(0.8);
    $(".menu-item2").each(function(i) {
        var angle = startingAngle + (slice * i);
        $(this).css({
            transform: "rotate(" + (angle) + "deg)"
        })
        $(this).find(".menu-item-icon2").css({
            transform: "rotate(" + (-angle) + "deg)"
        })
    })
    var on = false;

    $(".menu-toggle-button2").mousedown(function() {
        TweenMax.to($(".menu-toggle-icon2"), 0.1, {
            scale: 0.65
        })
    })
    $(document).mouseup(function() {
        TweenMax.to($(".menu-toggle-icon2"), 0.1, {
            scale: 1
        })
    });
    $(document).on("touchend", function() {
        $(document).trigger("mouseup")
    })
    $(".menu-toggle-button2").on("mousedown", pressHandler);
    $(".menu-toggle-button2").on("touchstart", function(event) {
        $(this).trigger("mousedown");
        event.preventDefault();
        event.stopPropagation();
    });

    function pressHandler(event) {
        on = !on;
        element = event.target;
//                $this = $(element).parents('menu-wrapper');   
//            console.log($this.html())             
        TweenMax.to($(this).children('.menu-toggle-icon2'), 0.4, {
            rotation: on ? 45 : 0,
            ease: Quint.easeInOut,
            force3D: true
        });

        on ? openMenu() : closeMenu();

    }
    function openMenu() {
            $('#menu-wrapper3').attr('class', 'hide')
            $('#menu-wrapper2').attr('class', 'menu-wrapper2')
        console.log($(".menu-item2").length)
//            $('.menu-wrapper').removeClass('hide')
        $(".menu-item2").each(function(i) {
            $('.menu-wrapper2').animate({'z-index': "", }, 10);
            console.log($(this).children(".menu-item-bounce2").length)
            var delay = i * 0.08;

            var $bounce = $(".menu-toggle-button2");//$(this).children(".menu-item-bounce");//

            TweenMax.fromTo($bounce, 0.2, {textShadow: "10px 10px 15px rgb(210, 199, 199)",
                color: "#98AAA9"
            }, {css: {'fill': '#000'},
                delay: delay,
                scaleX: 0.8,
                scaleY: 1.2,
                force3D: true,
                ease: Quad.easeInOut,
                onComplete: function() {
                    TweenMax.to($bounce, 0.15, {
                        // scaleX:1.2,,
                        css: {'fill': '#98AAA9'},
                        scaleY: 0.7,
                        force3D: true,
                        ease: Quad.easeInOut,
                        onComplete: function() {
                            TweenMax.to($bounce, 3, {
                                // scaleX:1,
                                scaleY: 0.8,
                                force3D: true,
                                ease: Elastic.easeOut,
                                easeParams: [1.1, 0.12]
                            })
                        }
                    })
                }
            });

            TweenMax.to($(this).children(".menu-item-button2"), 0.5, {
                delay: delay,
                y: distance,
                force3D: true,
                ease: Quint.easeInOut
            });
        })
    }
    function closeMenu() {
        $(".menu-item2").each(function(i) {
            var delay = i * 0.08;

            var $bounce = $(".menu-toggle-button2");//$(this).children(".menu-item-bounce");//

            TweenMax.fromTo($bounce, 0.2, {
                textShadow: "0px",
                color: "#98AAA9"
            }, {
                css: {'fill': '#000'},
                delay: delay,
                scaleX: 1,
                scaleY: 0.8,
                force3D: true,
                ease: Quad.easeInOut,
                onComplete: function() {
                    TweenMax.to($bounce, 0.15, {
                        // scaleX:1.2,
                        scaleY: 1.2, css: {'fill': '#98AAA9'},
                        force3D: true,
                        ease: Quad.easeInOut,
                        onComplete: function() {
                            TweenMax.to($bounce, 3, {
                                // scaleX:1,
                                scaleY: 1,
                                force3D: true,
                                ease: Elastic.easeOut,
                                easeParams: [1.1, 0.12]
                            })

                            $('.menu-wrapper2').animate({'z-index': "-9999", }, 10);
                        }
                    })

//            $('.menu-wrapper').toggleClass('hide')
                }
            });


            TweenMax.to($(this).children(".menu-item-button2"), 0.3, {
                delay: delay,
                y: 0,
                force3D: true,
                ease: Quint.easeIn
            });
        })

    }
})

$(document).ready(function() {
    var $this = '';
    var menuItemNum = $(".menu-item3").length;
    var angle = 30;
    var distance = 300;
    var startingAngle = -360 + (-angle / 2);
    var slice = angle / (menuItemNum - 1);
    TweenMax.globalTimeScale(0.8);
    $(".menu-item3").each(function(i) {
        var angle = startingAngle + (slice * i);
        $(this).css({
            transform: "rotate(" + (angle) + "deg)"
        })
        $(this).find(".menu-item-icon3").css({
            transform: "rotate(" + (-angle) + "deg)"
        })
    })
    var on = false;

    $(".menu-toggle-button4").mouseover(function(){
        $(".menu-toggle-button4").css("fill", "#B71C1C");
    });

     $(".menu-toggle-button4").mouseout(function(){
        $(".menu-toggle-button4").css("fill", "#ED7774");
    });
    
    $(".menu-toggle-button3").mouseover(function(){
        $(".menu-toggle-button3").css("fill", "#C5B51E");
    });

     $(".menu-toggle-button3").mouseout(function(){
        $(".menu-toggle-button3").css("fill", "#DBC921");
    });

    $(".menu-toggle-button3").mousedown(function() {
        TweenMax.to($(".menu-toggle-icon3"), 0.1, {
            scale: 0.65
        })
    })
    $(document).mouseup(function() {
        TweenMax.to($(".menu-toggle-icon3"), 0.1, {
            scale: 1
        })
    });
    $(document).on("touchend", function() {
        $(document).trigger("mouseup")
    })
    $(".menu-toggle-button3").on("mousedown", pressHandler);
    $(".menu-toggle-button3").on("touchstart", function(event) {
        $(this).trigger("mousedown");
        event.preventDefault();
        event.stopPropagation();
    });

    function pressHandler(event) {
        on = !on;
        element = event.target;
//                $this = $(element).parents('menu-wrapper');   
//            console.log($this.html())             
        TweenMax.to($(this).children('.menu-toggle-icon3'), 0.4, {
            rotation: on ? 45 : 0,
            ease: Quint.easeInOut,
            force3D: true
        });

        on ? openMenu() : closeMenu();

    }
    function openMenu() {
            $('#menu-wrapper2').attr('class', 'hide')
            $('#menu-wrapper3').attr('class', 'menu-wrapper3')
        console.log($(".menu-item3").length)
//            $('.menu-wrapper').removeClass('hide')
        $(".menu-item3").each(function(i) {
            $('.menu-wrapper3').animate({'z-index': "", }, 10);
            console.log($(this).children(".menu-item-bounce3").length)
            var delay = i * 0.08;

            var $bounce = $(".menu-toggle-button3");//$(this).children(".menu-item-bounce");//

            TweenMax.fromTo($bounce, 0.2, {textShadow: "10px 10px 15px rgb(210, 199, 199)",
                color: "#DBC921"
            }, {css: {'fill': '#000'},
                delay: delay,
                scaleX: 0.8,
                scaleY: 1.2,
                force3D: true,
                ease: Quad.easeInOut,
                onComplete: function() {
                    TweenMax.to($bounce, 0.15, {
                        // scaleX:1.2,,
                        css: {'fill': '#DBC921'},
                        scaleY: 0.7,
                        force3D: true,
                        ease: Quad.easeInOut,
                        onComplete: function() {
                            TweenMax.to($bounce, 3, {
                                // scaleX:1,
                                scaleY: 0.8,
                                force3D: true,
                                ease: Elastic.easeOut,
                                easeParams: [1.1, 0.12]
                            })
                        }
                    })
                }
            });

            TweenMax.to($(this).children(".menu-item-button3"), 0.5, {
                delay: delay,
                y: distance,
                force3D: true,
                ease: Quint.easeInOut
            });
        })
    }
    function closeMenu() {
        $(".menu-item3").each(function(i) {
            var delay = i * 0.08;

            var $bounce = $(".menu-toggle-button3");//$(this).children(".menu-item-bounce");//

            TweenMax.fromTo($bounce, 0.2, {
                textShadow: "0px",
                color: "#DBC921"
            }, {
                css: {'fill': '#000'},
                delay: delay,
                scaleX: 1,
                scaleY: 0.8,
                force3D: true,
                ease: Quad.easeInOut,
                onComplete: function() {
                    TweenMax.to($bounce, 0.15, {
                        // scaleX:1.2,
                        scaleY: 1.2, css: {'fill': '#DBC921'},
                        force3D: true,
                        ease: Quad.easeInOut,
                        onComplete: function() {
                            TweenMax.to($bounce, 3, {
                                // scaleX:1,
                                scaleY: 1,
                                force3D: true,
                                ease: Elastic.easeOut,
                                easeParams: [1.1, 0.12]
                            })

                            $('.menu-wrapper3').animate({'z-index': "-9999", }, 10);
                        }
                    })

//            $('.menu-wrapper').toggleClass('hide')
                }
            });


            TweenMax.to($(this).children(".menu-item-button3"), 0.3, {
                delay: delay,
                y: 0,
                force3D: true,
                ease: Quint.easeIn
            });
        })

    }
})
