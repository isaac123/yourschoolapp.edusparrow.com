
(function (e) {
    function t(e) {
        return "object" == typeof e ? e : {
            top: e,
            left: e
        }
    }
    var n = e.scrollTo = function (t, n, i) {
        e(window).scrollTo(t, n, i)
    };
    n.defaults = {
        axis: "xy",
        duration: parseFloat(e.fn.jquery) >= 1.3 ? 0 : 1,
        limit: !0
    }, n.window = function () {
        return e(window)._scrollable()
    }, e.fn._scrollable = function () {
        return this.map(function () {
            var t = this,
                    n = !t.nodeName || -1 != e.inArray(t.nodeName.toLowerCase(), ["iframe", "#document", "html", "body"]);
            if (!n)
                return t;
            var i = (t.contentWindow || t).document || t.ownerDocument || t;
            return /webkit/i.test(navigator.userAgent) || "BackCompat" == i.compatMode ? i.body : i.documentElement
        })
    }, e.fn.scrollTo = function (i, r, s) {
        return "object" == typeof r && (s = r, r = 0), "function" == typeof s && (s = {
            onAfter: s
        }), "max" == i && (i = 9e9), s = e.extend({}, n.defaults, s), r = r || s.duration, s.queue = s.queue && s.axis.length > 1, s.queue && (r /= 2), s.offset = t(s.offset), s.over = t(s.over), this._scrollable().each(function () {
            function o(e) {
                c.animate(d, r, s.easing, e && function () {
                    e.call(this, i, s)
                })
            }
            if (null != i) {
                var a, l = this,
                        c = e(l),//e('#timeline-body'),//
                        u = i,
                        d = {},
                        h = c.is("html,body");
                switch (typeof u) {
                    case "number":
                    case "string":
                        if (/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(u)) {
                            u = t(u);
                            break
                        }
                        if (u = e(u, this), !u.length)
                            return;
                    case "object":
                        (u.is || u.style) && (a = (u = e(u)).offset())
                }
                e.each(s.axis.split(""), function (e, t) {
                    var i = "x" == t ? "Left" : "Top",
                            r = i.toLowerCase(),
                            p = "scroll" + i,
                            f = l[p],
                            m = n.max(l, t);
                    if (a)
                        d[p] = a[r] + (h ? 0 : f - c.offset()[r]), s.margin && (d[p] -= parseInt(u.css("margin" + i)) || 0, d[p] -= parseInt(u.css("border" + i + "Width")) || 0), d[p] += s.offset[r] || 0, s.over[r] && (d[p] += u["x" == t ? "width" : "height"]() * s.over[r]);
                    else {
                        var g = u[r];
                        d[p] = g.slice && "%" == g.slice(-1) ? parseFloat(g) / 100 * m : g
                    }
                    s.limit && /^\d+$/.test(d[p]) && (d[p] = d[p] <= 0 ? 0 : Math.min(d[p], m)), !e && s.queue && (f != d[p] && o(s.onAfterFirst), delete d[p])
                }), o(s.onAfter)
            }
        }).end()
    }, n.max = function (t, n) {
        var i = "x" == n ? "Width" : "Height",
                r = "scroll" + i;
        if (!e(t).is("html,body"))
            return t[r] - e(t)[i.toLowerCase()]();
        var s = "client" + i,
                o = t.ownerDocument.documentElement,
                a = t.ownerDocument.body;
        return Math.max(o[r], a[r]) - Math.min(o[s], a[s])
    }
})(jQuery);

(function (e) {
    function t(t, n, i) {

        var r = n.hash.slice(1),
                s = document.getElementById(r) || document.getElementsByName(r)[0];
        if (s) {
            t && t.preventDefault();
            var o = e(i.target);
            if (!(i.lock && o.is(":animated") || i.onBefore && i.onBefore.call(i, t, s, o) === !1)) {
                if (i.stop && o.stop(!0), i.hash) {
                    var a = s.id == r ? "id" : "name",
                            l = e("<a> </a>").attr(a, r).css({
                        position: "absolute",
                        top: e(window).scrollTop(),
                        left: e(window).scrollLeft()
                    });
                    s[a] = "", e("body").prepend(l), location = n.hash, l.remove(), s[a] = r
                }
                o.scrollTo(s, i).trigger("notify.serialScroll", [s])
            }
        }
    }
    var n = location.href.replace(/#.*/, ""),
            i = e.localScroll = function (t) {
//                console.log(t)
                e("body").localScroll(t)
            };
    i.defaults = {
        duration: 1e3,
        axis: "y",
        event: "click",
        stop: !0,
        target: $('#timeline-body'),
        reset: !0
    }, i.hash = function (n) {
        if (location.hash) {
            if (n = e.extend({}, i.defaults, n), n.hash = !1, n.reset) {
                var r = n.duration;
                delete n.duration, e(n.target).scrollTo(0, n), n.duration = r
            }//chnages made
            t(0, location, n)

        }
    }, e.fn.localScroll = function (r) {
        function s() {
            return !(!this.href || !this.hash || this.href.replace(this.hash, "") != n || r.filter && !e(this).is(r.filter))
        }
        return r = e.extend({}, i.defaults, r), r.lazy ? this.bind(r.event, function (n) {
            var i = e([n.target, n.target.parentNode]).filter(s)[0];
            i && t(n, i, r)
        }) : this.find("a,area").filter(s).bind(r.event, function (e) {
            t(e, this, r)
        }).end().end()
    }
})(jQuery);

$(document).ready(function () {
    function e() {  
        
        if (!o) {
            var e = $('#timeline-body').scrollTop(),
                    t = $('#timeline-body').height();
                    
            $.each(c, function (n, i) {
                var r = ($(u, i), $(i).position().top),//offset changes made
                        s = $(i).height(),
                        o = r + s;
//                 console.log(r)
//                 console.log(e)
                if (r > e)
                    $(i).removeClass("active-section").removeClass("below-section").removeClass("mid-section").addClass("above-section");
                else if (e >= r && o > e) {
                    var a = $(i).attr("id"),
                            l = "bg-1";// a.replace("col", "bg");
                    if (e + t >= o)
                        $(i).removeClass("above-section").removeClass("below-section").removeClass("active-section").addClass("mid-section"), 1 == d && $("#timeline-body").addClass(l), $("#timeline-body").addClass(l + "-mid");
                    else {
                        $(i).removeClass("above-section").removeClass("mid-section").removeClass("below-section").addClass("active-section");
//                        $('.active-section .day-header').css('position', 'fixed').css('top', '170px');
                        {
                            $("#timeline-body").attr("class")
                        }
                        $("#timeline-body").hasClass(l) || ($("#timeline-body").attr("class", ""), $("#timeline-body").addClass(l))
                    }
                } else
                    $(i).removeClass("active-section").removeClass("mid-section").removeClass("above-section").addClass("below-section")
            }), d = !1
        }
    }

    function t(t) {
        1 == t ? l = setInterval(e, 50) : clearInterval(l)
    }

    function n() {
//        console.log('d')
        var e = $(window).height(),
                t = .8 * e,
                n = e / 2 - t / 2,
                r = n + .011 * t,
                s = n + .1 * t;
        $(".day-header-wrap").css({
            "font-size": (t / 20) + "px"
        }), $(".content", p).css({
            padding: 0 + "px 0 " + 0 + "px"
        }), $("#footer .content", p).css({
            padding: r + "px 0 " + r + "px"
        })/*, $(".content", p).css({
         "min-height": e - 2 * n + "px"
         }), 
         $(".column", p).css({
         "min-height": e + "px"
         });*/
        var a = .023 * e;
        16 >= a && (a = 16), $(".column .content").css({
            "font-size": "13px"
        });
        var l = $("#intro").height(),
                c = .3 * l,
                u = c + .6 * c,
                d = .55 * l - .5 * u;
        105 > d && (d = 105), $(".secondary").css({
            "padding-top": d + "px",
            "font-size": c + "px"
        });
        var f = $(window).width();
        f - 200 < $(window).height() ? $("#main").addClass("narrow") : $("#main").removeClass("narrow");
        var m = $(".content").width() + $(".day-header-wrap").width();
        if (!o && !h && f >= 480)
            for (i = 0; m + 70 >= f && 5 >= i; ) {
                var g = .9 * $(".day-header-wrap").css("font-size").replace("px", "");
                $(".day-header-wrap").css({
                    "font-size": g + "px"
                }), m = $(".content").width() + $(".day-header-wrap").width(), i += 1
            }
        h = !1
    }

    function r() {
        var e = $('#timeline-body').scrollTop(),
                t = $("#footer").offset().top;
        e >= t && ($("#timeline-body").removeClass(), $("#timeline-body").addClass("bg-1"));
        var n = $('#timeline-body').height();
        e >= n ? ($("#page-wrap").addClass("below-intro")/*, $(".below-fold").addClass("")*/) : ($("#page-wrap").removeClass("below-intro")/*, $(".below-fold").addClass("")*/)
    }
    /*!
     * isFontFaceSupported
     * Copyright (c) Diego Perini
     * via http://paulirish.com/2009/font-face-feature-detection/
     * 
     */
    var s = function () {
        var e, t = document,
                n = t.head || t.getElementsByTagName("head")[0] || docElement,
                i = t.createElement("style"),
                r = t.implementation || {
                    hasFeature: function () {
                        return !1
                    }
                };
        i.type = "text/css", n.insertBefore(i, n.firstChild), e = i.sheet || i.styleSheet;
        var s = r.hasFeature("CSS2", "") ? function (t) {
            if (!e || !t)
                return !1;
            var n = !1;
            try {
                e.insertRule(t, 0), n = !/unknown/i.test(e.cssRules[0].cssText), e.deleteRule(e.cssRules.length - 1)
            } catch (i) {
            }
            return n
        } : function (t) {
            return e && t ? (e.cssText = t, 0 !== e.cssText.length && !/unknown/i.test(e.cssText) && 0 === e.cssText.replace(/\r+|\n+/g, "").indexOf(t.split(" ")[0])) : !1
        };
        return s('@font-face { font-family: "font"; src: "font.ttf"; }')
    }();
    s || ($("#page-wrap").addClass("no-ff"), $(".play-button").html("Play Video"));
    var o = navigator.userAgent.match(/iPad|iPhone|iPod/i),
            a = window.devicePixelRatio > 1 ? !0 : !1;
    a && $("#page-wrap").addClass("retina"), o && $("#page-wrap").addClass("ios");
    var l, c = $(".section-container"),
            u = ".day-header",
            d = !0,
            h = !0,
            p = $("#main");
//            alert('san')
//       $(window).load(function() {
    function t() {
        $("#intro .secondary, #logo-wrap, #enticer-wrap").fadeIn(500)
    }

    function i() {
        $("#intro-inner").fadeIn(500, function () {
            t()
        })
    }
    n();
    var s = (setTimeout(i, 300), !0);
//    console.log(   $(".below-fold").is(':visible'))
//    console.log(   $(".column-inner-wrap").is(':visible'))
//    console.log( 'nodispaleyed')
//    $(".column-inner-wrap").fadeIn();
//    $(".below-fold").fadeIn();
//    exit;
    $(".column-inner-wrap").fadeIn(300, function () {
//    console.log(   $(".below-fold").is(':visible'))
//    console.log(   $(".column-inner-wrap").is(':visible'))
//    console.log( 'dispaleyed')
////        console.log(s)
        if (e(), n(), 1 == s) {
            if (window.location.hash) {
                var t = window.location.hash;

                t = t.replace("_", ""), t = t.replace("#col", "#sub-nav .col"), $(t).trigger("click")
            } else {
                var t = '#col-' + $('#curdatescrl').val();
//                console.log(t)
                t = t.replace("_", ""), t = t.replace("#col", "#sub-nav .col"), $(t).trigger("click")
            }
            s = !1
        }

    }), $("#sub-nav a, #logo-small, #logo, #enticer").localScroll({
        offset: {
            top: 1,
            left: 0
        },
        lazy: !0,
        duration: 1e3,
        hash: !0
    }), $(".date-picker").datepicker({
        dateFormat: "yy-mm-dd"
    }), $("#page-wrap").addClass("loaded"), o || ($('#timeline-body').scroll(function () {
//        console.log('d')
        r(), e()
    }), $(window).resize(function () {
        //n(), e()
    }), $(window).scroll(function () {
//           var actualscroll = $('.active-section ').scrollTop() - $('#timeline-body').offset().top ;
//        console.log($('.active-section ').offset().top)
//        console.log($('.active-section .day-header').offset().top)
//        console.log($('#timeline-body').scrollTop())
//           console.log(actualscroll)  
//        $('.active-section .day-header').css('position', 'absolute').css('top', '207px')
    }))
//        }), 
            , o || ($(".event").hover(function () {
                $(this).addClass("hovering")
            }, function () {
                $(this).removeClass("hovering")
            }), $(".play-button").hover(function () {
                s && $(this).html("&lsaquo;")
            }, function () {
                s && $(this).html("&rsaquo;")
            })), $(".expand").click(function (e) {
        if ($(e.target).is("a")) {
            if (!$(this).hasClass("expand") && !$(this).hasClass("show-details"))
                return !0;
            e.preventDefault()
        }
        var n = getSelection().toString();
        if (!n) {
            var i, r;
            i = $(this).hasClass("event") ? $(this) : $(this).parents(".event");
            var s = $(this).parents(".content"),
                    a = $(i).attr("id");
            window.location.hash = "#_" + a, a = "#" + a, r = ".full-details", $(i).addClass("expanding");
            var l = $(window).scrollTop();
            if ($(i).hasClass("expanded")) {
                if ($(".expand", i).html('More Details <span class="symbol">+</span>'), t(!0), $(i).offset().top < l) {
                    var c = -1 * $(s).css("padding-top").replace("px", "");
                    o || $.scrollTo(a, 500, {
                        offset: {
                            top: c,
                            left: 0
                        }
                    })
                }
                $(i).removeClass("expanded").find('.shordesc').slideDown(500)
                $(i).removeClass("expanded").find(r).slideUp(500, function () {
                    t(!1), $(i).removeClass("expanding")
                })
            } else {
                $(".expand", i).html('Less Details <span class="symbol">-</span>'), t(!0), $(i).offset().top > l || $(i).offset().top > l;
                var c = -1 * $(s).css("padding-top").replace("px", "");
                o || $.scrollTo(a, 500, {
                    offset: {
                        top: c,
                        left: 0
                    }
                }),
                        $(i).removeClass("expanded").find('.shordesc').slideUp(500),
                        $(i).addClass("expanded").find(r).slideDown(500, function () {
                    t(!1), $(i).removeClass("expanding")
                })
            }
            return !1
        }
    }), $(".play-button").click(function (e) {
        return e.preventDefault(), $("#video-wrap").fadeIn(700, function () {
            var e = $(window).height(),
                    t = .7 * $(window).width(),
                    n = .5625 * t,
                    i = .45 * (e - n) + "px";
            0 > i && (i = 0), $("#video").css("margin-top", i), $("#video").html('<iframe src="http://player.vimeo.com/video/48161845?autoplay=1" width="' + t + '" height="' + n + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe> <p><a href="http://vimeo.com/48161845">Design Week Portland</a> from <a href="http://vimeo.com/designweekportland">Design Week Portland</a> on <a href="http://vimeo.com">Vimeo</a>.</p>')
        }), !1
    }), $("#video-wrap").click(function () {
        $(this).fadeOut(500, function () {
            $("#video").html("")
        })
    }), $(".late-night").click(function () {
        return s && ($("#page-wrap").hasClass("night-mode") ? ($("#page-wrap").removeClass("night-mode"), $(this).html("Party Mode")) : ($("#page-wrap").addClass("night-mode"), $(this).hasClass("switched") || $(".day-header .day-number").each(function () {
            var e = $(this).html(),
                    t = '<div class="night-day">' + e + "</div>";
            $(this).append(t)
        }), $(this).html("Day Mode")), $(this).addClass("switched")), !1
    }), $(".final-enter").keypress(function (e) {
        13 == e.which && (e.preventDefault(), $("form").submit())
    }), $(".event:last-child").addClass("last")
});
