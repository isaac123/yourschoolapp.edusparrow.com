
// Initialize the jQuery File Upload widget:
$('.fileupload').fileupload({
    dataType: 'json',
    autoUpload: false,
//    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|mp4|mp3)$/i,
    maxFileSize: 40000000, //1MB
    // Enable image resizing, except for Android and Opera,
    // which actually support image resizing, but fail to
    // send Blob objects via XHR requests:
    disableImageResize: /Android(?!.*Chrome)|Opera/ 
    .test(window.navigator.userAgent),
    previewMaxWidth: 50,
    previewMaxHeight: 50,
    previewCrop: true,
    // Uncomment the following to send cross-domain cookies:
    //xhrFields: {withCredentials: true},
    url: commonSettings.contentsharingAjaxurl + "/contentSharingFileUpload",
       dropZone: $('#dropzone') 
}).on('fileuploaddone', function (e, data) {
    $('#content_share').removeClass('hide');
    console.log('ressdasult')
});
// Load existing files:
$('.fileupload').addClass('fileupload-processing');
// Initialize the jQuery File Upload widget:
$.ajax({
    autoUpload: false,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|mp4|mp3)$/i,
    maxFileSize: 40000000, //1MB
    // Enable image resizing, except for Android and Opera,
    // which actually support image resizing, but fail to
    // send Blob objects via XHR requests:
    disableImageResize: /Android(?!.*Chrome)|Opera/ 
    .test(window.navigator.userAgent),
    previewMaxWidth: 50,
    previewMaxHeight: 50,
    previewCrop: true,
    // Uncomment the following to send cross-domain cookies:
    //xhrFields: {withCredentials: true},
    type: "POST",
    url: commonSettings.contentsharingAjaxurl + "/contentSharingFileUpload",
    dataType: 'json',
    data: {masterid: $('#masterid').val()}
}).done(function (e, data) {
    if (e) {
        $('#content_share').removeClass('hide');
        var $form = $('.fileupload');
        $form.fileupload('option', 'done').call($form, $.Event('done'), {result: {files: e}});
    }
});
//var $form = $('.fileupload');        
//
//// Init fileuploader if not initialized
//// $form.fileupload();
//
//$form.fileupload('option', 'done').call($form, $.Event('done'), {result: {files: files}});