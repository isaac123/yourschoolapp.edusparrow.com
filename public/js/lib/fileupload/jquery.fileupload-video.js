/*
 * jQuery File Upload Video Preview Plugin
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* jshint nomen:false */
/* global define, require, window, document */

(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // Register as an anonymous AMD module:
        define([
            'jquery',
            'load-image',
            './jquery.fileupload-process'
        ], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS:
        factory(
            require('jquery'),
            require('load-image')
        );
    } else {
        // Browser globals:
        factory(
            window.jQuery,
            window.loadImage
        );
    }
}(function ($, loadImage) {
    'use strict';

    // Prepend to the default processQueue:
    $.blueimp.fileupload.prototype.options.processQueue.unshift(
        {
            action: 'loadVideo',
            // Use the action as prefix for the "@" options:
            prefix: true,
            fileTypes: '@',
            maxFileSize: '@',
            disabled: '@disableVideoPreview'
        },
        {
            action: 'setVideo',
            name: '@videoPreviewName',
            disabled: '@disableVideoPreview'
        }
    );

    // The File Upload Video Preview plugin extends the fileupload widget
    // with video preview functionality:
    $.widget('blueimp.fileupload', $.blueimp.fileupload, {

        options: {
            // The regular expression for the types of video files to load,
            // matched against the file type:
            loadVideoFileTypes:/^video\/(mp4|mp3)$/,
             maxFileSize: 40000000,
        },

        _videoElement: document.createElement('video'),

        processActions: {

            // Loads the video file given via data.files and data.index
            // as video element if the browser supports playing it.
            // Accepts the options fileTypes (regular expression)
            // and maxFileSize (integer) to limit the files to load:
            loadVideo: function (data, options) {
                if (options.disabled) {
                    return data;
                }
                var file = data.files[data.index],
                    url,
                    video;
                if (this._videoElement.canPlayType &&
                        this._videoElement.canPlayType(file.type) &&
                        ($.type(options.maxFileSize) !== 'number' ||
                            file.size <= options.maxFileSize) &&
                        (!options.fileTypes ||
                            options.fileTypes.test(file.type))) {
                    url = loadImage.createObjectURL(file);
                    if (url) {
                        video = this._videoElement.cloneNode(false);
                        video.src = url;
                        video.controls = true;
                        data.video = video;
                        return data;
                    }
                }
                return data;
            },

            // Sets the video element as a property of the file object:
            setVideo: function (data, options) {
                if (data.video && !options.disabled) {
                    data.files[data.index][options.name || 'preview'] = data.video;
                }
                return data;
            }

        }

    });

}));
//$(function () {
//    'use strict';
//    var url = commonSettings.contentsharingAjaxurl + "/contentSharingFileUpload";
//
//    $('#fileupload').fileupload({
//        url: url,
//        method: 'POST',
//        dataType: 'json',
//        autoUpload: true,
//        acceptFileTypes: /(\.|\/)(mp4)$/i,
//        maxFileSize: 40000000, // 40 MB
//        disableImageResize: /Android(?!.*Chrome)|Opera/
//            .test(window.navigator.userAgent),
//        previewMaxWidth: 300,
//        previewMaxHeight: 200,
//        previewCrop: true,
//    }).on('fileuploadadd', function (e, data) {
//        data.context = $('<div class="col-md-3 videopreview" />').appendTo('#files');
//        $.each(data.files, function (index, file) {
//            var node = $('<p/>');
//            if (!index) {
//                node
//                    .append('<br>')
//            }
//            node.appendTo(data.context);
//        });
//    }).on('fileuploadprocessalways', function (e, data) {
//        var index = data.index,
//            file = data.files[index],
//            node = $(data.context.children()[index]);
//        if (file.preview) {
//            node
//                .prepend('<br>')
//                .prepend(file.preview);
//        }
//        if (file.error) {
//            node
//                .append('<br>')
//                .append($('<span class="text-danger"/>').text(file.error));
//        }
//        if (index + 1 === data.files.length) {
//            data.context.find('button')
//                .text('Upload')
//                .prop('disabled', !!data.files.error);
//        }
//    }).on('fileuploadprogressall', function (e, data) {
//        var progress = parseInt(data.loaded / data.total * 100, 10);
//        $('#progress .progress-bar').css(
//            'width',
//            progress + '%'
//        );
//    }).on('fileuploaddone', function (e, data) {
//        $.each(data.result.files, function (index, file) {
//            if (file.url) {
//                var link = $('<a>')
//                    .attr('target', '_blank')
//                    .prop('href', file.url);
//                $(data.context.children()[index])
//                    .wrap(link).append($('<span/>').text(file.name));
//                $( "#filesHidden" ).append( '<input type="hidden" name="images[]" value="' + file.name + '">' );
//            } else if (file.error) {
//                var error = $('<span class="text-danger"/>').text(file.error);
//                $(data.context.children()[index])
//                    .append('<br>')
//                    .append(error);
//            }
//        });
//    }).on('fileuploadfail', function (e, data) {
//        $.each(data.files, function (index, file) {
//            var error = $('<span class="text-danger"/>').text('File upload failed.');
//            $(data.context.children()[index])
//                .append('<br>')
//                .append(error);
//        });
//    }).prop('disabled', !$.support.fileInput)
//        .parent().addClass($.support.fileInput ? undefined : 'disabled');
//
//});