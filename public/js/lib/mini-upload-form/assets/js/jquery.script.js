$(function () {
    var ul = $('.dropboxupl ul');

    $('#drop a').click(function () {
        // Simulate a click on the file input button
        // to show the file browser dialog
        $(this).parent().find('input').click();
    });

    // Initialize the jQuery File Upload plugin
    $('.dropboxupl').fileupload({
        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),
        url: commonSettings.announcementAjaxurl + "/dashboardfileUpload",
        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {
            var type = $('#type').val();
            var removeid = $('#removeid').val();
            var tpl = $('<li class="working"><input type="text" value="0" data-width="30" data-height="30"' +
                    ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><i class="fa  fa-file-text"></i><p></p><span class="hide" id=' + type + '_' + removeid + ' onclick="announcementSettings.removeUploadedFile(this)"></span><span class="fa fa-spin fa-spinner" style="background: none;"></span></li>');
            // Append the file name and file size
console.log(data.files[0]);
            tpl.find('p').text(data.files[0].name)
                    .append('<i><b style="display:none;">_</b>' + formatFileSize(data.files[0].size) + '</i>');
            // Add the HTML to the UL element
            data.context = tpl.appendTo(ul);

            // Initialize the knob plugin
          //  tpl.find('input').knob();

            // Listen for clicks on the cancel icon
            tpl.find('span').click(function () {
                if (tpl.hasClass('working')) {
                    jqXHR.abort();
                }
                tpl.fadeOut(function () {
                    tpl.remove();
                });
            });

            // Automatically upload the file once it is added to the queue
            var jqXHR = data.submit();
        },
        progress: function (e, data) {

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if (progress == 100) {
               // data.context.removeClass('working');
		data.context.find('span').removeClass('hide');
		data.context.find('.fa-spin').remove();
            }
        },
        fail: function (e, data) {
            // Something has gone wrong!
            data.context.addClass('error');
        }

    });


    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }

});