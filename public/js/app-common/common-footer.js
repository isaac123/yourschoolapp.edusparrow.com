(function() {
    [].slice.call(document.querySelectorAll('.tabs')).forEach(function(el) {
        new CBPFWTabs(el);
    });
})();
function resetActive(event, percent, step) {
    $(".progress-bar").css("width", percent + "%").attr("aria-valuenow", percent);
    $(".progress-completed").text(percent + "%");

    $("div").each(function() {
        if ($(this).hasClass("activestep")) {
            $(this).removeClass("activestep");
        }
    });

    if (event.target.className == "col-md-2") {
        $(event.target).addClass("activestep");
    }
    else {
        $(event.target.parentNode).addClass("activestep");
    }

    hideSteps();
    showCurrentStepInfo(step);
}

function hideSteps() {
    $("div").each(function() {
        if ($(this).hasClass("activeStepInfo")) {
            $(this).removeClass("activeStepInfo");
            $(this).addClass("hiddenStepInfo");
        }
    });
}


commonSettings.run_waitMe = function(ctrl) {
//    console.log('Loading')
    $(ctrl).waitMe({
        effect: 'roundBounce',
        text: 'Processing your request ...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: ''
    });
}

function showCurrentStepInfo(step) {
    var id = "#" + step;
    $(id).addClass("activeStepInfo");
}
$(document).ready(function() {
    $("#wizard").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onFinished: function(event, currentIndex)
        {
            $.ajax({
                type: "POST",
                url: "settings/completeSettings",
                data: {iscomplete: '1'},
                success: function(res) {
                    $('.modal-title').html('Message');
                    $('.modal-body').html(res);
                    $('#messageBox').click();
                    $('#msg_ok').click(function() {
                        window.location.href = 'assigning';
                    });

                }
            });
        }
    });
    
});

commonSettings.logout = function(mailurl,appurl) {
var frame = document.createElement("iframe");
frame.src = mailurl;
frame.style.position = "relative";
frame.style.left = "-9999px";
$(frame).on('load',function(){
    window.location.href =appurl;
});
document.body.appendChild(frame);
}
commonSettings.changePass = function() {
    $.ajax({
        type: "POST",
        url: commonSettings.usersAjaxurl + "/changePassSettings",
        dataType: "html",
        success: function(res) {
            $('.formBox-title').html('Change Password');
            $('.formBox-body').html(res);
            $('#formModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#formModal').modal('show')
            $('#confirm_save').unbind('click').click(function() {
                $('#confirm_close').click();
                var passwordData = {};
                $("#changePassword").find('input').each(function() {
                    passwordData[$(this).attr('name')] = $.trim($(this).val());
                });

                console.log(passwordData)
                $.ajax({
                    type: "POST",
                    url: commonSettings.usersAjaxurl + "/changePassword",
                    data: passwordData,
                    dataType: "JSON",
                    success: function(res) {
                        if (res) {
                            console.log(res)
                            console.log(res.type)
                            if (res['type'] == 'error') {
                                swal("Error", res['message'], "error");
                                swal({
                                    title: "Error",
                                    text: res['message'],
                                    type: "error",
                                    confirmButtonText: 'Ok',
                                    closeOnConfirm: true,
                                },
                                        function(isConfirm) {
                                            if (isConfirm) {

                                                $('#formModal').modal('show')
                                            }
                                        });
                                return false;
                            }
                            else if (res['type'] == 'success') {
                                swal({
                                    title: "Done",
                                    text: res['message'],
                                    type: "success",
                                    confirmButtonText: 'Ok',
                                    closeOnConfirm: true,
                                },
                                        function(isConfirm) {
                                            if (isConfirm) {
                                                $('#oldpassword').val('');
                                                $('#confirmPassword').val('');
                                                $('#password').val('');
                                            }
                                        });
                            }

                        }
                    }
                });
            });
        }
    });
}