var commonSettings = {};
commonSettings.studentListByLoggedUser = {};
commonSettings.copyAttributes = function (src, dest) {
    var destination = $(dest).eq(0);
    var source = $(src)[0];
    for (i = 0; i < source.attributes.length; i++) {
        var a = source.attributes[i];
        destination.attr(a.name, a.value);
    }
}
commonSettings.NumberOfRecords = 20;
commonSettings.jqueryDynmicTable = 5;


commonSettings.getStudentListByCurrentLoginID = function () {

    $.ajax({
        type: "POST",
        url: commonSettings.searchAjaxurl + "/getStudentListByCurrentLoginID",
        dataType: "JSON",
        success: function (res) {
            commonSettings.studentListByLoggedUser['stu_ids'] = res.stu_ids;
            console.log(res);
        }
    });
}
commonSettings.validateAmount = function (ctrl)
{
    var $this = $(ctrl);
    var splamount = $this.val().replace(/\[^0-9\.]/g, "");
    console.log(splamount)
    if (!parseFloat(splamount) > 0) {
        $this.val('');
        $('.msgBox-title').html('Message');
        $('.msgBox-body').html('Please Enter Valid Quantity');
        $('#messageBoxModal').modal();
        // return false;
    }
};

var myApp ={};
var pleaseWaitDiv = $(' <div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="formModal" aria-hidden="true">'+
        '<div class="modal-dialog">'+
            '<div class="modal-content">'+
                '<div class="modal-header">'+
                    '<h4>Processing...</h4>'+
                '</div>'+
                '<div class="modal-body  formBox-body"><span class="text-display">Processing your request...</span>'+
                    '<div class="progress progress-striped active progress-sm">'+
                                    ' <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">'+
                                        ' <span class="sr-only"></span>'+
                                     '</div>'+
                                ' </div>  '+
                '</div>'+
            '</div>'+
        '</div>'+   
    '</div>');

myApp.showPleaseWait = function () {
//    console.log(pleaseWaitDiv.html())
    pleaseWaitDiv.modal();
}

myApp.hidePleaseWait = function () {
    pleaseWaitDiv.modal('hide');
    pleaseWaitDiv.remove()
    $('.modal-backdrop').remove()
}