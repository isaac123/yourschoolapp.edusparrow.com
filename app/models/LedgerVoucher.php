<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class LedgerVoucher extends \Phalcon\Mvc\Model {

    public $voucher_id; //linkage
    public $closedby; //linkage
    public $closedcomments;
    public $cancelledby; //linkage
    public $cancelledcomments;
    public $paidby; //linkage
    public $paidcomments;
    public $vouchertype;
    public $amount;
    public $reason;
    public $receivedby;
    public $date;
    public $creditledger; //linkage
    public $debitledger; //linkage
    public $created_by; //linkage
    public $created_date;
    public $ledgerstatus;
    public $closed_date;
    public $record_type_id; //linkage
    public $record_type_item_id; //linkage

    /**
     * Initializer method for model.
     */

    public function initialize() {


        $this->belongsTo('creditledger', 'OrganizationalStructureValues', 'id', array(
            'alias' => 'OrganizationalStructureValues'
        ));

        $this->belongsTo('debitledger', 'OrganizationalStructureValues', 'id', array(
            'alias' => 'OrganizationalStructureValues'
        ));
    }

    public function beforeSave() {
        $voucherid = $this->voucher_id;
        if ($voucherid) {
            if ($this->vouchertype == 'RECEIPT') {
                $appln_recordtypeid = RecordTypes::findFirst("record_name LIKE 'APPLICATION FEES'")->id;
                $fee_recordtypeid = RecordTypes::findFirst("record_name LIKE 'FEE'")->id;
                if ($this->record_type_id == $appln_recordtypeid) { //Application
                    $result = json_decode($this->getDI()->getVoucherEditTrigger()->editApplicationVoucher($this));
                } else if ($this->record_type_id == $fee_recordtypeid) { //Fee
                    $result = json_decode($this->getDI()->getVoucherEditTrigger()->editFeeVoucher($this));
                } else {
                    return true;
                }
                if ($result->type == 'success') {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {

            return true;
        }
    }

    public function afterSave() {
        $result = 1;
        if ($this->vouchertype == 'JOURNAL' || $this->vouchertype == 'RECEIPT' || $this->vouchertype == 'PAYMENT') {
            $result = json_decode($this->getDI()->getVoucherEditTrigger()->editVoucher($this));
            if ($result->type == 'success') {
                $result = 1;
            } else {
                return false;
            }
        }
        if (!in_array($this->record_type_id, array('5', '6'))) { //exclude Salary & Advance Edit
            $transcation_det = Transaction::findFirst('voucherid=' . $this->voucher_id);
            if ($transcation_det) {
                $transcation_det->debit = $this->debitledger;
                $transcation_det->credit = $this->creditledger;
                $transcation_det->amount = $this->amount;
                $transcation_det->voucher_date = $this->date;
                if ($transcation_det->save()) {
                    return true;
                } else {
                    $error = '';
                    foreach ($transcation_det->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = ' ' . $error . ' ';
                    print_r(json_encode($message));
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    public function beforeDelete() {
        $voucherid = $this->voucher_id;
        if ($this->vouchertype == 'CONTRA') {
            $result = json_decode($this->getDI()->getVoucherDeleteTrigger()->deleteContraVoucher($voucherid));
        } else if ($this->vouchertype == 'PAYMENT') {

            $salary_recordtypeid = RecordTypes::findFirst("record_name LIKE 'SALARY VOUCHER'")->id;
            $advance_recordtypeid = RecordTypes::findFirst("record_name = 'ADVANCE'")->id;

            if ($this->record_type_id == $salary_recordtypeid) { //salary
                $result = json_decode($this->getDI()->getVoucherDeleteTrigger()->deleteSalaryVoucher($voucherid));
            } else if ($this->record_type_id == $advance_recordtypeid) { //advance
                $result = json_decode($this->getDI()->getVoucherDeleteTrigger()->deleteAdvanceVoucher($voucherid));
            } else {   //normar payment
                $result = json_decode($this->getDI()->getVoucherDeleteTrigger()->deletePaymentVoucher($voucherid));
            }
        } else if ($this->vouchertype == 'RECEIPT') {

            $appln_recordtypeid = RecordTypes::findFirst("record_name LIKE 'APPLICATION FEES'")->id;
            $fee_recordtypeid = RecordTypes::findFirst("record_name LIKE 'FEE'")->id;

            if ($this->record_type_id == $appln_recordtypeid) { //Application
                $result = json_decode($this->getDI()->getVoucherDeleteTrigger()->deleteApplicationVoucher($voucherid));
            } else if ($this->record_type_id == $fee_recordtypeid) { //Fee
                $result = json_decode($this->getDI()->getVoucherDeleteTrigger()->deleteFeeVoucher($voucherid));
            } else {   //normar receipt
                $result = json_decode($this->getDI()->getVoucherDeleteTrigger()->deleteReceiptVoucher($voucherid));
            }
        } else if ($this->vouchertype == 'JOURNAL') {
            $result = json_decode($this->getDI()->getVoucherDeleteTrigger()->deleteJournalVoucher($voucherid));
        } else if ($this->vouchertype == 'SUSPENSE') {
            $result = json_decode($this->getDI()->getVoucherDeleteTrigger()->deleteSuspenseVoucher($voucherid));
        }

        if ($result->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

    public function afterDelete() {
        $transaction = Transaction::find('voucherid=' . $this->voucher_id);
        if (count($transaction) > 0) {
            if (!$transaction->delete()) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

}
