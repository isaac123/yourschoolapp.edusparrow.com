<?php

class RelievingConditions extends \Phalcon\Mvc\Model {

    public $id;
    public $module;
    public $condition_name;
    public $condition_desc;
    public $condition;
    public $is_enabled;
    public $user_type;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
