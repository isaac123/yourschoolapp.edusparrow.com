<?php

class FeeItemSplitup extends \Phalcon\Mvc\Model {

    public $id;
    public $fee_item_id; //linkage
    public $name;
    public $amount;
    public $created_by; //linkage
    public $created_on;
    public $modified_by; //linkage
    public $modified_on;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
        $this->belongsTo('fee_item_id', 'FeeInstalments', 'id', array(
            'alias' => 'FeeInstalments',
            'reusable' => true
        ));
    }

}
