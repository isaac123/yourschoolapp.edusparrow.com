<?php

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Behavior\SoftDelete,
    Phalcon\Mvc\Model\Relation;

class GroupSubjectsTeachers extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $classroom_master_id; //linkage
    public $subject_id; //linkage
    public $teachers_id; //linkage

    public function initialize() {
        $this->belongsTo('classroom_master_id', 'ClassroomMaster', 'id', array(
            'alias' => 'ClassroomMaster',
            'reusable' => true
        ));
        $this->hasMany('id', 'StudentSubteacherRating', 'subject_master_id', array(
            'alias' => 'StudentSubteacherRating',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Subject Teachers cannot be deleted because he/she has activity in the system'
            )
        ));
        $this->hasMany('id', 'ClassTest', 'grp_subject_teacher_id', array(
            'alias' => 'ClassTest',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
                'message' => 'ClassTest cannot be deleted because he/she has activity in the system'
            )
        ));
        $this->hasMany('id', 'AssignmentsMaster', 'grp_subject_teacher_id', array(
            'alias' => 'AssignmentsMaster',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Assignment cannot be deleted because he/she has activity in the system'
            )
        ));
        $this->hasMany('id', 'MainexamMarks', 'grp_subject_teacher_id', array(
            'alias' => 'MainexamMarks',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Marks cannot be deleted because he/she has activity in the system'
            )
        ));
        $this->hasMany('id', 'HomeWorkTable', 'grp_subject_teacher_id', array(
            'alias' => 'HomeWorkTable',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Homework cannot be deleted because he/she has activity in the system'
            )
        ));
    }

}
