<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class InvoiceTable extends \Phalcon\Mvc\Model {

    public $id;
    public $purchase_order_no;
    public $invoice_no;
    public $dc_no;
    public $amount;
    public $details;
    public $movement_master_id;
    public $invoice_date;
    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}

