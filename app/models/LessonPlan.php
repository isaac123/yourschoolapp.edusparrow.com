<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class LessonPlan extends \Phalcon\Mvc\Model {

    public $id;
    public $subject_comb_id; //linkage
    public $topic_id; //linkage
    public $name;
    public $created_date;
    public $created_by; //linkage
    public $modified_date;
    public $modified_by; //linkage

    /**
     * Initializer method for model.
     */

    public function initialize() {
        
    }

}
