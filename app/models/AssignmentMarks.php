<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class AssignmentMarks extends \Phalcon\Mvc\Model {

    public $id;
    public $assignment_id; //linkage
    public $student_id; //linkage
    public $marks;
    public $outof;
    public $createdby; //linkage
    public $createdon;
    public $modifiedby; //linkage
    public $modifiedon;

    public function initialize() {
        
    }

    public function afterSave() {
        $response = $this->getDI()->getNotificationTrigger()->assignmentMarksNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
