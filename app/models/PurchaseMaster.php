<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class PurchaseMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $vendor_id;
    public $date;
    public $comments;
    public $status;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
