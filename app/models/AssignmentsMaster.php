<?php

use Phalcon\Mvc\Model,
    Phalcon\Mvc\Model\Relation;

/**
 * ResetPasswords
 * Stores the reset password codes and their evolution
 */
class AssignmentsMaster extends Model {

    public $id;
    public $grp_subject_teacher_id; //linkage
    public $topic;
    public $desc;
    public $comments;
    public $is_evaluation;
    public $is_upload;
    public $attachment;
    public $subjct_modules; //linkage
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;
    public $submission_date;
    public $status;
    public $file;
    public $acknowledge;

    public function initialize() {
        $this->belongsTo('grp_subject_teacher_id', 'GroupSubjectsTeachers', 'id', array(
            'alias' => 'GroupSubjectsTeachers',
            'reusable' => true
        ));
    }

    public function afterSave() {
        $response = $this->getDI()->getNotificationTrigger()->overallAssignmentNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
