<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class StudentClassteacherRating extends \Phalcon\Mvc\Model {

    public $rating_id; //linkage
    public $student_id; //linkage
    public $rating_division_id; //linkage
    public $class_master_id; //linkage
    public $rating_category; //linkage
    public $rating_value; //linkage
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    public function initialize() {
        $this->belongsTo('class_master_id', 'GroupClassTeachers', 'id', array(
            'alias' => 'GroupClassTeachers',
            'reusable' => true
        ));
    }

    public function afterSave() {
        $response = $this->getDI()->getNotificationTrigger()->classRatingNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
