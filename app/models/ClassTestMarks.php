<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class ClassTestMarks extends \Phalcon\Mvc\Model {

    public $classtest_marks_id;
    public $class_test_id; //linkage
    public $student_id; //linkage
    public $marks;
    public $outof;
    public $createdby; //linkage
    public $createdon;
    public $modifiedby; //linkage
    public $modifiedon;

    public function initialize() {
        
    }

    public function afterSave() {
        $response = $this->getDI()->getNotificationTrigger()->classtestNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
