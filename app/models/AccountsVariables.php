<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class AccountsVariables extends \Phalcon\Mvc\Model {

    public $variable_id;
    public $variable_name;
    public $variable_value;
    public $created_by;
    public $created_date;
    public $modified_by;
    public $modified_date;

    public function initialize() {
        
    }

}
