<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class SentSmsResults extends \Phalcon\Mvc\Model {

    public $id;
    public $user_id;  //linkage
    public $message;
    public $date;
    public $message_status;
    public $responseid;

    public function initialize() {
        
    }

}
