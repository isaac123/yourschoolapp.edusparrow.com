<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class StaffRating extends \Phalcon\Mvc\Model {

    public $rating_id;
    public $staff_id; //linkage
    public $rating_category; //linkage
    public $rating_value; //linkage
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    public function initialize() {


        $this->belongsTo('staff_id', 'StaffInfo', 'id', array(
            'alias' => 'StaffInfo'
        ));

        $this->belongsTo('rating_value', 'RatingCategoryValues', 'id', array(
            'alias' => 'RatingCategoryValues'
        ));
    }

}
