<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class StaffInfo extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $appointment_no;
    public $aggregate_key; //linkage
    public $loginid;
    public $Staff_Name;
    public $Gender;
    public $Date_of_Birth;
    public $Date_of_Joining;
    public $Mobile_No;
    public $Email;
    public $Address1;
    public $Address2;
    public $State;
    public $Country;
    public $Pin;
    public $Phone;
    public $status;
    public $relieve_status;
    public $biometric_id;
    public $pf_no;
    public $esi_no;

    /**
     * Initializer method for model.
     */
    public function initialize() {

        $this->hasMany('id', 'staff_general_master', 'staff_id', array(
            'alias' => 'StaffGeneralMaster',
            'foreignKey' => array(
                'message' => 'User cannot be deleted because he/she has activity in the system'
            )
        ));
    }

    /* public function afterUpdate() {
      $aggreArr = explode(',', $this->aggregate_key);
      if (count($aggreArr) > 0) {
      $params['username'] = $this->loginid; //'saranormalstaff'; //
      $params['aggregateid'] = $this->aggregate_key; //'saranormalstaff'; //
      $this->getDI()->getCalendar()->changeprincipalgrants($params);
      }
      } */

//    public function afterSave() {
//        if ($this->loginid) {
//            $params['username'] = $this->loginid;
//            $params['password'] = date('d/m/Y', $this->Date_of_Birth);
//            $params['fullname'] = $this->Staff_Name;
//            $params['email_address'] = $this->Email;
//            $params['rolenames'] = 'Staff';
//            $params['aggregateid'] = $this->aggregate_key;
//            $this->getDI()->getCalendar()->createuserprincipals($params);
//        }
//    }
}
