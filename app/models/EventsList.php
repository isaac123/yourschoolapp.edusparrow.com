<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 *  Not currently used
 *  
 */
class EventsList extends \Phalcon\Mvc\Model {

    public $id;
    public $event_id;
    public $events_to;
    public $events_toname;
    public $status;
    public $created_by;
    public $created_date;
    public $comments_by;
    public $modified_by;
    public $modified_date;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

    public function afterSave() {
        $response = $this->getDI()->getNotificationTrigger()->eventNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
