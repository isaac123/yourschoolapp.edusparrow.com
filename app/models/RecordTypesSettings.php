<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class RecordTypesSettings extends \Phalcon\Mvc\Model {

    public $id;
    public $record_types_id;  //linkage
    public $debit_ledger_ids;  //linkage
    public $allow_child_debit;
    public $credit_ledger_ids;  //linkage
    public $allow_child_credit;
    public $approval_required;
    public $approval_amount;
    public $created_by;
    public $created_on;
    public $modified_by;
    public $modified_on;

    public function initialize() {


        $this->belongsTo('record_types_id', 'RecordTypes', 'id', array(
            'alias' => 'RecordTypes'
        ));
    }

}
