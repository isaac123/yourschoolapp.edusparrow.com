<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class TaskActivities extends \Phalcon\Mvc\Model {

    public $id;
    public $task_id;
    public $initiator_id;
    public $task_from;
    public $task_to;
    public $type;
    public $status;
    public $comments;
    public $action;
    public $created_by;
    public $created_date;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
