<?php
use Phalcon\Mvc\Model\Validator\Uniqueness;

class CountryMaster extends \Phalcon\Mvc\Model
{
    
    public $id;
    public $country_code;
    public $country_name;
    
    /**
     * Initializer method for model.
     */
    public function initialize()
    {
        
        
    }
}