<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 *  Not currently used
 *  
 */
class LedgerMaster extends \Phalcon\Mvc\Model {

    public $lid;
    public $balance;
    public $type;
    public $created_by;
    public $created_date;
    public $parentid;
    public $ledgername;

    /**
     * Initializer method for model.
     */
    public function initialize() {

        $this->hasMany('lid', 'Transaction', 'debit', array(
            'alias' => 'Transaction',
            'foreignKey' => array(
                'message' => 'Ledger cannot be deleted as it has transactions associated with it'
            )
        ));

        $this->hasMany('lid', 'Transaction', 'credit', array(
            'alias' => 'Transaction',
            'foreignKey' => array(
                'message' => 'Ledger cannot be deleted as it has transactions associated with it'
            )
        ));
    }

}
