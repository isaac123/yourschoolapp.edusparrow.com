<?php
use Phalcon\Mvc\Model\Validator\Uniqueness;

class StudentProfileSettings extends \Phalcon\Mvc\Model
{

    public $id;
    public $parent_id;   //linkage  
    public $column_name;
    public $field_name;
    public $form_name;
    public $help_text;
    public $field_format;
    public $default_value;
    public $hide_or_show;
    public $mandatory;
    public $created_by; //linkage
    public $created_on;
    public $modified_by; //linkage
    public $modified_on;
    
    /**
     * Initializer method for model.
     */
    public function initialize()
    {
        
        
    }
}