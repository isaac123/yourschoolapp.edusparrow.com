<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class OrganizationalStructureMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $name;
    public $parent_id;  //linkage
    public $cycle_node;
    public $mandatory_for_admission;
    public $mandatory_for_appointment;
    public $is_subordinate;
    public $promotion_status;
    public $module;
    public $nodefont;
    public $color;
    
    public $upload_flag;
    public $basic_level;
    public $secondary_level;
    public $specific;
    public $default;
    public $getSubjects;

    public function initialize() {
        
        $this->hasMany('id', 'OrganizationalStructureValues', 'org_master_id', array(
            'alias' => 'OrganizationalStructureValues',
            'foreignKey' => array(
                'message' => 'Master node cannot be deleted!. Value nodes are created for this master node'
            )
        ));
    }

}
