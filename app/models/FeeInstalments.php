<?php

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Behavior\SoftDelete,
    Phalcon\Mvc\Model\Relation;

class FeeInstalments extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $fee_instalment_type_id; //linkage
    public $name;
    public $fee_amount;
    public $penalty_amount;
    public $fee_collection_start;
    public $fee_collection_end;
    public $created_by; //linkage
    public $created_on;
    public $modified_by; //linkage
    public $modified_on;
    public $confirmAsign;

    /**
     * Initializer method for model.
     */
    public function initialize() {



        $this->hasMany('id', 'FeeItemSplitup', 'fee_item_id', array(
            'alias' => 'FeeItemSplitup',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Fee cannot be deleted. Fee instalment activity in the system'
            )
        ));

        $this->belongsTo('fee_instalment_type_id', 'FeeInstalmentType', 'id', array(
            'alias' => 'FeeInstalmentType',
            'reusable' => true
        ));
    }

    public function afterCreate() {
//        print_r($this->confirmAsign); 
//        return false;
        $feeInsTyp = FeeInstalmentType::findFirstById($this->fee_instalment_type_id);
        $feeMas = FeeMaster::findFirstById($feeInsTyp->fee_master_id);
        if ($feeMas->is_mandatory == 1 && $feeInsTyp->is_default == 1 && $this->confirmAsign == 1) {
            $response = $this->getDI()->getFeeTrigger()->addfeewithins($this);
            $res = json_decode($response);
            if ($res->type == 'success') {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

}
