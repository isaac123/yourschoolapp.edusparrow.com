<?php

class InitialBalance extends \Phalcon\Mvc\Model {

    public $id;
    public $lid; //linkage
    public $opening_credit;
    public $opening_debit;
    public $closing_credit;
    public $closing_debit;
    public $created_by; //linkage
    public $created_on;
    public $modified_by; //linkage
    public $modified_on;

    public function initialize() {
        
    }

}
