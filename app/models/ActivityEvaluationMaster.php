

<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class ActivityEvaluationMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $subject_master_id;

    public function initialize() {

        $this->hasMany('id', 'ActivityEvaluationItems', 'activity_master_id', array(
            'alias' => 'ActivityEvaluationItems',
            'foreignKey' => array(
                'message' => 'Activity cannot be deleted'
            )
        ));
    }

}
