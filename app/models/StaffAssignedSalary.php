
<?php

class StaffAssignedSalary extends \Phalcon\Mvc\Model {

    public $id;  //linkage
    public $staffid;
    public $paytype;
    public $payamount;
    public $created_by;  //linkage
    public $created_date;
    public $modified_by;  //linkage
    public $modified_date;

    public function initialize() {
        
    }

}
