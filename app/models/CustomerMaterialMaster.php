<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class CustomerMaterialMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $customer_id;
    public $customer_type;
    public $material_id;
    public $quantity;
    public $unit;
    public $date;
    public $amount;
    public $movement_master_id; //linkage
}
