<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 *  Not currently used
 *  
 */
class Events extends \Phalcon\Mvc\Model {

    public $id;
    public $status;
    public $title;
    public $description;
    public $from_date;
    public $to_date;
    public $location;
    public $type;
    public $upload;
    public $selected_userlist;
    public $created_by;
    public $created_on;
    public $modified_by;
    public $modified_on;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }
}
