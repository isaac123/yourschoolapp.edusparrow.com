<?php

class StudentFeeRequestTable extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $student_id; //linkage
    public $fee_master_id; //linkage
    public $fee_instalment_typ_id; //linkage
    public $fee_instalment_id; //linkage
    public $status;
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
