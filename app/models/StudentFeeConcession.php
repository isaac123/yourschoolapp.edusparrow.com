<?php

class StudentFeeConcession extends \Phalcon\Mvc\Model {

    public $id;
    public $stu_fee_id; //linkage
    public $concession_amt;
    public $concession_status;
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
