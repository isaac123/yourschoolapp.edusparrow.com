<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class Stock extends \Phalcon\Mvc\Model {

    public $id;
    public $material_master_id;
    public $quantity;
    public $unit_id;
   
    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}

