<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class MainexamMarks extends \Phalcon\Mvc\Model {

    public $mainexam_marks_id;
    public $mainexam_id; //linkage
    public $student_id; //linkage
    public $grp_subject_teacher_id; //linkage
    public $marks;
    public $inherited_marks;
    public $outof;
    public $inherited_outof;
    public $subject_id; //linkage
    public $createdby; //linkage
    public $createdon;
    public $modifiedby; //linkage
    public $modifiedon;

    public function initialize() {
        $this->belongsTo('grp_subject_teacher_id', 'GroupSubjectsTeachers', 'id', array(
            'alias' => 'GroupSubjectsTeachers',
            'reusable' => true
        ));
    }

    public function afterSave() {
        $response = $this->getDI()->getNotificationTrigger()->mainexamNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
