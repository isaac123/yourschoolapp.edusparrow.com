<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class FormulaTable extends \Phalcon\Mvc\Model {

    public $id;
    public $outof;
    public $formula;
    public $subject_id; //linkage
    public $grp_subject_teacher_id; //linkage
    public $mainexm_id; //linkage
    public $exam_ids; //linkage

    public function initialize() {
        
    }

}
