<?php

class StudentRelievingMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $status;
    public $student_id; //linkage
    public $edu_inst;
    public $admission_date;
    public $joining_aggregate_key; //linkage
    public $relieving_aggregate_key; //linkage
    public $scl_left_date;
    public $stdcharacter;
    public $tc_made_date;
    public $tc_date;
    public $course_complete;
    public $qualify_promo;
    public $course_comp_det;
    public $scl_name;
    public $medium;
    public $reason;
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    public function initialize() {
        
    }

}
