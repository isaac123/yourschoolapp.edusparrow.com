<?php

class VoucherEditHistory extends \Phalcon\Mvc\Model {

    public $id;
    public $voucher_id; //linkage
    public $old_creditledger_id; //linkage
    public $new_creditledger_id; //linkage
    public $old_debitledger_id; //linkage
    public $new_debitledger_id; //linkage
    public $old_amount;
    public $new_amount;
    public $old_date;
    public $new_date;
    public $edit_comments;
    public $created_by; //linkage
    public $created_on;
    public $modified_by; //linkage
    public $modified_on;

    public function initialize() {
        
    }

}
