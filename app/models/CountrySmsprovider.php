<?php
use Phalcon\Mvc\Model\Validator\Uniqueness;

class CountrySmsprovider extends \Phalcon\Mvc\Model
{

    public $id;
    public $country_id;
    public $domain;
    public $sender;
    public $username;
    public $password;
    public $token;
    public $api;
    public $reportapi;
    public $xmlreportapi;
    
    public function initialize()
    {
        //$this->setConnectionService('dbAdmin');
    }
}