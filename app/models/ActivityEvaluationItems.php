

<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class ActivityEvaluationItems extends \Phalcon\Mvc\Model {

    public $id;
    public $activity_master_id;
    public $activity_id;
    public $activity_type;

    public function initialize() {

        $this->belongsTo('activity_master_id', 'ActivityEvaluationMaster', 'id', array(
            'alias' => 'ActivityEvaluationMaster'
        ));
    }

}
