<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class AnnouncementComments extends \Phalcon\Mvc\Model {

    public $id;
    public $master_id;
    public $comments;
    public $comments_by;
    public $created_by;
    public $created_date;
    public $modified_by;
    public $modified_date;
    
    public function initialize() {
        
    }

}
