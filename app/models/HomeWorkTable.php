<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class HomeWorkTable extends \Phalcon\Mvc\Model {

    public $id;
    public $grp_subject_teacher_id; //linkage
    public $subjct_modules; //linkage
    public $homework; //linkage
    public $hmwrkdate;
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    public function initialize() {
        $this->belongsTo('grp_subject_teacher_id', 'GroupSubjectsTeachers', 'id', array(
            'alias' => 'GroupSubjectsTeachers',
            'reusable' => true
        ));
    }

    public function afterSave() {
       $response  = $this->getDI()->getNotificationTrigger()->homeworkNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
