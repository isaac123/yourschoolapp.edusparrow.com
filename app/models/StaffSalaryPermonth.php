<?php

class StaffSalaryPermonth extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $monthyear;
    public $staffid; //linkage
    public $paidamount;
    public $status;
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
