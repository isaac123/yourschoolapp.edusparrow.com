<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class ClassTest extends \Phalcon\Mvc\Model {

    public $class_test_id; //linkage
    public $grp_subject_teacher_id; //linkage
    public $class_test_name;
    public $subjct_modules; //linkage
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;
    public $date;

    public function initialize() {
        $this->belongsTo('grp_subject_teacher_id', 'GroupSubjectsTeachers', 'id', array(
            'alias' => 'GroupSubjectsTeachers',
            'reusable' => true
        ));
    }
     public function afterSave() {
        $response = $this->getDI()->getNotificationTrigger()->testNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
