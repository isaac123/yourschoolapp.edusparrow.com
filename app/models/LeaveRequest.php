
<?php

use Phalcon\Mvc\Model;

/**
 * ResetPasswords
 * Stores the reset password codes and their evolution
 */
class LeaveRequest extends Model {

    public $id;
    public $leaveType; //linkage
    public $fromDate;
    public $toDate;
    public $frequency;
    public $duration;
    public $status;
    public $staffId; //linkage
    public $reason;
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    public function initialize() {
        
    }

}
