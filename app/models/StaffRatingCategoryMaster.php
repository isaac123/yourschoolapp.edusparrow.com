<?php

class StaffRatingCategoryMaster extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $category_name;
    public $category_weightage;
    public $createdby; //linkage
    public $createdon;
    public $modifiedby; //linkage
    public $modifiedon;

    public function initialize() {


        $this->hasMany('id', 'StaffRatingCategoryValues', 'rating_category', array(
            'alias' => 'StaffRatingCategoryValues',
            'foreignKey' => array(
                'message' => 'RatingCategory cannot be deleted because it has RatingLevel defined. Please delete all Rating Values for that category below and try again!.'
            )
        ));
    }

}
