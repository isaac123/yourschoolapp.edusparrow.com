<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class TaskMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $message;
    public $status;
    public $initiator_id;
    public $from_date;
    public $to_date;
    public $files;
    public $type;
    public $action;
    public $created_by;
    public $created_on;
    public $modified_by;
    public $modified_on;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
