<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class Settings extends \Phalcon\Mvc\Model {

    public $id;
    public $variableName;
    public $variableValue;
    public $variableDescription;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
