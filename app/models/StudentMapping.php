<?php

class StudentMapping extends \Phalcon\Mvc\Model {

    public $id;
    public $student_info_id; //linkage
    public $aggregate_key; //linkage
    public $status;
    public $subordinate_key; //linkage

    public function afterSave() {
        $aggreArr = explode(',', $this->aggregate_key);
        if (count($aggreArr) > 0 and in_array($this->status,array("Inclass","Admitted"))) {
            $params['username'] = StudentInfo::findFirstById($this->student_info_id)->loginid; //'saranormalstaff'; // 
            $params['aggregateid'] = $this->aggregate_key; //'saranormalstaff'; //
            //$this->getDI()->getCalendar()->changeprincipalgrants($params);
            $response = $this->getDI()->getFeeTrigger()->aggregatekeychange($this);
//            print_r($response);exit;
        }
//        $subaggreArr = explode(',', $this->subordinate_key);
//        if (count($subaggreArr) > 0) {
////            $response = $this->getDI()->getFeeTrigger()->subordinatekeychange($this);
//        }
    }

    public function initialize() {
        
    }

}
