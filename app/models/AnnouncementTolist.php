<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class AnnouncementTolist extends \Phalcon\Mvc\Model {

    public $id;
    public $to;
    public $to_name;
    public $announcement_id;
    public $status;
    public $created_by;
    public $created_date;
    public $modified_by;
    public $modified_date;

    public function initialize() {

        $this->belongsTo('announcement_id', 'Announcement', 'id', array(
            'alias' => 'Announcement'
        ));
    }

    public function afterSave() {
        $response = $this->getDI()->getNotificationTrigger()->announcementNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
