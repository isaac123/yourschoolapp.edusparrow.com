<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class StudentSubteacherRating extends \Phalcon\Mvc\Model {

    public $rating_id;
    public $student_id; //linkage
    public $rating_division_id; //linkage
    public $subject_master_id; //linkage
    public $rating_category; //linkage
    public $rating_value; //linkage
    public $subjct_modules; //linkage
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    public function initialize() {
        $this->belongsTo('subject_master_id', 'GroupSubjectsTeachers', 'id', array(
            'alias' => 'GroupSubjectsTeachers',
            'reusable' => true
        ));
    }

    public function afterSave() {
        $response = $this->getDI()->getNotificationTrigger()->subjectRatingNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
