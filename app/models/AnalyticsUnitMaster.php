

<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class AnalyticsUnitMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $subjects_division_id;
    public $unit_name;
    public $unit_description;
    public $academic_year_id;
    public $created_by;
    public $created_on;
    public $modified_by;
    public $modified_on;

    public function initialize() {
        
    }

}
