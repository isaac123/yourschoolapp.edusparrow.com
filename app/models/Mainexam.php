<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class Mainexam extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $node_id; //linkage
    public $exam_name;
    public $examCode;
    public $date;
    public $mark_record_start_date;
    public $mark_record_end_date;
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    public function initialize() {
        
    }
     public function afterSave() {
        $response = $this->getDI()->getNotificationTrigger()->examNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }
}
