<?php

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Behavior\SoftDelete,
    Phalcon\Mvc\Model\Relation;

class ClassgroupStudents extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $classroom_master_id; //linkage
    public $students_id; //linkage

    public function initialize() {
        $this->belongsTo('classroom_master_id', 'ClassroomMaster', 'id', array(
            'alias' => 'ClassroomMaster',
            'reusable' => true
        ));
    }

}
