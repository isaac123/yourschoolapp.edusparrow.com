

<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class SchoolMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $name;
    public $board_type;
    public $address;
    public $state;
    public $country;
    public $zipcode;
    public $isactive;

    public function initialize() {
    }

}
