<?php

class StudentFeeCancel extends \Phalcon\Mvc\Model {

    public $id;
    public $student_fee_id; //linkage
    public $status;
    public $cancelled_amount;
    public $cancelled_comment;
    public $created_by; //linkage
    public $created_on;
    public $modified_by; //linkage
    public $modified_on;

    /**
     * Initializer method for model.
     */
    public function initialize() {

        $this->belongsTo('student_fee_id', 'StudentFeeTable', 'id', array(
            'alias' => 'StudentFeeTable',
            'reusable' => true
        ));
    }

}
