<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class SchoolLogs extends \Phalcon\Mvc\Model {

    public $id;
    public $student_id;
    public $school_id;
    public $phone_number;
    public $is_accepted;
    public $is_transfered;
    public $created_by;
    public $created_on;
    public $modified_on;
    public $modified_by; 
   
    
    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
