<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class MeetingMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $meeting_id;
    public $meeting_name;
    public $welcome_msg;
    public $moderator_password;
    public $attendee_password;
    public $start_time;
    public $end_time;
    public $created_by;
    public $created_on;
    public $modified_by;
    public $modified_on;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
