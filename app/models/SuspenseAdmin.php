
<?php

use Phalcon\Mvc\Model;

/**
 * Suspense Admin
 * suspense voucher Close Voucher Details
 */
class SuspenseAdmin extends Model {

    public $id;
    public $susvouchid; //linkage
    public $amount;
    public $vouchid; //linkage
    public $vouchtype;

    public function initialize() {
        
    }

}
