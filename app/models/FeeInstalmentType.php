<?php

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Behavior\SoftDelete,
    Phalcon\Mvc\Model\Relation;

class FeeInstalmentType extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $fee_master_id; //linkage
    public $name;
    public $is_default;

    /**
     * Initializer method for model.
     */
    public function initialize() {



        $this->hasMany('id', 'FeeInstalments', 'fee_instalment_type_id', array(
            'alias' => 'FeeInstalments',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Fee cannot be deleted. Fee instalment activity in the system'
            )
        ));

        $this->belongsTo('fee_master_id', 'FeeMaster', 'id', array(
            'alias' => 'FeeMaster',
            'reusable' => true
        ));
    }

//    public function afterCreate() {
////        print_r($this->confirmAsign); 
////        return false;
//        $feeMas = FeeMaster::findFirstById($this->fee_master_id);
//        if ($feeMas->is_mandatory == 1 && $this->is_default == 1 && $this->confirmAsign==1) {
//            $response = $this->getDI()->getFeeTrigger()->addfeewithins($this);
//            $res = json_decode($response);
//            if ($res->type == 'success') {
//                return true;
//            } else {
//                return false;
//            }
//        } else {
//            return true;
//        }
//    }
}
