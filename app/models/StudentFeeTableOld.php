<?php

/**
 *  Dummy
 *  
 */
class StudentFeeTableOld extends \Phalcon\Mvc\Model {

    public $sid;
    public $application_no;
    public $academic_year_id;
    public $general_id;
    public $feeid;
    public $fee_type;
    public $paidamount;
    public $returnedamount;
    public $status;
    public $created_by;
    public $created_date;
    public $modified_by;
    public $modified_date;
    public $cancel_comment;
    public $studentid;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
