<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class MaterialMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $material_no;
    public $material_name;
    public $material_desc;
    public $material_type_id;
    public $status;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}


