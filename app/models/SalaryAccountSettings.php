<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class SalaryAccountSettings extends \Phalcon\Mvc\Model {

    public $id;
    public $paytype_id;  //linkage
    public $credit;  //linkage
    public $debit;  //linkage
    public $formula;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
