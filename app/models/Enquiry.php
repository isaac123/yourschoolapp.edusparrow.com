<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class Enquiry extends \Phalcon\Mvc\Model {

    public $id;
    public $name;
    public $parentname;
    public $purpose;
    public $email;
    public $phone;
    public $enqdate;
    public $towhom;
    public $intime;
    public $outtime;
    public $calltime;
    public $multiplenode; //linkage
    public $files;
    public $enqfrom;
    public $enqto;
    public $particluars;
    public $officeaddr;
    public $depatcheddate;
    public $replydate;
    public $enquiry_id; //linkage
    public $followupdate;
    public $status;
    public $studentname; //linkage

    /**
     * Initializer method for model.
     */

    public function initialize() {
        
    }

}
