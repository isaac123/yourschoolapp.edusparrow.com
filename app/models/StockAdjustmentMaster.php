<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class StockAdjustmentMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $status;
    public $created_on;
    public $cycle_by;
    public $modified_on;
    public $modified_by;
   
    public function initialize() {
        
    }
}
