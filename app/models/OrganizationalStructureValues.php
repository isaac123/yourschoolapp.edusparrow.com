<?php

use Phalcon\Mvc\Model\Message;

class OrganizationalStructureValues extends \Phalcon\Mvc\Model {

    public $id;
    public $org_master_id;  //linkage
    public $parent_id;  //linkage
    public $name;
    public $status;
    public $sequence;
    public $nodefont;
    public $nodecolor;
    public $orgname;
    public $rangefrom;
    public $rangeto;

    public function initialize() {

        $this->belongsTo('org_master_id', 'OrganizationalStructureMaster', 'id', array(
            'alias' => 'OrganizationalStructureMaster'
        ));
    }



    public function beforeDelete() {
        $stuapp = Application::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(aggregate_key, "-", "," ) )');
        $assigmnets = AssignmentsMaster::findFirst("subjct_modules = $this->id ");
        $tests = ClassTest::findFirst("subjct_modules = $this->id ");
        $classroom = ClassroomMaster::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(aggregated_nodes_id, "-", "," ) )');
        $dailyledsum = DailyLedgerSum::findFirstByLid($this->id);
        $enquiry = Enquiry::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(multiplenode, "-", "," ) )');
        $enquiryset = EnquirySettings::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(enquiry_node_id, "-", "," ) )');
        $feeMas = FeeMaster::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(node_id, "-", "," ) ) or find_in_set( "' . $this->id . '" ,REPLACE(fee_node, "-", "," ) )');
        $examcontrib = FormulaTable::findFirst('subject_id = ' . $this->id);
        $subjClsrmMas = GroupSubjectsTeachers::findFirst('subject_id = ' . $this->id);
        $homework = HomeWorkTable::findFirst('subjct_modules = ' . $this->id);
        $initialBal = InitialBalance::findFirstByLid($this->id);
        $voucher = LedgerVoucher::findFirst("creditledger = $this->id or debitledger = $this->id ");
        $mainexam = Mainexam::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(node_id, "-", "," ) )');
        $exammarks = MainexamMarks::findFirst('subject_id = ' . $this->id);
        $monthlyledsum = MonthlyLedgerSum::findFirstByLid($this->id);
        $periodmas = PeriodMaster::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(node_id, "-", "," ) )');
        $ratingDiv = RatingDivision::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(node_id, "-", "," ) )');
        $rectypset = RecordTypesSettings::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(debit_ledger_ids, "-", "," ) )'
                        . ' or find_in_set( "' . $this->id . '" ,REPLACE(credit_ledger_ids, "-", "," ) )');
        $salAccSet = SalaryAccountSettings::findFirst('debit = ' . $this->id
                        . ' or credit = ' . $this->id);
        $staffinfo = StaffInfo::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(aggregate_key, "-", "," ) )');
        $studenthis = StudentHistory::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(aggregate_key, "-", "," ) )');
        $studentMap = StudentMapping::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(aggregate_key, "-", "," ) )'
                        . ' or find_in_set( "' . $this->id . '" ,REPLACE(subordinate_key, "-", "," ) ) ');
        $stureliev = StudentRelievingMaster::findFirst('find_in_set( "' . $this->id . '" ,REPLACE(joining_aggregate_key, "-", "," ) )'
                        . ' or find_in_set( "' . $this->id . '" ,REPLACE(relieving_aggregate_key, "-", "," ) ) ');
        $stuSubjRating = StudentSubteacherRating::findFirst('subjct_modules = ' . $this->id);
        $transaction = Transaction::findFirst('debit = ' . $this->id
                        . ' or credit = ' . $this->id);
        $voucheredithis = VoucherEditHistory::findFirst('old_creditledger_id = ' . $this->id
                        . ' or new_creditledger_id = ' . $this->id
                        . ' or old_debitledger_id = ' . $this->id
                        . ' or new_debitledger_id = ' . $this->id);


        if ($stuapp || $studenthis || $studentMap || $stureliev ||
                $staffinfo || $salAccSet ||
                $classroom || $subjClsrmMas || $periodmas ||
                $assigmnets || $tests || $examcontrib || $homework || $mainexam || $exammarks || $ratingDiv || $stuSubjRating ||
                $dailyledsum || $initialBal || $voucher || $monthlyledsum || $rectypset || $transaction || $voucheredithis ||
                $enquiry || $enquiryset ||
                $feeMas
        ) {
            $message = new Message("Node has related entries in this system and can't be deleted!", "type", "mytpe");
            $this->appendMessage($message);
            return false;
        }
        return true;
    }

    /* public function afterCreate() {
      $subordinate = OrganizationalStructureMaster::findFirstById($this->org_master_id);
      if($subordinate->is_subordinate != 1){
      //        $check = ControllerBase::get_current_academic_year();
      $params['username'] = $this->id; //'saranormalstaff'; //
      $params['password'] = $this->name;   //'saranormalstaff'; //
      $params['fullname'] = $this->name; //'saranormalstaff'; //
      $params['email_address'] = $this->name . '@' . SUBDOMAIN . '.edusparrow.com'; //'saracalendaradmin@sdf.com'; //
      $responseParam = $this->getDI()->getCalendar()->addresource($params);
      //        print_($responseParam);
      //        return false;
      }
      } */
}
