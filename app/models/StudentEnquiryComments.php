<?php

use Phalcon\Mvc\Model;

/**
 *  currently not in use
 *  
 */
class StudentEnquiryComments extends Model {

    public $id;
    public $enquiry_id; //linkage
    public $status_id;
    public $comments;
    public $created_by;
    public $created_date;
    public $modified_by;
    public $modified_date;

    /**
     * Initializer method for model.
     */
    public function initialize() {

        //$this->setConnectionService('dbAdmin');
    }

}
