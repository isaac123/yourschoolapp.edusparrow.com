<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class PurchaseItems extends \Phalcon\Mvc\Model {

    public $id;
    public $purchase_master_id ;
    public $material_id;
    public $quantity;
    public $unit_id;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
