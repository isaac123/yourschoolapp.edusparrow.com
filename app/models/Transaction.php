<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Transaction extends \Phalcon\Mvc\Model {

    public $transactionid;
    public $transactiontype;
    public $debit; //linkage
    public $credit; //linkage
    public $amount;
    public $created_by; //linkage
    public $created_date;
    public $details;
    public $voucherid; //linkage
    public $voucher_date;

    /**
     * Initializer method for model.
     */
    public function initialize() {

        $this->belongsTo('voucherid', 'LedgerVoucher', 'voucher_id', array(
            'alias' => 'LedgerVoucher'
        ));

        $this->belongsTo('debit', 'OrganizationalStructureValues', 'id', array(
            'alias' => 'OrganizationalStructureValues'
        ));

        $this->belongsTo('credit', 'OrganizationalStructureValues', 'id', array(
            'alias' => 'OrganizationalStructureValues'
        ));
    }

    public function beforeSave() {
        $check = $this->transactionid > 0 ? Transaction::findFirst('transactionid = ' . $this->transactionid) : '';
        if ($check) {
            //cedit update
            $credit['old'] = array(
                'lid' => $check->credit,
                'credit' => $check->amount,
                'debit' => '0',
                'olddate' => $check->voucher_date,
            );
            //debit update
            $debit['old'] = array(
                'lid' => $check->debit,
                'debit' => $check->amount,
                'credit' => '0',
                'olddate' => $check->voucher_date,
            );
        }
//cedit update
        $credit['new'] = array(
            'lid' => $this->credit,
            'credit' => $this->amount,
            'debit' => '0',
            'newdate' => $this->voucher_date,
        );
        //debit update
        $debit['new'] = array(
            'lid' => $this->debit,
            'debit' => $this->amount,
            'credit' => '0',
            'newdate' => $this->voucher_date,
        );
//print_r($credit);
//print_r($debit);exit;
        if ($debit && $credit) {
            $cresponse = json_decode($this->getDI()->getVoucherTrigger()->transactionSummaryUpdate($credit));
            $dresponse = json_decode($this->getDI()->getVoucherTrigger()->transactionSummaryUpdate($debit));
            if ($cresponse->type == 'error' || $dresponse->type == 'error') {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public function beforeDelete() {
        $check = $this->transactionid > 0 ? Transaction::findFirst('transactionid = ' . $this->transactionid) : '';
        //cedit update
        $credit['old'] = array(
            'lid' => $check->credit,
            'credit' => $check->amount,
            'debit' => '0',
            'olddate' => $check->voucher_date,
        );
        //debit update
        $debit['old'] = array(
            'lid' => $check->debit,
            'debit' => $check->amount,
            'credit' => '0',
            'olddate' => $check->voucher_date,
        );
//print_r($credit);exit;
        if ($debit && $credit) {
            $cresponse = json_decode($this->getDI()->getVoucherTrigger()->transactionSummaryUpdate($credit));
            $dresponse = json_decode($this->getDI()->getVoucherTrigger()->transactionSummaryUpdate($debit));
            if ($cresponse->type == 'error' || $dresponse->type == 'error') {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

}
