
<?php

use Phalcon\Mvc\Model;

/**
 * ResetPasswords
 * Stores the reset password codes and their evolution
 */
class ApprovalItem extends Model {

    public $id;
    public $approval_master_id; //linkage
    public $forwaded_to; //linkage
    public $status;
    public $created_date;
    public $created_by; //linkage
    public $comments;

    public function initialize() {
        $this->belongsTo('approval_master_id', 'ApprovalMaster', 'id', array(
            'alias' => 'ApprovalMaster',
            'reusable' => true
        ));
    }

}
