<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class StockAdjustmentItems extends \Phalcon\Mvc\Model {

    public $id;
    public $adjustment_id;
    public $material_id;
    public $quantity;
    public $unit_id;
    public $extra_quantity;
    public $status;
    public function initialize() {
        
    }
}
