<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class StudentInfo extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $Admission_no;
    public $application_no; //linkage
    public $loginid;
    public $parent_loginid;
    public $rollno;
    public $Student_Name;
    public $Gender;
    public $Date_of_Birth;
    public $Date_of_Joining;
    public $Address1;
    public $Address2;
    public $State;
    public $Country;
    public $Pin;
    public $Phone;
    public $Email;
    public $place_of_birth;
    public $nationality;
    public $religion;
    public $community;
    public $caste_category;
    public $caste;
    public $first_language;
    public $photo;
    public $family_photo;
    public $other_photos;
    public $father_name;
    public $f_occupation;
    public $f_designation;
    public $f_phone_no;
    public $f_phone_no_status;
    public $f_education;
    public $mother_name;
    public $m_occupation;
    public $m_designation;
    public $m_phone_no;
    public $m_phone_no_status;
    public $m_education;
    public $other_guardian_name;
    public $g_occupation;
    public $g_designation;
    public $g_phone;
    public $g_phone_no_status;
    public $family_income;
    public $person_school_fee;
    public $circumtances;
    public $sibling;
    public $blood_group;
    public $height;
    public $weight;
    public $allergic;
    public $chronic;
    public $family_doctor;
    public $previous_school_name;
    public $previous_school_state;
    public $previous_school_country;
    public $attended_from;
    public $attended_to;
    public $achievements;
    public $comments;
    public $other_details;
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;
    public $transport;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

//    public function afterSave() {
//        if ($this->loginid) {
//            $params['username'] = $this->loginid;
//            $params['password'] = date('d/m/Y', $this->Date_of_Birth);
//            $params['fullname'] = $this->Staff_Name;
//            $params['email_address'] = $this->Email;
//            $params['rolenames'] = 'Staff';
//            $params['aggregateid'] = $this->aggregate_key;
//            $this->getDI()->getCalendar()->createuserprincipals($params);
//        }
//    }
}
