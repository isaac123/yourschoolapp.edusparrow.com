<?php

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Behavior\SoftDelete,
    Phalcon\Mvc\Model\Relation;

class ClassroomMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $name;
    public $aggregated_nodes_id; //linkage

    public function initialize() {
        $this->hasMany('id', 'GroupSubjectsTeachers', 'classroom_master_id', array(
            'alias' => 'GroupSubjectsTeachers',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Subject Teachers cannot be deleted because he/she has activity in the system'
            )
        ));
        $this->hasMany('id', 'GroupClassTeachers', 'classroom_master_id', array(
            'alias' => 'GroupClassTeachers',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
                'message' => ' Class Teachers cannot be deleted because he/she has activity in the system'
            )
        ));
    }

    /*public function afterCreate() {
        $params['username'] = 'Classroom_' . $this->id; //'saranormalstaff'; // 
        $params['password'] = $this->name;   //'saranormalstaff'; //
        $params['fullname'] = $this->name; //'saranormalstaff'; //
        $params['email_address'] = $this->name . '@' . SUBDOMAIN . '.edusparrow.com'; //'saracalendaradmin@sdf.com'; //
        $params['groupadmin'] = '2349';
        $responseParam = $this->getDI()->getCalendar()->addresource($params);
//        print_($responseParam);
        return false;
    }*/

}
