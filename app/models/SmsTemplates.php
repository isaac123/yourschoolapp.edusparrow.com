<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Behavior\SoftDelete;

class SmsTemplates extends \Phalcon\Mvc\Model {

    const DELETED = '0';
    const NOT_DELETED = '1';

    public $temp_id;  //linkage
    public $temp_content;
    public $temp_status;
    public $tags;
    public $is_active;

    public function initialize() {
        $this->addBehavior(new SoftDelete(
                array(
            'field' => 'is_active',
            'value' => SmsTemplates::DELETED
                )
        ));

        $this->hasMany('temp_id', 'SentSms', 'template_id', array(
            'alias' => 'SentSms',
            'foreignKey' => array(
                'message' => 'Template cannot be deleted as there are messages already sent'
            )
        ));
    }

}
