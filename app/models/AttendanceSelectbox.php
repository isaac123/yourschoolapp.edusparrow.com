<?php

use Phalcon\Mvc\Model;

/**
 * ResetPasswords
 * Stores the reset password codes and their evolution
 */
class AttendanceSelectbox extends Model {

    public $id; //linkage
    public $attendance_for;
    public $attendanceid;
    public $attendancename;
    public $attendancevalue;
    public $color;
    public $allowed_leave_typ;
    public $is_allowed_by_count;
    public $school_cal_formula;
    public $default;
    public $allowed_count;
    public $duration_month;
    public $punish;
    public $punish_limit;
    public $punish_group;
    public $punish_type;
    public $deduct_salary;
    public $show_in_payslip;
    public $substitute;

    public function initialize() {
        
    }

}
