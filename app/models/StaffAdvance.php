<?php

class StaffAdvance extends \Phalcon\Mvc\Model {

    public $id;  //linkage
    public $staffid;  //linkage
    public $amount;
    public $reason;
    public $status;
    public $created_by;  //linkage
    public $created_date;
    public $modified_by;  //linkage
    public $modified_date;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
