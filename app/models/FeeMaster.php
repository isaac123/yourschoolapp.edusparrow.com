<?php

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Behavior\SoftDelete,
    Phalcon\Mvc\Model\Relation;

class FeeMaster extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $node_id; //linkage
    public $fee_node; //linkage
    public $is_mandatory;
    public $is_approval_required;
    public $total_amt;
    public $paid_amt;
    public $cancelled_amt;
    public $concession_amt;

    /**
     * Initializer method for model.
     */
    public function initialize() {

        $this->hasMany('id', 'StudentFeeTable', 'fee_master_id', array(
            'alias' => 'StudentFeeTable',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Fee cannot be deleted activity in the system'
            )
        ));

        $this->hasMany('id', 'FeeInstalmentType', 'fee_master_id', array(
            'alias' => 'FeeInstalmentType',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Fee cannot be deleted activity in the system'
            )
        ));
    }

}
