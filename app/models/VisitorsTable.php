<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 *  currently not in use
 *  
 */
class VisitorsTable extends \Phalcon\Mvc\Model {

    public $id;
    public $visitor_type;
    public $visitor_name;
    public $phone;
    public $date;
    public $in_time;
    public $out_time;
    public $student_name;
    public $student_class;
    public $purpose_of_visit;
    public $whom;
    public $created_by;
    public $created_date;
    public $modified_by;
    public $modified_date;

}
