<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class EnquiryFollowups extends \Phalcon\Mvc\Model {

    public $id;
    public $enquiry_master_id; //linkage
    public $status;
    public $date;
    public $comments;
    public $created_by; //linkage
    public $created_on;
    public $modified_by; //linkage
    public $modified_on;

}
