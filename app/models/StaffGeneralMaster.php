<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class StaffGeneralMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $staff_id; //linkage
    public $photo;
    public $catg_id;
    public $nationality;
    public $address_proof1;
    public $address_proof2;
    public $address_proof3;
    public $qualification;
    public $blood_group;
    public $medical_details;
    public $spouse_name;
    public $s_occupation;
    public $s_designation;
    public $s_phoneno;
    public $parent_name;
    public $p_occupation;
    public $p_designation;
    public $p_phoneno;
    public $classes_handling;
    public $subjects_handling;
    public $extra_curricular;
    public $co_curricular;
    public $additional_qualifications;
    public $experience;
    public $organization;
    public $designation;
    public $comments;
    public $relieving_reason;
    public $relieved_by; //linkage
    public $dor;
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    /**
     * Initializer method for model.
     */
    public function initialize() {

        $this->belongsTo('staff_id', 'staff_info', 'id', array(
            'alias' => 'staff',
            'reusable' => true
        ));
    }

}
