<?php

class StudentFeeTable extends \Phalcon\Mvc\Model {

    public $id;
    public $student_id; //linkage
    public $fee_master_id; //linkage
    public $fee_instalment_typ_id; //linkage
    public $fee_instalment_id; //linkage
    public $paidamount;
    public $status;
    public $returnedamount;
    public $consession_amt;
    public $created_by; //linkage
    public $created_date;
    public $modified_by; //linkage
    public $modified_date;

    /**
     * Initializer method for model.
     */
    public function initialize() {

        $this->hasMany('id', 'StudentFeeCancel', 'student_fee_id', array(
            'alias' => 'StudentFeeCancel',
            'foreignKey' => array(
                'message' => 'Fee cannot be deleted because it has association in the system'
            )
        ));

        $this->belongsTo('fee_master_id', 'FeeMaster', 'id', array(
            'alias' => 'FeeMaster',
            'reusable' => true
        ));
    }

    public function beforeSave() {
        $response = $this->getDI()->getFeeTrigger()->feeUpdateOnChange($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
