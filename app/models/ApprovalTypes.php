
<?php

use Phalcon\Mvc\Model;

/**
 * ResetPasswords
 * Stores the reset password codes and their evolution
 */
class ApprovalTypes extends Model {

    public $id;
    public $approval_type; //linkage
    public $is_required;
    public $is_default_by_role; //linkage

    public function initialize() {
        $this->hasMany('id', 'ApprovalMaster', 'approval_type_id', array(
            'alias' => 'ApprovalMaster',
            'foreignKey' => array(
                'message' => 'Item cannot be deleted because he/she has activity in the system'
            )
        ));
    }

}
