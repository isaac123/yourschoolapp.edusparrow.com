<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Message;

class Application extends \Phalcon\Mvc\Model {

    public $id;
    public $application_no; //linkage
    public $aggregate_key; //linkage
    public $Student_Name;
    public $Gender;
    public $Date_of_Birth;
    public $status;
    public $key_in_status;
    public $Date_of_Joining;
    public $Address1;
    public $Address2;
    public $State;
    public $Country;
    public $Pin;
    public $Phone;
    public $Email;
    public $application_fee;
    public $application_fee_status;
    public $created_on;
    public $created_by; //linkage
    public $modified_on;
    public $modified_by; //linkage

    /**
     * Validate that emails are unique across users
     */

    public function validation() {
        $this->validate(new Uniqueness(array(
            "field" => "application_no",
            "message" => "The application number is already registered"
        )));

        return $this->validationHasFailed() != true;
    }

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

    public function beforeDelete() {
        $stuinfo = StudentInfo::findFirstByApplicationNo('application_no = ' . $this->application_no);
        if ($stuinfo) {
            $message = new Message("Application cannot be deleted!. Student has activities in this system", "type", "mytpe");
            $this->appendMessage($message);
            return false;
        }
        return true;
    }

}
