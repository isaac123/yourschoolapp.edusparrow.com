<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class SentSms extends \Phalcon\Mvc\Model {

    public $id;
    public $template_id;  //linkage
    public $time;
    public $number_of_recipients;
    public $number_of_failures;
    public $number_of_messages;
    public $report_file;

    public function initialize() {

        $this->belongsTo('template_id', 'SmsTemplates', 'temp_id', array(
            'alias' => 'SmsTemplates'
        ));

        $this->hasMany('id', 'SentSmsResults', 'sms_id', array(
            'alias' => 'SentSmsResults',
            'foreignKey' => array(
                'message' => 'SMS cannot be deleted as there are messages already sent'
            )
        ));
    }

}
