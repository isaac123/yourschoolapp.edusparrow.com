<?php

class StaffRatingCategoryValues extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $rating_category; //linkage
    public $rating_level_name;
    public $rating_level_value;
    public $createdby;
    public $createdon;
    public $modifiedby;
    public $modifiedon;

    public function initialize() {

        $this->belongsTo('rating_category', 'StaffRatingCategoryMaster', 'id', array(
            'alias' => 'StaffRatingCategoryMaster'));
    }

}
