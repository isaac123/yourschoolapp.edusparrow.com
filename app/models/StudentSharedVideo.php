<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class StudentSharedVideo extends \Phalcon\Mvc\Model {

    public $id;
    public $subject_comb_id;
    public $lesson_id;
    public $sub_lesson_id;
    public $video_id;
    public $created_by;
    public $created_on;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
