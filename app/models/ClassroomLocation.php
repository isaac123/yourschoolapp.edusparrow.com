<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class ClassroomLocation extends \Phalcon\Mvc\Model {

    public $id;
    public $classroom_master_id; //linkage
    public $location_name;
    public $created_by; //linkage
    public $created_on;
    public $modified_by; //linkage
    public $modified_on;

    public function initialize() {
        
    }

}
