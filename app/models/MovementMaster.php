<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class MovementMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $io;
    public $movement_type_id;
    public $movement_status;
    public $purchase_order_no;
    public $created_by;
    public $created_on;
    public $modified_by;
    public $modified_on;
   
    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}

