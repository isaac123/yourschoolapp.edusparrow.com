<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class TimelineReadings extends \Phalcon\Mvc\Model {

    public $id;
    public $type;
    public $grp_subject_teacher_id;
    public $title;
    public $files;
    public $date;
    public $created_by;
    public $created_on;
    public $modified_by;
    public $modified_on;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
