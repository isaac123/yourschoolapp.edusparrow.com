<?php

use Phalcon\Mvc\Model;

/**
 *  Not currently used
 *  
 */
class AttendanceValues extends Model {

    public $id;
    public $user_id;
    public $att_period_id;
    public $att_days_id;
    public $value_id;

    public function initialize() {

        $this->belongsTo('att_days_id', 'AttendanceDays', 'id', array(
            'alias' => 'AttendanceDays'
        ));
        $this->belongsTo('att_period_id', 'AttendancePeriods', 'id', array(
            'alias' => 'AttendancePeriods'
        ));
    }

    public function afterSave() {
        $response = $this->getDI()->getNotificationTrigger()->attendanceNotification($this);
        $res = json_decode($response);
        if ($res->type == 'success') {
            return true;
        } else {
            return false;
        }
    }

}
