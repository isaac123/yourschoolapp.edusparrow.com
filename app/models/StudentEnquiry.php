<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 *  currently not in use
 *  
 */
class StudentEnquiry extends \Phalcon\Mvc\Model {

    public $id;
    public $parent_name;
    public $contact_number;
    public $mail_id;
    public $candidate_name;
    public $aggregate_key;
    public $lead;
    public $status;
    public $comments;
    public $created_by; //linkage
    public $created_on;
    public $modified_by; //linkage
    public $modified_on;

    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}
