<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class Announcement extends \Phalcon\Mvc\Model {

    public $id;
    public $date;
    public $message;
    public $type;
    public $comments;
    public $files;
    public $status;
    public $selected_userlist;
    public $created_by;  //linkage
    public $created_date;
    public $modified_by;  //linkage
    public $modified_date;

    /**
     * Initializer method for model.
     */
    public function initialize() {

        $this->hasMany('id', 'AnnouncementTolist', 'announcement_id', array(
            'alias' => 'AnnouncementTolist',
            'foreignKey' => array(
                'message' => 'Announcement cannot be deleted'
            )
        ));
    }


}
