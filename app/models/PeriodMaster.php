<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class PeriodMaster extends \Phalcon\Mvc\Model {

    public $id;
    public $node_id;  //linkage
    public $user_type;
    public $period;  //linkage
    public $start_time;
    public $end_time;

    public function initialize() {
        
    }

}
