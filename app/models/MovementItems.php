<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class MovementItems extends \Phalcon\Mvc\Model {

    public $id;
    public $movement_master_id;  //linkage
    public $material_id;  //linkage
    public $quantity;
    public $unit_id;
    public $batch;
   
    /**
     * Initializer method for model.
     */
    public function initialize() {
        
    }

}

