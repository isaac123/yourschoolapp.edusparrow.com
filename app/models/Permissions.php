<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class Permissions extends \Phalcon\Mvc\Model {

    public $permission_id;
    public $permission_for;
    public $permission;
    public $created_by;  //linkage
    public $created_date;
    public $modified_by;  //linkage
    public $modified_date;

    public function initialize() {
        
    }

}
