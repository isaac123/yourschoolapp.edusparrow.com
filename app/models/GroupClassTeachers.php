<?php

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Behavior\SoftDelete,
    Phalcon\Mvc\Model\Relation;

class GroupClassTeachers extends \Phalcon\Mvc\Model {

    public $id; //linkage
    public $classroom_master_id; //linkage
    public $teachers_id; //linkage

    public function initialize() {
        $this->belongsTo('classroom_master_id', 'ClassroomMaster', 'id', array(
            'alias' => 'ClassroomMaster',
            'reusable' => true
        ));
        $this->hasMany('id', 'StudentClassteacherRating', 'class_master_id', array(
            'alias' => 'StudentClassteacherRating',
            'foreignKey' => array(
                  'action' => Relation::ACTION_CASCADE,
                'message' => 'Class Teachers cannot be deleted because he/she has activity in the system'
            )
        ));
    }

}
