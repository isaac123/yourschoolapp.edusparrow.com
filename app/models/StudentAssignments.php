<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

class StudentAssignments extends \Phalcon\Mvc\Model {

    public $id;
    public $student_id; //linkage
    public $assignment_id; //linkage
    public $upload_file_name;
    public $created_by; //linkage
    public $created_on;
    public $modified_by; //linkage
    public $modified_on;

    public function initialize() {


        $this->belongsTo('assignment_id', 'AssignmentsMaster', 'id', array(
            'alias' => 'AssignmentsMaster'
        ));

        $this->belongsTo('student_id', 'StudentInfo', 'Serial_no', array(
            'alias' => 'StudentInfo'
        ));
    }

}
