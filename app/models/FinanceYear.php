<?php

use Phalcon\Mvc\Model\Validator\Uniqueness;

/**
 *  Not currently used
 *  
 */
class FinanceYear extends \Phalcon\Mvc\Model {

    public $id;
    public $start_date;
    public $end_date;
    public $status;
    public $created_by;
    public $created_on;
    public $modified_by;
    public $modified_on;

    public function initialize() {
    }

}
