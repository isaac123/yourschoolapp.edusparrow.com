
<?php

use Phalcon\Mvc\Model;

/**
 * ResetPasswords
 * Stores the reset password codes and their evolution
 */
class ApprovalMaster extends Model
{
    public $id;
    public $approval_type_id; //linkage
    public $Item_id; //linkage
    public $requested_by; //linkage
    public $requested_date;
    public $approval_status;
    public $Approved_by;
    public $Approval_date;
    public $modified_by;
    public $modified_date;
    
    public function initialize()
    {
        $this->hasMany('id', 'ApprovalItem', 'approval_master_id', array(
            'alias' => 'ApprovalItem',
            'foreignKey' => array(
                'message' => 'Item cannot be deleted because he/she has activity in the system'
            )
        ));
         $this->belongsTo('approval_type_id', 'ApprovalTypes', 'id', array(
            'alias' => 'ApprovalTypes',
            'reusable' => true
        ));
    }
}
