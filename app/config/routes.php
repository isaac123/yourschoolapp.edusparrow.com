<?php

$router = new Phalcon\Mvc\Router();


$router->add('/confirm/{code}/subscription', array(
    'controller' => 'index',
    'action' => 'signup'
));

$router->add('/reset-password/{code}/{email}', array(
    'controller' => 'users',
    'action' => 'resetPassword'
));
$router->add(
        "/myclass/{controller}/{action}/{masterid}", array(
    "controller" => 1,
    "action" => 2,
    "masterid" => 3
        )
);
/* $router->add(
  "/chart/{controller}/{action}/{student}/{divValId}/{subDivValId}/{academicYrId}", array(
  "controller" => 1,
  "action" => 2,
  "student" => 3,
  "divValId" => 4,
  "subDivValId" => 5,
  "academicYrId" => 6,
  )
  ); */
return $router;
