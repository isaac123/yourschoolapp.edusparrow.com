<?php

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class VoucherEditTrigger extends Component {

    public function editVoucher($voucher) {
        //Suspense link check and status change parent suspense voucher
        $susadmin = SuspenseAdmin::findFirst('vouchid=' . $voucher->voucher_id);
        if ($susadmin) {
            $susadmin->amount = $voucher->amount;
            if ($susadmin->save()) {
                $suspvoucherdet = LedgerVoucher::findFirst('voucher_id=' . $susadmin->susvouchid);
                $susquery = 'SELECT SUM(led.amount) as totreceipamt , SUM(p.amount) as totpayamt '
                        . ' FROM SuspenseAdmin susadmin'
                        . ' LEFT JOIN  LedgerVoucher led ON led.voucher_id = susadmin.vouchid and led.vouchertype IN("RECEIPT","JOURNAL") '
                        . ' LEFT JOIN  LedgerVoucher p ON p.voucher_id = susadmin.vouchid and led.vouchertype IN("PAYMENT") '
                        . ' WHERE ( led.ledgerstatus = "Closed"  or  p.ledgerstatus = "Closed" ) '
                        . ' and susadmin.susvouchid =  ' . $susadmin->susvouchid
                        . ' GROUP BY susadmin.susvouchid ';
                $childsuspenamt = $this->modelsManager->executeQuery($susquery);
                if (count($childsuspenamt) > 0) {
                    $pending_amt = $suspvoucherdet->amount - $childsuspenamt[0]->totreceipamt + $childsuspenamt[0]->totpayamt;
                    $suspvoucherdet->ledgerstatus = $pending_amt != 0 ? 'Paid' : 'Closed';
                    if (!$suspvoucherdet->save()) {
                        foreach ($suspvoucherdet->getMessages() as $messages) {
                            $error .= $messages . "\n";
                        }
                        $message['type'] = 'error';
                        $message['message'] = $error;
                        $responseParam = (json_encode($message));
                    }
                }
            }
        }
        $message['type'] = 'success';
        $message['message'] = ' Voucher Details updated Successfully';
        $responseParam = (json_encode($message));
        return $responseParam;
    }

    public function editFeeVoucher($ledger_det) {
        $old_ledger_det = LedgerVoucher::findFirstByVoucherId($ledger_det->voucher_id);
        $studentfee = StudentFeeTable::findFirstById($ledger_det->record_type_item_id);
        $feeinstal = FeeInstalments::findFirstById($studentfee->fee_instalment_id);
        $studentfee->paidamount = ($studentfee->paidamount - $old_ledger_det->amount) + $ledger_det->amount;
        if ($feeinstal->fee_amount > $studentfee->paidamount && $studentfee->paidamount > 0) {
            $studentfee->status = 'Partialypaid';
        }
        if ($feeinstal->fee_amount > $studentfee->paidamount && $studentfee->paidamount == 0) {
            $studentfee->status = 'Unpaid';
        }
        if ($feeinstal->fee_amount == $studentfee->paidamount) {
            $studentfee->status = 'Paid';
        }
        if ($studentfee->save()) {
            $feemas = FeeMaster::findFirst('id=' . $studentfee->fee_master_id);
            $feemas->paid_amt = ($feemas->paid_amt - $old_ledger_det->amount) + $ledger_det->amount;
            if (!$feemas->save()) {
                foreach ($feemas->getMessages() as $messages) {
                    $error .= $messages;
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                $responseParam = (json_encode($message));
            } else {

                $message['type'] = 'success';
                $message['message'] = ' Voucher Details updated Successfully';
                $responseParam = (json_encode($message));
            }
        } else {
            foreach ($studentfee->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            $responseParam = (json_encode($message));
        }
        return $responseParam;
    }

    public function editApplicationVoucher($ledger_det) {
        $appln = Application::findFirstById($ledger_det->record_type_item_id);
        $appln->application_fee = $ledger_det->amount;
        if (!$appln->save()) {
            foreach ($appln->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            $responseParam = (json_encode($message));
        } else {

            $message['type'] = 'success';
            $message['message'] = ' Voucher Details updated Successfully';
            $responseParam = (json_encode($message));
        }

        return $responseParam;
    }

}
