<?php

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View;

class Calendar extends Component {

    /**
     * creates calendar for resources
     *
     * @param array $params
     */
    public function addresource($params) {
        $data = array(
            'username' => $params['username'],
            'password' => $params['password'],
            'fullname' => $params['fullname'],
            'email_address' => $params['email_address'],
            'groupadmin' => $params['groupadmin']
        );
        $data_string = json_encode($data);
        $responseParam = IndexController::curlIt(CALENDARAPI . 'createResourcePricipal', $data_string);
//        print_r($responseParam);exit;
        return $responseParam;
    }
    /**
     * modifies calendar access
     *
     * @param array $params
     */
    public function changeprincipalgrants($params) {
        $data = array(
            'username' => $params['username'],
            'aggregateid' => $params['aggregateid']
        );
        $data_string = json_encode($data);
        $responseParam = IndexController::curlIt(CALENDARAPI . 'changePrincipalGrants', $data_string);
        return $responseParam;
    }
    /**
     * modifies calendar access
     *
     * @param array $params
     */
    public function createuserprincipals($params) {
        $data_string = json_encode($params);
        $responseParam = IndexController::curlIt(CALENDARAPI . 'createUserPricipal', $data_string);
        return $responseParam;
    }

}
