<?php

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class VoucherTrigger extends Component {

    /**
     * creates calendar for resources
     *
     * @param array $params
     */
    public function transactionSummaryUpdate($params) {
        $message = array();
        $error = '';
        if (isset($params['old'])) {
            $motnhlySum = MonthlyLedgerSum::findFirst('monthyear LIKE "' . date('m-Y', $params['old']['olddate']) . '"'
                            . ' and lid = ' . $params['old']['lid']);

            if ($motnhlySum && $motnhlySum->id > 0) {
                $motnhlySum->lid = $params['old']['lid'];
                $motnhlySum->creditsum = $motnhlySum->creditsum - $params['old']['credit'];
                $motnhlySum->debitsum = $motnhlySum->debitsum - $params['old']['debit'];
                $motnhlySum->monthyear = date('m-Y', $params['old']['olddate']);

                if (!$motnhlySum->save()) {
                    foreach ($motnhlySum->getMessages() as $messages) {
                        $error .= $messages . '<br>';
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
            $dailySum = DailyLedgerSum::findFirst('date LIKE "' . date('d-m-Y', $params['old']['olddate']) . '"'
                            . ' and lid = ' . $params['old']['lid']);
            if ($dailySum && $dailySum->id > 0) {
                $dailySum->lid = $params['old']['lid'];
                $dailySum->creditsum = $dailySum->creditsum - $params['old']['credit'];
                $dailySum->debitsum = $dailySum->debitsum - $params['old']['debit'];
                $dailySum->date = date('d-m-Y', $params['old']['olddate']);
                if (!$dailySum->save()) {
                    foreach ($dailySum->getMessages() as $messages) {
                        $error .= $messages . '<br>';
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
        if (isset($params['new'])) {

            $motnhlySumn = MonthlyLedgerSum::findFirst('monthyear LIKE "' . date('m-Y', $params['new']['newdate']) . '"'
                            . ' and lid = ' . $params['new']['lid']) ?
                    MonthlyLedgerSum::findFirst('monthyear LIKE "' . date('m-Y', $params['new']['newdate']) . '"'
                            . ' and lid = ' . $params['new']['lid']) : new MonthlyLedgerSum();

            $motnhlySumn->lid = $params['new']['lid'];
            $motnhlySumn->creditsum = $motnhlySumn->creditsum + $params['new']['credit'];
            $motnhlySumn->debitsum = $motnhlySumn->debitsum + $params['new']['debit'];
            $motnhlySumn->monthyear = date('m-Y', $params['new']['newdate']);
            if (!$motnhlySumn->save()) {
                foreach ($motnhlySumn->getMessages() as $messages) {
                    $error .= $messages . '<br>';
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
            $dailySumn = DailyLedgerSum::findFirst('date LIKE "' . date('d-m-Y', $params['new']['newdate']) . '"'
                            . ' and lid = ' . $params['new']['lid']) ?
                    DailyLedgerSum::findFirst('date LIKE "' . date('d-m-Y', $params['new']['newdate']) . '"'
                            . ' and lid = ' . $params['new']['lid']) :
                    new DailyLedgerSum();

            $dailySumn->lid = $params['new']['lid'];
            $dailySumn->creditsum = $dailySumn->creditsum + $params['new']['credit'];
            $dailySumn->debitsum = $dailySumn->debitsum + $params['new']['debit'];
            $dailySumn->date = date('d-m-Y', $params['new']['newdate']);
            if (!$dailySumn->save()) {
                foreach ($dailySumn->getMessages() as $messages) {
                    $error .= $messages . '<br>';
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
        }

        if ($error == '') {
            $message['type'] = 'success';
            $responseParam = (json_encode($message));
            return $responseParam;
        }
    }

}
