<?php

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class MarksTrigger extends Component {

    /**
     * creates calendar for resources
     *
     * @param array $params
     */
    public function addMainExamMark($params) {
//        print_r($params); echo $params->node_id; exit;
        $transactionManager = new TransactionManager();
        $transactionService = $transactionManager->setDbService('db');
        $transaction = $transactionService->get();
        $mainexRec = MainexamMarks::find('mainexam_id = ' . $params->mainexam_id .
                        ' and subject_id = ' . $params->subject_id);
        $student_id_arr = array();
        foreach ($mainexRec as $mainEx) {
            $student_id_arr[] = $mainEx->student_id;
            try {
            $studentId = $mainEx->student_id; //student Id
            $examTwoId = $params->mainexam_id ; // linked test Id
            $combiId = $params->subject_id; //combination master Id  
            $ObtainedMarks = $params->marks; //entered   
            $obtainedOutOf = $params->outof; //entered   
           
                        $recursiveParam = array(
                            'stuId' => $studentId,
                            'x2' => $examTwoId,
                            'subject_id' => $combiId
                        );

                        MarksTrigger::recursiveRecalculateMarks($recursiveParam);
            return 1;
        } catch (Exception $ex) {

            return $ex;
        }
        }
    }
    
    /**
     * creates calendar for resources
     *
     * @param array $params
     */
    public function recursiveRecalculateMarks($params) {
               try {
//            $initType = $params['type']; //class test or mainexam
            $studentId = $params['stuId']; //student Id
            $examTwoId = $params['x2']; // linked test Id
            $combiId = $params['subject_id'] ? $params['subject_id'] : ''; //subject_id  
            $ObtainedMarks = $params['obtainedMarks']; //entered   
            $obtainedOutOf = $params['obtainedOutOf']; //entered   
            $examTwo = Mainexam::findFirstById($examTwoId); // linked test
            $examTwoMark = MainexamMarks::findFirst('mainexam_id = ' . $examTwoId .
                            ' and student_id = ' . $studentId .
                            ' and subject_id = ' . $combiId) ?
                    MainexamMarks::findFirst('mainexam_id = ' . $examTwoId .
                            ' and student_id = ' . $studentId .
                            ' and subject_id = ' . $combiId) :
                    new MainexamMarks();


            $mark = $ObtainedMarks ? $ObtainedMarks : ($examTwoMark->marks ? $examTwoMark->marks : 0);
            $calcOutOf = $obtainedOutOf ? $obtainedOutOf : ($examTwoMark->outof ? $examTwoMark->outof : 0);
//            echo $mark.'/'.$calcOutOf.'<br>';
            if ((floatval($calcOutOf) < floatval($mark))) {
                $message['type'] = 'error';
                $message['message'] = 'Student Mark exceeds total marks';
                print_r(json_encode($message));
                exit;
            }

            //linked main ex updation start
            $getAllRefMains = Mainexam::find('mainexam_id = ' . $examTwoId);
//            $getAllRefTst = ClassTest::find('mainexam_id = ' . $examTwoId .
//                            ' and subject_master_id = ' . $combiId);
//            $getAllRefAssigns = AssignmentsMaster::find('mainexam_id = ' . $examTwoId .
//                            ' and subject_master_id = ' . $combiId);
            $linkedmainIheritedTotal = 0;
            $linkedmainIheritedTOutOf = 0;
            $linkedmainrefOutOf = 0;
            foreach ($getAllRefMains as $mainrefex) {
                $refmainmarks = MainexamMarks::findFirst('mainexam_id = ' . $mainrefex->id .
                                ' and student_id = ' . $studentId .
                                ' and subject_id = ' . $combiId) ?
                        MainexamMarks::findFirst('mainexam_id = ' . $mainrefex->id .
                                ' and student_id = ' . $studentId .
                                ' and subject_id = ' . $combiId) :
                        new MainexamMarks();

                if ($refmainmarks->mainexam_marks_id > 0) {
                    $linkedmainIheritedTotal += (($refmainmarks->marks ? $refmainmarks->marks : 0) +
                            ($refmainmarks->inherited_marks ? $refmainmarks->inherited_marks : 0));
                    $linkedmainIheritedTOutOf += (($refmainmarks->outof ? $refmainmarks->outof : 0) +
                            ($refmainmarks->inherited_outof ? $refmainmarks->inherited_outof : 0));
                    $linkedmainrefOutOf += $mainrefex->mainexam_outof;
                }
            }
//            foreach ($getAllRefTst as $reftest) {
////                echo 'class_test_id = ' . $reftest->class_test_id .
////                                ' and student_id = ' . $studentId;
//                $reftestmarks = ClassTestMarks::findFirst('class_test_id = ' . $reftest->class_test_id .
//                                ' and student_id = ' . $studentId) ?
//                        ClassTestMarks::findFirst('class_test_id = ' . $reftest->class_test_id .
//                                ' and student_id = ' . $studentId) :
//                        new ClassTestMarks();
//                if ($reftestmarks->classtest_marks_id > 0) {
//                    $linkedmainIheritedTotal += ($reftestmarks->marks ? $reftestmarks->marks : 0);
//                    $linkedmainIheritedTOutOf += ($reftestmarks->outof ? $reftestmarks->outof : 0);
//                    $linkedmainrefOutOf += $reftest->mainexam_outof;
//                }
//            }
//
//
//
//            foreach ($getAllRefAssigns as $refassign) {
//                $refassmarks = AssignmentMarks::findFirst('assignment_id = ' . $refassign->id .
//                                ' and student_id = ' . $studentId) ?
//                        AssignmentMarks::findFirst('assignment_id = ' . $refassign->id .
//                                ' and student_id = ' . $studentId) :
//                        new AssignmentMarks();
//                if ($refassmarks->id > 0) {
//                    $linkedmainIheritedTotal += ($refassmarks->marks ? $refassmarks->marks : 0);
//                    $linkedmainIheritedTOutOf += ($refassmarks->outof ? $refassmarks->outof : 0);
//                    $linkedmainrefOutOf += $refassign->mainexam_outof;
//                }
//            }
//                echo 'assignment_id = ' . $refassign->id .
//                                ' and student_id = ' . $studentId;exit;

            $inhertMarkCalc = (($linkedmainIheritedTotal / $linkedmainIheritedTOutOf) * $linkedmainrefOutOf);
//            print_r($linkedmainIheritedTotal . ' / ' . $linkedmainIheritedTOutOf . ' * ' . $linkedmainrefOutOf . '<br>');
//            echo 'hai';
//            exit;
//            
//            print_r($mark.'/'.$calcOutOf);exit;

            if (($inhertMarkCalc > 0 || $examTwoMark->marks > 0 || $mark > 0 || $calcOutOf > 0 || $examTwoMark->inherited_marks > 0 || $examTwoMark->outof > 0)) {
                $examTwoMark->inherited_marks = $inhertMarkCalc ? $inhertMarkCalc : 0;
                $examTwoMark->mainexam_id = $examTwoId;
                $examTwoMark->student_id = $studentId;
                $examTwoMark->marks = $mark;
                $examTwoMark->inherited_outof = $linkedmainrefOutOf ? $linkedmainrefOutOf : 0;
                $examTwoMark->outof = round($calcOutOf, 2);
                $examTwoMark->combinationId = $combiId;

//                print_r($examTwoMark->student_id.' : '.$examTwoMark->marks);
//                 print_r($examTwoMark->outof );
                if (!$examTwoMark->save()) {
//                print_r($examTwoMark->getMessages());exit;
                    return $examTwoMark->getMessages();
                } else {
//                    print_r($examTwoMark->outof );
                    if ($examTwo->mainexam_id) {

                        $recursiveParam = array(
                            'stuId' => $studentId,
                            'x2' => $examTwo->mainexam_id,
                            'subject_id' => $combiId
                        );

                        MarksTrigger::recursiveRecalculateMarks($recursiveParam);
                    }
                }
            }

            return 1;
        } catch (Exception $ex) {

            return $ex;
        }
    }

}
