<?php

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class FeeTrigger extends Component {

    /**
     * creates calendar for resources
     *
     * @param array $params
     */
    public function addfee($params) {
//        print_r($params); echo $params->node_id; exit;
        $transactionManager = new TransactionManager();
        $transactionService = $transactionManager->setDbService('db');
        $transaction = $transactionService->get();
        $feeAggregate = explode('-', $params->node_id);
        $stuMapp = StudentMapping::find('status IN ("Inclass","Admitted")');
        $stc = 1;
        $c = 0;
        $actualfee = $params->fee_amount;
        /*         * Check filter * */
        $rules = FeeRules::findFirst("fee_name LIKE '$params->fee_name' ");
        if ($rules) {
            $ruleamount = $rules->value;
            $extrafee = $rules->is_percent == 1 ? ($actualfee * ($ruleamount / 100)) : $ruleamount;
            $actualfee += $extrafee;
        }
        if (count($stuMapp) > 0):
            foreach ($stuMapp as $cstudent) {
                $stuHis = StudentHistory::find('student_info_id = ' . $cstudent->student_info_id);
                ##assign new student fee
                $actualfee = (count($stuHis) > 1) ? $params->fee_amount : $actualfee;
                $is_fee_exits_student = 0;
                $subaggrediff = $aggrediff = array(1);
                ###Matching Aggregate key
                $stuaggregate = explode(',', $cstudent->aggregate_key);
                $aggrediff = array_diff($feeAggregate, $stuaggregate);
//                print_r($feeAggregate); print_r($stuaggregate); print_r($aggrediff);
                ###Matching Subordinate key
                $stusubaggregate = explode(',', $cstudent->subordinate_key);
                if (count($stusubaggregate) > 0):
//                    $feesubor = array_slice($feeAggregate, 2);
                    $subaggrediff = array_diff($aggrediff, $stusubaggregate);
                endif;
//                 print_r($aggrediff);       print_r($subaggrediff);
//                exit;
                if (count($aggrediff) == 0 || count($subaggrediff) == 0) {
                    $is_fee_exits_student = StudentFeeTable::findFirst('fee_master_id=' . $params->id . ' and student_id =' . $cstudent->student_info_id);
//echo  $is_fee_exits_student->id.'<br>';
                    if (!$is_fee_exits_student) {
//$c.= $params->node_id.'=>'.$cstudent->student_info_id.',';
                        $stc++;
                        $student_fee = new StudentFeeTable();
                        $student_fee->student_id = $cstudent->student_info_id;
                        $student_fee->fee_master_id = $params->id;
                        $student_fee->paidamount = '0.00'; //$params->fee_amount;
                        $student_fee->status = 'Unpaid';
                        $student_fee->returnedamount = 0;
                        $student_fee->fee_name = $params->fee_name;
                        $student_fee->fee_amount = $actualfee;
                        $student_fee->penalty_amount = $params->penalty_amount;
                        $student_fee->fee_collection_start = $params->fee_collection_start;
                        $student_fee->fee_collection_end = $params->fee_collection_end;
                        $student_fee->created_by = $params->created_by;
                        $student_fee->created_date = $params->created_on;
                        $student_fee->modified_by = $params->modified_by;
                        $student_fee->modified_date = $params->modified_on;
                        $student_fee->setTransaction($transaction);
//                        print_r($student_fee); exit;
                        if (!$student_fee->save()) {
                            $stc = 0;
                            $transaction->rollback("Fee not assigned!");
                            $message['type'] = 'error';
                            $message['message'] = 'Fee not assigned!' . $student_fee->getMessage();
                            $responseParam = (json_encode($message));
                            return $responseParam;
                        }
                    }
                }
            }
//            exit;
        endif;
//echo $stc.':'.$c;exit;
        if ($stc > 0) {
            $transaction->commit();
            $message['type'] = 'success';
            $responseParam = (json_encode($message));
            return $responseParam;
        }
    }

    /**
     * creates fee for student matching aggregate
     *
     * @param array $params
     */
    public function aggregatekeychange($params) {
//        $transactionManager = new TransactionManager();
//        $transactionService = $transactionManager->setDbService('db');
//        $transaction = $transactionService->get();
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $stuAggregate = explode(',', $params->aggregate_key);
        $stc = 1;
        $stuFeeSelector = $stuAggregate;
        $stuFeeSelectorQry = array();
        $stuFeeQry = array();

        while (count($stuFeeSelector) > 0) {
            $queryarr = preg_filter('/^([\d])*/', '(FIND_IN_SET("$0" , REPLACE( node_id, "-", "," ) ) >0)', $stuFeeSelector);
            $stuFeeSelectorQry[] = implode(' and ', $queryarr);
            array_shift($stuFeeSelector);
        }
        $stuFeeQry[] = '(' . implode(' OR ', $stuFeeSelectorQry) . ')';
        $stuFeeQry[] = ' is_mandatory  = 1';
//        print_r(implode(' AND ', $stuFeeQry));exit;
//        $currentQury = preg_filter('/^([\d])*/', '(FIND_IN_SET("$0" , REPLACE( node_id, "-", "," ) ) >0)', $stuAggregate);
        $fetchAllFee = FeeMaster::find(implode(' AND ', $stuFeeQry));
        foreach ($fetchAllFee as $feetobeassigned) {
            $is_fee_exits_student = StudentFeeTable::findFirst(' fee_master_id =' . $feetobeassigned->id
                            . ' and student_id =' . $params->student_info_id);
            if (!$is_fee_exits_student) {

                $stdentInfo = StudentInfo::findFirstById($params->student_info_id);
                $studentYear1 = date('y', $stdentInfo->Date_of_Joining);
                $studentYear2 = date('Y', $stdentInfo->Date_of_Joining);
                $acyr = '2015-16'; //current year
                $yrsplit = explode('-', $acyr);
                if (in_array($studentYear1, $yrsplit) || in_array($studentYear2, $yrsplit)) {
                    $stutype = 'new';
                } else {
                    $stutype = 'old';
                }
                $feenstyp = FeeInstalmentType::find('fee_master_id=' . $feetobeassigned->id . ' and is_default = 1');
                foreach ($feenstyp as $feeoldnew) {
                    $insname = $feeoldnew->name;
//                    echo $stutype.$insname;exit;.
                    if ($insname == 'New' && $stutype == 'new') {
                        $this->_insertFeeForStudent($params, $feetobeassigned, $feeoldnew, $uid, $stc);
                    } else if ($insname == 'Old' && $stutype == 'old') {
                        $this->_insertFeeForStudent($params, $feetobeassigned, $feeoldnew, $uid, $stc);
                    } else if ($insname == 'Single') {
                        $this->_insertFeeForStudent($params, $feetobeassigned, $feeoldnew, $uid, $stc);
                    }
                }
            }
        }
        if ($stc > 0) {
            $message['type'] = 'success';
            $responseParam = (json_encode($message));
            return $responseParam;
        }
    }

    /**
     * creates
     *
     * @param array $params
     */
    public function _insertFeeForStudent($params, $feetobeassigned, $feenstyp, $uid, $stc) {

        $feens = FeeInstalments::find('fee_instalment_type_id=' . $feenstyp->id);
        foreach ($feens as $feeass) {

            $stc++;
            $student_fee = new StudentFeeTable();
            $student_fee->student_id = $params->student_info_id;
            $student_fee->fee_master_id = $feetobeassigned->id;
            $student_fee->fee_instalment_id = $feeass->id;
            $student_fee->fee_instalment_typ_id = $feenstyp->id;
            $student_fee->paidamount = '0.00'; //$params->fee_amount;
            $student_fee->status = 'Unpaid';
            $student_fee->returnedamount = 0;
            $student_fee->created_by = $uid;
            $student_fee->created_date = time();
            $student_fee->modified_by = $uid;
            $student_fee->modified_date = time();
//                    $student_fee->setTransaction($transaction);
//                        print_r($student_fee); exit;
            if (!$student_fee->save()) {
                $stc = 0;
//                        $transaction->rollback("Fee not assigned!");
                $message['type'] = 'error';
                $message['message'] = 'Fee not assigned!' . $student_fee->getMessage();
                $responseParam = (json_encode($message));
                return $responseParam;
            }
        }
        return $stc;
    }

    /**
     * creates fee for student matching subordinate
     *
     * @param array $params
     */
    public function subordinatekeychange($params) {
//        print_r($params);  exit;
        $transactionManager = new TransactionManager();
        $transactionService = $transactionManager->setDbService('db');
        $transaction = $transactionService->get();
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $stuAggregate = explode(',', $params->subordinate_key);
        $mainAggregate = explode(',', $params->aggregate_key);
        $stuHis = StudentHistory::find('student_info_id = ' . $params->student_info_id);
        $queryCon = array();
        $stc = 1;
        //
        foreach ($stuAggregate as $stuag) {
            $queryCon[] = " FIND_IN_SET(  '$stuag', REPLACE(  node_id ,  '-',  ',' ) ) >0";
        }
//        $fetchAllFee = FeeMaster::find(array(
//                    'columns' => "id,node_id,fee_name,fee_amount,penalty_amount,fee_collection_start,"
//                    . " fee_collection_end , SUBSTRING_INDEX( node_id,  '-', -2 ) as sub_node_id",
//                    'having' => implode(' or ', $queryCon)
//        ));
        $fetchAllFee = FeeMaster::find(array(
                    'columns' => "id,node_id,fee_name,fee_amount,penalty_amount,fee_collection_start,"
                    . " fee_collection_end ",
                    'having' => implode(' or ', $queryCon)
        ));
//        print_r($fetchAllFee) ;exit;
//         print_r(count($fetchAllFee) ); 
        /*
         * select 
          FROM `fee_master`
          having FIND_IN_SET('3',REPLACE(`node_id`, '-', ',')) >0
          and FIND_IN_SET('6',REPLACE(`node_id`, '-', ','))>0
          and FIND_IN_SET('14',REPLACE(`node_id`, '-', ','))>0 */
        if (count($fetchAllFee) > 0):
            foreach ($fetchAllFee as $feetobeassigned) {
//                echo $feetobeassigned->id;exit; 
                $feeAggregate = explode('-', $feetobeassigned->node_id);
                $actualSubkey = array_diff($feeAggregate, $mainAggregate);
                $aggrediff = array_diff($actualSubkey, $stuAggregate);
//                print_r($feeAggregate);  print_r($aggrediff);  print_r($stuAggregate); 
                if (count($aggrediff) == 0) {

                    $actualfee = $feetobeassigned->fee_amount;
                    /*                     * Check filter * */
                    $rules = FeeRules::findFirst("fee_name LIKE '$feetobeassigned->fee_name' ");
                    if ($rules) {
                        $ruleamount = $rules->value;
                        $extrafee = $rules->is_percent == 1 ? ($actualfee * ($ruleamount / 100)) : $ruleamount;
                        $actualfee += $extrafee;
                    }
                    ##assign new student fee
                    $actualfee = (count($stuHis) > 1) ? $params->fee_amount : $actualfee;
//            print_r('fee_master_id = ' . $feetobeassigned->id . ' and  student_id= ' . $params->student_info_id) ;exit; 
                    $is_fee_exits_student = StudentFeeTable::findFirst('fee_master_id = ' . $feetobeassigned->id . ' and  student_id= ' . $params->student_info_id);
//                    echo $is_fee_exits_student;
                    if (!$is_fee_exits_student) {
                        $stc++;
                        $student_fee = new StudentFeeTable();
                        $student_fee->student_id = $params->student_info_id;
                        $student_fee->fee_master_id = $feetobeassigned->id;
                        $student_fee->paidamount = '0.00'; //$feetobeassigned->fee_amount;
                        $student_fee->status = 'Unpaid';
                        $student_fee->returnedamount = 0;
                        $student_fee->fee_name = $feetobeassigned->fee_name;
                        $student_fee->fee_amount = $actualfee;
                        $student_fee->penalty_amount = $feetobeassigned->penalty_amount;
                        $student_fee->fee_collection_start = $feetobeassigned->fee_collection_start;
                        $student_fee->fee_collection_end = $feetobeassigned->fee_collection_end;
                        $student_fee->created_by = $uid;
                        $student_fee->created_date = time();
                        $student_fee->modified_by = $uid;
                        $student_fee->modified_date = time();
                        $student_fee->setTransaction($transaction);
//                        print_r($student_fee);  exit;
                        if (!$student_fee->save()) {
                            $transaction->rollback("Fee not assigned!");
                            $message['type'] = 'error';
                            $message['message'] = 'Fee not assigned!' . $student_fee->getMessage();
                            $responseParam = (json_encode($message));
                            return $responseParam;
                        }
                    }
                }
            }
        endif;
        if ($stc > 0) {
            $transaction->commit();
            $message['type'] = 'success';
            $responseParam = (json_encode($message));
            return $responseParam;
        }
    }

    /**
     * creates calendar for resources
     *
     * @param array $params
     */
    public function addfeewithins($params) {
//       echo $params->fee_master_id; print_r($params->transaction);  
//        $transactionManager = new TransactionManager();
//        print_r($transactionManager->get());exit;
//        $transactionService = $transactionManager->setDbService('db');
//        print_r($transactionService);exit;
//        $transaction1 = $transactionService->get();
//        print_r($transaction1);exit;

        $feeInsTyp = FeeInstalmentType::findFirstById($params->fee_instalment_type_id);
        $feeMas = FeeMaster::findFirstById($feeInsTyp->fee_master_id);
//        print_r($feeMas);exit;
        $feeAggregate = ControllerBase::buildStudentQuery($feeMas->node_id); //explode('-', $feeMas->node_id);;
//        $feeNameAggregate = explode('-', $feeMas->fee_node);
        $stuMapp = StudentMapping::find('status IN ("Inclass","Admitted") and (' . implode(' OR ', $feeAggregate) . ')');
        $stc = 1;
        $c = 0;
        $actualfee = $params->fee_amount;
        if (count($stuMapp) > 0):
            foreach ($stuMapp as $cstudent) {
                $is_fee_exits_student = StudentFeeTable::findFirst('fee_instalment_typ_id=' . $feeInsTyp->id
                                . ' and fee_master_id =' . $feeMas->id
                                . ' and fee_instalment_id =' . $params->id
                                . ' and student_id =' . $cstudent->student_info_id);
//echo  $is_fee_exits_student->id.'<br>';
                if (!$is_fee_exits_student) {
//                    $stdentInfo = StudentInfo::findFirstById($cstudent->student_info_id);
//                    $studentYear1 = date('y', $stdentInfo->Date_of_Joining);
//                    $studentYear2 = date('Y', $stdentInfo->Date_of_Joining);
//                    $acyr = '2015-16'; //current year
//                    $yrsplit = explode('-', $acyr);
//                    if (in_array($studentYear1, $yrsplit) || in_array($studentYear2, $yrsplit)) {
//                        $stutype = 'new';
//                    } else {
//                        $stutype = 'old';
//                    }
                    $stdentAdCnt = StudentHistory::find('student_info_id = ' . $cstudent->student_info_id);
                    $stutype = (count($stdentAdCnt) > 1) ? 'old' : 'new';
                    $insname = $params->name;
                    if ($insname == 'New' && $stutype == 'new') {
                        $stc++;
                        $student_fee = new StudentFeeTable();
                        $student_fee->student_id = $cstudent->student_info_id;
                        $student_fee->fee_master_id = $feeMas->id;
                        $student_fee->fee_instalment_id = $params->id;
                        $student_fee->fee_instalment_typ_id = $feeInsTyp->id;
                        $student_fee->paidamount = '0.00'; //$params->fee_amount;
                        $student_fee->status = 'Unpaid';
                        $student_fee->returnedamount = 0;
                        $student_fee->created_by = $params->created_by;
                        $student_fee->created_date = $params->created_on;
                        $student_fee->modified_by = $params->modified_by;
                        $student_fee->modified_date = $params->modified_on;
//                    $student_fee->setTransaction($transaction);
//                        print_r($student_fee); exit;
                        if (!$student_fee->save()) {
                            $stc = 0;
//                        $transaction->rollback("Fee not assigned!");
                            $message['type'] = 'error';
                            $message['message'] = 'Fee not assigned!' . $student_fee->getMessage();
                            $responseParam = (json_encode($message));
                            return $responseParam;
                        }
                    } else if ($insname == 'Old' && $stutype == 'old') {
                        $stc++;
                        $student_fee = new StudentFeeTable();
                        $student_fee->student_id = $cstudent->student_info_id;
                        $student_fee->fee_master_id = $feeMas->id;
                        $student_fee->fee_instalment_id = $params->id;
                        $student_fee->fee_instalment_typ_id = $feeInsTyp->id;
                        $student_fee->paidamount = '0.00'; //$params->fee_amount;
                        $student_fee->status = 'Unpaid';
                        $student_fee->returnedamount = 0;
                        $student_fee->created_by = $params->created_by;
                        $student_fee->created_date = $params->created_on;
                        $student_fee->modified_by = $params->modified_by;
                        $student_fee->modified_date = $params->modified_on;
//                    $student_fee->setTransaction($transaction);
//                        print_r($student_fee); exit;
                        if (!$student_fee->save()) {
                            $stc = 0;
//                        $transaction->rollback("Fee not assigned!");
                            $message['type'] = 'error';
                            $message['message'] = 'Fee not assigned!' . $student_fee->getMessage();
                            $responseParam = (json_encode($message));
                            return $responseParam;
                        }
                    } else if ($insname == 'Single') {
                        $stc++;
                        $student_fee = new StudentFeeTable();
                        $student_fee->student_id = $cstudent->student_info_id;
                        $student_fee->fee_master_id = $feeMas->id;
                        $student_fee->fee_instalment_id = $params->id;
                        $student_fee->fee_instalment_typ_id = $feeInsTyp->id;
                        $student_fee->paidamount = '0.00'; //$params->fee_amount;
                        $student_fee->status = 'Unpaid';
                        $student_fee->returnedamount = 0;
                        $student_fee->created_by = $params->created_by;
                        $student_fee->created_date = $params->created_on;
                        $student_fee->modified_by = $params->modified_by;
                        $student_fee->modified_date = $params->modified_on;
//                    $student_fee->setTransaction($transaction);
//                        print_r($student_fee); exit;
                        if (!$student_fee->save()) {
                            $stc = 0;
//                        $transaction->rollback("Fee not assigned!");
                            $message['type'] = 'error';
                            $message['message'] = 'Fee not assigned!' . $student_fee->getMessage();
                            $responseParam = (json_encode($message));
                            return $responseParam;
                        }
                    }
//$c.= $params->node_id.'=>'.$cstudent->student_info_id.',';
                }
            }
//            exit;
        endif;
//echo $stc.':'.$c;exit;
        if ($stc > 0) {
//            $transaction->commit();
            $message['type'] = 'success';
            $responseParam = (json_encode($message));
            return $responseParam;
        }
    }

    /**
     * save feemaster
     *
     * @param array $params
     */
    public function feeUpdateOnChange($params) {
        $feeamount = 0;
        $paidamt = 0;
        $concession_amt = 0;
        $cancelled_amt = 0;
        $fee_detail = FeeInstalments::findFirst('id = ' . $params->fee_instalment_id);
        if ($params->id > 0) {
            $studntfee = StudentFeeTable::findFirst('id = ' . $params->id);
            $feeamount = $fee_detail->fee_amount;
            $paidamt = $studntfee->paidamount;
            $concession_amt = $studntfee->concession_amt;
            if ($studntfee->status == 'Cancelled')
                $cancelled_amt = $fee_detail->fee_amount - ( $studntfee->paidamount + $studntfee->concession_amt);
        }

        $get_feemas = FeeMaster::findFirstById($params->fee_master_id);
        $get_feemas->total_amt = $get_feemas->total_amt - $feeamount + $fee_detail->fee_amount;
        $get_feemas->paid_amt = $get_feemas->paid_amt - $paidamt + $params->paidamount;
        $get_feemas->concession_amt = $get_feemas->concession_amt - $concession_amt + $params->concession_amt;
        if ($params->status == 'Cancelled') {
            $cancelled_amt2 = $fee_detail->fee_amount - ( $params->paidamount + $params->concession_amt);
        }
        $get_feemas->cancelled_amt = $get_feemas->cancelled_amt - $cancelled_amt + $cancelled_amt2;

//        print_r($get_feemas);
//        exit;
        if (!$get_feemas->save()) {
            foreach ($get_feemas->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
//                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        } else {

            $message['type'] = 'success';
            $responseParam = (json_encode($message));
            return $responseParam;
        }
    }

}
