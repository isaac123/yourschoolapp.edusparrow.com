<?php

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class VoucherDeleteTrigger extends Component {

    public function deleteContraVoucher($voucherid) {
        $type_id = ApprovalTypes::findFirst("approval_type = 'Contra Voucher'")->id;
        $responseParam = $this->deleteApproveItem($type_id, $voucherid);
        return json_encode($responseParam);
    }

    public function deleteApproveItem($typeid, $voucherid) {
        $approv_mas = ApprovalMaster::findFirst('approval_type_id=' . $typeid . ' and Item_id=' . $voucherid);
        $msg = array();
        if ($approv_mas != '') {
            $masid = $approv_mas->id;
            if ($approv_mas->delete()) {
                $approv_item = ApprovalItem::find('approval_master_id=' . $masid);
                if (!$approv_item->delete()) {
                    $msg['type'] = 'error';
                }
                $msg['type'] = 'success';
            } else {
                $msg['type'] = 'success';
            }
        } else {
            $msg['type'] = 'success';
        }
        return $msg;
    }

    public function deletePaymentVoucher($voucherid) {
        $type_id = ApprovalTypes::findFirst("approval_type = 'Payment Voucher'")->id;
        $approve = $this->deleteApproveItem($type_id, $voucherid);
        if ($approve['type'] == 'success') {
            $result = $this->deleteSuspenseLink($voucherid);
            return $result;
        }
    }

    public function deleteReceiptVoucher($voucherid) {
        $result = $this->deleteSuspenseLink($voucherid);
        return $result;
    }

    public function deleteJournalVoucher($voucherid) {
        $type_id = ApprovalTypes::findFirst("approval_type = 'Journal Voucher'")->id;
        $approve = $this->deleteApproveItem($type_id, $voucherid);
        if ($approve['type'] == 'success') {
            $result = $this->deleteSuspenseLink($voucherid);
            return $result;
        }
    }

    public function deleteSuspenseVoucher($voucherid) {
        $typeid = ApprovalTypes::findFirst("approval_type = 'Suspense Voucher'")->id;
        $approve = $this->deleteApproveItem($typeid, $voucherid);
        if ($approve['type'] == 'success') {
            $response = $this->deleteSuspenseSubLink($voucherid);
        }
        return $response;
    }

    public function deleteSuspenseLink($voucherid) {
        $suspense_link = SuspenseAdmin::findFirst('vouchid=' . $voucherid);
        if ($suspense_link) {
            $vou_id = $suspense_link->susvouchid;
            if ($suspense_link->delete()) {
                $susledger = LedgerVoucher:: findFirst('voucher_id=' . $vou_id);
                $susledger->ledgerstatus = "Paid";
                if ($susledger->save()) {
                    $message['type'] = 'success';
                    $message['message'] = ' Voucher Details deleted Successfully';
                    $responseParam = (json_encode($message));
                }
            }
        } else {
            $message['type'] = 'success';
            $responseParam = (json_encode($message));
        }

        return $responseParam;
    }

    public function deleteSalaryVoucher($voucherid) {
        $i = 0;
        $ledger_det = LedgerVoucher::findFirstByVoucherId($voucherid);
        $staffid = explode(',', $ledger_det->record_type_item_id);
        foreach ($staffid as $value) {
            $staff_salary = StaffSalaryPermonth::findFirstById($value);
            $staff_salary->status = "Generated";
            if ($staff_salary->save()) {
                $adv_split = StaffAdvanceSplitup::find('month = "' . $staff_salary->monthyear . '"');
                if (count($adv_split) > 0) {
                    foreach ($adv_split as $val) {
                        $staff_advance = StaffAdvance::findFirstById($val->staff_advance_id);
                        if ($staff_advance->staffid == $staff_salary->staffid) {
                            $staff_advance->status = "Closed";
                            if ($staff_advance->save()) {
                                $val->status = "Closed";
                                if (!$val->save()) {

                                    foreach ($val->getMessages() as $messages) {
                                        $error .= $messages;
                                    }
                                    $message['type'] = 'error';
                                    $message['message'] = $error;
                                    $responseParam = (json_encode($message));
                                }
                            } else {

                                foreach ($staff_advance->getMessages() as $messages) {
                                    $error .= $messages;
                                }
                                $message['type'] = 'error';
                                $message['message'] = $error;
                                $responseParam = (json_encode($message));
                            }
                        }
                    }
                }
                $i++;
            } else {

                foreach ($staff_salary->getMessages() as $messages) {
                    $error .= $messages;
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                $responseParam = (json_encode($message));
            }
        }
        if ($i > 0) {
            $responseParam = $this->getDI()->getVoucherDeleteTrigger()->deletePaymentVoucher($voucherid);
        }
        return $responseParam;
    }

    public function deleteAdvanceVoucher($voucherid) {
        $i = 0;
        $ledger_det = LedgerVoucher::findFirstByVoucherId($voucherid);
        $adv_split = StaffAdvanceSplitup::find('staff_advance_id = ' . $ledger_det->record_type_item_id);
        if ($adv_split->delete()) {
            $staff_advance = StaffAdvance::findFirstById($ledger_det->record_type_item_id);
            if (!$staff_advance->delete()) {
                foreach ($staff_advance->getMessages() as $messages) {
                    $error .= $messages;
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            } else {
                $responseParam = $this->getDI()->getVoucherDeleteTrigger()->deletePaymentVoucher($voucherid);
            }
        } else {
            foreach ($adv_split->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            $responseParam = (json_encode($message));
        }
        return $responseParam;
    }

    public function deleteApplicationVoucher($voucherid) {
        $i = 0;
        $ledger_det = LedgerVoucher::findFirstByVoucherId($voucherid);
        $appln = Application::findFirstById($ledger_det->record_type_item_id);
        $appln->application_fee = 0;
        $appln->application_fee_status = 'Unpaid';
        if (!$appln->save()) {
            foreach ($appln->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            $responseParam = (json_encode($message));
        } else {

            $responseParam = $this->getDI()->getVoucherDeleteTrigger()->deleteReceiptVoucher($voucherid);
        }

        return $responseParam;
    }

    public function deleteFeeVoucher($voucherid) {
        $i = 0;
        $ledger_det = LedgerVoucher::findFirstByVoucherId($voucherid);
        $studentfeeid = $ledger_det->record_type_item_id;
        $studfeemas = StudentFeeTable::findFirstById($studentfeeid);
        $feeinstal = FeeInstalments::findFirstById($studfeemas->fee_instalment_id);
        $studfeemas->paidamount = $studfeemas->paidamount - $ledger_det->amount;
        if ($feeinstal->fee_amount > $studfeemas->paidamount && $studfeemas->paidamount > 0) {
            $studfeemas->status = 'Partialypaid';
        }
        if ($feeinstal->fee_amount > $studfeemas->paidamount && $studfeemas->paidamount == 0) {
            $studfeemas->status = 'Unpaid';
        }
        if ($feeinstal->fee_amount == $studfeemas->paidamount) {
            $studfeemas->status = 'Paid';
        }
        if ($studfeemas->save()) {
            $feemas = FeeMaster::findFirst('id=' . $studfeemas->fee_master_id);
            $feemas->paid_amt = $feemas->paid_amt - $ledger_det->amount;
            if ($feemas->save()) {
                $responseParam = $this->getDI()->getVoucherDeleteTrigger()->deleteReceiptVoucher($voucherid);
            } else {
                foreach ($feemas->getMessages() as $messages) {
                    $error .= $messages;
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                $responseParam = (json_encode($message));
            }
        } else {
            foreach ($studfeemas->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            $responseParam = (json_encode($message));
        }


        return $responseParam;
    }

    public function deleteSuspenseSubLink($voucherid) {
        $i = 0;
        $suspense = SuspenseAdmin::find('susvouchid = ' . $voucherid);
        if (count($suspense) > 0) {
            foreach ($suspense as $suspen) {
                $ledgervouch = LedgerVoucher::findFirst('voucher_id=' . $suspen->vouchid);
                if (!$ledgervouch->delete()) {
                    foreach ($ledgervouch->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    $responseParam = (json_encode($message));
                }
                $i++;
            }
            if ($i > 0) {
                $message['type'] = 'success';
                $message['message'] = ' Voucher Details deleted Successfully';
                $responseParam = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
            $message['message'] = ' Voucher Details deleted Successfully';
            $responseParam = (json_encode($message));
        }

        return $responseParam;
    }

}
