<?php

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class ActivityTrigger extends Component {

    /**
     * creates calendar for resources
     *
     * @param array $params
     */
    public function addActivity($params) {
//        print_r($params);
//            exit;
        $master_id = $params['master_id'];
        $available_from = $params['avlfrom'];
        $available_to = $params['avlto'];
        $activity_by = $params['activity_by'];
        $activity_id = $params['activity_id'];
        $activity_type = $params['activity_type'];
        $display_time = $params['display_time'];
        $activitydata = json_encode($params['activity_data']);
        $obj = new Cassandra();
        $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);
        if ($res) {
            //DELETE ENTRY
           /* $buildInsertquery = " Delete from activities "
                    . " where master_id = $master_id and activity_id = $activity_id and "
                    . " activity_type ='$activity_type';";
//            print_r($buildInsertquery);
//            exit;
            if ($result = $obj->query($buildInsertquery)) {
                if ($result[0]['result'] != 'success') {

                    $message['type'] = 'error';
                    $message['message'] = 'Problem in deleting';
                    $responseParamf = (json_encode($message));
                }
            }
            */
            $buildInsertquery = " INSERT INTO activities (master_id,activity_id,activity_type,display_time,available_from,available_to,activity_by,activity_data) 
                                        values ($master_id,$activity_id,'$activity_type',$display_time,$available_from,$available_to,'$activity_by','$activitydata');";
//            print_r($buildInsertquery);
//            exit;
            if ($result = $obj->query($buildInsertquery)) {
                if ($result[0]['result'] == 'success') {
                    $message['type'] = 'success';
                    $responseParamf = (json_encode($message));
                } else {

                    $message['type'] = 'error';
                    $message['message'] = 'Problem in inserting';
                    $responseParamf = (json_encode($message));
                }
            }
        }
        $obj->close();

        return $responseParamf;
    }

    public function addComment($params) {
//        print_r($params);
//            exit;
        $master_id = $params['master_id'];
        $activity_id = $params['activity_id'];
        $activity_type = $params['activity_type'];
        $activitycomm = ($params['comments']);
        $obj = new Cassandra();
        $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);
        if ($res) {
            $buildInsertquery = " select * from activities where master_id = $master_id and activity_id = $activity_id and "
                    . " activity_type ='$activity_type';";

            if ($result = $obj->query($buildInsertquery)) {
                $activitydatajson = json_decode($result[0]['activity_data']);
                $activitydatajson->comments[] = $activitycomm;
                $activitydata = json_encode($activitydatajson);
                $buildInsertquery2 = " update  activities set  activity_data = '$activitydata'"
                        . " where master_id = $master_id and activity_id = $activity_id and "
                        . " activity_type ='$activity_type';";
                if ($result1 = $obj->query($buildInsertquery2)) {
                    if ($result1[0]['result'] == 'success') {
                        $message['type'] = 'success';
                        $responseParamf = (json_encode($message));
                    } else {
                        $message['type'] = 'error';
                        $message['message'] = 'Problem in inserting';
                        $responseParamf = (json_encode($message));
                    }
                }
            }
        }
        $obj->close();

        return $responseParamf;
    }

    public function deleteActivity($params) {
//        print_r($params);
//            exit;
        $master_id = $params['master_id'];
        $activity_id = $params['activity_id'];
        $activity_type = $params['activity_type'];
        $obj = new Cassandra();
        $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);
        if ($res) {
            $buildInsertquery = " Delete from activities "
                    . " where master_id = $master_id and activity_id = $activity_id and "
                    . " activity_type ='$activity_type';";
//            print_r($buildInsertquery);
//            exit;
            if ($result = $obj->query($buildInsertquery)) {
                if ($result[0]['result'] == 'success') {
                    $message['type'] = 'success';
                    $responseParamf = (json_encode($message));
                } else {

                    $message['type'] = 'error';
                    $message['message'] = 'Problem in deleting';
                    $responseParamf = (json_encode($message));
                }
            }
        }
        $obj->close();

        return $responseParamf;
    }
     public function selectActivity($params) {
        $master_id = $params['master_id'];
        $activity_id = $params['activity_id'];
        $activity_type = $params['activity_type'];
        $obj = new Cassandra();
        $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);
        if ($res) {
            $buildInsertquery = " select * from activities "
                    . " where master_id = $master_id and activity_id = $activity_id and "
                    . " activity_type ='$activity_type';";
             $result = $obj->query($buildInsertquery);
         $responseParamf = $result[0];  
        }
        $obj->close();

        return $responseParamf;
    }

}
