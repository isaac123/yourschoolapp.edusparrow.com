<?php

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class ModuleEnabled extends Component {

    /**
     * returns the module enabled
     *
     * @param array $params
     */
    public function getAllModulesActivated() {


        $packageId = Settings::findFirstByVariableName('package_id')->variableValue;
        $data = array('package' => $packageId);
        $data_string = json_encode($data);
        $responseParam = json_decode(IndexController::curlIt(SALESAPI . 'package/getPackageModulesByPackageId', $data_string));
        $modules = (array) $responseParam->modules;
        return $modules;
    }

}
