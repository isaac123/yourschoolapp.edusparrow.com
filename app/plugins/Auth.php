<?php

use Phalcon\Mvc\User\Component;

/**
 * edu\Auth\Auth
 * Manages Authentication/Identity Management in Vokuro
 */
class Auth extends Component {

    /**
     * Checks the user credentials
     *
     * @param array $credentials
     * @return boolan
     */
    public function check($response) {
        //echo '<pre>';print_r($response['response']);
        $credentials = json_decode($response['response']);
         // print_r($credentials);;exit;
        if (!$credentials->status) {
            throw new Exception('Internal server error');
        }
        if ($credentials->status == 'ERROR') {
            throw new Exception($credentials->messages);
        }
//        echo base64_decode( $credentials->data->authenticateKey);exit;
        $roleNames = (array) $credentials->data->role_name;
        if (!in_array('Siteadmin', $roleNames)&& !in_array('Management', $roleNames)) {
            // IP RESTRICTION START
            $iprestriction = RoleRestrictByIp::find('role IN ("' . implode('","', $roleNames) . '")');
            $rempverole = array();
            $cliipAddress = $this->request->getClientAddress();
            if (count($iprestriction) > 0) {
                foreach ($iprestriction as $ipdet) {
                    if (!in_array($cliipAddress, explode(',', $ipdet->ip))) {
                        $rempverole[] = $ipdet->role;
                    }
                }
            }
            $arrdiff = array_diff($roleNames, $rempverole);
//         echo'<pre>';print_r($arrdiff);
//        exit;
            // IP RESTRICTION END
        }else{
            $arrdiff = $roleNames;
        }
        $this->session->set('auth-identity', array(
            'id' => $credentials->data->id,
            'name' => $credentials->data->name,
            'email' => $credentials->data->email,
            'role_name' => $arrdiff,
            'authenticateKey' => $credentials->data->authenticateKey
        ));
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param Vokuro\Models\Users $user
     */
    public function saveSuccessLogin($user) {
        $successLogin = new SuccessLogins();
        $successLogin->usersId = $user->id;
        $successLogin->ipAddress = $this->request->getClientAddress();
        $successLogin->userAgent = $this->request->getUserAgent();
        if (!$successLogin->save()) {
            $messages = $successLogin->getMessages();
            throw new Exception($messages[0]);
        }
    }

    /**
     * Implements login throttling
     * Reduces the efectiveness of brute force attacks
     *
     * @param int $userId
     */
    public function registerUserThrottling($userId) {
        $failedLogin = new FailedLogins();
        $failedLogin->usersId = $userId;
        $failedLogin->ipAddress = $this->request->getClientAddress();
        $failedLogin->attempted = time();
        $failedLogin->save();

        $attempts = FailedLogins::count(array(
                    'ipAddress = ?0 AND attempted >= ?1',
                    'bind' => array(
                        $this->request->getClientAddress(),
                        time() - 3600 * 6
                    )
        ));

        switch ($attempts) {
            case 1:
            case 2:
                // no delay
                break;
            case 3:
            case 4:
                sleep(2);
                break;
            default:
                sleep(4);
                break;
        }
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param Vokuro\Models\Users $user
     */
    public function createRememberEnviroment(Users $user) {
        $userAgent = $this->request->getUserAgent();
        $token = md5($user->email . $user->password . $userAgent);

        $remember = new RememberTokens();
        $remember->usersId = $user->id;
        $remember->token = $token;
        $remember->userAgent = $userAgent;

        if ($remember->save() != false) {
            $expire = time() + 86400 * 8;
            $this->cookies->set('RMU', $user->id, $expire);
            $this->cookies->set('RMT', $token, $expire);
        }
    }

    /**
     * Check if the session has a remember me cookie
     *
     * @return boolean
     */
    public function hasRememberMe() {
        return $this->cookies->has('RMU');
    }

    /**
     * Logs on using the information in the coookies
     *
     * @return Phalcon\Http\Response
     */
    public function loginWithRememberMe() {
        $userId = $this->cookies->get('RMU')->getValue();
        $cookieToken = $this->cookies->get('RMT')->getValue();

        $user = Users::findFirstById($userId);
        if ($user) {

            $userAgent = $this->request->getUserAgent();
            $token = md5($user->email . $user->password . $userAgent);

            if ($cookieToken == $token) {

                $remember = RememberTokens::findFirst(array(
                            'usersId = ?0 AND token = ?1',
                            'bind' => array(
                                $user->id,
                                $token
                            )
                ));
                if ($remember) {

                    // Check if the cookie has not expired
                    if ((time() - (86400 * 8)) < $remember->createdAt) {

                        // Check if the user was flagged
                        $this->checkUserFlags($user);

                        // Register identity
                        $this->session->set('auth-identity', array(
                            'id' => $user->id,
                            'name' => $user->login,
                            'role_name' => $user->profile->name
                        ));

                        // Register the successful login
                        $this->saveSuccessLogin($user);

                        return $this->response->redirect('users');
                    }
                }
            }
        }

        $this->cookies->get('RMU')->delete();
        $this->cookies->get('RMT')->delete();

        return $this->response->redirect('session/login');
    }

    /**
     * Checks if the user is banned/inactive/suspended
     *
     * @param Vokuro\Models\Users $user
     */
    public function checkUserFlags(Users $user) {
        if ($user->active != 'Y') {
            throw new Exception('The user is inactive');
        }
    }

    /**
     * Returns the current identity
     *
     * @return array
     */
    public function getIdentity() {
        $identity = $this->session->get('auth-identity');
        if (!$identity) {
            return array('role_name' => array('Guests'));
        } else {
            if (!in_array('Siteadmin', $identity['role_name'])&& !in_array('Management', $roleNames)) {
                // IP RESTRICTION START
                $iprestriction = RoleRestrictByIp::find('role IN ("' . implode('","', $identity['role_name']) . '")');
                $rempverole = array();
                $cliipAddress = $this->request->getClientAddress();
                if (count($iprestriction) > 0) {
                    foreach ($iprestriction as $ipdet) {
                        if (!in_array($cliipAddress, explode(',', $ipdet->ip))) {
                            $rempverole[] = $ipdet->role;
                        }
                    }
                }
                $arrdiff = array_diff($identity['role_name'], $rempverole);
                $identity['role_name'] = $arrdiff;
                $this->session->set('auth-identity', $identity);
//
                // IP RESTRICTION END
            }
            return $this->session->get('auth-identity');
        }
    }

    /**
     * Returns the current identity
     *
     * @return string
     */
    public function getName() {
        $identity = $this->session->get('auth-identity');
        return $identity['name'];
    }

    /**
     * Removes the user identity information from session
     */
    public function remove() {
//        if ($this->cookies->has('RMU')) {
//            $this->cookies->get('RMU')->delete();
//        }
//        if ($this->cookies->has('RMT')) {
//            $this->cookies->get('RMT')->delete();
//        }

        $this->session->remove('auth-identity');
        $this->session->remove('settingPage');
    }

    /**
     * Auths the user by his/her id
     *
     * @param int $id
     */
    public function authUserById($id) {
        $user = Users::findFirstById($id);
        if ($user == false) {
            throw new Exception('The user does not exist');
        }

        $this->checkUserFlags($user);
        $userRoles = UserRoles::find("userId= '" . $user->id . "'");
        $roleNames = array();
        foreach ($userRoles as $userRole) {
            $roleNames[] = $userRole->role_name;
        }
        $this->session->set('auth-identity', array(
            'id' => $user->id,
            'name' => $user->login,
            'email' => $user->email,
            'role_name' => $roleNames
        ));
    }

    /**
     * Get the entity related to user in the active identity
     *
     * @return \Vokuro\Models\Users
     */
    public function getUser() {
        $identity = $this->session->get('auth-identity');
        if (isset($identity['id'])) {

            $user = Users::findFirstById($identity['id']);
            if ($user == false) {
                throw new Exception('The user does not exist');
            }

            return $user;
        }

        return false;
    }

}
