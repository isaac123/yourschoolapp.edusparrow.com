<?php

use Phalcon\Mvc\User\Component,
    Phalcon\Mvc\View;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class NotificationTrigger extends Component {

    /**
     * creates calendar for resources
     *
     * @param array $params
     */
    public function homeworkNotification($params) {
        $identity = $this->auth->getIdentity();
        $grpSubjTeach = GroupSubjectsTeachers::findfirst('id = ' . $params->grp_subject_teacher_id);
        $classmaster = ClassroomMaster::findFirstById($grpSubjTeach->classroom_master_id);
        $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
        $stuquery = 'SELECT stuinfo.loginid,stumap.id,stumap.student_info_id,stuinfo.Student_Name,stumap.aggregate_key,'
                . 'stumap.subordinate_key,stumap.status'
                . ' FROM StudentMapping stumap LEFT JOIN'
                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" and (' . implode(' or ', $res)
                . ') ORDER BY stuinfo.Admission_no ASC';
        $students = $this->modelsManager->executeQuery($stuquery);
        $orgvaldet = OrganizationalStructureValues::findFirstById($params->subjct_modules);
        $aggregate_keys = HomeWorkController::_getMandNodesForExam($orgvaldet);
        $aggregate_keys = array_reverse($aggregate_keys);
        $message_content = implode('-', $aggregate_keys) . ' - ' . date('d-m-Y', $params->hmwrkdate);
        $ids = array();
        foreach ($students as $stud) {
            $ids[] = $stud->loginid;
        }
        $users = array(
            'tolist' => '"' . implode('","', $ids) . '"'
        );

        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
        $user_idresp = json_decode($user_id_resp);
        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }

        if (count($devicekeyArr) > 0) {
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['activity_id'] = $params->id;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $message_content; //short text to  notify
            $notify['date'] = date('Y-m-d H:i:s');
            $notify['location'] = 'NA';
            $notify['title'] = 'Homework';
            $notify['category'] = 'Mine';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
            $notiresp = json_decode($responseParam);
            if ($notiresp->status_code == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
        }

        return $responseParamf;
    }

    /**
     * creates calendar for resources
     *
     * @param array $params
     */
    public function attendanceNotification($params) {
        $identity = $this->auth->getIdentity();
        $attenValueExceeds = AttendanceSelectbox::findFirstById($params->value_id)->attendancename;
        $attPeriod = PeriodMaster::findFirstById($params->att_period_id)->period;
        $attDate = $params->att_date;
        $message_content = 'Your child is ' . $attenValueExceeds . ' on ' . date('d-m-Y', $attDate) . ' - Period ' . $attPeriod;
        $ids = $params->user_id;
        $users = array(
            'tolist' => implode(',', $ids)
        );
        $devicekeyArr = array();
        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
        $user_idresp = json_decode($user_id_resp);
        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }
        if (count($devicekeyArr) > 0) {
            $notify = array();
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['activity_id'] = $params->att_act_id;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $message_content; //short text to  notify
            $notify['date'] = date('Y-m-d');
            $notify['location'] = 'NA';
            $notify['title'] = 'Attendance';
            $notify['category'] = 'Mine';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
            $notiresp = json_decode($responseParam);
            if ($notiresp->status_code == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {	
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
            $responseParamf = (json_encode($message));
        }

        return $responseParamf;
    }

    public function announcementNotification($params) {
        $identity = $this->auth->getIdentity();
        $announcement = Announcement::findFirstById($params->announcement_id);
        $loginids = $params->to;
	if(SUBDOMAIN != "common"){
        $users = array(
            'tolist' => '"' . $loginids . '"'
        );
        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
       // print_r($user_id_resp);exit;
        $user_idresp = json_decode($user_id_resp);
        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }
	}else{ 
                    $userarr['id'] = SUBDOMAIN;
                    $userarr['uuid'] = SUBDOMAIN;
                    $userarr['device_token'] = SUBDOMAIN;
                    $userarr['device_type'] = SUBDOMAIN;
                    $userarr['aggregate_key'] = SUBDOMAIN;
                    $devicekeyArr[] = $userarr;
	}
        if (count($devicekeyArr) > 0) {
            $notify = array();
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['activity_id'] = $params->id;
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $announcement->message; //short text to  notify
            $notify['date'] = date('Y-m-d H:i:s', time());
            $notify['location'] = 'NA'; //read,unread,cmplted
            $nodetype = explode('_', $params->to_name);
            $notify['title'] = $nodetype[0];
            if ($nodetype[0] == 'Intimation') {
                $notify['category'] = 'Mine';
            } elseif ($nodetype[0] == 'Circular') {
                $notify['category'] = 'Myschool';
            } elseif ($nodetype[0] == 'Announcement') {
                $notify['category'] = 'Myclass';
            }
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
            $notiresp = json_decode($responseParam);
            if ($notiresp->status_code == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
        }
        return $responseParamf;
    }

    public function classtestNotification($params) {
        $identity = $this->auth->getIdentity();
        $classtest = ClassTest::findFirst('class_test_id=' . $params->class_test_id);
        $orgvaldet = OrganizationalStructureValues::findFirstById($classtest->subjct_modules);
        $aggregate_keys = HomeWorkController::_getMandNodesForExam($orgvaldet);
        $aggregate_keys = array_reverse($aggregate_keys);
        $message_content = $classtest->class_test_name . ' test - ' . implode('-', $aggregate_keys) . ' : ';
        $message_content .= $params->marks ? $params->marks . '/' : '';
        $message_content .= $params->outof ? $params->outof : '';
        $loginid = StudentInfo::findFirstById($params->student_id)->loginid;
        $users = array(
            'tolist' => '"' . $loginid . '"'
        );
        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
        $user_idresp = json_decode($user_id_resp);
        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }
        if (count($devicekeyArr) > 0) {
            $notify = array();
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['activity_id'] = $params->classtest_marks_id;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $message_content; //short text to  notify
            $notify['title'] = 'tests'; //read,unread,cmplted
            $notify['date'] = date('Y-m-d H:i:s', time());
            $notify['location'] = 'NA';
            $notify['category'] = 'Mine';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
            $notiresp = json_decode($responseParam);
            if ($notiresp->status_code == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
        }
        return $responseParamf;
    }

    public function mainexamNotification($params) {
        $identity = $this->auth->getIdentity();
        $exam = Mainexam::findFirstById($params->mainexam_id);
        $orgvaldet = OrganizationalStructureValues::findFirstById($params->subject_id);
        $aggregate_keys = HomeWorkController::_getMandNodesForExam($orgvaldet);
        $aggregate_keys = array_reverse($aggregate_keys);
//        $message_content = $exam->exam_name . ' - ' . implode('-', $aggregate_keys) . ' - ';
        $loginid = StudentInfo::findFirstById($params->student_id)->loginid;
        $message_content = $exam->exam_name . ' marks  - ' . implode('-', $aggregate_keys) . ' - ';
        $message_content .= $params->marks ? $params->marks . '/' : '';
        $message_content .= $params->outof ? $params->outof : '';
        $users = array(
            'tolist' => '"' . $loginid . '"'
        );
        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
        $user_idresp = json_decode($user_id_resp);

        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }
        if (count($devicekeyArr) > 0) {
            $notify = array();
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['activity_id'] = $params->mainexam_marks_id;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $message_content; //short text to  notify
            $notify['date'] = date('Y-m-d H:i:s', time());
            $notify['location'] = 'NA';
            $notify['title'] = 'Exams';
            $notify['category'] = 'Mine';
            $notification_data = json_encode($notify);
//            print_r($notification_data);exit;
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
//            print_r($responseParam);exit;
            $notiresp = json_decode($responseParam);
            if ($notiresp->status_code == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
        }
        return $responseParamf;
    }

    public function overallAssignmentNotification($params) {
        $identity = $this->auth->getIdentity();
        $grpSubjTeach = GroupSubjectsTeachers::findfirst('id = ' . $params->grp_subject_teacher_id);
        $classmaster = ClassroomMaster::findFirstById($grpSubjTeach->classroom_master_id);
        $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $grpSubjTeach->classroom_master_id);
        $stuquery = 'SELECT stumap.id,stumap.student_info_id,stuinfo.loginid'
                . ' FROM StudentMapping stumap LEFT JOIN'
                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                . ' and stuinfo.id IN (' . $classroomstu->students_id . ') ';
        $students = $this->modelsManager->executeQuery($stuquery);
        $orgvaldet = OrganizationalStructureValues::findFirstById($params->subjct_modules);
        $aggregate_keys = HomeWorkController::_getMandNodesForExam($orgvaldet);
        $aggregate_keys = array_reverse($aggregate_keys);
//        $message_content = implode('-', $aggregate_keys) . ' - ';
//        $message_content .= date('d-m-Y', $params->submission_date) . ' - ';
//        $message_content .= $params->topic . ' - ' . $params->desc;



        $message_content = $params->topic . ' - ' . implode('-', $aggregate_keys);
        $ids = array();
        foreach ($students as $stud) {
            $ids[] = $stud->loginid;
        }
        $users = array(
            'tolist' => '"' . implode('","', $ids) . '"'
        );
        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
        $user_idresp = json_decode($user_id_resp);
        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }
        if (count($devicekeyArr) > 0) {
            $notify = array();
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['activity_id'] = $params->id;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $message_content; //short text to  notify
            $notify['date'] = date('Y-m-d H:i:s', time());
            $notify['location'] = 'NA';
            $notify['title'] = 'Assignment';
            $notify['category'] = 'Mine';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
            $notiresp = json_decode($responseParam);
            if ($notiresp->status_code == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
        }
        return $responseParamf;
    }

    public function assignmentMarksNotification($params) {
        $identity = $this->auth->getIdentity();
        $assignment = AssignmentsMaster::findFirstById($params->assignment_id);
        $orgvaldet = OrganizationalStructureValues::findFirstById($assignment->subjct_modules);
        $aggregate_keys = HomeWorkController::_getMandNodesForExam($orgvaldet);
        $aggregate_keys = array_reverse($aggregate_keys);
        $message_content = $assignment->topic . ' marks - ' . implode('-', $aggregate_keys) . ' - ';
        $message_content .= $params->marks ? $params->marks . '/' : '';
        $message_content .= $params->outof ? $params->outof : '';
        $loginid = StudentInfo::findFirstById($params->student_id)->loginid;
        $users = array(
            'tolist' => '"' . $loginid . '"'
        );
        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
        $user_idresp = json_decode($user_id_resp);
        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }
        if (count($devicekeyArr) > 0) {
            $notify = array();
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['activity_id'] = 'marks_' . $params->id;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $message_content; //short text to  notify
            $notify['date'] = date('Y-m-d H:i:s');
            $notify['location'] = 'NA';
            $notify['title'] = 'Assignment';
            $notify['category'] = 'Mine';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
            $notiresp = json_decode($responseParam);
            if ($notiresp->status_code == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
        }
        return $responseParamf;
    }

    public function subjectRatingNotification($params) {
        $identity = $this->auth->getIdentity();
        $division = RatingDivision::findFirstById($params->rating_division_id);
        $category = RatingCategoryMaster::findFirstById($params->rating_category);
        $values = RatingCategoryValues::findFirst('id=' . $params->rating_value . ' and rating_category=' . $params->rating_category);
        $orgvaldet = OrganizationalStructureValues::findFirstById($params->subjct_modules);
        $aggregate_keys = HomeWorkController::_getMandNodesForExam($orgvaldet);
        $aggregate_keys = array_reverse($aggregate_keys);
        $ratingPoints = $weightage = 0;
        $ratings = StudentSubteacherRating::find('student_id = ' . $params->student_id
                        . ' and rating_division_id =' . $params->rating_division_id
                        . ' and subject_master_id = ' . $params->subject_master_id
                        . ' and subjct_modules = ' . $params->subjct_modules);
        if (count($ratings) > 0) {
            foreach ($ratings as $rat) {
                $category = RatingCategoryValues::findFirstById($rat->rating_value);
                $ratingCategorys = RatingCategoryMaster::findFirstById($category->rating_category);
                $ratingPoints += ($category->rating_level_value / 100) * $ratingCategorys->category_weightage;
                $weightage += $ratingCategorys->category_weightage;
            }
        }
        $message_content = implode('-', $aggregate_keys) . ' Rating - ' . $division->rating_name . ' : ';
        $message_content .= $ratingPoints . '/' . $weightage;
        $loginid = StudentInfo::findFirstById($params->student_id)->loginid;
        $users = array(
            'tolist' => '"' . $loginid . '"'
        );
        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
        $user_idresp = json_decode($user_id_resp);
        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }
        if (count($devicekeyArr) > 0) {
            $notify = array();
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['activity_id'] = 'subject_' . $params->rating_division_id . '_' . $params->subjct_modules;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $message_content; //short text to  notify
            $notify['date'] = date('Y-m-d H:i:s', time());
            $notify['location'] = 'NA';
            $notify['title'] = 'Rating';
            $notify['category'] = 'Mine';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
            $notiresp = json_decode($responseParam);
            if ($notiresp->status_code == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
        }
        return $responseParamf;
    }

    public function classRatingNotification($params) {
        $identity = $this->auth->getIdentity();
        $division = RatingDivision::findFirstById($params->rating_division_id);
        $category = RatingCategoryMaster::findFirstById($params->rating_category);
        $values = RatingCategoryValues::findFirst('id=' . $params->rating_value . ' and rating_category=' . $params->rating_category);
        $ratings = StudentClassteacherRating::find('student_id = ' . $params->student_id
                        . ' and rating_division_id =' . $params->rating_division_id);
        $ratingPoints = $total = 0;
        if ($ratings) {
            foreach ($ratings as $rating) {
                $category = RatingCategoryValues::findFirstById($rating->rating_value);
                $ratingCategorys = RatingCategoryMaster::findFirstById($rating->rating_category);
                $ratingPoints += ($category->rating_level_value / 100) * $ratingCategorys->category_weightage;
                $total += $ratingCategorys->category_weightage;
            }
        }
        $message_content = 'Class Rating - ' . $division->rating_name . ' : ' . $category->category_name . ' : ';
        $message_content .= $ratingPoints . '/' . $total;
        $loginid = StudentInfo::findFirstById($params->student_id)->loginid;
        $users = array(
            'tolist' => '"' . $loginid . '"'
        );
        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
        $user_idresp = json_decode($user_id_resp);
        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }
        if (count($devicekeyArr) > 0) {
            $notify = array();
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['activity_id'] = 'class_' . $params->rating_division_id;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $message_content; //short text to  notify
            $notify['date'] = date('Y-m-d H:i:s', time());
            $notify['location'] = 'NA';
            $notify['title'] = 'Rating';
            $notify['category'] = 'Mine';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
            $notiresp = json_decode($responseParam);
            if ($notiresp->status_code == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
        }
        return $responseParamf;
    }

    public function examNotification($params) {
        $identity = $this->auth->getIdentity();
	 $narr = preg_filter('/^([\d])*/', '(find_in_set("$0",  REPLACE( aggregated_nodes_id,  "-",  ","))>0)',explode('-',$params->node_id));
	 $nqry = '(' . implode(' and ', $narr) . ')';
        $classroom = ClassroomMaster::find($nqry);
            $ids = array();
        foreach ($classroom as $class) {
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $class->id);
     
	 if($classroomstu){
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stuinfo.loginid'
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') ';
            $students = $this->modelsManager->executeQuery($stuquery);
	   	if(count($students)){
            		foreach ($students as $stud) {
                	  $ids[] = $stud->loginid;
            		}
	   	}
	  }
        }
            //print_r($ids);
            //exit;
        $message_content = $params->exam_name . '(' . $params->examCode . ')';
        $users = array(
            'tolist' => '"' . implode('","', $ids) . '"'
        );
        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
        $user_idresp = json_decode($user_id_resp);
        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }
        if (count($devicekeyArr) > 0) {
            $notify = array();
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['activity_id'] = $params->id;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $message_content; //short text to  notify
            $notify['date'] = date('Y-m-d H:i:s', time());
            $notify['location'] = 'NA';
            $notify['title'] = 'Mainexam';
            $notify['category'] = 'Myclass';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
            $notiresp = json_decode($responseParam);
            if ($notiresp->status_code == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
        }
        return $responseParamf;
    }

    public function testNotification($params) {
        $identity = $this->auth->getIdentity();
        $grpSubjTeach = GroupSubjectsTeachers::findfirst('id = ' . $params->grp_subject_teacher_id);
        $classmaster = ClassroomMaster::findFirstById($grpSubjTeach->classroom_master_id);
        $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $grpSubjTeach->classroom_master_id);
        $stuquery = 'SELECT stumap.id,stumap.student_info_id,stuinfo.loginid'
                . ' FROM StudentMapping stumap LEFT JOIN'
                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                . ' and stuinfo.id IN (' . $classroomstu->students_id . ') ';
        $students = $this->modelsManager->executeQuery($stuquery);
        $orgvaldet = OrganizationalStructureValues::findFirstById($params->subjct_modules);
        $aggregate_keys = HomeWorkController::_getMandNodesForExam($orgvaldet);
        $aggregate_keys = array_reverse($aggregate_keys);
        $message_content = $params->class_test_name . ' - ' . implode('-', $aggregate_keys) . ' - ' . date('d F Y', $params->created_date);

        $ids = array();
        foreach ($students as $stud) {
            $ids[] = $stud->loginid;
        }
        $users = array(
            'tolist' => '"' . implode('","', $ids) . '"'
        );
        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
        $user_idresp = json_decode($user_id_resp);
        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }
        if (count($devicekeyArr) > 0) {
            $notify = array();
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['activity_id'] = $params->class_test_id;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $message_content; //short text to  notify
            $notify['date'] = date('Y-m-d H:i:s', time());
            $notify['location'] = 'NA';
            $notify['title'] = 'classtest';
            $notify['category'] = 'Myclass';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
            $notiresp = json_decode($responseParam);
            if ($notiresp->status_code == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
        }
        return $responseParamf;
    }

    public function eventNotification($params) {
        $identity = $this->auth->getIdentity();
        $event = Events::findFirstById($params->event_id);
        $loginids = explode(',', $params->events_to);
        $users = array(
            'tolist' => '"' . implode('","', $loginids) . '"'
        );
        $users_data = json_encode($users);
        $user_id_resp = IndexController::curlIt(USERAUTHAPI . 'getAllDeviceIdByUser', $users_data);
        $user_idresp = json_decode($user_id_resp);

        if ($user_idresp->status == 'SUCCESS') {
            $devicekeyArr = $user_idresp->messages;
        }
        if (count($devicekeyArr) > 0) {
            $notify = array();
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['activity_id'] = $params->id;
            $notify['initiator_id'] = $identity['name']; //session user
            $notify['targetor'] = $devicekeyArr; //to list
            $notify['message'] = $event->description . ' on ' . date('d-m-Y H:i', $event->from_date); //short text to  notify
            $notify['date'] = date('Y-m-d H:i:s', time());
            $notify['location'] = 'NA';
            $nodetype = explode('_', $params->events_toname);
            $notify['title'] = $nodetype[0];
            if ($nodetype[0] == 'Events') {
                $notify['category'] = 'Mine';
            } elseif ($nodetype[0] == 'SchoolEvents') {
                $notify['category'] = 'Myschool';
            } elseif ($nodetype[0] == 'ClassEvents') {
                $notify['category'] = 'Myclass';
            }
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'Notification_Queue.php', $notification_data);
            $notiresp = json_decode($responseParam);
            if ($notiresp->status == '1') {
                $message['type'] = 'success';
                $responseParamf = (json_encode($message));
            } else {
                $message['type'] = 'error';
                $message['message'] = $notiresp->status_info;
                $responseParamf = (json_encode($message));
            }
        } else {
            $message['type'] = 'success';
        }
        return $responseParamf;
    }

}
