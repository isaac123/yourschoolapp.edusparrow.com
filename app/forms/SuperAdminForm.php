<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;

class SuperAdminForm extends Form
{

    public function initialize()
    {
        
           $userRole = new Hidden("userRole",array("value" => "SuperAdmin"));
            
      
        $this->add($userRole);        

        $name = new Text('login', array(
            'placeholder' => 'Username',
            'value' => 'superadmin',
            'readonly' =>'readonly',
            'class'=>'form-control login-frm-input helvetica'
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The name is required'
            ))
        ));

        $this->add($name);

        $email = new Text('email', array(
            'placeholder' => 'Email',
            'readonly' =>'readonly',
            'class'=>'form-control login-frm-input helvetica'
        ));

        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'The e-mail is required'
            )),
            new Email(array(
                'message' => 'The e-mail is not valid'
            ))
        ));

        $this->add($email);

          // Password
        $password = new Password('password',array(
            
            'class'=>'form-control login-frm-input helvetica',
            'placeholder' => 'Password',
        ));

        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Password is required'
            )),
            new StringLength(array(
                'min' => 8,
                'messageMinimum' => 'Password is too short. Minimum 8 characters'
            )),
            new Confirmation(array(
                'message' => 'Password doesn\'t match confirmation',
                'with' => 'confirmPassword'
            ))
        ));

        $this->add($password);

        // Confirm Password
        $confirmPassword = new Password('confirmPassword',array(
            
            'class'=>'form-control login-frm-input helvetica',
            'placeholder' => 'Confirm Password',
        ));

        $confirmPassword->addValidators(array(
            new PresenceOf(array(
                'message' => 'The confirmation password is required'
            )),
            new Confirmation(array(
                'message' => 'Password mismatch',
                'with' => 'password'
            ))
        ));

        $this->add($confirmPassword);

            
    }
}
