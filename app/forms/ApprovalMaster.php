
<?php

use Phalcon\Mvc\Model;

/**
 * ResetPasswords
 * Stores the reset password codes and their evolution
 */
class ApprovalMaster extends Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $approval_type_id;

    /**
     *
     * @var integer
     */
    public $Item_id;

    /**
     *
     * @var integer
     */
    public $requested_by;

    /**
     *
     * @var string
     */
    public $requested_date;

    /**
     *
     * @var string
     */
    public $approval_status;

    /**
     *
     * @var integer
     */
    public $Approved_by;

    /**
     *
     * @var string
     */
    public $Approval_date;

    /**
     *
     * @var integer
     */
    public $modified_by;

    /**
     *
     * @var string
     */
    public $modified_date;

    /**
     *
     * @var integer
     */
    //public $forwaded_to;

    public function initialize()
    {
        $this->hasMany('id', 'ApprovalItem', 'approval_master_id', array(
            'alias' => 'ApprovalItem',
            'foreignKey' => array(
                'message' => 'Item cannot be deleted because he/she has activity in the system'
            )
        ));
         $this->belongsTo('approval_type_id', 'ApprovalTypes', 'id', array(
            'alias' => 'ApprovalTypes',
            'reusable' => true
        ));
    }
}
