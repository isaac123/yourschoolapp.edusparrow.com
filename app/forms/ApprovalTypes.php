
<?php

use Phalcon\Mvc\Model;

/**
 * ResetPasswords
 * Stores the reset password codes and their evolution
 */
class ApprovalTypes extends Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $approval_type;

    /**
     *
     * @var integer
     */
    public $is_required;

    public function initialize()
    {
        $this->hasMany('id', 'ApprovalMaster', 'approval_type_id', array(
            'alias' => 'ApprovalMaster',
            'foreignKey' => array(
                'message' => 'Item cannot be deleted because he/she has activity in the system'
            )
        ));
    }
}
