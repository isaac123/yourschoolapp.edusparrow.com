<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;

class AddFeeGroupForm extends Form {

    public function initialize($entity = null, $options = null) {

        $name = new Text('feeGrpName', array(
            'placeholder' => 'Fee Group name...',
            'class' => 'form-control'
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Fee Group name is required'
                    ))
        ));

        $this->add($name);

          $active = new Select("isactive", array(
            '1' => 'Yes',
            '0' => 'No'
                ), array(
            'useEmpty' => false,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control'));

        $this->add($active);

          $feedesc = new TextArea("feedesc", array(
            'placeholder' => 'Description',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
        ));

        $this->add($feedesc);
    }

}
