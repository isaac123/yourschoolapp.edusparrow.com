<?php
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class LedgerAddForm extends Form {

    public function initialize($entity = null, $options = null) {

        $ledgernm = new Text('ledgername', array(
            'placeholder' => 'Ledger Name...',
            'class' => 'form-control'
        ));

        $ledgernm->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Ledger Name is required'
                    ))
        ));
        $this->add($ledgernm);

      

        /*$parentledger = new Text('parentledger', array(
             'class' => 'form-control'
        ));

        $parentledger->addValidators(array(
                new PresenceOf(array(
                  'message' => 'Parent Ledger is required'
                  )) 
        ));

        $this->add($parentledger);*/
           
    }

}
