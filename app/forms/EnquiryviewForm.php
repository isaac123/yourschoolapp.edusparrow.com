<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;

class EnquiryviewForm extends Form {

    public function initialize($entity = null, $options = null) {
        
         $status = new Select('status', array(
            '1' => 'Open',
            '2' => 'Closed'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
        ));
       
            $status->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Status is required'
                        ))
            ));
       
            $this->add($status);
        
        $comments = new TextArea('comments', array(
            'placeholder' => 'Enter your Comments',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->comments) ? $entity->comments : '',
        ));

        $comments->addValidators(array(
            new PresenceOf(array(
                'message' => 'Enter the comments'
                    ))
        ));
        $this->add($comments);
        
        $followups = new Text('followups', array(
            'placeholder' => 'Date',
            'readonly' => '',
            'class' => 'form-control followupdate',
            'value' => isset($entity->followups) ? date('d-m-Y', $entity->followups) : '',
        ));
            
            $this->add($followups);
        
    }

}
