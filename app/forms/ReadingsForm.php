<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\File;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

class ReadingsForm extends Form {

    public function initialize($entity = null, $options = null) {
        $ReadingTitle =  $ReadingDate = $ReadingComments = $disabled = '';
        if ((isset($options['View']) && $options['View']) || isset($options['Edit']) && $options['Edit']) {
            $readingId = new Hidden('$readingId');
            $this->add($readingId);
            $ReadingTitle = isset($entity->title) ? $entity->title : '';
            $ReadingDate = isset($entity->date) ? date('Y-m-d H:i', $entity->date) : '';
            $ReadingComments = isset($entity->description) ? $entity->description : '';
        }
        $title = new Text('title', array(
            'placeholder' => 'Title',
            'class' => 'form-control',
             'title' => 'Title'
        ));
        $this->add($title);

        $description = new TextArea('description', array(
            'placeholder' => 'Enter the Description',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => $ReadingComments,
        ));
        $this->add($description);

        $files = new File("files", array(
            'placeholder' => 'File',
            'title' => 'File'
        ));
        $this->add($files);

        $avail_date = new Text('avail_date', array(
            'placeholder' => 'Choose a Date',
            'class' => 'form-control form_datetime',
            'title' => 'Date',
            'value' => $ReadingDate ? $ReadingDate : date('Y-m-d H:i'),
            'readonly' => '',
            $disabled => ''
        ));

        $this->add($avail_date);
    }

}
