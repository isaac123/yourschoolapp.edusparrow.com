<?php
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Date;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

class AnnouncementeditForm extends Form {

    public function initialize($entity = null, $options = null) {
        
              $Announcementmessage_val= $AnnouncementComments_val= $disabled = '';
  
       if ((isset($options['View']) && $options['View']) || isset($options['Edit']) && $options['Edit']) {
            $announcementId = new Hidden('announcementId');
            $this->add($announcementId);
            $Announcementmessage_val = isset($entity->message)?$entity->message:'';
            $AnnouncementComments_val = isset($entity->comments)?$entity->comments:'';
         }
          $disabled = isset($options['View'])?'disabled':'';

      

        $Announcementmsg = new Text('announcementmsg', array(
            'placeholder' => 'Announcement Message...',
            'class' => 'form-control',
            'value'=>$Announcementmessage_val,
            $disabled=> ''
        ));

        $this->add($Announcementmsg);
        

        $announcementcmts = new TextArea('announcementcmts', array(
            'placeholder' => 'Announcement Comments...',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value'=>$AnnouncementComments_val,
            $disabled=> ''
        ));
       $this->add($announcementcmts);

       

     
    }

}
