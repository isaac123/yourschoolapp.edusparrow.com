<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class AddExamForm extends Form {

    public function initialize() {

        $name = new Text('exname', array(
            'placeholder' => 'Exam Name',
            'class' => 'form-control'
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The name is required'
                    )),
            new StringLength(array(
                'messageMaximum' => 'The name is too Long',
                'max' => 50
                    ))
        ));

        $this->add($name);

        $classID = new Hidden('classID');
        $this->add($classID);

        $code = new Text('excode', array(
            'placeholder' => '3 Character Exam Code',
            'class' => 'form-control'
        ));

        $code->addValidators(array(
            new PresenceOf(array(
                'message' => 'Exam Code is required'
                    )),
            new StringLength(array(
                'messageMaximum' => 'Exam Code Should Be Of 3 Characters',
                'max' => 3,
                'min' => 3
                    ))
        ));

        $this->add($code);


        $date_1 = new Text('mark_record_start', array(
            'class' => 'form-control form_date',
            'value' => date('d-m-Y')
        ));

        $date_2 = new Text('mark_record_end', array(
            'class' => 'form-control form_date',
            'value' => date('d-m-Y')
        ));


//        $linkmainexamtomainexam = new Check('link_check_box', array(
//            'class' => 'box',
//            'onchange' => 'examSettings.loadMainExLnk()'
//        ));

        $this->add($date_1);
        $this->add($date_2);
//        $this->add($linkmainexamtomainexam);
        
//        $attr = array(
//            'name' => 'groupName'
//        );
//
//        $radio1 = new \Phalcon\Forms\Element\Radio("radio1", $attr);
//        $radio2 = new \Phalcon\Forms\Element\Radio("radio2", $attr);
//
//        $this->add($radio1);
//        $this->add($radio2);
    }

}
