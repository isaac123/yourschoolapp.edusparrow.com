<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class ConferenceForm extends Form {

    public function initialize($entity = null, $options = null) {

        $conference_name = new Text('conference_name', array(
            'placeholder' => 'Conference Name',
            'class' => 'form-control',
            'value' => isset($entity->conference_name) ? $entity->conference_name : ''
        ));
        
        $this->add($conference_name);

        $message = new TextArea('message', array(
            'placeholder' => 'Enter Message',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->message) ? $entity->message : '',
        ));
        
            $this->add($message);
    
        $files = new File("files", array(
            'placeholder' => 'File'
        ));
        
         $this->add($files);
         
        $start_time = new Text('start_time', array(
            'placeholder' => 'Start Time',
            'class' => 'form-control form_date ',
            'value' => isset($entity->start_time) ? $entity->start_time : ''
        ));
        
        $this->add($start_time);
        
        $end_time = new Text('end_time', array(
            'placeholder' => 'End Time',
            'class' => 'form-control',
            'value' => isset($entity->end_time) ? $entity->end_time : ''
        ));
        
        $this->add($end_time);
        
         $total_duration = new Text('total_duration', array(
            'placeholder' => '',
            'class' => 'form-control',
            'value' => isset($entity->total_duration) ? $entity->total_duration : ''
        ));
        
        $this->add($total_duration);
         
    }

}
