<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class StaffProfileForm extends Form {

    public function initialize($entity = null, $options = null) {
        $readonly = '';
        if (isset($options['edit']) && $options['edit']) {
            $staff_add_info = count(StaffGeneralMaster::find('staff_id =' . $entity->id)) ?
                    StaffGeneralMaster::findFirstByStaffId($entity->id) :
                    new StaffGeneralMaster();
            $readonly = 'readonly';
        }
        $fieldArr = $fieldArr2 = array();
        $hideOrShowFields = StaffProfileSettings::find("hide_or_show = 1 and parent_id!=0");
        foreach ($hideOrShowFields as $hideOrShow) {
            $fieldArr[$hideOrShow->id] = $hideOrShow->form_name;
        }
        $hideOrShowFields1 = StaffProfileSettings::find("mandatory = 1 and parent_id!=0");
        foreach ($hideOrShowFields1 as $hideOrShow1) {
            $fieldArr2[$hideOrShow1->id] = $hideOrShow1->form_name;
        }
        /**
         *  
         * Staff Appointment Details
         * 
         * */
        if (isset($options['edit']) && $options['edit']) {
            $appno = new Text('appointment_no', array(
                'placeholder' => 'Appointment number',
                'class' => 'form-control',
                'value' => $entity->appointment_no,
                'readonly' => '',
                'title' => 'Appointment number'
            ));
            $appno->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Appointment number is required'
                        ))
            ));
            $this->add($appno);
        }
//         else {
//            $appno = new Text('appointment_no', array(
//                'placeholder' => 'Appointment number',
//                'class' => 'form-control',
//                'value' => $appointment_no,
//               'readonly' =>'',
//                'title' => 'Appointment number'
//            ));
//        }
//        $acaYr = new Select("academicYrId", AcademicYearMaster::find('status IN ("c","n")'), array(
//            "using" => array("id", "academic_year_name"),
//            'useEmpty' => True,
//            'emptyText' => 'Select',
//            'emptyValue' => '',
//            'class' => 'form-control',
//            'value' => isset($entity->academic_year_id) ? $entity->academic_year_id : '',
//            $readonly =>''
//        ));
//        $acaYr->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The academic year is required'
//                    ))
//        ));
//
//        $this->add($acaYr);
        //
        


        $category = new Select("category", array(
            '1' => 'Teaching',
            '2' => 'Non-Teaching'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
            'value' => isset($staff_add_info->catg_id) ? $staff_add_info->catg_id : '',
            'title' => 'Teaching category'
        ));

        if (in_array("category", $fieldArr2)) {
            $category->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The category is required'
                        ))
            ));
        }
        if (in_array("category", $fieldArr)) {
            $this->add($category);
        }

        $doa = new Text('doa', array(
            'class' => 'form-control form_date',
            'placeholder' => 'Date of Appointment...',
            'value' => isset($entity->Date_of_Joining) ? date('d-m-Y', $entity->Date_of_Joining) : '',
            'title' => 'Date of Appointment',
            'readonly'=>''
        ));

        $doa->addValidators(array(
            new PresenceOf(array(
                'message' => 'The date of Apointment is required'
                    ))
        ));

        $this->add($doa);


        $name = new Text('staffName', array(
            'placeholder' => 'Staff name...',
            'class' => 'form-control',
            'value' => isset($entity->Staff_Name) ? $entity->Staff_Name : '',
            'title' => 'Staff name'
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Staff name is required'
                    )),
            new RegexValidator(array(
                'pattern' => '/^[a-zA-Z ]*$/',
                'message' => 'The Staff name is invalid'
                    ))
        ));

        $this->add($name);

//        $div = DivisionMaster::findFirst('divType = "staff" and academicYrID=' . $cacdyr->id);
//        $divVal = new Select('divVal', DivisionValues::find('divID = ' . $div->id), array(
//            "using" => array("id", "classname"),
//            'useEmpty' => true,
//            'emptyText' => 'Select',
//            'emptyValue' => '',
//            'class' => 'form-control',
//            'value' => isset($entity->Department) ? $entity->Department : ''
//        ));
//
//        $divVal->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'Department is required'
//                    ))
//        ));
//
//        $this->add($divVal);


        /**
         *  
         * Staff Personal Details
         * 
         * */
        $dob = new Text('dob', array(
            'class' => 'form-control',
            'placeholder' => 'Date of Birth...',
            'value' => isset($entity->Date_of_Birth) ? date('d-m-Y', $entity->Date_of_Birth) : '',
            'title' => 'Date of Birth',
             'readonly'=>''
        ));

        $dob->addValidators(array(
            new PresenceOf(array(
                'message' => 'The date of Birth is required'
                    ))
        ));

        $this->add($dob);

        $gender = new Select("gender", array(
            '1' => 'Female',
            '2' => 'Male'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
            'title' => 'gender',
            'value' => isset($entity->Gender) ? $entity->Gender : ''));
        $gender->addValidators(array(
            new PresenceOf(array(
                'message' => 'The gender is required'
                    ))
        ));

        $this->add($gender);
        $address = new TextArea('address1', array(
            'placeholder' => 'Address Line 1',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'title' => 'Address Line 1',
            'value' => isset($entity->Address1) ? $entity->Address1 : ''
        ));



        $nationality = new Select('nationality', CountryMaster::find(), array(
            "using" => array("country_code", "country_name"),
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
            'title' => 'Nationality',
            'value' => isset($staff_add_info->nationality) ? $staff_add_info->nationality : ''
        ));
        if (in_array("nationality", $fieldArr2)) {
            $nationality->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Nationality is required'
                        ))
            ));
            ;
        }
        if (in_array("nationality", $fieldArr)) {
            $this->add($nationality);
        }


        $address->addValidators(array(
            new PresenceOf(array(
                'message' => 'Address1 is required'
                    ))
        ));


        $this->add($address);

        $address2 = new TextArea('address2', array(
            'placeholder' => 'Address Line 2',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'title' => 'Address Line 2',
            'value' => isset($entity->Address2) ? $entity->Address2 : ''
        ));

//        $address2->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'Address2 is required'
//                    ))
//        ));

        $this->add($address2);

        $phone = new Text('phone', array(
            'placeholder' => 'Phone',
            'class' => 'form-control',
            'title' => 'Phone',
            'value' => isset($entity->Phone) ? $entity->Phone : ''
        ));

        $phone->addValidators(array(
            new PresenceOf(array(
                'message' => 'The phone number is required'
                    )), new RegexValidator(array(
                'pattern' => '/^[0-9]*$/',
                'message' => 'The phone number is invalid'
                    )) /* , 
                  new StringLength(array(
                  'min' => 10,
                  'messageMinimum' => 'Invalid phone number'
                  )) */
        ));

        $this->add($phone);

        $email = new Text('email', array(
            'placeholder' => 'Email',
            'class' => 'form-control',
            'title' => 'Email',
            'value' => isset($entity->Email) ? $entity->Email : ''
        ));

        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'The e-mail is required'
                    )),
            new Email(array(
                'message' => 'The e-mail is not valid'
                    ))
        ));

        $this->add($email);




        $state = new Text('state', array(
            'placeholder' => 'State...',
            'class' => 'form-control',
            'title' => 'State',
            'value' => isset($entity->State) ? $entity->State : ''
        ));

//        $state->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The state is required'
//                    ))
//        ));

        $this->add($state);


        $country = new Select('country', CountryMaster::find(), array(
            "using" => array("country_code", "country_name"),
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
            'title' => 'Country',
            'value' => isset($entity->Country) ? $entity->Country : ''
        ));

        $country->addValidators(array(
            new PresenceOf(array(
                'message' => 'Country name is required'
                    ))
        ));

        $this->add($country);


        $pin = new Text('pin', array(
            'placeholder' => 'Pin code...',
            'class' => 'form-control',
            'title' => 'Pin code',
            'value' => isset($entity->Pin) ? $entity->Pin : ''
        ));

//        $pin->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Pin code is required'
//                    )), new RegexValidator(array(
//                'pattern' => '/^[0-9]*$/',
//                'message' => 'The Pin code is invalid'
//                    ))
//        ));

        $this->add($pin);


        $qualification = new Text('qualification', array(
            'placeholder' => 'Qualification...',
            'class' => 'form-control',
            'title' => 'Qualification',
            'value' => isset($staff_add_info->qualification) ? $staff_add_info->qualification : ''
        ));

        if (in_array("qualification", $fieldArr2)) {
            $qualification->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Qualification is required'
                        ))
            ));
        }
        if (in_array("qualification", $fieldArr)) {
            $this->add($qualification);
        }
        $photo = new File("photo", array(
            'placeholder' => 'photo',
            'title' => 'photo',
            'value' => isset($staff_add_info->photo) ? $staff_add_info->photo : ''
        ));

        if (in_array("photo", $fieldArr)) {
            $this->add($photo);
        }

        $address_proof1 = new File("address_proof1", array(
            'placeholder' => 'Adress Proof 1',
            'title' => 'Adress Proof 1',
            'value' => isset($staff_add_info->address_proof1) ? $staff_add_info->address_proof1 : ''
        ));

        if (in_array("address_proof1", $fieldArr)) {
            $this->add($address_proof1);
        }

        $address_proof2 = new File("address_proof2", array(
            'placeholder' => 'Address Proof 2',
            'title' => 'Adress Proof 2',
            'value' => isset($staff_add_info->address_proof2) ? $staff_add_info->address_proof2 : ''
        ));

        if (in_array("address_proof2", $fieldArr)) {
            $this->add($address_proof2);
        }

        $address_proof3 = new File("address_proof3", array(
            'placeholder' => 'Address Proof 3',
            'title' => 'Adress Proof 3',
            'value' => isset($staff_add_info->address_proof3) ? $staff_add_info->address_proof3 : ''
        ));

        if (in_array("address_proof3", $fieldArr)) {
            $this->add($address_proof3);
        }


        /**
         *  
         * Staff Medical Details
         * 
         * */
        $blood_grp = new Select("blood_grp", array(
            'A-' => 'A-',
            'A+' => 'A+',
            'B-' => 'B-',
            'B+' => 'B+',
            'AB+' => 'AB+',
            'AB-' => 'AB-',
            'O-' => 'O-',
            'O+' => 'O+',
            'A1+' => 'A1+',
            'A1-' => 'A1-',
            'A2+' => 'A2+',
            'A2-' => 'A2-',
            'A1B+' => 'A1B+',
            'A1B-' => 'A1B-',
            'A2B+' => 'A2B+',
            'A2B-' => 'A2B-'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
            'title' => 'Blood Group',
            'value' => isset($staff_add_info->blood_group) ? $staff_add_info->blood_group : ''));

        if (in_array("blood_grp", $fieldArr2)) {
            $blood_grp->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Blood Group is required'
                        ))
            ));
        }
        if (in_array("blood_grp", $fieldArr)) {
            $this->add($blood_grp);
        }




        $othrMedDet = new TextArea('othrMedDet', array(
            'placeholder' => 'Any other details',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'title' => 'other medical details',
            'value' => isset($staff_add_info->medical_details) ? $staff_add_info->medical_details : ''
        ));
        if (in_array("othrMedDet", $fieldArr2)) {
            $othrMedDet->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The other medical details is required'
                        ))
            ));
        }
        if (in_array("othrMedDet", $fieldArr)) {

            $this->add($othrMedDet);
        }



        /**
         *  
         * Student Spouse Details
         * 
         * */
        $sname = new Text('sname', array(
            'placeholder' => 'Spouse Name...',
            'class' => 'form-control',
            'title' => 'Spouse Name',
            'value' => isset($staff_add_info->spouse_name) ? $staff_add_info->spouse_name : ''
        ));
        if (in_array("sname", $fieldArr2)) {
            $sname->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Spouse name is required'
                        )),
                new RegexValidator(array(
                    'pattern' => '/^[a-zA-Z ]*$/',
                    'message' => 'The Spouse name is invalid'
                        ))
            ));
        }
        if (in_array("sname", $fieldArr)) {

            $this->add($sname);
        }


        $soccup = new Text('soccup', array(
            'placeholder' => 'Spouse Occupation...',
            'class' => 'form-control',
            'title' => 'Spouse Occupation',
            'value' => isset($staff_add_info->s_occupation) ? $staff_add_info->s_occupation : ''
        ));
        if (in_array("soccup", $fieldArr2)) {
            $soccup->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Spouse Occupation is required'
                        ))
            ));
        }
        if (in_array("soccup", $fieldArr)) {

            $this->add($soccup);
        }


        $sdesign = new Text('sdesign', array(
            'placeholder' => 'Spouse Designation...',
            'class' => 'form-control',
            'title' => 'Spouse Designation',
            'value' => isset($staff_add_info->s_designation) ? $staff_add_info->s_designation : ''
        ));

        if (in_array("soccup", $fieldArr2)) {
            $sdesign->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Spouse Designation is required'
                        ))
            ));
        }
        if (in_array("soccup", $fieldArr)) {
            $this->add($sdesign);
        }




        $sphone = new Text('sphone', array(
            'placeholder' => 'Spouse Phone',
            'class' => 'form-control',
            'title' => 'Spouse Phone',
            'value' => isset($staff_add_info->s_phoneno) ? $staff_add_info->s_phoneno : ''
        ));


        if (in_array("sphone", $fieldArr2)) {
            $sphone->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Spouse\'s phone number is required'
                        )), new RegexValidator(array(
                    'pattern' => '/^[0-9]*$/',
                    'message' => 'The Spouse\'s phone number is invalid'
                        ))/* ,
                      new StringLength(array(
                      'min' => 10,
                      'messageMinimum' => 'Invalid Spouse\'s phone number'
                      )) */
            ));
        }
        if (in_array("sphone", $fieldArr)) {
            $this->add($sphone);
        }





        /**
         *  
         * Student Other Details
         * 
         * */
        $classHandl = new Text('classHandl', array(
            'placeholder' => 'Classes Handling (Interests) ...',
            'class' => 'form-control',
            'title' => 'Classes Handling (Interests) ',
            'value' => isset($staff_add_info->classes_handling) ? $staff_add_info->classes_handling : ''
        ));


        if (in_array("classHandl", $fieldArr2)) {
            $classHandl->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Classes Handling'
                        ))
            ));
        }
        if (in_array("classHandl", $fieldArr)) {
            $this->add($classHandl);
        }



        $subjHandl = new Text('subjHandl', array(
            'placeholder' => 'Subjects Handling (Interests)...',
            'class' => 'form-control',
            'title' => 'Subjects Handling (Interests) ',
            'value' => isset($staff_add_info->subjects_handling) ? $staff_add_info->subjects_handling : ''
        ));


        if (in_array("subjHandl", $fieldArr2)) {
            $subjHandl->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Subjects Handling'
                        ))
            ));
        }
        if (in_array("subjHandl", $fieldArr)) {
            $this->add($subjHandl);
        }


        $prevExp = new Text('prevExp', array(
            'placeholder' => 'Previous Work Experience (years)...',
            'class' => 'form-control',
            'title' => 'Previous Work Experience (years) ',
            'value' => isset($staff_add_info->experience) ? $staff_add_info->experience : ''
        ));


        if (in_array("prevExp", $fieldArr2)) {
            $prevExp->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Previous Work Experience'
                        )), new RegexValidator(array(
                    'pattern' => '/^[0-9]*$/',
                    'message' => 'Previous Work Experience is invalid'
                        ))/* ,
                      new StringLength(array(
                      'min' => 2,
                      'messageMinimum' => 'Invalid Work Experience'
                      )) */
            ));
        }
        if (in_array("prevExp", $fieldArr)) {
            $this->add($prevExp);
        }

        $Org = new Text('Org', array(
            'placeholder' => 'Organization...',
            'class' => 'form-control',
            'title' => 'Organization',
            'value' => isset($staff_add_info->organization) ? $staff_add_info->organization : ''
        ));

        if (in_array("Org", $fieldArr2)) {
            $Org->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Organization'
                        ))
            ));
        }
        if (in_array("Org", $fieldArr)) {
            $this->add($Org);
        }

        $designation = new Text('designation', array(
            'placeholder' => 'Designation...',
            'class' => 'form-control',
            'title' => 'Designation',
            'value' => isset($staff_add_info->designation) ? $staff_add_info->designation : ''
        ));

        if (in_array("designation", $fieldArr2)) {
            $designation->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Designation'
                        ))
            ));
        }
        if (in_array("designation", $fieldArr)) {
            $this->add($designation);
        }


        $exActi = new TextArea('exActi', array(
            'placeholder' => 'Extra curricular Activities',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'title' => 'Extra curricular Activities',
            'value' => isset($staff_add_info->extra_curricular) ? $staff_add_info->extra_curricular : ''
        ));

        if (in_array("exActi", $fieldArr2)) {
            $exActi->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Extra curricular Activities'
                        ))
            ));
        }
        if (in_array("exActi", $fieldArr)) {
            $this->add($exActi);
        }


        $coActi = new TextArea('coActi', array(
            'placeholder' => 'Co-curricular Activities',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'title' => ' Co-curricular Activities',
            'value' => isset($staff_add_info->co_curricular) ? $staff_add_info->co_curricular : ''
        ));
        if (in_array("coActi", $fieldArr2)) {
            $coActi->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Co-curricular Activities'
                        ))
            ));
        }
        if (in_array("coActi", $fieldArr)) {
            $this->add($coActi);
        }



        $comments = new TextArea('comments', array(
            'placeholder' => 'Comments',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'title' => ' comments',
            'value' => isset($staff_add_info->comments) ? $staff_add_info->comments : ''
        ));

        if (in_array("comments", $fieldArr2)) {
            $comments->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Comments '
                        ))
            ));
        }
        if (in_array("comments", $fieldArr)) {
            $this->add($comments);
        }



        $addQuali = new TextArea('addQuali', array(
            'placeholder' => 'Additional Qualifications',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'title' => ' Additional Qualifications',
            'value' => isset($staff_add_info->additional_qualifications) ? $staff_add_info->additional_qualifications : ''
        ));


        if (in_array("addQuali", $fieldArr2)) {
            $addQuali->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Additional Qualifications '
                        ))
            ));
        }
        if (in_array("addQuali", $fieldArr)) {

            $this->add($addQuali);
        }

        /**
         *  
         * Staff Parent/Guardian Details 
         * 
         * */
        $gname = new Text('gname', array(
            'placeholder' => 'Parent/Guardian Name...',
            'class' => 'form-control',
            'title' => ' Parent/Guardian Name',
            'value' => isset($staff_add_info->parent_name) ? $staff_add_info->parent_name : ''
        ));

        if (in_array("gname", $fieldArr2)) {

            $gname->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Parent/Guardian Name name is required'
                        )),
                new RegexValidator(array(
                    'pattern' => '/^[a-zA-Z ]*$/',
                    'message' => 'The Parent/Guardian Name is invalid'
                        ))
            ));
        }
        if (in_array("gname", $fieldArr)) {
            $this->add($gname);
        }



        $goccup = new Text('goccup', array(
            'placeholder' => 'Parent/Guardian Occupation...',
            'class' => 'form-control',
            'title' => ' Parent/Guardian Occupation',
            'value' => isset($staff_add_info->p_occupation) ? $staff_add_info->p_occupation : ''
        ));

        if (in_array("goccup", $fieldArr2)) {

            $goccup->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Parent/Guardian Occupation is required'
                        ))
            ));
        }
        if (in_array("goccup", $fieldArr)) {

            $this->add($goccup);
        }



        $gdesign = new Text('gdesign', array(
            'placeholder' => 'Parent/Guardian Designation...',
            'class' => 'form-control',
            'title' => ' Parent/Guardian Designation',
            'value' => isset($staff_add_info->p_designation) ? $staff_add_info->p_designation : ''
        ));
        if (in_array("gdesign", $fieldArr2)) {
            $gdesign->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Parent/Guardian Designation is required'
                        ))
            ));
        }
        if (in_array("gdesign", $fieldArr)) {
            $this->add($gdesign);
        }




        $gphone = new Text('gphone', array(
            'placeholder' => 'Phone',
            'class' => 'form-control',
            'title' => ' Parent/Guardian Phone',
            'value' => isset($staff_add_info->p_phoneno) ? $staff_add_info->p_phoneno : ''
        ));
        if (in_array("gphone", $fieldArr2)) {
            $gphone->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Parent/Guardian\'s phone number is required'
                        )), new RegexValidator(array(
                    'pattern' => '/^[0-9]*$/',
                    'message' => 'The Parent/Guardian\'s phone number is invalid'
                        ))/* ,
                      new StringLength(array(
                      'min' => 10,
                      'messageMinimum' => 'Invalid Parent/Guardian\'s phone number'
                      )) */
            ));
        }
        if (in_array("gphone", $fieldArr)) {
            $this->add($gphone);
        }
        
            $biometric_id = new Text('biometric_id', array(
            'placeholder' => 'BioMetric ID...',
            'class' => 'form-control',
            'title' => 'BioMetric ID',
            'value' => isset($staff_add_info->biometric_id) ? $staff_add_info->biometric_id : ''
        ));
        if (in_array("biometric_id", $fieldArr2)) {
            $biometric_id->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The BioMetric ID is required'
                        ))
            ));
        }
        if (in_array("biometric_id", $fieldArr)) {
            $this->add($biometric_id);
        }
        $pf_no = new Text('pf_no', array(
            'placeholder' => 'PF No...',
            'class' => 'form-control',
            'title' => 'PF No',
            'value' => isset($staff_add_info->pf_no) ? $staff_add_info->pf_no : ''
        ));
        if (in_array("pf_no", $fieldArr2)) {
            $pf_no->addValidators(array(
                new PresenceOf(array(
                    'message' => 'PF No is required'
                        ))
            ));
        }
        if (in_array("pf_no", $fieldArr)) {
            $this->add($pf_no);
        }
         $esi_no = new Text('esi_no', array(
            'placeholder' => 'ESI No...',
            'class' => 'form-control',
            'title' => 'ESI No',
            'value' => isset($staff_add_info->esi_no) ? $staff_add_info->esi_no : ''
        ));
        if (in_array("esi_no", $fieldArr2)) {
            $esi_no->addValidators(array(
                new PresenceOf(array(
                    'message' => 'ESI No is required'
                        ))
            ));
        }
        if (in_array("pf_no", $fieldArr)) {
            $this->add($esi_no);
        }
    }

}
