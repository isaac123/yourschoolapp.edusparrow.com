<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class MovementForm extends Form {

    public function initialize($entity = null, $options = null) {

        $po_no = new Text('po_no', array(
            'placeholder' => 'purchase Order No',
            'class' => 'form-control',
            'value' => isset($entity->po_no) ? $entity->po_no : ''
        ));
        $po_no->addValidators(array(
            new PresenceOf(array(
                'message' => 'Purchase Order No  is required'
                    ))
        ));
        $this->add($po_no);

        $movement_type = new Select('movement_type', array(
            'Partial' => 'Partial',
            'Closed' => 'Closed'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
        ));
        $movement_type->addValidators(array(
            new PresenceOf(array(
                'message' => 'Type is required'
                    ))
        ));

        $this->add($movement_type);


        $invoice_no = new Text('invoice_no', array(
            'placeholder' => 'Invoice No',
            'class' => 'form-control',
            'value' => isset($entity->invoice_no) ? $entity->invoice_no : ''
        ));
        $invoice_no->addValidators(array(
            new PresenceOf(array(
                'message' => 'Invoice No  is required'
                    ))
        ));
        $this->add($invoice_no);
        
          $dc_no = new Text('dc_no', array(
            'placeholder' => 'DC No',
            'class' => 'form-control',
            'value' => isset($entity->dc_no) ? $entity->dc_no : ''
        ));
        $dc_no->addValidators(array(
            new PresenceOf(array(
                'message' => 'DC No  is required'
                    ))
        ));
        $this->add($dc_no);
        
        $details = new TextArea('details', array(
            'placeholder' => '',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->details) ? $entity->details : ''
        ));
        $details->addValidators(array(
            new PresenceOf(array(
                'message' => 'Details is required'
                    ))
        ));
        $this->add($details);
        
         $amount = new Text('amount', array(
            'placeholder' => 'Amount',
            'class' => 'form-control',
             'onchange' => "movementinSettings.validateAmountdet(this)",
            'value' => isset($entity->amount) ? $entity->amount : ''
        ));
        $amount->addValidators(array(
            new PresenceOf(array(
                'message' => 'Amount  is required'
                    ))
        ));
        $this->add($amount);
    }

}
