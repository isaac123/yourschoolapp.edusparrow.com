<?php
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\File;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

class AssignmentForm extends Form
{

    public function initialize($entity = null, $options = null)
    {
        
        $AssignmentTopic_val= $AssignmentDesc_val = $AssignmentDeadline_val =  $AssignmentComments_val = $disabled = '';
        if ((isset($options['View']) && $options['View'])|| isset($options['Edit']) && $options['Edit']) {
            
            $assignmentId = new Hidden('assignmentId');
            $this->add($assignmentId);
            $AssignmentTopic_val = isset($entity->topic)?$entity->topic:'';
            $AssignmentDesc_val = isset($entity->desc)?$entity->desc:'';
            $AssignmentDeadline_val =  isset($entity->submission_date)?date('Y-m-d H:i',$entity->submission_date):'';
            $AssignmentComments_val = isset($entity->comments)?$entity->comments:'';
            
        }
        $disabled = isset($options['View'])?'disabled':'';
        $staffID = new Hidden('staffID');
          $this->add($staffID);


        $AssignmentTopic = new TextArea('AssignmentTopic', array(
            'placeholder' => 'Topic',
            'class' => 'form-control',
            'cols'=>"60",
            'rows'=>"2",
            'value'=>$AssignmentTopic_val,
            'title' => 'Assignment Topic',
            $disabled=> ''
        ));

        $AssignmentTopic->addValidators(array(
            new PresenceOf(array(
                'message' => 'Please enter assignment topic'
            ))
        ));

        $this->add($AssignmentTopic);


        $AssignmentDesc = new TextArea('AssignmentDesc', array(
            'placeholder' => 'Detailed Description',
            'class' => 'form-control',
            'cols'=>"90",
            'rows'=>"10",
            'value'=>$AssignmentDesc_val,
            'title' => 'Assignment Description',
            $disabled=> ''
        ));

        /*$AssignmentDesc->addValidators(array(
            new PresenceOf(array(
                'message' => 'Add Description'
            ))
        ));*/

        $this->add($AssignmentDesc);





        $AssignmentDeadline = new Text('AssignmentDeadline', array(
            'placeholder' => 'Choose a Date for Deadline',
            'class' => 'form-control form_date',
            'value'=>$AssignmentDeadline_val,
            'title' => 'Assignment Deadline',
            'readonly'=> '',
            $disabled=> ''
        ));

        $AssignmentDeadline->addValidators(array(
            new PresenceOf(array(
                'message' => 'Deadline is required'
            ))
        ));

        $this->add($AssignmentDeadline);



        $AssignmentComments = new TextArea('AssignmentComments', array(
            'placeholder' => 'Any Comments for yourself',
            'class' => 'form-control',
            'cols'=>"90",
            'rows'=>"10",
            'value'=>$AssignmentComments_val,
            $disabled=> ''
        ));

        $this->add($AssignmentComments);

        $files = new File("files", array(
            'placeholder' => 'File'
        ));

        $this->add($files);
    }
}
