<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class SchoolSettingsForm extends Form {

    public function initialize() {


        $sclname = new Text('school_name', array(
            'placeholder' => '',
            'class' => ''
        ));

        $sclname->addValidators(array(
            new PresenceOf(array(
                'message' => 'The name is required'
                    )),
            new RegexValidator(array(
                'pattern' => '/^[a-zA-Z ]*$/',
                'message' => 'The School name is invalid'
                    ))
        ));

        $this->add($sclname);

        //Admission Prefix
//        $admissionprefix = new Text('admission_prefix', array(
//            'placeholder' => '',
//            'class' => ''
//        ));

//        $admissionprefix->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Admission Prefix is required'
//                    )),
//            new RegexValidator(array(
//                'pattern' => '/^[a-zA-Z0-9]*$/',
//                'message' => 'The Admission Prefix is invalid'
//                    ))
//        ));

//        $this->add($admissionprefix);
        //Admission Sufix
//        $admissionsufix = new Text('admission_sufix', array(
//            'placeholder' => '',
//            'class' => ''
//        ));

//        $admissionsufix->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Admission Sufix is required'
//                    )),
//            new RegexValidator(array(
//                'pattern' => '/^[a-zA-Z0-9]*$/',
//                'message' => 'The Admission Sufix is invalid'
//                    ))
//        ));

//        $this->add($admissionsufix);


        //Appointment Prefix
//        $appointmentprefix = new Text('appointment_prefix', array(
//            'placeholder' => '',
//            'class' => ''
//        ));

//        $appointmentprefix->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Appointment Prefix is required'
//                    )),
//            new RegexValidator(array(
//                'pattern' => '/^[a-zA-Z0-9]*$/',
//                'message' => 'The Appointment Prefix is invalid'
//                    ))
//        ));

//        $this->add($appointmentprefix);


        //Appointment Sufix
//        $appointmentsufix = new Text('appointment_sufix', array(
//            'placeholder' => '',
//            'class' => ''
//        ));

//        $appointmentsufix->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Appointment Sufix is required'
//                    )),
//            new RegexValidator(array(
//                'pattern' => '/^[a-zA-Z0-9]*$/',
//                'message' => 'The Appointment Sufix is invalid'
//                    ))
//        ));

//        $this->add($appointmentsufix);


        $address = new TextArea('address_1', array(
            'placeholder' => '',
            'class' => '',
            'cols' => "10",
            'rows' => "2"
        ));

        $address->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Address is required'
                    ))
        ));

        $this->add($address);


        $address = new TextArea('address_2', array(
            'placeholder' => '',
            'class' => '',
            'cols' => "10",
            'rows' => "2"
        ));

        $this->add($address);

//        $logo = new File("file", array(
//            'placeholder' => ''
//        ));
////        $logo->addValidators(array(
////            new PresenceOf(array(
////                'message' => 'The logo is required'
////            ))
////        ));
//
//        $this->add($logo);
//        $phone = new Text('phone', array(
//            'placeholder' => '',
//            'class' => ''
//        ));
//
//        $phone->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The phone is required'
//                    )),
//            new RegexValidator(array(
//                'pattern' => '/^[0-9]*$/',
//                'message' => 'The Phone is invalid'
//                    ))
//        ));
//
//        $this->add($phone);
//
//        $email = new Text('email', array(
//            'placeholder' => '',
//            'class' => ''
//        ));
//
//        $email->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The e-mail is required'
//                    )),
//            new Email(array(
//                'message' => 'The e-mail is not valid'
//                    ))
//        ));
//
//        $this->add($email);


//        $name = new Text('primary_contact_name', array(
//            'placeholder' => '',
//            'class' => ''
//        ));
//
//        $name->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Primary contact name is required'
//                    )),
//            new RegexValidator(array(
//                'pattern' => '/^[a-zA-Z ]*$/',
//                'message' => 'The Primary contact name is invalid'
//                    ))
//        ));
//
//        $this->add($name);

        $pphone = new Text('primary_contact_phone', array(
            'placeholder' => '',
            'class' => ''
        ));

        $pphone->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Primary phone contact is required'
                    )),
            new RegexValidator(array(
                'pattern' => '/^[0-9]*$/',
                'message' => 'The  Primary phone contact is invalid'
                    ))
        ));

        $this->add($pphone);

        $pemail = new Text('primary_contact_email', array(
            'placeholder' => '',
            'class' => ''
        ));

        $pemail->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Primary contact e-mail is required'
                    )),
            new Email(array(
                'message' => 'The Primary contact e-mail is not valid'
                    ))
        ));

        $this->add($pemail);



//        $sname = new Text('secondary_contact_name', array(
//            'placeholder' => '',
//            'class' => ''
//        ));

//        $sname->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Secondary contact name is required'
//                    )),
//            new RegexValidator(array(
//                'pattern' => '/^[a-zA-Z ]*$/',
//                'message' => 'The Secondary contact name is invalid'
//                    ))
//        ));

//        $this->add($sname);

//        $sphone = new Text('secondary_contact_phone', array(
//            'placeholder' => '',
//            'class' => ''
//        ));

//        $sphone->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Secondary contact phone is required'
//                    )),
//            new RegexValidator(array(
//                'pattern' => '/^[0-9]*$/',
//                'message' => 'The Secondary contact phone is invalid'
//                    ))
//        ));

//        $this->add($sphone);

//        $semail = new Text('secondary_contact_email', array(
//            'placeholder' => '',
//            'class' => ''
//        ));

//        $semail->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The e-mail is required'
//                    )),
//            new Email(array(
//                'message' => 'The Secondary contact e-mail is not valid'
//                    ))
//        ));

//        $this->add($semail);


        $state = new Text('state', array(
            'placeholder' => '',
            'class' => '',
            'title' => 'State',
        ));

        $state->addValidators(array(
            new PresenceOf(array(
                'message' => 'The state is required'
                    ))
        ));

        $this->add($state);



        $pin = new Text('pin', array(
            'placeholder' => '',
            'class' => '',
            'title' => 'Pin code',
        ));

        $pin->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Pin code is required'
                    )), new RegexValidator(array(
                'pattern' => '/^[0-9]*$/',
                'message' => 'The Pin code is invalid'
                    ))
        ));

        $this->add($pin);
        
        

//        $max_stu = new Text('max_stu', array(
//            'placeholder' => '',
//            'class' => '',
//            'title' => 'Maximun number of students',
//        ));
//
//        $max_stu->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The student count is required'
//                    ))
//        ));
//
//        $this->add($max_stu);
//        $max_stf = new Text('max_stf', array(
//            'placeholder' => '',
//            'class' => '',
//            'title' => 'Maximun number of Staff',
//        ));
//
//        $max_stf->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Staff count is required'
//                    ))
//        ));
//
//        $this->add($max_stf);
        
    }

}
