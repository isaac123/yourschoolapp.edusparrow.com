<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class AddRatingForm extends Form {

    public function initialize() {

        $name = new Text('ratname', array(
            'placeholder' => 'Rating Name',
            'class' => 'form-control'
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The name is required'
                    )),
            new StringLength(array(
                'messageMaximum' => 'The name is too Long',
                'max' => 50
                    ))
        ));

        $this->add($name);

        $classID = new Hidden('classID');
        $this->add($classID);

    }

}
