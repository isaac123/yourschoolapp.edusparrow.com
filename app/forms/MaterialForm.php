<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class MaterialForm extends Form {

    public function initialize($entity = null, $options = null) {

        $material_no = new Text('material_no', array(
            'placeholder' => 'Material No',
            'class' => 'form-control',
            'value' => isset($entity->material_no) ? $entity->material_no : ''
        ));
       
        $this->add($material_no);

        $material_name = new Text('material_name', array(
            'placeholder' => 'Material Name',
            'class' => 'form-control material_name',
            'value' => isset($entity->material_name) ? $entity->material_name : ''
        ));
        $material_name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Material Name is required'
                    ))
        ));

        $this->add($material_name);

        $material_desc = new TextArea('material_desc', array(
            'placeholder' => 'Enter the Material Description',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->material_desc) ? $entity->material_desc : ''
        ));
        $material_desc->addValidators(array(
            new PresenceOf(array(
                'message' => 'Material Description is required'
                    ))
        ));
        $this->add($material_desc);
        
        $unit_name = new Text('unit_name', array(
            'placeholder' => 'Unit Name',
            'class' => 'form-control',
            'value' => isset($entity->unit_name) ? $entity->unit_name : ''
        ));
//         $unit_name->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'unit_name is required'
//                    ))
//        ));
        $this->add($unit_name);
        
        $materialno = new Text('materialno', array(
            'placeholder' => 'Material No',
            'class' => 'form-control',
            'value' => isset($entity->materialno) ? $entity->materialno : ''
        ));
        $this->add($materialno);
    }

}
