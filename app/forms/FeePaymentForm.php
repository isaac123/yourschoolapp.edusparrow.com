<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class FeePaymentForm extends Form {

    public function initialize($entity = null, $options = null) {

    $totamount = new Hidden('totamount',array( 'title'=>'Total amount'));
    $stufeeId = new Hidden('stufeeId',array( 'title'=>'Student Fee Id'));
    $tobepaidamount = new Hidden('tobepaidamount',array( 'title'=>'To be Paid amount'));
    $paidamount = new Hidden('paidamount',array( 'title'=>'Paid amount'));
    
        $this->add($totamount);
        $this->add($tobepaidamount);
        $this->add($paidamount);
        $this->add($stufeeId);
    
       /* $state = new Text('amount', array(
            'placeholder' => '0.00',
            'class' => 'form-control',
            'title'=>'Amount'
        ));

        $state->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Amount is required'
                    )), new RegexValidator(array(
                'pattern' => '/^[1-9](?:\d{1,2})+([,]\d{3})*(?:\.\d{0,1}[1-9])?|0?\.\d{0,1}[1-9]$/',
                'message' => 'The amount is invalid'
                    ))
        ));
        $this->add($state);


        $address = new TextArea('details', array(
            'placeholder' => 'Details',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'title'=>'Details'
        ));

        $address->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Details is required'
                    ))
        ));

        $this->add($address);*/
    }

}
