<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class EnquiryForm extends Form {

    public function initialize($entity = null, $options = null) {


        $cacdyr = OrganizationalStructureValues::find('org_master_id = ' . OrganizationalStructureMaster::findFirstByCycleNode(1)->id);

        $status = StudentEnquiryStatus::find();


        if (!isset($options['update']) && !$options['update']) {


            $parentname = new Text('parent_name', array(
                'placeholder' => 'Parent Name',
                'class' => 'form-control',
                'value' => isset($entity->parent_name) ? $entity->parent_name : ''
            ));


            $this->add($parentname);

            $phonenumber = new Text('phone_number', array(
                'placeholder' => 'Phone number',
                'class' => 'form-control',
                'value' => isset($entity->contact_number) ? $entity->contact_number : ''
            ));


            $this->add($phonenumber);

            $email = new Text('email', array(
                'placeholder' => 'Email',
                'class' => 'form-control',
                'value' => isset($entity->mail_id) ? $entity->mail_id : ''
            ));

            $this->add($email);

            $candidatename = new Text('candidate_name', array(
                'placeholder' => 'Candidate Name',
                'class' => 'form-control',
                'value' => isset($entity->candidate_name) ? $entity->candidate_name : ''
            ));

            $candidatename->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Candidate Name is required'
                        ))
            ));

            $this->add($candidatename);
            
            $comments = new TextArea('comments', array(
                'placeholder' => 'Enter Your Comments',
                'class' => 'form-control',
                'cols' => "60",
                'rows' => "2"
                    //'value' => isset($entity->comments) ? $entity->comments : '',
            ));

            $comments->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Comment is required'
                        ))
            ));

            $this->add($comments);



        } else {

//            print_r(count($entity));
//            exit;
            $comments = new TextArea('comments', array(
                'placeholder' => 'Enter Your Comments',
                'class' => 'form-control',
                'cols' => "60",
                'rows' => "2"
                    //'value' => isset($entity->comments) ? $entity->comments : '',
            ));

//            $comments->addValidators(array(
//                new PresenceOf(array(
//                    'message' => 'Comment is required'
//                        ))
//            ));

            $this->add($comments);

            $status = new Select('status', $status, array(
                "using" => array("id", "status"),
                'useEmpty' => true,
                'emptyText' => 'Select',
                'emptyValue' => '',
                'class' => 'form-control',
                'value' => isset($entity->status_id) ? $entity->status_id : ''
                    //'onchange' => 'enquirySettings.changeStatusComments(this.value)'
            ));

//            $status->addValidators(array(
//                new PresenceOf(array(
//                    'message' => 'Status is required'
//                        ))
//            ));

            $this->add($status);
        }
    }

}
