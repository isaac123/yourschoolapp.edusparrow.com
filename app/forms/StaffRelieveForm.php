<?php
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;

use Phalcon\Validation\Validator\PresenceOf;

class StaffRelieveForm extends Form
{

    public function initialize()
    {
        $staffID = new Hidden('staffID');
        $this->add($staffID);
          
        
        $dateOfRelieving = new Text('dor', array(
            'placeholder' => 'Choose relieving date...',            
            'class' => 'form-control form_date'
        ));

        $dateOfRelieving->addValidators(array(
            new PresenceOf(array(
                'message' => 'The relieving date is required'
            ))
        ));

        $this->add($dateOfRelieving);

        
        $relievReason = new TextArea('relievReason', array(
            'placeholder' => 'Reason for Relieving...',
            'class' => 'form-control',
            'cols'=>"60",
            'rows'=>"5"
        ));

        $relievReason->addValidators(array(
            new PresenceOf(array(
                'message' => 'Add reason'
            ))
        ));

        $this->add($relievReason);
    }
}
