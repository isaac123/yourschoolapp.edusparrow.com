<?php
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Date;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

class LeaveRequestForm extends Form
{

    public function initialize()
    {
        
         $staffID = new Hidden('staffID');
          $this->add($staffID);
          
        $fromDate = new Text('fromDate', array(
            'placeholder' => 'Choose From Date...',            
            'class' => 'form-control frm_date'
        ));

        $fromDate->addValidators(array(
            new PresenceOf(array(
                'message' => 'The From Date is required'
            ))
        ));

        $this->add($fromDate);

        
        $ToDate = new Text('toDate', array(
            'placeholder' => 'Choose To Date...',            
            'class' => 'form-control to_date'
        ));

        $ToDate->addValidators(array(
            new PresenceOf(array(
                'message' => 'The To Date is required'
            ))
        ));

        $this->add($ToDate);

          
     $levType = new Select('levType', AttendanceSelectbox::find('allowed_leave_typ=1 and attendance_for = "staff"'), array(
            "using" => array("id", "attendancename"),
			 "useEmpty"=>true,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control'
        ));
        $levType->addValidators(array(
            new PresenceOf(array(
                'message' => 'Please select leave reason'
                    ))
        ));

        $this->add($levType);

        $levReason = new TextArea('levReason', array(
            'placeholder' => 'Reason for leave...',
            'class' => 'form-control',
            'cols'=>"60",
            'rows'=>"5"
        ));

        $levReason->addValidators(array(
            new PresenceOf(array(
                'message' => 'Add reason'
            ))
        ));

        $this->add($levReason);
    }
}
