<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;

class ApplicationForm extends Form {

    public function initialize($entity = null, $options = null) {

        
        
        if (isset($options['view']) && $options['view']) {
            $appno = new Text('application_no', array(
                'placeholder' => 'Application number',
                'class' => 'form-control',
                "value" => $entity->application_no,
                'title' => 'Application number',
            ));
            

        $appno->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Application number is required'
                    ))
        ));
        $this->add($appno);
        } 
//        else {
//            $year = explode('-', $cacdyr->name);
//            $appmax = Application::find('application_no LIKE "' . $year[0] . '%"')->getLast()->application_no;
//            $appno = new Text('application_no', array(
//                'placeholder' => 'Application number',
//                'class' => 'form-control',
//                'title' => 'Application number',
//                "value" => (($appmax) ? ($appmax + 1) : ($year[0] . '001'))
//            ));
//        }


//        $acaYr = new Select("academicYrId",
//                OrganizationalStructureValues::find('org_master_id = '.OrganizationalStructureMaster::findFirstByCycleNode(1)->id
//                .' and status IN ("C","N")'), array(
//            "using" => array("id", "name"),
//            'useEmpty' => True,
//            'emptyText' => 'Select',
//            'emptyValue' => '',
//            'class' => 'form-control',
//        ));
//        $acaYr->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The academic year is required'
//                    ))
//        ));
//
//        $this->add($acaYr);





        $name = new Text('studentName', array(
            'placeholder' => 'Student name...',
            'class' => 'form-control',
            'title' => 'Student name',
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Student name is required'
                    ))
        ));

        $this->add($name);

        $gender = new Select("gender", array(
            '1' => 'Female',
            '2' => 'Male'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'title' => 'gender',
            'emptyValue' => '',
            'class' => 'form-control'));

        $gender->addValidators(array(
            new PresenceOf(array(
                'message' => 'The gender is required'
                    ))
        ));

        $this->add($gender);

        $dob = new Text('dob', array(
            'class' => 'form-control',
            'title' => 'date of birth',
            'readonly'=>''
        ));

        $dob->addValidators(array(
            new PresenceOf(array(
                'message' => 'The date of birth is required'
                    ))
        ));

        $this->add($dob);

        $doj = new Hidden('doj', array(
            'title' => 'date of admission',
            'class' => 'form-control doj_date',
            'readonly'=>'',
            'value'=> date('d-m-Y')
        ));

        $doj->addValidators(array(
            new PresenceOf(array(
                'message' => 'The date of admission is required'
                    ))
        ));

        $this->add($doj);


        $phone = new Text('phone', array(
            'placeholder' => 'Phone',
            'title' => 'Phone number',
            'class' => 'form-control'
        ));

        $phone->addValidators(array(
            new PresenceOf(array(
                'message' => 'The phone is required'
                    ))
                /* new StringLength(array(
                  'min' => 10,
                  'messageMinimum' => 'Phone number is required'
                  )) */
        ));

        $this->add($phone);

        $email = new Text('email', array(
            'placeholder' => 'Email',
            'title' => 'Email',
            'class' => 'form-control'
        ));

//        $email->addValidators(array(
//            /* new PresenceOf(array(
//              'message' => 'The e-mail is required'
//              )), */
//            new Email(array(
//                'message' => 'Valid E-mail is required'
//                    ))
//        ));

        $this->add($email);


        $address = new TextArea('address1', array(
            'placeholder' => 'Address Line 1',
            'title' => 'Address Line 1',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2"
        ));

        $address->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Address Line 1 is required'
                    ))
        ));

        $this->add($address);

        $address2 = new TextArea('address2', array(
            'placeholder' => 'Address Line 2',
            'title' => 'Address Line 2',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2"
        ));

//        $address2->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Address Line 2 is required'
//                    ))
//        ));

        $this->add($address2);



        $state = new Text('state', array(
            'placeholder' => 'State...',
            'class' => 'form-control',
            'title' => 'State',
        ));

        $state->addValidators(array(
            new PresenceOf(array(
                'message' => 'The state is required'
                    ))
        ));

        $this->add($state);

        /*
          $country = new Text('country', array(
          'placeholder' => 'Country name...',
          'class' => 'form-control'
          ));

          $this->add($country);
         */
        $country = new Select('country', CountryMaster::find(), array(
            "using" => array("country_code", "country_name"),
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'title' => 'Country name',
            'class' => 'form-control',
        ));

        $country->addValidators(array(
            new PresenceOf(array(
                'message' => 'Country name is required'
                    ))
        ));

        $this->add($country);

        $pin = new Text('pin', array(
            'placeholder' => 'Pin code...',
            'title' => 'Pin code',
            'class' => 'form-control'
        ));

//        $pin->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Pin  code is required'
//                    ))
//        ));

        $this->add($pin);

//        $div = DivisionMaster::findFirst('divType = "student" and academicYrID=' . $cacdyr->id);
//        $divVal = new Select('divVal', DivisionValues::find('divID = ' . $div->id), array(
//            "using" => array("id", "classname"),
//            'useEmpty' => true,
//            'emptyText' => 'Select',
//            'emptyValue' => '',
//            'class' => 'form-control',
//            'onchange' => 'applicationSettings.loadSections(this.value)'
//        ));
//
//        $divVal->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'A valid '.$div->divname.' is required'
//                    ))
//        ));
//
//        $this->add($divVal);
        /*
          $jdivVal = new Select('jdivVal', DivisionValues::find('divID = ' . $div->id), array(
          "using" => array("id", "classname"),
          'useEmpty' => True,
          'emptyText' => 'Select',
          'emptyValue' => '0',
          'class' => 'form-control',
          ));

          $jdivVal->addValidators(array(
          new PresenceOf(array(
          'message' => 'Select any type'
          ))
          ));

          $this->add($jdivVal);
         */
        
    }

}
