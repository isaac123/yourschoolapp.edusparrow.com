<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class AddFeeForm extends Form {

    public function initialize($entity = null, $options = null) {
        $fee_Inst = $new_val_ids = '';
        if (isset($options['edit']) && $options['edit']) {
            $fee_Inst = FeeInstalments::findFirst('fee_master_id =' . $entity->id);
            $new_val_ids = ControllerBase::getNameForKeys($entity->node_id);
        }
        $cycle_node_master = OrganizationalStructureMaster::findfirst('cycle_node = 1');
        $cycle_node_values = OrganizationalStructureValues::find('org_master_id = ' . $cycle_node_master->id);
        if (!isset($options['edit']) && !$options['edit']) {

            $acaYr = new Select("academicYrId", $cycle_node_values, array(
                "using" => array("id", "name"),
                'useEmpty' => True,
                'emptyText' => 'Select',
                'emptyValue' => '',
                'class' => 'form-control',
                'onchange' => "spacetreeutilsSettings.loadSpaceTreeForFee(document.getElementById('academicYrId').value,'node_id','divVal')"
            ));
            $acaYr->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The ' . $cycle_node_master->name . ' academic year is required'
                        ))
            ));

            $this->add($acaYr);
        }

//        $feenames = FeeMaster::find(array('columns' => 'DISTINCT fee_name'));
//        if (count($feenames) > 0) {
//            $name = new Select('feeName', FeeMaster::find(array('columns' => 'DISTINCT fee_name')), array(
//                "using" => array("fee_name", "fee_name"),
//                'useEmpty' => true,
//                'emptyText' => 'Select',
//                'emptyValue' => '',
//                'class' => 'form-control',
//                'value' => $entity->fee_name ? $entity->fee_name : ''
//            ));
//        } else {
//            $name = new Text('feeName', array(
//                'placeholder' => 'Fee name...',
//                'class' => 'form-control',
//                'value' => $entity->fee_name ? $entity->fee_name : ''
//            ));
//        }
//
//        $name->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Fee name is required'
//                    ))
//        ));
//
//        $this->add($name);

        $insname = new Text('Instalment_name[]', array(
            'placeholder' => 'Instalment name...',
            'class' => 'form-control',
            'value' => $fee_Inst ? $fee_Inst->name : ''
        ));
        $insname->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Instalment name is required'
                    ))
        ));

        $this->add($insname);
        $dob = new Text('fee-from[]', array(
            'class' => 'form-control form_date',
            'value' => $fee_Inst ? date('d-m-Y', $fee_Inst->fee_collection_start) : date('d-m-Y', time())
        ));

        $dob->addValidators(array(
            new PresenceOf(array(
                'message' => 'Fee Collection start date is required'
                    ))
        ));

        $this->add($dob);

        $doj = new Text('fee-to[]', array(
            'class' => 'form-control form_date',
            'value' => $fee_Inst ? date('d-m-Y', $fee_Inst->fee_collection_end) : date('d-m-Y', time())
        ));

        $doj->addValidators(array(
            new PresenceOf(array(
                'message' => 'Fee Collection end date'
                    ))
        ));
        $this->add($doj);



        $state = new Text('amount[]', array(
            'placeholder' => '0.00',
            'class' => 'form-control feeamount',
            'value' => $fee_Inst ? $fee_Inst->fee_amount : ''
        ));

        $state->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Amount is required'
                    )), new RegexValidator(array(
                //'pattern' => '/^(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})$/',
//                    'pattern' =>     '/^[1-9](?:\d{0,2})+([,]*[0-9]+)*(?:\.\d{0,2}[1-9])?|0?\.\d{0,2}[1-9]|0$/',
                'pattern' => '/^[1-9](?:\d{0,2})+([,]\d{3})*(?:\.\d{0,1}[1-9])?|0?\.\d{0,1}[1-9]$/',
                'message' => 'The amount is invalid'
                    ))
        ));
        $this->add($state);


        $pamount = new Text('penalty-amount[]', array(
            'placeholder' => '0.00',
            'class' => 'form-control',
            'value' => $fee_Inst ? $fee_Inst->penalty_amount : '0.00'
        ));

        $pamount->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Penalty Amount is required'
                    )), new RegexValidator(array(
                //'pattern' => '/^(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})$/',
//                    'pattern' =>     '/^[1-9](?:\d{0,2})+([,]*[0-9]+)*(?:\.\d{0,2}[1-9])?|0?\.\d{0,2}[1-9]|0$/',
                'pattern' => '/^[0-9](?:\d{0,2})+([,]\d{3})*(?:\.\d{0,1}[0-9])?|0?\.\d{0,1}[0-9]$/',
                'message' => 'The Penalty amount is invalid'
                    ))
        ));
        $this->add($pamount);

        $divVal = new Text('divVal', array(
            'placeholder' => 'Choose The Node',
            'class' => 'form-control',
            'disabled' => 'disabled',
            'onclick' => "spacetreeutilsSettings.loadSpaceTreeForSubject(document.getElementById('academicYrId').value,'node_id','divVal')",
            'value' => ($new_val_ids != '') ? implode(':', $new_val_ids) : ''
        ));

        $this->add($divVal);


        $advamount = new Numeric('advamnt', array(
            'placeholder' => '0.00',
            'class' => 'form-control',
        ));

//        $advamount->addValidators(array(
//            new RegexValidator(array(
//                'pattern' => '/^[0-9](?:\d{0,2})+([,]\d{3})*(?:\.\d{0,1}[0-9])?|0?\.\d{0,1}[0-9]$/',
//                'message' => 'The value is invalid'
//                    ))
//        ));
        $this->add($advamount);

        $valuetyp = new Select('valuetyp', array(
            '1' => 'Percentage',
            '2' => 'Total value'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control'
        ));

        $this->add($valuetyp);
    }

}
