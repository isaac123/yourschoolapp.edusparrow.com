<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class TimelineForm extends Form {

    public function initialize($entity = null, $options = null) {
        $activity_type = new Select('activity_type', array(
            '1' => 'Assignment',
            '2' => 'Class Test',
            '3' => 'HomeWork',
            '4' => 'Comments',
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
            'onchange' => "timelineSettings.loadActivityType(this.value)",
        ));
        $activity_type->addValidators(array(
            new PresenceOf(array(
                'message' => 'Type is required'
                    ))
        ));

        $this->add($activity_type);
        
        $comments = new TextArea('comments', array(
            'placeholder' => '',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->comments) ? $entity->comments : ''
        ));
        $comments->addValidators(array(
            new PresenceOf(array(
                'message' => 'comments is required'
                    ))
        ));
        $this->add($comments);
    }

}
