<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;

class AddFeetriggerForm extends Form {

    public function initialize($entity = null, $options = null) {

        $name = new Text('feetrgName', array(
            'placeholder' => 'Fee Trigger name...',
            'class' => 'form-control'
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Fee Trigger name is required'
                    ))
        ));

        $this->add($name);

        $active = new Select("isactive", array(
            '1' => 'Yes',
            '0' => 'No'
                ), array(
            'useEmpty' => false,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control'));

        $this->add($active);

        $feetrgController = new Text('feetrgController', array(
            'placeholder' => 'Fee Trigger Controller name...',
            'class' => 'form-control'
        ));

        $feetrgController->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Fee Trigger Controller name is required'
                    ))
        ));

        $this->add($feetrgController);

        $feetrgAction = new Text('feetrgAction', array(
            'placeholder' => 'Fee Trigger Action name...',
            'class' => 'form-control'
        ));

        $feetrgAction->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Fee Trigger Action name is required'
                    ))
        ));

        $this->add($feetrgController);
    }

}
