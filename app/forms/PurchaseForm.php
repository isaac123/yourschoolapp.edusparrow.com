<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class PurchaseForm extends Form {

    public function initialize($entity = null, $options = null) {

        $vendor_name = new Text('vendor_name', array(
            'placeholder' => 'Vendor Name',
            'class' => 'form-control',
            'value' => isset($entity->vendor_name) ? $entity->vendor_name : ''
        ));
        $vendor_name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Vendor Name is required'
                    ))
        ));

        $this->add($vendor_name);
        
         $comments = new TextArea('comments', array(
            'placeholder' => 'Enter the Comments',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->comments) ? $entity->comments : ''
        ));
//        $comments->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'Comments is required'
//                    ))
//        ));
        $this->add($comments);
        
    }

}
