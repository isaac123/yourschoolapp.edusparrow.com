<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class StaffPersonalForm extends Form {

    public function initialize($entity = null, $options = null) {
   
        $cacdyr =  ''; //AcademicYearMaster::findFirstByStatus('c');
        $readonly = '';
        if (isset($options['edit']) && $options['edit']) {
		
		 /*if($entity->id)
		 {*/
            $staff_add_info = count(StaffInfo::find('id =' . $entity->id)) ?
                    StaffInfo::findFirstById($entity->id) :
                    new StaffInfo();
            $readonly = 'readonly';
        }		
       
        $fieldArr = $fieldArr2 = array();
        $hideOrShowFields = StaffProfileSettings::find("hide_or_show = 1 and parent_id!=0");
        foreach ($hideOrShowFields as $hideOrShow) {
            $fieldArr[$hideOrShow->id] = $hideOrShow->form_name;
        }
        $hideOrShowFields1 = StaffProfileSettings::find("mandatory = 1 and parent_id!=0");
        foreach ($hideOrShowFields1 as $hideOrShow1) {
            $fieldArr2[$hideOrShow1->id] = $hideOrShow1->form_name;
        }
       
		
		/*$name = new Text('staffName', array(
            'placeholder' => 'Staff name...',
            'class' => 'form-control',
            'value' => isset($entity->Staff_Name) ? $entity->Staff_Name : ''
        ));
		
		$name->setLabel('Staff Name');
		
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Staff name is required'
                    ))
        ));

        $this->add($name);*/
		
		
     /*   $category = new Select("category", array(
            '1' => 'Teaching',
            '2' => 'Non-Teaching'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control',
            'value' => isset($staff_add_info->catg_id) ? $staff_add_info->catg_id : ''
        ));
		
		
		
        if (in_array("category", $fieldArr2)) {
            $category->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The category is required'
                        ))
            ));
        }
        if (in_array("category", $fieldArr)) {
            $this->add($category);
			$category->setLabel('Category');
        }

        $doa = new Date('doa', array(
            'class' => 'form-control',
            'value' => isset($entity->Date_of_Joining) ? date('Y-m-d', $entity->Date_of_Joining) : ''
        ));
		
		$doa->setLabel('Date Of Admission');
		
        $doa->addValidators(array(
            new PresenceOf(array(
                'message' => 'The date of Apointment is required'
                    ))
        ));

        $this->add($doa);


        

        $div = DivisionMaster::findFirst('divType = "staff" and academicYrID=' . $cacdyr->id);
        $divVal = new Select('divVal', DivisionValues::find('divID = ' . $div->id), array(
            "using" => array("id", "classname"),
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control',
            'value' => isset($entity->Department) ? $entity->Department : ''
        ));
		
		 $divVal->setLabel('Department');
		
        $divVal->addValidators(array(
            new PresenceOf(array(
                'message' => 'Select any type'
                    ))
        ));

        $this->add($divVal);*/


        /**
         *  
         * Staff Personal Details
         * 
         * */
        $dob = new Text('dob', array(
            'class' => 'form-control',
            'value' => isset($entity->Date_of_Birth) ? date('d-m-Y', $entity->Date_of_Birth) : ''
        ));
		
		$dob->setLabel('Date Of Birth');
		
        $dob->addValidators(array(
            new PresenceOf(array(
                'message' => 'The date of Birth is required'
                    ))
        ));

        $this->add($dob);
		
		$phone = new Text('phone', array(
            'placeholder' => 'Phone',
            'class' => 'form-control',
            'value' => isset($entity->Phone) ? $entity->Phone : ''
        ));
		
		$phone->setLabel('Phone No');
		
        $phone->addValidators(array(
           
             new PresenceOf(array(
              'message' => 'The phone is required'
              )),
             new RegexValidator(array(
                      'pattern' => '/^[0-9]*$/',
                'message' => 'Phone number is invalid'
                ))
           /* new StringLength(array(
                'min' => 10,
                'messageMinimum' => 'Invalid phone number'
                    ))*/
        ));

        $this->add($phone);

        $email = new Text('email', array(
            'placeholder' => 'Email',
            'class' => 'form-control',
            'value' => isset($entity->Email) ? $entity->Email : ''
        ));
		
		$email->setLabel('Email ');
		
        $email->addValidators(array(
            /* new PresenceOf(array(
              'message' => 'The e-mail is required'
              )), */
           new Email(array(
                'message' => 'The e-mail is not valid'
                    ))
        ));

        $this->add($email);

       /* $gender = new Select("gender", array(
            '1' => 'Female',
            '2' => 'Male'
                ), array(
            'useEmpty' => false,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control',
            'value' => isset($entity->gender) ? $entity->gender : ''));
            $gender->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The gender is required'
                        ))
            ));
			
			$gender->setLabel('Gender');
			
            $this->add($gender);*/
			
        $address = new TextArea('address1', array(
            'placeholder' => 'Address Line 1',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->Address1) ? $entity->Address1 : ''
        ));
		
		$address->setLabel('Address1');
     
        $address->addValidators(array(
                 new PresenceOf(array(
                  'message' => 'The Address is required'
                  )) 
        ));

       $this->add($address);

        $address2 = new TextArea('address2', array(
            'placeholder' => 'Address Line 2',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->Address2) ? $entity->Address2 : ''
        ));
		
		$address2->setLabel('Address2');
		
        $this->add($address2);

        



 
        $state = new Text('state', array(
            'placeholder' => 'State...',
            'class' => 'form-control',
            'value' => isset($entity->State) ? $entity->State : ''
        ));

		$state->setLabel('State ');
		
        $state->addValidators(array(
                 new PresenceOf(array(
                  'message' => 'The state is required'
                  )) 
        ));

        $this->add($state);


        $country = new Select('country', CountryMaster::find(), array(
            "using" => array("country_code", "country_name"),
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
            'value' => isset($entity->Country) ? $entity->Country : ''
        ));
		
		$country->setLabel('Country ');
		
        $country->addValidators(array(
            new PresenceOf(array(
                'message' => 'Country name is required'
                    ))
        ));

        $this->add($country);


        $pin = new Text('pin', array(
            'placeholder' => 'Pin code...',
            'class' => 'form-control',
            'value' => isset($entity->Pin) ? $entity->Pin : ''
        ));
		
		$pin->setLabel('Pin Code ');
		
        $pin->addValidators(array(
                  new PresenceOf(array(
                  'message' => 'The Pin code is required'
                  )),
                    new RegexValidator(array(
                      'pattern' => '/^[0-9]*$/',
                'message' => 'The Pin code is invalid'
                ))
       ));

        $this->add($pin);


        $qualification = new Text('qualification', array(
            'placeholder' => 'Qualification...',
            'class' => 'form-control',
            'value' => isset($staff_add_info->qualification) ? $staff_add_info->qualification : ''
        ));
		
		$qualification->setLabel('Qualification');
		
        if (in_array("qualification", $fieldArr2)) {
            $qualification->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Qualification is required'
                        ))
            ));
        }
        if (in_array("qualification", $fieldArr)) {
            $this->add($qualification);
        }
		
		
        $photo = new File("photo", array(
            'placeholder' => 'photo',
            'value' => isset($staff_add_info->photo) ? $staff_add_info->photo : ''
        ));

        if (in_array("photo", $fieldArr)) {
            $this->add($photo);
			$photo->setLabel('Photo');
        }

        $address_proof1 = new File("address_proof1", array(
            'placeholder' => 'Adress Proof 1',
            'value' => isset($staff_add_info->address_proof1) ? $staff_add_info->address_proof1 : ''
        ));

        if (in_array("address_proof1", $fieldArr)) {
            $this->add($address_proof1);
			
			$address_proof1->setLabel('Address Proof 1');
        }

        $address_proof2 = new File("address_proof2", array(
            'placeholder' => 'Address Proof 2',
            'value' => isset($staff_add_info->address_proof2) ? $staff_add_info->address_proof2 : ''
        ));

        if (in_array("address_proof2", $fieldArr)) {
            $this->add($address_proof2);
			$address_proof2->setLabel('Address Proof 2');
        }

        $address_proof3 = new File("address_proof3", array(
            'placeholder' => 'Address Proof 3',
            'value' => isset($staff_add_info->address_proof3) ? $staff_add_info->address_proof3 : ''
        ));

        if (in_array("address_proof3", $fieldArr)) {
            $this->add($address_proof3);
			$address_proof3->setLabel('Address Proof 3');
        }
	/*  }
          
          print_r($this);
         exit;
          */
	// }
         
         
       
    }

}
