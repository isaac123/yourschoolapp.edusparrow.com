<?php
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;

class AnnouncementForm extends Form {

    public function initialize($entity = null, $options = null) {

        $announcedt = new Text('announcementdate', array(
            'placeholder' => 'Annoucement Date...',
            'class' => 'form-control form-control-inline input-medium default-date-picker'
        ));

        $announcedt->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Annoucement Date is required'
                    ))
        ));
        $this->add($announcedt);

      

        $Announcementmsg = new TextArea('announcementmsg', array(
            'placeholder' => 'Announcement Title...',
            'class' => 'form-control'
        ));

        $Announcementmsg->addValidators(array(
                new PresenceOf(array(
                  'message' => 'Announcement Title is required'
                  )) 
        ));

        $this->add($Announcementmsg);
        

        $announcementcmts = new TextArea('announcementcmts', array(
            'placeholder' => 'Announcement Message...',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2"
        ));

        $announcementcmts->addValidators(array(
                 new PresenceOf(array(
                  'message' => 'Announcement Message is required'
                  )) 
        ));

        $this->add($announcementcmts);

       

     
    }

}
