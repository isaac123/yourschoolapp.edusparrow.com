<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class AddSubjectForm extends Form {

    public function initialize() {

        $name = new Text('subj', array(
            'placeholder' => 'Subject Name',
            'class' => 'form-control'
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The name is required'
                    )),
            new StringLength(array(
                'messageMaximum' => 'The name is too Long',
                'max' => 50
                    ))
        ));

        $this->add($name);

        $classID = new Hidden('classID');
        $this->add($classID);

        $code = new Text('code', array(
            'placeholder' => '3 Character Subject Code',
            'class' => 'form-control'
        ));

//        $code->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'Subject Code is required'
//                    )),
//            new StringLength(array(
//                'messageMaximum' => 'Subject Code Should Be Of 3 Characters',
//                'max' => 3,
//                'min' => 3
//                    ))
//        ));

        $this->add($code);

        $attr = array(
            'name' => 'groupName'
        );

        $radio1 = new \Phalcon\Forms\Element\Radio("radio1", $attr);
        $radio2 = new \Phalcon\Forms\Element\Radio("radio2", $attr);

        $this->add($radio1);
        $this->add($radio2);
    }

}
