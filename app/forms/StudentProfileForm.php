<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class StudentProfileForm extends Form {

    public function initialize($entity = null, $options = null) {
        $fieldArr = $fieldArr2 = array();
        $hideOrShowFields = StudentProfileSettings::find("hide_or_show = 1 and parent_id!=0");
        foreach ($hideOrShowFields as $hideOrShow) {
            $fieldArr[$hideOrShow->id] = $hideOrShow->form_name;
        }
        $hideOrShowFields1 = StudentProfileSettings::find("mandatory = 1 and parent_id!=0");
        foreach ($hideOrShowFields1 as $hideOrShow1) {
            $fieldArr2[$hideOrShow1->id] = $hideOrShow1->form_name;
        }
//print_r(in_array("rollno", $fieldArr2));

        $appno = new Text('application_no', array(
            'placeholder' => 'Application number',
            'class' => 'form-control',
            'readonly' => '',
            'value' => $entity->application_no
        ));

        $appno->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Application number is required'
                    ))
        ));
        $this->add($appno);


        $admisson_no = new Text('admission_no', array(
            'placeholder' => 'Admission number',
            'class' => 'form-control',
            'readonly' => '',
            'value' => $entity->Admission_no
        ));

        $admisson_no->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Admission number is required'
                    ))
        ));
        $this->add($admisson_no);

       
        $doj = new Text('doj', array(
            'class' => 'form-control form_date',
            'value' => isset($entity->Date_of_Joining) ? date('d-m-Y', $entity->Date_of_Joining) : ''
        ));

        $doj->addValidators(array(
            new PresenceOf(array(
                'message' => 'The date of joining is required'
                    ))
        ));

        $this->add($doj);

        $transport = new Select("transport", array(
            '1' => 'No',
            '2' => 'Yes'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control',
            'value' => isset($entity->transport) ? $entity->transport : ''
        ));

        if (in_array("transport", $fieldArr2)) {
            $transport->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The transport facility value is required'
                        ))
            ));
        }
        if (in_array("transport", $fieldArr)) {
            $this->add($transport);
        }

//        $rollno = new Text('rollno', array(
//            'class' => 'form-control',
//            'readonly' => '',
//            'value' => isset($entity->rollno) ? $entity->rollno : ''
//        ));
//$this->add($rollno);


        /**
         *  
         * Student Personal Details
         * 
         * */
        $name = new Text('studentName', array(
            'placeholder' => 'Student name...',
            'class' => 'form-control',
            'value' => isset($entity->Student_Name) ? $entity->Student_Name : ''
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Student name is required'
                    )),
            new RegexValidator(array(
                      'pattern' => '/^[a-zA-Z ]*$/',
                'message' => 'The Student name is invalid'
                    ))
        ));

        $this->add($name);

        $gender = new Select("gender", array(
            '1' => 'Female',
            '2' => 'Male'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
            'value' => isset($entity->Gender) ? $entity->Gender : ''
        ));

        $gender->addValidators(array(
            new PresenceOf(array(
                'message' => 'The gender is required'
                    ))
        ));

        $this->add($gender);

        $dob = new Text('dob', array(
            'class' => 'form-control',
            'value' => isset($entity->Date_of_Birth) ? date('d-m-Y', $entity->Date_of_Birth) : ''
        ));

        $dob->addValidators(array(
            new PresenceOf(array(
                'message' => 'The date of birth is required'
                    ))
        ));

        $this->add($dob);

        $pob = new Text('pob', array(
            'placeholder' => 'Place of Birth...',
            'class' => 'form-control',
            'value' => isset($entity->place_of_birth) ? $entity->place_of_birth : ''
        ));


        if (in_array("pob", $fieldArr2)) {
            $pob->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Place of Birth is required'
                        ))
            ));
        }

        if (in_array("pob", $fieldArr)) {
            $this->add($pob);
        }


        $phone = new Text('phone', array(
            'placeholder' => 'Phone',
            'class' => 'form-control',
            'value' => isset($entity->Phone) ? $entity->Phone : ''
        ));
        $phone->addValidators(array(
            new PresenceOf(array(
                'message' => 'The phone is required'
                    )),
                     new RegexValidator(array(
                      'pattern' => '/^[0-9]*$/',
                'message' => 'The Phone is invalid'
                    ))
           /* new StringLength(array(
                'min' => 10,
                'messageMinimum' => 'Invalid phone number'
                    ))*/
        ));
        $this->add($phone);




        $email = new Text('email', array(
            'placeholder' => 'Email',
            'class' => 'form-control',
            'value' => isset($entity->Email) ? $entity->Email : ''
        ));

        $email->addValidators(array(
            /*new PresenceOf(array(
                'message' => 'The e-mail is required'
                    )),*/
            new Email(array(
                'message' => 'The e-mail is required or not valid'
                    ))
        ));

        $this->add($email);


        $address = new TextArea('address1', array(
            'placeholder' => 'Address Line 1',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->Address1) ? $entity->Address1 : ''
        ));

        $address->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Address is required'
                    ))
        ));

        $this->add($address);

        $address2 = new TextArea('address2', array(
            'placeholder' => 'Address Line 2',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->Address2) ? $entity->Address2 : ''
        ));

       /* $address2->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Address is required'
                    ))
        ));*/

        $this->add($address2);



        $state = new Text('state', array(
            'placeholder' => 'State...',
            'class' => 'form-control',
            'value' => isset($entity->State) ? $entity->State : ''
        ));

        $state->addValidators(array(
            new PresenceOf(array(
                'message' => 'The state is required'
                    ))
        ));

        $this->add($state);


        $country = new Select('country', CountryMaster::find(), array(
            "using" => array("country_code", "country_name"),
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control',
            'value' => isset($entity->Country) ? $entity->Country : ''
        ));

        $country->addValidators(array(
            new PresenceOf(array(
                'message' => 'Country name is required'
                    ))
        ));

        $this->add($country);


        $pin = new Text('pin', array(
            'placeholder' => 'Pin code...',
            'class' => 'form-control',
            'value' => isset($entity->Pin) ? $entity->Pin : ''
        ));

//        $pin->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'The Pin  code is required'
//                    ))
//        ));

        $this->add($pin);


        $nationality = new Select('nationality', CountryMaster::find(), array(
            "using" => array("country_code", "country_name"),
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control',
            'value' => isset($entity->nationality) ? $entity->nationality : ''
        ));

        if (in_array("nationality", $fieldArr2)) {
            $nationality->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Nationality is required'
                        ))
            ));
        }

        if (in_array("nationality", $fieldArr)) {
            $this->add($nationality);
        }

        $religion = new Text('religion', array(
            'placeholder' => 'Religion...',
            'class' => 'form-control',
            'value' => isset($entity->religion) ? $entity->religion : ''
        ));

        if (in_array("religion", $fieldArr2)) {

            $religion->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Religion is required'
                        ))
            ));
        }

        if (in_array("religion", $fieldArr)) {
            $this->add($religion);
        }

        $caste_cat = new Text('caste_category', array(
            'placeholder' => 'Caste Category...',
            'class' => 'form-control',
            'value' => isset($entity->caste_category) ? $entity->caste_category : ''
        ));

        if (in_array("caste_category", $fieldArr2)) {

            $caste_cat->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Caste Category is required'
                        ))
            ));
        }

        if (in_array("caste_category", $fieldArr)) {
            $this->add($caste_cat);
        }

        $caste = new Text('caste', array(
            'placeholder' => 'Caste...',
            'class' => 'form-control',
            'value' => isset($entity->caste) ? $entity->caste : ''
        ));

        if (in_array("caste", $fieldArr2)) {

            $caste->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Caste is required'
                        ))
            ));
        }

        if (in_array("caste", $fieldArr)) {
            $this->add($caste);
        }

        $language = new Text('language', array(
            'placeholder' => 'First Language...',
            'class' => 'form-control',
            'value' => isset($entity->first_language) ? $entity->first_language : ''
        ));

        if (in_array("language", $fieldArr2)) {

            $language->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The First Language is required'
                        ))
            ));
        }

        if (in_array("language", $fieldArr)) {
            $this->add($language);
        }


        $photo = new File("photo", array(
            'placeholder' => 'photo'
        ));

        if (in_array("photo", $fieldArr)) {
            $this->add($photo);
        }

        $fam_photo = new File("family_photo", array(
            'placeholder' => 'Family Photo'
        ));

        if (in_array("family_photo", $fieldArr)) {
            $this->add($fam_photo);
        }

         $other_photos = new File("other_photos", array(
            'placeholder' => 'photo',
             'multiple' =>'',
             'name'=>'other_photos[]'
        ));
         
          if (in_array("other_photos", $fieldArr)) {
            $this->add($other_photos);
        }


        /**
         *  
         * Student Medical Details
         * 
         * */
        $blood_grp = new Select("blood_grp", array(
            'A-' => 'A-',
            'A+' => 'A+',
            'A1+' => 'A1+',
            'A1-' => 'A1-',
            'B-' => 'B-',
            'B+' => 'B+',
            'AB+' => 'AB+',
            'AB-' => 'AB-',
            'O-' => 'O-',
            'O+' => 'O+',
            'A1B+' => 'A1B+',
            'A1B-' => 'A1B-',
            'A2+' => 'A2+',
            'A2-' => 'A2-',
            'A2B+' => 'A2B+',
            'A2B-' => 'A2B-'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control',
            'value' => isset($entity->blood_group) ? $entity->blood_group : ''
        ));
        if (in_array("blood_grp", $fieldArr2)) {
            $blood_grp->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Blood Group is required'
                        ))
            ));
        }
        if (in_array("blood_grp", $fieldArr)) {
            $this->add($blood_grp);
        }

        $height = new Text('height', array(
            'placeholder' => '',
            'class' => 'form-control',
            'value' => isset($entity->height) ? $entity->height : ''
        ));
        if (in_array("height", $fieldArr2)) {

            $height->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Height is required'
                        )),
                new StringLength(array(
                    'max' => 3,
                    'messageMaxmimum' => 'Invalid Height'
                        )),
                 new RegexValidator(array(
                      'pattern' => '/^[0-9]*$/',
                'message' => 'The height is invalid'
                    ))
            ));
        }
        if (in_array("height", $fieldArr)) {
            $this->add($height);
        }



        $weight = new Text('weight', array(
            'placeholder' => '',
            'class' => 'form-control',
            'value' => isset($entity->weight) ? $entity->weight : ''
        ));
        if (in_array("weight", $fieldArr2)) {
            $weight->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The weight is required'
                        )),
                new StringLength(array(
                    'max' => 3,
                    'messageMaxmimum' => 'Invalid weight'
                        )),
                 new RegexValidator(array(
                      'pattern' => '/^[0-9]*$/',
                'message' => 'The weight is invalid'
                    ))
            ));
        }
        if (in_array("weight", $fieldArr)) {
            $this->add($weight);
        }

        $drugallergic = new TextArea('drugallergic', array(
            'placeholder' => 'Allergic to any drug? ',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'value' => isset($entity->allergic) ? $entity->allergic : ''
        ));

        if (in_array("drugallergic", $fieldArr2)) {
            $drugallergic->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill drug allergic'
                        ))
            ));
        }
        if (in_array("drugallergic", $fieldArr)) {
            $this->add($drugallergic);
        }


        $chronicill = new TextArea('chronicill', array(
            'placeholder' => 'Chronic illness if any',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'value' => isset($entity->chronic) ? $entity->chronic : ''
        ));

        if (in_array("chronicill", $fieldArr2)) {
            $chronicill->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Chronic illness '
                        ))
            ));
        }
        if (in_array("chronicill", $fieldArr)) {
            $this->add($chronicill);
        }

        $docDet = new TextArea('docDet', array(
            'placeholder' => 'Family doctor details',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'value' => isset($entity->family_doctor) ? $entity->family_doctor : ''
        ));

        if (in_array("docDet", $fieldArr2)) {
            $docDet->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Family doctor details is required'
                        ))
            ));
        }
        if (in_array("docDet", $fieldArr)) {
            $this->add($docDet);
        }




        /**
         *  
         * Student Previous School Details
         * 
         * */
        $prevSchoolName = new Text('prevSchoolName', array(
            'placeholder' => 'Previous School Name...',
            'class' => 'form-control',
            'value' => isset($entity->previous_school_name) ? $entity->previous_school_name : ''
        ));

        if (in_array("prevSchoolName", $fieldArr2)) {
            $prevSchoolName->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Previous School Name is required'
                        ))
            ));
        }
        if (in_array("prevSchoolName", $fieldArr)) {
            $this->add($prevSchoolName);
        }

        $prevState = new Text('prevState', array(
            'placeholder' => 'State...',
            'class' => 'form-control',
            'value' => isset($entity->previous_school_state) ? $entity->previous_school_state : ''
        ));
        if (in_array("prevState", $fieldArr2)) {
            $prevState->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Previous School State is required'
                        ))
            ));
        }
        if (in_array("prevState", $fieldArr)) {
            $this->add($prevState);
        }


        $prevcountry = new Select('pprevcountry', CountryMaster::find(), array(
            "using" => array("country_code", "country_name"),
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control',
            'value' => isset($entity->previous_school_country) ? $entity->previous_school_country : ''
        ));

        if (in_array("pprevcountry", $fieldArr2)) {
            $prevcountry->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Previous School country is required'
                        ))
            ));
        }
        if (in_array("pprevcountry", $fieldArr)) {
            $this->add($prevcountry);
        }


        $attfrom = new Text('attfrom', array(
            'class' => 'form-control frm_date',
            'value' => isset($entity->attended_from) ? date('d-m-Y', $entity->attended_from) : ''
        ));

        if (in_array("attfrom", $fieldArr2)) {
            $attfrom->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Attended from is required'
                        ))
            ));
        }
        if (in_array("attfrom", $fieldArr)) {
            $this->add($attfrom);
        }


        $attto = new Text('attto', array(
            'class' => 'form-control to_date',
            'value' => isset($entity->attended_to) ? date('d-m-Y', $entity->attended_to) : ''
        ));

        if (in_array("attto", $fieldArr2)) {
            $attto->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Attended to is required'
                        ))
            ));
        }
        if (in_array("attto", $fieldArr)) {
            $this->add($attto);
        }



        $achievements = new TextArea('achievements', array(
            'placeholder' => 'Achievements',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'value' => isset($entity->achievements) ? $entity->achievements : ''
        ));

        if (in_array("achievements", $fieldArr2)) {

            $achievements->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Achievements'
                        ))
            ));
        }
        if (in_array("achievements", $fieldArr)) {
            $this->add($achievements);
        }




        $comments = new TextArea('comments', array(
            'placeholder' => 'Comments',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'value' => isset($entity->comments) ? $entity->comments : ''
        ));

        if (in_array("comments", $fieldArr2)) {

            $comments->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Comments '
                        ))
            ));
        }
        if (in_array("comments", $fieldArr)) {
            $this->add($comments);
        }




        $odet = new TextArea('odet', array(
            'placeholder' => 'Other details',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'value' => isset($entity->other_details) ? $entity->other_details : ''
        ));

        if (in_array("odet", $fieldArr2)) {

            $odet->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Other details '
                        ))
            ));
        }
        if (in_array("odet", $fieldArr)) {
            $this->add($odet);
        }


        /**
         *  
         * Student Parent Details
         * 
         * */
        $fname = new Text('fname', array(
            'placeholder' => 'Father Name...',
            'class' => 'form-control',
            'value' => isset($entity->father_name) ? $entity->father_name : ''
        ));

        if (in_array("fname", $fieldArr2)) {

            $fname->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Father name is required'
                        )),
                 new RegexValidator(array(
                      'pattern' => '/^[a-zA-Z ]*$/',
                'message' => 'The Father name is invalid'
                    ))
            ));
        }

        if (in_array("fname", $fieldArr)) {
            $this->add($fname);
        }

        $foccup = new Text('foccup', array(
            'placeholder' => 'Father Occupation...',
            'class' => 'form-control',
            'value' => isset($entity->f_occupation) ? $entity->f_occupation : ''
        ));

        if (in_array("foccup", $fieldArr2)) {
            $foccup->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Father Occupation is required'
                        ))
            ));
        }

        if (in_array("foccup", $fieldArr)) {
            $this->add($foccup);
        }



        $fdesign = new Text('fdesign', array(
            'placeholder' => 'Father Designation...',
            'class' => 'form-control',
            'value' => isset($entity->f_designation) ? $entity->f_designation : ''
        ));

        if (in_array("fdesign", $fieldArr2)) {
            $fdesign->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Father Designation is required'
                        ))
            ));
        }

        if (in_array("fdesign", $fieldArr)) {
            $this->add($fdesign);
        }

        $fphone = new Text('fphone', array(
            'placeholder' => 'Phone',
            'class' => 'form-control',
            'value' => isset($entity->f_phone_no) ? $entity->f_phone_no : ''
        ));
        if (in_array("fphone", $fieldArr2)) {
            $fphone->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Father\'s phone number is required'
                        )), new RegexValidator(array(
                      'pattern' => '/^[0-9]*$/',
                'message' => 'The Father phone no is invalid'
                    ))/*,
                new StringLength(array(
                    'min' => 10,
                    'messageMinimum' => 'Invalid Father\'s phone number'
                        ))*/
            ));
        }

        if (in_array("fphone", $fieldArr)) {
            $this->add($fphone);
        }

        $fcontact = new Select("fcontact", array(
            '1' => 'Primary',
            '2' => 'Secondary'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control',
            'value' => isset($entity->f_phone_no_status) ? $entity->f_phone_no_status : ''
        ));

        if (in_array("fcontact", $fieldArr2)) {
            $fcontact->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Father\'s Contact Priority number is required'
                        )),
            ));
        }

        if (in_array("fcontact", $fieldArr)) {
            $this->add($fcontact);
        }

        $mname = new Text('mname', array(
            'placeholder' => 'Mother Name...',
            'class' => 'form-control',
            'value' => isset($entity->mother_name) ? $entity->mother_name : ''
        ));
        if (in_array("mname", $fieldArr2)) {

            $mname->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Mother name is required'
                        )),
                 new RegexValidator(array(
                      'pattern' => '/^[a-zA-Z ]*$/',
                'message' => 'The Mother name is invalid'
                    ))
            ));
        }

        if (in_array("mname", $fieldArr)) {
            $this->add($mname);
        }



        $moccup = new Text('moccup', array(
            'placeholder' => 'Mother Occupation...',
            'class' => 'form-control',
            'value' => isset($entity->m_occupation) ? $entity->m_occupation : ''
        ));

        if (in_array("moccup", $fieldArr2)) {
            $moccup->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The mother Occupation is required'
                        ))
            ));
        }

        if (in_array("moccup", $fieldArr)) {
            $this->add($moccup);
        }



        $mdesign = new Text('mdesign', array(
            'placeholder' => 'Mother Designation...',
            'class' => 'form-control',
            'value' => isset($entity->m_designation) ? $entity->m_designation : ''
        ));

        if (in_array("mdesign", $fieldArr2)) {
            $mdesign->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Mother Designation is required'
                        ))
            ));
        }

        if (in_array("mdesign", $fieldArr)) {
            $this->add($mdesign);
        }

        $mphone = new Text('mphone', array(
            'placeholder' => 'Phone',
            'class' => 'form-control',
            'value' => isset($entity->m_phone_no) ? $entity->m_phone_no : ''
        ));

        if (in_array("mphone", $fieldArr2)) {
            $mphone->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Mother\'s phone number is required'
                        )),
                 new RegexValidator(array(
                      'pattern' => '/^[0-9]*$/',
                'message' => 'The Mother Phone no is invalid'
                    ))/*,
                new StringLength(array(
                    'min' => 10,
                    'messageMinimum' => 'Invalid Mother\'s phone number'
                        ))*/
            ));
        }

        if (in_array("mphone", $fieldArr)) {
            $this->add($mphone);
        }

        $mcontact = new Select("mcontact", array(
            '1' => 'Primary',
            '2' => 'Secondary'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control',
            'value' => isset($entity->m_phone_no_status) ? $entity->m_phone_no_status : ''
        ));

        if (in_array("mcontact", $fieldArr)) {
            $this->add($mcontact);
        }


        $gname = new Text('gname', array(
            'placeholder' => 'Guardian Name...',
            'class' => 'form-control',
            'value' => isset($entity->other_guardian_name) ? $entity->other_guardian_name : ''
        ));

        if (in_array("gname", $fieldArr2)) {
            $gname->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Guardian name is required'
                        )),
                 new RegexValidator(array(
                      'pattern' => '/^[a-zA-Z ]*$/',
                'message' => 'The Guardian name is invalid'
                    ))
            ));
        }

        if (in_array("gname", $fieldArr)) {
            $this->add($gname);
        }

        $goccup = new Text('goccup', array(
            'placeholder' => 'Guardian Occupation...',
            'class' => 'form-control',
            'value' => isset($entity->g_occupation) ? $entity->g_occupation : ''
        ));

        if (in_array("goccup", $fieldArr2)) {
            $goccup->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Guardian Occupation is required'
                        ))
            ));
        }

        if (in_array("goccup", $fieldArr)) {
            $this->add($goccup);
        }


        $gdesign = new Text('gdesign', array(
            'placeholder' => 'Guardian Designation...',
            'class' => 'form-control',
            'value' => isset($entity->g_designation) ? $entity->g_designation : ''
        ));

        if (in_array("gdesign", $fieldArr2)) {
            $gdesign->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Guardian Designation is required'
                        ))
            ));
        }

        if (in_array("gdesign", $fieldArr)) {
            $this->add($gdesign);
        }

        $gphone = new Text('gphone', array(
            'placeholder' => 'Phone',
            'class' => 'form-control',
            'value' => isset($entity->g_phone) ? $entity->g_phone : ''
        ));
        if (in_array("gphone", $fieldArr2)) {
            $gphone->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Guardian\'s phone number is required'
                        )), new RegexValidator(array(
                      'pattern' => '/^[0-9]*$/',
                'message' => 'The Guardian phone no is invalid'
                    ))/*,
                new StringLength(array(
                    'min' => 10,
                    'messageMinimum' => 'Invalid Guardian\'s phone number'
                        ))*/
            ));
        }

        if (in_array("gphone", $fieldArr)) {
            $this->add($gphone);
        }

        $gcontact = new Select("gcontact", array(
            '1' => 'Primary',
            '2' => 'Secondary'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '0',
            'class' => 'form-control',
            'value' => isset($entity->g_phone_no_status) ? $entity->g_phone_no_status : ''
        ));

        if (in_array("gcontact", $fieldArr)) {
            $this->add($gcontact);
        }

        $feepayee = new Text('feepayee', array(
            'class' => 'form-control',
            'value' => isset($entity->person_school_fee) ? $entity->person_school_fee : ''
        ));
        if (in_array("feepayee", $fieldArr2)) {
            $feepayee->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The person/company paying school fee is required'
                        ))
            ));
        }

        if (in_array("feepayee", $fieldArr)) {
            $this->add($feepayee);
        }

        $famcir = new TextArea('famcir', array(
            'placeholder' => ' family circumstances',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'value' => isset($entity->circumtances) ? $entity->circumtances : ''
        ));
        if (in_array("famcir", $fieldArr2)) {
            $famcir->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill  family circumstances'
                        ))
            ));
        }

        if (in_array("famcir", $fieldArr)) {
            $this->add($famcir);
        }

        $siblings = new TextArea('siblings', array(
            'placeholder' => 'siblings',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "5",
            'value' => isset($entity->sibling) ? $entity->sibling : ''
        ));
        if (in_array("siblings", $fieldArr2)) {
            $siblings->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill siblings'
                        ))
            ));
        }

        if (in_array("siblings", $fieldArr)) {
            $this->add($siblings);
        }
        
        
        $f_education = new Text('f_education', array(
            'class' => 'form-control',
            'value' => isset($entity->f_education) ? $entity->f_education : ''
        ));
        if (in_array("f_education", $fieldArr2)) {
            $f_education->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Father Education Details'
                        ))
            ));
        }

        if (in_array("f_education", $fieldArr)) {
            $this->add($f_education);
        }
        
        $m_education = new Text('m_education', array(
            'class' => 'form-control',
            'value' => isset($entity->m_education) ? $entity->m_education : ''
        ));
        if (in_array("m_education", $fieldArr2)) {
            $m_education->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Mother Education Details'
                        ))
            ));
        }

        if (in_array("m_education", $fieldArr)) {
            $this->add($m_education);
        }
        
        $community = new Text('community', array(
            'class' => 'form-control',
            'value' => isset($entity->community) ? $entity->community : ''
        ));
        if (in_array("community", $fieldArr2)) {
            $community->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Community Details'
                        ))
            ));
        }

        if (in_array("community", $fieldArr)) {
            $this->add($community);
        }
        
         $family_income = new Text('family_income', array(
            'class' => 'form-control',
            'value' => isset($entity->family_income) ? $entity->family_income : ''
        ));
        if (in_array("family_income", $fieldArr2)) {
            $family_income->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Family Income Details'
                        )),new RegexValidator(array(
                      'pattern' => '/^[0-9.]*$/',
                'message' => 'Family Income Details is invalid'
                    ))
            ));
        }

        if (in_array("family_income", $fieldArr)) {
            $this->add($family_income);
        }
    }

}
