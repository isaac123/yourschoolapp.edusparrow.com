
<?php

use Phalcon\Mvc\Model;

/**
 * ResetPasswords
 * Stores the reset password codes and their evolution
 */
class ApprovalItem extends Model
{

    /**
     *
     * @var integer
     */
    public $id;


    /**
     *
     * @var integer
     */
    public $created_date;

    /**
     *
     * @var string
     */
    public $created_by;

    /**
     *
     * @var integer
     */
    public $forwaded_to;
    
    /**
     *
     * @var string
     */
    public $status;   

    /**
     *
     * @var string
     */
    public $comments;   
    
    /**
     *
     * @var string
     */
    public $approval_master_id;

    public function initialize()
    {
        $this->belongsTo('approval_master_id', 'ApprovalMaster', 'id', array(
            'alias' => 'ApprovalMaster',
            'reusable' => true
        ));
    }
}
