<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;

class AddDivisionForm extends Form {

    public function initialize() {

         $divType = new Hidden('divType');
          $this->add($divType);
          
         $academicYrID = new Hidden('academicYrID');
          $this->add($academicYrID);
          
        $cname = new Text('divname', array(
            'placeholder' => 'Division Name',
            'class' => 'form-control',
            'onChange' =>'setupSettings.addDivision(this)'
        ));

        $cname->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Division Name is required'
                    ))
        ));

        $this->add($cname);
    }

}
