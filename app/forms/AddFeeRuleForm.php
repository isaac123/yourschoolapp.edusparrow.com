<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class AddFeeRuleForm extends Form {

    public function initialize($entity = null, $options = null) {



        $name = new Text('feeName', array(
            'placeholder' => 'Fee name...',
            'class' => 'form-control',
            'value' => $entity->fee_name ? $entity->fee_name : '',
            "autocomplete"=>"off"
        ));

        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Fee name is required'
                    ))
        ));

        $this->add($name);



        $state = new Text('amount', array(
//            'placeholder' => '0.00',
            'class' => 'form-control',
            'value' => $entity->fee_amount ? $entity->fee_amount : ''
        ));

        $state->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Amount is required'
                    )),
            new RegexValidator(array(
                //'pattern' => '/^(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})$/',
//                    'pattern' =>     '/^[1-9](?:\d{0,2})+([,]*[0-9]+)*(?:\.\d{0,2}[1-9])?|0?\.\d{0,2}[1-9]|0$/',
                'pattern' => '/^[1-9](?:\d{0,2})+([,]\d{3})*(?:\.\d{0,1}[1-9])?|0?\.\d{0,1}[1-9]$/',
                'message' => 'The amount is invalid'
                    ))
        ));
        $this->add($state);
        
    }

}
