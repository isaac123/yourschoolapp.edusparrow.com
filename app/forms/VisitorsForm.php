<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class VisitorsForm extends Form {

    public function initialize($entity = null, $options = null) {
       
//        print_r(date('d-m-Y',$entity->date));
//        exit;
        $date = new Text('visidate', array(
            'class' => 'form-control form_date',
            'title' => 'Date',
            'readonly' => '',
            'value'=> ($entity->date ? (date('d-m-Y',$entity->date)) : '')
        ));
        $this->add($date);
       
        $name = new Text('name', array(
            'class' => 'form-control',
            'title' => 'Visitor Name',
            'value'=> $entity->visitor_name ? $entity->visitor_name : ''
        ));
        $this->add($name);
        
        $phone = new Text('phone', array(
            'class' => 'form-control',
            'title' => 'Phone Number',
            'value'=> $entity->phone ? $entity->phone : ''
        ));
        $this->add($phone);

        $intime = new Text('intime', array(
            'class' => 'form-control',
            'title' => 'In-Time',
            'value'=> $entity->in_time ? $entity->in_time : ''
        ));
        $this->add($intime);

        $outime = new Text('outime', array(
            'class' => 'form-control',
            'title' => 'Out-Time',
            'value'=> $entity->out_time ? $entity->out_time : ''
        ));
        $this->add($outime);
        
        $purposeofvisit = new TextArea('purposeofvisit', array(
            'class' => 'form-control',
            'title' => 'Purpose of visit',
            'value'=> $entity->purpose_of_visit ? $entity->purpose_of_visit : ''
        ));
        $this->add($purposeofvisit);
    }

}
