<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class EnquiryallForm extends Form {

    public function initialize($entity = null, $options = null) {
        $fieldArr = $fieldArr2 = array();

        if (isset($options['enqid']) && ($options['enqid'] > 0)) {
            $hideOrShowFields = EnquirySettings::find("hide_or_show = 1 and parent_id!=0 AND FIND_IN_SET(" . $options['enqid'] . ",enquiry_node_id)");
            foreach ($hideOrShowFields as $hideOrShow) {
                $fieldArr[$hideOrShow->id] = $hideOrShow->form_name;
            }
            $hideOrShowFields1 = EnquirySettings::find("mandatory = 1 and parent_id!=0 AND FIND_IN_SET(" . $options['enqid'] . ",enquiry_node_id)");
            foreach ($hideOrShowFields1 as $hideOrShow1) {
                $fieldArr2[$hideOrShow1->id] = $hideOrShow1->form_name;
            }
        }
        $name = new Text('name', array(
            'placeholder' => 'Name',
            'class' => 'form-control',
            'value' => isset($entity->name) ? $entity->name : ''
        ));

        if (in_array("name", $fieldArr2)) {
            $name->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Name is required'
                        ))
            ));
        }
//          print_r($fieldArr2);
//        exit;
        if (in_array("name", $fieldArr)) {
            $this->add($name);
        }
        $parentname = new Text('parentname', array(
            'placeholder' => 'Parent Name',
            'class' => 'form-control',
            'value' => isset($entity->parentname) ? $entity->parentname : ''
        ));

        if (in_array("parentname", $fieldArr2)) {
            $parentname->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Parent Name is required'
                        ))
            ));
        }
        if (in_array("parentname", $fieldArr)) {
            $this->add($parentname);
        }
        $purpose = new TextArea('purpose', array(
            'placeholder' => 'Enter the Reason',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->purpose) ? $entity->purpose : '',
        ));
        if (in_array("purpose", $fieldArr2)) {
            $purpose->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Reason is required'
                        ))
            ));
        }
        if (in_array("purpose", $fieldArr)) {
            $this->add($purpose);
        }
        $email = new Text('email', array(
            'placeholder' => 'Mail Id',
            'class' => 'form-control',
            'value' => isset($entity->email) ? $entity->email : ''
        ));

        if (in_array("email", $fieldArr2)) {
            $email->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Email is required'
                        ))
            ));
        }
        if (in_array("email", $fieldArr)) {
            $this->add($email);
        }
        $phone = new Text('phone', array(
            'placeholder' => 'Phone No',
            'class' => 'form-control',
            'value' => isset($entity->phone) ? $entity->phone : ''
        ));

        if (in_array("phone", $fieldArr2)) {
            $phone->addValidators(array(
                new PresenceOf(array(
                    'message' => 'phone no is required'
                        ))
            ));
        }
        if (in_array("phone", $fieldArr)) {
            $this->add($phone);
        }

        $date = new Text('dateenq', array(
            'placeholder' => 'Date',
            'readonly' => '',
            'class' => 'form-control form_date',
            'value' => isset($entity->enqdate) ? date('d-m-Y', $entity->enqdate) : '',
        ));

        if (in_array("dateenq", $fieldArr2)) {
            $date->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Date is required'
                        ))
            ));
        }
        if (in_array("dateenq", $fieldArr)) {
            $this->add($date);
        }

        $status = new Select('status', array(
            '1' => 'Open',
            '2' => 'Closed'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
        ));
        if (in_array("status", $fieldArr2)) {
            $status->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Status is required'
                        ))
            ));
        }
        if (in_array("status", $fieldArr)) {
            $this->add($status);
        }

        $towhom = new Text('towhom', array(
            'placeholder' => 'Whom',
            'class' => 'form-control',
            'value' => isset($entity->towhom) ? $entity->towhom : ''
        ));

        if (in_array("towhom", $fieldArr2)) {
            $towhom->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Name is required'
                        ))
            ));
        }
        if (in_array("towhom", $fieldArr)) {
            $this->add($towhom);
        }
        $intime = new Text('intime', array(
            'placeholder' => 'In Time',
            'readonly' => '',
            'class' => 'form-control enquiry_datetime',
            'value' => isset($entity->intime) ? $entity->intime : ''
        ));

        if (in_array("intime", $fieldArr2)) {
            $intime->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Time is required'
                        ))
            ));
        }
        if (in_array("intime", $fieldArr)) {
            $this->add($intime);
        }
        $outtime = new Text('outtime', array(
            'placeholder' => 'Out Time',
            'readonly' => '',
            'class' => 'form-control enquiry_datetime',
            'value' => isset($entity->outtime) ? $entity->outtime : ''
        ));

        if (in_array("outtime", $fieldArr2)) {
            $outtime->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Time is required'
                        ))
            ));
        }
        if (in_array("outtime", $fieldArr)) {
            $this->add($outtime);
        }
//        $multiplenode = new Text('multiplenode', array(
//            'placeholder' => 'Class and Grade',
//            'class' => 'form-control',
//        ));
//
//        if (in_array("multiplenode", $fieldArr2)) {
//            $multiplenode->addValidators(array(
//                new PresenceOf(array(
//                    'message' => 'Class And Grade is required'
//                        ))
//            ));
//        }
//        if (in_array("multiplenode", $fieldArr)) {
//            $this->add($multiplenode);
//        }

        $files = new File("files", array(
            'placeholder' => 'File'
        ));

        if (in_array("files", $fieldArr)) {
            $this->add($files);
        }

        $from = new Text('from', array(
            'placeholder' => 'From ',
            'class' => 'form-control',
            'value' => isset($entity->from) ? $entity->from : ''
        ));

        if (in_array("from", $fieldArr2)) {
            $from->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Time is required'
                        ))
            ));
        }
        if (in_array("from", $fieldArr)) {
            $this->add($from);
        }

        $to = new Text('to', array(
            'placeholder' => 'To ',
            'class' => 'form-control',
            'value' => isset($entity->to) ? $entity->to : ''
        ));
        if (in_array("to", $fieldArr2)) {
            $to->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Time is required'
                        ))
            ));
        }
        if (in_array("to", $fieldArr)) {
            $this->add($to);
        }

        $particulars = new TextArea('particulars', array(
            'placeholder' => '',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->particulars) ? $entity->particulars : '',
        ));

        if (in_array("particulars", $fieldArr2)) {
            $particulars->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Reason is required'
                        ))
            ));
        }
        if (in_array("particulars", $fieldArr)) {
            $this->add($particulars);
        }

        $officeaddr = new TextArea('officeaddr', array(
            'placeholder' => '',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->officeaddr) ? $entity->officeaddr : '',
        ));

        if (in_array("officeaddr", $fieldArr2)) {
            $officeaddr->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Address is required'
                        ))
            ));
        }
        if (in_array("officeaddr", $fieldArr)) {
            $this->add($officeaddr);
        }

        $depatcheddate = new Text('depatcheddat', array(
            'placeholder' => 'Depatched Date',
            'readonly' => '',
            'class' => 'form-control form_date',
            'value' => isset($entity->depatcheddate) ? date('d-m-Y', $entity->depatcheddate) : '',
        ));

        if (in_array("depatcheddat", $fieldArr2)) {
            $depatcheddate->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Date is required'
                        ))
            ));
        }
        if (in_array("depatcheddat", $fieldArr)) {
            $this->add($depatcheddate);
        }

        $replydate = new Text('replydat', array(
            'placeholder' => 'Reply Date',
            'readonly' => '',
            'class' => 'form-control form_date',
            'value' => isset($entity->replydate) ? date('d-m-Y', $entity->replydate) : '',
        ));

        if (in_array("replydat", $fieldArr2)) {
            $replydate->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Date is required'
                        ))
            ));
        }
        if (in_array("replydat", $fieldArr)) {
            $this->add($replydate);
        }
        $followupdate = new Text('followupdat', array(
            'placeholder' => 'FollowUP Date',
            'readonly' => '',
            'class' => 'form-control followupdate',
            'value' => isset($entity->followupdate) ? date('d-m-Y', $entity->followupdate) : '',
        ));

        if (in_array("followupdat", $fieldArr2)) {
            $followupdate->addValidators(array(
                new PresenceOf(array(
                    'message' => 'FollowUp Date is required'
                        ))
            ));
        }
        if (in_array("followupdat", $fieldArr)) {
            $this->add($followupdate);
        }

        $studentname = new Text('studentname', array(
            'placeholder' => 'Student Name ',
            'class' => 'form-control',
            'value' => isset($entity->studentname) ? $entity->studentname : ''
        ));
        if (in_array("studentname", $fieldArr2)) {
            $to->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Student Name is required'
                        ))
            ));
        }
        if (in_array("studentname", $fieldArr)) {
            $this->add($studentname);
        }
    }

}
