<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class StudentPersonalForm extends Form {

    public function initialize($entity = null, $options = null) {
        if (isset($options['edit']) && $options['edit']) {
          //  if (isset($entity) && $entity) {
                $stuGen = StudentInfo::findFirstById($entity->id);
                
                //The following is not able to edit
                /*$name = new Text('studentName', array(
                    'placeholder' => 'Student name...',
                    'class' => 'form-control',
                    'value' => $entity->Student_Name
                ));

                $name->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'The Student name is required'
                            ))
                ));

                $name->setLabel('Name');
                $this->add($name);

                $gender = new Select("gender", array(
                    '1' => 'Female',
                    '2' => 'Male'
                        ), array(
                    'useEmpty' => false,
                    'emptyText' => 'Select',
                    'emptyValue' => '0',
                    'class' => 'form-control',
                    'value' => $entity->Gender));


                $gender->setLabel('Gender');
                $gender->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'The gender is required'
                            ))
                ));

                $this->add($gender);

                $dob = new Text('dob', array(
                    'class' => 'form-control form-control-inline input-medium default-date-picker',
                    'value' => date('d-m-Y',$entity->Date_of_Birth)
                ));
                $dob->setLabel('Date of Birth');

                $dob->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'The date of birth is required'
                            ))
                ));

                $this->add($dob);
                $pob = new Text('pob', array(
                    'placeholder' => 'Place of Birth...',
                   'class' => 'form-control',
                    'value' =>  $stuGen->place_of_birth?$stuGen->place_of_birth:''
                ));
                $pob->setLabel('Place of Birth');

                $pob->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'The Place of Birth is required'
                            ))
                ));

                $this->add($pob);*/

                $phone = new Text('phone', array(
                    'placeholder' => 'Phone',
                    'class' => 'form-control',
                    'value' => $entity->Phone
                ));
                $phone->setLabel('Phone');

                $phone->addValidators(array(
                     new PresenceOf(array(
                      'message' => 'The phone is required'
                      )), 
            new RegexValidator(array(
                      'pattern' => '/^[0-9]*$/',
                'message' => 'Phone number is invalid'
                    ))/*,
                    new StringLength(array(
                        'min' => 10,
                        'messageMinimum' => 'Invalid phone number'
                            ))*/
                ));

                $this->add($phone);

                $email = new Text('email', array(
                    'placeholder' => 'Email',
                    'class' => 'form-control',
                    'value' => $entity->Email
                ));

                $email->setLabel('Email');
                $email->addValidators(array(
                    /* new PresenceOf(array(
                      'message' => 'The e-mail is required'
                      )), */
                    new Email(array(
                        'message' => 'The e-mail is not valid'
                            ))
                ));

                $this->add($email);


                $address = new TextArea('address1', array(
                    'placeholder' => 'Address Line 1',
                    'class' => 'form-control',
                    'cols' => "60",
                    'rows' => "2",
                    'value' => $entity->Address1
                ));
                $address->setLabel('Address Line 1');

                $address->addValidators(array(
                         new PresenceOf(array(
                          'message' => 'The Address 1 is required'
                          )) 
                ));

                $this->add($address);

                $address2 = new TextArea('address2', array(
                    'placeholder' => 'Address Line 2',
                    'class' => 'form-control',
                    'cols' => "60",
                    'rows' => "2",
                    'value' => $entity->Address2
                ));
                $address2->setLabel('Address Line 2');

                $address2->addValidators(array(
                           new PresenceOf(array(
                          'message' => 'The Address 2 is required'
                          )) 
                ));

                $this->add($address2);



                $state = new Text('state', array(
                    'placeholder' => 'State...',
                    'class' => 'form-control',
                    'value' => $entity->State
                ));

                $state->setLabel('state');
                $state->addValidators(array(
                         new PresenceOf(array(
                          'message' => 'The state is required'
                          )) 
                ));

                $this->add($state);


                $country = new Select('country', CountryMaster::find(), array(
                    "using" => array("country_code", "country_name"),
                    'useEmpty' => false,
                    'emptyText' => 'Select',
                    'emptyValue' => '0',
                    'class' => 'form-control',
                    'value' => $entity->Country
                ));
                $country->setLabel('country');

                $country->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'Country name is required'
                            ))
                ));

                $this->add($country);


                $pin = new Text('pin', array(
                    'placeholder' => 'Pin code...',
                    'class' => 'form-control',
                    'value' => $entity->Pin
                ));

                $pin->setLabel('pin');
                $pin->addValidators(array(
                          new PresenceOf(array(
                          'message' => 'The Pin  code is required'
                          )),
            new RegexValidator(array(
                      'pattern' => '/^[0-9]*$/',
                'message' => 'Pin Code is invalid'
                    ))
                ));

                $this->add($pin);


            /* $nationality = new Select('nationality', CountryMaster::find(), array(
                    "using" => array("country_code", "country_name"),
                    'useEmpty' => false,
                    'emptyText' => 'Select',
                    'emptyValue' => '0',
                    'class' => 'form-control',
                    'value' => $stuGen->nationality
                ));

                $nationality->setLabel('nationality');
                $nationality->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'Nationality is required'
                            ))
                ));

                $this->add($nationality);


                $religion = new Text('religion', array(
                    'placeholder' => 'Religion...',
                    'class' => 'form-control',
                    'value' => $stuGen->religion
                ));

                $religion->setLabel('religion');

                $religion->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'The Religion is required'
                            ))
                ));

                $this->add($religion);


                $caste_cat = new Text('caste_cat', array(
                    'placeholder' => 'Caste Category...',
                    'class' => 'form-control',
                    'value' => $stuGen->caste_category
                ));


                $caste_cat->setLabel('Caste Category');
                $caste_cat->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'The Caste Category is required'
                            ))
                ));

                $this->add($caste_cat);


                $caste = new Text('caste', array(
                    'placeholder' => 'Caste...',
                    'class' => 'form-control',
                    'value' => $stuGen->caste
                ));
                $caste->setLabel('Caste');

                $caste->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'The Caste is required'
                            ))
                ));

                $this->add($caste);


                $language = new Text('language', array(
                    'placeholder' => 'First Language...',
                    'class' => 'form-control',
                    'value' => $stuGen->first_language
                ));
                $language->setLabel('language');

                $language->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'The First Language is required'
                            ))
                ));

                $this->add($language);

                 */


                $photo = new File("photo", array(
                    'placeholder' => 'photo',
                    'value' => $stuGen->photo
                ));

                $photo->setLabel('Photo');
                $this->add($photo);



                $fam_photo = new File("family_photo", array(
                    'placeholder' => 'Family Photo',
                    'value' => $stuGen->family_photo
                ));

                $fam_photo->setLabel('Family Photo');
                $this->add($fam_photo);
            //}
        }
    }

}
