<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class AssignSalaryForm extends Form {

    public function initialize($entity = null, $options = null) {

        $basicpay = new Text('Basic_Pay', array(
            'placeholder' => '0.00',
            'class' => 'form-control amountfield',
            'title' => 'Basic Pay',
            'onkeyup' => 'salarySettings.calculatetotal()'
        ));

        $basicpay->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Basic Pay is required'
                    )), new RegexValidator(array(
                'pattern' => '/^[1-9](?:\d{0,2})+([,]\d{3})*(?:\.\d{0,1}[1-9])?|0?\.\d{0,1}[1-9]$/',
                'message' => 'The Basic Pay is in invalid'
                    ))
        ));

        $this->add($basicpay);

        $hra = new Text('HRA', array(
            'placeholder' => '0.00',
            'class' => 'form-control amountfield',
            'title' => 'HRA',
            'onkeyup' => 'salarySettings.calculatetotal()'
        ));

        $hra->addValidators(array(
            new PresenceOf(array(
                'message' => 'The HRA is required'
                    )), new RegexValidator(array(
                'pattern' => '/^[1-9](?:\d{0,2})+([,]\d{3})*(?:\.\d{0,1}[1-9])?|0?\.\d{0,1}[1-9]$/',
                'message' => 'The HRA is in invalid'
                    ))
        ));

        $this->add($hra);
        $pf = new Text('PF', array(
            'placeholder' => '0.00',
            'class' => 'form-control amountfield',
            'title' => 'PF',
            'onkeyup' => 'salarySettings.calculatetotal()'
        ));

        $pf->addValidators(array(
            new PresenceOf(array(
                'message' => 'The PF is required'
                    )), new RegexValidator(array(
                'pattern' => '/^[1-9](?:\d{0,2})+([,]\d{3})*(?:\.\d{0,1}[1-9])?|0?\.\d{0,1}[1-9]$/',
                'message' => 'The PF is in invalid'
                    ))
        ));

        $this->add($pf);
        $conveyance = new Text('conveyance', array(
            'placeholder' => '0.00',
            'class' => 'form-control amountfield',
            'title' => 'conveyance',
            'onkeyup' => 'salarySettings.calculatetotal()'
        ));

        $conveyance->addValidators(array(
            new PresenceOf(array(
                'message' => 'The conveyance is required'
                    )), 
            /*new RegexValidator(array(
                'pattern' => '/^[1-9](?:\d{0,2})+([,]\d{3})*(?:\.\d{0,1}[1-9])?|0?\.\d{0,1}[1-9]$/',
                'message' => 'The conveyance is in invalid'
                    ))*/
        ));

        $this->add($conveyance);
        $splallow = new Text('Special_Allowances', array(
            'placeholder' => '0.00',
            'class' => 'form-control amountfield',
            'title' => 'Special Allowances',
            'onkeyup' => 'salarySettings.calculatetotal()'
        ));

        $splallow->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Special Allowances  is required'
                    )),
            /*new RegexValidator(array(
                'pattern' => '/^[1-9](?:\d{0,2})+([,]\d{3})*(?:\.\d{0,1}[1-9])?|0?\.\d{0,1}[1-9]$/',
                'message' => 'The Special Allowances  is in invalid'
                    ))*/
        ));

        $this->add($splallow);
        $grospy = new Text('Gross_Pay', array(
            'placeholder' => '0.00',
            'class' => 'form-control amountfield',
            'title' => 'Gross Pay',
            'onkeyup' => 'salarySettings.calculatetotal()'
        ));

        $grospy->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Gross Pay is required'
                    )), new RegexValidator(array(
                'pattern' => '/^[1-9](?:\d{0,2})+([,]\d{3})*(?:\.\d{0,1}[1-9])?|0?\.\d{0,1}[1-9]$/',
                'message' => 'The Gross Pay is in invalid'
                    ))
        ));

        $this->add($grospy);
    }

}
