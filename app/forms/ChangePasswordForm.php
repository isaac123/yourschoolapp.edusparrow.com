<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;

class ChangePasswordForm extends Form {

    public function initialize($entity = null, $options = null) {

        if (!isset($options['resetPassword']) && !$options['resetPassword']) {
            // old Password
            $opassword = new Password('oldpassword', array(
                "class" => "form-control"
            ));

            $opassword->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Old Password is required'
                        ))
            ));

            $this->add($opassword);
        }

        // Password
//        print_r($options);
        $password = new Password('password', array(
            "class" => "form-control"
        ));

        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'New Password is required'
                    )),
            new StringLength(array(
                'min' => 8,
                'messageMinimum' => 'New Password is too short. Minimum 8 characters'
                    )),
            new Confirmation(array(
                'message' => 'Entered New  Password doesn\'t match',
                'with' => 'confirmPassword'
                    ))
        ));

        $this->add($password);

        // Confirm Password
        $confirmPassword = new Password('confirmPassword', array(
            "class" => "form-control"
        ));

        $confirmPassword->addValidators(array(
            new PresenceOf(array(
                'message' => 'The confirmation password is required'
                    ))
        ));

        $this->add($confirmPassword);
    }

}
