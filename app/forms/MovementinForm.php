<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class MovementinForm extends Form {

    public function initialize($entity = null, $options = null) {

        $po_no = new Text('po_no', array(
            'placeholder' => 'Enter purchase Order No',
            'class' => 'form-control hide',
        ));
        $this->add($po_no);

        $movement_type = new Select('movement_type', array(
            'Partial' => 'Partial',
            'Closed' => 'Closed'
                ), array(
            'useEmpty' => true,
            'emptyText' => 'Select',
            'emptyValue' => '',
            'class' => 'form-control',
            'value' => isset($entity->movement_type) ? $entity->movement_type : ''
        ));
        $movement_type->addValidators(array(
            new PresenceOf(array(
                'message' => 'Type is required'
                    ))
        ));
        $this->add($movement_type);

        $invoice_no = new Text('invoice_no', array(
            'placeholder' => 'Invoice No',
            'class' => 'form-control',
            'value' => isset($entity->invoice_no) ? $entity->invoice_no : ''
        ));
        $invoice_no->addValidators(array(
            new PresenceOf(array(
                'message' => 'Invoice No  is required'
                    ))
        ));
        $this->add($invoice_no);

        $dc_no = new Text('dc_no', array(
            'placeholder' => 'DC No',
            'class' => 'form-control',
            'value' => isset($entity->dc_no) ? $entity->dc_no : ''
        ));
//        $dc_no->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'DC No  is required'
//                    ))
//        ));
        $this->add($dc_no);

        $details = new TextArea('details', array(
            'placeholder' => '',
            'class' => 'form-control',
            'cols' => "60",
            'rows' => "2",
            'value' => isset($entity->details) ? $entity->details : ''
        ));
//        $details->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'Details is required'
//                    ))
//        ));
        $this->add($details);

        $tot_amount = new Text('tot_amount', array(
            'placeholder' => 'Amount',
            'readonly' => '',
            'class' => 'form-control tot_amount ',
            'onchange' => "movementinSettings.validateAmountdet(this)",
            'value' => isset($entity->tot_amount) ? $entity->tot_amount : ''
        ));
//        $amount->addValidators(array(
//            new PresenceOf(array(
//                'message' => 'Amount  is required'
//                    ))
//        ));
        $this->add($tot_amount);

        $material_name = new Text('material_name', array(
            'placeholder' => 'Material Name',
            'class' => 'form-control',
        ));
        $this->add($material_name);

        $vendor_name = new Text('vendor_name', array(
            'placeholder' => 'Vendor Name',
            'class' => 'form-control',
            'value' => isset($entity->vendor_name) ? $entity->vendor_name : ''
        ));
       
        $this->add($vendor_name);
    }

}
