<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Date,
    Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;

class StudentRelieveForm extends Form {

    public function initialize($entity = null, $options = null) {
        $relieventity = '';
        $school_name = Settings::findFirstByVariableName('school_name')->variableValue;
        $school_addr = Settings::findFirstByVariableName('school_address')->variableValue;
        $school_det = $school_name . ", " . $school_addr;
        $studentId = new Hidden('studentId', array('value' => $entity->id));
        $this->add($studentId);
        if (isset($options['edit']) && $options['edit']) {
            $relieventity = $options['relieventity'];

            $readonly = $relieventity ? 'disabled' : '';
            $school = new TextArea('school', array(
                'placeholder' => 'Name & Complete Address of the Educational Institution',
                'class' => 'form-control',
                'cols' => "60",
                'rows' => "5",
                'title' => 'Name & Complete Address of the Educational Institution',
                'value' => $school_det,
                $readonly => ''
            ));
            $school->setLabel('Name & Complete Address of the Educational Institution');

            $school->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Educational Institution details '
                        ))
            ));

            $this->add($school);

            $name = new Text('studentName', array(
                'placeholder' => 'Student name...',
                'class' => 'form-control',
                'readonly' => '',
                'title' => 'Student name',
                'value' => $entity->Student_Name,
                $readonly => ''
            ));
            $name->setLabel('Student name');

            $name->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Student name is required'
                        ))
            ));

            $this->add($name);


            $fname = new Text('fname', array(
                'placeholder' => 'Father Name...',
                'class' => 'form-control',
                'title' => 'Father name',
                'value' => $entity->father_name,
                $readonly => ''
            ));
            $fname->setLabel('Father Name');

            $fname->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Father name is required'
                        ))
            ));

            $this->add($fname);

            $mname = new Text('mname', array(
                'placeholder' => 'Mother Name...',
                'class' => 'form-control',
                'title' => 'Mother name',
                'value' => $entity->mother_name,
                $readonly => ''
            ));
            $mname->setLabel('Mother Name');

            $mname->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Mother name is required'
                        ))
            ));

            $this->add($mname);

            $gname = new Text('gname', array(
                'placeholder' => 'Guardian Name...',
                'class' => 'form-control',
                'title' => 'Guardian name',
                'value' => $entity->other_guardian_name,
                $readonly => ''
            ));
            $gname->setLabel('Guardian Name');

            $gname->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Guardian name is required'
                        ))
            ));

            $this->add($gname);

            $gender = new Select("gender", array(
                '1' => 'Female',
                '2' => 'Male'
                    ), array(
                'useEmpty' => true,
                'emptyText' => 'Select',
                'emptyValue' => '',
                'disabled' => '',
                'title' => 'Gender',
                'class' => 'form-control',
                'value' => $entity->Gender,
                $readonly => ''
            ));
            $gender->setLabel('Gender');

            $gender->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The gender is required'
                        ))
            ));

            $this->add($gender);


            $nationality = new Select('nationality', CountryMaster::find(), array(
                "using" => array("country_code", "country_name"),
                'useEmpty' => false,
                'emptyText' => 'Select',
                'emptyValue' => '0',
                'class' => 'form-control',
                'title' => 'nationality',
                'value' => $entity->nationality,
                $readonly => ''
            ));
            $nationality->setLabel('Nationality');

            $nationality->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Nationality is required'
                        ))
            ));

            $this->add($nationality);


            $religion = new Text('religion', array(
                'placeholder' => 'Religion...',
                'class' => 'form-control',
                'title' => 'Religion',
                'value' => $entity->religion,
                $readonly => ''
            ));
            $religion->setLabel('Religion');

            $religion->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Religion is required'
                        ))
            ));

            $this->add($religion);

            $dob = new Text('dob', array(
                'class' => 'form-control form_date',
                'disabled' => '',
                'title' => 'Date of Birth',
                'value' => date('d-m-Y', $entity->Date_of_Birth ),
                $readonly => ''));
            $dob->setLabel('Date of Birth');

            $dob->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The date of birth is required'
                        ))
            ));

            $this->add($dob);


            $doj = new Text('doj', array(
            'class' => 'form-control form_date',
            'disabled' => '',
            'title' => 'Date of Admission',
            'value' => date('d-m-Y', $entity->Date_of_Joining),
            $readonly => ''));
            $doj->setLabel('Date of Admission');

            $doj->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The date of Admission is required'
                        ))
            ));

            $this->add($doj);

//            $jdivVal = new Text('jdivVal', array(
//                'class' => 'form-control'));
//            $jdivVal->setLabel('Standard of Admission');
//
//            $jdivVal->addValidators(array(
//                new PresenceOf(array(
//                    'message' => 'Select any standard of Admission'
//                        ))
//            ));
//
//            $this->add($jdivVal);



            $dor = new Text('dor', array(
                'class' => 'form-control form_date',
                'readonly' => '',
                'title' => 'Date when the student left the school',
                'value' => $relieventity->scl_left_date ? date('d-m-Y', $relieventity->scl_left_date) : '',
                $readonly => ''
            ));
            $dor->setLabel('Date when the student left the school');

            $dor->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The date of relieving is required'
                        ))
            ));

            $this->add($dor);


            $conduct = new TextArea('conduct', array(
                'placeholder' => 'Conduct & Character of the student',
                'class' => 'form-control',
                'title' => 'Conduct & Character of the student',
                'cols' => "60",
                'value' => $relieventity ? $relieventity->stdcharacter : '',
                'rows' => "5",
                $readonly => ''
            ));
            $conduct->setLabel('Conduct & Character of the student');

            $conduct->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please Conduct & Character of the student'
                        ))
            ));

            $this->add($conduct);


            $applytc = new Text('applytc', array(
                'class' => 'form-control form_date',
                'readonly' => '',
                'value' => $relieventity ? date('d-m-Y', $relieventity->tc_made_date) : '',
                'title' => 'Date on which application for Transfer Certificate was made by the Parent',
                $readonly => ''
            ));
            $applytc->setLabel('Date on which application for Transfer Certificate was made by the Parent');

            $applytc->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The date when applied for Transfer Certificat is required'
                        ))
            ));

            $this->add($applytc);

            $dotc = new Text('dotc', array(
                'class' => 'form-control form_date',
                'readonly' => '',
                'title' => 'Date of Transfer Certificate',
                'value' => $relieventity->tc_date ? date('d-m-Y', $relieventity->tc_date) : date('d-m-Y', time()),
                $readonly => ''
            ));
            $dotc->setLabel('Date of Transfer Certificate');

            $dotc->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Date of Transfer Certificate is required'
                        ))
            ));

            $this->add($dotc);

//            $rdivVal =  new Text('rdivVal', array(
//                'class' => 'form-control'));
//            $rdivVal->setLabel('Standard in which the student was studying at the time of leaving');
//
//            $rdivVal->addValidators(array(
//                new PresenceOf(array(
//                    'message' => 'Select any relieving standard'
//                        ))
//            ));
//
//            $this->add($rdivVal);


            $course_complt = new Select("course_complt", array(
                '1' => 'No',
                '2' => 'Yes'
                    ), array(
                'useEmpty' => false,
                'emptyText' => 'Select',
                'emptyValue' => '0',
                'value' => $relieventity ? $relieventity->course_complete : '',
                'title' => 'Whether completed the course successfully',
                'class' => 'form-control',
                $readonly => ''
            ));
            $course_complt->setLabel('Whether completed the course successfully');

            $this->add($course_complt);


            $promoted_higher = new Select("promoted_higher", array(
                '1' => 'No',
                '2' => 'Yes'
                    ), array(
                'useEmpty' => false,
                'emptyText' => 'Select',
                'emptyValue' => '0',
                'value' => $relieventity ? $relieventity->qualify_promo : '',
                'title' => 'Whether qualified for promotion to higher standard',
                'class' => 'form-control',
                $readonly => ''
            ));
            $promoted_higher->setLabel('Whether qualified for promotion to higher standard');

            $this->add($promoted_higher);

            $complt_det = new TextArea('complt_det', array(
                'placeholder' => 'Course completion details',
                'class' => 'form-control',
                'cols' => "60",
                'value' => $relieventity ? $relieventity->course_comp_det : '',
                'title' => 'Course completion details',
                'rows' => "5",
                $readonly => ''
            ));
            $complt_det->setLabel('Course completion details');

            $complt_det->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Please fill Course completion details '
                        ))
            ));

            $this->add($complt_det);

            $SchoolName = new Text('SchoolName', array(
                'placeholder' => 'School Name...',
                'class' => 'form-control',
                'title' => 'School Name',
                'value' => $school_name,
                $readonly => ''
            ));
            $SchoolName->setLabel('Name of the School');
            $SchoolName->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Name of the School is required'
                        ))
            ));

            $this->add($SchoolName);

            $language = new Text('language', array(
                'placeholder' => 'First Language...',
                'title' => 'First Language',
                'class' => 'form-control',
                'value' => $entity->first_language,
                $readonly => ''
            ));
            $language->setLabel('First Language');

            $language->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The First Language is required'
                        ))
            ));

            $this->add($language);


            $moi = new Text('moi', array(
                'placeholder' => 'Medium of Instruction...',
                'class' => 'form-control',
                'title' => 'Medium of Instruction',
                'value' => $relieventity->medium ? $relieventity->medium : 'English',
                $readonly => ''
            ));
            $moi->setLabel('Medium of Instruction');

            $moi->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Medium of Instruction is required'
                        ))
            ));

            $this->add($moi);
            $relievReason = new TextArea('relievReason', array(
                'placeholder' => 'Reason for Relieving...',
                'class' => 'form-control',
                'title' => 'Reason for Relieving',
                'cols' => "60",
                'value' => $relieventity ? $relieventity->reason : '',
                'rows' => "5",
                $readonly => ''
            ));
            $relievReason->setLabel('Reason');

            $relievReason->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Add reason'
                        ))
            ));

            $this->add($relievReason);
        }
    }

}
