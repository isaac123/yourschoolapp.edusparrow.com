<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class AttendancesettingsController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->tag->prependTitle("Attendance Settings| ");
        $this->assets->addCss('css/edustyle.css');
    }

    public function staffAttendanceSettingsAction() {
//        $this->tag->prependTitle("Attendance Settings| ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs("js/attendancesettings/settings.js");
//        $this->assets->addJs("js/attendancesettings/staffsettings.js");

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $records = AttendanceSelectbox::find('attendance_for = "staff" ORDER BY default DESC');
        $this->view->records = $records;
    }

    public function updateStaffSettingsAction() {

        $this->tag->prependTitle("Attendance Settings| ");
        $this->assets->addJs("js/attendancesettings/staffsettings.js");

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $count = $this->request->getPost('count');
            //echo $count;

            $hidden_count = $this->request->getPost('hidden_count');
            //echo $hidden_count;
            if ($hidden_count > 0) {
                $hidden_names = $this->request->getPost('hidden');

                for ($i = 0; $i < $hidden_count; $i++) {
                    //echo '<div class="alert alert-block alert-danger fade in">' . $hidden_names[$i] . '</div>';
                    $record_del = AttendanceSelectbox::findfirst(' attendance_for = "staff"'
                                    . ' and attendancename = "' . $hidden_names[$i] . '"');
                    if ($record_del->delete() == false) {

                        $error = '';
                        foreach ($record_del->getMessages() as $message) {
                            $error .= $message;
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    }
                }
            }
            $success = 0;
            for ($i = 0; $i < $count; $i++) {
                $name_value_pair = $this->request->getPost("$i");
                //echo $pair[0];


                $record = AttendanceSelectbox::findfirst('attendance_for = "staff"'
                                . ' and attendancename = "' . $name_value_pair[0] . '"');

                if (!($record)) {
                    //New entry
                    $AttendanceSelectbox = new AttendanceSelectbox();
                    $AttendanceSelectbox->assign(array("academic_year_id" => $current_acd_year,
                        "attendance_for" => "staff",
                        "attendancename" => $name_value_pair[0],
                        "attendanceid" => $name_value_pair[1],
                        "attendancevalue" => $name_value_pair[2],
                        "color" => $name_value_pair[3],
                        "allowed_leave_typ" => $name_value_pair[4],
                        "default" => 0));
                    if (!($AttendanceSelectbox->save())) {
                        $error = '';
                        foreach ($AttendanceSelectbox->getMessages() as $message) {
                            $error .= $message;
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    }
                    //echo "new";
                    $success = 1;
                } else {
                    $record->attendance_for = "staff";
                    $record->attendancename = $name_value_pair[0];
                    $record->attendanceid = $name_value_pair[1];
                    $record->attendancevalue = $name_value_pair[2];
                    $record->color = $name_value_pair[3];
                    $record->allowed_leave_typ = $name_value_pair[4];
                    $record->save();
                    //echo "update";
                    $success = 1;
                }
            }
            if ($success) {
                echo '<div class="alert alert-block alert-success fade in">Attendance Setting Updated Successfully</div>';
            }
        }
    }

    public function addNumofDaysAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Attendance Settings| ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs("js/attendancesettings/staffsettings.js");
        if ($this->request->isPost()) {
            $this->view->leavetype = $this->request->getPost('leavetype');
            $this->view->identity = $this->auth->getIdentity();
            $this->view->attendanceval = AttendanceSelectbox::find('attendanceid <> "P" AND attendance_for = "staff"');
            $this->view->attendanceval = AttendanceSelectbox::find('attendanceid <> "P" AND attendance_for = "staff"');
            $this->view->attendanceFreq = AttendanceFrequency::findFirst('frequency_for = "staff"');
            $this->view->attendancevalbyid = AttendanceSelectbox::findFirstById($this->request->getPost('id'));
        }
    }

    public function updateAttendanceAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $this->tag->prependTitle("Attendance Settings| ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs("js/attendancesettings/staffsettings.js");
        if ($this->request->isPost()) {
            $attedance_update = AttendanceSelectbox::findFirstById($this->request->getPost('id'));
            $allowed_days = $this->request->getPost('allowed_days');
            $allowed_month = $this->request->getPost('allowed_month');
            $is_allowed_by_count = $this->request->getPost('is_allowed_by_count');
            $school_cal_formula = $this->request->getPost('formula');
//            $salarydeduct = $this->request->getPost('salarydeduct');
//            $sal_deduct = $this->request->getPost('sal_deduct');
//            $deduct_days = $this->request->getPost('deduct_days');
//            $deduct_type = $this->request->getPost('deduct_type');
//            $deduct_max_days = $this->request->getPost('deduct_max_days');
//            $showinpayslip = $this->request->getPost('showinpayslip');
//            $attendacetype = $this->request->getPost('attendacetype');
            //update attendance
            $attedance_update->allowed_count = $allowed_days;
            $attedance_update->duration_month = $allowed_month;
            $attedance_update->is_allowed_by_count = $is_allowed_by_count;
            $attedance_update->school_cal_formula = $school_cal_formula;
//            $attedance_update->punish = $salarydeduct;
//            if ($salarydeduct == 1) {
//                $attedance_update->punish_limit = $deduct_max_days;
//                $attedance_update->punish_group = $deduct_days;
//                $attedance_update->punish_type = $deduct_type;
//                $attedance_update->deduct_salary = $sal_deduct;
//            } else {
//                $attedance_update->punish_limit = '';
//                $attedance_update->punish_group = '';
//                $attedance_update->punish_type = '';
//                $attedance_update->deduct_salary = '';
//            }
//            $attedance_update->show_in_payslip = $showinpayslip;
//            if ($showinpayslip == 0)
//                $attedance_update->substitute = $attendacetype;
//            else
//                $attedance_update->substitute = '';

            $error = '';
//            print_r($attedance_update);exit;
            if (!$attedance_update->save()) {
                foreach ($attedance_update->getMessages() as $message) {
                    $error .= $message;
                }
            }
            if ($error == '') {
                echo '<div class="alert alert-block alert-success fade in">Attendance updated successfully!</div>';
            } else {
                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            }
        }
    }

    public function updateStaffFrequencyAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $count = $this->request->getPost('count');
            $frequency = $this->request->getPost('frequency');

            $error = '';
            for ($i = 0; $i < $count; $i++) {
                $record = AttendanceFrequency::findfirst('frequency_for = "staff"');
                if ($record) {
                    //Db entry exist already, just update the frequency
                    $record->frequency = $frequency[$i];
                } else {
                    //Db entry does not exist, so create one
                    $record = new AttendanceFrequency();
                    $record->node_id = 0;
                    $record->frequency_for = "staff";
                    $record->frequency = $frequency[$i];
                }
                if (!$record->save()) {
                    foreach ($record->getMessages() as $message) {
                        $error .= $message;
                    }
                }
            }

            if ($error == '') {
                echo '<div class="alert alert-block alert-success fade in">Number of Periods were updated successfully!</div>';
            } else {
                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            }
        }
    }

    #####Staff settings ends here #####

    public function studentAttendanceSettingsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Attendance Settings| ");
//        $this->assets->addCss('css/edustyle.css');
        $records = AttendanceSelectbox::find(' attendance_for = "student" ORDER BY default DESC');
        $this->view->records = $records;
    }

    public function updateStudentSettingsAction() {

        $this->tag->prependTitle("Attendance Settings| ");
        $this->assets->addJs("js/attendancesettings/settings.js");

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $count = $this->request->getPost('count');
            //echo $count;

            $hidden_count = $this->request->getPost('hidden_count');
            //echo $hidden_count;
            if ($hidden_count > 0) {
                $hidden_names = $this->request->getPost('hidden');

                for ($i = 0; $i < $hidden_count; $i++) {
                    //echo '<div class="alert alert-block alert-danger fade in">' . $hidden_names[$i] . '</div>';
                    $record_del = AttendanceSelectbox::findfirst('attendance_for = "student"'
                                    . ' and attendancename = "' . $hidden_names[$i] . '"');
                    if ($record_del->delete() == false) {

                        $error = '';
                        foreach ($record_del->getMessages() as $message) {
                            $error .= $message;
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    }
                }
            }
            $success = 0;
            for ($i = 0; $i < $count; $i++) {
                $name_value_pair = $this->request->getPost("$i");
                //echo $pair[0];


                $record = AttendanceSelectbox::findfirst('attendance_for = "student"'
                                . ' and attendancename = "' . $name_value_pair[0] . '"');

                if (!($record)) {
                    //New entry
                    $AttendanceSelectbox = new AttendanceSelectbox();
                    $AttendanceSelectbox->assign(array("academic_year_id" => $current_acd_year,
                        "attendance_for" => "student",
                        "attendancename" => $name_value_pair[0],
                        "attendanceid" => $name_value_pair[1],
                        "attendancevalue" => $name_value_pair[2],
                        "color" => $name_value_pair[3],
                        "default" => 0));
                    if (!($AttendanceSelectbox->save())) {
                        $error = '';
                        foreach ($AttendanceSelectbox->getMessages() as $message) {
                            $error .= $message;
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    }
                    $success = 1;
                } else {
                    $record->attendance_for = "student";
                    $record->attendancename = $name_value_pair[0];
                    $record->attendanceid = $name_value_pair[1];
                    $record->attendancevalue = $name_value_pair[2];
                    $record->color = $name_value_pair[3];
                    $record->save();
                    $success = 1;
                }
            }

            if ($success) {
                echo '<div class="alert alert-block alert-success fade in">Attendance Setting Updated Successfully</div>';
            }
        }
    }

    public function updateStudentFrequencyAction() {

        $this->tag->prependTitle("Attendance Settings| ");
        $this->assets->addJs("js/attendancesettings/settings.js");

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
//            $count = $this->request->getPost('count');
//            $class_ids = $this->request->getPost('class_ids');
            $frequency = $this->request->getPost('frequency');

            $error = '';
//            for ($i = 0; $i < $count; $i++) {
            //. ' and node_id = ' . $class_ids[$i] 
            $record = AttendanceFrequency::findfirst('frequency_for = "student"');
            if ($record) {
                //Db entry exist already, just update the frequency
                $record->frequency = $frequency[0];
            } else {
                //Db entry does not exist, so create one
                $record = new AttendanceFrequency();
                $record->node_id = 0;
                $record->frequency_for = "student";
                $record->frequency = $frequency[0];
            }
            if (!$record->save()) {
                foreach ($record->getMessages() as $message) {
                    $error .= $message;
                }
            }
//            }

            if ($error == '') {
                echo '<div class="alert alert-block alert-success fade in">Number of Periods were updated successfully!</div>';
            } else {
                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            }
        }
    }

    public function getAttendancePeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
            $periods = PeriodMaster::find('user_type LIKE "student"');
            $arrayval = array();
            foreach ($periods as $period) {
                $nodes = explode('-', $period->node_id);
                array_walk($nodes, function (&$item, $key) {
                    $item = OrganizationalStructureValues::findFirstById($item)->name;
                });
                $node_name = implode('>>', $nodes);
                $arrayval[$node_name][] = array(
                    'id' => $period->id,
                    'node_id' => $period->node_id,
                    'user_type' => $period->user_type,
                    'period' => $period->period,
                    'start_time' => date('H:i',$period->start_time),
                    'end_time' => date('H:i',$period->end_time)
                );
            }
            $this->view->arrayval = $arrayval;
        }
    }

    public function assignPeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->node_id = $this->request->getPost('node_id');
    }

    public function addPeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {
            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'aggregate' && $value) {
                $params['aggregateids'][] = $value;
            } else {

                $params[$key] = $value;
            }
        }
        $ids = explode('-', $params['node_id']);
        $res = ControllerBase::buildExamQuery($params['node_id']);
        $periods = PeriodMaster::findFirst($res[0] . ' and period = "' . $params['period'] . '"'
                        . ' and start_time=' . $params['start_time'] . ' and end_time = ' . $params['end_time']) ?
                PeriodMaster::findFirst($res[0] . ' and period = "' . $params['period'] . '"'
                        . ' and start_time=' . $params['start_time'] . ' and end_time = ' . $params['end_time']) :
                PeriodMaster::findFirst($res[0]
                        . ' and start_time=' . $params['start_time'] . ' and end_time = ' . $params['end_time']) ?
                        PeriodMaster::findFirst($res[0]
                                . ' and start_time=' . $params['start_time'] . ' and end_time = ' . $params['end_time']) : new PeriodMaster();

        $periods->node_id = $params['node_id'];
        $periods->period = $params['period'];
        $periods->start_time = $params['start_time'];
        $periods->end_time = $params['end_time'];
        $periods->user_type = "student";
        if ($periods->save()) {
            $message['type'] = 'success';
            $message['ID'] = $ids[0];
            $message['message'] = ' Periods Added Successfully';
            print_r(json_encode($message));
            exit;
        } else {
            $error = '';
            foreach ($periods->getMessages() as $messages) {
                $error .= $messages;
            }
            echo $error;
            exit;
        }
    }

    public function editAttnPeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->id = $id = $this->request->getPost('id');
        $this->view->periodMastr = $periodMastr = PeriodMaster::findFirstById($id);
    }

    public function updatePeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }
        //$periods->node_id = $params['node_id'];
        $starttime = $params['start_time'];
        $endtime = $params['end_time'];
        $starttimestamp = strtotime('01-01-1970 ' . $params['start_time']);
        $endtimestamp = strtotime('01-01-1970 ' . $params['end_time']);
        if (isset($params['node_id']) && $params['node_id'] != '') {
            $periodsmas = new PeriodMaster();
            $contionqry = 'LOCATE(node_id ,"' . $params['node_id'] . '")'
                    . ' and( (start_time between "' . $starttimestamp . '" and "' . $endtimestamp . '" )'
                    . '   or ( end_time between "' . $starttimestamp . '" and "' . $endtimestamp . '" ))';
            $nodeid = $params['node_id'];
        }
        if (isset($params['period_id']) && $params['period_id'] != '') {
            $periodsmas = PeriodMaster::findFirst(' id = ' . $params['period_id']);
            $contionqry = 'LOCATE(node_id ,"' . $periodsmas->node_id . '")'
                    . ' and ( (start_time between "' . $starttimestamp . '" and "' . $endtimestamp . '" )'
                    . '   or ( end_time between "' . $starttimestamp . '" and "' . $endtimestamp . '" ))'
                    . ' and id != ' . $params['period_id'];
            $nodeid = $periodsmas->node_id;
        }
//echo $contionqry;exit;
        $periods = PeriodMaster::findFirst($contionqry);

        if ($periods) {
            $message['type'] = 'error';
            $message['message'] = ' Periods Already Existed';
            print_r(json_encode($message));
            exit;
        } else {
            $periodsmas->period = $params['period'];
            $periodsmas->node_id = $nodeid;
            $periodsmas->user_type = 'student';
            $periodsmas->start_time = $starttimestamp;
            $periodsmas->end_time = $endtimestamp;
            if ($periodsmas->save()) {
                $message['type'] = 'success';
                $message['message'] = ' Periods Updated Successfully';
                print_r(json_encode($message));
                exit;
            } else {
                $error = '';
                foreach ($periods->getMessages() as $messages) {
                    $error .= $messages;
                }
                echo $error;
                exit;
            }
        }
   
    }

    public function deleteAttnPeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $this->view->id = $id = $this->request->getPost('id');
        $periodMastr = PeriodMaster::findFirstById($id);
        $ids = explode('-', $periodMastr->node_id);
        if ($periodMastr->delete()) {
            $message['type'] = 'success';
            $message['ID'] = $ids[0];
            $message['message'] = ' Periods Deleted Successfully';
            print_r(json_encode($message));
            exit;
        } else {
            $error = '';
            foreach ($periodMastr->getMessages() as $messages) {
                $error .= $messages;
            }
            echo $error;
            exit;
        }
    }

}
