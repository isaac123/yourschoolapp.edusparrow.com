<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class ApplicationController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Application | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/application.js');
        $this->assets->addJs('js/appscripts/approval.js');
        $this->assets->addJs('js/application/manageApplication.js');
        $this->view->form = new ApplicationForm();
    }

    public function loadSubtreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost() && $this->request->getPost('orgvalueid') != '') {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
            $this->view->org_value = $org_value = OrganizationalStructureValues::find('parent_id = ' . $orgvalueid
                            . ' GROUP BY  org_master_id ');
//             $this->view->org_mas_value = $org_mas_value = OrganizationalStructureMaster::find('parent_id = '.$orgvalueid);
        }
    }

    public function issueApplicationAction() {
        $form = new ApplicationForm();
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $message = array();
        try {
            if ($this->request->isPost()) {
                if ($form->isValid($this->request->getPost()) != false) {

                    $params = $queryParams = array();
                    foreach ($this->request->getPost() as $key => $value) {
                        $IsSubdiv = explode('_', $key);
                        if ($IsSubdiv[0] == 'aggregate' && $value) {
                            $params['aggregateids'][] = $value;
                        } else {

                            $params[$key] = $value;
                        }
                    }
//                    $cacdyr = OrganizationalStructureValues::findFirst('org_master_id = ' .
//                                    OrganizationalStructureMaster::findFirstByCycleNode(1)->id
//                                    . ' and status = "C"');
                    $cacdyr = OrganizationalStructureValues::findFirst('org_master_id = ' .
                                    OrganizationalStructureMaster::findFirstByCycleNode(1)->id
                                    . ' and  id IN ( ' . implode(',', $params['aggregateids']) . ')');
                    $year = explode('-', $cacdyr->name);
                    $appmax = Application::find('application_no LIKE "' . $year[0] . '%"')->getLast()->application_no;
                    $newappno = (($appmax) ? ($appmax + 1) : ($year[0] . '001'));
                    $application = new Application();
                    $application->application_no = $newappno; //$params['application_no'];
                    $application->aggregate_key = implode(',', $params['aggregateids']);
                    $application->Student_Name = $params['studentName'];
                    $application->Gender = $params['gender'];
                    $application->Date_of_Birth = strtotime($params['dob']);
                    $application->status = 'Issued';
                    $application->key_in_status = 'Yet to be admitted';
                    $application->Date_of_Joining = '';
                    $application->Address1 = $params['address1'];
                    $application->Address2 = $params['address2'];
                    $application->State = $params['state'];
                    $application->Country = $params['country'];
                    $application->Pin = $params['pin'];
                    $application->Phone = $params['phone'];
                    $application->Email = $params['email'] ? $params['email'] : '';
                    $application->created_by = $uid;
                    $application->created_on = time();
                    $application->modified_by = $uid;
                    $application->modified_on = time();


//                    print_r($application);exit;
                    $feegrpname = 'Application';
                    if ($application->save()) {


                        $feetypdet = RecordTypes::findFirstByRecordName(strtoupper($feegrpname) . ' FEES');
                        $message['type'] = 'success';
                        $message['ItemID'] = $application->id;
                        $message['recordTypId'] = $feetypdet->id;
//                        $message['message'] = '<div class="alert alert-block alert-success fade in">Application issued Successfully</div>';
                        $message['message'] = 'Application issued Successfully!';
                        print_r(json_encode($message));
                        exit;
                    } else {
                        foreach ($application->getMessages() as $messages) {
                            $error .= $messages;
                        }
                        $message['type'] = 'error';
//                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        $message['message'] = $error;
                        print_r(json_encode($message));
                        exit;
                    }
                } else {

                    $error = '';
                    foreach ($this->request->getPost() as $key => $val) {
                        foreach ($form->getMessagesFor($key) as $messages) {
                            $error .= $messages . "\n";
                        }
                    }
                    $message['type'] = 'error';
//                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
//            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        }
    }

    public function changeApprovalStatusAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;

        $message = array();
        try {
            if ($this->request->isPost()) {

                $appln = Application::findFirstById($this->request->getPost('itemID'));
                $appln->status = $this->request->getPost('status');

                $voucheramount = $this->request->getPost('voucheramount');
                if ($voucheramount > 0 && $voucheramount != '') {
                    $appln->application_fee = $voucheramount;
                    $appln->application_fee_status = 'Paid';
                    $appln->modified_by = $uid;
                    $appln->modified_on = time();
                }
                if ($appln->save()) {
                    $message['type'] = 'success';
//                    $message['message'] = '<div class="alert alert-block alert-success fade in">' . $appln->status . ' Successfully</div>';
                    $message['message'] = $appln->status . ' Successfully!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    foreach ($appln->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
//                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
//                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        }
    }

    public function applicationTblHdrAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Application | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/application.js');
        $this->assets->addJs('js/appscripts/admission.js');
        $this->assets->addJs('js/application/manageApplication.js');
//        $this->view->divMName = ControllerBase::get_division_name_student();

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function getAllApplicationsAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadApplicationDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = $queryParams1 = array();
        $orderphql = array();
        foreach ($this->request->getPost() as $key => $value) {

            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'aggregate' && $value) {
                $params['aggregateids'][] = $value;
            } else {

                $params[$key] = $value;
            }
        }

        if ($params['appno'] != '' && isset($params['appno'])) {
            $queryParams[] = 'application_no Like "%' . $params['appno'] . '%"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }

        if ($params['name'] != '' && isset($params['name'])) {
            $queryParams[] = 'Student_Name Like "%' . $params['name'] . '%"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }

        if (isset($params['aggregateids']) && count($params['aggregateids']) > 0):
            $res = ControllerBase::buildStudentQuery(implode(',', $params['aggregateids']));
            $queryParams[] = ' (' . implode(' or ', $res) . ') ';
        endif;

        if ($params['issuefrm'] != '' && isset($params['issuefrm'])) {
            $ifstart = strtotime($params['issuefrm'] . ' 00:00:00');
            $queryParams[] = 'created_on >= "' . $ifstart . '"';
        }

        if ($params['issueto'] != '' && isset($params['issueto'])) {
            $ifend = strtotime($params['issueto'] . ' 23:59:59');
            $queryParams[] = 'created_on <= "' . $ifend . '"';
        }

        if ($params['dojfrm'] != '' && isset($params['dojfrm'])) {
            $ifstart = strtotime($params['dojfrm'] . ' 00:00:00');
            $queryParams[] = 'Date_of_Joining >= "' . $ifstart . '"';
        }
        if ($params['dojto'] != '' && isset($params['dojto'])) {
            $ifend = strtotime($params['dojto'] . ' 23:59:59');
            $queryParams[] = 'Date_of_Joining <= "' . $ifend . '"';
        }

        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $queryParams1[] = 'application_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'Student_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;


        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $orderphql[] = $this->getSortColumnNameForApplication($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }
        $order = implode(',', $orderphql);
        $applications = Application::find(array(
                    implode(' and ', $queryParams),
                    "order" => $order,
                    "limit" => array(
                        "number" => $this->request->getPost('iDisplayLength'),
                        "offset" => $this->request->getPost('iDisplayStart')
                    )
        ));
        $totapplications = Application::find(array(
                    implode(' and ', $queryParams),
                    "columns" => "COUNT(*) as cnt",
        ));

        $rowEntries = $this->formatApplicationTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => intval($totapplications[0]->cnt),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForApplication($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "application_no";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "application_no";
            case 1:
                return "Student_Name";
            case 2:
                return "Gender";
            case 3:
                return "Date_of_Birth";
            case 4:
                return "Date_of_Joining";
            case 5:
                return "status";
            case 6:
                return "key_in_status";
            default:
                return "application_no";
        }
    }

    public function formatApplicationTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $labelclass = (in_array($items->status, array('Admitted', 'Approved'))) ? 'success' : (($items->status == 'Rejected') ? 'danger' :
                                (($items->status == 'Issued') ? 'warning' : 'primary'));
                $dataclass = ($items->key_in_status == 'Completed') ? 'success' :
                        (($items->key_in_status == 'In Progress') ? 'danger' : 'info');
                $row['application_id'] = $items->id ? $items->id : '';
                $row['address'] = $items->Address1 . ',' . $items->Address2;
                $row['mobile'] = $items->Phone ? $items->Phone : '';
                $row['Email'] = $items->Email ? $items->Email : '';
                $row['application_no'] = $items->application_no ? $items->application_no : '';
//                $row['academicYr'] = AcademicYearMaster::findFirstById($items->academic_year_id)->academic_year_name;
                $row['Student_Name'] = $items->Student_Name ? $items->Student_Name : '';
                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';
//                $row['class'] = DivisionValues::findFirstById($items->Division_Class)->classname;
                $row['dob'] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row['doj'] = ($items->Date_of_Joining) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row['labelclass'] = $labelclass;
                $row['status'] = $items->status;
                $row['dataclass'] = $dataclass;
                $row['issuedon'] = $items->created_on ? date('d-m-Y', $items->created_on) : '';
                $staffissued = StaffInfo::findFirstById($items->created_by);
                $row['issuedby'] = $staffissued->Staff_Name . '<p class="text text-muted small">(' . $staffissued->appointment_no . ')<p>';

                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';

                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }


                $studInfo = StudentInfo::findFirstByApplicationNo($items->application_no);
                if ($items->status != 'Rejected')
                    $row['key_in_status'] = $items->key_in_status;
                else
                    $row['key_in_status'] = "";
                $row['Actions'] = '';
                if (in_array($items->status, array('Approved'))):
                    $row['Actions'] = '<label class="label label-primary pull-right" type="button"
                                                    onclick="applicationSettings.admitStudent(this)"  
                                                    id="stu_admit">Admit</label>';
                endif;
                if (in_array($items->status, array('Issued'))):
                    $row['Actions'] = '<label class="label label-info pull-right" type="button"
                                                    onclick="applicationSettings.forwardDetails(' . $items->id . ')"  
                                                    id="forward_det">Forward</label>';
                endif;
                if (!in_array($items->key_in_status, array('Completed')) && in_array($items->status, array('Admitted'))):
//                    $row['Actions'] = '<a href="' . $this->url->get("student-profile/keyInStudentData", array("application_no" => $items->application_no)) . '"> 
//                                                <span class="fa fa-pencil"  title="Key-In-Data">                                         
//                                                </span>
//                                            </a>  ';

                    $row['Actions'] = '<a href="javascript:;" onclick="applicationSettings.loadeditprofile(' . $studInfo->id . ')"> 
                                                <span class="fa fa-edit mini-stat-icon-action pink"  title="Edit Profile">                                         
                                                </span>
                                            </a>  ';
                endif;

                $row['Print'] = '-';
                $row['feestatus'] = $items->application_fee_status ? $items->application_fee_status : 'Unpaid';
                $feeclass = $items->application_fee_status ? 'success' : 'danger';
                $row['statusdet'] = '
<label data-original-title="Status" style="font-size:12px;padding: 7px;"

        data-content="<table class=\'table table-striped table-hover table-bordered\'
        style=\'font-size:12px;\'>
        <tbody>
        <tr>
        <td>Application status</td>
        <td><span class = \'text text-' . $labelclass . '\'> ' . $items->status . '</span></td>
        </tr>

        <tr>
        <td>Key-in data status</td>
        <td><span class = \'text text-' . $dataclass . '\'>' . $row['key_in_status'] . '</span></td>
        </tr>

        <tr>
        <td>Application Fee Status</td>
        <td><span class = \'text text-' . $feeclass . '\'>' . $row['feestatus'] . '</span></td>
        </tr>
        </tbody>                    
        </table>"
            data-placement="top" data-trigger="hover" class="label label-' . $labelclass . ' popovers">' . $items->status . '</label>  ';
                if ($items->application_fee_status == 'Paid') {
                    $row['Print'] = '<a target="_blank" href="' . $this->url->get() . 'template/applicationfeeReceipt?applicationid=' . $items->id . '" >
                              <i class="fa fa-print hide"></i> </a>';
                } else {
                    $row['Print'] = '<span class="label label-success hide" id="pay_fee" onclick="applicationSettings.loadPayForm(' . $items->id . ')" style="cursor:pointer;">Pay</span>';
                }

                $row['Student_Name'] = ' <span title="' . $items->application_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;" >' . substr($items->Student_Name, 0, 1) . '</span>';

                $row['Student_Name'] .= '<span class="name" title="' . $items->Student_Name . '"> 
                                      ' . strtolower($items->Student_Name) . '</span>';


                //voucher details
                //$ledgervoucherdet = $items->id ? LedgerVoucher::findfirst( $items->id ): '';


                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    public function viewApplicationByIDAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Application | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/application.js');
        if ($this->request->isPost()) {
            $appId = $this->request->getPost('appID');
            $identity = $this->auth->getIdentity();
            $this->view->uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
            $this->view->application = Application::findFirstById($appId);
            $this->view->form = new ApplicationForm($this->view->application, array(
                'view' => true
            ));
        }
    }

    public function isCheckModuleAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $resModules = ControllerBase::getAllModules();

        $applicationid = $this->request->getPost('applicationid') ? $this->request->getPost('applicationid') : '';
        $message = array();

        if ($applicationid != '') {
            $appdet = Application::findFirstById($applicationid);
            $feegrpname = 'Application';
            $feetypdet = RecordTypes::findFirstByRecordName(strtoupper($feegrpname) . ' FEES');
            $message['status'] = $appdet->status;
            $message['ItemID'] = $applicationid;
            $message['recordTypId'] = $feetypdet->id;
        }

        $modules = (array) $resModules->modules;
        if (in_array('Fee', $modules)) {
            $message['type'] = 'yes';
        } else {
            $message['type'] = 'no';
        }

        echo json_encode($message);
        exit;
    }

    public function payApplicationFeeFormAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Application | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/application.js');

        $feetypename = strtoupper('Application');
        $record_typid = RecordTypes::findfirst("record_name = '$feetypename FEES'")->id;
        $record_typsetting = RecordTypesSettings::findfirst('record_types_id =' . $record_typid);
        $this->view->record_typsetting = $record_typsetting;


        $this->view->record_typ_id = $record_typid;
        $this->view->voucher_type = 'Receipt Voucher';

        //debit ledgerid
        $debitldgrText = 'cash';

        $debitldgrconditions = "name LIKE :searchtxt:";
        $debitldgrparameters = array(
            "searchtxt" => "%" . $debitldgrText . "%"
        );


        $debitledgers = OrganizationalStructureValues::findFirst(array(
                    $debitldgrconditions,
                    "bind" => $debitldgrparameters
        ));

        $this->view->debitldgr = $debitdview = $this->findsimpleparentledger('', $debitledgers->id, '', '');
        $this->view->debitldgrid = $debitledgers->id;


        //  credit ledgerid
        $creditldgrText = 'Application Fees';

        $creditldgrconditions = "name LIKE :searchtxt:";
        $creditldgrparameters = array(
            "searchtxt" => "%" . $creditldgrText . "%"
        );

        $creditledgers = OrganizationalStructureValues::findFirst(array(
                    $creditldgrconditions,
                    "bind" => $creditldgrparameters
        ));

        $this->view->creditldgr = $creditdview = $this->findsimpleparentledger('', $creditledgers->id, '', '');
        $this->view->creditldgrid = $creditledgers->id;
    }

    public function voucherReceiptAction() {
        $this->view->setRenderLevel(VIEW::LEVEL_ACTION_VIEW);

        $this->view->schoolinfo = ControllerBase::_getSchoolVariables();

        $applicationid = $this->request->get('applicationid');

        $studentAcdInfo = Application::findfirst('id = ' . $applicationid);

        //$stuFeeinfo = StudentFeeTable::findFirstById($student_fee_id);



        $exploded_path = $studentAcdInfo->aggregate_key ? explode(',', $studentAcdInfo->aggregate_key) : '';
        if ($exploded_path != '') {
            $cycle_node_value = OrganizationalStructureValues::findfirst('id = ' . $exploded_path[0]);
            $path_name = '';
            foreach ($exploded_path as $path) {
                $value = OrganizationalStructureValues::findfirst('id = ' . $path)->name;
                $path_name = $path_name . '>>' . $value;
            }
        }


        $recptvouchinfo = LedgerVoucher::findFirstByRecordTypeItemId($applicationid);

        $this->view->student = $stuInfo;
        $this->view->studentAcdInfo = $studentAcdInfo;
        //   $this->view->feename = $feename->fee_name;
        $this->view->classname = ltrim($path_name, '>>');
        $this->view->amountinwords = ControllerBase::_convertWords($studentAcdInfo->application_fee);

        $this->view->stuFeeinfo = $stuFeeinfo;
        $this->view->recptvouchinfo = $recptvouchinfo;
    }

    //functions


    public function _getNonMandNodesForAssigning($id, $nodes = array()) {
        $fields = OrganizationalStructureMaster::find('parent_id=' . $id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
                if ($field->is_subordinate != 1) {
                    $nodes[$field->id] = $field->name;
                    // } else {
                    $nodes = $this->_getNonMandNodesForAssigning($field->id, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function _getMandNodesForAssigning($acdyrMas, $nodes = array()) {

        $nodes[$acdyrMas->id] = $acdyrMas->name;
        //  echo $acdyrMas->name;
        $fields = OrganizationalStructureMaster::find('parent_id=' . $acdyrMas->id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
                if ($field->is_subordinate != 1 && $field->mandatory_for_admission == 1) {
//                    $nodes[$field->id] = $field->name;
                    $nodes = $this->_getMandNodesForAssigning($field, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function findsimpleparentledger($space = '', $lid, $choutput = '', $temp = '') {
        $choutput.=$space . SearchController::getledgernamefor($lid) . $temp;
        $parentid = OrganizationalStructureValues::findFirstById($lid)->parent_id;
        //foreach ($chld as $chl) {
        if (!empty($parentid))
            $choutput = SearchController::findsimpleparentledger('>>', $parentid, $choutput, '');
        //}

        return $choutput;
    }

    public function getledgernamefor($ledgerid) {
        $ledgname = OrganizationalStructureValues::findFirstById($ledgerid)->name;
        if (!empty($ledgname))
            return $ledgname;
    }

    public function generateApplExcelAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = $queryParams1 = array();
        $orderphql = array();
        foreach ($this->request->getPost() as $key => $value) {

            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'aggregate' && $value) {
                $params['aggregateids'][] = $value;
            } else {

                $params[$key] = $value;
            }
        }
// echo '<pre>';print_r($params);exit;
        $reports = array();
        $title[] = array('Application Search Criteria ');

        if ($params['appno'] != '' && isset($params['appno'])) {
            $title[] = array('Application No :', $params['appno']);
            $queryParams[] = 'application_no Like "%' . $params['appno'] . '%"';
        }

        if ($params['name'] != '' && isset($params['name'])) {
            $title[] = array('Student Name :', $params['name']);
            $queryParams[] = 'Student_Name Like "%' . $params['name'] . '%"';
        }

        if (isset($params['aggregateids']) && count($params['aggregateids']) > 0):
            $res = ControllerBase::buildStudentQuery(implode(',', $params['aggregateids']));
            $queryParams[] = ' (' . implode(' or ', $res) . ') ';
        endif;
        if (($params['issuefrm'] != '' && isset($params['issuefrm'])) || ($params['issueto'] != '' && isset($params['issueto']))) {
            $title[] = array('Issued date :', 'From', $params['issuefrm'], 'To', $params['issueto']);
        }
        if ($params['issuefrm'] != '' && isset($params['issuefrm'])) {
            $ifstart = strtotime($params['issuefrm'] . ' 00:00:00');
            $queryParams[] = 'created_on >= "' . $ifstart . '"';
        }

        if ($params['issueto'] != '' && isset($params['issueto'])) {
            $ifend = strtotime($params['issueto'] . ' 23:59:59');
            $queryParams[] = 'created_on <= "' . $ifend . '"';
        }

        if (($params['dojfrm'] != '' && isset($params['dojfrm'])) || ($params['dojto'] != '' && isset($params['dojto']))) {
            $title[] = array('Date Of Admission :', 'From', $params['dojfrm'], 'To', $params['dojto']);
        }
        if ($params['dojfrm'] != '' && isset($params['dojfrm'])) {
            $ifstart = strtotime($params['dojfrm'] . ' 00:00:00');
            $queryParams[] = 'Date_of_Joining >= "' . $ifstart . '"';
        }
        if ($params['dojto'] != '' && isset($params['dojto'])) {
            $ifend = strtotime($params['dojto'] . ' 23:59:59');
            $queryParams[] = 'Date_of_Joining <= "' . $ifend . '"';
        }

        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $title[] = array('Search text :', $params['sSearch']);
            $queryParams1[] = 'application_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'Student_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;

        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $orderphql[] = $this->getSortColumnNameForApplication($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }
        $order = implode(',', $orderphql);

        $totapplications = Application::find(array(
                    implode(' and ', $queryParams),
                    "columns" => "COUNT(*) as cnt",
        ));
        $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $mandnode = $this->_getMandNodesForAssigning($acdyrMas);
        foreach ($params['aggregateids'] as $aggvalue) {
            $orgva = OrganizationalStructureValues::findFirstById($aggvalue);
            $orgm = OrganizationalStructureMaster::findFirstById($orgva->org_master_id);
            $title[] = array($orgm->name, $orgva->name);
        }
        $title[] = array('');
        $title[] = array('Application Details ');
//        echo '<pre>';  print_r($title);  exit;
        $header = array(
            'Applicant Name', 'Application No.', 'Gender'
        );

        foreach ($mandnode as $node) {
            $header[] = ucfirst($node);
        }
        $header[] = 'Date of Birth';
        $header[] = 'Issued Date';
        $header[] = 'Admitted date';
        $header[] = 'Application Status';
        $header[] = 'Key-in data Status';
        $header[] = 'Application fee Status';

        $header[] = 'Address1';
        $header[] = 'Address2';
        $header[] = 'State';
        $header[] = 'Country';
        $header[] = 'Pin';
        $header[] = 'Phone';
        $header[] = 'Email';
        $tottalrescount = intval($totapplications[0]->cnt);
        $x = 0;
        $csvlimit = 500;
        while ($x < $tottalrescount) {
            $remaincount = $tottalrescount - $x;
            if ($remaincount < $csvlimit)
                $limit = $remaincount;
            else
                $limit = $csvlimit;
            $offset = $x;
            $params['limit'] = $limit;
            $params['offset'] = $offset;
            $applications = Application::find(array(
                        implode(' and ', $queryParams),
                        "order" => $order,
                        "limit" => array(
                            "number" => $params['limit'],
                            "offset" => $params['offset']
                        )
            ));

            $exerows = $this->formatApplicationTableDataexcel($applications);
            $reports[] = $exerows;
            $x = $x + $limit;
        }
//         echo '<pre>';print_r($reports);exit;
        $filename = "ApplicationReport-" . date('m-d-Y H:i:s') . ".csv";
        ControllerBase::exportExcel($filename, $reports, $header, $title);
    }

  
    public function formatApplicationTableDataexcel($result) {
        $rowEntries = array();
        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $row['Student_Name'] = $items->Student_Name ? $items->Student_Name : '';
                $row['application_no'] = $items->application_no ? $items->application_no : '';
                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';
                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';
                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }

                $row['dob'] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row['issuedon'] = $items->created_on ? date('d-m-Y', $items->created_on) : '';
                $row['doj'] = ($items->Date_of_Joining) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row['status'] = $items->status;
                $row['key_in_status'] = ($items->status != 'Rejected') ? $items->key_in_status : "";
                $row['feestatus'] = $items->application_fee_status ? $items->application_fee_status : 'Unpaid';

                $row['Address1'] = $items->Address1 ? $items->Address1 : '';
                $row['Address2'] = $items->Address2 ? $items->Address2 : '';
                $row['State'] = $items->State ? $items->State : '';
                $row['Country'] = $items->Country ? $items->Country : '';
                $row['Pin'] = $items->Pin ? $items->Pin : '';
                $row['Phone'] = $items->Phone ? $items->Phone : '';
                $row['Email'] = $items->Email ? $items->Email : '';

                $rowEntries[] = array_values($row);
            endforeach;
        }
        return $rowEntries;
    }

}
