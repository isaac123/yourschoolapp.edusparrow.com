
<?php

use Phalcon\Mvc\View;

class ReportsController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu");
        $this->view->setTemplateAfter('private');
    }

    public $mandnode;
    public $staffnodes = array();
    public $mandnodeFortransprt;
    public $feenode;

    public function getStudentsReportAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();

        $aggregateval = '';
        //  $studata=$this->request->getPost('studata');

        $studata = json_decode($this->request->getPost('studata'));

        foreach ($studata as $key => $value) {
            $IsSubdiv = explode('_', $key);

            if ($IsSubdiv[0] == 'aggregate' && $value) {
                $params['aggregateids'][] = $value;
            } else {

                $params[$key] = $value;
            }
        }


        if (isset($params['aggregateids']) && count($params['aggregateids']) > 0):
            foreach ($params['aggregateids'] as $value) {
                $queryParams[] = 'FIND_IN_SET (' . $value . ', stuhistory.aggregate_key ) ';
            }
        //$queryParams[] = 'stuhistory.aggregate_key Like "%' . implode(',', $params['aggregateids']) . '%"';
        endif;

        if (isset($params['loginid']) && $params['loginid'] != ''):
            $queryParams[] = 'stuinfo.loginid  Like "%' . $params['loginid'] . '%"';
        endif;

        if (isset($params['name']) && $params['name'] != ''):
            $queryParams[] = 'stuinfo.Student_Name  Like "%' . $params['name'] . '%"';
        endif;

        if (isset($params['status']) && $params['status'] != ''):
            $queryParams[] = 'stuhistory.status  Like "%' . $params['status'] . '%"';
        endif;

        $whereQuery = count($queryParams) > 0 ? ('WHERE ' . implode(' and ', $queryParams)) : '';



        $getlimit = $this->request->get('limit');

        $phql_full = 'SELECT COUNT(*) as cnt
            FROM StudentInfo stuinfo
            JOIN StudentHistory stuhistory ON stuinfo.id=stuhistory.student_info_id ' . $whereQuery;
        $expectedcount = $this->modelsManager->executeQuery($phql_full);
        $tottalrescount = $expectedcount[0]['cnt'];
        $x = 0;

//print_r($phql_full);
//            exit;

        $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->mandnode = $this->_getMandNodesForAssigning($acdyrMas);

        $header = $reports = array();
//                'Student Name', 'Admission No', 'Date of Birth', 'Gender',

        $header[] = 'Student Name';
        $header[] = 'Admission No';
        $header[] = 'Application No';
        $header[] = 'Date of Birth';
        $header[] = 'Gender';

        foreach ($this->mandnode as $node) {
            $header[] = ucfirst($node);
        }

        $header[] = 'Student Login';
        $header[] = 'Parent Login';
        $header[] = 'Date Of Joining';
        $header[] = 'Father Name';
        $header[] = 'Father Occupation';
        $header[] = 'Father Designation';
        $header[] = 'Father Phone No';
        $header[] = 'Father Phone No Status';
        $header[] = 'Father Education';
        $header[] = 'Mother Name';
        $header[] = 'Mother Occupation';
        $header[] = 'Mother Designation';
        $header[] = 'Mother Phone No';
        $header[] = 'Mother Phone No Status';
        $header[] = 'Mother Education';
        $header[] = 'Other Guardian Name';
        $header[] = 'Guardian Occupation';
        $header[] = 'Guardian Designation';
        $header[] = 'Guardian Phone No';
        $header[] = 'Guardian Phone No Status';
        $header[] = 'Family Income';
        $header[] = 'Family Doctor';
        $header[] = 'Address1';
        $header[] = 'Address2';
        $header[] = 'State';
        $header[] = 'Country';
        $header[] = 'Pin';
        $header[] = 'Phone';
        $header[] = 'Email';
        $header[] = 'Edusparrow Email';
        $header[] = 'Place of Birth';
        $header[] = 'Nationality';
        $header[] = 'Religion';

        $header[] = 'Caste Category';
        $header[] = 'Caste';
        $header[] = 'First Language';
        $header[] = 'Person School Fee';
        $header[] = 'Circumtances';
        $header[] = 'Sibling';
        $header[] = 'Blood Group';
        $header[] = 'Height';
        $header[] = 'Weight';
        $header[] = 'Allergic';
        $header[] = 'Chronic';
        $header[] = 'Previous School Name';
        $header[] = 'Previous School State';
        $header[] = 'Previous School Country';
        $header[] = 'Attended From';
        $header[] = 'Attended To';
        $header[] = 'Achievements';
        $header[] = 'Comments';
        $header[] = 'Other Details';
        $header[] = 'Transport';
        $header[] = 'Status';

        $getlimit = $params['limit'];
        // echo $limit;

        while ($x < $tottalrescount) {

            $remaincount = $tottalrescount - $x;
            //  echo ($remaincount) . "<br/>" . $tottalrescount . "<br/>";
            if ($remaincount < $getlimit)
                $limit = $remaincount;
            else
                $limit = $getlimit;

            $offset = $x;

            $phql_limited = 'SELECT stuinfo.id,stuinfo.Admission_no,stuinfo.application_no,stuinfo.Student_Name,
                                stuinfo.loginid,stuinfo.parent_loginid,stuinfo.Date_of_Birth,stuinfo.Gender,
                                stuhistory.aggregate_key,stuhistory.status,stuinfo.rollno,stuinfo.Date_of_Joining,
                                 stuinfo.Address1,stuinfo.Address2,stuinfo.State,stuinfo.Country,stuinfo.Pin,
                                 stuinfo.Phone,stuinfo.Email,stuinfo.place_of_birth,stuinfo.nationality,
                                  stuinfo.religion,stuinfo.caste_category,stuinfo.caste,
                                  stuinfo.first_language,stuinfo.father_name,stuinfo.f_occupation,stuinfo.f_designation,
                                  stuinfo.f_phone_no,stuinfo.f_phone_no_status,stuinfo.f_education,stuinfo.mother_name,
                                   stuinfo.m_occupation,stuinfo.m_designation,stuinfo.m_phone_no,stuinfo.m_phone_no_status,
                                  stuinfo.m_education,stuinfo.other_guardian_name,stuinfo.g_occupation,stuinfo.g_designation,
                                  stuinfo.g_phone,stuinfo.g_phone_no_status,stuinfo.family_income,stuinfo.person_school_fee,
                                   stuinfo.circumtances,stuinfo.sibling,stuinfo.blood_group,stuinfo.height,
                                   stuinfo.weight,stuinfo.allergic,stuinfo.chronic,stuinfo.family_doctor,
                                  stuinfo.previous_school_name,stuinfo.previous_school_state,stuinfo.previous_school_country,
                                  stuinfo.attended_from,stuinfo.attended_to,stuinfo.achievements,stuinfo.comments,
                                  stuinfo.other_details,stuinfo.transport
                                FROM StudentInfo stuinfo
                                JOIN StudentHistory stuhistory ON stuinfo.id=stuhistory.student_info_id ' . $whereQuery . ' ORDER BY stuinfo.Student_Name LIMIT ' . $limit . ' OFFSET ' . $offset;

//print_r($phql_limited);
//           exit; 
            $result = $this->modelsManager->executeQuery($phql_limited);

            $x = $x + $limit;

            $reports[] = $this->formatStudentList($result);
        }

//            print_r($reports);
//            exit;

        $filename = 'Student_List_' . date('d-m-Y') . '.csv';

        $counter = 0;
        $unlink_file = DOWNLOAD_DIR . 'Student_List_' . date('d-m-Y') . '.csv';
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }

        $fp = fopen(DOWNLOAD_DIR . $filename, 'a');

        $delimiter = ",";
        fputcsv($fp, $header, $delimiter);
        foreach ($reports as $chunkrep) {
            $fp = fopen(DOWNLOAD_DIR . $filename, 'a');
            foreach ($chunkrep as $row) {
                fputcsv($fp, $row, $delimiter);
            }
        }
        fclose($fp);

        $file = DOWNLOAD_DIR . $filename;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function formatStudentList($result) {
        $reports = array();

        if (count($result) > 0) {
            foreach ($result as $items) {

                $row = array();

                $row[] = $items->Student_Name;
                $row[] = $items->Admission_no;
                $row[] = $items->application_no ? $items->application_no : '-';
                $row[] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '-';
                $row[] = ($items->Gender == 1) ? 'Female' : 'Male';


                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';
                $class_arr = array();
                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $class_arr[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }

                    foreach ($this->mandnode as $key => $mandnodeval) {
                        $row[] = isset($class_arr[$key]) ? $class_arr[$key] : '-';
                    }
                }
                $row[] = $items->loginid;
                $row[] = $items->parent_loginid;
                $row[] = ($items->Date_of_Joining) ? date('d-m-Y', $items->Date_of_Joining) : '-';
                $row[] = $items->father_name ? $items->father_name : '-';
                $row[] = $items->f_occupation ? $items->f_occupation : '-';
                $row[] = $items->f_designation ? $items->f_designation : '-';
                $row[] = $items->f_phone_no ? $items->f_phone_no : '-';
                $row[] = $items->f_phone_no_status ? (($items->f_phone_no_status == 1) ? 'Primary' : 'Secondary') : '-';
                $row[] = $items->f_education ? $items->f_education : '-';
                $row[] = $items->mother_name ? $items->mother_name : '-';
                $row[] = $items->m_occupation ? $items->m_occupation : '-';
                $row[] = $items->m_designation ? $items->m_designation : '-';
                $row[] = $items->m_phone_no ? $items->m_phone_no : '-';
                $row[] = $items->m_phone_no_status ? (($items->m_phone_no_status == 1) ? 'Primary' : 'Secondary') : '-';
                $row[] = $items->m_education ? $items->m_education : '-';
                $row[] = $items->other_guardian_name ? $items->other_guardian_name : '-';
                $row[] = $items->g_occupation ? $items->g_occupation : '-';
                $row[] = $items->g_designation ? $items->g_designation : '-';
                $row[] = $items->g_phone ? $items->g_phone : '-';
                $row[] = $items->g_phone_no_status ? (($items->g_phone_no_status == 1) ? 'Primary' : 'Secondary') : '-';
                $row[] = $items->family_income ? $items->family_income : '-';
                $row[] = $items->family_doctor ? $items->family_doctor : '-';
                $row[] = $items->Address1 ? $items->Address1 : '-';
                $row[] = $items->Address2 ? $items->Address2 : '-';
                $row[] = $items->State ? $items->State : '-';
                $row[] = $items->Country ? CountryMaster::findfirst('country_code ="' . $items->Country . '"')->country_name : '-';
                $row[] = $items->Pin ? $items->Pin : '-';
                $row[] = $items->Phone ? $items->Phone : '-';
                $row[] = $items->Email ? $items->Email : '-';
                $row[] = $items->loginid ? $items->loginid . '@' . SUBDOMAIN . '.edusparrow.com' : '-';
                $row[] = $items->place_of_birth ? $items->place_of_birth : '-';
                $row[] = $items->nationality ? CountryMaster::findfirst('country_code ="' . $items->nationality . '"')->country_name : '-';
                $row[] = $items->religion ? $items->religion : '-';

                $row[] = $items->caste_category ? $items->caste_category : '-';
                $row[] = $items->caste ? $items->caste : '-';
                $row[] = $items->first_language ? $items->first_language : '-';
                $row[] = $items->person_school_fee ? $items->person_school_fee : '-';
                $row[] = $items->circumtances ? $items->circumtances : '-';
                $row[] = $items->sibling ? $items->sibling : '-';
                $row[] = $items->blood_group ? $items->blood_group : '-';
                $row[] = $items->height ? $items->height : '-';
                $row[] = $items->weight ? $items->weight : '-';
                $row[] = $items->allergic ? $items->allergic : '-';
                $row[] = $items->chronic ? $items->chronic : '-';
                $row[] = $items->previous_school_name ? $items->previous_school_name : '-';
                $row[] = $items->previous_school_state ? $items->previous_school_state : '-';
                $row[] = $items->previous_school_country ? CountryMaster::findfirst('country_code ="' . $items->previous_school_country . '"')->country_name : '-';
                $row[] = $items->attended_from ? date('d-m-Y', $items->attended_from) : '-';
                $row[] = $items->attended_to ? date('d-m-Y', $items->attended_to) : '-';
                $row[] = $items->achievements ? $items->achievements : '-';
                $row[] = $items->comments ? $items->comments : '-';
                $row[] = $items->other_details ? $items->other_details : '-';
                $row[] = $items->transport ? (($items->transport == 2) ? 'Yes' : 'No') : '-';



                $row[] = $items->status;

                $reports[] = $row;
            }
        }
        return $reports;
    }

    public function getRatingReportAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();

        $aggregateval = '';
        //  $studata=$this->request->getPost('studata');

        $ratingdata = json_decode($this->request->getPost('params'));
        foreach ($ratingdata as $key => $value) {
            $params[$key] = $value;
        }
        $master_id = $params['subject_masterid'];
        $combiType = $params['type'];
        $subject_module = $params['subject_module'] ? $params['subject_module'] : '';

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        if ($combiType == 'subject') {
//            $grpSubjTeach = GroupSubjectsTeachers::findfirst('id = ' . $master_id);
            $classmaster = ClassroomMaster::findFirstById($master_id);
            $this->view->node_id = $classmaster->aggregated_nodes_id;
            $this->view->node_name = ControllerBase::getNameForKeys($classmaster->aggregated_nodes_id);
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);

////            $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
//            $orgvaldet = OrganizationalStructureValues::findFirstById($grpSubjTeach->subject_id);
//            $subj_arr = $this->_getMandNodesForExam($orgvaldet);
//            $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", stumap.aggregate_key)>0)', array_keys($subj_arr));
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stuinfo.Student_Name,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status'
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                    . ' and (' . implode(' or ', $res). ') and (' . implode(' and ', $subjQuery) . ')'
                    . ' ORDER BY stuinfo.Admission_no ASC';

            $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);
        } else {
            $grpSubjTeach = GroupClassTeachers::findfirst('id = ' . $master_id);
            $this->view->type = 'class';
            $this->view->subject = '';
            $subjectid = '';
            $classmaster = ClassroomMaster::findFirstById($grpSubjTeach->classroom_master_id);
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//            $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stuinfo.Student_Name,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status'
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" '
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                    . 'and (' . implode(' or ', $res). ') '
                    . ' ORDER BY stuinfo.Admission_no ASC';

            $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);
        }
        $header = $rowArr = $yaxis = array();
        foreach ($students as $stu) {
            $ratingCategorys = RatingCategoryMaster::find();
            foreach ($ratingCategorys as $ratingCategory) {
                $ratingPoints = 0;
                $ratingValues = RatingCategoryValues::find('rating_category = ' . $ratingCategory->id);
                $ratTotPoints = $ratingCategory->category_weightage;
                foreach ($ratingValues as $rvalue) {
                    $studentRating = ($combiType == 'subject') ? StudentSubteacherRating::findFirst(
                                    ' subject_master_id =' . $grpSubjTeach->id
                                    . ' and rating_category =' . $ratingCategory->id
                                    . ' and rating_value = ' . $rvalue->id
                                    . ' and subjct_modules = ' . $subject_module
                                    . ' and student_id = ' . $stu->student_info_id
                            ) :
                            StudentClassteacherRating::findFirst(
                                    'class_master_id =' . $grpSubjTeach->id
                                    . ' and rating_category =' . $ratingCategory->id
                                    . ' and rating_value = ' . $rvalue->id
                                    . ' and student_id = ' . $stu->student_info_id
                    );

                    if ($studentRating->rating_id > 0) {
                        $ratingPoints += ($rvalue->rating_level_value / 100) * $ratingCategory->category_weightage;
                    }
                }



                $rowArr[$stu->student_info_id][$ratingCategory->id] = array(
                    'rating_name' => $ratingCategory->category_name,
                    'ratingPoints' => $ratingPoints,
                    'StudentName' => $stu->Student_Name,
                    'aggreegatekey' => $stu->aggregate_key
                );

                $yaxis[$ratingCategory->id] = $ratingCategory->category_name . " ($ratTotPoints pts)";

                $yaxis[$ratingCategory->id] = array(
                    'categoryname' => $ratingCategory->category_name,
                    'ratPoints' => $ratTotPoints);
            }
        }

        $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');

        $nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);

        $this->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
        if ($combiType == 'subject') {
            $orgvaldet = OrganizationalStructureValues::findFirstById($grpSubjTeach->subject_id);

            $subject_keys = ReportsController::_getMandNodesForSubject($orgvaldet);

            $subject_keys = array_reverse($subject_keys);
        }
//   print_r($subject_keys);exit;     

        $header[] = 'Student Name';
//        array_shift($this->mandnode);
//        print_r($this->mandnode);exit;
        $clsscnt = 0;
        foreach ($this->mandnode as $node) {
            if ($clsscnt != 0) {
                $header[] = ucfirst($node);
            }
            $clsscnt++;
        }

        foreach ($yaxis as $yaxisval) {
            $header[] = $yaxisval['categoryname'];
        }

        $subject_head = array();
        $icnt = 0;
        foreach ($header as $headerval) {
            if ($icnt == 0) {
                $subject_head[] = 'Subject : ' . implode(' >> ', $subject_keys);
            } else {
                $subject_head[] = '';
            }
            $icnt++;
        }

        $reportdata = array();
        foreach ($rowArr as $stuarr) {
            $reportval = array();
            $stu_cnt = 0;
            foreach ($stuarr as $stuarrval) {
                if ($stu_cnt == 0) {
                    $reportval[] = $stuarrval['StudentName'];

                    $aggregatevals = $stuarrval['aggreegatekey'] ? explode(',', $stuarrval['aggreegatekey']) : '';
                    $class_arr = array();
                    $aggcnt = 0;
                    if ($aggregatevals != '') {
                        foreach ($aggregatevals as $aggregateval) {
                            if ($aggcnt != 0) {
                                $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                                $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                                $class_arr[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                            }
                            $aggcnt++;
                        }
//                        array_shift($class_arr);
                        $aggcntval = 0;
                        foreach ($this->mandnode as $key => $mandnodeval) {
                            if ($aggcntval != 0) {
                                $reportval[] = isset($class_arr[$key]) ? $class_arr[$key] : '-';
                            }
                            $aggcntval++;
                        }
                    }
                }
                $reportval[] = $stuarrval['ratingPoints'];
                $stu_cnt++;
            }
            $reportdata[] = $reportval;
        }

//        echo '<pre>';
//        print_r($header);
//        print_r($reportdata);
//       print_r ( array($rowArr,$yaxis));
//        exit;


        $filename = 'Student_Rating_List_' . date('d-m-Y') . '.csv';

        $counter = 0;
        $unlink_file = DOWNLOAD_DIR . 'Student_Rating_List_' . date('d-m-Y') . '.csv';
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }

        $fp = fopen(DOWNLOAD_DIR . $filename, 'a');

        $delimiter = ",";
        $emptyarr = array();
        fputcsv($fp, $subject_head, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $header, $delimiter);
        foreach ($reportdata as $reportsval) {
//            $fp = fopen(DOWNLOAD_DIR . $filename, 'a');
            fputcsv($fp, $reportsval, $delimiter);
        }
        fclose($fp);

        $file = DOWNLOAD_DIR . $filename;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function getStaffsReportAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();

        $stfdata = json_decode($this->request->getPost('stfdata'));
        $aggregateval = '';
        foreach ($stfdata as $key => $value) {
            $IsSubdiv = explode('_', $key);
            $params[$key] = $value;
            if ($IsSubdiv[0] == 'aggregate') {

                if ($value != '')
                    $aggregateval .= $value . ',';
            }
        }
//print_r($params);
//exit;

        if (isset($params['appointment_no']) && $params['appointment_no'] != '') {
            $queryParams[] = "appointment_no='" . $params['appointment_no'] . "'";
        }

        if (isset($params['name']) && $params['name'] != '') {

            $queryParams[] = "Staff_Name LIKE '" . $params['name'] . "%'";
        }

        if (isset($params['loginid']) && $params['loginid'] != '') {

            $queryParams[] = "loginid = '" . $params['loginid'] . "'";
        }

        if (isset($params['status']) && $params['status'] != ''):
            $queryParams[] = 'status  Like "%' . $params['status'] . '%"';
        endif;

        $queryParams[] = 'status IN("Appointed","Relieved","Relieving initiated")';

        $getlimit = $params['limit'];

        $queryParams[] = "status != 'NULL'";

        $conditionvals = (count($queryParams) > 0) ? ' WHERE ' . implode(' and ', $queryParams) : '';

        $phql_full = 'SELECT COUNT(*) as cnt FROM StaffInfo' . $conditionvals;
//            
//            print_r($phql_full);
//            exit;

        $expectedcount = $this->modelsManager->executeQuery($phql_full);
        $tottalrescount = $expectedcount[0]['cnt'];
        $x = 0;

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');

        $header = $reports = array();
//         
        $header[] = 'Staff Name';
        $header[] = 'Appointment No';
        $header[] = 'Gender';

        foreach ($acdyrMas as $field) {
            $header[] = ucfirst($field->name);
            $this->staffnodes[$field->id] = $field->name;
        }
        $header[] = 'Login';
        $header[] = 'Date of Birth';
        $header[] = 'Date of Joining';
        $header[] = 'Mobile No';
        $header[] = 'Email';
        $header[] = 'Edusparrow Email';
        $header[] = 'Address1';
        $header[] = 'Address2';
        $header[] = 'State';
        $header[] = 'Country';
        $header[] = 'Nationality';
        $header[] = 'Pin';
        $header[] = 'Phone';
        $header[] = 'Category';
        $header[] = 'Qualification';
        $header[] = 'Blood Group';
        $header[] = 'Medical Details';
        $header[] = 'Spouse Name';
        $header[] = 'Spouse Occupation';
        $header[] = 'Spouse Designation';
        $header[] = 'Spouse Phone No';
        $header[] = 'Parent Name';
        $header[] = 'Parent Occupation';
        $header[] = 'Parent Designation';
        $header[] = 'Parent Phone No';
        $header[] = 'Classes Handling';
        $header[] = 'Subjects Handling';
        $header[] = 'Extra Curricular';
        $header[] = 'Co-Curricular';
        $header[] = 'Additional Qualifications';
        $header[] = 'Experience';
        $header[] = 'Organization';
        $header[] = 'Designation';
        $header[] = 'Comments';
        $header[] = 'Relieving Reason';
        $header[] = 'Date Of Relieving';

        $header[] = 'Status';


        while ($x < $tottalrescount) {

            $remaincount = $tottalrescount - $x;
            //  echo ($remaincount) . "<br/>" . $tottalrescount . "<br/>";
            if ($remaincount < $getlimit)
                $limit = $remaincount;
            else
                $limit = $getlimit;

            $offset = $x;
//             stf_gnrl_mas.catg_id,stf_gnrl_mas.nationality,stf_gnrl_mas.qualification,stf_gnrl_mas.blood_group,
//                             stf_gnrl_mas.medical_details,stf_gnrl_mas.spouse_name,stf_gnrl_mas.s_occupation,stf_gnrl_mas.s_designation,
//                             stf_gnrl_mas.s_phoneno,stf_gnrl_mas.parent_name,stf_gnrl_mas.p_occupation,stf_gnrl_mas.p_designation,
//                             stf_gnrl_mas.p_phoneno,stf_gnrl_mas.classes_handling,stf_gnrl_mas.subjects_handling,stf_gnrl_mas.extra_curricular,
//                             stf_gnrl_mas.co_curricular,stf_gnrl_mas.additional_qualifications,stf_gnrl_mas.experience,stf_gnrl_mas.organization,
//                             stf_gnrl_mas.designation,stf_gnrl_mas.comments,stf_gnrl_mas.relieving_reason,stf_gnrl_mas.relieved_by,
//                             stf_gnrl_mas.dor JOIN StaffGeneralMaster stf_gnrl_mas ON stf_gnrl_mas.staff_id=stf_info.id 


            $phql_limited = 'SELECT stf_info.appointment_no,stf_info.Staff_Name,stf_info.Gender,stf_info.status,
            		     stf_info.aggregate_key,stf_info.loginid,stf_info.Date_of_Joining,stf_info.Date_of_Birth,
                             stf_info.id,stf_info.Mobile_No,stf_info.Email,stf_info.Address1,stf_info.Address2,
                             stf_info.State,stf_info.Country,stf_info.Pin,stf_info.Phone,stf_info.relieve_status
                             FROM StaffInfo  stf_info 
                            ' . $conditionvals . ' LIMIT ' . $limit . ' OFFSET ' . $offset;


//            print_r($phql_limited);
//            exit;

            $result = $this->modelsManager->executeQuery($phql_limited);

            $x = $x + $limit;

            $reports[] = $this->formatStaffList($result);
        }

//            print_r($header);
//            exit;

        $filename = 'Staff_List_' . date('d-m-Y') . '.csv';

        $counter = 0;
        $unlink_file = DOWNLOAD_DIR . 'Staff_List_' . date('d-m-Y') . '.csv';
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }
        $fp = fopen(DOWNLOAD_DIR . $filename, 'a');

        $delimiter = ",";
        fputcsv($fp, $header, $delimiter);

        foreach ($reports as $chunkrep) {
            $fp = fopen(DOWNLOAD_DIR . $filename, 'a');
            foreach ($chunkrep as $row) {
                fputcsv($fp, $row, $delimiter);
            }
        }
        fclose($fp);

        $file = DOWNLOAD_DIR . $filename;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function formatStaffList($result) {
        $reports = array();

        if (count($result) > 0) {
            foreach ($result as $items) {

                $row = array();

                $stff_general_dets = StaffGeneralMaster::findFirst('staff_id =' . $items->id);

                $row[] = $items->Staff_Name;
                $row[] = $items->appointment_no;
                $row[] = ($items->Gender == 1) ? 'Female' : 'Male';

                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';
                $staffdep_arr = array();
                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $staffdep_arr[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }

                    foreach ($this->staffnodes as $key => $mandnodeval) {
                        $row[] = isset($staffdep_arr[$key]) ? $staffdep_arr[$key] : '-';
                    }
                }
                $row[] = $items->loginid;
                $row[] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row[] = ($items->Date_of_Joining) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row[] = $items->Mobile_No ? $items->Mobile_No : '-';
                $row[] = $items->Email ? $items->Email : '-';
                $row[] = $items->loginid ? $items->loginid . '@' . SUBDOMAIN . '.edusparrow.com' : '-';
                $row[] = $items->Address1 ? $items->Address1 : '-';
                $row[] = $items->Address2 ? $items->Address2 : '-';
                $row[] = $items->State ? $items->State : '-';
                $row[] = $items->Country ? CountryMaster::findfirst('country_code ="' . $items->Country . '"')->country_name : '-';
                $row[] = isset($stff_general_dets->nationality) ? CountryMaster::findfirst('country_code ="' . $stff_general_dets->nationality . '"')->country_name : '-';
                $row[] = $items->Pin ? $items->Pin : '-';
                $row[] = $items->Phone ? $items->Phone : '-';
                $row[] = isset($stff_general_dets->catg_id) ? (($stff_general_dets->catg_id == 1) ? 'Teaching' : 'Non-Teaching') : '-';
                $row[] = isset($stff_general_dets->qualification) ? $stff_general_dets->qualification : '-';
                $row[] = isset($stff_general_dets->blood_group) ? $stff_general_dets->blood_group : '-';
                $row[] = isset($stff_general_dets->medical_details) ? $stff_general_dets->medical_details : '-';
                $row[] = isset($stff_general_dets->spouse_name) ? $stff_general_dets->spouse_name : '-';
                $row[] = isset($stff_general_dets->s_occupation) ? $stff_general_dets->s_occupation : '-';
                $row[] = isset($stff_general_dets->s_designation) ? $stff_general_dets->s_designation : '-';
                $row[] = isset($stff_general_dets->s_phoneno) ? $stff_general_dets->s_phoneno : '-';
                $row[] = isset($stff_general_dets->parent_name) ? $stff_general_dets->parent_name : '-';
                $row[] = isset($stff_general_dets->p_occupation) ? $stff_general_dets->p_occupation : '-';
                $row[] = isset($stff_general_dets->p_designation) ? $stff_general_dets->p_designation : '-';
                $row[] = isset($stff_general_dets->p_phoneno) ? $stff_general_dets->p_phoneno : '-';
                $row[] = isset($stff_general_dets->classes_handling) ? $stff_general_dets->classes_handling : '-';
                $row[] = isset($stff_general_dets->subjects_handling) ? $stff_general_dets->subjects_handling : '-';
                $row[] = isset($stff_general_dets->extra_curricular) ? $stff_general_dets->extra_curricular : '-';
                $row[] = isset($stff_general_dets->co_curricular) ? $stff_general_dets->co_curricular : '-';
                $row[] = isset($stff_general_dets->additional_qualifications) ? $stff_general_dets->additional_qualifications : '-';
                $row[] = isset($stff_general_dets->experience) ? $stff_general_dets->experience : '-';
                $row[] = isset($stff_general_dets->organization) ? $stff_general_dets->organization : '-';
                $row[] = isset($stff_general_dets->designation) ? $stff_general_dets->designation : '-';
                $row[] = isset($stff_general_dets->comments) ? $stff_general_dets->comments : '-';
                $row[] = isset($stff_general_dets->relieving_reason) ? $stff_general_dets->relieving_reason : '-';
                $row[] = isset($stff_general_dets->dor) ? $stff_general_dets->dor : '-';


                $row[] = $items->status;

//                print_r($row);
//            exit;
                $reports[] = $row;
            }
        }
        return $reports;
    }

    public function getSmsReportAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        $smsdata = json_decode($this->request->getPost('smsdata'));
        foreach ($smsdata as $key => $value) {
            $params[$key] = $value;
        }
        $get_report_file = SentSms::findFirstById($params['fileid'])->report_file;
        echo '<pre>';
        $expldval = explode('.', $get_report_file);
        $filename = $expldval[0] . '_1.csv';
        $unlink_file = DOWNLOAD_DIR . $filename;
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }
        $delimiter = ',';
        if (($handle = fopen(DOWNLOAD_DIR . $get_report_file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $newfb = fopen(DOWNLOAD_DIR . $filename, 'a');
                if ($data[$num - 1] != 'Smsid') {
                    $responseid = $data[$num - 1];
                    if (!empty($responseid)) {
                        $getreport = ControllerBase::smsReportByGatewayXML($responseid);
                        $data[$num - 2] = $getreport['recipients'][0][1] ? $getreport['recipients'][0][1] : '';
//$getreport['Report']['status'] ? $getreport['Report']['status'] : '';
                    }
                    array_pop($data);
                    fputcsv($newfb, $data, $delimiter);
                } else {
                    array_pop($data);
                    fputcsv($newfb, $data, $delimiter);
                }
            }
            fclose($handle);
        }
        fclose($newfb);
        $file = DOWNLOAD_DIR . $filename;
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function getGropupSmsReportsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        $smsdata = json_decode($this->request->getPost('smsdata'));
        foreach ($smsdata as $key => $value) {
            $params[$key] = $value;
        }
        $sms_reprt = SentSms::findFirstById($params['fileid']);
        $get_report_file = $sms_reprt->report_file;
        echo '<pre>';

        $expldval = explode('.', $get_report_file);
        $filename = $expldval[0] . '_1.csv';

        $unlink_file = DOWNLOAD_DIR . $filename;
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }

        $delimiter = ',';
        if (($handle = fopen(DOWNLOAD_DIR . $get_report_file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $newfb = fopen(DOWNLOAD_DIR . $filename, 'a');
                if ($num > 0) {
                    if ($data[$num - 1] != 'Smsid') {
                        $responseid = $data[$num - 1];
                        if (!empty($responseid)) {
                            $getreport = ControllerBase::smsReportByGatewayXML($responseid);
//                            print_r($getreport);exit;
                            $frommsg = $data[0];
                            $msgdate = $data[$num - 3];

                            if (count($getreport['recipients']) > 0) {
                                foreach ($getreport['recipients'] as $array_val) {
                                    $exl_data = array();
                                    $exl_data[] = $frommsg;
                                    $exl_data[] = $array_val['0'];
                                    $exl_data[] = $getreport['Message']; //str_replace('\n', '<br>', $array_val['message']['sms']);
                                    $exl_data[] = $array_val['2'];
                                    $exl_data[] = $array_val['1'];

                                    fputcsv($newfb, $exl_data, $delimiter);
                                }
                            }
                        }
                    } else {
                        array_pop($data);
                        fputcsv($newfb, $data, $delimiter);
                    }
                } else {
                    array_pop($data);
                    fputcsv($newfb, $data, $delimiter);
                    $expldval = explode(',', $data[1]);
                    foreach ($expldval as $val) {
                        $exl_data = array();
                        $exl_data[] = $frommsg;
                        $exl_data[] = $val;
                        $exl_data[] = $data[2];
                        $exl_data[] = $msgdate;
                        $exl_data[] = 'Failed';
                        fputcsv($newfb, $exl_data, $delimiter);
                    }
                }
            }
            fclose($handle);
        }

        fclose($newfb);

        $file = DOWNLOAD_DIR . $filename;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function getGropupSmsReportsOldAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        $smsdata = json_decode($this->request->getPost('smsdata'));
        foreach ($smsdata as $key => $value) {
            $params[$key] = $value;
        }
        $sms_reprt = SentSms::findFirstById($params['fileid']);
        $get_report_file = $sms_reprt->report_file;
        echo '<pre>';

        $expldval = explode('.', $get_report_file);
        $filename = $expldval[0] . '_1.csv';

        $unlink_file = DOWNLOAD_DIR . $filename;
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }

        $delimiter = ',';
        if (($handle = fopen(DOWNLOAD_DIR . $get_report_file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $newfb = fopen(DOWNLOAD_DIR . $filename, 'a');
                if ($num > 0) {
                    if ($data[$num - 1] != 'Smsid') {
                        $responseid = $data[$num - 1];
                        print_r($responseid);
                        if (!empty($responseid)) {
                            $getreport = ControllerBase::smsReportByGatewayXML($responseid);
//                            print_r($getreport);exit;
                            $frommsg = $data[0];
                            $msgdate = $data[$num - 3];

                            if (count($getreport['Report']) > 0 && !isset($getreport['Report']['number'])) {
                                foreach ($getreport['Report'] as $array_val) {
                                    $exl_data = array();
                                    $exl_data[] = $frommsg;
                                    $exl_data[] = $array_val['number'];
                                    $exl_data[] = $getreport['message']['sms']; //str_replace('\n', '<br>', $array_val['message']['sms']);
                                    $exl_data[] = $msgdate;
                                    $exl_data[] = $array_val['status'];

                                    fputcsv($newfb, $exl_data, $delimiter);
                                }
                            } else {
                                $array_val = $getreport['Report'];
                                $exl_data = array();
                                $exl_data[] = $frommsg;
                                $exl_data[] = $array_val['number'];
                                $exl_data[] = $getreport['message']['sms']; //str_replace('\n', '<br>', $array_val['message']['sms']);
                                $exl_data[] = $msgdate;
                                $exl_data[] = $array_val['status'];

                                fputcsv($newfb, $exl_data, $delimiter);
                            }

                            $data[$num - 2] = $getreport['Report']['status'] ? $getreport['Report']['status'] : '';
                        }
                    } else {
                        array_pop($data);
                        fputcsv($newfb, $data, $delimiter);
                    }
                } else {
                    array_pop($data);
                    fputcsv($newfb, $data, $delimiter);
                    $expldval = explode(',', $data[1]);
                    foreach ($expldval as $val) {
                        $exl_data = array();
                        $exl_data[] = $frommsg;
                        $exl_data[] = $val;
                        $exl_data[] = $data[2];
                        $exl_data[] = $msgdate;
                        $exl_data[] = 'Failed';
                        fputcsv($newfb, $exl_data, $delimiter);
                    }
                }
            }
            fclose($handle);
        }

        fclose($newfb);

        $file = DOWNLOAD_DIR . $filename;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function getTransportStudentsReportsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();
        $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
        $aggregateval = '';

        $studata = json_decode($this->request->getPost('trnsprtnode'));
//print_r($studata);
//exit;
        foreach ($studata as $key => $value) {
            $IsSubdiv = explode('_', $key);

            if ($IsSubdiv[0] == 'aggregate' && $value) {
                $params['aggregateids'][] = $value;
            } else {

                $params[$key] = $value;
            }
        }

        $acdyrMasFortransprt = OrganizationalStructureMaster::findFirst('is_subordinate =1  and module ="transport"');
        $this->mandnodeFortransprt = $this->_getMandNodesForAssigningTranport($acdyrMasFortransprt);

        $getlimit = $params['limit'];

        $header = $reports = array();
        $header[] = 'Student Name';
        $header[] = 'Roll No';

        foreach ($this->mandnode as $node) {
            $header[] = ucfirst($node);
        }
        foreach ($this->mandnodeFortransprt as $node) {
            $header[] = ucfirst($node);
        }


        $currentQury = preg_filter('/^([\d])*/', '(FIND_IN_SET("$0" , REPLACE( subordinate_key, "-", "," ) ) >0)', $params['aggregateids']);

        $condition = (count($currentQury) > 0) ? '(' . implode(' AND ', $currentQury) . ')' : '';

        $students = StudentMapping::find(array(
                    $condition,
                    'columns' => 'COUNT(*) as cnt'));
// 
        $tottalrescount = $students[0]->cnt;
        $x = 0;



        while ($x < $tottalrescount) {

            $remaincount = $tottalrescount - $x;
//              echo ($remaincount) . "<br/>" . $tottalrescount . "<br/>";
            if ($remaincount < $getlimit)
                $limit = $remaincount;
            else
                $limit = $getlimit;

            $offset = $x;

//            echo 'test';
//            exit;

            $result = StudentMapping::find(array(
                        $condition,
                        "limit" => array(
                            "number" => $limit,
                            "offset" => $offset
                        )
            ));

//            print_r($result);
//            exit;
            $x = $x + $limit;

            $reports[] = $this->formatStuTransportList($result);
        }

//        print_r($reports);
//        exit;

        $filename = 'Transport_Student_List_' . date('d-m-Y') . '.csv';

        $counter = 0;
        $unlink_file = DOWNLOAD_DIR . 'Transport_Student_List_' . date('d-m-Y') . '.csv';
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }

        $fp = fopen(DOWNLOAD_DIR . $filename, 'a');

        $delimiter = ",";
        fputcsv($fp, $header, $delimiter);
        foreach ($reports as $chunkrep) {
            $fp = fopen(DOWNLOAD_DIR . $filename, 'a');
            foreach ($chunkrep as $row) {
                fputcsv($fp, $row, $delimiter);
            }
        }
        fclose($fp);

        $file = DOWNLOAD_DIR . $filename;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function formatStuTransportList($result) {
        $reports = array();

        if (count($result) > 0) {
            foreach ($result as $items) {
                $row = array();
                $stuInfo = StudentInfo::findFirstById($items->student_info_id);
                $row[] = $stuInfo->Student_Name;
                $row[] = isset($stuInfo->rollno) ? $stuInfo->rollno : '-';


                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';
                $class_arr = array();
                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $class_arr[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }

                    foreach ($this->mandnode as $key => $mandnodeval) {
                        $row[] = isset($class_arr[$key]) ? $class_arr[$key] : '-';
                    }
                }

                $suborbinatevals = $items->subordinate_key ? explode(',', $items->subordinate_key) : '';
                $suborbinateval_arr = array();
                if ($suborbinatevals != '') {
                    foreach ($suborbinatevals as $suborbinateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($suborbinateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $suborbinateval_arr[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }

                    foreach ($this->mandnodeFortransprt as $key => $node) {
                        $row[] = isset($suborbinateval_arr[$key]) ? $suborbinateval_arr[$key] : '-';
                    }
                }
                $reports[] = $row;
            }
        }
        return $reports;
    }

    public function getFeeStudentsReportsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');

        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->mandnode = $this->_getMandNodesForAssigning($acdyrMas);

        $feenode = OrganizationalStructureMaster::findFirst('is_subordinate =1 and module ="Fee"');
        $this->feenode = $this->_getMandNodesForAssigningTranport($feenode);


        $studata = json_decode($this->request->getPost('feeSearchData'));
        echo '<pre>';
//print_r($studata);
//exit;
        $params = $queryParams = array();

        $aggregateval = '';
        foreach ($studata as $key => $value) {
            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'feeaggregate' && $value != '') {
                $params['feeaggregateids'][] = $value;
            }
            $params[$key] = $value;
            if ($IsSubdiv[0] == 'aggregate') {

                if ($value != '')
                    $params['aggregateids'][] = $value;
            }
        }

        $studentid = $params['stu_feeAssselectedID'];
        if (isset($params['stu_feeAssselectedID']) && $params['stu_feeAssselectedID'] != '') {
            $student = StudentMapping::findFirstById($studentid);
        }

        if (isset($params['feeaggregateids']) && count($params['feeaggregateids']) > 0) {
            $feelistQuery = preg_filter('/^([\d])*/', '(FIND_IN_SET("$0" , REPLACE(fee_node, "-", "," )) >0)', $params['feeaggregateids']); //'fee_node LIKE "' . implode('-', $params['feeaggregateids']) . '"';
        }
        if (isset($params['aggregateids']) && count($params['aggregateids']) > 0) {
            $stulistQuery = preg_filter('/^([\d])*/', '(FIND_IN_SET("$0" ,aggregate_key) >0)', $params['aggregateids']); // 'aggregate_key LIKE "' . implode(',', $params['aggregateids']) . '"';
        }
//print_r(implode(' AND ',$feelistQuery));exit;
        if (empty($studentid) && $studentid == '') {

            $students = StudentMapping::find(implode(' and ', $stulistQuery));
            $stu_ids = array();
            foreach ($students as $studentval) {
                $stu_ids[] = $studentval->student_info_id;
            }
//            print_r($stu_ids);exit;
        } else {
            $stu_ids[] = $student->student_info_id;
        }
        $stu_ids_uniq = array_unique($stu_ids);

        $feelst = FeeMaster::find(implode(' and ', $feelistQuery));
        $fee_masids = array();
        foreach ($feelst as $feeval) {
            $fee_masids[] = $feeval->id;
        }

        $this->view->student = $student;
        $this->view->aggregateids = $aggregateids = $student ? $student->aggregate_key : implode(',', $params['aggregateids']);
//        $this->view->feelst = $allFee;

        $condition = array();

        if (isset($params['feestatus']) && $params['feestatus'] != '') {
            if ($params['feestatus'] == 'Concession') {
                $condition[] = ' concession_amt > 0';
            } else {
                $condition[] = " status ='" . $params['feestatus'] . "'";
            }
        }

        if ((count($fee_masids) > 0)) {
            $condition[] = ' fee_master_id IN (' . implode(',', $fee_masids) . ')';
        }
        if ((count($stu_ids) > 0)) {
            $condition[] = ' student_id IN (' . implode(',', $stu_ids_uniq) . ')';
        }

        $stufee_dets = StudentFeeTable::find((count($condition) > 1) ? implode(' AND ', $condition) : $condition);

//        print_r($stufee_dets);
//        exit;

        $getlimit = $params['limit'];

        $header = $reports = array();
        $header[] = 'Student Name';
        foreach ($this->mandnode as $node) {
            $header[] = ucfirst($node);
        }
        foreach ($this->feenode as $feenode) {
            $header[] = ucfirst($feenode);
        }
        $header[] = 'Amount';
        $header[] = 'Paid';
        $header[] = 'Concession';
        $header[] = 'To Be Paid';
        $header[] = 'Cancelled';
        $header[] = 'Status';


        $conditionqry = (count($condition) > 1) ? implode(' AND ', $condition) : $condition;



        $tot_qry = 'SELECT sum(fm.fee_amount)as total, sum(st.paidamount) as paidamount, sum(st.returnedamount) as returnedamount, sum(st.concession_amt) as concession_amt, sum(fm.fee_amount-(st.paidamount+st.concession_amt+st.returnedamount)) as unpaid 
                    FROM StudentFeeTable st
                    INNER JOIN FeeInstalments fm on fm.id = st.fee_instalment_id WHERE ' . $conditionqry;

        $stufee_tot_dets = $this->modelsManager->executeQuery($tot_qry);


        $Cancel_tot_qry = "SELECT sum(fm.fee_amount - st.paidamount)as cancelamt
                            FROM StudentFeeTable st
                            INNER JOIN FeeInstalments fm on fm.id = st.fee_instalment_id WHERE st.status = 'Cancelled' AND " . $conditionqry;

        $stufee_cancel_tot_dets = $this->modelsManager->executeQuery($Cancel_tot_qry);

        $fee_arr_overall_tot = array();

        $tobeamt = $stufee_tot_dets[0]->total - ($stufee_tot_dets[0]->paidamount + $stufee_tot_dets[0]->concession_amt + $stufee_cancel_tot_dets[0]->cancelamt);


        $stufee_tobe_dets = StudentFeeTable::find(array(
                    $conditionqry));
        $fee_grp_total = $fee_grp_paid_total = $fee_grp_cancel_total = $fee_grp_tobepaid_total = $fee_grp_concession_total = $tot_stu_count = 0;
        $fee_arr = array();
        if (count($stufee_tobe_dets) > 0) {
            foreach ($stufee_tobe_dets as $stufee) {

                $feeInstallment = FeeInstalments::findFirstById($stufee->fee_instalment_id);
                $fee_total = $fee_paid_total = $fee_canceltotal = $fee_concession_total = $fee_tobepaid_total = 0;
                $fee_total += $feeInstallment->fee_amount;
                $fee_grp_total += $fee_total;
                $fee_paid_total += $stufee->paidamount;
                $fee_grp_paid_total += $fee_paid_total;

                $fee_concession_amt = $stufee->concession_amt;
                $fee_concession_total += $fee_concession_amt;
                $fee_grp_concession_total +=$fee_concession_total;


                if ($stufee->status == 'Cancelled') {
                    $fee_canceltotal += $feeInstallment->fee_amount - $stufee->paidamount;
                    $fee_grp_cancel_total += $fee_canceltotal;
                } else {
                    $fee_tobepaid_total += $feeInstallment->fee_amount - $stufee->paidamount - $fee_concession_amt;
                    $fee_grp_tobepaid_total += $fee_tobepaid_total;
                }
            }

            $fee_arr['tobeamt'] = $fee_grp_tobepaid_total;
        }


        $tobeamt = $fee_arr['tobeamt'];
//        echo $tobeamt;
//        exit;

        $fee_arr_overall_tot['header'] = array('Total Amount -' . $stufee_tot_dets[0]->total,
            'Paid Amount -' . $stufee_tot_dets[0]->paidamount,
            'Concession Amount -' . $stufee_tot_dets[0]->concession_amt,
            'To Paid Amount -' . $tobeamt,
            'Cancelled Amount -' . $stufee_cancel_tot_dets[0]->cancelamt);

        $stufee_dets = StudentFeeTable::find(array(
                    $conditionqry,
                    'columns' => 'COUNT(*) as cnt'));

        $tottalrescount = $stufee_dets[0]->cnt;
        $x = 0;


//echo '<pre>';
        while ($x < $tottalrescount) {

            $remaincount = $tottalrescount - $x;
//              echo ($remaincount) . "-" . $tottalrescount . "<br/>";
            if ($remaincount < $getlimit)
                $limit = $remaincount;
            else
                $limit = $getlimit;

            $offset = $x;

            $result = StudentFeeTable::find(array(
                        $conditionqry,
                        "limit" => array(
                            "number" => $limit,
                            "offset" => $offset
                        )
            ));


            $x = $x + $limit;
            $reports[] = $this->formatStuFeeReportList($result);
        }

//        print_r($reports);
//        exit;

        $filename = 'Fee_Report_Student_List_' . date('d-m-Y') . '.csv';

        $counter = 0;
        $unlink_file = DOWNLOAD_DIR . 'Fee_Report_Student_List_' . date('d-m-Y') . '.csv';
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }

        $fp = fopen(DOWNLOAD_DIR . $filename, 'a');
        $empty_arr = array();
        $delimiter = ",";
        fputcsv($fp, $fee_arr_overall_tot['header'], $delimiter);
        fputcsv($fp, $empty_arr, $delimiter);
        fputcsv($fp, $empty_arr, $delimiter);
        fputcsv($fp, $header, $delimiter);

//        unset($reports[0]['header']);

        foreach ($reports as $keyval => $chunkrep) {
            $fp = fopen(DOWNLOAD_DIR . $filename, 'a');
            foreach ($chunkrep as $key => $row) {
                fputcsv($fp, $row, $delimiter);
            }
        }
        fclose($fp);

        $file = DOWNLOAD_DIR . $filename;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function formatStuFeeReportList($stufee_dets) {
        $reports = array();

        $fee_grp_total = $fee_grp_paid_total = $fee_grp_cancel_total = $fee_grp_tobepaid_total = $fee_grp_concession_total = $tot_stu_count = 0;
        if (count($stufee_dets) > 0) {
            foreach ($stufee_dets as $stufee) {

                $fee_arr = array();
                $feeInstallment = FeeInstalments::findFirstById($stufee->fee_instalment_id);
                $fee_total = $fee_paid_total = $fee_canceltotal = $fee_concession_total = $fee_tobepaid_total = 0;
                $fee_total += $feeInstallment->fee_amount;
                $fee_grp_total += $fee_total;
                $fee_paid_total += $stufee->paidamount;
                $fee_grp_paid_total += $fee_paid_total;

                $fee_concession_amt = $stufee->concession_amt;
                $fee_concession_total += $fee_concession_amt;
                $fee_grp_concession_total +=$fee_concession_total;


                if ($stufee->status == 'Cancelled') {
                    $fee_canceltotal += $feeInstallment->fee_amount - $stufee->paidamount;
                    $fee_grp_cancel_total += $fee_canceltotal;
                } else {
                    $fee_tobepaid_total += $feeInstallment->fee_amount - $stufee->paidamount - $fee_concession_amt;
                    $fee_grp_tobepaid_total += $fee_tobepaid_total;
                }


                $groupByFeename = FeeMaster::findFirstById($stufee->fee_master_id);


                $stuGenInfo = $stuInfo = StudentInfo::findFirstById($stufee->student_id);

                $fee_arr[] = $stuInfo->Student_Name;

                $aggregatevals = StudentMapping::findFirst('student_info_id = ' . $stufee->student_id)->aggregate_key ?
                        explode(',', StudentMapping::findFirst('student_info_id = ' . $stufee->student_id)->aggregate_key) : '';
                //$aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';
                $class_name = array();
                if ($aggregatevals != '') {
                    $remove_ye = 0;
                    foreach ($aggregatevals as $path) {
//                        $value = OrganizationalStructureValues::findfirst('id = ' . $path)->name;
                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($path);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);

//                        if ($remove_ye > 0) {
//                            $class_name[] = $value;
                        $class_name[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
//                        }
//                        $remove_ye++;
                    }

                    foreach ($this->mandnode as $key => $mandnodeval) {
                        $fee_arr[] = isset($class_name[$key]) ? $class_name[$key] : '-';
                    }
                }

                $exploded_path = explode('-', $groupByFeename->fee_node);
                $fee_name = array();
                $remove_ye = 0;
                foreach ($exploded_path as $path) {
//                    $value = OrganizationalStructureValues::findfirst('id = ' . $path)->name;
                    $orgnztn_str_det = OrganizationalStructureValues::findFirstById($path);
                    $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);

                    $fee_name[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
//                    $fee_name[] = $value;
                }

                foreach ($this->feenode as $keyfee => $feenode) {
                    $fee_arr[] = isset($fee_name[$keyfee]) ? $fee_name[$keyfee] : '-';
                }
                $fee_name_grp = implode('>>', $fee_name);
//                print_r($aggregatevals);
//                print_r($row);
//                exit;

                $fee_arr[] = $fee_total;
                $fee_arr[] = $fee_paid_total;
                $fee_arr[] = $fee_concession_total;
                $fee_arr[] = $fee_tobepaid_total;
                $fee_arr[] = $fee_canceltotal;
                $fee_arr[] = $stufee->status;


                $reports[] = $fee_arr;
            }
        }
        return $reports;
    }

    public function getClassAttendanceListReportAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();

        $aggregateval = '';
        //  $studata=$this->request->getPost('studata');

        $clsrptdata = json_decode($this->request->getPost('clsrptdata'));
        foreach ($clsrptdata as $key => $value) {
            $params[$key] = $value;
        }
        $master_id = $params['subject_masterid'];

        $assignedclass = GroupClassTeachers::findFirstById($master_id);
        $classroommas = ClassroomMaster::findFirstById($assignedclass->classroom_master_id);
        $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $assignedclass->id);
//        $res = ControllerBase::buildStudentQuery($classroommas->aggregated_nodes_id);
        $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                . 'stumap.subordinate_key,stumap.status'
                . ' FROM StudentMapping stumap LEFT JOIN'
                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                . '  and (' . implode(' or ', $res). ') '
                . ' ORDER BY stuinfo.Student_Name ASC';
        $classStudents = $this->modelsManager->executeQuery($stuquery);
        $monthlyPercent = array();
        if (count($classStudents) > 0):
            foreach ($classStudents as $classStudent) {
                $overallPercent = array();
                $params['user_id'] = $classStudent->student_info_id;
                $params['user_type'] = 'student';
//                    echo $classStudent->student_info_id;
//                    echo '<br/>';
//                    $params['master_id'] = $master_id;
//                    e
//                    $params['academic_year_id'] = Controllerbase::get_current_academic_year();
                $overallPercent = AttendanceController::getAttendancePercentMonthly($params);
                $monthlyPercent[] = $overallPercent;
//                    break;
            }
//                exit;
        endif;
        $monthlyPer = $monthlyPercent;

        $aggregatevals = $classroommas->aggregated_nodes_id ? explode('-', $classroommas->aggregated_nodes_id) : '';
        $class_arr = array();
        $aggcnt = 0;
        if ($aggregatevals != '') {
            foreach ($aggregatevals as $aggregateval) {
                if ($aggcnt != 0) {
                    $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                    $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                    $class_arr[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_mas_det->name . ' : ' . $orgnztn_str_det->name : '-';
                }
                $aggcnt++;
            }
        }

        $header = array();

        $header[] = 'Student Name';

        foreach ($monthlyPer[0][0] as $monthhdr) {
            $header[] = $monthhdr['text'] . '( % )';
        }

        $subject_head = array();

        foreach ($class_arr as $headerval) {
            $subject_head[] = $headerval;
        }

        $icnt = count($header) - count($subject_head);

        for ($ii = 0; $ii < $icnt; $ii++) {
            $subject_head[] = '';
        }

        $reportdata = array();
        foreach ($monthlyPer as $per) {
            $reportval = array();
            $stu_cnt = 0;
            foreach ($per[1] as $stuarrval) {
                if ($stu_cnt == 0) {
                    $reportval[] = $stuarrval['name'];

//                    $aggregatevals = $stuarrval['aggreegatekey'] ? explode(',', $stuarrval['aggreegatekey']) : '';
//                    $class_arr = array();
//                    $aggcnt = 0;
//                    if ($aggregatevals != '') {
//                        foreach ($aggregatevals as $aggregateval) {
//                            if ($aggcnt != 0) {
//                                $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
//                                $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
//                                $class_arr[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
//                            }
//                            $aggcnt++;
//                        }
////                        array_shift($class_arr);
//                        $aggcntval = 0;
//                        foreach ($this->mandnode as $key => $mandnodeval) {
//                            if ($aggcntval != 0) {
//                                $reportval[] = isset($class_arr[$key]) ? $class_arr[$key] : '-';
//                            }
//                            $aggcntval++;
//                        }
//                    }
                }
                $reportval[] = $stuarrval['percent'];
                $stu_cnt++;
            }
            $reportdata[] = $reportval;
        }

//        echo '<pre>';
//        print_r($header);
//        print_r($reportdata);
////       print_r ( array($rowArr,$yaxis));
//        exit;


        $filename = 'Attendance_Students_List_' . date('d-m-Y') . '.csv';

        $counter = 0;
        $unlink_file = DOWNLOAD_DIR . 'Attendance_Students_List_' . date('d-m-Y') . '.csv';
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }

        $fp = fopen(DOWNLOAD_DIR . $filename, 'a');

        $delimiter = ",";
        $emptyarr = array();
        fputcsv($fp, $subject_head, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $header, $delimiter);
        foreach ($reportdata as $reportsval) {
//            $fp = fopen(DOWNLOAD_DIR . $filename, 'a');
            fputcsv($fp, $reportsval, $delimiter);
        }
        fclose($fp);

        $file = DOWNLOAD_DIR . $filename;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function getAttendanceReportAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        $aggregateval = '';
        $clsrptdata = json_decode($this->request->getPost('params'));
        foreach ($clsrptdata as $key => $value) {
            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'aggregate' && $value) {
                $params['aggregateids'][] = $value;
            } else {

                $params[$key] = $value;
            }
        }

        $res = ControllerBase::buildStudentQuery(implode(',', $params['aggregateids']));
        $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                . 'stumap.subordinate_key,stumap.status,stuinfo.Student_Name '
                . ' FROM StudentMapping stumap LEFT JOIN'
                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" and (' . implode(' or ', $res)
                . ') ORDER BY stuinfo.Student_Name ASC';
        $classStudents = $this->modelsManager->executeQuery($stuquery);
        if ($params['reportyp'] == 'monthly') {
            $monthlyPercent = array();
            if (count($classStudents) > 0):
                foreach ($classStudents as $classStudent) {
                    $overallPercent = array();
                    $params['user_id'] = $classStudent->student_info_id;
                    $params['user_type'] = 'student';
                    $overallPercent = AttendanceController::getAttendancePercentMonthly($params);
                    $monthlyPercent[] = $overallPercent;
                }
            endif;

            $params['reportPer'] = $monthlyPercent;
        }
        if ($params['reportyp'] == 'daily') {
            $dailyPercent = array();
            if (count($classStudents) > 0):
                foreach ($classStudents as $classStudent) {
                    $overallPercent = array();
                    $params['user_id'] = $classStudent->student_info_id;
                    $params['user_type'] = 'student';
                    $params['month'] = $params['month'];
                    $params['year'] = $params['year'];
                    $monthPercent = AttendanceController::getAttendancePercentDaily($params);
                    $dailyPercent[] = $monthPercent;
                }
            endif;

            $params['reportPer'] = $dailyPercent;
        }
        $params['students'] = $classStudents;
        $this->exportAttendanceReport($params);
    }

    public function exportAttendanceReport($params) {
        $class_arr = array();
        $aggcnt = 0;
        if (count($params['aggregateids']) > 0) {
            foreach ($params['aggregateids'] as $aggregateval) {
                if ($aggcnt != 0) {
                    $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                    $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                    $class_arr[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_mas_det->name . ' : ' . $orgnztn_str_det->name : '-';
                }
                $aggcnt++;
            }
        }

        $header = array();
        $header[] = 'Student Name';

        foreach ($params['reportPer'][0][0] as $monthhdr) {
            $header[] = $monthhdr['text'] . '(%)';
        }

        $subject_head = array();
        foreach ($class_arr as $headerval) {
            $subject_head[] = $headerval;
        }
        $icnt = count($header) - count($subject_head);
        for ($ii = 0; $ii < $icnt; $ii++) {
            $subject_head[] = '';
        }
        $reportdata = array();
        $i = 0;
        foreach ($params['students'] as $stu) {
            $reportval = array();
            $reportval[] = $stu->Student_Name;
            foreach ($params['reportPer'][$i][1] as $stuPer) {
                $reportval[] = $stuPer['percent'];
            }
            $reportdata[] = $reportval;
            $i++;
        }

//echo '<pre>';print_r($header);exit;
        $filename = 'Classwise_Attendance_Report_' . date('d-m-Y') . '.csv';
        $counter = 0;
        $unlink_file = DOWNLOAD_DIR . 'Classwise_Attendance_Report_' . date('d-m-Y') . '.csv';
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }

        $fp = fopen(DOWNLOAD_DIR . $filename, 'a');

        $delimiter = ",";
        $emptyarr = array();
        fputcsv($fp, $subject_head, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $header, $delimiter);
        foreach ($reportdata as $reportsval) {
            fputcsv($fp, $reportsval, $delimiter);
        }
        fclose($fp);

        $file = DOWNLOAD_DIR . $filename;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

//functions

    public function _getNonMandNodesForAssigning($id, $nodes = array()) {
        $fields = OrganizationalStructureMaster::find('parent_id=' . $id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
// if ($field->mandatory_for_admission != 1) {
                if ($field->is_subordinate != 1) {
                    $nodes[$field->id] = $field->name;
// } else {
                    $nodes = $this->_getNonMandNodesForAssigning($field->id, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function _getMandNodesForAssigning($acdyrMas, $nodes = array()) {

        $nodes[$acdyrMas->id] = $acdyrMas->name;
//  echo $acdyrMas->name;
        $fields = OrganizationalStructureMaster::find('parent_id=' . $acdyrMas->id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
//if ($field->mandatory_for_admission == 1) {
                if ($field->is_subordinate != 1) {
//                    $nodes[$field->id] = $field->name;
                    $nodes = $this->_getMandNodesForAssigning($field, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function _getMandNodesForAssigningTranport($acdyrMas, $nodes = array()) {

        $nodes[$acdyrMas->id] = $acdyrMas->name;
//          echo $acdyrMas->name;
//         echo 'parent_id=' . $acdyrMas->id;
        $fields = OrganizationalStructureMaster::find('parent_id=' . $acdyrMas->id);
        if (count($fields) > 0):
            foreach ($fields as $field) {

//                    $nodes[$field->id] = $field->name;
                $nodes = $this->_getMandNodesForAssigningTranport($field, $nodes);
            }
        endif;

        return $nodes;
    }

    public function _getMandNodesForSubject($acdyrMas, $nodes = array()) {
//        echo 'test';exit;
        $fields = OrganizationalStructureValues::findFirstById($acdyrMas->parent_id);
        $query = 'id =' . $acdyrMas->org_master_id . ' AND module ="Subject"';
        $org_mas = OrganizationalStructureMaster::find(array(
                    $query,
                    "columns" => "COUNT(*) as cnt"
        ));
        if (isset($org_mas[0]->cnt) && $org_mas[0]->cnt == 1):
            $nodes[$acdyrMas->id] = $acdyrMas->name;
//                echo $acdyrMas->name;
            if (isset($fields->parent_id)):
//            echo $fields->parent_id;
                $nodes = ReportsController::_getMandNodesForSubject($fields, $nodes);
            endif;
        endif;

        return $nodes;
    }

    public function _getMandNodesForExam($acdyrMas, $nodes = array()) {
        $fields = OrganizationalStructureValues::findFirstById($acdyrMas->parent_id);
        $iscycle = OrganizationalStructureMaster::findFirstById($acdyrMas->org_master_id);
//        $query = 'id =' . $fields->org_master_id . ' AND module !="Subject"';
//        $org_mas = OrganizationalStructureMaster::find(array(
//                    $query,
//                    "columns" => "COUNT(*) as cnt"
//        ));
//        echo $iscycle->id.$iscycle->promotion_status.$name->id.'<br>';
        if (!in_array($iscycle->module, array("structural", "StaffCycleNode", "Ledger"))) {
            if ($iscycle->module != "Subject"):
                $nodes[$acdyrMas->id] = $acdyrMas->name;

            endif;
            if (isset($fields->parent_id)):
//            echo $fields->parent_id;
                $nodes = $this->_getMandNodesForExam($fields, $nodes);
            endif;
        }

        return $nodes;
    }

    /** for reference * */
    public function excelreportAction() {
        $this->checkSession(false);
        $mars = new Zend_Session_Namespace('mars');

        if (isset($mars->employee_id)) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $params = $this->_request->getPost();
            $params = $this->_request->getParams();
            $Invoices = new Application_Model_DbTable_Invoices();
            $writer = new Zend_Log_Writer_Stream('file://' . DOCUMENTSBASEFOLDER . APPLOGFILEPATH);
            $logger = new Zend_Log($writer);
            //echo "<pre>";print_r($params);exit;
            $params['join_updt'] = $params['join_where'] = $params['join_columns'] = $params['join_group'] = "";
            $params_query = Zend_Json::decode($params['query']);
            if (isset($params_query['fromPage'])) {
                if ($params_query['fromPage'] == 'rejected_adj' || $params_query['fromPage'] == 'pending_adj') {
                    $inv_req_status = ($params_query['fromPage'] == 'rejected_adj') ? "'rejected'" : "'requested'";
                    $params['join_where'] = " and invdt.invoice_request_status = " . $inv_req_status;
                } else {

                    $params['where'] = $Invoices->getInvoicesQueryByParams(Zend_Json::decode($params['query']));
                    if (isset($params_query['actionCode']) && $params_query['actionCode'] == 11) {
                        $params['join_where'] = " and invdt.invoice_request_status =  'approved'";
                    }
                }
            } else {

                $params['where'] = $Invoices->getInvoicesQueryByParams(Zend_Json::decode($params['query']));
                if (isset($params_query['actionCode']) && $params_query['actionCode'] == 11) {
                    $params['join_where'] = " and invdt.invoice_request_status =  'approved'";
                }
            }
            echo "<pre>";
            print_r($params); //exit;

            $sellatestInvoice = $Invoices->lastestInvoiceUpdates($params);
            $params['latest_updt'] = implode(',', $sellatestInvoice);
            $tottalrescount = $Invoices->XlreportPatcount($params);
            $logger->info(':[csvexport]:Query generation started');
            $x = 0;
            $reports = array();
            $header = array(
                'Client Name', 'Account Manager', 'Project Date', 'Patient Name', 'Patient #Id', 'Invoice Number', 'Invoice Status', 'Action Code',
                'Followup Date', 'Payer Name', 'Invoice Date', 'Location', 'Charge Price',
                'Start Date', 'End Date', 'Collection Rate', 'Mars Code', 'Approval Note', 'Collection Note'
                , 'Assigned Balance', 'Remaining Balance', 'Cash Amount', 'Adjustment Amount', 'Total Transactions',
                'Estimated Collectibility', 'Collection Performance', 'Assignment Aging', 'Project Aging', 'True Aging'
            );
            while ($x < $tottalrescount) {
                $remaincount = $tottalrescount - $x;
                echo ($remaincount) . "<br/>" . $tottalrescount . "<br/>";
                if ($remaincount < CSVLIMIT)
                    $limit = $remaincount;
                else
                    $limit = CSVLIMIT;
                echo $limit;
                $offset = $x;
                echo "execute query with LIMIT " . $limit . " OFFSET " . $offset . "<br/>";
                $logger->info(':[csvexport]: Executing query [' . ($x + 1) . "-" . ($limit + $offset) . ']');
                $params['limit'] = $limit;
                $params['offset'] = $offset;
                $exerows = $Invoices->Xlreportresults($params);
                print_r(count($exerows));
                $reports[] = $exerows;
                $x = $x + $limit;
            }
            $this->view->dateFormat = DATEFORMAT;
            $filename = "InvoiceReports-" . date('m-d-Y H:i:s') . ".csv";
            $logger->info(':[csvexport]: CSV Convertion started with Result!');
            $counter = 0;
            $fp = fopen(DOCUMENTSBASEFOLDER . PROJECTSDOWNLOADFOLDER . "/invoices/" . $filename, 'a');
            $delimiter = "|";
            fputcsv($fp, $header, $delimiter);
            foreach ($reports as $chunkrep) {
                echo count($chunkrep);
                $fp = fopen(DOCUMENTSBASEFOLDER . PROJECTSDOWNLOADFOLDER . "/invoices/" . $filename, 'a');
                $counter+=count($chunkrep);
                $logger->info(':[csvexport]: Converting [' . $counter . ']!');
                $delimiter = "|";
                foreach ($chunkrep as $row) {
                    fputcsv($fp, $row, $delimiter);
                }
                fclose($fp);
                $logger->info(':[csvexport]: CSV Convertion [' . $counter . '] Completed!');
            }
            $file = DOCUMENTSBASEFOLDER . PROJECTSDOWNLOADFOLDER . "/invoices/" . $filename;
            $logger->info(':[csvexport]: CSV file Generated in name ' . $filename);
            if (file_exists($file)) {
                $logger->info(':[csvexport]: Exporting CSV file!');
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . $filename . '";');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                exit;
            }
        } else {
            $this->_helper->redirector('index', 'index');
        }
    }

    public function loadAcademicReportsAction() {

        $this->view->setRenderLevel(VIEW::LEVEL_ACTION_VIEW);
        $identity = $this->auth->getIdentity();
        if (in_array('Staff', $identity['role_name'])) {
            $this->view->staff = $staff = StaffInfo::findFirstByLoginid($identity['name']);
            $query = (in_array('SuperAdmin', $identity['role_name'])) ? '' : 'FIND_IN_SET( ' . $staff->id . ' , teachers_id)';
            $combinations = array();
            $assignedCount = GroupSubjectsTeachers::find($query);
            $clsassignedCount = GroupClassTeachers::find($query);
            if (count($assignedCount) > 0 || count($clsassignedCount) > 0) {
                foreach ($assignedCount as $assignedclass) {
                    $classroom = ClassroomMaster::findFirstById($assignedclass->classroom_master_id);
                    $combinations[$classroom->id]['name'] = $classroom->name;
                }
                foreach ($clsassignedCount as $clsassignedclass) {
                    $classroom = ClassroomMaster::findFirstById($clsassignedclass->classroom_master_id);
                    $combinations[$classroom->id]['name'] = $classroom->name;
                }
            } else {
                $errorMessage = 'No Class assigned for you yet!';
            }
        } else {
            $errorMessage = 'Only teachers have access to his page!';
        }
        $param['type'] = 'mainexam_mark';
        $this->view->combinations = $combinations;
        $this->view->errorMessage = $errorMessage;
    }

    public function getActivityListAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $identity = $this->auth->getIdentity();
        $this->view->staff = $staff = StaffInfo::findFirstByLoginid($identity['name']);
        $masterid = $this->request->getPost('masterid');
        $classroommas = ClassroomMaster::findFirstById($masterid);
        if (!(in_array('SuperAdmin', $identity['role_name'])))
            $query[] = 'FIND_IN_SET( ' . $staff->id . ' , teachers_id)';
        $query[] = 'classroom_master_id =' . $masterid;
        $groupSubj = GroupSubjectsTeachers::find(implode(' and ', $query));
        $subj_Ids = $subjids = array();
        if (count($groupSubj) > 0):
            foreach ($groupSubj as $pusid) {
                $subj_Ids[] = $pusid->subject_id;
            }
            $subjids = array_unique($subj_Ids);
        endif;

        $examsqry = ControllerBase::buildExamQuery($classroommas->aggregated_nodes_id);
        $this->view->exams = Mainexam ::find(implode(' or ', $examsqry));
        $this->view->ratings = $ratings = RatingDivision::find(implode(' or ', $examsqry));
        $this->view->subjects = count($subjids) > 0 ? OrganizationalStructureValues::find('id IN (' . implode(',', $subjids) . ')') : '';
        $this->view->masterid = $masterid;
    }

    public function loadMonthlyReportAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->masterid = $master_id = $gvnparams['masterid'] = $this->request->getPost('masterid');
            $this->view->reptyp = $reptyp = $gvnparams['reptyp'] = $this->request->getPost('reptyp');
            $calres = ReportsController::calculateAttendancePercentMonthly($gvnparams);
            if (count($calres) > 0) {
                $this->view->classStudents = $calres[0];
                $this->view->monthlyPercent = $calres[1];
                $this->view->monthhead = $calres[4];
                $this->view->valhead = $calres[3];
                $this->view->legend = $calres[2];
                $this->view->result = $calres[5];
                $this->view->st = $calres[6];
                $this->view->et = $calres[7];
                $this->view->classroomname = $calres[8];
            } else {

                $this->view->error = '<div class="alert alert-danger">Attendance Not Yet Taken</div>';
            }
        }
    }

    public function calculateAttendancePercentMonthly($gvnparams) {

        $master_id = $gvnparams['masterid'];
        $reptyp = $gvnparams['reptyp'];
        $newarr = array();
        $user_type = 'student';
        $classroommas = ClassroomMaster::findFirstById($master_id);
//        $res = ControllerBase::buildStudentQuery($classroommas->aggregated_nodes_id);
        $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classroommas->id);
        $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                . 'stumap.subordinate_key,stumap.status,stuinfo.loginid,stuinfo.Date_of_Joining,'
                . ' stuinfo.Admission_no,stuinfo.photo,stuinfo.Student_Name '
                . ' FROM StudentMapping stumap LEFT JOIN'
                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                . '  and (' . implode(' or ', $res)  . ') '
                . ' ORDER BY stuinfo.Student_Name ASC';
        $classStudents = $this->modelsManager->executeQuery($stuquery);
        $monthlyPercent = $result = array();
        if (count($classStudents) > 0):
            $params['monthhead'] = array();
            $params['valhead'] = array();
            foreach ($classStudents as $classStudent) {
                $overallPercent = array();
                $params['user_id'] = $classStudent->loginid;
                $params['student_id'] = $classStudent->student_info_id;
                $params['doj'] = $classStudent->Date_of_Joining;
                $params['user_type'] = 'student';
                $overallPercent = ReportsController::getAttendancePercentMonthly($params);
                $monthlyPercent = array_merge($monthlyPercent, $overallPercent[0]);
                $params['monthhead'] = ($overallPercent[1]);
                $params['valhead'] = ($overallPercent[2]);
                $params['legend'] = ($overallPercent[3]);
            }
        endif;
        $i = $j = 0;
        $mntharr = array_unique($params['monthhead']);
        usort($mntharr, 'ReportsController::month_compare');
//            print_r($mntharr);exit;
        if (count($mntharr) > 0) {
            $firstele = (count($mntharr) > 1) ? array_shift($mntharr) : $mntharr[0];
            $lastele = (count($mntharr) > 1) ? array_pop($mntharr) : $mntharr[0];
            $startdt = (date('Y-m-01 00:00:00', strtotime('01-' . $firstele)));
            $enddt = (date('Y-m-t 00:00:00', strtotime('01-' . $lastele)));
            $begin = new DateTime($startdt);
            $end = new DateTime($enddt);
            $interval = DateInterval::createFromDateString('1 month');
            $period = new DatePeriod($begin, $interval, $end);
            foreach ($period as $dt) {
                $monthhead[] = ($dt->format("m-Y"));
            }

            $valhead = array_unique($params['valhead']);
            if ($reptyp == 1) {

                $legend = array();

                $result['thead'][$i][$j]['text'] = 'Students';
                $result['thead'][$i][$j]['rowspan'] = '';
                $result['thead'][$i][$j]['colspan'] = '';
                foreach ($classStudents as $classStudent) {
                    if (count($monthhead) > 0) {
                        foreach ($monthhead as $hvalue) {
                            $atttaken = $stu_att_totByVal = 0;
                            if (count($valhead) > 0) {
                                foreach ($valhead as $vvalue) {
                                    $student_att_props_val = AttendanceSelectbox::findFirst("attendance_for = '$user_type'"
                                                    . ' and attendanceid= "' . $vvalue . '"');
                                    $noofperiods = PeriodMaster::find('LOCATE(node_id,"' . str_replace(',', '-', $classStudent->aggregate_key) . '" )' . " and user_type = 'student'");
                                    $counttaken = $monthlyPercent[$classStudent->loginid][$hvalue][$vvalue] ? ($monthlyPercent[$classStudent->loginid][$hvalue][$vvalue] / $noofperiods) : 0;
                                    $atttaken += ($counttaken / count($noofperiods));
                                    $stu_att_totByVal+= ($counttaken / count($noofperiods)) * $student_att_props_val->attendancevalue;
                                }
                            }
                            $monthlyPercent[$classStudent->loginid][$hvalue]['percent'] = $atttaken > 0 ? (round($stu_att_totByVal / $atttaken * 100, 2)) : '';
                        }
                    }
                }

                if (count($monthhead) > 0) {
                    foreach ($monthhead as $hvalue) {
                        $j++;
                        $monthhdr = explode('-', $hvalue);
                        $result['thead'][$i][$j]['text'] = '<a href="javascript:void(0)" onclick="academicReportSettings.loadDailyReport(this)"
                       class="monthExpand" month="' . $monthhdr['0'] . '" year="' . $monthhdr['1'] . '" 
                           masterid="' . $master_id . '" reptyp="1"  >
                        <span class="btn btn-round btn-info">' . date('M - Y', strtotime('01-' . $hvalue)) . '</span></a>';
                        $result['thead'][$i][$j]['rowspan'] = '';
                        $result['thead'][$i][$j]['colspan'] = '';
                    }

                    $j++;
                    $result['thead'][$i][$j]['text'] = 'Total';
                    $result['thead'][$i][$j]['rowspan'] = '';
                    $result['thead'][$i][$j]['colspan'] = '';
                }
            } else {

                foreach ($classStudents as $classStudent) {
                    $noofperiods = PeriodMaster::find('LOCATE(node_id,"' . str_replace(',', '-', $classStudent->aggregate_key) . '" )' . " and user_type = 'student'");
                    if (count($monthhead) > 0) {
                        foreach ($monthhead as $hvalue) {
                            if (count($valhead) > 0) {
                                foreach ($valhead as $vvalue) {
                                    $atttaken = $stu_att_totByVal = 0;
                                    $counttaken = $monthlyPercent[$classStudent->loginid][$hvalue][$vvalue] ? ($monthlyPercent[$classStudent->loginid][$hvalue][$vvalue] ) : 0;
                                    $atttaken += ($counttaken / count($noofperiods));
                                    $monthlyPercent[$classStudent->loginid][$hvalue][$vvalue] = ($atttaken > 0) ? $atttaken : '';
                                }
                            }
                        }
                    }
                }
                $legend = array_unique($params['legend']);
                $result['thead'][$i][$j]['text'] = 'Students';
                $result['thead'][$i][$j]['rowspan'] = '2';
                $result['thead'][$i][$j]['colspan'] = '';

                if (count($monthhead) > 0) {
                    foreach ($monthhead as $hvalue) {
                        $j++;
                        $monthhdr = explode('-', $hvalue);
                        $result['thead'][$i][$j]['text'] = '<a href="javascript:void(0)" onclick="academicReportSettings.loadDailyReport(this)"
                       class="monthExpand" month="' . $monthhdr['0'] . '" year="' . $monthhdr['1'] . '" 
                           masterid="' . $master_id . '" reptyp="0"  >
                        <span class="btn btn-round btn-info">' . date('M - Y', strtotime('01-' . $hvalue)) . '</span></a>';
                        $result['thead'][$i][$j]['rowspan'] = '';
                        $result['thead'][$i][$j]['colspan'] = count($valhead);
                    }
                    $j++;
                    $result['thead'][$i][$j]['text'] = 'Total';
                    $result['thead'][$i][$j]['rowspan'] = '';
                    $result['thead'][$i][$j]['colspan'] = count($valhead);
                }
                $i++;
                if (count($monthhead) > 0) {
                    foreach ($monthhead as $hvalue) {
                        if (count($valhead) > 0) {
                            foreach ($valhead as $vvalue) {
                                $j++;
                                $result['thead'][$i][$j]['text'] = $vvalue;
                                $result['thead'][$i][$j]['rowspan'] = '';
                                $result['thead'][$i][$j]['colspan'] = '';
                            }
                        }
                    }
                    if (count($valhead) > 0) {
                        foreach ($valhead as $vvalue) {
                            $j++;
                            $result['thead'][$i][$j]['text'] = $vvalue;
                            $result['thead'][$i][$j]['rowspan'] = '';
                            $result['thead'][$i][$j]['colspan'] = '';
                        }
                    }
                }
            }

//            print_r($monthlyPercent);
//            print_r($legend);
//            print_r($valhead);
//                print_r($monthhead);
//                print_r($result);
//                exit;
            $newarr = array($classStudents, $monthlyPercent, $legend, $valhead, $monthhead, $result, $firstele, $lastele, $classroommas->name);
        }
        return $newarr;
    }

    public function getAttendancePercentMonthly($params) {
        $user_id = $params['user_id'];
        $user_type = $params['user_type']; //staff or student
        $params['type'] = 'attendance';
        $per = $attvalueDays = array();
        $monthhead = $params['monthhead'];
        $valhead = $params['valhead'];
        $legend = $params['legend'];
        $obj = new Cassandra();
        $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);

        if ($res) {
            $buildquery = "select * from month_attendance where"
                    . " user_id = '" . $user_id . "'";
//echo $buildquery;exit;
            if ($result = $obj->query($buildquery)) {
                for ($i = 0; $i < count($result); $i++):
                    $abbrevation = AttendanceSelectbox::findFirstById($result[$i]['value']);
                    $monthhead[] = $result[$i]['month'];
                    $valhead[$abbrevation->id] = $abbrevation->attendanceid;
                    $legend[$abbrevation->id] = $abbrevation->attendanceid . ' - ' . $abbrevation->attendancename;
                    $attvalueDays[$user_id][$result[$i]['month']][$abbrevation->attendanceid] = $result[$i]['counter_value'];
                endfor;
            }
        }
        $obj->close();
        ksort($legend);
        ksort($valhead);
        return array($attvalueDays, $monthhead, $valhead, $legend);
    }

    public function loadDailyReportAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->masterid = $master_id = $gnparam['masterid'] = $this->request->getPost('masterid');
            $this->view->reptyp = $reptyp = $gnparam['reptyp'] = $this->request->getPost('reptyp');
            $this->view->month = $month = $gnparam['month'] = $this->request->getPost('month');
            $this->view->year = $year = $gnparam['year'] = $this->request->getPost('year');
            $rescal = ReportsController::calculateAttendancePercentDaily($gnparam);
            $this->view->classStudents = $rescal[0];
            $this->view->dailyPercent = $rescal[1];
            $this->view->monthhead = $rescal[4];
            $this->view->valhead = $rescal[3];
            $this->view->legend = $rescal[2];
            $this->view->result = $rescal[5];
            $this->view->classroomname = $calres[6];
        }
    }

    public function calculateAttendancePercentDaily($gnparam) {
        $master_id = $gnparam['masterid'];
        $reptyp = $gnparam['reptyp'];
        $month = $gnparam['month'];
        $year = $gnparam['year'];
        $user_type = 'student';
        $newarr = array();
//        $assignedclass = GroupClassTeachers::findFirstById($master_id);
        $classroommas = ClassroomMaster::findFirstById($master_id);
        $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classroommas->id);
//        $res = ControllerBase::buildStudentQuery($classroommas->aggregated_nodes_id);
        $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                . 'stumap.subordinate_key,stumap.status,stuinfo.loginid,stuinfo.Date_of_Joining,'
                . ' stuinfo.Admission_no,stuinfo.photo,stuinfo.Student_Name '
                . ' FROM StudentMapping stumap LEFT JOIN'
                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                . '  and (' . implode(' or ', $res)  . ') '
                . ' ORDER BY stuinfo.Student_Name ASC';
        $classStudents = $this->modelsManager->executeQuery($stuquery);
        $dailyPercent = $result = array();
        if (count($classStudents) > 0):
            $params['monthhead'] = array();
            $params['valhead'] = array();
            foreach ($classStudents as $classStudent) {
                $overallPercent = array();
                $params['user_id'] = $classStudent->loginid;
                $params['student_id'] = $classStudent->student_info_id;
                $params['doj'] = $classStudent->Date_of_Joining;
                $params['user_type'] = 'student';
                $params['month'] = $month;
                $params['year'] = $year;
                $overallPercent = ReportsController::getAttendancePercentDaily($params);
                $dailyPercent = array_merge($dailyPercent, $overallPercent[0]);
//                    $params['monthhead'] = ($overallPercent[1]);
                $params['valhead'] = ($overallPercent[2]);
                $params['legend'] = ($overallPercent[3]);
            }
        endif;
        $i = $j = 0;

        $startdt = (date('Y-m-01 00:00:00', strtotime('01-' . $month . '-' . $year)));
        $enddt = (date('Y-m-01 00:00:00', strtotime('01-' . $month . '-' . $year . ' +1 month')));
        $begin = new DateTime($startdt);
        $end = new DateTime($enddt);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        foreach ($period as $dt) {
            $monthhead[] = strtotime($dt->format("Y-m-d H:i:s"));
        }
//            $monthhead = array_unique($params['monthhead']);
        $valhead = array_unique($params['valhead']);

        if ($reptyp == 1) {
            foreach ($classStudents as $classStudent) {
                $noofperiods = PeriodMaster::find('LOCATE(node_id,"' . str_replace(',', '-', $classStudent->aggregate_key) . '" )' . " and user_type = 'student'");
                if (count($monthhead) > 0) {
                    foreach ($monthhead as $hvalue) {
                        $atttaken = $stu_att_totByVal = 0;
                        if (count($valhead) > 0) {
                            foreach ($valhead as $vvalue) {
                                $student_att_props_val = AttendanceSelectbox::findFirst("attendance_for = '$user_type'"
                                                . ' and attendanceid= "' . $vvalue . '"');
                                $noofperiods = PeriodMaster::find('LOCATE(node_id,"' . str_replace(',', '-', $classStudent->aggregate_key) . '" )' . " and user_type = 'student'");
                                $counttaken = $dailyPercent[$classStudent->loginid][$hvalue][$vvalue] ? ($dailyPercent[$classStudent->loginid][$hvalue][$vvalue] ) : 0;
                                $atttaken += ($counttaken / count($noofperiods));
                                $stu_att_totByVal+= ($counttaken / count($noofperiods)) * $student_att_props_val->attendancevalue;
                            }
                        }
                        $dailyPercent[$classStudent->loginid][$hvalue]['percent'] = ($atttaken > 0) ? round(($stu_att_totByVal / ($atttaken * $noofperiods)) * 100, 2) : '';
                    }
                }
            }
            $legend = array();
            $result['thead'][$i][$j]['text'] = 'Students&nbsp;&nbsp;&nbsp;&nbsp;';
            $result['thead'][$i][$j]['rowspan'] = '';
            $result['thead'][$i][$j]['colspan'] = '';

            if (count($monthhead) > 0) {
                foreach ($monthhead as $hvalue) {
                    $j++;
                    $result['thead'][$i][$j]['text'] = date('D d', $hvalue);
                    $result['thead'][$i][$j]['rowspan'] = '';
                    $result['thead'][$i][$j]['colspan'] = '';
                }
                $j++;
                $result['thead'][$i][$j]['text'] = 'Total';
                $result['thead'][$i][$j]['rowspan'] = '';
                $result['thead'][$i][$j]['colspan'] = '';
            }
        } else {

            foreach ($classStudents as $classStudent) {
                $noofperiods = PeriodMaster::find('LOCATE(node_id,"' . str_replace(',', '-', $classStudent->aggregate_key) . '" )' . " and user_type = 'student'");
                if (count($monthhead) > 0) {
                    foreach ($monthhead as $hvalue) {
                        if (count($valhead) > 0) {
                            foreach ($valhead as $vvalue) {
                                $atttaken = $stu_att_totByVal = 0;
                                $counttaken = $dailyPercent[$classStudent->loginid][$hvalue][$vvalue] ? ($dailyPercent[$classStudent->loginid][$hvalue][$vvalue] ) : 0;
                                $atttaken += ($counttaken / count($noofperiods));
                                $dailyPercent[$classStudent->loginid][$hvalue][$vvalue] = ($atttaken > 0) ? $atttaken : '';
                            }
                        }
                    }
                }
            }
            $result['thead'][$i][$j]['text'] = 'Students&nbsp;&nbsp;&nbsp;&nbsp;';
            $result['thead'][$i][$j]['rowspan'] = '2';
            $result['thead'][$i][$j]['colspan'] = '';

            $legend = array_unique($params['legend']);
            if (count($monthhead) > 0) {
                foreach ($monthhead as $hvalue) {
                    $j++;
                    $result['thead'][$i][$j]['text'] = date('D d', $hvalue);
                    $result['thead'][$i][$j]['rowspan'] = '';
                    $result['thead'][$i][$j]['colspan'] = count($valhead);
                }
                $j++;
                $result['thead'][$i][$j]['text'] = 'Total';
                $result['thead'][$i][$j]['rowspan'] = '';
                $result['thead'][$i][$j]['colspan'] = count($valhead);
            }
            $i++;
            if (count($monthhead) > 0) {
                foreach ($monthhead as $hvalue) {
                    if (count($valhead) > 0) {
                        foreach ($valhead as $vvalue) {
                            $j++;
                            $result['thead'][$i][$j]['text'] = $vvalue;
                            $result['thead'][$i][$j]['rowspan'] = '';
                            $result['thead'][$i][$j]['colspan'] = '';
                        }
                    }
                }
                if (count($valhead) > 0) {
                    foreach ($valhead as $vvalue) {
                        $j++;
                        $result['thead'][$i][$j]['text'] = $vvalue;
                        $result['thead'][$i][$j]['rowspan'] = '';
                        $result['thead'][$i][$j]['colspan'] = '';
                    }
                }
            }
        }
//            print_r($dailyPercent);
//            print_r($legend);
//            print_r($valhead);
//            print_r($monthhead);
//            print_r($result);
//            exit;
        $newarr = array($classStudents, $dailyPercent, $legend, $valhead, $monthhead, $result, $classroommas->name);
        return $newarr;
    }

    public function month_compare($a, $b) {
        $t1 = strtotime('01-' . $a);
        $t2 = strtotime('01-' . $b);
        return $t1 - $t2;
    }

    public function getAttendancePercentDaily($params) {
        $user_id = $params['user_id'];
        $params['type'] = 'attendance';
        $attvalueDays = array();
        $month = $params['month'];
        $year = $params['year'];
        $start = strtotime('01-' . $month . '-' . $year);
        $end = strtotime(date('t-m-Y', strtotime('01-' . $month . '-' . $year)));
        $monthhead = $params['monthhead'];
        $valhead = $params['valhead'];
        $legend = $params['legend'];
        $obj = new Cassandra();
        $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);

        if ($res) {
            $buildquery = "select * from day_attendance where"
                    . " user_id = '" . $user_id . "'"
                    . " and date >= $start "
                    . " and date <= $end "
                    . " ALLOW FILTERING; ";
//echo $buildquery;exit;
            if ($result = $obj->query($buildquery)) {
                for ($i = 0; $i < count($result); $i++):
                    $abbrevation = AttendanceSelectbox::findFirstById($result[$i]['value']);
                    $monthhead[] = $result[$i]['date'];
                    $valhead[$abbrevation->id] = $abbrevation->attendanceid;
                    $legend[$abbrevation->id] = $abbrevation->attendanceid . ' - ' . $abbrevation->attendancename;
                    $nextval = $attvalueDays[$user_id][$result[$i]['date']][$abbrevation->attendanceid] ? $attvalueDays[$user_id][$result[$i]['date']][$abbrevation->attendanceid] + 1 : 1;
                    $attvalueDays[$user_id][$result[$i]['date']][$abbrevation->attendanceid] = $nextval;
                endfor;
            }
        }
        $obj->close();
        ksort($legend);
        ksort($valhead);
        return array($attvalueDays, $monthhead, $valhead, $legend);
    }

    function attendanceMonReportPrintAction() {
        $this->view->setTemplateAfter('printTemplates');
        if ($this->request->isPost()) {
            $this->view->masterid = $master_id = $gvnparams['masterid'] = $this->request->getPost('masterid');
            $this->view->reptyp = $reptyp = $gvnparams['reptyp'] = $this->request->getPost('reptyp');
            $calres = ReportsController::calculateAttendancePercentMonthly($gvnparams);
            $this->view->classStudents = $calres[0];
            $this->view->monthlyPercent = $calres[1];
            $this->view->monthhead = $calres[4];
            $this->view->valhead = $calres[3];
            $this->view->legend = $calres[2];
            $this->view->result = $calres[5];
            $this->view->st = $calres[6];
            $this->view->et = $calres[7];
        }
    }

    function attendanceDailyReportPrintAction() {
        $this->view->setTemplateAfter('printTemplates');
        if ($this->request->isPost()) {
            $this->view->masterid = $master_id = $gnparam['masterid'] = $this->request->getPost('masterid');
            $this->view->reptyp = $reptyp = $gnparam['reptyp'] = $this->request->getPost('reptyp');
            $this->view->month = $month = $gnparam['month'] = $this->request->getPost('month');
            $this->view->year = $year = $gnparam['year'] = $this->request->getPost('year');
            $rescal = ReportsController::calculateAttendancePercentDaily($gnparam);
            $this->view->classStudents = $rescal[0];
            $this->view->dailyPercent = $rescal[1];
            $this->view->monthhead = $rescal[4];
            $this->view->valhead = $rescal[3];
            $this->view->legend = $rescal[2];
            $this->view->result = $rescal[5];
        }
    }

    public function loadExamReportAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $master_id = $this->request->getPost('masterid');
            $examid = $this->request->getPost('examid');
            $classroommas = ClassroomMaster::findFirstById($master_id);
            $grpsubjids = GroupSubjectsTeachers::find('classroom_master_id = ' . $classroommas->id);
            $subj_Ids = $subjids = $finids = array();
            if (count($grpsubjids) > 0):
                foreach ($grpsubjids as $pusid) {
                    $finids[] = $pusid->id;
                    $subj_Ids[] = $pusid->subject_id;
                }
                $subjids = array_unique($finids);
            endif;

//            $res = ControllerBase::buildStudentQuery($classroommas->aggregated_nodes_id);
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classroommas->id);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status,stuinfo.loginid,stuinfo.Date_of_Joining,'
                    . ' stuinfo.Admission_no,stuinfo.photo,stuinfo.Student_Name '
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                        . ' and (' . implode(' or ', $res)  . ') '
                    . ' ORDER BY stuinfo.Admission_no ASC';
            $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);
            $this->view->masterid = $master_id;
            $this->view->subject_id = array_unique($subj_Ids);
            $this->view->mainExId = $examid;
            $this->view->sub_mas_id = implode(',', $subjids);
            $this->view->classroomname = $classroommas->name;
            $this->view->exam = Mainexam ::findFirstById($examid);
        }
    }

    public function loadClassTestReportAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $identity = $this->auth->getIdentity();
            $this->view->staff = $staff = StaffInfo::findFirstByLoginid($identity['name']);
            $master_id = $this->request->getPost('masterid');
            $subjid = $this->request->getPost('subjid');
            $this->view->selsubject = $selsubject = OrganizationalStructureValues::findFirstById($subjid);
            if (!(in_array('SuperAdmin', $identity['role_name'])))
                $query[] = 'FIND_IN_SET( ' . $staff->id . ' , teachers_id)';
            $query[] = 'classroom_master_id =' . $master_id;
            $query[] = 'subject_id = ' . $subjid;
            $classroommas = ClassroomMaster::findFirstById($master_id);
            $grpsubjids = GroupSubjectsTeachers::find(implode(' and ', $query));
            $subj_Ids = $subjids = $finids = $clststQury = array();
            if (count($grpsubjids) > 0):
                foreach ($grpsubjids as $pusid) {
                    $finids[] = $pusid->id;
                    $subj_Ids[] = $pusid->subject_id;
                }
                $subjids = array_unique($finids);
            endif;
            $finsids = $subj_Ids;
            $subjects = count($subj_Ids) > 0 ? ControllerBase::getAllPossibleSubjectsold($subj_Ids) : array();
            if (count($subjects) > 0):
                foreach ($subjects as $sid) {
                    $finsids[] = $sid->id;
                }
                $finsids = array_unique($finsids);
            endif;
            if (count($subjids) > 0) {

                $clststQury[] = " grp_subject_teacher_id IN (" . implode(' , ', $subjids) . ")";
            }
            if (count($subj_Ids) > 0) {
                $clststQury[] = "  subjct_modules IN (" . implode(' , ', $finsids) . ")";
            }


            $res = ControllerBase::buildStudentQuery($classroommas->aggregated_nodes_id);
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classroommas->id);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status,stuinfo.loginid,stuinfo.Date_of_Joining,'
                    . ' stuinfo.Admission_no,stuinfo.photo,stuinfo.Student_Name '
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                    . '  and (' . implode(' or ', $res)  . ')'
                    . ' ORDER BY stuinfo.Student_Name ASC';
            $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);
            $this->view->clstest = $clstest = ClassTest::find(implode(' and ', $clststQury));
            $this->view->subjects = $subjects;
            $this->view->subject_id = array_unique($subj_Ids);
            $this->view->sub_mas_id = implode(',', $subjids);
            $this->view->classroomname = $classroommas->name;
        }
    }

    public function loadAssignmentReportAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $identity = $this->auth->getIdentity();
            $this->view->staff = $staff = StaffInfo::findFirstByLoginid($identity['name']);
            $master_id = $this->request->getPost('masterid');
            $subjid = $this->request->getPost('subjid');
            $this->view->selsubject = $selsubject = OrganizationalStructureValues::findFirstById($subjid);
            if (!(in_array('SuperAdmin', $identity['role_name'])))
                $query[] = 'FIND_IN_SET( ' . $staff->id . ' , teachers_id)';
            $query[] = 'classroom_master_id =' . $master_id;
            $query[] = 'subject_id = ' . $subjid;
            $classroommas = ClassroomMaster::findFirstById($master_id);
            $grpsubjids = GroupSubjectsTeachers::find(implode(' and ', $query));
            $subj_Ids = $subjids = $finids = $clststQury = array();
            if (count($grpsubjids) > 0):
                foreach ($grpsubjids as $pusid) {
                    $finids[] = $pusid->id;
                    $subj_Ids[] = $pusid->subject_id;
                }
                $subjids = array_unique($finids);
            endif;
            $finsids = $subj_Ids;
            $subjects = count($subj_Ids) > 0 ? ControllerBase::getAllPossibleSubjectsold($subj_Ids) : array();
            if (count($subjects) > 0):
                foreach ($subjects as $sid) {
                    $finsids[] = $sid->id;
                }
                $finsids = array_unique($finsids);
            endif;
            if (count($subjids) > 0) {

                $clststQury[] = " grp_subject_teacher_id IN (" . implode(' , ', $subjids) . ")";
            }
            if (count($subj_Ids) > 0) {
                $clststQury[] = "  subjct_modules IN (" . implode(' , ', $finsids) . ")";
            }


//            $res = ControllerBase::buildStudentQuery($classroommas->aggregated_nodes_id);
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classroommas->id);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status,stuinfo.loginid,stuinfo.Date_of_Joining,'
                    . ' stuinfo.Admission_no,stuinfo.photo,stuinfo.Student_Name '
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                    . '  and (' . implode(' or ', $res) . ')'
                    . ' ORDER BY stuinfo.Student_Name ASC';
            $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);
            $this->view->ass = $ass = AssignmentsMaster::find(implode(' and ', $clststQury));
            $this->view->subjects = $subjects;
            $this->view->subject_id = array_unique($subj_Ids);
            $this->view->sub_mas_id = implode(',', $subjids);
            $this->view->classroomname = $classroommas->name;
        }
    }

    public function loadClassRatingReportAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {

            $this->view->class_master_id = $master_id = $this->request->getPost('masterid');
            $this->view->rating_division_id = $catid = $this->request->getPost('catid');
            $this->view->category_paramaters = $category_paramaters = RatingCategoryMaster::find();
            $this->view->division = RatingDivision::findFirstById($catid);
            $classroommas = ClassroomMaster::findFirstById($master_id);
//            $res = ControllerBase::buildStudentQuery($classroommas->aggregated_nodes_id);
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classroommas->id);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status,stuinfo.loginid,stuinfo.Date_of_Joining,'
                    . ' stuinfo.Admission_no,stuinfo.photo,stuinfo.Student_Name '
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                    . '  and (' . implode(' or ', $res) . ') '
                    . ' ORDER BY stuinfo.Student_Name ASC';
            $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);
            $this->view->classroomname = $classroommas->name;

            $identity = $this->auth->getIdentity();
            $this->view->staff = $staff = StaffInfo::findFirstByLoginid($identity['name']);
            $query = $subj_Ids = $subjids = $finids = $finsids = $clststQury = array();

            if (!(in_array('SuperAdmin', $identity['role_name'])))
                $query[] = 'FIND_IN_SET( ' . $staff->id . ' , teachers_id)';
            $query[] = 'classroom_master_id =' . $master_id;
//            echo implode(' and ', $query);exit;
            $grpsubjids = GroupClassTeachers::find(implode(' and ', $query));
            if (count($grpsubjids) > 0):
                foreach ($grpsubjids as $pusid) {
                    $finids[] = $pusid->id;
                }
                $subjids = array_unique($finids);
            endif;
//            print_r( implode(',', $subjids));exit;
            $this->view->sub_mas_id = implode(',', $subjids);
        }
    }

    public function loadSubjectRatingReportAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {

            $identity = $this->auth->getIdentity();
            $this->view->staff = $staff = StaffInfo::findFirstByLoginid($identity['name']);
            $this->view->class_master_id = $master_id = $this->request->getPost('masterid');
            $this->view->rating_division_id = $catid = $this->request->getPost('catid');
            $this->view->subject = $subjid = $this->request->getPost('subjid');
            $this->view->category_paramaters = $category_paramaters = RatingCategoryMaster::find();
            $this->view->division = RatingDivision::findFirstById($catid);
            $this->view->selsubject = $selsubject = OrganizationalStructureValues::findFirstById($subjid);
            $classroommas = ClassroomMaster::findFirstById($master_id);
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classroommas->id);
//            $res = ControllerBase::buildStudentQuery($classroommas->aggregated_nodes_id);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status,stuinfo.loginid,stuinfo.Date_of_Joining,'
                    . ' stuinfo.Admission_no,stuinfo.photo,stuinfo.Student_Name '
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                    . '  and (' . implode(' or ', $res) . ')'
                    . ' ORDER BY stuinfo.Student_Name ASC';
            $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);

            $query = $subj_Ids = $subjids = $finids = $finsids = $clststQury = array();

            if (!(in_array('SuperAdmin', $identity['role_name'])))
                $query[] = 'FIND_IN_SET( ' . $staff->id . ' , teachers_id)';
            $query[] = 'classroom_master_id =' . $master_id;
            $query[] = 'subject_id = ' . $subjid;
            $grpsubjids = GroupSubjectsTeachers::find(implode(' and ', $query));
            if (count($grpsubjids) > 0):
                foreach ($grpsubjids as $pusid) {
                    $finids[] = $pusid->id;
                    $subj_Ids[] = $pusid->subject_id;
                }
                $subjids = array_unique($finids);
            endif;
            $finsids = array_unique($subj_Ids);
            $this->view->sub_mas_id = implode(',', $subjids);
            $this->view->subject_id = array_unique($subj_Ids);
            $this->view->classroomname = $classroommas->name;
        }
    }

    public function loadHomeWorksBySubjectAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $identity = $this->auth->getIdentity();
            $this->view->staff = $staff = StaffInfo::findFirstByLoginid($identity['name']);
            $master_id = $this->request->getPost('masterid');
            $subjid = $this->request->getPost('subjid');
            $this->view->selsubject = $selsubject = OrganizationalStructureValues::findFirstById($subjid);
            if (!(in_array('SuperAdmin', $identity['role_name'])))
                $query[] = 'FIND_IN_SET( ' . $staff->id . ' , teachers_id)';
            $query[] = 'classroom_master_id =' . $master_id;
            $query[] = 'subject_id = ' . $subjid;
            $grpsubjids = GroupSubjectsTeachers::find(implode(' and ', $query));
            $subj_Ids = $subjids = $finids = $clststQury = array();
            if (count($grpsubjids) > 0):
                foreach ($grpsubjids as $pusid) {
                    $finids[] = $pusid->id;
                    $subj_Ids[] = $pusid->subject_id;
                }
                $subjids = array_unique($finids);
            endif;
            if (count($subjids) > 0) {

                $clststQury[] = " grp_subject_teacher_id IN (" . implode(' , ', $subjids) . ")";
            }
            if (count($subj_Ids) > 0) {
                $clststQury[] = "  subjct_modules IN (" . implode(' , ', $subj_Ids) . ")";
            }
            $conditionvals = (count($clststQury) > 0) ? implode(' and ', $clststQury) : '';

            $this->view->homeworks = $homeworks = HomeWorkTable::find($conditionvals . ' ORDER BY hmwrkdate desc');
        }
    }

}
