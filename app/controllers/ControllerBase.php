<?php

use Phalcon\Mvc\Dispatcher,
    Phalcon\Acl;

class ControllerBase extends \Phalcon\Mvc\Controller {

    /**
     * The filepath of the ACL role file  
     *
     * @var string
     */
    private $filename = 'role_resource.json';
    private $nodearr = Array();

    /**
     * Execute before the router so we can determine if this is a provate controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher) {
        $controllerName = $dispatcher->getControllerName();
        // Check if the user have permission to the current option
        $actionName = $dispatcher->getActionName();
        // Get the current identity
        $identity = $this->auth->getIdentity();
//        $this->auth->remove();
//       echo'<pre>';    print_r($identity);   exit;


        $acl = $this->getAcl();
        $allow = 0;
        foreach ($identity['role_name'] as $role) {
//            echo'<pre>';print_r($acl);exit;
            if ($acl->isAllowed($role, $controllerName, $actionName)) {
                $allow = 1;
                break;
            }
        }
        if ($allow == 0) {
            $dispatcher->forward(array(
                'controller' => 'errorpage',
                'action' => 'accessdenied'
            ));
//            return false;
        }
    }

    public function afterExecuteRoute($dispatcher) {
        $controllerName = $dispatcher->getControllerName();
        // Check if the user have permission to the current option
        $actionName = $dispatcher->getActionName();
//echo $controllerName.'/'.$actionName;exit;
        //Check For Resource Nodes 
//        if ($controllerName == 'organizational-structure' && $actionName == 'updateOrgValues'):
//            $params = $dispatcher->getParams();
//            $dispatcher->forward(array(
//                'controller' => 'calendar',
//                'action' => 'addNewResource',
//                "params" => $params
//            ));
//
//        endif;
    }

    /**
     * Returns the ACL list
     *
     * @return Phalcon\Acl\Adapter\Memory
     */
    public function getAcl() {
        $acl = new Phalcon\Acl\Adapter\Memory();
        $acl->setDefaultAction(Phalcon\Acl::DENY);
        // Check if the ACL is already generated
        if (file_exists(DATA_DIR . $this->filename)) {
            // Get the ACL from the data file
            $jdata = file_get_contents(DATA_DIR . $this->filename);
            $rolearr = json_decode($jdata);
//            $acl->addRole(new Phalcon\Acl\Role('Guests'));

            foreach ($rolearr->PublicResource as $resource => $actions) {
                $acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
            }

            foreach ($rolearr->PrivateResource as $resource => $actions) {
                $acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
            }
            foreach ($rolearr->Roles as $role) {
                foreach ($role as $specificrole) {
                    $acl->addRole(new Phalcon\Acl\Role($specificrole));
                }
            }
            foreach ($rolearr->Roles->Default_Roles as $role) {
                foreach ($rolearr->PublicResource as $resource => $actions) {
                    foreach ($actions as $action) {
                        $acl->allow($role, $resource, $action);
                    }
                }
            }
//            echo'<pre>'; print_r('sa'); exit;
//            echo'<pre>';print_r($acl);exit;
            //Grant acess to private area to role Users
            foreach ($rolearr->RoleResource as $role => $resources) {
                foreach ($resources as $resource => $actions) {
                    foreach ($actions as $action) {
                        $acl->allow($role, $resource, $action);
                    }
                }
            }
        }
        return $acl;
    }

    protected function forward($uri) {
        return $this->response->redirect($uri);
    }

    public function get_current_academic_year() {

        $ids = OrganizationalStructureMaster::findFirst('cycle_node = 1 and module = "StudentCycleNode"')->id;
        $cacdyr = OrganizationalStructureValues::findFirst('org_master_id = ' . $ids . ' and status = "C"');
        return $cacdyr;
    }

    public function getStudentPad($usertyp) {
        $user = $usertyp == 'student' ? 'stupadding' : 'stfpadding';
        $default = $usertyp == 'student' ? '*****' : '****';
        $sstringLen = Settings::findFirstByVariableName($user)->variableValue ?
                Settings::findFirstByVariableName($user)->variableValue : $default;
        return $sstringLen;
    }

    public function getCurrentAcademicYear($actualstructural) {

        $ids = OrganizationalStructureMaster::findFirst('cycle_node = 1 and module = "StudentCycleNode"')->id;
        if ($actualstructural) {
            $cacdyr = OrganizationalStructureValues::findFirst('org_master_id = ' . $ids
                            . ' and status = "C"'
                            . ' and parent_id IN (' . $actualstructural . ')');
        } else {
            $cacdyr = OrganizationalStructureValues::findFirst('org_master_id = ' . $ids . ' and status = "C"');
        }
        return $cacdyr;
    }

    public function getCurrentStaffCycle($actualstructural) {

        $ids = OrganizationalStructureMaster::findFirst('cycle_node = 1 and module = "Department"')->id;
        if ($actualstructural) {
            $cacdyr = OrganizationalStructureValues::findFirst('org_master_id = ' . $ids
                            . ' and status = "C"'
                            . ' and parent_id IN (' . $actualstructural . ')');
        } else {
            $cacdyr = OrganizationalStructureValues::findFirst('org_master_id = ' . $ids . ' and status = "C"');
        }
        return $cacdyr;
    }

    public function getCurrentFinancialYear($actualstructural) {

        $ids = OrganizationalStructureMaster::findFirst('cycle_node = 1 and module = "Ledger"')->id;
        if ($actualstructural) {
            $cacdyr = OrganizationalStructureValues::findFirst('org_master_id = ' . $ids
                            . ' and status = "C"'
                            . ' and parent_id IN (' . $actualstructural . ')');
        } else {
            $cacdyr = OrganizationalStructureValues::findFirst('org_master_id = ' . $ids . ' and status = "C"');
        }
        return $cacdyr;
    }

    public function getAllModules() {

        $packageId = Settings::findFirstByVariableName('package_id')->variableValue;
        $data = array('package' => $packageId);
        $data_string = json_encode($data);
        $responseParam = json_decode(IndexController::curlIt(SALESAPI . 'package/getPackageModulesByPackageId', $data_string));
//        print_r($responseParam);exit;
        return $responseParam;
    }

    public function add_transaction($param) {
        $transaction = new Transaction();
        $transaction->transactiontype = $param['transactiontype'];
        $transaction->debit = $param['debit'];
        $transaction->credit = $param['credit'];
        $transaction->amount = $param['amount'];
        $transaction->created_by = $param['created_by'];
        $transaction->created_date = $param['created_date'];
        $transaction->details = $param['details'];
        $transaction->voucherid = $param['voucherid'];
        $transaction->voucher_date = $param['voucher_date'];

        if ($transaction->save()) {
            return "success";
        } else {
            return "Error";
        }
    }

    public function _getValueTreeFor($nodeid) {
        $exist = OrganizationalStructureValues::find('parent_id =' . $nodeid);
        $nodeval = OrganizationalStructureValues::findFirstById($nodeid);
        $masternode = OrganizationalStructureMaster::findFirstById($nodeval->org_master_id);
        $choutput = array();
        if (count($exist) > 0) {
            $nodearr = array();
            $nodearr['id'] = $nodeval->id;
            $nodearr['name'] = $nodeval->name;


            foreach ($exist as $chl) {
                $nodearr['children'][] = ControllerBase::_getValueTreeFor($chl->id);
            }
            $choutput = $nodearr;
        } else {
            $nodearr = array();
            $nodearr['id'] = $nodeval->id;
            $nodearr['name'] = $nodeval->name;

            $choutput = $nodearr;
        }

        return $choutput;
    }

    public function _getValueTreeMainNodes($nodeid) {
        $exist = OrganizationalStructureValues::find('parent_id =' . $nodeid);
        $nodeval = OrganizationalStructureValues::findFirstById($nodeid);
        $masternode = OrganizationalStructureMaster::findFirst('id=' . $nodeval->org_master_id . ' and is_subordinate!=1');
//        echo $masternode->is_subordinate . ' : ' . $nodeval->name . '<br>';
        $choutput = '';
        if (!empty($masternode)) {
            $choutput = array();
            if (count($exist) > 0) {
                $nodearr = array();
                $nodearr['id'] = $nodeval->id;
                $nodearr['name'] = $nodeval->name;
                foreach ($exist as $chl) {
                    $loopres = ControllerBase::_getValueTreeMainNodes($chl->id);
                    if (!empty($loopres))
                        $nodearr['children'][] = $loopres;
                }
                $choutput = $nodearr;
            } else {
                $nodearr = array();
                $nodearr['id'] = $nodeval->id;
                $nodearr['name'] = $nodeval->name;

                $choutput = $nodearr;
            }
        }
        return $choutput;
    }

    public function getCycleNodeId($nodeid) {
        //Module that returns the cycle node id for the given node
        $master = OrganizationalStructureMaster::findfirst('cycle_node = 1')->id;
        $cycle_node_values = OrganizationalStructureValues::find('org_master_id = ' . $master);
        foreach ($cycle_node_values as $cycle_node_value) {
            $cycle_node_ids[] = $cycle_node_value->id;
        }

        $node = OrganizationalStructureValues::findfirst("id = $nodeid");
        while ($node->parent_id != 0) {
            if (in_array($node->id, $cycle_node_ids)) {
                return ($node->id);
            } else {
                $node = OrganizationalStructureValues::findfirst("id = $node->parent_id");
            }
        }

        return ($node->org_master_id);
    }

    public function _getValue($nodeid, $id, $name, &$nodearr) {

        $exist = OrganizationalStructureValues::find('parent_id =' . $nodeid);
        $nodeval = OrganizationalStructureValues::findFirstById($nodeid);
        $masternode = OrganizationalStructureMaster::findFirstById($nodeval->org_master_id);

        if (count($exist) > 0) {
            foreach ($exist as $chl) {
                $nodearr[$nodeid]['id'] = $id;
                $nodearr[$nodeid]['name'] = $name;
                ControllerBase::_getValue($chl->id, $id . '-' . $chl->id, $name . '>>' . $chl->name, $nodearr);
            }
        } else {
            $nodearr[$nodeid]['id'] = $id;
            $nodearr[$nodeid]['name'] = $name;
        }
        return $nodearr;
    }

    public function _fetchCommonPath($node_id) {
        //explode the string by ':' to seperate out each selected node
        $nodes = explode(':', $node_id);

        $arrLength = count($nodes);
        if ($arrLength < 2) {
            return $node_id;
        }

        // Determine loop length
        // Find shortest string in array: Can bring down iterations dramatically, but the function arrayStrLenMin() itself can cause ( more or less) iterations.
//        $end = count(explode('-', $arr[0])); //strlen($arr[0]);

        foreach ($nodes as $node) {
            $node_split[] = explode('-', $node);
        }

        //Convert array to an array of string lengths
        $lengths = array_map('count', $node_split);
        $end = max($lengths);


        for ($i = 1; $i <= $end; $i++) {
            // Grab the part from 0 up to $i
            $commonStrMax = array_slice($node_split[0], 0, $i);

            // Loop through all the values in array, and compare if they match
            foreach ($node_split as $key => $str) {

                // Didn't match, return the part that did match
                $result = array_intersect($commonStrMax, array_slice($str, 0, $i));
                if (count($result) != count($commonStrMax)) {
                    return array_slice($commonStrMax, 0, $i - 1);
                }
            }
        }
        // Special case: No mismatch (hence no return) happened until loop end!
        return $commonStrMax; // Thus entire first common string is the common prefix!
    }

    public function _getSchoolVariables() {
        $svar = Settings::find();
        $arrval = array();
        foreach ($svar as $value) {
            $arrval[$value->variableName] = $value->variableValue;
        }
        return $arrval;
    }

    public function _convertWords($number) {
        if (($number < 0) || ($number > 999999999)) {
            throw new Exception("Number is out of range");
        }
        $Gn = floor($number / 1000000);  /* Millions (giga) */
        $number -= $Gn * 1000000;
        $kn = floor($number / 1000);     /* Thousands (kilo) */
        $number -= $kn * 1000;
        $Hn = floor($number / 100);      /* Hundreds (hecto) */
        $number -= $Hn * 100;
        $Dn = floor($number / 10);       /* Tens (deca) */
        $n = $number % 10;               /* Ones */

        $res = "";

        if ($Gn) {
            $res .= FeePaymentController::_convertWords($Gn) . " Million";
        }

        if ($kn) {
            $res .= (empty($res) ? "" : " ") .
                    FeePaymentController::_convertWords($kn) . " Thousand";
        }

        if ($Hn) {
            $res .= (empty($res) ? "" : " ") .
                    FeePaymentController::_convertWords($Hn) . " Hundred";
        }
        $ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
            "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
            "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
            "Nineteen");
        $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
            "Seventy", "Eigthy", "Ninety");

        if ($Dn || $n) {
            if (!empty($res)) {
                $res .= " and ";
            }

            if ($Dn < 2) {
                $res .= $ones[$Dn * 10 + $n];
            } else {
                $res .= $tens[$Dn];

                if ($n) {
                    $res .= "-" . $ones[$n];
                }
            }
        }

        if (empty($res)) {
            $res = "zero";
        }


        return $res;
    }

    public function caculate_month_attendanceold($param) {

        $month = $param['month'];
        $year = $param['year'];
        $workndays = 0;
//        $teacher_id = $param['staffid'];
//        echo $month.' '.$year.' '.$teacher_id.'<br>';
//        $getDepart = StaffInfo::findFirstById($teacher_id)->Department;
//        $academic_yr = $academic_yr = ControllerBase::get_current_academic_year();
        // Check for complete attendance
        $last = date("t", mktime(0, 0, 0, $month, 1, $year));
        $currentlast = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
        $start_date = mktime(0, 0, 0, $month, 1, $year);
        $end_date = mktime(0, 0, 0, $month, $last, $year);
        $end = ($currentlast < $end_date) ? $currentlast : $end_date;
//        echo $start_date.' '. $end;
//                echo   $start_date.$end;exit;
        $currentdate = $start_date;
        while ($end >= $currentdate) { //loop through month
//       echo date('d-m-Y', $currentdate).'<br>';
            //check for attendance
//             echo 'user_type = "staff" and date = "' . $currentdate . '"' .
//                            " and master_id = $getDepart  and academic_year_id = $academic_yr";exit;
            $attendaceDays = AttendanceDays::findFirst('user_type = "staff" and date = "' . $currentdate . '"');

            if (!$attendaceDays) {
                return 0;
            } else {
                $workndays ++;
                $attFreq = AttendanceFrequency::find(" frequency_for ='staff'");
                if (!$attFreq) {
                    return 0;
                } else {
                    foreach ($attFreq as $value) {
                        for ($i = 0; $i < $value->frequency; $i++) {
                            $periodAtt = AttendancePeriods::find("att_days_id = $attendaceDays->id");
                            foreach ($periodAtt as $attper) {

                                if (!$attper) {
                                    return 0;
                                }
                            }
                        }
                    }
                }
            }
            $currentdate = strtotime("+1 day", $currentdate);
        }

        return $workndays;
    }

    public function caculate_month_attendance($param) {

        $month = $param['month'];
        $year = $param['year'];
        $monthyear = $month . '-' . $year;
        $attvalueDays = 0;
        $staff = StaffInfo::findFirstById($param['staffid']);

        $obj = new Cassandra();
        $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);

        if ($res) {
            $buildquery = "select counter_value from month_attendance where"
                    . " user_id = '" . $staff->loginid . "'"
                    . " and month ='$monthyear' ";

            if ($result = $obj->query($buildquery)) {
                for ($i = 0; $i < count($result); $i++):
                    $attvalueDays += $result[$i]['counter_value'];
                endfor;
            }
        }
        $obj->close();
        $freq = PeriodMaster::find('LOCATE(node_id,"' . str_replace(',', '-', $staff->aggregate_key) . '" )' . " and user_type = 'staff'");
        $workndays = $attvalueDays / $freq;
        return $workndays;
    }

    public function caculate_payroll_attendance($param) {

        $month = $param['month'];
        $year = $param['year'];
        $monthyear = $month . '-' . $year;
        $attvalueDays = array();
        $staff = StaffInfo::findFirstById($param['staffid']);
        $obj = new Cassandra();
        $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);

        if ($res) {
            $buildquery = "select * from month_attendance where"
                    . " user_id = '" . $staff->loginid . "'"
                    . " and month ='$monthyear' ";

//        echo '<pre>'; print_r($buildquery);exit;
            if ($result = $obj->query($buildquery)) {
                for ($i = 0; $i < count($result); $i++):
                    $abbrevation = AttendanceSelectbox::findFirstById($result[$i]['value']);
                    $attvalueDays1 = array('valueid' => '#' . $abbrevation->attendanceid,
                        'month' => $result[$i]['month'],
                        'counter_value' => $result[$i]['counter_value']);
                    $attvalueDays[] = $attvalueDays1;
                endfor;
            }
        }
        $obj->close();
        return $attvalueDays;
    }

    public function caculate_payroll_attendanceold($param) {

        $month = $param['month'];
        $year = $param['year'];
        $teacher_id = $param['staffid'];
        $start_date = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
        $end_date = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));

//        print_r($month . " ");
//        print_r($year . " ");
//        print_r($start_date . " ");
//        print_r($end_date . " ");


        $start_date = strtotime($start_date . " 00:00:00");
        $end_date = strtotime($end_date . " 00:00:00 ");

//        print_r($start_date . " ");
//        print_r($end_date . " ");
        //Get the staff department id
//        $staff_department = StaffInfo::findfirst('id = ' . $teacher_id)->Department;
        //Get the attendance frequency for the department that the staff belongs to
        $attendance_frequency = AttendanceFrequency::findfirst('frequency_for = "staff"')->frequency;
        //print_r("Attendance_frequency = " . $attendance_frequency);
        //
        //Get the attendance selectbox value for staff other than present
        $attendance_values = AttendanceSelectbox::find('attendance_for = "staff" and attendanceid <> "P" ORDER BY substitute ASC');

        //Get all the attendance_days id for the given date and department
        $phql = 'Select group_concat(a.id) as id from attendance_days as a where' .
                ' a.date >= ' . $start_date . ' and a.date <= ' . $end_date .
                ' and a.day_type = 2 and a.user_type = "staff" ';


        $results = $this->db->query($phql);
        $result = $results->fetch();
        $ids = $result['id'];

        foreach ($attendance_values as $attendance_value) {
            $result['count'] = $value_sum = $value_count = 0;
            //get the sum of attendance values and its count for the attendance ids that have been got before.
            if (count($ids) > 0):
                $phql = 'Select SUM(1- c.attendancevalue) as sum, count(*) as count from attendance_values as a '
                        . ' INNER JOIN attendance_selectbox as c on c.id=a.value_id'
                        . '  where a.att_days_id IN (' . $ids . ')'
                        . ' and a.value_id = ' . $attendance_value->id
                        . ' and a.user_id = ' . $teacher_id;
//            print_r($phql);
//            exit;
                $results = $this->db->query($phql);
                $result = $results->fetch();
            endif;


//            $formula = $attendance_value->school_cal_formula;
            $n = $result['count'];
            $f = $attendance_frequency;
//            eval($attendance_value->school_cal_formula);
//            echo '<pre>c'.$n.' '.$f;; print_r($attendance_value->school_cal_formula);echo 'r'.$res.'x'. $x;
            $value_sum = $n;
//            echo $attendance_frequency.': '.$value_sum;
            $value_count = $result['count'] / $attendance_frequency;
//            print_r("Value Sum:" . $value_sum);
//            print_r("Value Count:" . $value_count);   
//            exit;
            //Addition processing when salary deduction is needed
//            if ($attendance_value->punish == 1) {
//
//                //settings has been done day wise, so need to alter the query and get the count again
//                if ($attendance_value->punish_type == 1) {
//                    //Daywise settings
//                    //Below query takes the day into account when the day contains atleast 1 entry
//                    $phql = 'Select count(*) as count from attendance_values as a, attendance_selectbox as c where' .
//                            ' a.att_days_id IN (' . $ids . ')' .
//                            ' and a.value_id = ' . $attendance_value->id . ' and a.user_id = ' .
//                            $teacher_id . ' and c.id = ' . $attendance_value->id . ' GROUP BY a.att_days_id';
//
//                    $results = $this->db->query($phql);
//                    $result = $results->fetchAll();
//
//                    $value_count = count($result);
//                }
//
//                if ($value_count > $attendance_value->punish_limit) {
//                    $punishable_instances = $value_count - $attendance_value->punish_limit;
//                    $punishable_instances_grouped = round($punishable_instances / ($attendance_value->punish_group), 2);
//                    //print_r('After Punish:');
//                    $value_sum = $value_sum + ($punishable_instances_grouped * $attendance_value->deduct_salary);
//                    //print_r($value_sum);
//                }
//            }
//
//            if ($attendance_value->show_in_payslip == 1 || $attendance_value->show_in_payslip == "") {
//                $output['school'][$attendance_value->id] = array('id' => $attendance_value->id,
//                    'name' => $attendance_value->attendancename,
//                    'instances' => $value_count,
//                    'deductions' => $value_sum);
//                $output['payslip'][$attendance_value->id] = $output['school'][$attendance_value->id];
//            } else {
//                
//            }
            $output['school'][$attendance_value->id] = array('id' => $attendance_value->id,
                'name' => $attendance_value->attendancename,
                'instances' => $value_count,
                'deductions' => $value_sum);

            $output['payslip'][$attendance_value->substitute]['instances'] += $value_count;
            $output['payslip'][$attendance_value->substitute]['deductions'] += $value_sum;
        }
//        echo "<pre>";
//        print_r($output);
//        exit;
        return($output);
    }

    public function buildTree($rcpathname, $parentId) {
        $name = OrganizationalStructureValues::findfirst('id = ' . $parentId);
        $iscycle = OrganizationalStructureMaster::findFirstById($name->org_master_id);
        if (!in_array($iscycle->module, array("structural", "StaffCycleNode", "Ledger"))) {
            $rcpathname[] = $name->name;
            $rcpathname = ControllerBase::buildTree($rcpathname, $name->parent_id);
        }
        return $rcpathname;
    }

    public function loadCombinations($param) {
        $identity = $this->auth->getIdentity();
        if (in_array('Staff', $identity['role_name'])) {
            $this->view->staff = $staff = StaffInfo::findFirstByLoginid($identity['name']);
            $query = (in_array('SuperAdmin', $identity['role_name'])) ? '' : 'FIND_IN_SET( ' . $staff->id . ' , teachers_id)';
            $combinations = array();
            $getPermissionType = Permissions::findFirstByPermissionFor($param['type']);
            if ($getPermissionType->permission == '1') { // get class teacher combinations
                $assignedCount = GroupClassTeachers::find($query);
                if (count($assignedCount) > 0) {
                    foreach ($assignedCount as $assignedclass) {
                        $classroom = ClassroomMaster::findFirstById($assignedclass->classroom_master_id);
                        $name_param = ControllerBase::getNameForKeys($classroom->aggregated_nodes_id);
                        $combinations[$assignedclass->id]['name'] = $classroom->name;
                        $combinations[$assignedclass->id]['classroom'] = $classroom->id;
                    }
                    $type = 'classTeacher';
                } else {
                    $errorMessage = 'No Class assigned for you yet!';
                }
            } else {
                $assignedCount = GroupSubjectsTeachers::find($query);
                if (count($assignedCount) > 0) {
                    foreach ($assignedCount as $assignedclass) {
                        $classroom = ClassroomMaster::findFirstById($assignedclass->classroom_master_id);
                        $getCombinations = OrganizationalStructureValues::findFirst('id=' . $assignedclass->subject_id);
                        $name_param = ControllerBase::getNameForKeys($classroom->aggregated_nodes_id);
                        $combinations[$assignedclass->id]['name'] = $classroom->name;
                        $combinations[$assignedclass->id]['subject'] = $getCombinations->name;
                        $combinations[$assignedclass->id]['classroom'] = $classroom->id;
                    }
                    $type = 'subjectTeacher';
                } else {
                    $errorMessage = 'No Class assigned for you yet!';
                }
            }
        } else {
            $errorMessage = 'Only teachers have access to his page!';
        }

        return array($errorMessage, $combinations, $type);
    }

    public function buildTreeQuery($rcpathname, $parentId) {
        $name = OrganizationalStructureValues::findfirst('id = ' . $parentId);
        $iscycle = OrganizationalStructureMaster::findFirstById($name->org_master_id);
//        echo $iscycle->id.$iscycle->promotion_status.$name->id.'<br>';
        if (!in_array($iscycle->module, array("structural"))) {
            $rcpathname[$name->id] = $name->id;
            $rcpathname = ControllerBase::buildTreeQuery($rcpathname, $name->parent_id);
        }
        return $rcpathname;
    }

    public function buildTreeQueryFullSearch($rcpathname, $parentId) {
        $name = OrganizationalStructureValues::findfirst('id = ' . $parentId);
        $iscycle = OrganizationalStructureMaster::findFirstById($name->org_master_id);
        $rcpathname[$name->id] = $name->id;
        if ($name->parent_id > 0) {
            $rcpathname = ControllerBase::buildTreeQueryFullSearch($rcpathname, $name->parent_id);
        }
        return $rcpathname;
    }

    public function buildFeeQuery($aggregated_nodes_id) {
        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
        //    echo '<pre>'.$aggregated_nodes_id.'<br>';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeQuery($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = '' . implode(',', $inp) . '';
            //        echo $searchword.''.'<br>';
            if (count($name_param) < 1) {
                $name_param[] = '' . implode(',', $inp) . '';
            }
            $i = 0;
            foreach ($name_param as $k => $v) {
//                        echo preg_match("/\b$searchword\b/i", $v);
                if (preg_match("/\b$v\b/i", $searchword)) {
                    $name_param[$k] = $searchword;
                    $i = 1;
                    break;
                }
            }

            if ($i == 0) {
                $name_param[] = '' . implode(',', $inp) . '';
            }
        }
        $generateloop = array();
//        $name_param[] = '108,112,137';
        foreach ($name_param as $wholenode) {
            $getorgmas = array();
            $indvval = explode(',', $wholenode);
            foreach ($indvval as $node) {
                $getorgmas[] = OrganizationalStructureValues::findFirstById($node)->org_master_id;
            }

            $generateloop[implode('-', $getorgmas)][] = $wholenode;
        }
//        $name_param = array( "3,6","3,8");
//        return '"' . implode('" , "', $name_param) . '"';
//        return preg_filter('/^([\d,])*/', 'aggregate_key LIKE  "$0%"', $name_param);
        $nquery = $rquery = array();
        foreach ($generateloop as $andvalue) {
            $inquery = array();
            foreach ($andvalue as $orvalue) {
                $narr = preg_filter('/^([\d,])*/', '(find_in_set("$0", REPLACE(node_id , "-", "," ))>0)', explode(',', $orvalue));
                $inquery[] = '(' . implode(' and ', $narr) . ')';
            }
            $rquery[] = '(' . implode(' or ', $inquery) . ')';
        }
        $nquery[] = '(' . implode(' and ', $rquery) . ')';
//        print_r($nquery);exit;

        return $nquery;
    }

    public function buildStudentQuery($aggregated_nodes_id) {
        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
        //    echo '<pre>'.$aggregated_nodes_id.'<br>';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeQuery($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = '' . implode(',', $inp) . '';
            //        echo $searchword.''.'<br>';
            if (count($name_param) < 1) {
                $name_param[] = '' . implode(',', $inp) . '';
            }
            $i = 0;
            foreach ($name_param as $k => $v) {
//                        echo preg_match("/\b$searchword\b/i", $v);
                if (preg_match("/\b$v\b/i", $searchword)) {
                    $name_param[$k] = $searchword;
                    $i = 1;
                    break;
                }
            }

            if ($i == 0) {
                $name_param[] = '' . implode(',', $inp) . '';
            }
        }
        $generateloop = array();
//        $name_param[] = '108,112,137';
        foreach ($name_param as $wholenode) {
            $getorgmas = array();
            $indvval = explode(',', $wholenode);
            foreach ($indvval as $node) {
                $getorgmas[] = OrganizationalStructureValues::findFirstById($node)->org_master_id;
            }

            $generateloop[implode('-', $getorgmas)][] = $wholenode;
        }
//        $name_param = array( "3,6","3,8");
//        return '"' . implode('" , "', $name_param) . '"';
//        return preg_filter('/^([\d,])*/', 'aggregate_key LIKE  "$0%"', $name_param);
        $nquery = $rquery = array();
        foreach ($generateloop as $andvalue) {
            $inquery = array();
            foreach ($andvalue as $orvalue) {
                $narr = preg_filter('/^([\d,])*/', '(find_in_set("$0", aggregate_key)>0)', explode(',', $orvalue));
                $inquery[] = '(' . implode(' and ', $narr) . ')';
            }
            $rquery[] = '(' . implode(' or ', $inquery) . ')';
        }
        $nquery[] = '(' . implode(' and ', $rquery) . ')';
//        print_r($nquery);exit;

        return $nquery;
    }

    public function aggregateCombi($aggregated_nodes_id) {
        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        foreach ($Totarr as $rcvalue) {
            $node = OrganizationalStructureValues::findFirstById($rcvalue);
            $getorgmas = $node->org_master_id;
            $name_param[$node->parent_id][$getorgmas][] = $rcvalue;
        }
        return $name_param;
    }

    public function buildSubjectsQuery($aggregated_nodes_id) {
        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
        //    echo '<pre>'.$aggregated_nodes_id.'<br>';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeQuery($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = '' . implode(',', $inp) . '';
            //        echo $searchword.''.'<br>';
            if (count($name_param) < 1) {
                $name_param[] = '' . implode(',', $inp) . '';
            }
            $i = 0;
            foreach ($name_param as $k => $v) {
//                        echo preg_match("/\b$searchword\b/i", $v);
                if (preg_match("/\b$v\b/i", $searchword)) {
                    $name_param[$k] = $searchword;
                    $i = 1;
                    break;
                }
            }

            if ($i == 0) {
                $name_param[] = '' . implode(',', $inp) . '';
            }
        }
        $generateloop = array();
//        $name_param[] = '108,112,137';
        foreach ($name_param as $wholenode) {
            $getorgmas = array();
            $indvval = explode(',', $wholenode);
            foreach ($indvval as $node) {
                $getorgmas[] = OrganizationalStructureValues::findFirstById($node)->org_master_id;
            }

            $generateloop[implode('-', $getorgmas)][] = $wholenode;
        }

        return $generateloop;
    }

    public function getGrpSubjMasPossiblities($aggregates, $type = 'subject') {

        $get_Query_For_Subjct = $aggregiven = ControllerBase::getCommonIdsForKeys(implode(',', $aggregates));

        $nquery = $clsgrpids = array();
        foreach ($get_Query_For_Subjct as $nvalue) {
            $narr = preg_filter('/^([\d,])*/', '(find_in_set("$0",  REPLACE(aggregated_nodes_id ,  "-",  "," ))>0)'
                    , explode('-', $nvalue));
            $nquery[] = '(' . implode(' or ', $narr) . ') ';
        }
        $qgrp[] = '(' . implode(' or ', $nquery) . ') ';
        $grpqry = count($qgrp) > 0 ? implode(' and ', $qgrp) : '';
        $subdivval = ClassroomMaster::find($grpqry);
        $grpsubids = $subjIds = $id_val = array();
        if (count($subdivval) > 0) {
            foreach ($subdivval as $nodes) {

                $pattern = '/[-,]/';
                $Totarr = (preg_split($pattern, implode(',', $aggregates)));
                $generateloop = ControllerBase::buildSubjectsQuery($nodes->aggregated_nodes_id);
                $nquerys = $rquery = array();
                foreach ($generateloop as $andvalue) {
                    $inquery = array();
                    foreach ($andvalue as $orvalue) {

                        $narr = preg_filter('/^([\d,])*/', '(in_array("$0", $Totarr))', explode(',', $orvalue));
                        $inquery[] = '(' . implode(' && ', $narr) . ')';
                    }
                    $rquery[] = '(' . implode(' || ', $inquery) . ')';
                }
                $nquerys = '(' . implode(' && ', $rquery) . ')';

                $result = eval(sprintf('return (%s);', $nquerys));
                if ($result) {
                    $grpsubids[] = $nodes->id;
                }
            }
        }

        $finids = $subjids = array();
        if (count($grpsubids) > 0) {

            $getgrpqy = 'classroom_master_id IN (' . implode(',', array_unique($grpsubids)) . ')';
            $grpsubjids = (count($grpsubids) > 0 && $type == 'subject') ? GroupSubjectsTeachers::find($getgrpqy) : GroupClassTeachers::find($getgrpqy);
            if (count($grpsubjids) > 0):
                foreach ($grpsubjids as $pusid) {
                    $finids[] = $pusid->id;
                }
                $subjids = array_unique($finids);
            endif;
        }
        return count($subjids) > 0 ? $subjids : array();
    }

    public function getNameForKeysFullSearch($aggregated_nodes_id) {

        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
//                echo '<pre>';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeNameFullSearch($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = implode('>>', $inp);
//                    echo $searchword.'asdasdasd';
            if (count($name_param) < 1) {
                $name_param[] = implode('>>', $inp);
            }
            $i = 0;
            foreach ($name_param as $k => $v) {
//                        echo preg_match("/\b$searchword\b/i", $v);
                if (preg_match("/\b$v\b/i", $searchword)) {
                    $name_param[$k] = $searchword;
                    $i = 1;
                    break;
                }
            }
            if ($i == 0) {
                $name_param[] = implode('>>', $inp);
            }
        }

        return $name_param;
    }

    public function getNameForKeys($aggregated_nodes_id) {

        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
//                echo '<pre>';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeName($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = implode('>>', $inp);
//                    echo $searchword.'asdasdasd';
            if (count($name_param) < 1) {
                $name_param[] = implode('>>', $inp);
            }
            $i = 0;
            foreach ($name_param as $k => $v) {
//                        echo preg_match("/\b$searchword\b/i", $v);
                if (preg_match("/\b$v\b/i", $searchword)) {
                    $name_param[$k] = $searchword;
                    $i = 1;
                    break;
                }
            }
            if ($i == 0) {
                $name_param[] = implode('>>', $inp);
            }
        }
//        exit;
        return $name_param;
    }

    public function getMandNameForKeys($aggregated_nodes_id) {

        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
//                echo '<pre>';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeMandName($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = implode('>>', $inp);
//                    echo $searchword.'asdasdasd';
            if (count($name_param) < 1) {
                $name_param[] = implode('>>', $inp);
            }
            $i = 0;
            foreach ($name_param as $k => $v) {
//                        echo preg_match("/\b$searchword\b/i", $v);
                if (preg_match("/\b$v\b/i", $searchword)) {
                    $name_param[$k] = $searchword;
                    $i = 1;
                    break;
                }
            }
            if ($i == 0) {
                $name_param[] = implode('>>', $inp);
            }
        }
//        exit;
        return $name_param;
    }

    public function getCommonIdsForKeysNEw($aggregated_nodes_id) {

        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
//                echo '<pre>';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeIds($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = implode('-', $inp);
            $name_param[] = implode('-', $inp);
        }
        return $name_param;
    }

    public function getCommonIdsForKeys($aggregated_nodes_id) {

        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
//                echo '<pre>';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeIds($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = implode('-', $inp);
//                    echo $searchword.'asdasdasd';
            if (count($name_param) < 1) {
                $name_param[] = implode('-', $inp);
            }
            $i = 0;
            foreach ($name_param as $k => $v) {
                if (preg_match("/\b$v\b/i", $searchword)) {
                    $name_param[$k] = $searchword;
                    $i = 1;
                    break;
                }
            }
            if ($i == 0) {
                $name_param[] = implode('-', $inp);
            }
        }
        return $name_param;
    }

    public function getCommonIdsForKeysFullSearch($aggregated_nodes_id) {

        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
//                echo '<pre>';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeQueryFullSearch($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = implode('-', $inp);
//                    echo $searchword.'asdasdasd';
            if (count($name_param) < 1) {
                $name_param[] = implode('-', $inp);
            }
            $i = 0;
            foreach ($name_param as $k => $v) {
                if (preg_match("/\b$v\b/i", $searchword)) {
                    $name_param[$k] = $searchword;
                    $i = 1;
                    break;
                }
            }
            if ($i == 0) {
                $name_param[] = implode('-', $inp);
            }
        }
        return $name_param;
    }

    public function buildTreeIds($rcpathname, $parentId) {
        $name = OrganizationalStructureValues::findfirst('id = ' . $parentId);
        $iscycle = OrganizationalStructureMaster::findFirstById($name->org_master_id);
        if (!in_array($iscycle->module, array("structural"))) {
            $rcpathname[] = $name->id;
            $rcpathname = ControllerBase::buildTreeIds($rcpathname, $name->parent_id);
        }
        return $rcpathname;
    }

    public function buildTreeName($rcpathname, $parentId) {
        if ($parentId) {
            $name = OrganizationalStructureValues::findfirst('id = ' . $parentId);
            $iscycle = OrganizationalStructureMaster::findFirstById($name->org_master_id);
            if (!in_array($iscycle->module, array("structural", "StaffCycleNode", "Ledger"))) {
                $rcpathname[] = $name->name;
                $rcpathname = ControllerBase::buildTreeName($rcpathname, $name->parent_id);
            }
        }
        return $rcpathname;
    }

    public function buildTreeMandName($rcpathname, $parentId) {
        if ($parentId) {
            $name = OrganizationalStructureValues::findfirst('id = ' . $parentId);
            $iscycle = OrganizationalStructureMaster::findFirstById($name->org_master_id);

            if (!in_array($iscycle->module, array("structural", "StaffCycleNode", "Ledger")) && $iscycle->mandatory_for_admission == "1") {
//            if ($iscycle->promotion_status != "above cycle node" && $iscycle->mandatory_for_admission == "1") {
                $rcpathname[] = $name->name;
                $rcpathname = ControllerBase::buildTreeName($rcpathname, $name->parent_id);
            }
        }
        return $rcpathname;
    }

    public function buildTreeNameFullSearch($rcpathname, $parentId) {

        $name = OrganizationalStructureValues::findFirst('id = ' . $parentId);
        if ($name->id > 0):
            $rcpathname[] = $name->name;
            $rcpathname = ControllerBase::buildTreeNameFullSearch($rcpathname, $name->parent_id);
        endif;
        return $rcpathname;
    }

    public function buildExamQuery($aggregated_nodes_id) {
        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
//                echo '<pre>';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeQuery($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = '' . implode('-', $inp) . '';
//                    echo $searchword.'asdasdasd';
            if (count($name_param) < 1) {
                $name_param[] = '' . implode('-', $inp) . '';
            }
            $i = 0;
            foreach ($name_param as $k => $v) {
//                        echo preg_match("/\b$searchword\b/i", $v);
                if (preg_match("/\b$v\b/i", $searchword)) {
                    $name_param[$k] = $searchword;
                    $i = 1;
                    break;
                }
            }


            if ($i == 0) {
                $name_param[] = '' . implode('-', $inp) . '';
            }
        }
//        $name_param = array( "3,6","3,8");
//        return '"' . implode('" , "', $name_param) . '"';
//        return preg_filter('/^([\d-])*/', 'node_id LIKE  "$0%"', $name_param);
        return preg_filter('/^([\d-])*/', 'LOCATE(node_id,"$0")', $name_param);
    }

    public function buildSubjectQuery($aggregated_nodes_id) {
//        print_r($aggregated_nodes_id);exit;    
        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
//                echo '<pre>';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeQuery($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = '' . implode('-', $inp) . '';
//                    echo $searchword.'asdasdasd';
            if (count($name_param) < 1) {
                $name_param[] = '' . implode('-', $inp) . '';
            }
            $i = 0;
            foreach ($name_param as $k => $v) {
//                        echo preg_match("/\b$searchword\b/i", $v);
                if (preg_match("/\b$v\b/i", $searchword)) {
                    $name_param[$k] = $searchword;
                    $i = 1;
                    break;
                }
            }

            if ($i == 0) {
                $name_param[] = '' . implode('-', $inp) . '';
            }
        }
//        print_r($name_param);exit;
//        $name_param = array( "3,6","3,8");
//        return $name_param;
        return preg_filter('/^([\d-])*/', 'LOCATE("$0",aggregated_nodes_id)', $name_param);
    }

    public function getAllSubjectAndSubModules($subaggregateids) {
        $subjectsmas_pids_query = "SELECT GROUP_CONCAT(c.id) AS pids FROM OrganizationalStructureMaster  c
                                WHERE c.module = 'Subject' AND c.is_subordinate = 1";
        $subjectsmas_pids = $this->modelsManager->executeQuery($subjectsmas_pids_query);
        if (isset($subaggregateids) && count($subaggregateids)):
            $subjagg = ControllerBase::_getAllChildren($subaggregateids[count($subaggregateids) - 1], $subjectsmas_pids[0]->pids);
        endif;
        return $subjagg;
    }

    public function getAlSubjChildNodes($aggregateids) {
        $subjects_pids_query = "SELECT GROUP_CONCAT(c.parent_id) AS pids FROM OrganizationalStructureMaster c
inner join OrganizationalStructureMaster p on p.id = c.parent_id
WHERE c.module = 'Subject' AND c.is_subordinate = 1
 and p.is_subordinate = 0";
        $subjects_pids = $this->modelsManager->executeQuery($subjects_pids_query);
        $subjpids = $availMasids = array();
        $subjmasids = explode(',', $subjects_pids[0]->pids);
        foreach ($aggregateids as $givnagg):
            $gvnnode = OrganizationalStructureValues::findFirstById($givnagg);
            if (in_array($gvnnode->org_master_id, $subjmasids)) {
                $subjpids[] = $givnagg;
                $availMasids[] = $gvnnode->org_master_id;
            }
        endforeach;
        $remainmasids = array_diff($subjmasids, $availMasids);
        if (count($remainmasids) > 0) {
            if (count($aggregateids) > 1)
                array_shift($aggregateids);
            $subidsall = ControllerBase::_getAllChildren(implode(',', $aggregateids), implode(',', $remainmasids));
            $subjpids = $subidsall;
        }
        return $subjpids;
    }

    public function _getAllChildren($aids, $subjects_mas_ids, $subjids = array()) {

        $subjids[] = $aids;
        if (count($aids) > 0) {
            $nxt = OrganizationalStructureValues::find(array('columns' => 'group_concat(id) as ids',
                        'parent_id  IN ( ' . $aids . ') and org_master_id  IN (' . $subjects_mas_ids . ')'
            ));
        }
        if ($nxt[0]->ids != '') {
            $idsnxt = $nxt[0]->ids;
            $subjids = ControllerBase::_getAllChildren($idsnxt, $subjects_mas_ids, $subjids);
        }

        return $subjids;
    }

    public function getGrpSubjMasPossiblitiesold($aggregates, $type = 'subject') {
        $get_Query_For_Subjct = $aggregiven = ControllerBase::getCommonIdsForKeysNEw(implode(',', $aggregates));
        if (count($get_Query_For_Subjct) > 1) {
            array_shift($get_Query_For_Subjct);
            array_shift($aggregiven);
        }
        $nquery = $clsgrpids = array();
        foreach ($get_Query_For_Subjct as $nvalue) {
            $narr = preg_filter('/^([\d,])*/', '(find_in_set("$0",  REPLACE(aggregated_nodes_id ,  "-",  "," ))>0)'
                    , explode('-', $nvalue));
            $nquery[] = '(' . implode(' or ', $narr) . ') ';
        }
        $qgrp[] = '(' . implode(' or ', $nquery) . ') ';
        $grpqry = count($qgrp) > 0 ? implode(' and ', $qgrp) : '';
        $subdivval = ClassroomMaster::find($grpqry);
        $grpsubids = $subjIds = $id_val = array();
        if (count($subdivval) > 0) {
            foreach ($subdivval as $nodes) {
                $v = 0;

                $res = ControllerBase::getCommonIdsForKeysNEw($nodes->aggregated_nodes_id);
                if (count($res) > 1)
                    array_shift($res);
                $arrayfdiff = array_diff($aggregiven, $res);
                $arrayfdiff2 = array_diff($res, $aggregiven);
                if (count($arrayfdiff) < count($aggregiven) || count($arrayfdiff2) < count($res)) {
                    $grpsubids[] = $nodes->id;
                    $v = 1;
                }
                if ($v == 0) {
                    foreach ($res as $set) {

                        $inerarrdiff = array_diff($aggregates, explode('-', $set));
                        $inerarrdiff2 = array_diff(explode('-', $set), $aggregates);
                        if (count($inerarrdiff) == 0 || count($inerarrdiff2) == 0) {
                            $grpsubids[] = $nodes->id;
                        }
                    }
                }
//                }
            }
        }
        $finids = $subjids = array();
        $getgrpqy = (count($grpsubids) > 0 ) ? 'classroom_master_id IN (' . implode(',', array_unique($grpsubids)) . ')' : '';
        $grpsubjids = $getgrpqy ?
                ((count($grpsubids) > 0 && $type == 'subject') ? GroupSubjectsTeachers::find($getgrpqy) : GroupClassTeachers::find($getgrpqy)) : '';
        //       $grpsubjids = count($grpsubids) > 0 ? GroupSubjectsTeachers::find('classroom_master_id IN (' . implode(',', $grpsubids) . ')') : '';
        if (count($grpsubjids) > 0):
            foreach ($grpsubjids as $pusid) {
                $finids[] = $pusid->id;
            }
            $subjids = array_unique($finids);
        endif;
        return $subjids;
    }

    public function getSujMasByCombi($aggregates, $type = 'subject') {
        $get_Query_For_Subjct = $aggregiven = ControllerBase::getCommonIdsForKeysNEw(implode(',', $aggregates));
//        if (count($get_Query_For_Subjct) > 1) {
//            array_shift($get_Query_For_Subjct);
//            array_shift($aggregiven);
//        }        $exactcnt = count($aggregiven) - 1;

        $arracheck = ControllerBase::aggregateCombi(implode(',', $aggregates));
//        print_r($arracheck);
        $nquery = $clsgrpids = array();
        foreach ($get_Query_For_Subjct as $nvalue) {
            $narr = preg_filter('/^([\d,])*/', '(find_in_set("$0",  REPLACE(aggregated_nodes_id ,  "-",  "," ))>0)'
                    , explode('-', $nvalue));
            $nquery[] = '(' . implode(' or ', $narr) . ') ';
        }
        $qgrp[] = '(' . implode(' or ', $nquery) . ') ';
        $grpqry = count($qgrp) > 0 ? implode(' and ', $qgrp) : '';
        $subdivval = ClassroomMaster::find($grpqry);
        $grpsubids = $subjIds = $id_val = array();
        if (count($subdivval) > 0) {
            foreach ($subdivval as $nodes) {
                $v = 0;
                $falsecombi = 0;
                $res = ControllerBase::aggregateCombi($nodes->aggregated_nodes_id);
                foreach ($arracheck as $key => $combi) {
                    if (isset($res[$key])) {
                        foreach ($combi as $seckey => $seccombi) {
//                            echo $key.'-'.$seckey;
                            if (isset($res[$key][$seckey])) {
//                                  print_r($res[$key][$seckey]);  print_r($seccombi);
                                $arrayfdiff2 = array_diff($res[$key][$seckey], $seccombi);
//                                print_r((count($arrayfdiff2) < count($res[$key][$seckey])));
                                if ((count($arrayfdiff2) < count($res[$key][$seckey]))) {
                                    //do nothing
                                } else {
                                    $falsecombi = 1;
                                }
                            }
                        }
                    }
                }
                if ($falsecombi != 1) {
                    $grpsubids[] = $nodes->id;
                }
            }
        }
        $finids = $subjids = array();
        $getgrpqy = (count($grpsubids) > 0 ) ? 'classroom_master_id IN (' . implode(',', array_unique($grpsubids)) . ')' : '';
        $grpsubjids = $getgrpqy ?
                ((count($grpsubids) > 0 && $type == 'subject') ? GroupSubjectsTeachers::find($getgrpqy) : GroupClassTeachers::find($getgrpqy)) : '';
        if (count($grpsubjids) > 0):
            foreach ($grpsubjids as $pusid) {
                $finids[] = $pusid->id;
            }
            $subjids = array_unique($finids);
        endif;
        return $subjids;
    }

    public function getAllPossibleSubjects($subjectsids) {
        $subjectsid = GroupSubjectsTeachers::find('id IN (' . implode(',', $subjectsids) . ')');
        foreach ($subjectsid as $svalue) {
            $subjrr[] = $svalue->subject_id;
        }
        $subjects = OrganizationalStructureValues::find("id IN (" . implode(',', $subjrr) . ")");
        return $subjects;
    }

    public function getAllPossibleSubjectsold($aggregates) {
        $currentQury[] = '( parent_id IN (' . implode(',', $aggregates) . '))';
        $subjects_mas = OrganizationalStructureMaster::find(array(
                    'columns' => 'GROUP_CONCAT(id) as ids',
                    ' module = "Subject" and is_subordinate = 1 '
        ));
        $subjects = OrganizationalStructureValues::find(
                        array(
                            'org_master_id IN (' . $subjects_mas[0]->ids . ') '
                            . ' and (' . implode(' or ', $currentQury) . ')'
                        )
        );
        return $subjects;
    }

    public function _getBalnceForDate($start_date, $aid) {
        $ledger = OrganizationalStructureValues::findFirstById($aid);
        $prev_month = (date('t-m-Y', strtotime(date('d-m-Y', $start_date) . " -1 month")) . ' 00:00:00');
        $datetime1 = new DateTime($prev_month);
        $datetime2 = new DateTime(date('d-m-Y', $start_date) . '  00:00:00');
        $interval = date_diff($datetime1, $datetime2);
        $intindays = $interval->format('%d');
        $initialbal_credit = 0;
        $initialbal_debit = 0;
        $initialbal = InitialBalance::findFirstByLid($aid);
        if ($initialbal):
            $initialbal_credit = $initialbal->opening_credit;
            $initialbal_debit = $initialbal->opening_debit;
        endif;
        if (abs($intindays) == 0) {
            $prev_month = (date('d-m-Y', $start_date));
        } else {
            $datetime11 = strtotime(date('01-m-Y', $start_date) . ' 00:00:00');
            $datetime21 = strtotime(date('d-m-Y', $start_date) . ' 00:00:00');
            $dailySum = DailyLedgerSum::findFirst(array(
                        'columns' => 'sum(creditsum) as creditsum, sum(debitsum) as debitsum',
                        "UNIX_TIMESTAMP( STR_TO_DATE( date,  '%d-%m-%Y' ) ) 
BETWEEN  '" . $datetime11 . "'
AND  '" . $datetime21 . "'  and lid = " . $ledger->id
            ));
        }
        $fetMontlyledSum = "Select sum(creditsum) as creditsum, sum(debitsum) as debitsum  from MonthlyLedgerSum where
 LAST_DAY( STR_TO_DATE( CONCAT(  '01-',  monthyear ) ,  '%d-%m-%Y' ) ) <= STR_TO_DATE(  '$prev_month',  '%d-%m-%Y' )
 and lid = " . $ledger->id;

        $montsum = $this->modelsManager->executeQuery($fetMontlyledSum);
        $creditsum = ($montsum && $montsum[0]->creditsum > 0 ? $montsum[0]->creditsum : 0) + ($dailySum ? $dailySum->creditsum : 0);
        $debitsum = ($montsum && $montsum[0]->debitsum > 0 ? $montsum[0]->debitsum : 0) + ($dailySum ? $dailySum->debitsum : 0);
        $ledgertype = $creditsum > $debitsum ? 'Cr' : 'Dr';
        $openingbalance = abs(($creditsum + $initialbal_credit) - ($debitsum + $initialbal_debit));
        return array($openingbalance, $ledgertype);
    }

    public function _getClsnBalnceForFromTo($start_date, $end_date, $aid) {
        $ledger = OrganizationalStructureValues::findFirstById($aid);
        $datetime1 = new DateTime(date('d-m-Y', $start_date) . ' 00:00:00');
        $datetime2 = new DateTime(date('d-m-Y', $end_date) . ' 00:00:00');
        $interval = date_diff($datetime1, $datetime2);
        $creditsum = $debitsum = 0;
        $intinmonth = $interval->format('%m');
        if (abs($intinmonth) > 1) {
            $next_month = strtotime(date('Y-m-01', strtotime(date('Y-m-d', $start_date) . " +1 month")) . ' 00:00:00');
            $prev_month = strtotime(date('Y-m-t', strtotime(date('Y-m-d', $end_date) . " -1 month")) . ' 23:59:59');

            $fetMontlyledSum = "Select sum(creditsum) as creditsum, sum(debitsum) as debitsum  from MonthlyLedgerSum where
 UNIX_TIMESTAMP( STR_TO_DATE( CONCAT(  '01-',  monthyear ) ,  '%d-%m-%Y' ) ) BETWEEN
  '$next_month' AND  '$prev_month'  and lid = " . $ledger->id;

            $montsum = $this->modelsManager->executeQuery($fetMontlyledSum);
            $datetime11 = new DateTime(date('d-m-Y', $start_date) . ' 00:00:00');
            $datetime22 = new DateTime(date('t-m-Y', $start_date) . ' 23:59:59');
            $interval1 = date_diff($datetime11, $datetime22);
            if (abs($interval1) > 0) {
                $datetime112 = strtotime(date('d-m-Y', $start_date) . ' 00:00:00');
                $datetime221 = strtotime(date('t-m-Y', $start_date) . '  00:00:00');
                $dailySum1 = DailyLedgerSum::findFirst(array(
                            'columns' => 'sum(creditsum) as creditsum, sum(debitsum) as debitsum',
                            "UNIX_TIMESTAMP( STR_TO_DATE( date,  '%d-%m-%Y' ) ) 
                                BETWEEN  '" . $datetime112 . "'
                                AND  '" . $datetime221 . "'  and lid = " . $ledger->id
                ));
            }
            $datetime111 = new DateTime(date('01-m-Y', $end_date) . ' 00:00:00');
            $datetime222 = new DateTime(date('d-m-Y', $end_date) . ' 23:59:59');
            $interval2 = date_diff($datetime111, $datetime222);
            if (abs($interval2) > 0) {
                $datetime121 = strtotime(date('01-m-Y', $end_date) . ' 00:00:00');
                $datetime211 = strtotime(date('d-m-Y', $end_date) . ' 23:59:59');
                $dailySum2 = DailyLedgerSum::findFirst(array(
                            'columns' => 'sum(creditsum) as creditsum, sum(debitsum) as debitsum',
                            "UNIX_TIMESTAMP( STR_TO_DATE( date,  '%d-%m-%Y' ) ) 
                                BETWEEN  '" . $datetime121 . "'
                                AND  '" . $datetime211 . "'  and lid = " . $ledger->id
                ));
            }
            if ($montsum) {
                if ($montsum[0]->creditsum > 0)
                    $creditsum+=$montsum[0]->creditsum;
                if ($montsum[0]->debitsum > 0)
                    $debitsum+=$montsum[0]->debitsum;
            }if ($dailySum1) {
                if ($dailySum1->creditsum > 0)
                    $creditsum+=$dailySum1->creditsum;
                if ($dailySum1->debitsum > 0)
                    $debitsum+=$dailySum1->debitsum;
            }if ($dailySum2) {
                if ($dailySum2->creditsum > 0)
                    $creditsum+=$dailySum2->creditsum;
                if ($dailySum2->debitsum > 0)
                    $debitsum+=$dailySum2->debitsum;
            }
        } else {

            $datetimef = strtotime(date('d-m-Y', $start_date) . ' 00:00:00');
            $datetimet = strtotime(date('d-m-Y', $end_date) . '  23:59:59');
            $dailySum = DailyLedgerSum::findFirst(array(
                        'columns' => 'sum(creditsum) as creditsum, sum(debitsum) as debitsum',
                        "UNIX_TIMESTAMP( STR_TO_DATE(date,  '%d-%m-%Y' ) ) 
                                BETWEEN  '" . $datetimef . "'
                                AND  '" . $datetimet . "'  and lid = " . $ledger->id
            ));
            if ($dailySum) {
                if ($dailySum->creditsum > 0)
                    $creditsum+=$dailySum->creditsum;
                if ($dailySum->debitsum > 0)
                    $debitsum+=$dailySum->debitsum;
            }
        }
        return array($creditsum, $debitsum);
    }

    public function _getOpeningBalnceFromTo($start_date, $end_date, $aid) {
        $ledger = OrganizationalStructureValues::findFirstById($aid);

        $datetime1 = new DateTime(date('d-m-Y', $start_date) . ' 00:00:00');
        $datetime2 = new DateTime(date('d-m-Y', $end_date) . ' 00:00:00');
        $interval = date_diff($datetime1, $datetime2);
        $creditsum = $debitsum = 0;
        $initialbal_credit = 0;
        $initialbal_debit = 0;
        $initialbal = InitialBalance::findFirstByLid($aid);
        if ($initialbal):
            $initialbal_credit = $initialbal->opening_credit;
            $initialbal_debit = $initialbal->opening_debit;
        endif;
        $intinmonth = $interval->format('%m');
        if (abs($intinmonth) > 1) {
            $next_month = strtotime(date('Y-m-01', strtotime(date('Y-m-d', $start_date) . " +1 month")) . ' 00:00:00');
            $prev_month = strtotime(date('Y-m-t', strtotime(date('Y-m-d', $end_date) . " -1 month")) . ' 23:59:59');

            $fetMontlyledSum = "Select sum(creditsum) as creditsum, sum(debitsum) as debitsum  from MonthlyLedgerSum where
 UNIX_TIMESTAMP( STR_TO_DATE( CONCAT(  '01-',  monthyear ) ,  '%d-%m-%Y' ) ) BETWEEN
  '$next_month' AND  '$prev_month'  and lid = " . $ledger->id;

            $montsum = $this->modelsManager->executeQuery($fetMontlyledSum);
            $datetime11 = new DateTime(date('d-m-Y', $start_date) . ' 00:00:00');
            $datetime22 = new DateTime(date('t-m-Y', $start_date) . ' 23:59:59');
            $interval1 = date_diff($datetime11, $datetime22);
            if (abs($interval1) > 0) {
                $datetime112 = strtotime(date('d-m-Y', $start_date) . ' 00:00:00');
                $datetime221 = strtotime(date('t-m-Y', $start_date) . '  00:00:00');
                $dailySum1 = DailyLedgerSum::findFirst(array(
                            'columns' => 'sum(creditsum) as creditsum, sum(debitsum) as debitsum',
                            "UNIX_TIMESTAMP( STR_TO_DATE( date,  '%d-%m-%Y' ) ) 
                                BETWEEN  '" . $datetime112 . "'
                                AND  '" . $datetime221 . "'  and lid = " . $ledger->id
                ));
            }
            $datetime111 = new DateTime(date('01-m-Y', $end_date) . ' 00:00:00');
            $datetime222 = new DateTime(date('d-m-Y', $end_date) . ' 23:59:59');
            $interval2 = date_diff($datetime111, $datetime222);
            if (abs($interval2) > 0) {
                $datetime121 = strtotime(date('01-m-Y', $end_date) . ' 00:00:00');
                $datetime211 = strtotime(date('d-m-Y', $end_date) . ' 23:59:59');
                $dailySum2 = DailyLedgerSum::findFirst(array(
                            'columns' => 'sum(creditsum) as creditsum, sum(debitsum) as debitsum',
                            "UNIX_TIMESTAMP( STR_TO_DATE( date,  '%d-%m-%Y' ) ) 
                                BETWEEN  '" . $datetime121 . "'
                                AND  '" . $datetime211 . "'  and lid = " . $ledger->id
                ));
            }
            if ($montsum) {
                if ($montsum[0]->creditsum > 0)
                    $creditsum+=$montsum[0]->creditsum;
                if ($montsum[0]->debitsum > 0)
                    $debitsum+=$montsum[0]->debitsum;
            }if ($dailySum1) {
                if ($dailySum1->creditsum > 0)
                    $creditsum+=$dailySum1->creditsum;
                if ($dailySum1->debitsum > 0)
                    $debitsum+=$dailySum1->debitsum;
            }if ($dailySum2) {
                if ($dailySum2->creditsum > 0)
                    $creditsum+=$dailySum2->creditsum;
                if ($dailySum2->debitsum > 0)
                    $debitsum+=$dailySum2->debitsum;
            }
        } else {

            $datetimef = strtotime(date('d-m-Y', $start_date) . ' 00:00:00');
            $datetimet = strtotime(date('d-m-Y', $end_date) . '  23:59:59');
            $dailySum = DailyLedgerSum::findFirst(array(
                        'columns' => 'sum(creditsum) as creditsum, sum(debitsum) as debitsum',
                        "UNIX_TIMESTAMP( STR_TO_DATE(date,  '%d-%m-%Y' ) ) 
                                BETWEEN  '" . $datetimef . "'
                                AND  '" . $datetimet . "'  and lid = " . $ledger->id
            ));
            if ($dailySum) {
                if ($dailySum->creditsum > 0)
                    $creditsum+=$dailySum->creditsum;
                if ($dailySum->debitsum > 0)
                    $debitsum+=$dailySum->debitsum;
            }
        }
        $openingbalance = 0;
        $openingbalance = abs(($creditsum + $initialbal_credit) - ($debitsum + $initialbal_debit));

        return array($openingbalance);
    }

    public function _getLegerTransacTotal($start_date, $end_date, $aid) {
        if ($start_date == '') {
            // $qcnt = ' and (ldgr.date <= "' . $end_date . '") ';
            $qcnt = " and DATE_FORMAT(  FROM_UNIXTIME( ldgr.date ),  '%d-%m-%Y' ) <=  '" . date('d-m-Y', $end_date) . "'";
        } else {
            // $qcnt = " and (ldgr.date  between  '" . $start_date . "' and  '": . $end_date . "') ";
            $qcnt = " and  DATE_FORMAT(  FROM_UNIXTIME( ldgr.date ) ,  '%Y-%m-%d' ) 
BETWEEN STR_TO_DATE(  '" . date('d-m-Y', $start_date) . "',  '%d-%m-%Y' ) 
AND STR_TO_DATE(  '" . date('d-m-Y', $end_date) . "',  '%d-%m-%Y' ) ";
        }
        $trans_sql = 'SELECT  count(*) as cnt '
                . ' FROM  LedgerVoucher ldgr '
                . ' INNER JOIN Transaction trans ON ldgr.voucher_id=trans.voucherid'
                . '  WHERE (ldgr.debitledger = ' . $aid . ' OR ldgr.creditledger = ' . $aid . ') '
                . $qcnt
                . "  ORDER BY ldgr.date  ,  trans.transactionid ASC  ";
        $transactions = $this->modelsManager->executeQuery($trans_sql);
        return $transactions[0]->cnt;
    }

    public function _getLegerTransac($start_date, $end_date, $aid, $othparams = array()) {
        $orderphql = $row = array();
        if ($start_date == '') {
            $qcnt = ' and (ldgr.date <= "' . $end_date . '") ';
            //$qcnt = " and DATE_FORMAT(  FROM_UNIXTIME( ldgr.date ) ,  '%d-%m-%Y' ) <=  '".date('d-m-Y', $end_date) ."'";
        } else {
            $qcnt = " and (ldgr.date  between  '" . $start_date . "' and  '" . $end_date . "') ";
            //$qcnt =" and  DATE_FORMAT(  FROM_UNIXTIME( ldgr.date ) ,  '%Y-%m-%d' ) 
//BETWEEN STR_TO_DATE(  '".date('d-m-Y', $start_date) ."',  '%d-%m-%Y' ) 
//AND STR_TO_DATE(  '".date('d-m-Y', $end_date) ."',  '%d-%m-%Y' ) ";
        }
        $limit = $othparams['iDisplayLength'];
        $startl = $othparams['iDisplayStart'];
        $limitoff = '';
        if (count($othparams) > 0) {
            $limitoff = '  LIMIT ' . $startl . ' ,' . $limit;
        }
//print_r($qcnt);exit;
        $ledger = OrganizationalStructureValues::findFirstById($aid);
        $trans_sql = 'SELECT  '
                . ' trans.credit,trans.debit,trans.amount,'
                . ' trans.transactiontype,trans.voucherid,trans.created_by,'
                . "trans.created_date,trans.details,trans.transactionid,  ldgr.date "
                . ' FROM  LedgerVoucher ldgr '
                . ' INNER JOIN Transaction trans ON ldgr.voucher_id=trans.voucherid'
                . '  WHERE (trans.debit = ' . $aid . ' OR trans.credit = ' . $aid . ') '
                . $qcnt
                . "  ORDER BY   ldgr.date ,  trans.transactionid ASC  "
                . $limitoff;

//print_r($trans_sql);exit;
        $transactions = $this->modelsManager->executeQuery($trans_sql);
        $prevdate = $start_date ? $start_date : strtotime(date('Y-m-d', $transactions[0]->date));
        $prev_date = strtotime(date('Y-m-d', strtotime(date('Y-m-d', $prevdate) . " -1 day")) . " 23:59:59");
        $closingbalance = ControllerBase::_getBalnceForDate($end_date, $aid);
        $openingbalance = ControllerBase::_getBalnceForDate($prev_date, $aid);
        if (count($transactions) > 0) {

            $prevdatet = strtotime(date('Y-m-d', $transactions[0]->date));
            $prev_datet = strtotime(date('Y-m-d', strtotime(date('Y-m-d', $prevdate) . " -1 day")) . " 23:59:59");
            $closchangeable = $openingbalance; //ControllerBase::_getBalnceForDate($prev_datet, $aid);
//            print_r($closchangeable); exit;
//echo  $prev_datet;exit;
            $absolute_difference = 0;
            $Totalcamount = 0;
            $Totaldamount = 0;
            // $closingbalance = 0;
            foreach ($transactions as $transaction) {
                if ($transaction->credit == $ledger->id) {
                    if ($closchangeable[1] == 'Cr') {
                        $closchangeable[0] = $closchangeable[0] + $transaction->amount;
                    } else if (($closchangeable[1] == 'Dr')) {
                        $closchangeable[0] = $closchangeable[0] - $transaction->amount;
                    }
                    $Totalcamount = $Totalcamount + $transaction->amount;
                }
                if ($transaction->debit == $ledger->id) {
                    $Totaldamount = $Totaldamount + $transaction->amount;
                    if ($closchangeable[1] == 'Cr') {
                        $closchangeable[0] = $closchangeable[0] - $transaction->amount;
                    } else if (($closchangeable[1] == 'Dr')) {
                        $closchangeable[0] = $closchangeable[0] + $transaction->amount;
                    }
                }

                //$ledtyps = ($ledger->type == 'c') ? ' Cr' : ' Dr';
                //$selacctysp = ($ledger->type == 'c') ? ' Dr' : ' Cr';
                //$cltyp = ($closchangeable >= 0 ) ? $ledtyps : ($closchangeable < 0 ? $selacctysp : '');
                //$closingbalance = $closchangeable;
                $amnt = $transaction->amount;
                $c = ($ledger->id == $transaction->credit) ? $amnt : '-';
                $d = ($ledger->id == $transaction->debit) ? $amnt : '-';
                $head = ($ledger->id == $transaction->credit) ? OrganizationalStructureValues::findfirst('id = ' . $transaction->debit)->name : OrganizationalStructureValues::findfirst('id = ' . $transaction->credit)->name;
                $headtype = ($ledger->id == $transaction->credit) ? 'Dr' : 'Cr';
                $voucher = LedgerVoucher::findfirst('voucher_id = ' . $transaction->voucherid);
                $usrlginid = StaffInfo::findFirstById($transaction->created_by);
                $html = '';
                $html.='
            <tr>
                <td>' . $usrlginid->Staff_Name . '</td>
                <td>' . date('d-m-Y', $transaction->created_date) . '</td>
            </tr>';
                $hov_fn = '<button data-original-title="Transaction Details " style="font-size:12px;padding: 7px;"
                                           
                                                data-content="   <table class=\' table table-striped table-hover table-bordered data\' style=\'font-size:12px;\'>
        <thead>
            <tr>
                <th> Created By </th>
                <th> Created Date </th>
            </tr>
        </thead>
        <tbody>' . $html . '
        </tbody>
    </table>"
    data-placement="top" data-trigger="hover" class="btn btn-info popovers">
    <label  style="cursor: pointer;"> ' . $transaction->transactionid . '</label> </button> 
                                              
 </button>  ';
                $data[] = array(
                    'transid' => $transaction->transactionid,
                    'date' => date('d-m-Y', $transaction->date), //$transaction->date, //
                    'voucherid' => $voucher->vouchertype . ' : ' . $transaction->voucherid,
                    'head' => $head,
                    'headtype' => $headtype,
                    'debitamt' => $d,
                    'creditamt' => $c,
                    'debitmoney' => number_format($d, 2, '.', ','),
                    'creditmoney' => number_format($c, 2, '.', ','),
                    'clbnc' => number_format(abs($closchangeable[0]), 2, '.', '') . ' ' . $openingbalance[1],
                    'details' => $transaction->details,
                    'created_by' => $usrlginid->Staff_Name,
                    'created_date' => date('d-m-Y', $transaction->created_date),
                    'hovdet' => $hov_fn,
                    'comments' => $voucher->reason
                );
            } //end transaction 
            //$ledtyp = ($ledger->type == 'c') ? ' Cr' : ' Dr';
            //$selacctyp = ($ledger->type == 'c') ? ' Dr' : ' Cr';
            //$acctyp = ($openingbalance >= 0 ) ? $ledtyp : ($openingbalance < 0 ? $selacctyp : '');
            //$clacctyp = ($closingbalance >= 0 ) ? $ledtyp : ($closingbalance < 0 ? $selacctyp : '');
//            echo $aid.'<br>';
            $row = array('header' => array(
                    'ledger_name' => $ledger->name,
                    'totaldamount' => number_format($Totaldamount, 2, '.', ''),
                    'totalcamount' => number_format($Totalcamount, 2, '.', ''),
                    'openingbalance' => number_format(abs($openingbalance[0]), 2, '.', '') . ' ' . '(' . $openingbalance[1] . ')',
                    'closingbalance' => number_format(abs($closingbalance[0]), 2, '.', '') . ' ' . '(' . $closingbalance[1] . ')',
                    'cropeninbalnce' => (($openingbalance[1] == ' Cr') ? (number_format(abs($openingbalance[0]), 2, '.', '') ) : '-'),
                    'dropeninbalnce' => (($openingbalance[1] == ' Dr') ? (number_format(abs($openingbalance[0]), 2, '.', '') ) : '-'),
                    'headtype' => $openingbalance[1]
                ),
                'data' => $data);
        }// end count of transaction
        return $row;
    }

    public function sendSMSByGateway($number, $message, $msgformat = 'text') {
        $number = '9843258019';
        $smsprovider = CountrySmsprovider::findFirst('domain = "' . SUBDOMAIN . '"');

        $error_description = array('101' => 'Invalid username/password',
            '102' => 'Sender not exist',
            '103' => 'Receiver not exist',
            '104' => 'Invalid route',
            '105' => 'Invalid message type',
            '106' => 'SMS content not exist',
            '107' => 'Transaction template mismatch',
            '108' => 'Low credits in the specified route',
            '109' => 'Account is not eligible for API',
            '110' => 'Promotional route will be working from 9am to 9pm only');

        $errorcodes = array(101, 102, 103, 104, 105, 106, 107, 108, 109, 110);

        $api = $smsprovider->api;
        $vars = array(
            '$token' => urlencode($smsprovider->token),
            '$sender' => urlencode($smsprovider->sender),
            '$message' => urlencode($message),
            '$msgtypeid' => (($msgformat == 'unicode') ? 3 : 1),
            '$number' => $number,
        );

//        print_r(strtr($api, $vars));exit;
        $fp = fopen(strtr($api, $vars), "r");
        $response = stream_get_contents($fp);
        fpassthru($fp);
        fclose($fp);
//        print_r($response);exit;
        $error = (in_array($response, $errorcodes));
        if ($error) {
            //process only when there is error
            $errordescription = $error_description[$response];
            return 0;
        } else {
            return $response;
        }
    }

    public function smsReportByGateway($messageid) {
        $smsprovider = CountrySmsprovider::findFirst('domain = "' . SUBDOMAIN . '"');
        $api = $smsprovider->reportapi;
        $vars = array(
            '$token' => urlencode($smsprovider->token),
            '$messageid' => $messageid,
        );
        $fp = fopen(strtr($api, $vars), "r");
        $response = stream_get_contents($fp);
        fpassthru($fp);
        fclose($fp);
        return json_decode($response);
    }

    public function smsReportByGatewayXML($messageid) {
//      $messageid = '85857';
        $smsprovider = CountrySmsprovider::findFirst('domain = "' . SUBDOMAIN . '"');
        $api = $smsprovider->reportapi;
        $token = urlencode($smsprovider->token);
        /* $xml = "<?xml version='1.0' encoding='UTF-8'?>
          <deliveryreport>
          <token>$token</token>
          <messageid>$messageid</messageid>
          </deliveryreport>";
          $vars = array(
          '$xml' => urlencode($xml)
          ); */

        $vars = array(
            '$token' => $token,
            '$messageid' => $messageid
        );
        $fp = fopen(strtr($api, $vars), "r");
        $response = stream_get_contents($fp);
        fpassthru($fp);
        fclose($fp);
        $resparray = json_decode($response, TRUE);
        ini_set('error_reporting', E_ALL);
        /* $xmlresp = simplexml_load_string(str_replace('<br>', '', $response));
          $respjson = json_encode($xmlresp); */
        $replyarr = array(
            'Message' => $resparray[3],
            'recipients' => $resparray[4],
        );

        return $replyarr;
    }

    public function exportExcel($filename, $reports, $header, $title = array()) {
        $counter = 0;
        $file = DOWNLOAD_DIR . $filename;
        $fp = fopen($file, 'a');
        $delimiter = ",";
        if (count($title) > 0):
            foreach ($title as $tvalue) {
                fputcsv($fp, $tvalue, $delimiter);
            }
        endif;
        fputcsv($fp, $header, $delimiter);
        foreach ($reports as $chunkrep) {
            $fp = fopen($file, 'a');
            $counter+=count($chunkrep);
            foreach ($chunkrep as $row) {
                fputcsv($fp, $row, $delimiter);
            }
            fclose($fp);
        }
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function FeeAndLedgerTree($node_id) {

        $fenames = OrganizationalStructureController::_getParentNames($node_id);
        $inp = array_reverse($fenames);

        $valuesnodescurstrcut = ControllerBase::getCommonIdsForKeysFullSearch($node_id);
        foreach ($valuesnodescurstrcut as $vvalue) {
            $actualval[] = implode(',', explode('-', $vvalue));
        }
        $actualstructural = implode(',', $actualval);
        $curfinyr = ControllerBase::getCurrentFinancialYear($actualstructural);
        if (!$curfinyr) {
            $messages['type'] = 'error';
            $messages['message'] = "Financial year is not yet created ";
            print_r(json_encode($messages));
            exit;
        }
        $orgids = ControllerBase::getLedOrgMasIds();
        $feeparent = OrganizationalStructureValues::findFirst("name LIKE 'Indirect Income'"
                        . " and org_master_id IN (" . implode(',', $orgids) . ")"
                        . " and parent_id = $curfinyr->id");

        if (!$feeparent) {
            $messages['type'] = 'error';
            $messages['message'] = "Create basic ledger nodes for current financial year.";
            print_r(json_encode($messages));
            exit;
        }
        $parentldid = $ledfeeroot = $feeparent->id;
        foreach ($inp as $inpvalue) {
            $lidp = OrganizationalStructureValues::findFirst("name LIKE '" . strtoupper($inpvalue) . "'"
                            . " and org_master_id IN (" . implode(',', $orgids) . ")"
                            . " and parent_id = $parentldid");
            if ($lidp) {
                $arryfeledger[] = $lidp->id;
                $parentldid = $lidp->id;
            }
        }
        $loopedledfeeParent = $parentldid;
        $arrledfeetreenodes = $arryfeledger;
        return array($ledfeeroot, $loopedledfeeParent, $arrledfeetreenodes, $orgids);
    }

    public function getLedOrgMasIds() {
        $masterid = OrganizationalStructureMaster::find('module="Ledger"');
        foreach ($masterid as $value) {
            $orgids[] = $value->id;
        }
        return $orgids;
    }

    public function getFeeOrgMasIds() {
        $masterid = OrganizationalStructureMaster::find('module="Fee"');
        foreach ($masterid as $value) {
            $orgids[] = $value->id;
        }
        return $orgids;
    }

    public function getNodeStructural($node_id) {
        $val = OrganizationalStructureValues::findFirstById($node_id);
        $masterid = OrganizationalStructureMaster::findFirstById($val->org_master_id);
        if ($val->parent_id > 0) {
            if ($masterid->module != 'structural') {
                $sclid = ControllerBase::getNodeStructural($val->parent_id);
            } else {
                $sclid = $node_id;
            }
        }
        return $sclid;
    }

    public function getFeeRootNode($node_id) {

        $masterid = OrganizationalStructureMaster::findFirst('module="Fee"');
        $val = OrganizationalStructureValues::find('org_master_id=' . $masterid->id);
        $chlmnode = ControllerBase::getNodeStructural($node_id);
        foreach ($val as $alvalue) {
            $valuesnodescurstrcut = ControllerBase::getCommonIdsForKeysFullSearch($alvalue->id);
            $actualval = explode(',', str_replace('-', ',', implode(',', $valuesnodescurstrcut)));
            if (in_array($chlmnode, $actualval)) {
                $ediingFee = $alvalue;
            }
        }
        return $ediingFee;
    }

    public function groupNodesById($aggregated_nodes_id) {
        $pattern = '/[-,]/';
        $Totarr = (preg_split($pattern, $aggregated_nodes_id));
        $name_param = array();
        $rcpathname = '';
        foreach ($Totarr as $rcvalue) {
            $res = ControllerBase::buildTreeQuery($rcpathname, $rcvalue);
            $inp = array_reverse($res);
            $searchword = '' . implode(',', $inp) . '';
            if (count($name_param) < 1) {
                $name_param[] = '' . implode(',', $inp) . '';
            }
            $i = 0;
            foreach ($name_param as $k => $v) {
                if (preg_match("/\b$v\b/i", $searchword)) {
                    $name_param[$k] = $searchword;
                    $i = 1;
                    break;
                }
            }

            if ($i == 0) {
                $name_param[] = '' . implode(',', $inp) . '';
            }
        }
        foreach ($name_param as $wholenode) {
            $getorgmas = array();
            $indvval = explode(',', $wholenode);
            foreach ($indvval as $node) {
                $getorgmas[$node->parentid] = $node;
            }
        }

        return $getorgmas;
    }

}
