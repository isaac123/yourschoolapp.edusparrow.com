<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class SearchController extends ControllerBase {

    /**
     * The filepath of the ACL role file  
     *
     * @var string
     */
    private $filename = 'role_resource.json';

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        // no display
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $this->tag->prependTitle("Search | ");
        $this->assets->addCss('css/edustyle.css');
    }

    public function studentAutocompleteAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $this->view->callBackFn = $this->request->getPost('callBackFn');
        }
//        $this->tag->prependTitle("Search | ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs("js/search/search.js");
    }

    public function stuAutocompltAction() {
        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('query');
                $stu_list_from_logged_user = $this->request->getPost('stulist') ? $this->request->getPost('stulist') : '';
                $stulistQuery[] = "(stinfo.Admission_no LIKE '%$searchText%' OR stinfo.loginid LIKE '%$searchText%' OR stinfo.Student_Name LIKE '%$searchText%')";
                if (($stu_list_from_logged_user)) {
                    $stulistQuery[] = ($stu_list_from_logged_user) ?
                            ' stumap.student_info_id IN (' . implode(',', $stu_list_from_logged_user) . ')' : '';
                }
                $condition = count($stulistQuery) > 0 ? " WHERE " . implode(' and ', $stulistQuery) : '';
                $studentsq = "Select stumap.student_info_id, stinfo.Student_Name, stinfo.loginid, "
                        . " GROUP_CONCAT( oval.name) as aggname "
                        . " from StudentInfo stinfo "
                        . " INNER JOIN  StudentMapping stumap on stumap.student_info_id = stinfo.id"
                        . " INNER JOIN OrganizationalStructureValues as oval on  find_in_set(oval.id,stumap.aggregate_key)"
                        . $condition
                        . "  GROUP BY stumap.student_info_id"
                        . " ORDER BY stinfo.Student_Name"
                        . " LIMIT 0,10 ";


//                echo $studentsq; exit;
                $students = $this->modelsManager->executeQuery($studentsq);
                $stuJsonArr = array();
                foreach ($students as $student) {
                    $stuJsonArr[] = array(
                        'id' => $student->student_info_id,
                        'name' => $student->Student_Name . " (" . $student->loginid . ")-" . str_replace(',', ' >> ', $student->aggname)
                    );
                }
                if ($stuJsonArr) {
                    $stujson = json_encode($stuJsonArr);
                    header('Content-Type: application/json');
                    echo $stujson;
                    exit;
                } else {
                    $error = '';
                    foreach ($students->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function stuAutocomplt1Action() {
        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('query');
                $conditions = "Admission_no LIKE :searchtxt: OR loginid LIKE :searchtxt: OR Student_Name LIKE :searchtxt:";
                $parameters = array(
                    "searchtxt" => "%" . $searchText . "%"
                );

                $stu_list_from_logged_user = $this->request->getPost('stulist') ? $this->request->getPost('stulist') : '';

                if ($stu_list_from_logged_user == '') {
                    $students = StudentInfo::find(array(
                                $conditions,
                                "bind" => $parameters,
                                "limit" => array("number" => 10, "offset" => 0)
                    ));
                    //print_r($students);exit;
                    $stuJsonArr = array();
                    foreach ($students as $student) {

                        $sturelivdet = $student->id ?
                                StudentMapping::findFirst('student_info_id = ' . $student->id) : '';

                        if ($sturelivdet != '') {

                            $stumappingdets = $sturelivdet->aggregate_key ? explode(',', $sturelivdet->aggregate_key) : '';

                            // $stumappingdets = $student->aggregate_key ? explode(',',$student->aggregate_key) : '';
                            $maping_values = '';
                            if ($stumappingdets != '') {
                                foreach ($stumappingdets as $stumappingdet) {
                                    $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stumappingdet);
                                    $maping_values .= $orgnztn_str_det->name . '-';
                                }
                            }

                            $stuJsonArr[] = array(
                                'id' => $student->id,
                                'name' => $student->Student_Name . " (" . $student->loginid . ")-" . rtrim($maping_values, '-')
                            );
                        }
                    }
                } else {
                    foreach ($stu_list_from_logged_user as $studentid) {

                        $student = StudentInfo::findFirstById($studentid);
                        $sturelivdet = $student->id ?
                                StudentMapping::findFirst('student_info_id = ' . $student->id . ' AND status !="Relieved"') : '';

                        if ($sturelivdet != '') {

                            $stumappingdets = $sturelivdet->aggregate_key ? explode(',', $sturelivdet->aggregate_key) : '';

                            // $stumappingdets = $student->aggregate_key ? explode(',',$student->aggregate_key) : '';
                            $maping_values = '';
                            if ($stumappingdets != '') {
                                foreach ($stumappingdets as $stumappingdet) {
                                    $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stumappingdet);
                                    $maping_values .= $orgnztn_str_det->name . '-';
                                }
                            }

                            $stuJsonArr[] = array(
                                'id' => $student->id,
                                'name' => $student->Student_Name . " (" . $student->loginid . ")-" . rtrim($maping_values, '-')
                            );
                        }
                    }
                }
                if ($stuJsonArr) {
                    $stujson = json_encode($stuJsonArr);
                    header('Content-Type: application/json');
                    echo $stujson;
                    exit;
                } else {
                    $error = '';
                    foreach ($students->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function stuAutocompltClassAction() { //unused
        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('query');
                $class = $this->request->getPost('class');
                $academicYrId = $this->request->getPost('academicYrId');

                $stulistQuery[] = "(fm.Admission_no LIKE '%$searchText%' OR fm.loginid LIKE '%$searchText%' OR "
                        . " fm.Student_Name LIKE '%$searchText%')";
                if (isset($academicYrId) && $academicYrId != '') {
                    $stulistQuery[] = "stumap.aggregate_key LIKE '%$academicYrId%'";
                }
                if (isset($class) && $class != 0) {
                    $stuquery = ControllerBase::buildStudentQuery($class);
                    $stulistQuery[] = "(" . implode(' OR ', $stuquery) . ")";
                }
                $condition = count($stulistQuery) > 0 ? " WHERE " . implode(' and ', $stulistQuery) : '';
                $studentsq = "Select stumap.student_info_id, fm.rollno, fm.Student_Name, fm.loginid, "
                        . " GROUP_CONCAT( oval.name) as aggname "
                        . " from StudentInfo fm "
                        . " INNER JOIN  StudentMapping stumap on stumap.student_info_id = fm.id"
                        . " INNER JOIN OrganizationalStructureValues as oval on  find_in_set(oval.id,stumap.aggregate_key)"
                        . $condition
                        . "  GROUP BY stumap.student_info_id"
                        . " ORDER BY fm.Student_Name"
                        . " LIMIT 0,10 ";

//                echo $studentsq; exit;
                $students = $this->modelsManager->executeQuery($studentsq);
                $stuJsonArr = array();
                foreach ($students as $student) {
                    $stuJsonArr[] = array(
                        'id' => $student->student_info_id,
                        'name' => $student->Student_Name . " - " . $student->rollno . " (" . $student->loginid . ") -" . str_replace(',', ' >> ', $student->aggname)
                    );
                }

                if ($stuJsonArr) {
                    $stujson = json_encode($stuJsonArr);
                    header('Content-Type: application/json');
                    echo $stujson;
                    exit;
                } else {
                    $error = '';
                    foreach ($students->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function stuAutocompltWithRelievedStudentAction() {//changed
        try {
            if ($this->request->isPost()) {
//                $searchText = $this->request->getPost('search');
                $params = $queryParams = array();
                $searchAggre = $this->request->getPost('searchData');
                foreach ($this->request->getPost() as $key => $value) {
                    $IsSubdiv = explode('_', $key);
                    if ($IsSubdiv[0] == 'aggregate' && $value != '') {
                        $params['aggregateids'][] = $value;
                    } else {
                        $params[$key] = $value;
                    }
                }
                $searchText = $params['search'];

                $stulistQuery[] = "stumap.status NOT IN ('Relieved')";
                $stulistQuery[] = "(fm.Admission_no LIKE '%$searchText%' OR fm.loginid LIKE '%$searchText%' OR "
                        . " fm.Student_Name LIKE '%$searchText%')";
                if (isset($searchAggre) && count($searchAggre) > 0) {

                    $Qury = preg_filter('/^([\d])*/', '(FIND_IN_SET("$0" , REPLACE(stumap.aggregate_key, "-", "," ) ) >0)', $params['aggregateids']);

                    $stulistQuery[] = '(' . implode(' AND ', $Qury) . ')'; //'stumap.aggregate_key LIKE "' . implode(',', $params['aggregateids']) . '%"';
                }

//                $class = $this->request->getPost('class');
//                $academicYrId = $this->request->getPost('academicYrId');
//                $searchval = $this->request->getPost('search');
//                if ($searchText == '')
//                    $searchText = $searchval;
//                if (isset($academicYrId) && $academicYrId != '') {
//                    $stulistQuery[] = "stumap.aggregate_key LIKE '%$academicYrId%'";
//                }
//                if (isset($class) && $class != 0) {
//                    $stuquery = ControllerBase::buildStudentQuery($class);
//                    $stulistQuery[] = "(" . implode(' OR ', $stuquery) . ")";
//                }

                $condition = count($stulistQuery) > 0 ? " WHERE " . implode(' and ', $stulistQuery) : '';
                $studentsq = "Select stumap.student_info_id, fm.rollno, fm.Student_Name, fm.loginid, "
                        . " GROUP_CONCAT( oval.name) as aggname "
                        . " from StudentInfo fm "
                        . " INNER JOIN  StudentMapping stumap on stumap.student_info_id = fm.id"
                        . " INNER JOIN OrganizationalStructureValues as oval on  find_in_set(oval.id,stumap.aggregate_key)"
                        . $condition
                        . "  GROUP BY stumap.student_info_id"
                        . " ORDER BY fm.Student_Name"
                        . " LIMIT 0,10 ";

//                echo $studentsq; exit;
                $students = $this->modelsManager->executeQuery($studentsq);
                $stuJsonArr = array();
                foreach ($students as $student) {
                    $stuJsonArr[] = array(
                        'id' => $student->student_info_id,
//                        'name' => $student->Student_Name . " - " . $student->rollno . " (" . $student->loginid . ") -" . str_replace(',', ' >> ', $student->aggname)
                        'name' => $student->Student_Name
                    );
                }

                if ($stuJsonArr) {
                    $stujson = json_encode($stuJsonArr);
                    header('Content-Type: application/json');
                    echo $stujson;
                    exit;
                } else {
                    $error = '';
                    foreach ($students->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function stuAutocompltFeeAssignAction() {//changed
        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('query');
                $searchval = $this->request->getPost('search');
                $params = $queryParams = array();
                $searchAggre = $this->request->getPost('searchAggre');
                foreach ($this->request->getPost('searchAggre') as $key => $value) {
                    $IsSubdiv = explode('_', $key);
                    if ($IsSubdiv[0] == 'aggregate' && $value != '') {
                        $params['aggregateids'][] = $value;
                    }
                }
                if ($searchText == '')
                    $searchText = $searchval;

                $stulistQuery[] = "(fm.Admission_no LIKE '%$searchText%' OR fm.loginid LIKE '%$searchText%' OR "
                        . " fm.Student_Name LIKE '%$searchText%')";
                $stulistQuery[] = "stumap.status NOT IN ('Relieved')";
                if (isset($searchAggre) && count($searchAggre) > 0) {
                    $Qury = preg_filter('/^([\d])*/', '(FIND_IN_SET("$0" , REPLACE(stumap.aggregate_key, "-", "," ) ) >0)', $params['aggregateids']);

                    $stulistQuery[] = '(' . implode(' AND ', $Qury) . ')';

//                    $stulistQuery[] = 'stumap.aggregate_key LIKE "' . implode(',', $params['aggregateids']) . '%"';
                }
//                print_r($stulistQuery);exit;
                $condition = count($stulistQuery) > 0 ? " WHERE " . implode(' and ', $stulistQuery) : '';
                $studentsq = "Select stumap.student_info_id, fm.rollno, fm.Student_Name, fm.loginid, "
                        . " GROUP_CONCAT( oval.name) as aggname "
                        . " from StudentInfo fm "
                        . " INNER JOIN  StudentMapping stumap on stumap.student_info_id = fm.id"
                        . " INNER JOIN OrganizationalStructureValues as oval on  find_in_set(oval.id,stumap.aggregate_key)"
                        . $condition
                        . "  GROUP BY stumap.student_info_id"
                        . " ORDER BY fm.Student_Name"
                        . " LIMIT 0,10 ";

//                echo $studentsq; exit;  
                $students = $this->modelsManager->executeQuery($studentsq);
                $stuJsonArr = array();
                foreach ($students as $student) {
                    $stuJsonArr[] = array(
                        'id' => $student->student_info_id,
                        'name' => $student->Student_Name . " - " . $student->rollno . " (" . $student->loginid . ") -" . str_replace(',', ' >> ', $student->aggname)
                    );
                }

                if ($stuJsonArr) {
                    $stujson = json_encode($stuJsonArr);
                    header('Content-Type: application/json');
                    echo $stujson;
                    exit;
                } else {
                    $error = '';
                    foreach ($students->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function stuAutocompltForRelievAction() { //changed
        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('query');

                $stulistQuery[] = "(fm.Admission_no LIKE '%$searchText%' OR fm.loginid LIKE '%$searchText%' OR "
                        . " fm.Student_Name LIKE '%$searchText%')";
                $stulistQuery[] = "stumap.status NOT IN ('Relieved')";

                $condition = count($stulistQuery) > 0 ? " WHERE " . implode(' and ', $stulistQuery) : '';
                $studentsq = "Select stumap.student_info_id, fm.rollno, fm.Student_Name, fm.loginid, "
                        . " GROUP_CONCAT( oval.name) as aggname "
                        . " from StudentInfo fm "
                        . " INNER JOIN  StudentMapping stumap on stumap.student_info_id = fm.id"
                        . " INNER JOIN OrganizationalStructureValues as oval on  find_in_set(oval.id,stumap.aggregate_key)"
                        . $condition
                        . "  GROUP BY stumap.student_info_id"
                        . " ORDER BY fm.Student_Name"
                        . " LIMIT 0,10 ";

//                echo $studentsq; exit;
                $students = $this->modelsManager->executeQuery($studentsq);
                $stuJsonArr = array();
                foreach ($students as $student) {
                    $stuJsonArr[] = array(
                        'id' => $student->student_info_id,
                        'name' => $student->Student_Name . " - " . $student->rollno . " (" . $student->loginid . ") -" . str_replace(',', ' >> ', $student->aggname)
                    );
                }
                if ($stuJsonArr) {
                    $stujson = json_encode($stuJsonArr);
                    header('Content-Type: application/json');
                    echo $stujson;
                    exit;
                } else {
                    $error = '';
                    foreach ($students->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    //ledger autocomplete 
    public function ledgerAutocompltAction() {
        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('query');
                $masterid = OrganizationalStructureMaster::find('module="Ledger"');
                foreach ($masterid as $value) {
                    $orgids[] = $value->id;
                }
                $val = 'Select * FROM OrganizationalStructureValues where ' .
                        'org_master_id IN (' . implode(',', $orgids) . ') AND name ' .
                        " LIKE '%$searchText%' and status = 'C' ";
                $ledgers = $this->modelsManager->executeQuery($val);
                foreach ($ledgers as $ledger) {
                    $dview = $this->findsimpleparentledger('&nbsp;&nbsp;&nbsp;<font color="#FF33CC" ><b>', $ledger->id, '', '</b></font>');
                    $ledgJsonArr[] = array(
                        'id' => $ledger->id,
                        'name' => $dview
                    );
                    //print_r($dview);
                }
                // exit;
                if ($ledgJsonArr) {
                    $ledgjson = json_encode($ledgJsonArr);
                    header('Content-Type: application/json');
                    echo $ledgjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($ledgers->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function studentAutoCmpltSearchAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        try {
            if ($this->request->isPost()) {
                $searchId = $this->request->getPost('studentID');
                if (!$searchId) {
                    echo '<div class="alert alert-block alert-danger fade in">Select a suggested value!</div>';
                    exit;
                } else {
                    $stuarr = array();
                    $student = StudentInfo::findFirstById($searchId);
                    foreach ($student as $key => $sVal)
                        $stuarr[$key] = $sVal;
                    $stuJson = json_encode($stuarr);
                    echo $stuJson;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function studentGroupSearchAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->callBackFn = $this->request->getPost('callBackFn');
        }
        $this->tag->prependTitle("Search | ");
        $this->assets->addCss('css/edustyle.css');
    }

    public function loadClassByAcademicYearAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $academicYrID = $this->request->getPost('academicYr');
            $cacdyr = AcademicYearMaster::findFirstById($academicYrID);
            $this->view->div = DivisionMaster::findFirst('divType = "student" and academicYrID=' . $cacdyr->id);
        }
    }

    public function loadSectionsByClassAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->classId = $this->request->getPost('classId');
            $academicYrID = $this->request->getPost('academicYr');
            $cacdyr = AcademicYearMaster::findFirstById($academicYrID);
            $this->view->div = DivisionMaster::findFirst('divType = "student" and academicYrID=' . $cacdyr->id);
            $this->view->subdivs = SubDivisionMaster::find('divID =' . $this->view->div->id);
        }
    }

    public function studentGrpSearchAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        try {
            if ($this->request->isPost()) {
                $classId = $this->request->getPost('divValID');
                $academicYrID = $this->request->getPost('academicYrId');
                $subDivVal = $this->request->getPost('subDivVal');
                if (!$classId && !$academicYrID && !$subDivVal) {
                    echo '<div class="alert alert-block alert-danger fade in">Select a suggested value!</div>';
                    exit;
                } else {
                    $conditions = '';
                    $parameters = array();
                    if ($classId) {
                        $conditions .= "Division_Class LIKE :divValID:";
                        $parameters["divValID"] = $classId; //DivisionValues::findFirstById($classId)->classname;
                    }
                    if ($subDivVal) {
                        $conditions .= "  AND Subdivision_section LIKE :subDivVal:";
                        $parameters["subDivVal"] = $subDivVal; //SubDivisionValues::findFirstById($subDivVal)->name;
                    }
                    $students = StudentInfo::find(array(
                                $conditions,
                                "bind" => $parameters
                    ));
                    $stuList = array();
                    foreach ($students as $student) {
                        $stuarr = array();
                        foreach ($student as $key => $sVal)
                            $stuarr[$key] = $sVal;
                        $stuList[] = $stuarr;
                    }
                    $stuJson = json_encode($stuList);
                    echo $stuJson;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $messages) {
                $error .= $messages;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function staffAutocompleteAction() {
        // Shows only the view related to the action
        //$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->callBackFn = $this->request->getPost('callBackFn');
        }
        $this->tag->prependTitle("Search | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs("js/search/search.js");
    }

    public function stfAutocompltAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            if ($this->request->isPost()) {
                $identity = $this->auth->getIdentity();
                $searchText = $this->request->getPost('query');
                $conditions = "(appointment_no LIKE :searchtxt: OR loginid LIKE :searchtxt: OR Staff_Name LIKE :searchtxt:) AND (status IN('Appointed','Relieved','Relieving initiated') OR loginid LIKE 'Superadmin'  OR loginid LIKE 'Siteadmin')";
                $parameters = array(
                    "searchtxt" => "%" . $searchText . "%"
                );

                $staffs = StaffInfo::find(array(
                            $conditions,
                            "bind" => $parameters
                ));
//                                print_r($staffs);exit;
                $stfJsonArr = array();
                foreach ($staffs as $staff) {
                    $stfJsonArr[] = array(
                        'id' => $staff->id,
                        'name' => $staff->Staff_Name . "-" . $staff->appointment_no . "(" . $staff->loginid . ")"
                    );
                }

                if ($stfJsonArr) {
                    $stfjson = json_encode($stfJsonArr);
                    header('Content-Type: application/json');
                    echo $stfjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($staffs->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function mailstfAutocompltAction() {
        try {
            $param = array('role' => $isreq->is_default_by_role,
                'appkey' => APPKEY,
                'subdomain' => SUBDOMAIN,
                'businesskey' => BUSINESSKEY);
            $mail_data_string = json_encode($param);
//                      print_r($param); 
            $response = IndexController::curlIt(USERAUTHAPI . 'getAcademicStaff', $mail_data_string);
            $roleuser = json_decode($response);
            if (!$roleuser->status) {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                print_r(json_encode($message));
                exit;
            }
            if ($roleuser->status == 'ERROR') {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $roleuser->messages . '</div>';
                print_r(json_encode($message));
                exit;
            } else if ($roleuser->status == 'SUCCESS' && $roleuser->data) {
                $data = implode(',', $roleuser->data);
                $searchText = $this->request->get('q');
                $conditions = " status = 'Appointed' and (appointment_no LIKE :searchtxt: OR"
                        . " loginid LIKE :searchtxt: OR Staff_Name LIKE :searchtxt:)"
                        . " and loginid IN (" . $data . ")";
                $parameters = array(
                    "searchtxt" => "%" . $searchText . "%"
                );

                $staffs = StaffInfo::find(array(
                            $conditions,
                            "bind" => $parameters
                ));
//            print_r($staffs);exit;
                $stfJsonArr = array();
                foreach ($staffs as $staff) {
                    $stfJsonArr[] = array(
                        'id' => $staff->Staff_Name . "(" . $staff->loginid . ")",
                        'name' => $staff->Staff_Name . "-" . $staff->Appointment_no . "(" . $staff->loginid . ")"
                    );
                }

                if ($stfJsonArr) {
                    $stfjson = json_encode($stfJsonArr);
                    header('Content-Type: application/json');
                    echo $stfjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($staffs->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function staffAutoCmpltSearchAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        try {
            if ($this->request->isPost()) {
                $searchId = $this->request->getPost('staffID');
                if (!$searchId) {
                    echo '<div class="alert alert-block alert-danger fade in">Select a suggested value!</div>';
                    exit;
                } else {
                    $stfarr = array();
                    $staff = StaffInfo::findFirstById($searchId);
                    foreach ($staff as $key => $sVal)
                        $stfarr[$key] = $sVal;
                    $stfJson = json_encode($stfarr);
                    echo $stfJson;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function staffGroupSearchAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->callBackFn = $this->request->getPost('callBackFn');
        }
//        $this->tag->prependTitle("Search | ");
//        $this->assets->addCss('css/edustyle.css');
        //print_r($rolearr);exit;
    }

    public function loadDeptByAcademicYrAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $academicYrID = $this->request->getPost('academicYr');
            $cacdyr = AcademicYearMaster::findFirstById($academicYrID);
            $this->view->div = DivisionMaster::findFirst('divType = "staff" and academicYrID=' . $cacdyr->id);
        }
    }

    public function staffGrpSearchAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        try {
            if ($this->request->isPost()) {
                $deptId = $this->request->getPost('divValID');
                $academicYrID = $this->request->getPost('academicYrId');
                if (!$deptId && !$academicYrID) {
//                    echo '<div class="alert alert-block alert-danger fade in">Select a suggested value!</div>';
//                    exit;
                    $students = StaffInfo::find();
                } else {
                    $conditions = '';
                    $parameters = array();

                    if ($deptId && !$academicYrID) {
                        $conditions .= "Department LIKE :divValID:";
                        $parameters["divValID"] = $deptId;
                        $students = StaffInfo::find(array(
                                    $conditions,
                                    "bind" => $parameters
                        ));
                    } elseif ($deptId && $academicYrID) {
                        $conditions .= "Department LIKE :divValID: AND academic_year_id LIKE :acd_yr_id:";
                        $parameters["divValID"] = $deptId;
                        $parameters["acd_yr_id"] = $academicYrID;
                        $students = StaffInfo::find(array(
                                    $conditions,
                                    "bind" => $parameters
                        ));
                    } elseif (!$deptId && $academicYrID) {
                        $conditions .= "academic_year_id LIKE :acd_yr_id:";
                        $parameters["acd_yr_id"] = $academicYrID;
                        $students = StaffInfo::find(array(
                                    $conditions,
                                    "bind" => $parameters
                        ));
                    }
                }
                $stuList = array();
                foreach ($students as $student) {
                    $stuarr = array();
                    foreach ($student as $key => $sVal)
                        $stuarr[$key] = $sVal;
                    $stuList[] = $stuarr;
                }
                $stuJson = json_encode($stuList);
                echo $stuJson;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    //Payment Voucher autocomplete 
    public function debitLegderAutocompltAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('search');
                $record_type_id = $this->request->getPost('record_type_id');

                //First find whether there is any record type with the ID provided.
                $record_type = RecordTypesSettings::findfirst('record_types_id = ' . $record_type_id);



                $ledgJsonArr = array();
                $ids = array();
                $ledger_ids = array();


                if ($record_type && ($record_type->debit_ledger_ids != "")) {
                    //Action when there is a record type and debit ledger id is not empty

                    $ids[] = $record_type->debit_ledger_ids;
                    if ($record_type->allow_child_debit) {

                        //$i = 0;
                        do {

                            $ledger_ids[] = implode(',', $ids);
                            $ledids = OrganizationalStructureValues::find(array(
                                        'columns' => 'id',
                                        'parent_id IN (' . implode(',', $ids) . ')'
                            ));
                            $ids = array();
                            if (count($ledids) > 0):
                                foreach ($ledids as $ledid) {
                                    $ids[] = $ledid->id;
                                }
                            endif;
                        } while (count($ids) > 0);
                    } else {
                        $ledger_ids[] = implode(',', $ids);
                    }
                    $search_ids = implode(',', $ledger_ids);
                    $phql = 'Select group_concat(id) as id from organizational_structure_values as a where ' .
                            'a.id IN (' . $search_ids . ') AND a.name ' .
                            " LIKE '%$searchText%'";
                    $results = $this->db->query($phql);
                    $result = $results->fetch();

                    if ($result['id'] != NULL) {
                        $result_ids = explode(",", $result['id']);


                        foreach ($result_ids as $ledger_id) {
                            $dview = $this->findsimpleparentledger('&nbsp;&nbsp;&nbsp;<font color="#FF33CC" ><b>', $ledger_id, '', '</b></font>');
                            $ledgJsonArr[] = array(
                                'id' => $ledger_id,
                                'name' => $dview
                            );
                            //print_r($dview);
                        }

                        if ($ledgJsonArr) {
                            $ledgjson = json_encode($ledgJsonArr);
                            header('Content-Type: application/json');
                            echo $ledgjson;
                            exit;
                        } else {
                            $error = '';
                            foreach ($ledgers->getMessages() as $msg) {
                                $error .= $msg;
                            }
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            exit;
                        }
                    }
                } else {
                    $masterid = OrganizationalStructureMaster::find('module="Ledger"');
                    foreach ($masterid as $value) {
                        $orgids[] = $value->id;
                    }
                    $val = 'Select * FROM OrganizationalStructureValues where ' .
                            'org_master_id IN (' . implode(',', $orgids) . ') AND name ' .
                            " LIKE '%$searchText%' and status = 'C' ";
                    $ledgers = $this->modelsManager->executeQuery($val);
                    foreach ($ledgers as $ledger) {
                        $dview = $this->findsimpleparentledger('&nbsp;&nbsp;&nbsp;<font color="#FF33CC" ><b>', $ledger->id, '', '</b></font>');
                        $ledgJsonArr[] = array(
                            'id' => $ledger->id,
                            'name' => $dview
                        );
                        //print_r($dview);
                    }
                    // exit;
                    if ($ledgJsonArr) {
                        $ledgjson = json_encode($ledgJsonArr);
                        header('Content-Type: application/json');
                        echo $ledgjson;
                        exit;
                    } else {
                        $error = '';
                        foreach ($ledgers->getMessages() as $message) {
                            $error .= $message;
                        }
                        echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function creditLegderAutocompltAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('search');
                $record_type_id = $this->request->getPost('record_type_id');

                //First find whether there is any record type with the ID provided.
                $record_type = RecordTypesSettings::findfirst('record_types_id = ' . $record_type_id);



                $ledgJsonArr = array();
                $ledger_ids = array();
                $ids = array();

                if ($record_type && ($record_type->credit_ledger_ids != "")) {
                    //Action when there is a record type and credit ledger id is not empty

                    $ids[] = $record_type->credit_ledger_ids;
                    if ($record_type->allow_child_credit) {

                        do {

                            $ledger_ids[] = implode(',', $ids);
                            $ledids = OrganizationalStructureValues::find(array(
                                        'columns' => 'id',
                                        'parent_id IN (' . implode(',', $ids) . ')'
                            ));
                            $ids = array();
                            if (count($ledids) > 0):
                                foreach ($ledids as $ledid) {
                                    $ids[] = $ledid->id;
                                }
                            endif;
                        } while (count($ids) > 0);
                    } else {
                        $ledger_ids[] = implode(',', $ids);
                    }
                    //We have $ledger_ids at this point containing the ids which should be included in query
                    $search_ids = implode(',', $ledger_ids);
                    $phql = 'Select group_concat(a.id) as id from organizational_structure_values as a where ' .
                            'a.id IN (' . $search_ids . ') AND a.name ' .
                            " LIKE '%$searchText%'";
                    $results = $this->db->query($phql);
                    $result = $results->fetch();
                    if ($result['id'] != NULL) {
                        $result_ids = explode(",", $result['id']);


                        foreach ($result_ids as $ledger_id) {
                            $dview = $this->findsimpleparentledger('&nbsp;&nbsp;&nbsp;<font color="#FF33CC" ><b>', $ledger_id, '', '</b></font>');
                            $ledgJsonArr[] = array(
                                'id' => $ledger_id,
                                'name' => $dview
                            );
                            //print_r($dview);
                        }


                        if ($ledgJsonArr) {
                            $ledgjson = json_encode($ledgJsonArr);
                            header('Content-Type: application/json');
                            echo $ledgjson;
                            exit;
                        } else {
                            $error = '';
                            foreach ($ledgers->getMessages() as $msg) {
                                $error .= $msg;
                            }
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            exit;
                        }
                    }
                } else {
                    $masterid = OrganizationalStructureMaster::find('module="Ledger"');
                    foreach ($masterid as $value) {
                        $orgids[] = $value->id;
                    }
                    $val = 'Select * FROM OrganizationalStructureValues where ' .
                            'org_master_id IN (' . implode(',', $orgids) . ') AND name ' .
                            " LIKE '%$searchText%' and status = 'C' ";
                    $ledgers = $this->modelsManager->executeQuery($val);
                    foreach ($ledgers as $ledger) {
                        $dview = $this->findsimpleparentledger('&nbsp;&nbsp;&nbsp;<font color="#FF33CC" ><b>', $ledger->id, '', '</b></font>');
                        $ledgJsonArr[] = array(
                            'id' => $ledger->id,
                            'name' => $dview
                        );
                        //print_r($dview);
                    }
                    // exit;
                    if ($ledgJsonArr) {
                        $ledgjson = json_encode($ledgJsonArr);
                        header('Content-Type: application/json');
                        echo $ledgjson;
                        exit;
                    } else {
                        $error = '';
                        foreach ($ledgers->getMessages() as $message) {
                            $error .= $message;
                        }
                        echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function LegderAutocompltWitIDAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('search');

                //First find whether there is any record type with the ID provided.

                $ledgJsonArr = array();
                $ids = "";
                $ledger_ids = "";
                $fields = OrganizationalStructureMaster::find('module LIKE "Ledger"');
                foreach ($fields as $value) {
                    $ledgids[] = $value->id;
                }
                $phql = 'Select * FROM  OrganizationalStructureValues where org_master_id IN(' . implode(',', $ledgids) .
                        ') and name' . " LIKE '%$searchText%' and status = 'C' ";
                $ledgers = $this->modelsManager->executeQuery($phql);
                foreach ($ledgers as $ledger) {
                    $dview = $this->findsimpleparentledger('', $ledger->id);
                    $ledgJsonArr[] = array(
                        'id' => $ledger->id,
                        'name' => $dview //. '-' . $ledger->lid
                    );
                    //print_r($dview);
                }
                // exit;
                if ($ledgJsonArr) {
                    $ledgjson = json_encode($ledgJsonArr);
                    header('Content-Type: application/json');
                    echo $ledgjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($ledgers->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function findsimpleparentledger($space = '', $lid, $choutput = '', $temp = '') {
        $choutput.=$space . SearchController::getledgernamefor($lid) . $temp;
        $parentid = OrganizationalStructureValues::findFirstById($lid)->parent_id;
        //foreach ($chld as $chl) {
        if (!empty($parentid))
            $choutput = SearchController::findsimpleparentledger('&nbsp;>>&nbsp;', $parentid, $choutput, '');

        return $choutput;
    }

    public function getledgernamefor($ledgerid) {
        $ledgname = OrganizationalStructureValues::findFirstById($ledgerid)->name;
        if (!empty($ledgname))
            return $ledgname;
    }

    public function stfAutocompltWithStaffIdAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            if ($this->request->isPost()) {
                $identity = $this->auth->getIdentity();
                $searchText = $this->request->getPost('query');
                $conditions = "(appointment_no LIKE :searchtxt: OR loginid LIKE :searchtxt: OR Staff_Name LIKE :searchtxt:)AND status IN('Appointed','Relieving initiated')";
                $parameters = array(
                    "searchtxt" => "%" . $searchText . "%"
                );
                $staffs = StaffInfo::find(array(
                            $conditions,
                            "bind" => $parameters
                ));
                $stfJsonArr = array();
                foreach ($staffs as $staff) {
                    $stfJsonArr[] = array(
                        'id' => $staff->id,
                        'name' => $staff->Staff_Name . "-" . $staff->Appointment_no . "(" . $staff->loginid . ")"
                    );
                }

                if ($stfJsonArr) {
                    $stfjson = json_encode($stfJsonArr);
                    header('Content-Type: application/json');
                    echo $stfjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($staffs->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function userAutocompltAction() {
        try {
            if ($this->request->isPost()) {

                $userArr = array();
                $searchText = $this->request->getPost('query');
                $conditions = "Admission_no LIKE :searchtxt: OR loginid LIKE :searchtxt: OR Student_Name LIKE :searchtxt:";
                $parameters = array(
                    "searchtxt" => "%" . $searchText . "%"
                );

                $students = StudentInfo::find(array(
                            $conditions,
                            "bind" => $parameters
                ));
                //print_r($students);exit;
//                $stuJsonArr = array();
                foreach ($students as $student) {
                    $userArr[] = array(
                        'id' => 'student_' . $student->id . "_" . $student->loginid . '-' . $student->Phone,
                        'name' => $student->Student_Name . "-" . $student->Admission_no . "(" . $student->loginid . ")"
                    );
                }

                $conditions = "(appointment_no LIKE :searchtxt: OR loginid LIKE :searchtxt: OR Staff_Name LIKE :searchtxt:)";
                $parameters = array(
                    "searchtxt" => "%" . $searchText . "%"
                );

                $staffs = StaffInfo::find(array(
                            $conditions,
                            "bind" => $parameters
                ));
//                $stfJsonArr = array();
                foreach ($staffs as $staff) {
                    $userArr[] = array(
                        'id' => 'staff_' . $staff->id . "_" . $staff->loginid . '-' . $staff->Mobile_No,
                        'name' => $staff->Staff_Name . "-" . $staff->appointment_no . "(" . $staff->loginid . ")"
                    );
                }


                if ($userArr) {
                    $userjson = json_encode($userArr);
                    header('Content-Type: application/json');
                    echo $userjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($students->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function tagsAutoCmpltAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            if ($this->request->isGet()) {

                $userArr = array();
                $searchText = $this->request->get('term');

                $conditions = "tagname LIKE :searchtxt:";
                $parameters = array(
                    "searchtxt" => "%" . $searchText . "%"
                );

                $smstem = SmsTags::find(array(
                            $conditions,
                            "bind" => $parameters
                ));
//                $stfJsonArr = array();
                foreach ($smstem as $tags) {
                    $userArr[] = array(
                        'label' => $tags->tagname,
                        'value' => $tags->tagname,
                    );
                }

                $userjson = json_encode($userArr);
                header('Content-Type: application/json');
                echo $userjson;
                exit;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function stuAutocompltforFeeAction() {
        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('query');
                $conditions = "Admission_no LIKE :searchtxt: OR loginid LIKE :searchtxt: OR Student_Name LIKE :searchtxt:";
                $parameters = array(
                    "searchtxt" => "%" . $searchText . "%"
                );


                $queryParams = array();

                $identity = $this->auth->getIdentity();
                $uid = $identity['id'];
                $appItem = ApprovalItem::find(array(
                            'conditions' => "forwaded_to =$uid ",
                            'columns' => "MAX(id) as id,approval_master_id",
                            'order' => "id DESC",
                            'group' => "approval_master_id"
                ));

                $selectedMasterArr = array();
                if (count($appItem) > 0):
                    foreach ($appItem as $appItm) {
                        $selectedMasterArr[] = $appItm->approval_master_id;
                    }
                endif;

                if (count($selectedMasterArr) > 0) {
                    $queryParams[] = ' appm.id IN (' . implode(',', $selectedMasterArr) . ')';
                }

                $phql = 'SELECT appm.id,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,stufee.student_id,
                        stufee.fee_item_id,stufee.status,stuinfo.Student_Name,stuinfo.loginid
                        FROM ApprovalMaster appm, StudentFeeTable stufee, ApprovalTypes aptyp, StudentInfo stuinfo WHERE
           ' . implode(' and ', $queryParams) . ' '
                        . 'and aptyp.id=appm.approval_type_id '
                        . 'and appm.Item_id=stufee.id '
                        . 'and stufee.student_id=stuinfo.id '
                        . 'and aptyp.approval_type IN("Fee cancel","General Fees")'
                        . 'and stuinfo.Student_Name'
                        . " LIKE '%$searchText%'";




                $result = $this->modelsManager->executeQuery($phql);


                $stuJsonArr = array();
                foreach ($result as $student) {

                    //print_r($student); exit;
                    $sturol = StudentAcademicMaster::findFirstByStudentId($student->student_id)->rollno;
                    $stuJsonArr[] = array(
                        'id' => $student->student_id,
                        'name' => $student->Student_Name . "-" . $sturol . " (" . $student->loginid . ") "
                    );
                }


                if ($stuJsonArr) {
                    $stujson = json_encode($stuJsonArr);
                    header('Content-Type: application/json');
                    echo $stujson;
                    exit;
                } else {
                    $error = 'error';
//                    foreach ($students->getMessages() as $message) {
//                        $error .= $message;
//                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function feenameAutocompltAction() {
        try {
            $searchText = $this->request->get('search');
            $condition = '';
            $params = $queryParams = array();
            $searchAggre = $this->request->getPost('searchAggre');
            foreach ($this->request->getPost('searchAggre') as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'feeaggregate' && $value != '') {
                    $params['aggregateids'][] = $value;
                }
            }
//            $stulistQuery[] = " oval.name LIKE '%$searchText%'";
            if (isset($params['aggregateids']) && count($params['aggregateids']) > 0) {
                $stulistQuery[] = 'fm.fee_node LIKE "' . implode('-', $params['aggregateids']) . '%"';
            }
//                $stulistQuery[] = "stumap.status NOT IN ('Relieved')";
//
            $condition = count($stulistQuery) > 0 ? " WHERE " . implode(' and ', $stulistQuery) : '';
            $having = " having feename LIKE '%$searchText%' ";
            $phql = "SELECT fm.id, GROUP_CONCAT( oval.name ) AS feename FROM FeeMaster fm "
                    . " INNER JOIN OrganizationalStructureValues as oval on   FIND_IN_SET( oval.id, REPLACE( fm.fee_node ,  '-',  ',' ) ) "
                    . $condition
                    . "  GROUP BY  fee_node "
                    . $having
                    . "  LIMIT 0,10 ";
//            echo $phql;
//            exit;
            $feenames = $this->modelsManager->executeQuery($phql);

            $stfJsonArr = array();
            foreach ($feenames as $feename) {
                $stfJsonArr[] = array(
                    'id' => $feename->id,
                    'name' => str_replace(',', ' >> ', $feename->feename)
                );
            }

            if ($stfJsonArr) {
                $stfjson = json_encode($stfJsonArr);
                header('Content-Type: application/json');
                echo $stfjson;
                exit;
            } else {
                $error = '';
                foreach ($staffs->getMessages() as $message) {
                    $error .= $message;
                }
                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                exit;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function getStudentListByCurrentLoginIDAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            if ($this->request->isPost()) {
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                $stu_final_arr = array();
                if (in_array('Classroom', $identity['role_name']) && (count($identity['role_name']) == 1)) {
                    $subject_teacher_records = GroupSubjectsTeachers::find('teachers_id  LIKE "%' . $uid . '%"');
                    $class_teacher_records = GroupClassTeachers::find('teachers_id  LIKE "%' . $uid . '%"');
                    $stu_array = array();
                    foreach ($subject_teacher_records as $subject_teacher_record) {
                        $nodes_array[] = $subject_teacher_record->aggregated_nodes_id;
                    }
                    foreach ($class_teacher_records as $class_teacher_record) {
                        $nodes_array[] = $class_teacher_record->aggregated_nodes_id;
                    }
                    $res = ControllerBase::buildStudentQuery(implode(' , ', $nodes_array));
                    $students = StudentMapping::find('status = "Inclass" and (' . implode(' or ', $res) . ')');
                    foreach ($students as $student) {
                        $stu_array[] = $student->id;
                    }
                    $stu_final_arr['stu_ids'] = array_unique($stu_array);
                } else {
                    $stu_final_arr['stu_ids'] = '';
                }
                echo json_encode($stu_final_arr);
                exit;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function _getMandNodesForExam($acdyrMas, $nodes = array()) {
        $fields = OrganizationalStructureValues::findFirstById($acdyrMas->parent_id);
        $iscycle = OrganizationalStructureMaster::findFirstById($acdyrMas->org_master_id);
//        $query = 'id =' . $fields->org_master_id . ' AND module !="Subject"';
//        $org_mas = OrganizationalStructureMaster::find(array(
//                    $query,
//                    "columns" => "COUNT(*) as cnt"
//        ));
//        echo $iscycle->id.$iscycle->promotion_status.$name->id.'<br>';
        if (!in_array($iscycle->module, array("structural", "StaffCycleNode", "Ledger"))) {
            if ($iscycle->module != "Subject"):
                $nodes[$acdyrMas->id] = $acdyrMas->name;

            endif;
            if (isset($fields->parent_id)):
//            echo $fields->parent_id;
                $nodes = $this->_getMandNodesForExam($fields, $nodes);
            endif;
        }

        return $nodes;
    }

    public function stfAutocompltForRoleAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            if ($this->request->isPost()) {
                $identity = $this->auth->getIdentity();
                $searchText = $this->request->getPost('query');
                $conditions = "(appointment_no LIKE :searchtxt: OR loginid LIKE :searchtxt: OR Staff_Name LIKE :searchtxt:) AND status IN('Appointed','Relieving initiated','NULL')";
                $parameters = array(
                    "searchtxt" => "%" . $searchText . "%"
                );

                $staffs = StaffInfo::find(array(
                            $conditions,
                            "bind" => $parameters
                ));
//                                print_r($staffs);exit;
                $stfJsonArr = array();
                foreach ($staffs as $staff) {
                    $stfJsonArr[] = array(
                        'id' => $staff->loginid,
                        'name' => $staff->Staff_Name . "-" . $staff->appointment_no . "(" . $staff->loginid . ")"
                    );
                }

                if ($stfJsonArr) {
                    $stfjson = json_encode($stfJsonArr);
                    header('Content-Type: application/json');
                    echo $stfjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($staffs->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function materialAutoCompltAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            if ($this->request->isPost()) {
                $identity = $this->auth->getIdentity();
                $searchText = $this->request->getPost('query');
                $material_list = $this->request->getPost('mat_unit_id');

                if (count($material_list) > 0) {
                    foreach ($material_list as $ids) {
                        $matitem = MaterialUnit::findFirst('material_master_id = ' . $ids);
                        $unititems = UnitMaster::findFirstById($matitem->unit_master_id);
                        $queryParams[] = "!(id =  $ids)";
                    }
                }
                if (count($queryParams) > 0) {
                    $queryconditions[] = '(' . implode(' AND  ', $queryParams) . ')';
                }
                $queryconditions[] = "(material_name LIKE  '%" . $searchText . "%'"
                        . " OR material_no LIKE '%" . $searchText . "%')";

                $condition = (count($queryconditions) > 0) ? ' where ' . implode(' and ', $queryconditions) : '';


                $matdet = 'SELECT id,material_name,material_no FROM MaterialMaster mat ' . $condition;


                $material = $this->modelsManager->executeQuery($matdet);

                $stfJsonArr = array();
                foreach ($material as $mat) {
                    $stfJsonArr[] = array(
                        'id' => $mat->id,
                        'name' => $mat->material_name
                    );
                }

                if ($stfJsonArr) {
                    $stfjson = json_encode($stfJsonArr);
                    header('Content-Type: application/json');
                    echo $stfjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($staffs->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function stockmaterialAutoCompltAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            if ($this->request->isPost()) {
                $identity = $this->auth->getIdentity();
                $searchText = $this->request->getPost('query');
                $material_list = $this->request->getPost('mat_unit_id');

                if (count($material_list) > 0) {
                    foreach ($material_list as $ids) {
                        $matitem = MaterialUnit::findFirst('material_master_id = ' . $ids);
                        $unititems = UnitMaster::findFirstById($matitem->unit_master_id);
                        // $material_id = explode('_', $ids);
                        $queryParams[] = "!(sto.material_master_id =  $ids AND  sto.unit_id  = $unititems->id  )";
                    }
                }
                if (count($queryParams) > 0) {
                    $queryconditions[] = '(' . implode(' AND  ', $queryParams) . ')';
                }
                $stock = Stock::find(array('columns' => 'group_concat(material_master_id) as material_id'));
                $queryconditions[] = "(mat.material_name LIKE  '%" . $searchText . "%'"
                        . " OR mat.material_no LIKE '%" . $searchText . "%'"
                        . " AND (mat.id IN (" . $stock[0]->material_id . ")))";

                $query = 'SELECT adj.material_id  FROM StockAdjustmentMaster sto INNER JOIN StockAdjustmentItems adj ON adj.adjustment_id = sto.id where adj.status NOT IN("Rejected","Approved") ';

                $item = $this->modelsManager->executeQuery($query);

                $items = array();
                foreach ($item as $id) {
                    $items[] = $id->material_id;
                }

                $queryconditions[] = "!(mat.id IN (" . implode(',', $items) . ")) ";

                $condition = (count($queryconditions) > 0) ? ' where ' . implode(' and ', $queryconditions) : '';

                $matdet = 'SELECT mat.id,mat.material_name FROM MaterialMaster mat INNER Join Stock sto ON sto.material_master_id = mat.id ' . $condition . 'group by mat.id';


//               print_r($matdet);
//               exit;
                $material = $this->modelsManager->executeQuery($matdet);

//                print_r($material);
//                exit;

                $stfJsonArr = array();
                foreach ($material as $mat) {
                    $unit = MaterialUnit::findFirst('material_master_id =' . $mat->id);
                    $stfJsonArr[] = array(
                        'id' => $mat->id,
                        'unit_id' => $unit->unit_master_id,
                        'name' => $mat->material_name
                    );
                }

                if ($stfJsonArr) {
                    $stfjson = json_encode($stfJsonArr);
                    header('Content-Type: application/json');
                    echo $stfjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($staffs->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function materialAutoCompleteAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            if ($this->request->isPost()) {
                $identity = $this->auth->getIdentity();
                $searchText = $this->request->getPost('query');
                $material_list = $this->request->getPost('mat_unit_id');

                if (count($material_list) > 0) {
                    foreach ($material_list as $ids) {
                        $matitem = MaterialUnit::findFirst('material_master_id = ' . $ids);
                        $unititems = UnitMaster::findFirstById($matitem->unit_master_id);
                        $queryParams[] = "!(id =  $ids)";
                    }
                }
                if (count($queryParams) > 0) {
                    $queryconditions[] = '(' . implode(' AND  ', $queryParams) . ')';
                }

                $queryconditions[] = "(material_name LIKE  '%" . $searchText . "%'"
                        . " OR material_no LIKE '%" . $searchText . "%')";

                $query = 'SELECT adj.material_id  FROM StockAdjustmentMaster sto INNER JOIN StockAdjustmentItems adj ON adj.adjustment_id = sto.id where adj.status NOT IN("Rejected","Approved") ';

                $item = $this->modelsManager->executeQuery($query);

                $items = array();
                foreach ($item as $id) {
                    $items[] = $id->material_id;
                }

                $queryconditions[] = "!(mat.id IN (" . implode(',', $items) . ")) ";

                $condition = (count($queryconditions) > 0) ? ' where ' . implode(' and ', $queryconditions) : '';


                $matdet = 'SELECT id,material_name,material_no FROM MaterialMaster mat ' . $condition;


                $material = $this->modelsManager->executeQuery($matdet);


                $stfJsonArr = array();
                foreach ($material as $mat) {
                    $stfJsonArr[] = array(
                        'id' => $mat->id,
                        'name' => $mat->material_name
                    );
                }

                if ($stfJsonArr) {
                    $stfjson = json_encode($stfJsonArr);
                    header('Content-Type: application/json');
                    echo $stfjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($staffs->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function orgmasSearchAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('search');

                //First find whether there is any record type with the ID provided.

                $orgMasJsonArr = array();
                $ids = "";


                $conditions = "name LIKE :searchtxt:";
                $parameters = array(
                    "searchtxt" => "%" . $searchText . "%"
                );

                $orgmasdets = OrganizationalStructureMaster::find(array(
                            $conditions,
                            "bind" => $parameters
                ));

//   
                foreach ($orgmasdets as $orgmasdet) {
                    $dview = SearchController::findorgmasparent('', $orgmasdet->id);
                    $orgMasJsonArr[] = array(
                        'id' => $orgmasdet->id,
                        'name' => $dview //$orgmasdet->name 
                    );
                }
                // exit;
                if ($orgMasJsonArr) {
                    $orgmasjson = json_encode($orgMasJsonArr);
                    header('Content-Type: application/json');
                    echo $orgmasjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($orgmasdets->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function _getRecursiveCycle($editval) {
        $valnode = OrganizationalStructureValues::findFirstById($editval);
        $iscycle = OrganizationalStructureMaster::findFirstById($valnode->org_master_id);
        if ($iscycle->cycle_node == '1') {
            $rcpathname = $editval;
        } else {
            $rcpathname = SearchController::_getRecursiveCycle($valnode->parent_id);
        }
        return $rcpathname;
    }

    public function _getRecursiveparent($editval, $rcpathname = array()) {
        $rcpathname[] = $editval;
        $nextchild = OrganizationalStructureValues::find('parent_id = ' . $editval);
        if (count($nextchild) > 0) {
            foreach ($nextchild as $chld) {
                $rcpathname = SearchController::_getRecursiveparent($chld->id, $rcpathname);
            }
        }
        return $rcpathname;
    }

    public function orgmasValueSearchAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        try {
            if ($this->request->isPost()) {
                $searchText = $this->request->getPost('search');
                $editval = $this->request->getPost('editval');

//               
                $cyclenode = SearchController::_getRecursiveCycle($editval);
                $name = OrganizationalStructureValues::findfirst('id = ' . $cyclenode);
//                print_r($cyclenode); exit;
                $getValidMovableMasNodes = SearchController::_getRecursiveparent($cyclenode);
                //First find whether there is any record type with the ID provided.
//                print_r(implode(',', $getValidMovableMasNodes)); exit;
                $orgMasJsonArr = array();
                $ids = "";


                $orgmasdets = OrganizationalStructureValues::find(array(
                            "name LIKE '%$searchText%' and id != $editval  and id IN (" . implode(',', $getValidMovableMasNodes) . ")"
                ));


                foreach ($orgmasdets as $orgmasdet) {
                    $dview = $this->findorgvalparent('', $orgmasdet->id);
                    $orgMasJsonArr[] = array(
                        'id' => $orgmasdet->id,
                        'name' => $dview//$orgmasdet->name 
                    );
                    //print_r($dview);
                }
                // exit;
                if ($orgMasJsonArr) {
                    $orgmasjson = json_encode($orgMasJsonArr);
                    header('Content-Type: application/json');
                    echo $orgmasjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($orgmasdets->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function findorgmasparent($space = '', $id, $choutput = '', $temp = '') {
        $choutput.=$space . SearchController::getorgmasname($id) . $temp;
        $parentid = OrganizationalStructureMaster::findFirstById($id)->parent_id;
        //foreach ($chld as $chl) {
        if (!empty($parentid))
            $choutput = SearchController::findorgmasparent('&nbsp;>>&nbsp;', $parentid, $choutput, '');
        //}

        return $choutput;
    }

    public function getorgmasname($id) {
        $name = OrganizationalStructureMaster::findFirstById($id)->name;
        if (!empty($name))
            return $name;
    }

    public function findorgvalparent($space = '', $id, $choutput = '', $temp = '') {
        $choutput.=$space . SearchController::getorgvalname($id) . $temp;
        $parentid = OrganizationalStructureValues::findFirstById($id)->parent_id;
        //foreach ($chld as $chl) {
        if (!empty($parentid))
            $choutput = SearchController::findorgvalparent('&nbsp;>>&nbsp;', $parentid, $choutput, '');
        //}

        return $choutput;
    }

    public function getorgvalname($id) {
        $name = OrganizationalStructureValues::findFirstById($id)->name;
        if (!empty($name))
            return $name;
    }

    public function stockcustAutocmpltAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            if ($this->request->isPost()) {
                $identity = $this->auth->getIdentity();
                $searchText = $this->request->getPost('query');
                $conditions = "(customer_name LIKE :searchtxt:)";

                $parameters = array(
                    "searchtxt" => "%" . $searchText . "%"
                );
                $customer = CustomerMaster::find(array(
                            $conditions,
                            "bind" => $parameters
                ));
//                print_r($material);
//                exit;
                $stfJsonArr = array();
                foreach ($customer as $cust) {
                    $stfJsonArr[] = array(
                        'id' => $cust->id,
                        'name' => $cust->customer_name . "(" . $cust->format_id . ")"
                    );
                }

                if ($stfJsonArr) {
                    $stfjson = json_encode($stfJsonArr);
                    header('Content-Type: application/json');
                    echo $stfjson;
                    exit;
                } else {
                    $error = '';
                    foreach ($staffs->getMessages() as $message) {
                        $error .= $message;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function autocmpltforChartAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            $searchText = $this->request->get('q');
            $node_id = $this->request->get('id');
            $res1 = ControllerBase::buildStudentQuery($node_id);
            $conditions[] = "(stuinfo.Student_Name LIKE  '%" . $searchText . "%'"
                    . " OR stuinfo.loginid LIKE '%" . $searchText . "%')";
            $condition = (count($conditions) > 0) ? implode(' and ', $conditions) : '';

            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status, stuinfo.Phone, stuinfo.f_phone_no_status,
                    stuinfo.f_phone_no_status,stuinfo.f_phone_no,
                        stuinfo.m_phone_no_status,stuinfo.m_phone_no,
                        stuinfo.g_phone_no_status,stuinfo.g_phone,stuinfo.loginid,stuinfo.Student_Name '
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" and '
                    . '(' . implode(' and ', $res1) . ') and ' . $condition;

            $stumapdet = $this->modelsManager->executeQuery($stuquery);

            $stuJsonArr = array();
            foreach ($stumapdet as $student) {
                $stuJsonArr[] = array(
                    'id' => $student->student_info_id,
                    'name' => $student->Student_Name . " (" . $student->loginid . ")"
                );
            }
            if ($stuJsonArr) {
                $stujson = json_encode($stuJsonArr);
                header('Content-Type: application/json');
                echo $stujson;
                exit;
            } else {
                $error = '';
                foreach ($students->getMessages() as $message) {
                    $error .= $message;
                }
                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                exit;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function cmprsnChartStudAutoCompleteAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            $searchText = $this->request->get('q');
            $nodes = $this->request->get('nodes');
            $conditions[] = "(stuinfo.Student_Name LIKE  '%" . $searchText . "%'"
                    . " OR stuinfo.loginid LIKE '%" . $searchText . "%')";
            $condition = (count($conditions) > 0) ? implode(' and ', $conditions) : '';
            $res = ControllerBase::buildStudentQuery($nodes);
            $stuquery = 'SELECT stuinfo.loginid,stumap.id,stumap.student_info_id,stuinfo.Student_Name,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status'
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" and (' . implode(' or ', $res)
                    . ') and ' . $condition . 'ORDER BY stuinfo.Admission_no ASC';
            $students = $this->modelsManager->executeQuery($stuquery);
            $stuJsonArr = array();
            foreach ($students as $student) {
                $stuJsonArr[] = array(
                    'id' => $student->student_info_id,
                    'name' => $student->Student_Name . " (" . $student->loginid . ")"
                );
            }
            if ($stuJsonArr) {
                $stujson = json_encode($stuJsonArr);
                header('Content-Type: application/json');
                echo $stujson;
                exit;
            } else {
                $error = '';
                foreach ($students->getMessages() as $message) {
                    $error .= $message;
                }
                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                exit;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function studAutoCmpltforProgressChartAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            $searchText = $this->request->get('query');
            $currentyearid = $this->request->get('currentyearid');
            $res = ControllerBase::buildStudentQuery(implode(',', $currentyearid));
            $stuquery = 'SELECT stuinfo.loginid,stumap.id,stumap.student_info_id,stuinfo.Student_Name,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status'
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" and (' . implode(' or ', $res)
                    . ') ORDER BY stuinfo.Admission_no ASC';
            $students = $this->modelsManager->executeQuery($stuquery);
            $stuJsonArr = array();
            foreach ($students as $student) {
                $stuJsonArr[] = array(
                    'id' => $student->student_info_id,
                    'name' => $student->Student_Name . " (" . $student->loginid . ")"
                );
            }
            if ($stuJsonArr) {
                $stujson = json_encode($stuJsonArr);
                header('Content-Type: application/json');
                echo $stujson;
                exit;
            } else {
                $error = '';
                foreach ($students->getMessages() as $message) {
                    $error .= $message;
                }
                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                exit;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function allEventsUserSearchAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $stuJsonArr = array();
        $searchText = $this->request->get('q');
        $nodes = OrganizationalStructureMaster::find('module IN("structural","StudentCycleNode","Department")');
        $clsrooms = ClassroomMaster::find("name LIKE  '%" . $searchText . "%'");
        $stuquery = 'SELECT stuinfo.loginid,stumap.id,stumap.student_info_id,stuinfo.Student_Name,stuinfo.id'
                . ' FROM StudentMapping stumap INNER JOIN'
                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" '
                . ' and (stuinfo.Student_Name LIKE  "%' . $searchText . '%"'
                . ' OR stuinfo.loginid LIKE "%' . $searchText . '%")';
        $students = $this->modelsManager->executeQuery($stuquery);
        $staffs = StaffInfo::find("(Staff_Name LIKE  '%" . $searchText . "%'"
                        . " OR loginid LIKE '%" . $searchText . "%') "
                        . " AND (status IN('Appointed','Relieving initiated') OR loginid LIKE 'Superadmin' OR loginid LIKE 'Siteadmin')");

        if ($searchText == 'staff'):
            $stfs = StaffInfo::find("(status IN('Appointed','Relieving initiated') OR loginid LIKE 'Superadmin' OR loginid LIKE 'Siteadmin')");
            if (count($stfs) > 0):
                foreach ($stfs as $stf) {
                    $stuJsonArr[] = array(
                        'id' => 'staff_' . $stf->id,
                        'name' => $stf->Staff_Name . " (" . $stf->loginid . ")"
                    );
                }
            endif;
        endif;

        if ($searchText == 'student' || $searchText == 'parent'):
            $stud = 'SELECT stuinfo.loginid,stumap.id,stumap.student_info_id,stuinfo.Student_Name,stuinfo.id'
                    . ' FROM StudentMapping stumap INNER JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" ';
            $allstudents = $this->modelsManager->executeQuery($stud);
            if (count($allstudents) > 0) {
                foreach ($allstudents as $allstud) {
                    if ($searchText == 'student') {
                        $stuJsonArr[] = array(
                            'id' => 'student_' . $allstud->id,
                            'name' => $allstud->Student_Name . " (" . $allstud->loginid . ")"
                        );
                    }
                    if ($searchText == 'parent') {
                        $stuJsonArr[] = array(
                            'id' => 'parent_' . $allstud->id,
                            'name' => $allstud->Student_Name . "'s Parent (" . $allstud->parent_loginid . ")"
                        );
                    }
                }
            }
        endif;

        $masids = array();
        if (count($nodes) > 0) {
            foreach ($nodes as $node) {
                $masids[] = $node->id;
            }
        }
        if (count($masids) > 0) {
            $org_values = OrganizationalStructureValues::find('org_master_id IN (' . implode(',', $masids) . ') and name LIKE "%' . $searchText . '%" and status = "C"');
            if (count($org_values) > 0) {
                foreach ($org_values as $org) {
                    $mas = OrganizationalStructureMaster::findFirstById($org->org_master_id);
                    if ($mas->module == 'structural') {
                        $stuJsonArr[] = array(
                            'id' => 'school_' . $org->id,
                            'name' => $org->name
                        );
                    } else {
                        $stuJsonArr[] = array(
                            'id' => 'node_' . $org->id,
                            'name' => $org->name
                        );
                    }
                }
            }
        }
        if (count($clsrooms) > 0) {
            foreach ($clsrooms as $clsroom) {
                $stuJsonArr[] = array(
                    'id' => 'class_' . $clsroom->id,
                    'name' => $clsroom->name
                );
            }
        }
        if (count($students) > 0) {
            foreach ($students as $student) {
                $stuJsonArr[] = array(
                    'id' => 'student_' . $student->id,
                    'name' => $student->Student_Name . " (" . $student->loginid . ")"
                );
                $stuJsonArr[] = array(
                    'id' => 'parent_' . $student->id,
                    'name' => $student->Student_Name . "'s Parent (" . $student->parent_loginid . ")"
                );
            }
        }
        if (count($staffs) > 0) {
            foreach ($staffs as $staff) {
                $stuJsonArr[] = array(
                    'id' => 'staff_' . $staff->id,
                    'name' => $staff->Staff_Name . " (" . $staff->loginid . ")"
                );
            }
        }

        if ($stuJsonArr) {
            $stujson = json_encode(array_slice($stuJsonArr, 0, 10));
            header('Content-Type: application/json');
            echo $stujson;
            exit;
        } else {
            $error = '';
            foreach ($students->getMessages() as $message) {
                $error .= $message;
            }
            foreach ($clsrooms->getMessages() as $message) {
                $error .= $message;
            }
            foreach ($staffs->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function taskUserSearchAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $searchText = $this->request->get('search');
        $staffs = StaffInfo::find(""
                        . "(Staff_Name LIKE  '%" . $searchText . "%'"
                        . " OR loginid LIKE '%" . $searchText . "%') "
                        . " AND (status IN('Appointed','Relieving initiated') OR loginid LIKE 'Superadmin' OR loginid LIKE 'Siteadmin')");
        $stuJsonArr = array();
        if (count($staffs) > 0) {
            foreach ($staffs as $staff) {
                $stuJsonArr[] = array(
                    'id' => 'staff_' . $staff->id,
                    'name' => $staff->Staff_Name . " (" . $staff->loginid . ")"
                );
            }
        }
        if ($stuJsonArr) {
            $stujson = json_encode($stuJsonArr);
            header('Content-Type: application/json');
            echo $stujson;
            exit;
        } else {
            $error = '';
            foreach ($staffs->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

    public function getAllUsersforSearchboxAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $stuJsonArr = array();
        $searchText = $this->request->get('q');
        $stuquery = 'SELECT stuinfo.loginid,stumap.id,stumap.student_info_id,stuinfo.Student_Name,stuinfo.id'
                . ' FROM StudentMapping stumap INNER JOIN'
                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" '
                . ' and (stuinfo.Student_Name LIKE  "%' . $searchText . '%"'
                . ' OR stuinfo.loginid LIKE "%' . $searchText . '%")';
        $students = $this->modelsManager->executeQuery($stuquery);
        $staffs = StaffInfo::find("(Staff_Name LIKE  '%" . $searchText . "%'"
                        . " OR loginid LIKE '%" . $searchText . "%') "
                        . " AND (status IN('Appointed','Relieving initiated') OR loginid LIKE 'Superadmin' OR loginid LIKE 'Siteadmin')");

        if (count($students) > 0) {
            foreach ($students as $student) {
                $stuJsonArr[] = array(
                    'id' => 'student_' . $student->id,
                    'name' => $student->Student_Name . " (" . $student->loginid . ")"
                );
                $stuJsonArr[] = array(
                    'id' => 'parent_' . $student->id,
                    'name' => $student->Student_Name . "'s Parent (" . $student->parent_loginid . ")"
                );
            }
        }
        if (count($staffs) > 0) {
            foreach ($staffs as $staff) {
                $stuJsonArr[] = array(
                    'id' => 'staff_' . $staff->id,
                    'name' => $staff->Staff_Name . " (" . $staff->loginid . ")"
                );
            }
        }

        if ($stuJsonArr) {
            $stujson = json_encode(array_slice($stuJsonArr, 0, 10));
            header('Content-Type: application/json');
            echo $stujson;
            exit;
        } else {
            $error = '';
            foreach ($students->getMessages() as $message) {
                $error .= $message;
            }
            foreach ($staffs->getMessages() as $message) {
                $error .= $message;
            }
            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            exit;
        }
    }

}
