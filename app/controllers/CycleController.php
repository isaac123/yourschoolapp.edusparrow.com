<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class CycleController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->tag->prependTitle("Student Cycle | ");
        $this->assets->addCss('css/circularmenu/circlemenu.css');
        $this->assets->addCss('css/circularmenu/menu.css');
        $this->assets->addCss('css/circularmenu/share.css');
        $this->assets->addCss('css/circularmenu/share-1.css');
        $this->assets->addCss('css/circularmenu/tooltip.css');

        $this->assets->addJs("js/circularmenu/TweenMax.min.js");
        $this->assets->addJs("js/circularmenu/menu.js");
        $this->assets->addJs("js/circularmenu/share.js");
        $this->assets->addJs("js/circularmenu/share-1.js");
    }

    public function svgAction() {
             $time =SCRIPTLT;
        $this->tag->prependTitle("Student Cycle | ");
        $this->view->identity = $this->auth->getIdentity();
        $this->tag->prependTitle("Student Profile | ");

        $this->view->studentid = $stuuserid = ($this->request->get('studentid')) ? ($this->request->get('studentid')) : '';
        
        $this->assets->addCss('css/pageexpand/component.css?'.$time);
        $this->assets->addCss('css/pageexpand/normalize.css');
        $this->assets->addCss('css/circularmenu/component.css');
        $this->assets->addCss('css/circularmenu/menu.css');
        $this->assets->addCss('css/circularmenu/share.css');
        $this->assets->addCss('css/calendar.css');
        $this->assets->addCss('css/tooltip-classic.css'); //twoway cycle
        $this->assets->addCss('css/report.css');
//        $this->assets->addCss('css/edustyle.css');
//        
//        $this->assets->addJs('js/jquery-ui.js');
        $this->assets->addJs("js/extra/underscore/underscore-min.js");
        $this->assets->addJs("js/extra/circularmenu/TweenMax.min.js");
        $this->assets->addJs("js/extra/circularmenu/menu.js");
        $this->assets->addJs("js/extra/circularmenu/share.js");

        $this->assets->addJs("js/extra/cycle/calendar.js");
        $this->assets->addJs("js/extra/cycle/app.js");
        
        $this->assets->addJs('js/appscripts/spacetree/spacetreeutils.js');
        $this->assets->addJs("js/appscripts/spacetree/feespacetree.js");

        $this->assets->addJs("js/appscripts/cycle/studentcycle.js");
        $this->assets->addJs("js/appscripts/cycle/staffcycle.js");

        $this->assets->addJs('js/appscripts/application/application.js');
        $this->assets->addJs('js/appscripts/application/manageApplication.js');
        $this->assets->addJs('js/appscripts/approval/approval.js');
        $this->assets->addJs('js/appscripts/admission/admission.js');

        //classroom
        $this->assets->addJs("js/appscripts/assigning/assignSection.js");
        $this->assets->addJs('js/appscripts/classroom/classroom.js');
        $this->assets->addJs("js/appscripts/attendance/attendance.js");
        $this->assets->addJs("js/appscripts/exam/exam.js");
        $this->assets->addJs("js/appscripts/mark/mark.js");
        $this->assets->addJs("js/appscripts/assignment/assignment.js");
        $this->assets->addJs("js/appscripts/rating/rating.js");
        $this->assets->addJs('js/appscripts/studentprofile/studentprofile.js');
//        $this->assets->addJs('js/appscripts/video/video.js');
        //classroom admin        
        $this->assets->addJs("js/appscripts/attendancesettings/settings.js");
        $this->assets->addJs("js/appscripts/classroomadmin/classroomadmin.js");
        //fee
        $this->assets->addJs('js/appscripts/feeassigning/feeassigning.js');
        $this->assets->addJs("js/appscripts/feepayment/feepayment.js");
        $this->assets->addJs("js/appscripts/voucher/voucher.js");
        $this->assets->addJs('js/appscripts/feedemand/feedemand.js');
        $this->assets->addJs('js/appscripts/fee/fee.js');
        $this->assets->addJs('js/appscripts/fee/managefee.js');
        $this->assets->addJs('js/appscripts/fee/feeclub.js');
        //promotion
        $this->assets->addJs("js/appscripts/evaluation/evaluation.js");
        $this->assets->addJs('js/appscripts/promotion/promotion.js');
        //relieving
        $this->assets->addJs("js/appscripts/relieving/relieving.js");
        $this->assets->addJs("js/appscripts/search/search.js");
        //profile
        $this->assets->addJs('js/appscripts/studenthomepage/dashboard.js');
        $this->assets->addJs('js/appscripts/studenthomepage/highcharts.js');
        $this->assets->addJs('js/appscripts/studenthomepage/exporting.js');
        $this->assets->addJs('js/appscripts/studenthomepage/chartStyle.js');

        //profile settings
        $this->assets->addJs('js/appscripts/communicate/communicate.js');
        $this->assets->addJs("js/appscripts/assigning/assign.js");
        $this->assets->addJs('js/appscripts/appointment/appointment.js');
    }

    public function staffsvgAction() {


        if ($this->request->get('staffid')) {
            $this->view->staffId = $this->request->get('staffid');
        } else {
            $this->view->staffId = '';
        }

        $this->view->identity = $this->auth->getIdentity();
        $this->tag->prependTitle("Staff Cycle | ");
//        $this->assets->addCss('css/edustyle.css');
        
        $this->assets->addCss('css/circularmenu/staffmenu.css');
        $this->assets->addCss('css/pageexpand/component.css');
        $this->assets->addCss('css/tooltip-classic.css'); //twoway cycle
        
        $this->assets->addJs("js/extra/underscore/underscore-min.js");
        $this->assets->addJs("js/extra/circularmenu/TweenMax.min.js");
        $this->assets->addJs("js/extra/circularmenu/staffmenu.js");
        $this->assets->addJs("js/extra/jquery.battatech.excelexport.min.js");
        
        $this->assets->addJs('js/appscripts/spacetree/spacetreeutils.js');
        $this->assets->addJs('js/appscripts/appointment/appointment.js');

        $this->assets->addJs("js/appscripts/cycle/studentcycle.js");
        $this->assets->addJs("js/appscripts/cycle/staffcycle.js");
        //assigning

        $this->assets->addJs("js/appscripts/assigning/assign.js");
        $this->assets->addJs("js/appscripts/staffrating/staffrating.js");

        //relieving
        $this->assets->addJs("js/appscripts/relieving/relieving.js");
        $this->assets->addJs("js/appscripts/search/search.js");

        //attendance

        $this->assets->addJs("js/appscripts/attendance/attendance.js");
        $this->assets->addJs("js/appscripts/attendancesettings/settings.js");
        $this->assets->addJs("js/appscripts/attendancesettings/staffsettings.js");
        $this->assets->addJs('js/appscripts/approval/approval.js');

        //payroll

        $this->assets->addJs('js/appscripts/payroll/salary.js');
        $this->assets->addJs('js/appscripts/payroll/advance.js');
        $this->assets->addJs('js/appscripts/payroll/payroll.js');
        $this->assets->addJs("js/appscripts/voucher/voucher.js");
        //profle
        $this->assets->addJs('js/appscripts/studenthomepage/dashboard.js');
        $this->assets->addJs('js/appscripts/staff/staffSearch.js');
//        $this->assets->addJs('js/appscripts/event/event.js');
    }

    public function financesvgAction() {

        $this->tag->prependTitle("Finance Cycle | ");

        $this->view->identity = $this->auth->getIdentity();

        $this->assets->addCss('css/pageexpand/component.css');
        $this->assets->addCss('css/pageexpand/normalize.css');

        $this->assets->addCss('css/circularmenu/financemenu.css');
        $this->assets->addJs("js/circularmenu/TweenMax.min.js");
        $this->assets->addJs("js/circularmenu/financemenu.js");
        $this->assets->addJs("js/financecycle/financecycle.js");

        //What i added
        $this->assets->addJs("js/financereport/financereport.js");
        $this->assets->addJs("js/transaction/transaction.js");
        $this->assets->addJs("js/accountssettings/accountssettings.js");

        //Voucher
        $this->assets->addJs("js/voucher/voucher.js");
        $this->assets->addJs('js/appscripts/approval.js');
        $this->assets->addJs('js/payroll/advance.js');
        $this->assets->addJs('js/payroll/payroll.js');
        $this->assets->addJs("js/feepayment/feepayment.js");

        //fee-demand
        $this->assets->addJs('js/feedemand/feedemand.js');

        $this->view->financeyear = $financeyear = FinanceYear::findfirst('status = "c"');

        //fee collected so far
        $feepaid = StudentFeeTable::findfirst(array('created_date >= ' . $financeyear->start_date . ' and created_date <= ' . $financeyear->end_date,
                    "columns" => "SUM(paidamount) as fee"));

        //fee collected so far
        $salarypaid = StaffSalaryPermonth::findfirst(array('created_date >= ' . $financeyear->start_date . ' and created_date <= ' . $financeyear->end_date,
                    "columns" => "SUM(paidamount) as salary"));

        $this->view->feecollected = $feepaid->fee;
        $this->view->salarypaid = $salarypaid->salary;

        if ($feepaid->fee == 0) {
            $this->view->feecollected = 0;
        }

        if ($salarypaid->salary == 0) {
            $this->view->salarypaid = 0;
        }

        $masterid = OrganizationalStructureMaster::find('module="Ledger"');
        foreach ($masterid as $value) {
            $orgids[] = $value->id;
        }
        $cashledgerdet = OrganizationalStructureValues::findFirst('name LIKE "Cash"  and org_master_id IN (' . implode(',', $orgids) . ')');
        $this->view->ledger_id = $ledger_id = $cashledgerdet->id;
        $this->view->sdat = $start_date = strtotime(date('d-m-Y') . " 00:00:00");
        $this->view->edat = $end_date = strtotime(date('d-m-Y') . " 23:59:59");
        //Get the list of ledgers
        $prev_date = strtotime(date('Y-m-d', strtotime(date('d-m-Y', $start_date) . ' -1 day')) . " 23:59:59");
        $ledger = OrganizationalStructureValues::findFirstById($ledger_id);
        $openingbalance = ControllerBase::_getBalnceForDate($prev_date, $ledger_id);
        $closingbalance = ControllerBase::_getBalnceForDate($end_date, $ledger_id);
        $this->view->openingbalance = number_format(abs($openingbalance[0]), 2, '.', '') . ' ' . '(' . $openingbalance[1] . ')';
        $this->view->closingbalance = number_format(abs($closingbalance[0]), 2, '.', '') . ' ' . '(' . $closingbalance[1] . ')';
//        $this->view->row = $row;
    }

    public function inventorysvgAction() {


        if ($this->request->get('staffid')) {
            $this->view->staffId = $this->request->get('staffid');
        } else {
            $this->view->staffId = '';
        }

        $this->view->identity = $this->auth->getIdentity();
        $this->tag->prependTitle("Inventory Cycle | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/spacetree/spacetreeutils.js');
        $this->assets->addCss('css/pageexpand/component.css');
//        $this->assets->addCss('css/pageexpand/normalize.css');
        $this->assets->addCss('css/circularmenu/inventorymenu.css');
        $this->assets->addJs('js/appointment/appointment.js');

        $this->assets->addJs("js/circularmenu/TweenMax.min.js");
        $this->assets->addJs("js/circularmenu/inventorymenu.js");

        $this->assets->addJs("js/cycle/studentcycle.js");
        $this->assets->addJs("js/cycle/staffcycle.js");
        $this->assets->addJs("js/cycle/inventorycycle.js");
        //assigning

        $this->assets->addJs("js/assigning/assign.js");
        $this->assets->addJs("js/staffrating/staffrating.js");

        //relieving
        $this->assets->addJs("js/appscripts/relieving.js");
        $this->assets->addJs("js/search/search.js");

        //attendance

        $this->assets->addJs("js/attendance/attendance.js");
        $this->assets->addJs("js/attendancesettings/settings.js");
        $this->assets->addJs("js/attendancesettings/staffsettings.js");
        $this->assets->addJs('js/appscripts/approval.js');

        //payroll

        $this->assets->addJs('js/payroll/salary.js');
        $this->assets->addJs('js/payroll/advance.js');
//        $this->assets->addJs('js/payroll/advSplView.js');
//        $this->assets->addJs('js/payroll/advSplitup.js');
        $this->assets->addJs('js/payroll/payroll.js');
        $this->assets->addJs("js/voucher/voucher.js");
        $this->assets->addJs('js/appscripts/approval.js');
        //profle
        $this->assets->addJs('js/studenthomepage/dashboard.js');
        $this->assets->addJs("js/underscore/underscore-min.js");

        $this->assets->addJs('js/event/event.js');
        //inventory
        $this->assets->addJs('js/inventory/inventory.js');
        $this->assets->addJs('js/inventory/addmaterial.js');
        $this->assets->addJs('js/vendor/vendor.js');
        $this->assets->addJs('js/movementin/movementin.js');
        $this->assets->addJs('js/movementout/movementout.js');
        $this->assets->addJs('js/movementout/searchmaterial.js');
    }

    public function loadStudentDetailsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $textArr = array();
            $stuId = $this->request->getPost('stuId');
            $current_acd_yr = Controllerbase::get_current_academic_year();
            $subtxt = $hsubtxt = '';
            $subtxtp = array();
            $stuinfo = StudentInfo::findFirstById($stuId);
            $stuAcdinfo = StudentMapping::findFirst('student_info_id=' . $stuId);
            $stuHisinfo = StudentHistory::find('student_info_id=' . $stuId);
            $stuRelievinfo = StudentRelievingMaster::findFirst('student_id=' . $stuId);
            if ($stuinfo) {

//          $promotehistory = '<tspan x="80" y="0" fill="#32B196" font-family="BebasNeueRegular" >' .
//                                $stuacd->status . ' ( ' . $acdname . ' ) ' . '</tspan><tspan x="80" y="20" fill="#32B196" font-family="BebasNeueRegular" >'
//                                . $divname . '  : ' . $divvalp . ',' . implode(',', $subtxtp) . '</tspan>';
//                $feependings = StudentFeeTable::findFirst(
//                                array('columns' => 'SUM (paidamount) as paid , sum(cancel_comment) as cancel',
//                                    'student_id = ' . $stuId . ' and academic_year_id = ' . $current_acd_yr));
//                 $psql = 'SELECT SUM (stufee.paidamount) as paid , SUM (stufee.concession_amt) as concession , sum(stufeecan.cancelled_amount) as cancel '
//                    . 'FROM FeeMaster feemas, StudentFeeTable stufee , StudentFeeCancel stufeecan'
//                    . 'WHERE stufeecan.status = "Approved" AND '
//                    . 'stufee.student_id ='.$stuId.' AND '
//                    .  "(FIND_IN_SET('$current_acd_yr',REPLACE(node_id, '-', ',')) >0) AND "
//                    . 'stufee.fee_master_id = feemas.id';

                $psql = ' SELECT SUM (stufee.paidamount) as paid , SUM (stufee.concession_amt) as concession , SUM(stufeecan.cancelled_amount) as cancel'
                        . ' FROM StudentFeeTable stufee'
                        . ' INNER JOIN FeeMaster AS feemas ON stufee.fee_master_id = feemas.id'
                        . ' LEFT JOIN StudentFeeCancel stufeecan ON stufee.id = stufeecan.student_fee_id'
                        . ' AND stufeecan.status = "Approved"'
                        . ' WHERE stufee.student_id =' . $stuId
                        . " AND (FIND_IN_SET( '$current_acd_yr->id', REPLACE( node_id, '-', ',' ) ) >0)";

//echo $psql;exit;
                $feependings = $this->modelsManager->executeQuery($psql);

//                  echo $feependings[0]['paid'] .'-paid';
//                  echo $feependings[0]['concession'] . '-conss';
//                  echo $feependings[0]['cancel'] .'-can';
                $paidamt = $feependings[0]['paid'] ? $feependings[0]['paid'] : '0.00';
                $concess = $feependings[0]['concession'] ? $feependings[0]['concession'] : '0.00';
                $cancel = $feependings[0]['cancel'] ? $feependings[0]['cancel'] : '0.00';
//                  print_r($feependings);
//                  exit;

                $textArr['stuAdmissionTxt'] = '<tspan class="share-item" x="0" y="0" fill="#00BCD4" '
                        . 'font-family="BebasNeueRegular" > Admitted on : ' . date('d-m-Y', $stuinfo->Date_of_Joining) . '  </tspan>';
                $textArr['stuFeeTxt'] = '<tspan class="share-item" x="8" y="0" fill="#98AAA9" '
                        . 'font-family="BebasNeueRegular" >paid : ' . $paidamt . '  </tspan>'
                        . '<tspan class="share-item" x="28" y="20" fill="#98AAA9" '
                        . 'font-family="BebasNeueRegular" >concession : ' . $concess . '  </tspan>'
                        . '<tspan class="share-item" x="23" y="40" fill="#98AAA9" '
                        . 'font-family="BebasNeueRegular" >canceled  : ' . $cancel . '  </tspan>';

//                        . ' Total concession amount : ' . $feependings[0]['concession'] ? $feependings[0]['concession'] : '0.00' . ' '
//                        . ' Total canceled amount : ' . $feependings[0]['cancel'] ? $feependings[0]['cancel'] : '0.00' . ' '
//                        . ' </tspan>' ;

                $studentbranch = explode(',', $stuAcdinfo->aggregate_key);
                $y = 0;
                foreach ($studentbranch as $aggreval) {
                    $orgval = OrganizationalStructureValues::findFirstById($aggreval);
                    $orgMaster = OrganizationalStructureMaster::findFirstById($orgval->org_master_id);
                    $subtxt .= '<tspan x="0" y="' . $y . '" fill="#32B196" font-family="BebasNeueRegular" >'
                            . $orgMaster->name . ' : ' . $orgval->name . '</tspan>';
                    $y+=20;
                }
                foreach ($stuHisinfo as $hisaggreval) {

                    $hstudentbranch = explode(',', $hisaggreval->aggregate_key);
                    $hy = 0;
                    foreach ($hstudentbranch as $haggreval) {
                        $horgval = OrganizationalStructureValues::findFirstById($haggreval);
                        $horgMaster = OrganizationalStructureMaster::findFirstById($horgval->org_master_id);
                        $hsubtxt .= '<tspan x="0" y="' . $hy . '" fill="#32B196" font-family="BebasNeueRegular" >'
                                . $horgMaster->name . ' : ' . $horgval->name . '</tspan>';
                        $hy+=20;
                    }
                }


                $stuRelivdate = isset($stuRelievinfo->scl_left_date) ? date('d-m-Y', $stuRelievinfo->scl_left_date) : '';

                $stuStatusCntnt = $stuAcdinfo->status ? (($stuAcdinfo->status == 'Relieved') ?
                                $stuAcdinfo->status . ' on ' . $stuRelivdate : $stuAcdinfo->status ) : '';
                $stu_status = $stuStatusCntnt ? '<tspan x="0" y="0"'
                        . ' fill="#ED7774" font-family="BebasNeueRegular" >' . $stuStatusCntnt . '</tspan>' : '';

                $textArr['stuEvalTxt'] = $stu_status;

//                $textArr['stuEvalTxt'] = (count($stuHisinfo) > 1) ? $stu_status : '<tspan x="0" y="0"'
//                        . ' fill="#ED7774" font-family="BebasNeueRegular" >New Joining</tspan>';

                $textArr['stuAcdTxt'] = $subtxt;

                $textArr['Studentname'] = '<tspan x="-5" y="0" fill="#424242" font-family="BebasNeueRegular" text-anchor="middle" title="' . $stuinfo->Student_Name . '">
                       ' . (strlen($stuinfo->Student_Name) > 15 ? substr($stuinfo->Student_Name, 0, 15) . "..." : $stuinfo->Student_Name) . '  </tspan>';
                $textArr['studentLogin'] = '<tspan x="6" y="0"  fill="#424242" font-family="BebasNeueBook" >' .
                        $stuinfo->loginid . '  </tspan>';
                $textArr['stupic'] = '<span class="mini-stat-icon s-pic1" style ="background: #fff !important;position: absolute;'
                        . ' top: 340px;  left: 365px;">'
                        . $this->tag->image(array(
                            "src" => $stuinfo->photo ? (FILES_URI . $stuinfo->photo) : ('images/user.jpg'),
                            'style' => 'border-radius:50%',
                            'width' => '90px',
                            'height' => '90px')) . '</span>';
            }
            echo json_encode($textArr);
            exit;
        }
    }

    public function loadStaffDetailsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $textArr = array();
            $stuinfo = StaffInfo::findFirstById($this->request->getPost('stfId'));
            $stuGeninfo = StaffGeneralMaster::findFirstByStaffId($stuinfo->id);
            if ($stuinfo) {
//                $feependings = StaffSalaryPermonth::findFirst(
//                                array('columns' => ' (paidamount) as paid ',
//                                    'staffid = ' . $stfId,
//                                    'order' => 'id DESC'));
//                $textArr['stfPayrollTxt'] = '<tspan x="5" y="0" fill="#98AAA9" font-family="BebasNeueRegular" >Total paid amount : '
//                        . $feependings->paid . '10000  </tspan>';

                $textArr['stfAppointTxt'] = '<tspan x="-5" y="0" fill="#00BCD4" font-family="BebasNeueRegular" > Appointed on : '
                        . date('d-m-Y', $stuinfo->Date_of_Joining) . '  </tspan>';

                $d_start = new DateTime(date('Y-m-d', $stuinfo->Date_of_Joining));
                $d_end = new DateTime(date('Y-m-d', (time()))); //$stuGeninfo->dor ? $stuGeninfo->dor :

                $diff = $d_start->diff($d_end);
                $textArr['stuEvalTxt'] = ($stuinfo->status == 'Appointed') ?
//                        '<tspan x="19" y="0" fill="#ED7774" font-family="BebasNeueRegular" >Previous experience : ' . ($stuGeninfo->experience ?$stuGeninfo->experience :'0 '). ' Year(s)</tspan>'
                        '<tspan x="19" y="0" fill="#ED7774" font-family="BebasNeueRegular" >Current experience : ' .
                        $diff->format('%y') . '  Year(s)</tspan>' : ( ($stuinfo->status == 'Relieving initiated') ?
                                '<tspan x="19" y="0" fill="#32B196" font-family="BebasNeueRegular" >' . $stuinfo->status . ' </tspan>' :
                                '<tspan x="19" y="0" fill="#32B196" font-family="BebasNeueRegular" > Relieved on ' . date('d-m-Y', $stuGeninfo->dor) . ' </tspan>' );

                $studentbranch = explode(',', $stuinfo->aggregate_key);
                foreach ($studentbranch as $aggreval) {
                    $orgval = OrganizationalStructureValues::findFirstById($aggreval);
                    $orgMaster = OrganizationalStructureMaster::findFirstById($orgval->org_master_id);
                    $txtdept = '<tspan x="53" y="0" fill="#32B196" font-family="BebasNeueRegular" >';
                    $txtdept .= $orgMaster->name . '  : ' . $orgval->name;
                    $txtdept .= ' </tspan>';
                }


                $textArr['stuAcdTxt'] = $txtdept;
                $textArr['Staffname'] = '<tspan x="-12" y="0" fill="#424242" font-family="BebasNeueRegular"  title="' . $stuinfo->Staff_Name . '">
                  ' . (strlen($stuinfo->Staff_Name) > 15 ? substr($stuinfo->Staff_Name, 0, 15) . "..." : $stuinfo->Staff_Name) . ' </tspan>';
                $textArr['staffLogin'] = '<tspan x="12" y="0"  fill="#424242" font-family="BebasNeueBook" >'
                        . $stuinfo->loginid . '  </tspan>';
                $textArr['stfpic'] = '<span class="mini-stat-icon s-pic1" style ="background: #fff !important;position: absolute; top: 340px;  left: 365px;">'
                        . $this->tag->image(array(
                            "src" => $stuGeninfo ? ($stuGeninfo->photo ? (FILES_URI . $stuGeninfo->photo) : ('images/user.jpg')) : ('images/user.jpg'),
                            'style' => 'border-radius:50%',
                            'width' => '90px',
                            'height' => '90px')) . '</span>';
            }
            echo json_encode($textArr);
            exit;
        }
    }

}

?>