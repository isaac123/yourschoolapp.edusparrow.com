<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class StaffRatingController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->tag->prependTitle("Evaluation | ");
        $this->assets->addCss('css/edustyle.css');
    }

    public function staffRatingSettingsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

//        $this->tag->prependTitle("Staff Rating Settings| ");
//        $this->assets->addJs("js/staffrating/staffrating.js");
        // $current_acd_year = ControllerBase::get_current_academic_year();
        $record = StaffRatingCategoryMaster::find();
        $this->view->records = $record;
    }

    public function updateStaffRatingCategoryAction() {
        $this->tag->prependTitle("Staff Rating Settings| ");
        //$this->assets->addJs("js/staffrating/staffrating.js");

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if ($this->request->isPost()) {
            $count = $this->request->getPost('count');
            // $current_acd_year = ControllerBase::get_current_academic_year();
            //echo $count;

            $err = 0;

            $hidden_count = $this->request->getPost('hidden_count');
            //echo $hidden_count;
            if ($hidden_count > 0) {
                $hidden_names = $this->request->getPost('hidden');

                for ($i = 0; $i < $hidden_count; $i++) {
                    //echo '<div class="alert alert-block alert-danger fade in">' . $hidden_names[$i] . '</div>';
                    $record_del = StaffRatingCategoryMaster::findfirst('category_name = "' . $hidden_names[$i] . '"');
                    if ($record_del->delete() == false) {

                        $err = 1;
                        $error = '';
                        foreach ($record_del->getMessages() as $message) {
                            $error .= $message;
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    }
                }
            }

            if ($err == 0) {
                for ($i = 0; $i < $count; $i++) {
                    $name_value_pair = $this->request->getPost("$i");
                    //echo $pair[0];


                    $record = StaffRatingCategoryMaster::findfirst('category_name = "' . $name_value_pair[0] . '"');

                    if (!($record)) {
                        //New entry
                        $rating_Category_master = new StaffRatingCategoryMaster();
                        $rating_Category_master->assign(array(
                            "category_name" => $name_value_pair[0],
                            "category_weightage" => $name_value_pair[1]));
                        if (!($rating_Category_master->save())) {
                            $error = '';
                            foreach ($rating_Category_master->getMessages() as $message) {
                                $error .= $message;
                                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            }
                        }
                        //echo "new";
                    } else {
                        $record->category_weightage = $name_value_pair[1];
                        $record->save();
                        //echo "update";
                    }
                }
            }

            $category_records = StaffRatingCategoryMaster::find();
            $this->view->category_records = $category_records;
            //$this->view->current_acd_year = $current_acd_year;
        }
    }

    public function updateStaffRatingLevelAction() {
        $this->tag->prependTitle("Staff Rating Settings| ");
        $this->assets->addJs("js/staffrating/staffrating.js");

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $error = "";

        if ($this->request->isPost()) {
            $count = $this->request->getPost('total_count');
            // $current_acd_year = ControllerBase::get_current_academic_year();

            $hidden_count = $this->request->getPost('hidden_count');
            //echo $hidden_count;
            if ($hidden_count > 0) {
                $hidden_names = $this->request->getPost('hidden');
                $names = $this->request->getPost('names');

                for ($i = 0; $i < $hidden_count; $i++) {

                    $masterrecord_del = StaffRatingCategoryMaster::findfirst('category_name = "' . $names[$i] . '"');

                    $record_del = StaffRatingCategoryValues::findfirst('rating_category = ' . $masterrecord_del->id . ' and rating_level_name = "' . $hidden_names[$i] . '"');

                    $staffrating_del = StaffRating::findFirst('rating_category = ' . $masterrecord_del->id . ' and rating_value =' . $record_del->id);

                    if ($staffrating_del->delete()) {


                        if ($record_del->delete() == false) {

                            //$error = '';
                            foreach ($record_del->getMessages() as $message) {
                                $error .= $message;
                                //echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            }
                        }
                    } else {
                        foreach ($staffrating_del->getMessages() as $message) {
                            $error .= $message;
                            //echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    }
                }
            }

            for ($i = 0; $i < $count; $i++) {
                $rating_level = $this->request->getPost("$i");
                //echo '<div class="alert alert-block alert-danger fade in">' . $rating_level[0] . $rating_level[1] . '</div>';

                $masterrecord = StaffRatingCategoryMaster::findfirst('category_name = "' . $rating_level[0] . '"');

                $record = StaffRatingCategoryValues::findfirst('rating_category = ' . $masterrecord->id . ' and rating_level_name = "' . $rating_level[1] . '"');


                if (!($record)) {
                    //New entry
                    $rating_Category_values = new StaffRatingCategoryValues();
                    $rating_Category_values->assign(array("rating_category" => $masterrecord->id,
                        "rating_level_name" => $rating_level[1],
                        "rating_level_value" => $rating_level[2]));
                    if (!($rating_Category_values->save())) {
                        //$error = '';
                        foreach ($rating_Category_values->getMessages() as $message) {
                            //$error .= $message;
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    }
                    //echo "new";
                } else {
                    $record->rating_level_value = $rating_level[2];
                    $record->save();
                    //echo "update";
                }
            }
            if ($error == "") {
                echo "Rating Level Added Succesfully";
            } else {
                echo $error;
            }
        }
    }

    public function addStaffRatingAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Add Staff Rating | ");
//        $this->assets->addJs("js/staffrating/staffrating.js");
        //$this->view->divname = ControllerBase::get_division_name_staff();
        //  $this->view->div_val = ControllerBase::get_division_values_staff(ControllerBase::get_current_academic_year());
    }

    public function loadSectionsByClassAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->classId = $this->request->getPost('classId');

            $this->view->subdivname = ControllerBase::get_sub_division_name_staff();
            $this->view->subdivVal = ControllerBase::get_sub_division_values_staff($this->view->classId);
        }
    }

    public function loadStaffRatingAction() {

        $this->tag->prependTitle("Add Staff Rating | ");
        $this->assets->addJs("js/staffrating/staffrating.js");

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_staff = 1;
        $this->view->display_category = 1;
        if ($this->request->isPost()) {


            //List of staff
//            $staff = StaffInfo::find('Department = "' . $classId . '" and Subdivision = "' .
//                            $subdivid . '"');
            $staff = StaffInfo::find();
            $category_records = StaffRatingCategoryMaster::find();
            $this->view->category_records = $category_records;
            $rating_value_records = array();

            if (count($category_records) > 0) {
                $i = 0;
                foreach ($category_records as $record) {
                    $rating_value_records[$i++] = array('id' => $record->id,
                        'values' => StaffRatingCategoryValues::find('rating_category = ' . $record->id));
                }
            } else {
                $this->view->display_category = 0;
            }

            $i = 0;
            if (count($staff) > 0) {
                foreach ($staff as $stf) {

                    //$rows[$i++] = array($stf->Staff_Name, $stf->id);
                    $rows[$i++] = array($stf->Staff_Name, $stf->id, $rating_id, $stf->appointment_no);
                    //echo '<div class="alert alert-block alert-danger fade in">' .$student->id. '</div>';
                }

                $this->view->rows = $rows;
                $this->view->rating_value_records = $rating_value_records;
                // $this->view->return_uri = $return_uri;
            } else {
                $this->view->display_staff = 0;
            }
        }
    }

    public function updateStaffRatingAction() {

        $this->tag->prependTitle("Add Staff Rating | ");
        $this->assets->addJs("js/staffrating/staffrating.js");

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            // $classId = $this->request->getPost('class_id');
            //   $subject_id = $this->request->getPost('subject_id');
            //  $cacdyr = AcademicYearMaster::findFirstByStatus('c')->id;
            $stud_id = $this->request->getPost('stud_id');
            $category_count = $this->request->getPost('category_count');
            $success = 1;

            for ($i = 0; $i < $category_count; $i++) {

                $rating = $this->request->getPost("$i");

                $staff_rating = StaffRating::findfirst('staff_id = ' . $stud_id . ' and rating_category = ' . $rating[0]);

                if (!$staff_rating) {
                    //New record
                    $StaffRating = new StaffRating();
                    $StaffRating->assign(array(
                        "staff_id" => $stud_id,
                        "rating_category" => $rating[0],
                        "rating_value" => $rating[1]
                    ));
                    $StaffRating->save();
                    if (!($StaffRating->save())) {
                        $success = 0;
                        $error = '';
                        foreach ($StaffRating->getMessages() as $message) {
                            $error .= $message;
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    }
                } else {
                    $staff_rating->rating_value = $rating[1];
                    $staff_rating->save();
                }
            }
            echo $success;
        }
    }

    public function viewAction() {

        $this->tag->prependTitle("View Staff Rating | ");
        $this->assets->addJs("js/staffrating/staffrating.js");


        $this->view->divname = ControllerBase::get_division_name_staff();
        $this->view->div_val = ControllerBase::get_division_values_staff(ControllerBase::get_current_academic_year());
    }

    public function viewStaffRatingAction() {

        $this->tag->prependTitle("View Staff Rating | ");
        $this->assets->addJs("js/staffrating/staffrating.js");

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_staff = 1;
        $this->view->display_category = 1;
        if ($this->request->isPost()) {
            $classId = $this->request->getPost('class_id');
            $classname = $this->request->getPost('class_name');
            $cacdyr = AcademicYearMaster::findFirstByStatus('c')->id;


            $subdivid = $this->request->getPost('subDivVal');
            $subdivname = $this->request->getPost('subdiv_name');


            //List of staff
            $staff = StaffInfo::find('Department = "' . $classId . '" and Subdivision = "' .
                            $subdivid . '"');
            $category_records = StaffRatingCategoryMaster::find('academic_year_id = ' . $cacdyr);
            $this->view->category_records = $category_records;

            if (count($category_records) > 0) {
                $i = 0;
                foreach ($category_records as $record) {
                    $rating_value_records[$i++] = array('id' => $record->id,
                        'values' => StaffRatingCategoryValues::find('rating_category = ' . $record->id));
                }
            } else {
                $this->view->display_category = 0;
            }

            $i = 0;
            if (count($staff) > 0) {
                foreach ($staff as $stf) {

                    $rows[$i++] = array($stf->Staff_Name, $stf->id, $classId, $subdivid, $cacdyr);
                    //echo '<div class="alert alert-block alert-danger fade in">' .$student->id. '</div>';
                }

                $this->view->rows = $rows;
                $this->view->rating_value_records = $rating_value_records;
            } else {
                $this->view->display_staff = 0;
            }
        }
    }

    public function staffEvaluationAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Class Activity | ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs("js/staffrating/staffrating.js");
    }

    public function loadAttendanceSettingsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function getAttendancePeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
            $periods = PeriodMaster::find('user_type LIKE "staff"');
            $arrayval = array();
            foreach ($periods as $period) {
                $nodes = explode('-', $period->node_id);
                array_walk($nodes, function (&$item, $key) {
                    $item = OrganizationalStructureValues::findFirstById($item)->name;
                });
                $node_name = implode('>>', $nodes);
                $arrayval[$node_name][] = array(
                    'id' => $period->id,
                    'node_id' => $period->node_id,
                    'user_type' => $period->user_type,
                    'period' => $period->period,
                    'start_time' => date('H:i',$period->start_time),
                    'end_time' => date('H:i',$period->end_time)
                );
            }
            $this->view->arrayval = $arrayval;
        }
    }

    public function assignPeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->node_id = $this->request->getPost('node_id');
    }

    public function addPeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {
            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'aggregate' && $value) {
                $params['aggregateids'][] = $value;
            } else {

                $params[$key] = $value;
            }
        }
        $ids = explode('-', $params['node_id']);
        $periods = PeriodMaster::findFirst('LOCATE(node_id ,"' . $params['node_id'] . '")' . ' and period = "' . $params['period'] . '"'
                        . ' and start_time=' . $params['start_time'] . ' and end_time = ' . $params['end_time']) ?
                PeriodMaster::findFirst('LOCATE(node_id ,"' . $params['node_id'] . '")' . ' and period = "' . $params['period'] . '"'
                        . ' and start_time=' . $params['start_time'] . ' and end_time = ' . $params['end_time']) :
                PeriodMaster::findFirst('LOCATE(node_id ,"' . $params['node_id'] . '")'
                        . ' and start_time=' . $params['start_time'] . ' and end_time = ' . $params['end_time']) ?
                        PeriodMaster::findFirst('LOCATE(node_id ,"' . $params['node_id'] . '")'
                                . ' and start_time=' . $params['start_time'] . ' and end_time = ' . $params['end_time']) : new PeriodMaster();
        $periods->node_id = $params['node_id'];
        $periods->period = $params['period'];
        $periods->start_time = $params['start_time'];
        $periods->end_time = $params['end_time'];
        $periods->user_type = "staff";
        if ($periods->save()) {
            $message['type'] = 'success';
            $message['ID'] = $ids[0];
            $message['message'] = ' Periods Added Successfully';
            print_r(json_encode($message));
            exit;
        } else {
            $error = '';
            foreach ($periods->getMessages() as $messages) {
                $error .= $messages;
            }
            echo $error;
            exit;
        }
    }

    public function editAttnPeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->id = $id = $this->request->getPost('id');
        $this->view->periodMastr = $periodMastr = PeriodMaster::findFirstById($id);
    }

    public function updatePeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }
        //$periods->node_id = $params['node_id'];
        $starttime = $params['start_time'];
        $endtime = $params['end_time'];
        $starttimestamp = strtotime('01-01-1970 ' . $params['start_time']);
        $endtimestamp = strtotime('01-01-1970 ' . $params['end_time']);
        if (isset($params['node_id']) && $params['node_id'] != '') {
            $periodsmas = new PeriodMaster();
            $contionqry = 'LOCATE(node_id ,"' . $params['node_id'] . '")'
                    . ' and( (start_time between "' . $starttimestamp . '" and "' . $endtimestamp . '" )'
                    . '   or ( end_time between "' . $starttimestamp . '" and "' . $endtimestamp . '" ))';
            $nodeid = $params['node_id'];
        }
        if (isset($params['period_id']) && $params['period_id'] != '') {
            $periodsmas = PeriodMaster::findFirst(' id = ' . $params['period_id']);
            $contionqry = 'LOCATE(node_id ,"' . $periodsmas->node_id . '")'
                    . ' and ( (start_time between "' . $starttimestamp . '" and "' . $endtimestamp . '" )'
                    . '   or ( end_time between "' . $starttimestamp . '" and "' . $endtimestamp . '" ))'
                    . ' and id != ' . $params['period_id'];
            $nodeid = $periodsmas->node_id;
        }
//echo $contionqry;exit;
        $periods = PeriodMaster::findFirst($contionqry);

        if ($periods) {
            $message['type'] = 'error';
            $message['message'] = ' Periods Already Existed';
            print_r(json_encode($message));
            exit;
        } else {
            $periodsmas->period = $params['period'];
            $periodsmas->node_id = $nodeid;
            $periodsmas->user_type = 'staff';
            $periodsmas->start_time = $starttimestamp;
            $periodsmas->end_time = $endtimestamp;
            if ($periodsmas->save()) {
                $message['type'] = 'success';
                $message['message'] = ' Periods Updated Successfully';
                print_r(json_encode($message));
                exit;
            } else {
                $error = '';
                foreach ($periods->getMessages() as $messages) {
                    $error .= $messages;
                }
                echo $error;
                exit;
            }
        }
    }

    public function deleteAttnPeriodsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $this->view->id = $id = $this->request->getPost('id');
        $periodMastr = PeriodMaster::findFirstById($id);
        $ids = explode('-', $periodMastr->node_id);
        if ($periodMastr->delete()) {
            $message['type'] = 'success';
            $message['ID'] = $ids[0];
            $message['message'] = ' Periods Deleted Successfully';
            print_r(json_encode($message));
            exit;
        } else {
            $error = '';
            foreach ($periodMastr->getMessages() as $messages) {
                $error .= $messages;
            }
            echo $error;
            exit;
        }
    }

}
