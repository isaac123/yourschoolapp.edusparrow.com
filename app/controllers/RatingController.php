<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class RatingController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function assignRatingAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->view_rating = 0;
        $this->view->ratings = RatingDivision::find();
    }

    public function getRatingsByCycleAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->cycleID = $this->request->getPost('cycleID');
            $this->view->subjects = RatingDivision::find('node_id LIKE "%-' . $this->view->cycleID
                            . '-%" OR node_id LIKE "' . $this->view->cycleID . '-%" OR node_id LIKE "%-'
                            . $this->view->cycleID . '" ORDER BY node_id');
            $allmains = RatingDivision::find();
            $subjects = array();
            if (count($allmains) > 0):
                foreach ($allmains as $mainex) {
                    $nodes = explode('-', $mainex->node_id);
                    array_walk($nodes, function (&$item, $key) {
                        $item = OrganizationalStructureValues::findFirstById($item)->name;
                    });
                    $node_name = implode('>>', $nodes);
                    $subjects[$node_name][] = array(
                        'id' => $mainex->id,
                        'node_id' => $mainex->node_id,
                        'rating_name' => $mainex->rating_name
                    );
                }
            endif;
            $this->view->subjects = $subjects;


            $this->view->form = new AddExamForm();
        }
    }

    public function addRatingFormAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->classId = $this->request->getPost('classID');
        $this->view->form = new AddRatingForm();
    }

    public function editRatingFormAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $examid = $this->request->getPost('ratid');
        $this->view->rating = RatingDivision::findFirstById($examid);
        $this->view->form = new AddRatingForm();
    }

    public function addRatingAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $form = new AddRatingForm();
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $message = array();
        // print_r($form->getMessages());
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) == false) {
                foreach ($form->getMessages() as $messages) {
                    $error .= $messages . "<br>";
                }
                //echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            } else {

                $params = array();
                foreach ($this->request->getPost() as $key => $value) {
                    $params[$key] = $value;
                }
                $classID = $params['classID'];
                $subjectName = $params['ratname'];
                $examid = $params['ratid'];
                $query[] = 'node_id = ' . $classID . ' and rating_name = "' . $subjectName . '"';
                if (isset($examid)) {
                    $query[] = "id != '$examid'";
                    $subject = RatingDivision::findFirstById($examid);
                } else {
                    $subject = new RatingDivision();
                }
                $subject->node_id = $classID;
                $subject->rating_name = $subjectName;
                $subject->created_by = $uid;
                $subject->created_on = time();
                $subject->modified_by = $uid;
                $subject->modified_on = time();
//                print_r(implode(' and ', $query));exit;
                $unique = RatingDivision::findFirst(implode(' and ', $query));
                if (($unique)) {
                    $message['type'] = 'error';
                    $message['message'] = 'Rating code Exists already';
                    print_r(json_encode($message));
                    exit;
                }
                if ($subject->save()) {
                    $message['type'] = 'success';
                    $message['message'] = 'Rating saved successfully!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($subject->getMessages() as $messages) {
                        $error .= $messages . "<br>";
                    }
                    // echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function ratingSettingsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $record = RatingCategoryMaster::find();
        $this->view->records = $record;
    }

    public function indexAction() {
        $this->tag->prependTitle("Evaluation | ");
        $this->assets->addCss('css/edustyle.css');
    }

    public function updateRatingCategoryAction() {
        $this->tag->prependTitle("Rating Settings| ");
        $this->assets->addJs("js/rating/rating.js");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $err = 0;
        if ($this->request->isPost()) {
            $count = $this->request->getPost('count');
            $hidden_count = $this->request->getPost('hidden_count');
            //echo $hidden_count;
            if ($hidden_count > 0) {
                $hidden_names = $this->request->getPost('hidden');

                for ($i = 0; $i < $hidden_count; $i++) {
                    $record_del = RatingCategoryMaster::findfirst(' category_name = "' . $hidden_names[$i] . '"');
                    if ($record_del->delete() == false) {
                        $err = 1;
                        $error = '';
                        foreach ($record_del->getMessages() as $messages) {
                            $error .= $messages;
                        }

                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        header('Content-Type: application/json');
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }

            if ($err == 0) {
                for ($i = 0; $i < $count; $i++) {
                    $name_value_pair = $this->request->getPost("$i");
                    //echo $pair[0];


                    $record = RatingCategoryMaster::findfirst(' category_name = "' . $name_value_pair[0] . '"');

                    if (!($record)) {
                        //New entry
                        $rating_Category_master = new RatingCategoryMaster();
                        $rating_Category_master->assign(array(
                            "category_name" => $name_value_pair[0],
                            "category_weightage" => $name_value_pair[1]));
                        if (!($rating_Category_master->save())) {
                            $error = '';
                            foreach ($rating_Category_master->getMessages() as $message) {
                                $error .= $message;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            header('Content-Type: application/json');
                            print_r(json_encode($message));
                            exit;
                        }
                        //echo "new";
                    } else {
                        $record->category_weightage = $name_value_pair[1];
                        $record->save();
                        header('Content-Type: application/html');
                        //echo "update";
                    }
                }
            }

            $category_records = RatingCategoryMaster::find();
            $this->view->category_records = $category_records;
        }
    }

    public function updateRatingLevelAction() {
        $this->tag->prependTitle("Rating Settings| ");
        $this->assets->addJs("js/rating/rating.js");
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $error = "";
        if ($this->request->isPost()) {
            $count = $this->request->getPost('total_count');
            $hidden_count = $this->request->getPost('hidden_count');
            //echo $hidden_count;
            if ($hidden_count > 0) {
                $hidden_names = $this->request->getPost('hidden');
                $names = $this->request->getPost('names');

                for ($i = 0; $i < $hidden_count; $i++) {
                    $masterrecord_del = RatingCategoryMaster::findfirst(' category_name = "' . $names[$i] . '"');
                    $record_del = RatingCategoryValues::findfirst('rating_category = ' . $masterrecord_del->id
                                    . ' and rating_level_name = "' . $hidden_names[$i] . '"');

                    if ($record_del->delete() == false) {
                        foreach ($record_del->getMessages() as $message) {
                            $error .= $message;
                        }
                    }
                }
            }

            for ($i = 0; $i < $count; $i++) {
                $rating_level = $this->request->getPost("$i");
                $masterrecord = RatingCategoryMaster::findfirst('category_name = "' . $rating_level[0] . '"');
                $record = RatingCategoryValues::findfirst('rating_category = ' . $masterrecord->id
                                . ' and rating_level_name = "' . $rating_level[1] . '"');
                if (!($record)) {
                    //New entry
                    $rating_Category_values = new RatingCategoryValues();
                    $rating_Category_values->assign(array("rating_category" => $masterrecord->id,
                        "rating_level_name" => $rating_level[1],
                        "rating_level_value" => $rating_level[2]));
                    if (!($rating_Category_values->save())) {
                        foreach ($rating_Category_values->getMessages() as $message) {
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    }
                } else {
                    $record->rating_level_value = $rating_level[2];
                    $record->save();
                    //echo "update";
                }
            }
            if ($error == "") {
                echo "Rating Level Added Succesfully";
            } else {
                echo $error;
            }
        }
    }

    public function saveAssignRatingsAction() {


        $this->tag->prependTitle("Rating Settings| ");
        $this->assets->addJs("js/rating/rating.js");

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {

            $return_status = 2;
            //0 -> duplicate name, 1 -> Error while updating database, 2 -> success

            $current_acd_year = ControllerBase::get_current_academic_year();

            $commonSetting = $this->request->getPost('commonSetting');

            $rating_name = strtoupper($this->request->getPost('rating_name'));

            if ($commonSetting == 'false') {
                $classId = $this->request->getPost('classId');
                //echo '<div class="alert alert-block alert-danger fade in">'.$classId.'</div>';

                $record = RatingDivision::find('div_val_id = ' . $classId . ' and academic_year_id = ' .
                                $current_acd_year . ' and rating_name = "' . $rating_name . '"');

                if (count($record) > 0) {
                    //Name already exists
                    $return_status = 0;
                } else {
                    $RatingDivision = new RatingDivision();
                    $RatingDivision->assign(array(
                        'div_val_id' => $classId,
                        'rating_name' => $rating_name,
                        'is_entire_school' => 0,
                        'academic_year_id' => $current_acd_year
                    ));
                    if (!($RatingDivision->save())) {
                        $return_status = 1;
                        $error = '';
                        foreach ($RatingDivision->getMessages() as $message) {
                            $error .= $message;
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    }
                }
            } else {

                $divvals = ControllerBase::get_division_values_student($current_acd_year);
                //echo '<div class="alert alert-block alert-danger fade in">'.$classId.'</div>';

                $duplicates = RatingDivision::find('academic_year_id = ' .
                                $current_acd_year . ' and rating_name = "' . $rating_name . '"');

                if (count($duplicates) == 0) {
                    //Rating Name is not taken up

                    foreach ($divvals as $divval) {

                        $record = RatingDivision::find('div_val_id = ' . $divval->id . ' and academic_year_id = ' . $current_acd_year);

                        $RatingDivision = new RatingDivision();
                        $RatingDivision->assign(array(
                            'div_val_id' => $divval->id,
                            'rating_name' => $rating_name,
                            'is_entire_school' => 1,
                            'academic_year_id' => $current_acd_year
                        ));
                        if (!($RatingDivision->save())) {
                            $return_status = 1;
                            $error = '';
                            foreach ($RatingDivision->getMessages() as $message) {
                                $error .= $message;
                                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            }
                        }
                    }
                } else {
                    //Name already exists
                    $return_status = 0;
                }
            }

            if ($return_status == 2) {
                echo '<div class="alert alert-block alert-danger fade in">Rating Added Successfully</div>';
            } else if ($return_status == 1) {
                echo '<div class="alert alert-block alert-danger fade in"> Error While updating database</div>';
            } else if ($return_status == 0) {
                echo '<div class="alert alert-block alert-danger fade in"> Rating Name already taken up, try a different name</div>';
            }
        }
    }

    public function viewAssignRatingAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->assets->addCss('css/edustyle.css');
        $this->view->selectedrating = $this->request->getPost('selectedrating');
        $this->view->action = $this->request->getPost('action');
        $this->view->ratinglist = RatingDivision::findFirstById($this->view->selectedrating);
        $identity = $this->auth->getIdentity();
        $this->view->uid = $identity['id'];
        $this->view->form = new RatingForm($this->view->ratinglist, array($this->view->action => true));
    }

    public function saveRatingAction() {


        $assignmentform = new RatingForm();
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        try {


            if ($this->request->isPost()) {

                if ($assignmentform->isValid($this->request->getPost()) == false) {

                    $error = '';
                    foreach ($assignmentform->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $assignmentId = $this->request->getPost('ratingId');
                    $assignment = isset($assignmentId) ? RatingDivision::findFirstById($assignmentId) : new AssignmentMaster();
                    $success = 1;
                    if ($assignment->is_entire_school == 1) {
                        $records = RatingDivision::find('rating_name = "' . $assignment->rating_name . '"');
                        foreach ($records as $record) {
                            $record->rating_name = $this->request->getPost('ratingname');
                            if ($record->save()) {
                                $success = 1;
                            } else {
                                $success = 0;
                            }
                        }
                    } else {
                        $assignment->rating_name = $this->request->getPost('ratingname');
                        $assignment->status = '1';
                        if ($assignment->save()) {
                            $success = 1;
                        } else {
                            $success = 0;
                        }
                    }

                    if ($success) {
                        $message['type'] = 'success';
                        $message['ItemID'] = $assignment->id;
                        $message['message'] = '<div class="alert alert-block alert-success fade in">Rating saved Successfully</div>';
                        print_r(json_encode($message));
                        exit;
                    } else {
                        $error = '';
                        foreach ($assignment->getMessages() as $message) {
                            $error .= $message;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {

            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function deleteRatingAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        try {
            if ($this->request->isPost()) {
                $assignmentId = $this->request->getPost('rating');
                $assignment = RatingDivision::findFirstById($assignmentId);

                $success = 1;
                if ($assignment->is_entire_school == 1) {
                    $phql = "SELECT GROUP_CONCAT(id) as id FROM rating_division where rating_name = '" . $assignment->rating_name . "'";
                    $results = $this->db->query($phql);
                    $result = $results->fetch();

                    $subjteach = StudentSubteacherRating::find('rating_division_id IN(' . $result['id'] . ')');
                    $classsubjteach = StudentClassteacherRating::find('rating_division_id IN(' . $result['id'] . ')');
                    $error = '';


                    if ((count($subjteach) == 0) && (count($classsubjteach) == 0)) {
                        $records = RatingDivision::find('rating_name = "' . $assignment->rating_name . '"');
                        foreach ($records as $record) {
                            if ($record->delete()) {
                                $success = 1;
                            } else {
                                $success = 0;
                                foreach ($record->getMessages() as $msg) {
                                    $error .= $msg;
                                }
                            }
                        }
                    } else {
                        $success = 0;
                        $error = 'Rating cannot be deleted as there are students already rated';
                    }
                } else {
                    if ($assignment->delete()) {
                        $success = 1;
                    } else {
                        $success = 0;
                    }
                }

                if ($success) {
                    $message['type'] = 'success';
                    $message['message'] = '<div class="alert alert-block alert-success fade in">Rating deleted Successfully</div>';
                    print_r(json_encode($message));
                    exit;
                } else {

                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {

            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function addStudentRatingAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $identity = $this->auth->getIdentity();
        if (in_array('Staff', $identity['role_name'])) {
            $param['type'] = 'mainexam_mark';
            $getCombination = ControllerBase::loadCombinations($param);
            $this->view->combinations = $getCombination[1];
            $this->view->type = $getCombination[2];
            $this->view->errorMessage = $getCombination[0];
        } else {
            $this->view->errorMessage = 'Only teachers have access to this page!';
        }
    }

    public function loadRatingNameAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_rating = 1;
        if ($this->request->isPost()) {
            $subject_masterid = $this->request->getPost('subject_masterid');
             $this->view->return_uri = $return_uri = $this->request->getPost('return_uri');
            //Get div_val_id from subject_masterid
            if ($return_uri == "subject_teacher_rating") {
                $subject_master = GroupSubjectsTeachers::findfirst('id = ' . $subject_masterid);
            } else {
                $subject_master = GroupClassTeachers::findfirst('id = ' . $subject_masterid);
            }
            $classmaster = ClassroomMaster::findFirstById($subject_master->classroom_master_id);
            $this->view->subject_id = isset($subject_master->subject_id) ? $subject_master->subject_id : '';
            $res = ControllerBase::buildExamQuery($classmaster->aggregated_nodes_id);

            //fetch the rating_names
            $ratings = RatingDivision::find(implode(' or ', $res));
            if (count($ratings) > 0) {
                $this->view->ratings = $ratings;
            } else {
                $this->view->display_rating = 0;
            }
        }
    }

    public function loadStudentsRatingAction() {

        $this->tag->prependTitle("Add Student Rating | ");
        $this->assets->addJs("js/rating/rating.js");

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_students = 1;
        $this->view->display_category = 1;
        if ($this->request->isPost()) {

            foreach ($this->request->getPost() as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'aggregate' && $value != '') {
                    $params['aggregateids'][] = $value;
                } else {
                    $params[$key] = $value;
                }
            }

            $this->view->rating_id = $rating_id = $params['rating_id'];
            $aggregate_keys = $params['aggregateids'][count($params['aggregateids']) - 1];
            $master_id = $params['subject_masterid'];
            $this->view->master_id = $master_id;
            $return_uri = $params['return_uri'];
            if ($return_uri == "subject_teacher_rating") {
                $grpSubjTeach = GroupSubjectsTeachers::findFirstById($master_id);
                $this->view->type = 'subject';
                $subjectid = $aggregate_keys;
                $classmaster = ClassroomMaster::findFirstById($grpSubjTeach->classroom_master_id);
                $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//                echo $grpSubjTeach->classroom_master_id;exit;
//                $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);         
//                $orgvaldet = OrganizationalStructureValues::findFirstById($grpSubjTeach->subject_id);
//                $subj_arr = $this->_getMandNodesForExam($orgvaldet);
//                $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", stumap.aggregate_key)>0)', array_keys($subj_arr));
                $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                        . 'stumap.subordinate_key,stumap.status'
                        . ' ,stuinfo.Gender,stuinfo.photo,stuinfo.rollno,stuinfo.Student_Name '
                        . ' FROM StudentMapping stumap LEFT JOIN'
                        . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                        . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                        . ' and (' . implode(' or ', $res) . ') '
//                        . ' and (' . implode(' and ', $subjQuery) . ') '
                        . ' ORDER BY stuinfo.Admission_no ASC';

//                print_r($stuquery);exit;
                $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);
            } else {
                $grpSubjTeach = GroupClassTeachers::findfirst('id = ' . $master_id);
                $this->view->type = 'class';
                $this->view->subject = '';
                $subjectid = '';
                $classmaster = ClassroomMaster::findFirstById($grpSubjTeach->classroom_master_id);
                $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//                $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
                $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                        . 'stumap.subordinate_key,stumap.status'
                        . ' ,stuinfo.Gender,stuinfo.photo,stuinfo.rollno,stuinfo.Student_Name '
                        . ' FROM StudentMapping stumap LEFT JOIN'
                        . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id '
                        . ' WHERE stumap.status = "Inclass" '
                        . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                        . 'and (' . implode(' or ', $res)
                        . ' ORDER BY stuinfo.Admission_no ASC';
                $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);
            }
              
            $this->view->node_id = $classmaster->aggregated_nodes_id;
            $this->view->node_name = ControllerBase::getNameForKeys($classmaster->aggregated_nodes_id);
            $category_records = RatingCategoryMaster::find();
            $this->view->category_records = $category_records;
            $rating_value_records = array();
            if (count($category_records) > 0) {
                $i = 0;
                foreach ($category_records as $record) {
                    $rating_value_records[$i++] = array('id' => $record->id,
                        'values' => RatingCategoryValues::find('rating_category = ' . $record->id));
                }
            } else {
                $this->view->display_category = 0;
            }

            $i = 0;
            if (count($students) > 0) {
                foreach ($students as $student) {
                    $gender = ($student->Gender == 1 ? 'Female' : 'Male');
                    $rows[$i++] = array(
                        $student->Student_Name,
                        $student->student_info_id,
                        $grpSubjTeach->id,
                        $rating_id,
                        $student->rollno,
                        $student->photo,
                        $subjectid,
                        $gender);
                }
                $this->view->rows = $rows;
                $this->view->rating_value_records = $rating_value_records;
                $this->view->return_uri = $return_uri;
            } else {
                $this->view->display_students = 0;
            }
        }
    }

    public function _getMandNodesForExam($acdyrMas, $nodes = array()) {
        $fields = OrganizationalStructureValues::findFirstById($acdyrMas->parent_id);
        $iscycle = OrganizationalStructureMaster::findFirstById($acdyrMas->org_master_id);
//        $query = 'id =' . $fields->org_master_id . ' AND module !="Subject"';
//        $org_mas = OrganizationalStructureMaster::find(array(
//                    $query,
//                    "columns" => "COUNT(*) as cnt"
//        ));
//        echo $iscycle->id.$iscycle->promotion_status.$name->id.'<br>';
        if (!in_array($iscycle->module, array("structural", "StaffCycleNode", "Ledger"))) {
            if ($iscycle->module != "Subject"):
                $nodes[$acdyrMas->id] = $acdyrMas->name;

            endif;
            if (isset($fields->parent_id)):
//            echo $fields->parent_id;
                $nodes = $this->_getMandNodesForExam($fields, $nodes);
            endif;
        }

        return $nodes;
    }

    public function updateStudentsRatingAction() {

        $this->tag->prependTitle("Add Student Rating | ");
        $this->assets->addJs("js/rating/rating.js");
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $params = $queryParams = array();

            foreach ($this->request->getPost() as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'aggregate' && $value) {
                    $params['aggregateids'][] = $value;
                } else {
                    $params[$key] = $value;
                }
            }
            if ($this->request->getPost('return_uri') == "subject_teacher_rating") {
                $subject_masterid = $this->request->getPost('subject_masterid');
                $stud_id = $this->request->getPost('stud_id');
                $rating_id = $this->request->getPost('rating_id');
                $category_count = $this->request->getPost('category_count');
                $success = 1;
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                $subject = GroupSubjectsTeachers::findfirst('id = ' . $subject_masterid);

                $subjct_modules = $params['aggregateids'][count($params['aggregateids']) - 1];
//                echo $subjct_modules;
//                exit;

                for ($i = 0; $i < $category_count; $i++) {
                    $rating = $params[$i];
                    $student_rating = StudentSubteacherRating::findfirst('student_id = ' . $stud_id
                                    . ' and rating_division_id =' . $rating_id
                                    . ' and subject_master_id = ' . $subject->id
                                    . ' and subjct_modules = ' . $subjct_modules
                                    . ' and rating_category = ' . $rating[0]);

                    if (!$student_rating) {
                        //New record
                        $StudentSubteacherRating = new StudentSubteacherRating();
                        $StudentSubteacherRating->assign(array(
                            "student_id" => $stud_id,
                            "rating_division_id" => $rating_id,
                            "subject_master_id" => $subject->id,
                            "subjct_modules" => $subjct_modules,
                            "rating_category" => $rating[0],
                            "rating_value" => $rating[1],
                            "created_by" => $uid,
                            "created_date" => time()
                        ));
                        if (!($StudentSubteacherRating->save())) {
                            $success = 0;
                            $error = '';
                            foreach ($StudentSubteacherRating->getMessages() as $message) {
                                $error .= $message;
                                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            }
                        }
                    } else {
                        $student_rating->rating_value = $rating[1];
                        $student_rating->modified_by = $uid;
                        $student_rating->modified_date = time();
                        $student_rating->save();
                    }
                }
                echo $success;
            } else {
                $class_masterid = $this->request->getPost('subject_masterid');
                $stud_id = $this->request->getPost('stud_id');
                $rating_id = $this->request->getPost('rating_id');
                $category_count = $this->request->getPost('category_count');
                $success = 1;
                for ($i = 0; $i < $category_count; $i++) {
                    $rating = $this->request->getPost("$i");
                    $student_rating = StudentClassteacherRating::findfirst('student_id = ' . $stud_id
                                    . ' and rating_division_id =' . $rating_id
                                    . ' and class_master_id = ' . $class_masterid .
                                    ' and rating_category = ' . $rating[0]);

                    if (!$student_rating) {
                        //New record
                        $StudentClassteacherRating = new StudentClassteacherRating();
                        $StudentClassteacherRating->assign(array(
                            "student_id" => $stud_id,
                            "rating_division_id" => $rating_id,
                            "class_master_id" => $class_masterid,
                            "rating_category" => $rating[0],
                            "rating_value" => $rating[1],
                            "created_by" => $uid,
                            "created_date" => time()
                        ));
                        if (!($StudentClassteacherRating->save())) {
                            $success = 0;
                            $error = '';
                            foreach ($StudentClassteacherRating->getMessages() as $message) {
                                $error .= $message;
                                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            }
                        }
                    } else {
                        $student_rating->rating_value = $rating[1];
                        $student_rating->modified_by = $uid;
                        $student_rating->modified_date = time();
                        $student_rating->save();
                    }
                }
                echo $success;
            }
        }
    }

    public function getRatingByCategoryAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if ($this->request->isPost()) {
            $master_id = $this->request->getPost('subject_masterid'); //$params['master_id']; //'1'; //
            $combiType = $this->request->getPost('type'); //$params['type']; //'subject'; //
            $subject_module = $this->request->getPost('subject_module') ? $this->request->getPost('subject_module') : '';
            $identity = $this->auth->getIdentity();
            $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
            if ($combiType == 'subject') {

                $grpSubjTeach = GroupSubjectsTeachers::findfirst('id = ' . $master_id);
                $classmaster = ClassroomMaster::findFirstById($grpSubjTeach->classroom_master_id);
                $this->view->node_id = $classmaster->aggregated_nodes_id;
                $this->view->node_name = ControllerBase::getNameForKeys($classmaster->aggregated_nodes_id);
                $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);

//                $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
                $orgvaldet = OrganizationalStructureValues::findFirstById($grpSubjTeach->subject_id);
                $subj_arr = RatingController::_getMandNodesForExam($orgvaldet);

                $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", stumap.aggregate_key)>0)', array_keys($subj_arr));
                $stuquery = 'SELECT stumap.id,stumap.student_info_id,stuinfo.Student_Name,stumap.aggregate_key,'
                        . 'stumap.subordinate_key,stumap.status'
                        . ' FROM StudentMapping stumap LEFT JOIN'
                        . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" '
                        . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                        . ' and (' . implode(' or ', $res)  . ')'
//                        . ' and (' . implode(' and ', $subjQuery) . ')'
                        . '  ORDER BY stuinfo.Admission_no ASC';

                $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);
            } else {
                $grpSubjTeach = GroupClassTeachers::findfirst('id = ' . $master_id);
                $this->view->type = 'class';
                $this->view->subject = '';
                $subjectid = '';
                $classmaster = ClassroomMaster::findFirstById($grpSubjTeach->classroom_master_id);
                $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//                $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
                $stuquery = 'SELECT stumap.id,stumap.student_info_id,stuinfo.Student_Name,stumap.aggregate_key,'
                        . 'stumap.subordinate_key,stumap.status'
                        . ' FROM StudentMapping stumap LEFT JOIN'
                        . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id'
                        . '  WHERE stumap.status = "Inclass" '
                        . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                        . ' and (' . implode(' or ', $res)  . ') '
                        . ' ORDER BY stuinfo.Admission_no ASC';

                $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);
            }
            $rowArr = $yaxis = array();
            foreach ($students as $stu) {
                $ratingCategorys = RatingCategoryMaster::find();
                foreach ($ratingCategorys as $ratingCategory) {
                    $ratingPoints = 0;
                    $ratingValues = RatingCategoryValues::find('rating_category = ' . $ratingCategory->id);
                    $ratTotPoints = $ratingCategory->category_weightage;
                    foreach ($ratingValues as $rvalue) {
                        $studentRating = ($combiType == 'subject') ? StudentSubteacherRating::findFirst(
                                        ' subject_master_id =' . $grpSubjTeach->id
                                        . ' and rating_category =' . $ratingCategory->id
                                        . ' and rating_value = ' . $rvalue->id
                                        . ' and subjct_modules = ' . $subject_module
                                        . ' and student_id = ' . $stu->id
                                ) :
                                StudentClassteacherRating::findFirst(
                                        'class_master_id =' . $grpSubjTeach->id
                                        . ' and rating_category =' . $ratingCategory->id
                                        . ' and rating_value = ' . $rvalue->id
                                        . ' and student_id = ' . $stu->id
                        );
                        if ($studentRating->rating_id > 0) {
                            $ratingPoints += ($rvalue->rating_level_value / 100) * $ratingCategory->category_weightage;
                        }
                    }


                    $rowArr[$stu->id][$ratingCategory->id] = array(
                        'rating_name' => $ratingCategory->category_name,
                        'ratingPoints' => $ratingPoints,
                        'StudentName' => $stu->Student_Name
                    );

                    $yaxis[$ratingCategory->id] = $ratingCategory->category_name . " ($ratTotPoints pts)";

                    $yaxis[$ratingCategory->id] = array(
                        'categoryname' => $ratingCategory->category_name,
                        'ratPoints' => $ratTotPoints);
                }
            }
//            $yaxisuniqueArr = array_unique($yaxis);
            $this->view->rowArr = $rowArr;
            $this->view->yaxisuniqueArr = $yaxis;
//        echo '<pre>';
//        print_r ( array($rowArr,$yaxisuniqueArr));
//        exit;
        }
    }

    public function addClassteacherRatingAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $identity = $this->auth->getIdentity();
        if (in_array('Staff', $identity['role_name'])) {
            $param['type'] = 'rating';
            $getCombination = ControllerBase::loadCombinations($param);
            $this->view->combinations = $getCombination[1];
            $this->view->type = $getCombination[2];
            $this->view->errorMessage = $getCombination[0];
        } else {
            $this->view->errorMessage = 'Only teachers have access to this page!';
        }
    }

    public function getRatingByMainCatAction($params) {

        $master_id = $params['master_id']; //'1'; //
        $combiType = $params['type']; //'subject'; //
        $masterCombi = ($combiType == 'subject') ? SubDivisionSubjects::findFirstById($master_id) :
                ClassTeachersMaster::findFirstById($master_id);
        $divValId = $masterCombi->div_val_id;
        $subDivValId = $masterCombi->concat_subdivs;
        $academicYrId = $params['academic_year_id']; // '1'; //

        $havingQuery = $subDivValId ? "(LENGTH('$subDivValId') - LENGTH( REPLACE('$subDivValId',Subdivision , '' ) ))/ LENGTH(Subdivision )>0
                    OR
                    (LENGTH('$subDivValId') - LENGTH( REPLACE('$subDivValId',Subdivision ,'' ) )) / LENGTH(Subdivision ) IS NULL" : '';
        $having = $havingQuery ? 'having' : '';

        $students = StudentInfo::find(array(
                    "Division_Class =$divValId",
                    $having => $havingQuery
        ));
        $rowArr = array();
        foreach ($students as $stu) {
            $divRatings = RatingDivision::find('div_val_id =' . $divValId
                            . ' and academic_year_id=' . $academicYrId);
            foreach ($divRatings as $divRating) {
                $ratTotPoints = $ratingValTot = 0;
                $ratingCategorys = RatingCategoryMaster::find('academic_year_id=' . $academicYrId);
                foreach ($ratingCategorys as $ratingCategory) {
                    $ratingPoints = 0;
                    $ratingValues = RatingCategoryValues::find('rating_category = ' . $ratingCategory->id);
                    $ratTotPoints += $ratingCategory->category_weightage;
                    foreach ($ratingValues as $rvalue) {
                        $studentRating = ($combiType == 'subject') ? StudentSubteacherRating::findFirst(
                                        'rating_division_id =' . $divRating->id
                                        . ' and subject_master_id =' . $masterCombi->subject_id
                                        . ' and rating_category =' . $ratingCategory->id
                                        . ' and rating_value = ' . $rvalue->id
                                        . ' and student_id = ' . $stu->id
                                ) :
                                StudentClassteacherRating::findFirst(
                                        'rating_division_id =' . $divRating->id
                                        . ' and class_master_id =' . $masterCombi->id
                                        . ' and rating_category =' . $ratingCategory->id
                                        . ' and rating_value = ' . $rvalue->id
                                        . ' and student_id = ' . $stu->id
                        );
                        if ($studentRating->rating_id > 0) {
                            $ratingPoints += ($rvalue->rating_level_value / 100) * $ratingCategory->category_weightage;
                        }
//                    echo $ratingCategory->category_name.' - '. $ratingPoints.'<br>';
                    }


                    $rowArr[$divRating->id][$stu->id] = array(
                        'rating_name' => $divRating->rating_name,
//                        'subjectName' => SubjectsDivision::findFirstById($masterCombi->subject_id)->subjectName,
                        'ratingPoints' => $ratingPoints,
                        'StudentName' => $stu->Student_Name
                    );

                    $yaxis[$divRating->id] = $divRating->rating_name . " ($ratTotPoints pts)";
                }
            }
        }
        $yaxisuniqueArr = array_unique($yaxis);
//        echo '<pre>';
        return array($rowArr, $yaxisuniqueArr);
//        exit;
    }

    public function loadSectionsByClassAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_subdiv = 0;
        if ($this->request->isPost()) {
            $this->view->classId = $this->request->getPost('classId');

            $this->view->subdivname = ControllerBase::get_sub_division_name_student();
            $subdivvals = ControllerBase::get_sub_division_values_student($this->view->classId);

            if (count($subdivvals)) {
                $this->view->subdivVal = $subdivvals;
                $this->view->display_subdiv = 1;
            }
        }
    }

    public function loadSubjectsBySectionAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_subject = 0;
        if ($this->request->isPost()) {
            $classId = $this->request->getPost('class_id');

            $subdivid = $this->request->getPost('subDivVal');

            //fetch the subjects
            $subjects = ControllerBase::get_subjects($classId, $subdivid);

            if (count($subjects) > 0) {
                $this->view->display_subject = 1;
                $this->view->subjects = $subjects;
            }
        }
    }

}
