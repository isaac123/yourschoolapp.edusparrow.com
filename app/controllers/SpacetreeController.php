<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class SpacetreeController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->tag->prependTitle("Space Tree | ");
    }

    public function spacetreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Space Tree | ");
        if ($this->request->isPost()) {
            $cycle = $this->request->getPost('cycle');
            $res = ControllerBase::_getValueTreeFor($cycle);
        }
    }
    public function getTreeAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $cycle = $this->request->getPost('cycle');
            $res = ControllerBase::_getValueTreeFor($cycle);

            print_r(json_encode($res));
            exit;
        }
    }
    public function getTreeForMainNodesAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $cycle = $this->request->getPost('cycle');
            $res = ControllerBase::_getValueTreeMainNodes($cycle);

            print_r(json_encode($res));
            exit;
        }
    }

}

?>