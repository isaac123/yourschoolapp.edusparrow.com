<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class RelievingController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function relieveStaffAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Relieving | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appointment/appointment.js');
        $this->assets->addJs("js/appscripts/relieving.js");
        $this->assets->addJs('js/appscripts/approval.js');

        $nodes = array();

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');
        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                $nodes[$field->id] = $field->name;
            }
        endif;

//        print_r($nodes);
//        exit;

        $this->view->nodes = $nodes;
        $this->view->mandnode = $nodes;
    }

    public function stftblhdrAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $nodes = array();

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');
        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                $nodes[$field->id] = $field->name;
            }
        endif;

//        print_r($nodes);
//        exit;

        $this->view->nodes = $nodes;
        $this->view->mandnode = $nodes;
    }

    public function loadRelieveStfDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = $queryParams1 = array();
        $orderphql = array();
        foreach ($this->request->getPost() as $key => $value) {
            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'aggregate' && $value) {
                $params['aggregateids'][] = $value;
            } else {

                $params[$key] = $value;
            }
        }


        if (isset($params['appointment_no']) && $params['appointment_no'] != '') {
            $queryParams[] = "appointment_no='" . $params['appointment_no'] . "'";
        }

        if (isset($params['name']) && $params['name'] != '') {

            $queryParams[] = "Staff_Name LIKE '" . $params['name'] . "%'";
        }

        if (isset($params['loginid']) && $params['loginid'] != '') {

            $queryParams[] = "loginid = '" . $params['loginid'] . "'";
        }


        if (isset($params['sSearch']) && $params['sSearch'] != ''):

            $psql_deprt = OrganizationalStructureValues::findFirst('name LIKE"%' . $params['sSearch'] . '%"');       //'SELECT id FROM OrganizationalStructureValues WHERE name LIKE"%' . $params['sSearch'] . '%"';
            if (isset($psql_deprt)) {
                $dep_id = isset($psql_deprt->id) ? $psql_deprt->id : '';
                if ($dep_id != '')
                    $queryParams1[] = 'aggregate_key Like "%' . $dep_id . '%"';
            }

            $queryParams1[] = 'appointment_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'Staff_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;


        $queryParams[] = 'status IN ("Appointed","Relieved","Relieving initiated")';
        $orderphql = array();
        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $orderphql[] = $this->getSortColumnNameForRelieveStf($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }
        $order = implode(',', $orderphql);
        $applications = StaffInfo::find(array(
                    implode(' and ', $queryParams),
                    "order" => $order,
                    "limit" => array(
                        "number" => $this->request->getPost('iDisplayLength'),
                        "offset" => $this->request->getPost('iDisplayStart')
                    )
        ));
        $totapplications = StaffInfo::find(array(
                    implode(' and ', $queryParams),
                    "columns" => "COUNT(*)",
        ));

        $rowEntries = $this->formatRelieveStfTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($totapplications),
            "iTotalDisplayRecords" => count($applications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForRelieveStf($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "appointment_no";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "appointment_no";
            case 1:
                return "Staff_Name";
            case 2:
                return "Gender";
            case 3:
                return "loginid";
            case 4:
                return "Date_of_Joining";
            case 5:
                return "status";
            default:
                return "appointment_no";
        }
    }

    public function formatRelieveStfTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $row['appointment_no'] = $items->appointment_no ? $items->appointment_no : '';
                //    $row['Staff_Name'] = $items->Staff_Name ? $items->Staff_Name : '';


                $staffgeninfo = StaffGeneralMaster::findFirstByStaffId($items->id);
                if ($staffgeninfo->photo) {
                    $row['Staff_Name'] = '<span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Staff_Name'] .= $this->tag->image(array(
                        "src" => $staffgeninfo->photo ? (FILES_URI . $staffgeninfo->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Staff_Name'] .= '</span>';
                } else {
                    $row['Staff_Name'] = ' <span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;" >  ' . substr($items->Staff_Name, 0, 1) . '</span>';
                }
                $row['Staff_Name'] .= '<span title="' . $items->Staff_Name . ' (' . $items->appointment_no . ') ' . '" class="name"> 
                                        ' . (strlen($items->Staff_Name) > 15 ? substr($items->Staff_Name, 0, 15) . "..." : $items->Staff_Name) . '</span>';
                //$row['Staff_Name'] .= '<span title="' . $items->appointment_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                      ' . $items->Staff_Name .'</span>';



                $row['Gender'] = ($items->Gender == 1) ? 'Female' : 'Male';
                $row['loginid'] = $items->loginid ? $items->loginid : '';
                $row['Date_of_Joining'] = ($items->Date_of_Joining) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row['status'] = $items->relieve_status ? ($items->relieve_status == 'Relieved' ?
                                '<label href="javascript:void(0)" class="label label-danger">' . $items->relieve_status . '</label> ' :
                                ($items->relieve_status == 'Approved' ?
                                        '<label href="javascript:void(0)" class="label label-success">' . $items->status . '</label> ' :
                                        ($items->relieve_status == 'Appointed' ?
                                                '<label href="javascript:void(0)" class="label label-info">' . $items->status . '</label> ' :
                                                '<label href="javascript:void(0)" class="label label-danger">Relieving initiated</label> '))) : '<label href="javascript:void(0)" class="label label-info">' . $items->status . '</label> ';
                $row['Actions'] = '';
                if (in_array($items->relieve_status, array('Approved')) && !in_array($items->status, array('Relieved'))) {
                    $row['Actions'] = '<a href="javascript:void(0)" class="btn btn-danger"  itemID ="' . $items->id . '"   '
                            . ' onclick="relievingSettings.staffRelievPermanently(this)" >
                                    <i class="icon-off"></i>Relieve</a> ';
                } else if (in_array($items->relieve_status, array('Initiated'))) {
                    $row['Actions'] = '<a href="javascript:void(0)" class="btn btn-success"  itemID ="' . $items->id . '"   '
                            . ' onclick="relievingSettings.loadForwardforRelieve(this)" >
                                  Forward</a>';
                } else if (in_array($items->status, array('Appointed'))) {
                    $row['Actions'] = '<a href="javascript:void(0)" class="btn btn-default" '
                            . 'onclick="relievingSettings.loadRelievForm (' . $items->id . ')" >Initiate Relieve</a> ';
                } else if (in_array($items->status, array('Relieved'))) {
                    $row['Actions'] = '<a href="javascript:void(0)" class="text text-muted">-</a> '; //. $items->status 
                } else {
                    $row['Actions'] = '<a class="btn btn-warning"                
                                        appType ="9"  itemID ="' . $items->id . '"    
                                                    "  
                                                    id="stf_status">' . $items->relieve_status . '</a>';
                }

                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';

                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }

                $row['Print'] = '-';

                if ($items->status == 'Relieved') {

                    $row['Print'] = '<a target="_blank" title="Appointment Lettrt" href="' . $this->url->get() . 'print/staffReliev?staffid=' . $items->id . '" >
                                 <span class="mini-stat-icon tar"><i class="fa fa-print"></i></span> </a>';
                }


                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    public function staffRelievModalAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->form = new StaffRelieveForm();
        if ($this->request->isPost()) {
            $this->view->staff = StaffInfo::findFirstById($this->request->getPost('stfId'));
        }
    }

    public function relieveStaffByReasonAction() {

        $relievform = new StaffRelieveForm();
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        try {
            if ($this->request->isPost()) {
                if ($relievform->isValid($this->request->getPost()) == false) {
                    $error = '';
                    foreach ($this->request->getPost() as $key => $val) {
                        foreach ($relievform->getMessagesFor($key) as $messages) {
                            $error .= $messages . "</br>";
                        }
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $dor = strtotime(date('d-m-Y', strtotime($this->request->getPost('dor'))) . " 00:00:00");
                    $staff = StaffInfo::findFirstById($this->request->getPost('staffID'));
                    $staffGm = StaffGeneralMaster::findFirstByStaffId($this->request->getPost('staffID')) ?
                            StaffGeneralMaster::findFirstByStaffId($this->request->getPost('staffID')) :
                            new StaffGeneralMaster();
                    $identity = $this->auth->getIdentity();
                    $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;


                    $staffGm->staff_id = $this->request->getPost('staffID');
                    $staffGm->relieving_reason = $this->request->getPost('relievReason');
                    $staffGm->relieved_by = $uid;
                    $staffGm->dor = $dor;
                    if (!$staffGm->created_by) {
                        $staffGm->created_by = $uid;
                        $staffGm->created_date = time();
                    }
                    $staffGm->modified_by = $uid;
                    $staffGm->modified_date = time();
//                    print_r($staffGm);exit;
                    if ($staffGm->save()) {
                        $staff->status = 'Relieving initiated';
                        $staff->relieve_status = 'Initiated';
                        if ($staff->save()) {
                            $message['type'] = 'success';
                            $message['ItemID'] = $staff->id;
                            $message['message'] = '<div class="alert alert-block alert-success fade in">Releiving initiated successfully!</div>';
                            print_r(json_encode($message));
                            exit;
                        } else {

                            foreach ($staff->getMessages() as $message) {
                                $error .= $message;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {

                        foreach ($staffGm->getMessages() as $message) {
                            $error .= $message;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function relieveStaffPermanentlyAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $message = array();
        try {
            if ($this->request->isPost()) {
                $staff = StaffInfo::findFirstById($this->request->getPost('staffID'));
                $staff->status = 'Relieved';
//                print_r($staff);
//                exit;
                if ($staff->save()) {
                    $param = array(
                        'appkey' => APPKEY,
                        'subdomain' => SUBDOMAIN,
                        'businesskey' => BUSINESSKEY);
                    $param['login'] = $staff->loginid;
                    $data_string = json_encode($param);
                    $response = IndexController::curlIt(USERAUTHAPI . 'deleteUser', $data_string);
//                    print_r($response);
                    $userRemoved = json_decode($response);
//          print_r($loginCreated);;
                    if (!$userRemoved->status) {
                        $message['type'] = 'error';
                        $message['message'] = 'Internal server error';
                        print_r(json_encode($message));
                        exit;
                    }
                    if ($userRemoved->status == 'ERROR') {
                        $message['type'] = 'error';
                        $message['message'] = $userRemoved->messages;
                        print_r(json_encode($message));
                        exit;
                    } else if ($userRemoved->status == 'SUCCESS') {
                        $message['type'] = 'success';
                        $message['message'] = 'Staff relieved successfully!';
                        print_r(json_encode($message));
                        exit;
                    }
                } else {

                    foreach ($staff->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function studentAction() {
        $this->view->disableLevel(array(
            View::LEVEL_LAYOUT => true,
            View::LEVEL_MAIN_LAYOUT => true
        ));
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->stuAutoCompleteHtml = $htmlContent = $this->view->getRender('search', 'studentAutocomplete', array('callBackFn' => 'relievingSettings.loadStudent'));
        $this->assets->addJs("js/search/search.js");
        $this->assets->addJs("js/appscripts/relieving.js");
    }

    public function studentRelievModalAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->student = json_decode($this->request->getPost('studentJson'));
//            print_r( $this->view->student);exit;

            $this->view->isrelieved = $rel = StudentRelievingMaster::findFirst('student_id=' . $this->view->student->id . ' order by id desc');
            $isrelieved = ($rel->status != "Rejected") ? $rel : '';
            if ($isrelieved->status == 'Relieved') {
                $this->flash->error('Student relieved already!');
                exit;
            }
//            print_r($isrelieved);exit;
            $this->view->form = new StudentRelieveForm($this->view->student, array(
                'edit' => true,
                'relieventity' => $isrelieved
            ));
        }
    }

    public function relieveStudentAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $studentId = $this->request->getPost('studentId');
        $student = StudentInfo::findFirstById($studentId);
        $studentAcd = StudentMapping::findFirstByStudentInfoId($studentId);
        $studentHis = StudentHistory::findFirst('student_info_id = ' . $studentId . ' and aggregate_key LIKE "' . $studentAcd->aggregate_key . '"');
        $relievform = new StudentRelieveForm($student, array('edit' => true));
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $message = array();
        try {
            if ($this->request->isPost()) {

                $params = $queryParams = $queryParams1 = array();
                foreach ($this->request->getPost() as $key => $value) {
                    $IsSubdiv = explode('_', $key);
                    if ($IsSubdiv[0] == 'raggregate' && $value) {
                        $params['raggregateids'][] = $value;
                    } else if ($IsSubdiv[0] == 'jaggregate' && $value) {
                        $params['jaggregateids'][] = $value;
                    } else {

                        $params[$key] = $value;
                    }
                }
                if ($relievform->isValid($this->request->getPost()) == false) {
                    $error = '';
                    foreach ($this->request->getPost() as $key => $val) {
                        foreach ($relievform->getMessagesFor($key) as $messages) {
                            $error .= $messages . "</br>";
                        }
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    /*                     * relieve conditions * */
                    $relievecntns = RelievingConditions::find("user_type= 'student'");
                    foreach ($relievecntns as $relievecntn) {
                        $qrybuild = "";
                        $qrycn = array();
                        if ($relievecntn->module == "Fee")
                            $qrycn[] = $relievecntn->condition;
                        $qrycn[] = "student_id = " . $studentId;
                        $qrybuild = StudentFeeTable::findFirst(implode(' and ', $qrycn));
                        $relievecntns = UserRelieveStatus::findFirst("module='" . $relievecntn->module . "' and  user_type= 'student' and user_id = " . $studentId) ? UserRelieveStatus::findFirst("module='" . $relievecntn->module . "' and  user_type= 'student' and user_id = " . $studentId) : new UserRelieveStatus();

                        $relievecntns->module = $relievecntn->module;
                        $relievecntns->module_status = $qrybuild ? 0 : 1;
                        $relievecntns->user_id = $studentId;
                        $relievecntns->user_type = 'student';
                        $relievecntns->comments = $relievecntn->condition_desc;
                        if (!$relievecntns->save()) {
                            foreach ($relievecntns->getMessages() as $message) {
                                $error .= $message . "<br>";
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                    /*                     * relieve conditions * */

                    $relievingMaster = new StudentRelievingMaster();
                    // StudentRelievingMaster::findFirst('student_id=' . $studentId) ?
//                            StudentRelievingMaster::findFirstByStudentId($studentId) : 
                    $student->father_name = $params['fname'];
                    $student->mother_name = $params['mname'];
                    $student->other_guardian_name = $params['gname'];
                    $student->nationality = $params['nationality'];
                    $student->religion = $params['religion'];
                    $student->first_language = $params['language'];
                    $student->modified_by = $uid;
                    $student->modified_date = time();

                    $studentAcd->status = "Relieving Initiated";
                    $studentHis->status = "Relieving Initiated";

                    $relievingMaster->status = 'Initiated';
                    $relievingMaster->student_id = $student->id;
                    $relievingMaster->edu_inst = $params['school'];
                    $relievingMaster->admission_date = strtotime($params['doj']);
                    $relievingMaster->joining_aggregate_key = implode(',', $params['jaggregateids']);
                    $relievingMaster->scl_left_date = strtotime($params['dor']);
                    $relievingMaster->relieving_aggregate_key = implode(',', $params['raggregateids']);
                    $relievingMaster->stdcharacter = $params['conduct'];
                    $relievingMaster->tc_made_date = strtotime($params['applytc']);
                    $relievingMaster->tc_date = strtotime($params['dotc']);
                    $relievingMaster->course_complete = $params['course_complt'];
                    $relievingMaster->qualify_promo = $params['promoted_higher'];
                    $relievingMaster->course_comp_det = $params['complt_det'];
                    $relievingMaster->scl_name = $params['SchoolName'];
                    $relievingMaster->medium = $params['moi'];
                    $relievingMaster->reason = $params['relievReason'];
                    $relievingMaster->created_by = $uid;
                    $relievingMaster->created_date = time();
                    $relievingMaster->modified_by = $uid;
                    $relievingMaster->modified_date = time();
//                    print_r($relievingMaster); exit;
                    if ($relievingMaster->save()) {
                        if ($student->save() && $studentAcd->save() && $studentHis->save()) {
                            $message['ItemID'] = $relievingMaster->id;
                            $message['type'] = 'success';
                            $message['message'] = '<div class="alert alert-block alert-success fade in">Relieving initiated successfully!</div>';
                            print_r(json_encode($message));
                            exit;
                        } else {
                            foreach ($student->getMessages() as $message) {
                                $error .= $message . "<br>";
                            }
                            foreach ($studentAcd->getMessages() as $message) {
                                $error .= $message . "<br>";
                            }
                            foreach ($studentHis->getMessages() as $message) {
                                $error .= $message . "<br>";
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        foreach ($relievingMaster->getMessages() as $message) {
                            $error .= $message . "<br>";
                        }
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $message) {
                $error .= $message . "<br>";
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function relieveStudentPermanentlyAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $itemID = $this->request->getPost('itemID');
        $relievingMaster = StudentRelievingMaster::findFirstById($itemID);
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $message = array();
        try {
            if ($this->request->isPost()) {
                $studentInfo = StudentInfo::findFirst('id =' . $relievingMaster->student_id);
                $studentAm = StudentMapping::findFirst('student_info_id =' . $relievingMaster->student_id);
                $studentHis = StudentHistory::findFirst('student_info_id =' . $relievingMaster->student_id .
                                " and aggregate_key LIKE '$studentAm->aggregate_key'");
                $relievingMaster->status = 'Relieved';
                $relievingMaster->modified_by = $uid;
                $relievingMaster->modified_date = time();
                $studentAm->status = 'Relieved';
                $studentHis->status = 'Relieved';
//                    $studentHis->student_info_id = $studentAm->student_info_id;
//                    $studentHis->aggregate_key = $studentAm->aggregate_key;
//                print_r($studentHis);exit;
                if ($relievingMaster->save()) {
                    if ($studentAm->save()) {
                        if ($studentHis->save()) {
                            $param = array(
                                'appkey' => APPKEY,
                                'subdomain' => SUBDOMAIN,
                                'businesskey' => BUSINESSKEY);
                            $param['login'] = $studentInfo->loginid;
                            $data_string = json_encode($param);
                            $response = IndexController::curlIt(USERAUTHAPI . 'deleteUser', $data_string);
//                   print_r($response);;
                            $userRemoved = json_decode($response);
//          print_r($loginCreated);;
                            if (!$userRemoved->status) {
                                $message['type'] = 'error';
                                $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                                print_r(json_encode($message));
                                exit;
                            }
                            if ($userRemoved->status == 'ERROR') {
                                $message['type'] = 'error';
                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $userRemoved->messages . '</div>';
                                print_r(json_encode($message));
                                exit;
                            } else if ($userRemoved->status == 'SUCCESS') {

                                $message['ItemID'] = $itemID;
                                $message['type'] = 'success';
                                $message['message'] = 'Relieved successfully!';
                                print_r(json_encode($message));
                                exit;
                            }
                        } else {
                            foreach ($studentHis->getMessages() as $message) {
                                $error .= $message;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {

                        foreach ($studentAm->getMessages() as $message) {
                            $error .= $message;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    foreach ($relievingMaster->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function loadjSubtreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost() && $this->request->getPost('orgvalueid') != '') {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
//            echo 'parent_id = ' . $orgvalueid . ' and  cycle_node =2 GROUP BY  org_master_id ';exit;
            $this->view->org_value = $org_value = OrganizationalStructureValues::find('parent_id = ' . $orgvalueid .
                            ' GROUP BY  org_master_id ');
//             $this->view->org_mas_value = $org_mas_value = OrganizationalStructureMaster::find('parent_id = '.$orgvalueid);
        }
    }

    public function loadrSubtreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost() && $this->request->getPost('orgvalueid') != '') {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
//            echo 'parent_id = ' . $orgvalueid . ' and  cycle_node =2 GROUP BY  org_master_id ';exit;
            $this->view->org_value = $org_value = OrganizationalStructureValues::find('parent_id = ' . $orgvalueid .
                            ' GROUP BY  org_master_id ');
//             $this->view->org_mas_value = $org_mas_value = OrganizationalStructureMaster::find('parent_id = '.$orgvalueid);
        }
    }

    public function changeApprovalStatusStudentAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        try {
            if ($this->request->isPost()) {

                $appln = StudentRelievingMaster::findFirstById($this->request->getPost('itemID'));
                $appln->status = $this->request->getPost('status');
                if ($appln->save()) {
                    $studmapping = StudentMapping::findFirst('student_info_id = ' . $appln->student_id);
                    $studmapping->status = Inclass;
                    if ($studmapping->save()) {
                        $studhistry = StudentHistory::findFirst('student_info_id = ' . $appln->student_id);
                        $studhistry->status = Inclass;
                        if ($studhistry->save()) {
                            $message['type'] = 'success';
                            $message['message'] = $appln->status . ' Successfully!';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        foreach ($studmapping->getMessages() as $messages) {
                            $error .= $messages;
                        }
                        $message['type'] = 'error';
                        $message['message'] = $error;
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    foreach ($appln->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        }
    }

    public function changeApprovalStatusStaffAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        try {
            if ($this->request->isPost()) {

                $appln = StaffInfo::findFirstById($this->request->getPost('itemID'));
                $status = $this->request->getPost('status');
                if ($status == "Rejected") {
                    $appln->relieve_status = '';
                    $appln->status = 'Appointed';
                } else {
                    $appln->relieve_status = $status;
                    $appln->status = 'Relieving initiated';
                }

                if ($appln->save()) {
                    $message['type'] = 'success';
                    $message['message'] = $appln->relieve_status . ' Successfully!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    foreach ($appln->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        }
    }

    public function viewRelieveDetailsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost() && $this->request->getPost('stuid') != '') {
            $this->view->stuid = $stuid = $this->request->getPost('stuid');
            $this->view->relivstatus = $relivstatus = UserRelieveStatus::find('user_id = ' . $stuid . ' and user_type = "student"');
        }
    }

    public function studentReliveSettingsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->fieldlist = RelievingConditions::find();
    }

    public function refreshRelieveDetailsAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost() && $this->request->getPost('stuid') != '') {
            $studentId = $this->request->getPost('stuid');
            $relievecntns = RelievingConditions::find("user_type= 'student'");
            foreach ($relievecntns as $relievecntn) {
                $qrybuild = "";
                $qrycn = array();
                if ($relievecntn->module == "Fee")
                    $qrycn[] = $relievecntn->condition;
                $qrycn[] = "student_id = " . $studentId;
                $qrybuild = StudentFeeTable::findFirst(implode(' and ', $qrycn));
                $relievecntns = UserRelieveStatus::findFirst("module='" . $relievecntn->module . "' and  user_type= 'student' and user_id = " . $studentId) ? UserRelieveStatus::findFirst("module='" . $relievecntn->module . "' and  user_type= 'student' and user_id = " . $studentId) : new UserRelieveStatus();

                $relievecntns->module = $relievecntn->module;
                $relievecntns->module_status = $qrybuild ? 0 : 1;
                $relievecntns->user_id = $studentId;
                $relievecntns->user_type = 'student';
                $relievecntns->comments = $relievecntn->condition_desc;
                if (!$relievecntns->save()) {
                    foreach ($relievecntns->getMessages() as $message) {
                        $error .= $message . "<br>";
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
            $message['type'] = 'success';
            print_r(json_encode($message));
            exit;
        }
    }

    public function updateRelievingStatusAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();

        try {
            if ($this->request->isPost()) {
                $fieldId = $this->request->getPost('itemID');
                $value = $this->request->getPost('value');
                $fieldItem = RelievingConditions::findFirstById($fieldId);
                $fieldItem->is_enabled = $value;
                if ($fieldItem->save()) {
                    $error = '';
                    $message['type'] = 'success';
                    $message['message'] = $fieldItem->module . ' Updated Successfuly!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($fieldItem->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        }
    }

}
