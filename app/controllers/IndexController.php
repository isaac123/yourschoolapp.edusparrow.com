<?php

class IndexController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
    }

    public function indexAction() {
        $this->view->setTemplateAfter('public');
        $this->tag->prependTitle("Welcome to Online School Application | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addCss('css/login.css');
        $form = new LoginForm();
        try {
            $identity = $this->auth->getIdentity();
            // If there is no identity available the user is redirected to index/index
            if (!is_array($identity) || !isset($identity['id'])) {
                if ($this->request->isPost()) {
                    if ($form->isValid($this->request->getPost()) == false) {
                        $error = '';
                        foreach ($form->getMessages() as $message) {
                            $error .= $message . '<br>';
                        }
                        $this->flash->error($error);
                    } else {
                        $data = array(
                            'login' => $this->request->getPost('login'), //'eteduspa00001',
                            'password' => $this->request->getPost('password'), //'21/12/2008',
                            'appkey' => APPKEY,
                            'subdomain' => SUBDOMAIN,
                            'businesskey' => BUSINESSKEY
                        );
                        $data_string = json_encode($data);
//                        echo '<pre>';
//                        print_r(USERAUTHAPI);
//                        echo '<br>';
//                        print_r($data_string);
//                        echo '<br>';
                        $responseParam = $this->curlIt(USERAUTHAPI . 'authenticate', $data_string);
//                        print_r($responseParam);
//                        exit;
                        $this->auth->check(array(
                            'response' => $responseParam
                        ));
                        $onboradScreen = Settings::findFirstByVariableName('settings_completed');
                        if (strtolower($onboradScreen->variableValue) != "yes") {
                            return $this->response->redirect('onboard');
                        } else {
                            return $this->response->redirect('staff/dashboard');
                        }
                    }
                }
            } else {

//                print_r($identity['role_name']);
//                exit;

                if (in_array("Student", $identity['role_name']) || in_array("Parent", $identity['role_name'])) {
                    return $this->dispatcher->forward(array(
                                'controller' => 'dashboard',
                                'action' => 'stuIndex'
                    ));
                } else if (in_array("Staff", $identity['role_name'])) {
                   return $this->response->redirect('staff/dashboard');
                } else {
                    return $this->dispatcher->forward(array(
                                'controller' => 'staff',
                                'action' => 'dashboard'
                    ));
                }
            }
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }

        $this->view->form = $form;
    }

    public function curlIt($url, $data_string) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function signupAction() {
        $this->view->setTemplateAfter('boxedlogin');
        $this->tag->prependTitle("Onboard Settings| ");
        $this->assets->addCss('css/pages.css');
        $this->view->form = $form = new SuperAdminForm();
        $code = $this->dispatcher->getParam('code');
        if ($code) {
            $confirmation_code = Settings::findFirstByVariableName('SA_access_token');
            if ($confirmation_code) {
                $token_exp = Settings::findFirstByVariableName('SA_token_isActive');
                if ($confirmation_code->variableValue != $code || strtolower($token_exp->variableValue) != "yes") {
                    return $this->dispatcher->forward(array(
                                'controller' => 'index',
                                'action' => 'index'
                    ));
                }
                $this->view->saEmail = Settings::findFirstByVariableName('SA_access_email')->variableValue;
                try {
                    if ($this->request->isPost()) {
                        if ($form->isValid($this->request->getPost()) != false) {
                            $login = $this->request->getPost('login');
                            $password = $this->request->getPost('password');
                            $data = array(
                                'login' => $login,
                                'password' => $password,
                                'appkey' => APPKEY,
                                'subdomain' => SUBDOMAIN,
                                'businesskey' => BUSINESSKEY,
                                'forcereset' => 1
                            );
                            $data_string = json_encode($data);
                            $responseParam = IndexController::curlIt(USERAUTHAPI . 'changePassword', $data_string);
//                            echo '<pre>';print_r($responseParam);exit;
                            $usrResponse = json_decode($responseParam);
                            if (!$usrResponse->status) {
                                $this->view->servererror = 'Internal server error';
                            }
                            if ($usrResponse->status == 'ERROR') {
                                $this->view->servererror = $usrResponse->messages;
                            } else if ($usrResponse->status == 'SUCCESS' && $usrResponse->data->id > 0) {
                                $responseParam2 = IndexController::curlIt(MAILSERVICEAPI . 'changePassword', $data_string);
//                                  print_r($data_string); print_r($responseParam2);exit;
                                $mailResponse = json_decode($responseParam2);
                                if (!$mailResponse->status) {
                                    $this->view->servererror = 'Internal server error';
                                }
                                if ($mailResponse->status == 'ERROR') {
                                    $this->view->servererror = $mailResponse->messages;
                                } else if ($mailResponse->status == 'SUCCESS' && $mailResponse->data->id != '') {
                                    $token_exp->variableValue = 'No';
                                    if ($token_exp->save()) {
                                         $data = array(
                            'login' => $login, //'eteduspa00001',
                            'password' => $password, //'21/12/2008',
                            'appkey' => APPKEY,
                            'subdomain' => SUBDOMAIN,
                            'businesskey' => BUSINESSKEY
                        );
                        $data_string = json_encode($data);
                        $responseParam = $this->curlIt(USERAUTHAPI . 'authenticate', $data_string);
                        $this->auth->check(array(
                            'response' => $responseParam
                        ));
			 return $this->response->redirect('onboard');
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception $e) {
                    $this->flash->error($e->getMessage());
                }
            } else {
                return $this->dispatcher->forward(array(
                            'controller' => 'onboard',
                            'action' => 'index'
                ));
            }
        }
    }

}
