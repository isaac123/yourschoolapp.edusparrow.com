<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class AdmissionController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function admitStudentAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;

        try {
            if ($this->request->isPost()) {
                $application = Application::findFirstById($this->request->getPost('itemID'));
//                $admission_prefix = (Settings::findFirstByVariableName('admission_prefix')->variableValue);

                $admissionNumPrfix = Settings::findFirstByVariableName('admission_prefix') ?
                        Settings::findFirstByVariableName('admission_prefix') : new Settings();
                $admissionNumSufix = Settings::findFirstByVariableName('admission_sufix') ?
                        Settings::findFirstByVariableName('admission_sufix') : new Settings();
                $prefix = $admissionNumPrfix ? $admissionNumPrfix->variableValue : '';
                $sufix = $admissionNumSufix ? $admissionNumSufix->variableValue : '';
                $stringLen = ControllerBase::getStudentPad('student');
                $admission_prefix = $prefix . $stringLen . $sufix;


                $stumax = StudentInfo::find();
                $adminNumArr = array();
                foreach ($stumax as $stuAdmin) {
                    $adminNumArr[] = $stuAdmin->Admission_no; // ltrim ($stuAdmin->loginid, 's');;
                }
//                 print_r($adminNumArr);
                $arraySorted = natsort($adminNumArr);
//                print_r($adminNumArr);
                $lastId = array_pop($adminNumArr);

                $asterikstr = str_replace("*", "", $admission_prefix, $asterikcount);
                $formartArr = explode('*', $admission_prefix);
                $nextAdminNumber = str_replace($formartArr, "", $lastId);
                $nextgennum = $nextAdminNumber + 1;
                //echo 'prevnextgennum:'.$nextgennum;
                //$countOfZeros = abs(strlen($nextgennum) - $asterikcount);
                //$countOfZeros++;
                $nextgennum = str_pad($nextgennum, $asterikcount, "0", STR_PAD_LEFT);

                $i = 0;
                $nextAdminNo = '';
                foreach ($formartArr as $formatStr):
                    if ($formatStr != '') {
                        $nextAdminNo .= $formatStr;
                    } else {
                        if ($i == 0)
                            $nextAdminNo .= $nextgennum;
                        $i = 1;
                    }
                endforeach;

                //echo 'lastid:'.$lastId.',asterikstr:'.$asterikstr.'asterikcount:'.$asterikcount.'nextAdminNumber:'.$nextAdminNumber.'nextgennum:'.$nextgennum.'countOfZeros:'.$countOfZeros.'$nextAdminNo:'.$nextAdminNo;
//                print_r($formartArr);exit;
//                $admission_no = $admission_prefix . ($stumax ? ($stumax + 1) : 1);
                ##Student Login  
                $param = array(
                    'appkey' => APPKEY,
                    'subdomain' => SUBDOMAIN,
                    'businesskey' => BUSINESSKEY);
                $param['slogin'] = 's' . $nextAdminNo;
                $param['plogin'] = 'p' . $nextAdminNo;
                $param['password'] = date('d/m/Y', $application->Date_of_Birth);
                $param['email'] = $application->Email ? $application->Email : ('s' . $nextAdminNo . '@' . SUBDOMAIN . '.edusparrow.com');
                $data_string = json_encode($param);
                $response = IndexController::curlIt(USERAUTHAPI . 'createStudentLogin', $data_string);
//                   print_r($response);;exit;
                $loginCreated = json_decode($response);
//          print_r($loginCreated);;
                if (!$loginCreated->status) {
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                    print_r(json_encode($message));
                    exit;
                }
                ##Student LOGIN CREATION END
                if ($loginCreated->status == 'ERROR') {
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $loginCreated->messages . '</div>';
                    print_r(json_encode($message));
                    exit;
                } else if ($loginCreated->status == 'SUCCESS' && $loginCreated->data->id > 0) {

                    ##MAIL LOGIN
                    $param = array(
                        'domain' => SUBDOMAIN,
                        'login' => 's' . $nextAdminNo,
                        'password' => date('d/m/Y', $application->Date_of_Birth));
                    $mail_data_string = json_encode($param);
//                      print_r($param); 
                    $response = IndexController::curlIt(MAILSERVICEAPI . 'createWebMailUser', $mail_data_string);
//                   print_r($response);;
                    $mailloginCreated = json_decode($response);
//                    print_r($mailloginCreated);
//                    exit;
                    if (!$mailloginCreated->status) {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                    if ($mailloginCreated->status == 'ERROR') {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $mailloginCreated->messages . '</div>';
                        print_r(json_encode($message));
                        exit;
                    } else if ($mailloginCreated->status == 'SUCCESS' && $mailloginCreated->data->id) {

                        ## Admit Student
                        $stuInfo = new StudentInfo();
                        $stuMapping = new StudentMapping();
                        $stuInfo->assign(array(
                            'Admission_no' => $nextAdminNo,
                            'application_no' => $application->application_no,
                            'Student_Name' => $application->Student_Name,
                            'Gender' => $application->Gender,
                            'Date_of_Birth' => $application->Date_of_Birth,
                            'Date_of_Joining' => time(), //$application->Date_of_Joining,
                            'Address1' => $application->Address1,
                            'Address2' => $application->Address2,
                            'State' => $application->State,
                            'Country' => $application->Country,
                            'Pin' => $application->Pin,
                            'Phone' => $application->Phone,
                            'Email' => $application->Email ? $application->Email : ('s' . $nextAdminNo . '@' . SUBDOMAIN . '.edusparrow.com'),
                            'loginid' => $loginCreated->data->stulogin,
                            'parent_loginid' => $loginCreated->data->parentlogin,
                        ));

                        $stuInfo->created_by = $uid;
                        $stuInfo->created_date = time();
                        $stuInfo->modified_by = $uid;
                        $stuInfo->modified_date = time();
                        if ($stuInfo->save()) {
                            $stuMapping->student_info_id = $stuInfo->id;
                            $stuMapping->aggregate_key = $application->aggregate_key;

                            $admit_stu_val = Settings::findFirst('variableName ="admit_student"');

                            $stu_status = 'Admitted';
                            if ($admit_stu_val->variableValue == '1')
                                $stu_status = 'Inclass';

                            $stuMapping->status = $stu_status;

                            $stuAcademic = new StudentHistory();
                            $stuAcademic->student_info_id = $stuInfo->id;
                            $stuAcademic->aggregate_key = $application->aggregate_key;
                            $stuAcademic->status = $stu_status;
                            if (!$stuAcademic->save()) {
                                $error = '';
                                foreach ($stuAcademic->getMessages() as $messages) {
                                    $error .= $messages;
                                }
                                $message['type'] = 'error';
                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                print_r(json_encode($message));
                                exit;
                            } else if (!$stuMapping->save()) {
                                $error = '';
                                foreach ($stuMapping->getMessages() as $messages) {
                                    $error .= $messages;
                                }
                                $message['type'] = 'error';
                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                print_r(json_encode($message));
                                exit;
                            } else {

                                $appln = Application::findFirstById($this->request->getPost('itemID'));
                                $admit_stu_val = Settings::findFirst('variableName ="admit_student"');

                                $stu_status = 'Admitted';
                                if ($admit_stu_val->variableValue == '1')
                                    $stu_status = 'Inclass';

                                $appln->status = $stu_status;
                                $appln->key_in_status = 'In Progress';
                                $appln->modified_by = $uid;
                                $appln->modified_on = time();
                                $appln->Date_of_Joining = $stuInfo->Date_of_Joining;
                                $gender = ($stuInfo->Gender == '2') ? 'Male' : 'Female';
                                if ($appln->save()) {

                                    //calendar creation 
                                    /* $params['username'] = $stuInfo->loginid;
                                      $params['password'] = date('d/m/Y', $stuInfo->Date_of_Birth);
                                      $params['fullname'] = $stuInfo->Student_Name;
                                      $params['email_address'] = $stuInfo->Email;
                                      $params['rolenames'] = 'Student';
                                      $params['aggregateid'] = $stuMapping->aggregate_key;
                                      // print_r($params);
                                      $data_string = json_encode($params);
                                      $responseParam = IndexController::curlIt(CALENDARAPI . 'createUserPricipal', $data_string);
                                      $calloginCreated = json_decode($responseParam);
                                      // print_r($calloginCreated);
                                      if (!$calloginCreated->status) {
                                      $message['type'] = 'error';
                                      $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                                      print_r(json_encode($message));
                                      exit;
                                      } else if ($calloginCreated->status == 'ERROR') {
                                      $message['type'] = 'error';
                                      $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $loginCreated->messages . '</div>';
                                      print_r(json_encode($message));
                                      exit;
                                      } else if ($calloginCreated->status == 'SUCCESS' && $calloginCreated->data->id > 0) { */
                                    $message['type'] = 'success';
                                    $message['message'] = $messageparam = '<div class="alert alert-block alert-success fade in">'
                                            . ' <b>Student Admitted successfully </b>'
                                            . "<table><tr><td> Student's Name </td><td>:</td><td> " . $application->Student_Name . "</td></tr>
                      <tr><td> Admission No  </td><td>:</td><td> " . $stuInfo->Admission_no . "</td></tr>
                      <tr><td> Gender  </td><td>:</td><td> " . $gender . "</td></tr>
                      <tr><td> Student's LoginId  </td><td>:</td><td> " . $stuInfo->loginid . "</td></tr>
                      <tr><td> Parents LoginId  </td><td>:</td><td> " . $stuInfo->parent_loginid . "</td></tr></table>
                      </div>";
                                    print_r(json_encode($message));
                                    exit;
//                                    }
                                } else {
                                    foreach ($appln->getMessages() as $message) {
                                        $error .= $message;
                                    }
                                    $message['type'] = 'error';
                                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                    print_r(json_encode($message));
                                    exit;
                                }
                            }
                        } else {
                            $error = '';
                            foreach ($stuInfo->getMessages() as $message) {
                                $error .= $message;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                    ##MAIL LOGIN CREATION END
                } else {
                    $error = '';
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-error">Login Creation failed!<br/>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function _createLogin($param) {
        $user = new Users();
        $userRole = new UserRoles();
        $user->assign(array(
            'login' => $param['login'],
            'email' => $param['email'],
            'active' => 'Y',
            'password' => $this->security->hash($param['password'])
        ));
        if ($user->save()) {
            $userRole->role_name = $param['role'];
            $userRole->user = $user;
            if ($userRole->save()) {
                return $user->id;
            }
        }
        return "0";
    }

}
