<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Mvc\View;

class UsersController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->persistent->conditions = null;
        return $this->dispatcher->forward(array(
                    'controller' => 'users',
                    'action' => 'login'
        ));
    }

    public function logoutAction() {
        $this->auth->remove();

//        $data = array(
//            'sesid' => $_COOKIE['roundcube_sessid']
//        );
//        $data_string = json_encode($data);
//         print_r($_COOKIE);    exit;
//        $res = IndexController::curlIt(MAILSERVICEAPI . 'logoutWebMailUser', $data_string);
//        $res = IndexController::curlIt(MAILCLIENT . '/?_task=logout', $data_string);
//              print_r($_COOKIE);        print_r(MAILCLIENT . '/?_task=logout');  print_r($res);exit;
//         return $this->dispatcher->forward(array(
//          'controller' => 'index',
//          'action' => 'index'
//          )); 
         return $this->response->redirect('index');
//        return $this->response->redirect(MAILCLIENT . '/?_task=logoutfromapp');
    }

    /**
     * Shows the forgot password form
     */
    public function forgotPasswordAction() {


        $this->tag->prependTitle("Forgot Password | ");

        $this->assets->addCss('css/edustyle.css');
        $form = new ForgotPasswordForm();

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) != false) {


                $user = Users::findFirstByEmail($this->request->getPost('email'));
                if ($user) {

                    $resetPassword = new ResetPasswords();
                    $resetPassword->usersId = $user->id;
                    if ($resetPassword->save()) {
                        $this->flash->success('Success! Please check your inbox for an email reset password');
                    }
                } else {

                    $this->flash->error('There is no account associated to this email');
                    return $this->dispatcher->forward(array(
                                'controller' => 'index',
                                'action' => 'index'
                    ));
                }
                $this->flash->error($resetPassword->getMessages());
            }
        }

        $this->view->form = $form;
    }

    public function resetPasswordAction() {
        $this->tag->prependTitle("Reset Password | ");

        $this->assets->addCss('css/edustyle.css');
        $code = $this->dispatcher->getParam('code');

        $resetPassword = ResetPasswords::findFirstByCode($code);
        if (!$resetPassword) {
            return $this->dispatcher->forward(array(
                        'controller' => 'index',
                        'action' => 'index'
            ));
        }

        if ($resetPassword->reset != 'N') {
            return $this->dispatcher->forward(array(
                        'controller' => 'index',
                        'action' => 'index'
            ));
        }

        $resetPassword->reset = 'Y';

        /**
         * Change the confirmation to 'reset'
         */
        if (!$resetPassword->save()) {

            foreach ($resetPassword->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                        'controller' => 'index',
                        'action' => 'index'
            ));
        }
        /**
         * Identify the user in the application
         */
        $this->auth->authUserById($resetPassword->usersId);

        $this->flash->success('Please reset your password');

        return $this->dispatcher->forward(array(
                    'controller' => 'users',
                    'action' => 'changePassword'
        ));
    }

    public function changePassSettingsAction() {
// Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->changepassForm = new ChangePasswordForm();
    }

    /**
     * Users must use this action to change its password
     */
    public function changePasswordAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $form = new ChangePasswordForm();
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) == false) {
                $error = '';
                foreach ($this->request->getPost() as $key => $val) {
                    foreach ($form->getMessagesFor($key) as $messages) {
                        $error .= $messages . "\n";
                    }
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            } else {
                try {
                    $oldpassword = $this->request->getPost('oldpassword');
                    $newpassword = $this->request->getPost('password');
                    $login = $identity['name'];
                    $data = array(
                        'login' => $login,
                        'oldPassoword' => $oldpassword,
                        'password' => $newpassword,
                        'appkey' => APPKEY,
                        'subdomain' => SUBDOMAIN,
                        'businesskey' => BUSINESSKEY
                    );
//                    print_r(USERAUTHAPI . 'changePassword');
                    $data_string = json_encode($data);
                    $responseParam = IndexController::curlIt(USERAUTHAPI . 'changePassword', $data_string);
                    //                 print_r($responseParam);
                    $usrResponse = json_decode($responseParam);
                    if (!$usrResponse->status) {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                    ##Student Password change END
                    if ($usrResponse->status == 'ERROR') {
                        $message['type'] = 'error';
//                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $usrResponse->messages . '</div>';
                        $message['message'] = $usrResponse->messages;
                        print_r(json_encode($message));
                        exit;
                    } else if ($usrResponse->status == 'SUCCESS' && $usrResponse->data->id > 0) {
                        ##Student Mail password change END
                        $responseParam2 = IndexController::curlIt(MAILSERVICEAPI . 'changePassword', $data_string);
                        $mailResponse = json_decode($responseParam2);
                        //                           print_r($mailResponse);
                        if (!$mailResponse->status) {
                            $message['type'] = 'error';
//                            $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                            $message['message'] = 'Internal server error';
                            print_r(json_encode($message));
                            exit;
                        }
                        if ($mailResponse->status == 'ERROR') {
                            $message['type'] = 'error';
//                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $usrResponse->messages . '</div>';
                            $message['message'] = $mailResponse->messages;
                            print_r(json_encode($message));
                            exit;
                        } else if ($mailResponse->status == 'SUCCESS' && $mailResponse->data->id != '') {
                            ##Student calendar password change END
                            /*$responseParam3 = IndexController::curlIt(CALENDARAPI . 'changePassword', $data_string);
                            $calResponse = json_decode($responseParam3);
                            //print_r($calResponse);
                            if (!$calResponse->status) {
                                $message['type'] = 'error';
                                $message['message'] = 'Internal server error';
//                                $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                                print_r(json_encode($message));
                                exit;
                            }
                            ##Student LOGIN CREATION END
                            if ($calResponse->status == 'ERROR') {
                                $message['type'] = 'error';
//                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $usrResponse->messages . '</div>';
                                $message['message'] = $calResponse->messages;
                                print_r(json_encode($message));
                                exit;
                            } else if ($calResponse->status == 'SUCCESS' && $calResponse->data->id != '') {

                            }*/
                                $message['type'] = 'success';
                                $message['message'] = 'Password changed successfully';
                                print_r(json_encode($message));
                                exit;
                        }
                    }
                } catch (Exception $ex) {
                    foreach ($ex->getMessages() as $message) {
                        $error .= $message . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function usersManagementAction() {
        // $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Users | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/users/users.js');
    }

    public function usertableHeaderAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function usertableDataAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = $queryParams1 = array();
        $orderphql = array();

        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }
        if ($params['val'] == 'Staff') {
            $type = 'Staff';
            $queryParams[] = 'status IN ("Appointed","Relieved","Appoint","initiate Relieving")';
            if (isset($params['sSearch']) && $params['sSearch'] != ''):
                $queryParams1[] = 'Staff_Name Like "%' . $params['sSearch'] . '%"';
                $queryParams1[] = 'Date_of_Birth >= "' . strtotime($params['sSearch'] . ' 00:00:00') . '" and Date_of_Birth <= "' . strtotime($params['sSearch'] . ' 23:59:59') . '"';
                $queryParams1[] = 'loginid Like "%' . $params['sSearch'] . '%"';
                $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
            endif;
            for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
                $orderphql[] = $this->getSortColumnNameForApplication($this->request->getPost('iSortCol_' . $i)) . ' ' .
                        $this->request->getPost('sSortDir_' . $i);
            }
            $data = StaffInfo::find(array(
                        implode(' and ', $queryParams),
                        "order" => $order,
                        "limit" => array(
                            "number" => $this->request->getPost('iDisplayLength'),
                            "offset" => $this->request->getPost('iDisplayStart')
                        )
            ));

            $totapplications = StaffInfo::find(array(
                        implode(' and ', $queryParams),
                        "columns" => "COUNT(*) as cnt",
            ));
        }
        if ($params['val'] == 'Student' || $params['val'] == 'Parent') {
            $type = $params['val'] == 'Parent' ? 'Parent' : 'Student';
            if (isset($params['sSearch']) && $params['sSearch'] != ''):
                $queryParams1[] = 'Student_Name Like "%' . $params['sSearch'] . '%"';
                $queryParams1[] = 'Date_of_Birth >= "' . strtotime($params['sSearch'] . ' 00:00:00') . '" and Date_of_Birth <= "' . strtotime($params['sSearch'] . ' 23:59:59') . '"';
                $queryParams1[] = $params['val'] == 'Parent' ? 'parent_loginid Like "%' . $params['sSearch'] . '%"' : 'loginid Like "%' . $params['sSearch'] . '%"';
                ;
                $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
            endif;
            for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
                $orderphql[] = $this->getSortColumnNameForApplication($this->request->getPost('iSortCol_' . $i)) . ' ' .
                        $this->request->getPost('sSortDir_' . $i);
            }
            $data = StudentInfo::find(array(
                        implode(' and ', $queryParams),
                        "order" => $order,
                        "limit" => array(
                            "number" => $this->request->getPost('iDisplayLength'),
                            "offset" => $this->request->getPost('iDisplayStart')
                        )
            ));
            $totapplications = StudentInfo::find(array(
                        "columns" => "COUNT(*) as cnt",
            ));
        }
        $rowEntries = $this->formatApplicationTableData($data, $type);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => (count($data)),
            "iTotalDisplayRecords" => (intval($totapplications[0]->cnt)),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForApplication($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "id";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "id";

            default:
                return "id";
        }
    }

    public function formatApplicationTableData($result, $type) {
        $rowEntries1 = array();
        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $row['id'] = $items->id ? $items->id : '';
                $row['name'] = $items->Staff_Name ? $items->Staff_Name : ( $type == 'Parent' ? $items->Student_Name . "'s Parent" : $items->Student_Name);
                $loginid = $type != 'Parent' ? $type . ',' . $items->loginid . ',' . date('d-m-Y', $items->Date_of_Birth) : $type . ',' . $items->parent_loginid . ',' . date('d-m-Y', $items->Date_of_Birth);
                $row['login'] = $type != 'Parent' ? $items->loginid : $items->parent_loginid;
                $row['type'] = $type;
                $row['dob'] = date('d-m-Y', $items->Date_of_Birth);
                $data = array(
                    'login' => $type != 'Parent' ? $items->loginid : $items->parent_loginid,
                    'appkey' => APPKEY,
                    'subdomain' => SUBDOMAIN,
                    'businesskey' => BUSINESSKEY
                );
                $data_string = json_encode($data);
                $responseParam = IndexController::curlIt(USERAUTHAPI . 'getallUSerDetails', $data_string);
                $arr = json_decode($responseParam);
                $row['email'] = $arr->data->email;
                $row['status'] = ($arr->data->active == 'Y') ? "Active" : (($arr->data->active == 'N') ? "InActive" : 'Nil');

                $row['actions'] = '<button class="btn btn-success pull-right" style="height:35px;width:110px" type="button"
                                                    onclick="usersSettings.resetPassword(' . "'$loginid'" . ')"  >
                                                   Reset</button> ';
                $row['active'] = '<a href="javascript:;" onclick="usersSettings.checkstatus(' . "'$loginid'" . ')"> 
                                                <span class="fa fa-edit mini-stat-icon-action pink">                                         
                                                </span>
                                            </a>';

                $rowEntries[] = $row;
            endforeach;
        }
        return $rowEntries;
    }

    public function changeUserPasswordAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        if ($this->request->isPost()) {
            try {
                $params = $this->request->getPost('loginid');
                $logindet = explode(',', $params);
                $password = str_replace('-', '/', $logindet[2]);
                $login = $logindet[1];
                $data = array(
                    'login' => $login,
                    'password' => $password,
                    'appkey' => APPKEY,
                    'subdomain' => SUBDOMAIN,
                    'businesskey' => BUSINESSKEY,
                    'forcereset' => 1
                );
                $data_string = json_encode($data);
                $responseParam = IndexController::curlIt(USERAUTHAPI . 'changePassword', $data_string);
                $usrResponse = json_decode($responseParam);
                if (!$usrResponse->status) {
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                    print_r(json_encode($message));
                    exit;
                }
                if ($usrResponse->status == 'ERROR') {
                    $message['type'] = 'error';
//                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $usrResponse->messages . '</div>';
                    $message['message'] = $usrResponse->messages;
                    print_r(json_encode($message));
                    exit;
                } else if ($usrResponse->status == 'SUCCESS' && $usrResponse->data->id > 0) {
                    ##Student Mail password change END
                    $responseParam2 = IndexController::curlIt(MAILSERVICEAPI . 'changePassword', $data_string);
                    $mailResponse = json_decode($responseParam2);
                    //                           print_r($mailResponse);
                    if (!$mailResponse->status) {
                        $message['type'] = 'error';
//                            $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                        $message['message'] = 'Internal server error';
                        print_r(json_encode($message));
                        exit;
                    }
                    if ($mailResponse->status == 'ERROR') {
                        $message['type'] = 'error';
//                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $usrResponse->messages . '</div>';
                        $message['message'] = $mailResponse->messages;
                        print_r(json_encode($message));
                        exit;
                    } else if ($mailResponse->status == 'SUCCESS' && $mailResponse->data->id != '') {
                        ##Student calendar password change END
                        /*$responseParam3 = IndexController::curlIt(CALENDARAPI . 'changePassword', $data_string);
                        $calResponse = json_decode($responseParam3);
                        //print_r($calResponse);
                        if (!$calResponse->status) {
                            $message['type'] = 'error';
                            $message['message'] = 'Internal server error';
//                                $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                        ##Student LOGIN CREATION END
                        if ($calResponse->status == 'ERROR') {
                            $message['type'] = 'error';
//                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $usrResponse->messages . '</div>';
                            $message['message'] = $calResponse->messages;
                            print_r(json_encode($message));
                            exit;
                        } else if ($calResponse->status == 'SUCCESS' && $calResponse->data->id != '') {
                            
                        }*/
                        $message['type'] = 'success';
                            $message['message'] = 'Password Changed as ' . $logindet[2];
                            print_r(json_encode($message));
                            exit;
                    }
                }
            } catch (Exception $ex) {
                foreach ($ex->getMessages() as $message) {
                    $error .= $message . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function activeProfileAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->params = $params = $this->request->getPost('loginid');
            $logindet = explode(',', $params);
            $login = $logindet[1];
            $data = array(
                'login' => $login,
                'password' => $logindet[2],
                'appkey' => APPKEY,
                'subdomain' => SUBDOMAIN,
                'businesskey' => BUSINESSKEY,
            );
            $data_string = json_encode($data);
            $responseParam = IndexController::curlIt(USERAUTHAPI . 'getallUSerDetails', $data_string);
            $arr = json_decode($responseParam);
            $this->view->usrResponse = $arr->data;
        }
    }

    public function inactiveStatusAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        if ($this->request->isPost()) {
            try {
                $params = $this->request->getPost('loginid');
                $logindet = explode(',', $params);
                $login = $logindet[1];
                $data = array(
                    'login' => $login,
                    'appkey' => APPKEY,
                    'subdomain' => SUBDOMAIN,
                    'businesskey' => BUSINESSKEY
                );
                $data_string = json_encode($data);
                $responseParam = IndexController::curlIt(USERAUTHAPI . 'inactiveUserStatus', $data_string);
                $usrResponse = json_decode($responseParam);
                if (!$usrResponse->status) {
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                    print_r(json_encode($message));
                    exit;
                }
                if ($usrResponse->status == 'ERROR') {
                    $message['type'] = 'error';
                    $message['message'] = $usrResponse->messages;
                    print_r(json_encode($message));
                    exit;
                } else {
                    $message['type'] = 'success';
                    $message['message'] = ' Inactivated successfully';
                    print_r(json_encode($message));
                    exit;
                }
            } catch (Exception $ex) {
                foreach ($ex->getMessages() as $message) {
                    $error .= $message . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function activeStatusAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        if ($this->request->isPost()) {
            try {
                $params = $this->request->getPost('loginid');
                $logindet = explode(',', $params);
                $login = $logindet[1];
                $data = array(
                    'login' => $login,
                    'appkey' => APPKEY,
                    'subdomain' => SUBDOMAIN,
                    'businesskey' => BUSINESSKEY
                );
                $data_string = json_encode($data);
                $responseParam = IndexController::curlIt(USERAUTHAPI . 'activeUserStatus', $data_string);
                $usrResponse = json_decode($responseParam);
                if (!$usrResponse->status) {
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                    print_r(json_encode($message));
                    exit;
                }
                if ($usrResponse->status == 'ERROR') {
                    $message['type'] = 'error';
                    $message['message'] = $usrResponse->messages;
                    print_r(json_encode($message));
                    exit;
                } else {
                    $message['type'] = 'success';
                    $message['message'] = ' User Activated successfully';
                    print_r(json_encode($message));
                    exit;
                }
            } catch (Exception $ex) {
                foreach ($ex->getMessages() as $message) {
                    $error .= $message . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function checkstatusAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $this->request->getPost('loginid');
        $logindet = explode(',', $params);
        $login = $logindet[1];
        $data = array(
            'login' => $login,
            'appkey' => APPKEY,
            'subdomain' => SUBDOMAIN,
            'businesskey' => BUSINESSKEY
        );
        $data_string = json_encode($data);
        $responseParam = IndexController::curlIt(USERAUTHAPI . 'getallUSerDetails', $data_string);
        $arr = json_decode($responseParam);
        $message['status'] = $arr->data->active;
        print_r(json_encode($message));
        exit;
    }

    public function userDeleteAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $this->request->getPost('loginid');
        $logindet = explode(',', $params);
        if ($logindet[0] == 'Student' || $logindet[0] == 'Parent'):
            $studinfo = $logindet[0] == 'Student' ? StudentInfo::findFirst('loginid = "' . $logindet[1] . '"') : StudentInfo::findFirst('parent_loginid = "' . $logindet[1] . '"');
            $studfee = StudentFeeTable::find('student_id =' . $studinfo->id . ' and status IN ("Paid","Partially Paid")');
            if (!$studfee) {
                $studfeedel = StudentFeeTable::find('student_id =' . $studinfo->id);
                foreach ($studfeedel as $stud) {
                    $stud->delete();
                }
                $appfee = Application::findFirst('application_no =' . $studinfo->application_no . ' and status IN ("Paid","Partially Paid")');
                if (!$appfee) {
                    if ($appfee->delete()) {
                        $apprvtype = ApprovalTypes::findFirst('approval_type = "Application"')->id;
                        $apprvmas = ApprovalMaster::findFirst('approval_type_id =' . $apprvtype . ' and Item_id =' . $appfee->id);
                        if ($apprvmas->delete()) {
                            $apprvitem = ApprovalItem::findFirst('approval_master_id =' . $apprvmas->id);
                            if (!$apprvitem->delete()) {
                                foreach ($apprvitem->getMessages() as $messages) {
                                    $error .= $messages;
                                }
                                $message['type'] = 'error';
                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                print_r(json_encode($message));
                                exit;
                            }
                        }
                    }
                }
            } else {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in"> Unable to Delete</div>';
                print_r(json_encode($message));
                exit;
            }
            $studmap = StudentMapping::findFirst('student_info_id =' . $studinfo->id);
            $studhistory = StudentHistory::findFirst('student_info_id =' . $studinfo->id);
            if ($studinfo->delete() && $studmap->delete() && $studhistory->delete()) {
                $data = array(
                    'login' => $logindet[1],
                    'appkey' => APPKEY,
                    'subdomain' => SUBDOMAIN,
                    'businesskey' => BUSINESSKEY
                );
                $data_string = json_encode($data);
                $responseParam = IndexController::curlIt(USERAUTHAPI . 'userDelete', $data_string);
                $usrResponse = json_decode($responseParam);
                if ($usrResponse->status == 'SUCCESS') {
                    $responseParam2 = IndexController::curlIt(MAILSERVICEAPI . 'userDelete', $data_string);
                    $mailResponse = json_decode($responseParam2);
                    if ($mailResponse->status == 'SUCCESS') {
                       /* $responseParam3 = IndexController::curlIt(CALENDARAPI . 'userDelete', $data_string);
                        $calResponse = json_decode($responseParam3);
                        if ($mailResponse->status == 'SUCCESS') {
                            $message['type'] = 'success';
                            $message['ItemID'] = $logindet[1];
                            $message['message'] = '<div class="alert alert-block alert-success fade in">Deleted Successfully</div>';
                            print_r(json_encode($message));
                            exit;
                        }*/
                        $message['type'] = 'success';
                            $message['ItemID'] = $logindet[1];
                            $message['message'] = '<div class="alert alert-block alert-success fade in">Deleted Successfully</div>';
                            print_r(json_encode($message));
                            exit;
                    }
                }
            } else {
                foreach ($studmap->getMessages() as $messages) {
                    $error .= $messages;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            }
        endif;
        if ($logindet[0] == 'Staff') {
            $staffinfo = StaffInfo::findFirst('loginid = "' . $logindet[1] . '"');
            if ($staffinfo->delete()) {
                $data = array(
                    'login' => $logindet[1],
                    'appkey' => APPKEY,
                    'subdomain' => SUBDOMAIN,
                    'businesskey' => BUSINESSKEY
                );
                $data_string = json_encode($data);
                $responseParam = IndexController::curlIt(USERAUTHAPI . 'userDelete', $data_string);
                $usrResponse = json_decode($responseParam);
                if ($usrResponse->status == 'SUCCESS') {
                    $responseParam2 = IndexController::curlIt(MAILSERVICEAPI . 'userDelete', $data_string);
                    $mailResponse = json_decode($responseParam2);
                    if ($mailResponse->status == 'SUCCESS') {
                       /* $responseParam3 = IndexController::curlIt(CALENDARAPI . 'userDelete', $data_string);
                        $calResponse = json_decode($responseParam3);
                        if ($mailResponse->status == 'SUCCESS') {
                           
                        }*/
                         $message['type'] = 'success';
                            $message['ItemID'] = $logindet[1];
                            $message['message'] = '<div class="alert alert-block alert-success fade in">Deleted Successfully</div>';
                            print_r(json_encode($message));
                            exit;
                    }
                }
            } else {
                foreach ($studinfo->getMessages() as $messages) {
                    $error .= $messages;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

}
