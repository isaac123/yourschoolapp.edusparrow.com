<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class UsersroleController extends ControllerBase {

    /**
     * The filepath of the ACL role file  
     *
     * @var string
     * */
    private $filename = 'role_resource.json';

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->tag->prependTitle("Assigning | ");
        $this->assets->addJs("js/assigning/assign.js");
        if (file_exists(DATA_DIR . $this->filename)) {
            // Get the ACL from the data file
            $jdata = file_get_contents(DATA_DIR . $this->filename);
            $rolelst = json_decode($jdata);
            $rolearr = $rolelst->Roles;
            $roles = array();
            /* foreach ($rolearr->Default_Roles as $role) {
              $roles[] = $role;
              } */
            foreach ($rolearr->Communicate_Roles as $role) {
                $roles[] = $role;
            }
            foreach ($rolearr->Cycles_Roles as $role) {
                $roles[] = $role;
            }
            foreach ($rolearr->StudentCycle_Roles as $role) {
                $roles[] = $role;
            }
            foreach ($rolearr->StaffCycle_Roles as $role) {
               $roles[] = $role;
            }
            foreach ($rolearr->FinanceCycle_Roles as $role) {
                 $roles[] = $role;
            }
            foreach ($rolearr->Transport_Roles as $role) {
                 $roles[] = $role;
            }
            foreach ($rolearr->Other_Roles as $role) {
                $roles[] = $role;
            }
            foreach ($rolearr->Administrator_Roles as $role) {
                 $roles[] = $role;
            }
            $this->view->roles = $roles;
        }

        $data = array('subdomain' => SUBDOMAIN);
        $datastring = json_encode($data);
        $responseParam = IndexController::curlIt(USERAUTHAPI . 'getUsersDetails', $datastring);
//              print_r($responseParam);exit;

        $usrResponse = json_decode($responseParam);

//                     echo '<pre>';
//                        print_r($usrResponse->messages);
//                        exit;
//                        
        if (!$usrResponse->status) {
//                         echo 'success - not';
//                         exit;
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
            print_r(json_encode($message));
            exit;
        }
        ##User Roles Details Not Avail
        if ($usrResponse->status == 'ERROR') {
//                         echo 'success - error';
//                         exit;
            $message['type'] = 'error';
//                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $usrResponse->messages . '</div>';
            $message['message'] = $usrResponse->messages;
            print_r(json_encode($message));
            exit;
        } else if ($usrResponse->status == 'SUCCESS') {

//                        echo 'success';
//                        echo '<pre>';
//                        print_r($usrResponse->messages);
//                        exit;
            $this->view->rolesdetfromUserRole = $usrResponse->messages;
        }
    }

    public function assignUsrRolesByStaffAction() {
        
        $this->tag->prependTitle("Role Assigning | ");
        $this->assets->addJs("js/appscripts/userrole/rolemenu.js");
    }
    public function loadStaffRolesAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
         if ($this->request->isPost()) {
              $staffid = $this->request->getPost('stflogin');
              $this->view->user = StaffInfo::findFirstByLoginid($staffid);
        if (file_exists(DATA_DIR . $this->filename)) {
            // Get the ACL from the data file
            $jdata = file_get_contents(DATA_DIR . $this->filename);
            $rolelst = json_decode($jdata);
            $rolearr = $rolelst->Roles;
            $roles = array();
            
            foreach ($rolearr->Communicate_Roles as $key=> $role) {
                $roles['Communicate_Roles'][] = $role;
            }
            foreach ($rolearr->Cycles_Roles as $key=> $role) {
                $roles['Cycles_Roles'][] = $role;
            }
            foreach ($rolearr->StudentCycle_Roles as $key=> $role) {
                $roles['StudentCycle_Roles'][] = $role;
            }
            foreach ($rolearr->StaffCycle_Roles as $key=> $role) {
               $roles['StaffCycle_Roles'][] = $role;
            }
            foreach ($rolearr->FinanceCycle_Roles as $key=> $role) {
                 $roles['FinanceCycle_Roles'][] = $role;
            }
            foreach ($rolearr->Other_Roles as $key=> $role) {
                $roles['Role_Management'][] = $role;
            }
            foreach ($rolearr->Transport_Roles as $key=> $role) {
                 $roles['Transport_Roles'][] = $role;
            }
            foreach ($rolearr->Administrator_Roles as $key=> $role) {
                 $roles['Administrator_Roles'][] = $role;
            }
            $this->view->roles = $roles;
        }
        $data = array('subdomain' => SUBDOMAIN, 'stflogin'=>$staffid);
        $datastring = json_encode($data);
        $responseParam = IndexController::curlIt(USERAUTHAPI . 'getUsersDetailsById', $datastring);
        $usrResponse = json_decode($responseParam);
        if (!$usrResponse->status) {
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
            print_r(json_encode($message));
            exit;
        }
        ##User Roles Details Not Avail
        if ($usrResponse->status == 'ERROR') {
            $message['type'] = 'error';$message['message'] = $usrResponse->messages;
            print_r(json_encode($message));
            exit;
        } else if ($usrResponse->status == 'SUCCESS') {
            $this->view->rolesdetfromUserRole = $usrResponse->messages;
        }
         }
    }
    public function assignUsrRolesAction() {
        // no display
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
//        print_r($this->request->getPost());exit;
            $message = array();
        if ($this->request->isPost()) {
            $params = array();
            foreach ($this->request->getPost() as $key => $value) {
                $params[$key] = $value;
            }
            $params['subdomain']= SUBDOMAIN;
            $roledata = json_encode($params);
            $responseParam = IndexController::curlIt(USERAUTHAPI . 'saveUsrRoleDetails', $roledata);
//            print_r($responseParam);exit;
            $usrResponse = json_decode($responseParam);
            if (!$usrResponse->status) {
//                         echo 'success - not';
//                         exit;
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                print_r(json_encode($message));
                exit;
            }
            ##User Roles Details Not Avail
            if ($usrResponse->status == 'ERROR') {
//                         echo 'success - error';
//                         exit;
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $usrResponse->messages . '</div>';
                //  $message['message'] = $usrResponse->messages;
                print_r(json_encode($message));
                exit;
            } else if ($usrResponse->status == 'SUCCESS') {

                $message['type'] = 'success';
                $message['message'] = '<div class="alert alert-block alert-success">User Roles Updated Successfully.!</div>';
                print_r(json_encode($message));
                exit;
            }
        } else {
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">Select role to assign</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function roleMenuResourceAction() {
        $this->tag->prependTitle("Assigning | ");
        $this->assets->addJs("js/userrole/rolemenu.js");
        $this->assets->addCss('css/edustyle.css');
        if (file_exists(DATA_DIR . $this->filename)) {
            // Get the ACL from the data file
            $jdata = file_get_contents(DATA_DIR . $this->filename);
            $rolelst = json_decode($jdata);
            $rolearr = $rolelst->Roles;
            $roles = array();
            foreach ($rolearr->Default_Roles as $role) {
                $roles[] = $role;
            }
            foreach ($rolearr->Office_Roles as $role) {
                $roles[] = $role;
            }
            foreach ($rolearr->Administrator_Roles as $role) {
                $roles[] = $role;
            }
            foreach ($rolearr->Academic_Roles as $role) {
                $roles[] = $role;
            }
            $this->view->roles = $roles;
        }
    }

    public function assignRolesMenuAction() {
        // no display
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        //print_r($this->request->getPost());exit;
        if ($this->request->isPost()) {


            $menuActive = 0;
            if (count($this->request->getPost('on')) > 0) {
                foreach ($this->request->getPost('on') as $key => $value) {
                    $usrrole = explode('_', $key);
                    $mid = $usrrole[1];
                    $rolename = $usrrole[2];
                    $menu = MenuResources::findFirstById($mid);
                    $menuRole = RoleMenuResource::findFirst('role_name = "' . $rolename . '" and menu_resid =' . $mid) ?
                            RoleMenuResource::findFirst('role_name = "' . $rolename . '" and menu_resid =' . $mid) :
                            new RoleMenuResource();
                    $menuRole->role_name = $rolename;
                    $menuRole->menu_resid = $mid;
                    $menuRole->status = '1';

                    if (!$menuRole->save()) {
                        $error = '';
                        foreach ($menuRole->getMessages() as $message) {
                            $error .= $message;
                        }
                    }
                }
            }

            if (count($this->request->getPost('off')) > 0) {
                foreach ($this->request->getPost('off') as $key => $value) {
                    $usrrole = explode('_', $key);
                    $mid = $usrrole[1];
                    $rolename = $usrrole[2];
                    $menu = MenuResources::findFirstById($mid);
                    $menuRole = RoleMenuResource::findFirst('role_name = "' . $rolename . '" and menu_resid =' . $mid);

                    if ($menuRole && !$menuRole->delete()) {
                        $error = '';
                        foreach ($menuRole->getMessages() as $message) {
                            $error .= $message;
                        }
                    }
                }
            }




            if ($error) {
                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                exit;
            } else {

                echo '<div class="alert alert-success">Resources Saved Successfully</div>';
                exit;
            }
        } else {
            echo '<div class="alert alert-block alert-danger fade in">Select resource to assign</div>';
            exit;
        }
    }

}
