<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class ApprovalViewController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu");
        $this->view->setTemplateAfter('private');
    }

    public function loadSubtreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost() && $this->request->getPost('orgvalueid') != '') {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
            $this->view->org_value = $org_value = OrganizationalStructureValues::find('parent_id = ' . $orgvalueid
                            . ' GROUP BY  org_master_id ');
//             $this->view->org_mas_value = $org_mas_value = OrganizationalStructureMaster::find('parent_id = '.$orgvalueid);
        }
    }

    //student application approval
    public function loadApprovedApplnAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function applicationTblHdrAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->classandacademicdet = '';

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadApplicationDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();

        $aggregateval = '';
        foreach ($this->request->getPost() as $key => $value) {
            $IsSubdiv = explode('_', $key);
//            if ($IsSubdiv[0] == 'subDivVal') {
//                $params[$IsSubdiv[0]][] = $value;
//            } else {

            $params[$key] = $value;
            //}

            if ($IsSubdiv[0] == 'aggregate') {

                if ($value != '')
                    $aggregateval .= $value . ',';
            }
        }

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;


        //print_r($applications);
        // exit;


        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;

        if ($aggregateval != ''):
            $queryParams[] = 'app.aggregate_key  Like "%' . rtrim($aggregateval, ',') . '%"';
        endif;



        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $queryParams1[] = 'app.application_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'app.Student_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'appm.approval_status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;

        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['tdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }
        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) . ' and' : '';

        //echo $conditionvals;
        $psql = 'SELECT appm.id as appmid,app.id,app.application_no,app.application_no,
             app.Student_Name, app.aggregate_key,app.Gender,app.Date_of_Birth,
             app.Date_of_Birth,app.Date_of_Joining,app.Address1,app.Address2,app.Phone,
             app.Email,appm.Item_id,appm.approval_status,appm.approval_type_id,
             app.application_fee_status
          FROM Application app, ApprovalMaster appm, ApprovalTypes aptyp WHERE
           ' . $conditionvals . ''
                . ' appm.Item_id=app.id '
                . 'and aptyp.id=appm.approval_type_id '
                . 'and aptyp.approval_type = "Application"';
//echo $psql;exit;
        $totapplications_qry = $psql;

        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $psql .= ' ORDER BY ' . $this->getSortColumnNameForApplication($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }


        $psql .=' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        $applications = $this->modelsManager->executeQuery($psql);

//print_r(count($applications));
//        exit;

        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);



        $rowEntries = $this->formatApplicationTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => count($totapplications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForApplication($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "app.application_no";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "app.application_no";
            case 2:
                return "app.Student_Name";
            case 3:
                return "app.Gender";
            case 5:
                return "app.Date_of_Birth";
            case 6:
                return "app.Date_of_Joining";

            default:
                return "app.application_no";
        }
    }

    public function formatApplicationTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();


//                $app_sts = ($items->id) ? ApprovalMaster::findFirst('Item_id =' . $items->id) : '';
//               print_r('test'. $app_sts->approval_status);
//                exit;

                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->appmid ? $items->appmid : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';

                $row['application_id'] = $items->id ? $items->id : '';
                $row['address'] = $items->Address1 . ',' . $items->Address2;
                $row['mobile'] = $items->Phone ? $items->Phone : '';
                $row['Email'] = $items->Email ? $items->Email : '';
                $row['application_no'] = $items->application_no ? $items->application_no : '';
                $row['academicYr'] = ''; //AcademicYearMaster::findFirstById($items->academic_year_id)->academic_year_name;
                $row['Student_Name'] = $items->Student_Name ? $items->Student_Name : '';
                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';

                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';

                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }


                $row['Student_Name'] = ' <span title="' . $items->application_no . '" 
                                              class="mini-stat-icon tar "  >' . substr($items->Student_Name, 0, 1) . '</span>';

                $row['Student_Name'] .= '<span title="' . $items->Student_Name . ' (' . $items->application_no . ') ' . '"  style="vertical-align: middle;position: relative;top: 10px;cursor:default;word-break: break-all;text-transform: uppercase !important;"> 
                                      ' . (strlen($items->Student_Name) > 15 ? substr($items->Student_Name, 0, 15) . "..." : $items->Student_Name) . '</span>';

                //  $row['Student_Name'] .= '<span  title="' . $items->application_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                        ' . $items->Student_Name . '</span>';


                $row['class'] = ''; //DivisionValues::findFirstById($items->Division_Class)->classname;
                $row['dob'] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row['doj'] = ($items->Date_of_Joining) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row['labelclass'] = $labelclass;
                $row['approval_status'] = $items->approval_status;
                $row['dataclass'] = ''; // $dataclass;
                $row['appfeestatus'] = $items->application_fee_status ? $items->application_fee_status : 'Unpaid';
                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->appmid);

                $forwardto = $appitem->getLast()->forwaded_to;
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                // print_r('uid: '. $uid .'-frto: '.$forwardto);
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons" style="margin-bottom:0;">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;
                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /** relive student approval * */
    public function studentReliveApprvlAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadRelieveStuTblHdrAction() {


        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        // $this->view->divMName = ControllerBase::get_division_name_student();
        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');

        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadRelieveStuDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {

            $params[$key] = $value;
        }

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;

        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;


        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;

        if (isset($params['sSearch'])):
            $queryParams1[] = 'stu.Admission_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stu.Student_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'appm.approval_status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;


        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['tdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) . ' and' : '';

        //echo $conditionvals;
        $psql = 'SELECT appm.id as appmid,stureliv.id,stu.application_no,stu.Admission_no,
             stu.Student_Name,stu.Gender,stureliv.relieving_aggregate_key,
             stureliv.admission_date,stureliv.student_id,stureliv.scl_left_date,stureliv.created_date
             ,appm.Item_id,appm.approval_status,appm.approval_type_id,stu.photo
          FROM StudentRelievingMaster stureliv, ApprovalMaster appm, ApprovalTypes aptyp, StudentInfo stu WHERE
           ' . $conditionvals . ''
                . ' appm.Item_id=stureliv.id '
                . 'and stu.id=stureliv.student_id '
                . 'and aptyp.id=appm.approval_type_id '
                . 'and aptyp.approval_type = "Relieving(student)"';
//echo $psql;exit;
        $totapplications_qry = $psql;

        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $psql .= ' ORDER BY ' . $this->getSortColumnNameForRelieveStu($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }


        $psql .=' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        $applications = $this->modelsManager->executeQuery($psql);

//print_r(count($applications));
//        exit;

        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);



        $rowEntries = $this->formatRelieveStuTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => count($totapplications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForRelieveStu($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "stu.Admission_no";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "stu.Admission_no";
            case 1:
                return "stu.Student_Name";
            case 2:
                return "stu.Gender";
            case 6:
                return "stureliv.created_date";

            default:
                return "stu.Admission_no";
        }
    }

    public function formatRelieveStuTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();


//                $app_sts = ($items->id) ? ApprovalMaster::findFirst('Item_id =' . $items->id) : '';
//               print_r('test'. $app_sts->approval_status);
//                exit;student_id

                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->appmid ? $items->appmid : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';

                $row['admission_no'] = $items->Admission_no ? $items->Admission_no : '';
                $row['application_no'] = $items->application_no ? $items->application_no : '';
                //$row['academicYr'] = AcademicYearMaster::findFirstById($items->academic_year_id)->academic_year_name;
                $row['Student_Name'] = $items->Student_Name ? $items->Student_Name : '';
                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';
                // $row['class'] = DivisionValues::findFirstById($items->Division_Class)->classname;

                $aggregatevals = $items->relieving_aggregate_key ? explode(',', $items->relieving_aggregate_key) : '';

                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }

                if ($items->photo) {
                    $row['Student_Name'] = '<span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Student_Name'] .= $this->tag->image(array(
                        "src" => $items->photo ? (FILES_URI . $items->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Student_Name'] .= '</span>';
                } else {
                    $row['Student_Name'] = ' <span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;"  >' . substr($items->Student_Name, 0, 1) . '</span>';
                }

                $row['Student_Name'] .= '<span title="' . $items->Student_Name . ' (' . $items->Admission_no . ') ' . '"  style="vertical-align: middle;position: relative;top: 10px;cursor:default;word-break: break-all;text-transform: uppercase !important;"> 
                                      ' . (strlen($items->Student_Name) > 15 ? substr($items->Student_Name, 0, 15) . "..." : $items->Student_Name) . '</span>';
                //   $row['Student_Name'] .= '<span title="' . $items->Admission_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                         ' . $items->Student_Name . '</span>';



                $row['relievdate'] = ($items->created_date) ? date('d-m-Y', $items->created_date) : '';
                $row['dateofjoin'] = ($items->admission_date) ? date('d-m-Y', $items->admission_date) : '';
                $row['labelclass'] = $labelclass;

                $row['checklist'] = '';
                $row['approval_status'] = $items->approval_status;

                $row['Actions'] = '';

                $appitem = ApprovalItem::find('approval_master_id=' . $items->appmid);

                $forwardto = $appitem->getLast()->forwaded_to;
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                // print_r('uid: '. $uid .'-frto: '.$forwardto);
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $checkRelievecntn = UserRelieveStatus::findFirst("module_status=0 and user_type= 'student' and user_id = " . $items->student_id);
                    if (($checkRelievecntn)) {
                        $row['checklist'] = '<span class="btn btn-danger " onclick="relievingSettings.viewRelieveDetails(' . $items->student_id . ')">Check status</span>';
                        $row['Actions'] = '<div class="bs-glyphicons" style="margin-bottom:0px;">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li>  <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';
                    } else {
                        $row['Actions'] = '<div class="bs-glyphicons" style="margin-bottom:0px;">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';
                    }

                endif;

                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /** Staff Relive Approval * */
    public function loadStaffRelvApplnAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $nodes = array();
        //  $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');

        //   $nodes[$acdyrMas->id] = $acdyrMas->name;
        //  echo $acdyrMas->name;
        //    $fields = OrganizationalStructureMaster::find('parent_id=' . $acdyrMas->id);
        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                //if ($field->mandatory_for_admission == 1) {
//                    $nodes[$field->id] = $field->name;
                $nodes[$field->id] = $field->name;
                //}
            }
        endif;

        $this->view->nodes = $nodes; //$this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $nodes; //$this->_getMandNodesForAssigning($acdyrMas);
        //  exit;
    }

    public function applnStaffRelvTblHdrAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->classandacademicdet = '';

        $nodes = array();

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');
        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                $nodes[$field->id] = $field->name;
            }
        endif;

//        print_r($nodes);
//        exit;

        $this->view->nodes = $nodes;
        $this->view->mandnode = $nodes;
    }

    public function loadStaffRelvApplicationDataAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to = $uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;


        //print_r($applications);
        // exit;


        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;


        if (isset($params['sSearch']) && $params['sSearch'] != ''):

            $psql_deprt = OrganizationalStructureValues::findFirst('name LIKE"%' . $params['sSearch'] . '%"');

            if (isset($psql_deprt)) {
                $dep_id = isset($psql_deprt->id) ? $psql_deprt->id : '';
                if ($dep_id != '')
                    $queryParams1[] = 'stff.aggregate_key Like "%' . $dep_id . '%"';
            }

            $queryParams1[] = 'stff.appointment_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stff.Staff_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'appm.approval_status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;

        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['tdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }
        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) . ' and' : '';


        // echo $conditionvals;

        $psql = 'SELECT appm.id as appmid,stff.id,stff.appointment_no,
             stff.Staff_Name, stff.aggregate_key,stff.Gender,stff.Date_of_Birth,
             stff.Date_of_Joining,stff.Address1,stff.Address2,stff.Mobile_No,
             stff.Email,appm.Item_id,appm.approval_status,appm.approval_type_id
          FROM StaffInfo stff, ApprovalMaster appm, ApprovalTypes aptyp WHERE
           ' . $conditionvals . ''
                . ' appm.Item_id=stff.id '
                . 'and aptyp.id=appm.approval_type_id '
                . 'and aptyp.approval_type = "Relieving(staff)"';
//echo $psql;exit;
        $totapplications_qry = $psql;

        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $psql .= ' ORDER BY ' . $this->getSortColumnNameForStaffRelvApplication($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }


        $psql .=' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        $applications = $this->modelsManager->executeQuery($psql);

//print_r(count($applications));
//        exit;

        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);



        $rowEntries = $this->formatStaffRelvApplicationTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => count($totapplications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForStaffRelvApplication($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "stff.appointment_no";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "stff.appointment_no";
            case 2:
                return "stff.Staff_Name";
            case 3:
                return "stff.Gender";
            case 5:
                return "stff.Date_of_Birth";
            case 6:
                return "stff.Date_of_Joining";

            default:
                return "stff.appointment_no";
        }
    }

    public function formatStaffRelvApplicationTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();


//                $app_sts = ($items->id) ? ApprovalMaster::findFirst('Item_id =' . $items->id) : '';
//               print_r('test'. $app_sts->approval_status);
//                exit;

                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->appmid ? $items->appmid : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';

                $row['application_id'] = $items->id ? $items->id : '';
                $row['address'] = $items->Address1 . ',' . $items->Address2;
                $row['mobile'] = $items->Mobile_No ? $items->Mobile_No : '';
                $row['Email'] = $items->Email ? $items->Email : '';
                $row['appointment_no'] = $items->appointment_no ? $items->appointment_no : '';
                $row['academicYr'] = ''; //AcademicYearMaster::findFirstById($items->academic_year_id)->academic_year_name;
                $row['Staff_Name'] = $items->Staff_Name ? $items->Staff_Name : '';
                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';

                $staffgeninfo = StaffGeneralMaster::findFirstByStaffId($items->id);
                if ($staffgeninfo->photo) {
                    $row['Staff_Name'] = '<span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Staff_Name'] .= $this->tag->image(array(
                        "src" => $staffgeninfo->photo ? (FILES_URI . $staffgeninfo->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Staff_Name'] .= '</span>';
                } else {
                    $row['Staff_Name'] = ' <span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;" >' . substr($items->Staff_Name, 0, 1) . '</span>';
                }
                // $row['Staff_Name'] .= '<span title="' . $items->appointment_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                       ' . $items->Staff_Name . '</span>';
                $row['Staff_Name'] .= '<span title="' . $items->Staff_Name . ' (' . $items->appointment_no . ') ' . '"  style="vertical-align: middle;position: relative;top: 10px;cursor:default;word-break: break-all;text-transform: uppercase !important;"> 
                                      ' . (strlen($items->Staff_Name) > 15 ? substr($items->Staff_Name, 0, 15) . "..." : $items->Staff_Name) . '</span>';


                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';

                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }


                $row['reason'] = $items->id ? (StaffGeneralMaster::findFirst('staff_id =' . $items->id)->relieving_reason ? StaffGeneralMaster::findFirst('staff_id =' . $items->id)->relieving_reason : '-') : '-';

                $row['class'] = ''; //DivisionValues::findFirstById($items->Division_Class)->classname;
                $row['dob'] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row['doj'] = ($items->Date_of_Joining) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row['labelclass'] = $labelclass;
                $row['approval_status'] = $items->approval_status;
                $row['dataclass'] = ''; // $dataclass;
                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->appmid);

                $forwardto = $appitem->getLast()->forwaded_to;
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                // print_r('uid: '. $uid .'-frto: '.$forwardto);
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons" style="margin-bottom:0px;">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;
                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /** Staff Approval * */
    public function loadStaffApplnAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $nodes = array();


        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');

        //   $nodes[$acdyrMas->id] = $acdyrMas->name;
        //  echo $acdyrMas->name;
        //    $fields = OrganizationalStructureMaster::find('parent_id=' . $acdyrMas->id);
        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                //if ($field->mandatory_for_admission == 1) {
//                    $nodes[$field->id] = $field->name;
                $nodes[$field->id] = $field->name;
                //}
            }
        endif;

        $this->view->nodes = $nodes; //$this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $nodes; //$this->_getMandNodesForAssigning($acdyrMas);
        //  exit;
    }

    public function applnStaffTblHdrAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->classandacademicdet = '';

        $nodes = array();

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');
        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                $nodes[$field->id] = $field->name;
            }
        endif;

//        print_r($nodes);
//        exit;

        $this->view->nodes = $nodes;
        $this->view->mandnode = $nodes;
    }

    public function loadStaffApplicationDataAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);



        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {

            $params[$key] = $value;
        }



        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to = $uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;


        //print_r($applications);
        // exit;


        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;


        if (isset($params['sSearch']) && $params['sSearch'] != ''):

            $psql_deprt = OrganizationalStructureValues::findFirst('name LIKE"%' . $params['sSearch'] . '%"');

            if (isset($psql_deprt)) {
                $dep_id = isset($psql_deprt->id) ? $psql_deprt->id : '';
                if ($dep_id != '')
                    $queryParams1[] = 'stff.aggregate_key Like "%' . $dep_id . '%"';
            }

            $queryParams1[] = 'stff.appointment_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stff.Staff_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'appm.approval_status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;

        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['tdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }
        $queryParams[] = ' appm.Item_id=stff.id';
        $queryParams[] = 'aptyp.id=appm.approval_type_id';
        $queryParams[] = 'aptyp.approval_type = "Appointment"';
        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';


        // echo $conditionvals;

        $psql = 'SELECT appm.id as appmid,stff.id,stff.appointment_no,
             stff.Staff_Name, stff.aggregate_key,stff.Gender,stff.Date_of_Birth,
             stff.Date_of_Joining,stff.Address1,stff.Address2,stff.Mobile_No,
             stff.Email,appm.Item_id,appm.approval_status,appm.approval_type_id
          FROM StaffInfo stff, ApprovalMaster appm, ApprovalTypes aptyp WHERE
           ' . $conditionvals;


        $psql1 = 'SELECT count(*) as totcnt
          FROM StaffInfo stff, ApprovalMaster appm, ApprovalTypes aptyp WHERE
           ' . $conditionvals;
//echo $psql;exit;
        $totapplications_qry = $psql1;

        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $psql .= ' ORDER BY ' . $this->getSortColumnNameForStaffApplication($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }


        $psql .=' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        $applications = $this->modelsManager->executeQuery($psql);

//print_r(count($applications));
//        exit;

        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);



        $rowEntries = $this->formatStaffApplicationTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => $totapplications[0]->totcnt,
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForStaffApplication($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "stff.appointment_no";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "stff.appointment_no";
            case 2:
                return "stff.Staff_Name";
            case 3:
                return "stff.Gender";
            case 5:
                return "stff.Date_of_Birth";
            case 6:
                return "stff.Date_of_Joining";

            default:
                return "stff.appointment_no";
        }
    }

    public function formatStaffApplicationTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();


//                $app_sts = ($items->id) ? ApprovalMaster::findFirst('Item_id =' . $items->id) : '';
//               print_r('test'. $app_sts->approval_status);
//                exit;

                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->appmid ? $items->appmid : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';

                $row['application_id'] = $items->id ? $items->id : '';
                $row['address'] = $items->Address1 . ',' . $items->Address2;
                $row['mobile'] = $items->Mobile_No ? $items->Mobile_No : '';
                $row['Email'] = $items->Email ? $items->Email : '';
                $row['appointment_no'] = $items->appointment_no ? $items->appointment_no : '';
                $row['academicYr'] = ''; //AcademicYearMaster::findFirstById($items->academic_year_id)->academic_year_name;
                $row['Staff_Name'] = $items->Staff_Name ? $items->Staff_Name : '';
                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';


                $staffgeninfo = StaffGeneralMaster::findFirstByStaffId($items->id);
                if ($staffgeninfo->photo) {
                    $row['Staff_Name'] = '<span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Staff_Name'] .= $this->tag->image(array(
                        "src" => $staffgeninfo->photo ? (FILES_URI . $staffgeninfo->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Staff_Name'] .= '</span>';
                } else {
                    $row['Staff_Name'] = ' <span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;" >' . substr($items->Staff_Name, 0, 1) . '</span>';
                }
                $row['Staff_Name'] .= '<span title="' . $items->Staff_Name . ' (' . $items->appointment_no . ') ' . '" class="name"> 
                                        ' . (strlen($items->Staff_Name) > 15 ? substr($items->Staff_Name, 0, 15) . "..." : $items->Staff_Name) . '</span>';

                //  $row['Staff_Name'] .= '<span title="' . $items->appointment_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                        ' . $items->Staff_Name . '</span>';


                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';

                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }

                $row['class'] = ''; //DivisionValues::findFirstById($items->Division_Class)->classname;
                $row['dob'] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row['doj'] = ($items->Date_of_Joining) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row['labelclass'] = $labelclass;
                $row['approval_status'] = $items->approval_status;
                $row['dataclass'] = ''; // $dataclass;
                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->appmid);

                $forwardto = $appitem->getLast()->forwaded_to;
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                // print_r('uid: '. $uid .'-frto: '.$forwardto);
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons" style="margin-bottom:0px;">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';


                endif;
                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /** Student Approvals * */
    public function studentApprovalsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    /** Fee Cancel Approval * */
    public function feeCancelApprvlAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadfeeCancelTblHdrAction() {


        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        // $this->view->divMName = ControllerBase::get_division_name_student();
        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');

        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadfeeCancelDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {

            $params[$key] = $value;
        }

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;

        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;


        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;

        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $queryParams1[] = 'stufee.student_fee_id Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stuinfo.Student_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'appm.approval_status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;


        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['tdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';


        //echo $conditionvals;
        $psql = 'SELECT appm.id as appmid,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,stufee.student_fee_id,
                        stufee.cancelled_amount,stufee.status,stufeedet.student_id,
                        feemas.fee_node,stufee.id, stuinfo.Student_Name, stuinfo.photo,
                        stuinfo.Admission_no  FROM  ApprovalMaster appm 
 INNER JOIN StudentFeeCancel stufee on appm.Item_id=stufee.id
INNER JOIN    StudentFeeTable stufeedet on stufeedet.id = stufee.student_fee_id
INNER JOIN  FeeMaster feemas on feemas.id=stufeedet.fee_master_id
INNER JOIN FeeInstalmentType feeinstyp on feeinstyp.id = stufeedet.fee_instalment_typ_id and feeinstyp.fee_master_id = stufeedet.fee_master_id
INNER JOIN FeeInstalments feeins ON  feeins.id = stufeedet.fee_instalment_id and feeins.fee_instalment_type_id = stufeedet.fee_instalment_typ_id
INNER JOIN ApprovalTypes aptyp  on  aptyp.id=appm.approval_type_id  and aptyp.approval_type IN("Fee cancel")
INNER JOIN    StudentInfo stuinfo on stuinfo.id = stufeedet.student_id WHERE
           ' . $conditionvals . ' ORDER BY appm.approval_status DESC,appm.id DESC';
//echo $psql;exit;

        $totapplications_qry = $psql;

//        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
//            $psql .= ' ORDER BY ' . $this->getSortColumnNameForfeeCancel($this->request->getPost('iSortCol_' . $i)) . ' ' .
//                    $this->request->getPost('sSortDir_' . $i);
//        }


        $psql .=' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        //   echo 'test';
//          echo $psql;exit;
        $applications = $this->modelsManager->executeQuery($psql);
        // echo 'test';exit;
//print_r(count($applications));
//        exit;

        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);



        $rowEntries = $this->formatfeeCancelTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => count($totapplications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForfeeCancel($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "appmid";
        }
        switch ($sortColumnIndex) {

//            case 0:
//                return "stu.Admission_no";


            default:
                return "appmid";
        }
    }

    public function formatfeeCancelTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):

                $row = array();
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;


                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->appmid ? $items->appmid : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type) . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';

                $row['feecan_id'] = $items->student_fee_id ? $items->student_fee_id : '';

                $row['requested_by'] = $items->requested_by ? StaffInfo::findFirstById($items->requested_by)->loginid : '';
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '';
                $row['Approved_by'] = $items->Approved_by ? StaffInfo::findFirstById($items->Approved_by)->loginid : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '';
                $row['labelclass'] = $labelclass;
                // $row['class'] = DivisionValues::findFirstById($items->Division_Class)->classname;

                $row['stuid'] = $items->student_id ? $items->student_id : '';

                $studet = $items->student_id ? $items->student_id : '';

                /** aggregate val* */
                $stumappingdets = $studet ? ( StudentMapping::findFirst('student_info_id = ' . $studet)->aggregate_key ?
                                explode(',', StudentMapping::findFirst('student_info_id = ' . $studet)->aggregate_key) : '') : '';



                if ($stumappingdets != '') {
                    foreach ($stumappingdets as $stumappingdet) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stumappingdet);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }


                //  $fee_itmdet=  $items->fee_item_id ? FeeItems::findFirstById($items->fee_item_id) : '';

                $row['stuname'] = $items->Student_Name ? $items->Student_Name : '';
                $aggregatevals = $items->fee_node ? explode('-', $items->fee_node) : '';

                $class = array();
                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $class [] = $orgnztn_str_det->name;
                    }
                }
                $row['feename'] = implode('>>', $class);
                $row['feeamt'] = $items->cancelled_amount ? $items->cancelled_amount : '';


                if ($items->photo) {
                    $row['Student_Name'] = '<span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Student_Name'] .= $this->tag->image(array(
                        "src" => $items->photo ? (FILES_URI . $items->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Student_Name'] .= '</span>';
                } else {
                    $row['Student_Name'] = ' <span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;" >' . substr($items->Student_Name, 0, 1) . '</span>';
                }
                //$row['Student_Name'] .= '<span title="' . $items->Admission_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                      ' . $items->Student_Name . '</span>';

                $row['Student_Name'] .= '<span title="' . $items->Student_Name . ' (' . $items->Admission_no . ') ' . '"  style="vertical-align: middle;position: relative;top: 10px;cursor:default;word-break: break-all;text-transform: uppercase !important;"> 
                                      ' . (strlen($items->Student_Name) > 15 ? substr($items->Student_Name, 0, 15) . "..." : $items->Student_Name) . '</span>';


                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->appmid);

                $forwardto = $appitem->getLast()->forwaded_to;
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;

                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /** Staff Another Approvals * */
    public function staffApprovalsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->identity = $this->auth->getIdentity();
    }

    /** Leave Approval * */
    public function manageLeavesAppAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $nodes = array();
        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');

        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                $nodes[$field->id] = $field->name;
            }
        endif;

        $this->view->nodes = $nodes; //$this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $nodes; //$this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadLeavetableHeaderAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $nodes = array();
        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');

        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                $nodes[$field->id] = $field->name;
            }
        endif;

        $this->view->nodes = $nodes; //$this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $nodes; //$this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadLeavetableDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        //$this->view->leaves = LeaveRequest::find('staffId = ' . $identity['id'] .' ORDER BY status');

        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {

            $params[$key] = $value;
        }

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;

        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));


        $selectedMasterArr = array();

        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;

        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;

        if (isset($params['sSearch']) && $params['sSearch'] != ''):

            $psql_deprt = OrganizationalStructureValues::findFirst('name LIKE"%' . $params['sSearch'] . '%"');

            if (isset($psql_deprt)) {
                $dep_id = isset($psql_deprt->id) ? $psql_deprt->id : '';
                if ($dep_id != '')
                    $queryParams1[] = 'stfinf.aggregate_key Like "%' . $dep_id . '%"';
            }

            $queryParams1[] = 'stfinf.appointment_no Like "%' . $params['sSearch'] . '%"';

            $queryParams1[] = 'atendcslc.attendancename Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'appm.approval_status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;


        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['tdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) . ' and' : '';


        //echo $conditionvals;
        $psql = 'SELECT appm.id,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,leav.id as leavid,leav.leaveType,
                        leav.fromDate,leav.toDate,leav.duration,leav.reason,leav.staffId,atendcslc.attendancename,
                        stfinf.Staff_Name,stfinf.aggregate_key,stfinf.appointment_no
                        FROM  ApprovalMaster appm, LeaveRequest leav, AttendanceSelectbox atendcslc,
                         ApprovalTypes aptyp, StaffInfo stfinf WHERE
           ' . $conditionvals . ' '
                . ' aptyp.id=appm.approval_type_id '
                . 'and appm.Item_id=leav.id '
                . 'and atendcslc.id=leav.leaveType '
                . 'and stfinf.id=leav.staffId '
                . 'and aptyp.approval_type IN("Leave")';
//echo $psql;exit;

        $totapplications_qry = $psql;

//        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
//            $psql .= ' ORDER BY ' . $this->getSortColumnNameForLeavetable($this->request->getPost('iSortCol_' . $i)) . ' ' .
//                    $this->request->getPost('sSortDir_' . $i);
//        }


        $psql .=' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        //   echo 'test';
        //  echo $psql;exit;
        $applications = $this->modelsManager->executeQuery($psql);
        // echo 'test';exit;
//print_r(count($applications));
//        exit;

        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);



        $rowEntries = $this->formatLeaveTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($totapplications),
            "iTotalDisplayRecords" => count($applications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    /* public function getSortColumnNameForLeavetable($sortColumnIndex) {
      if (!isset($sortColumnIndex)) {
      return "appm.id";
      }
      switch ($sortColumnIndex) {

      //            case 0:
      //                return "stu.Admission_no";


      default:
      return "appm.id";
      }
      } */

    public function formatLeaveTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):


                $row = array();
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;


                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->id ? $items->id : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type) . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $row['requested_by'] = $items->requested_by ? StaffInfo::findFirstById($items->requested_by)->loginid : '';
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '';
                $row['Approved_by'] = $items->Approved_by ? StaffInfo::findFirstById($items->Approved_by)->loginid : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '';
                $row['labelclass'] = $labelclass;
                // $row['class'] = DivisionValues::findFirstById($items->Division_Class)->classname;

                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';

                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }

                $row['stfname'] = $items->Staff_Name ? $items->Staff_Name : '';
                $row['appointment_no'] = $items->appointment_no ? $items->appointment_no : '';


                $row['levtype'] = AttendanceSelectbox::findFirstById($items->leaveType)->attendancename;
                $row['frmdate'] = $items->fromDate ? date('d-m-Y', $items->fromDate) : '-';
                $row['todate'] = $items->toDate ? date('d-m-Y', $items->toDate) : '-';
                $row['duration'] = $items->duration;

                $staffgeninfo = StaffGeneralMaster::findFirstByStaffId($items->staffId);
                if ($staffgeninfo->photo) {
                    $row['Staff_Name'] = '<span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Staff_Name'] .= $this->tag->image(array(
                        "src" => $staffgeninfo->photo ? (FILES_URI . $staffgeninfo->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Staff_Name'] .= '</span>';
                } else {
                    $row['Staff_Name'] = ' <span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;"  >' . substr($items->Staff_Name, 0, 1) . '</span>';
                }
                $row['Staff_Name'] .= '<span title="' . $items->Staff_Name . ' (' . $items->appointment_no . ') ' . '" style="vertical-align: middle;position: relative;top: 10px;cursor:default;word-break: break-all;text-transform: uppercase !important;"> 
                                        ' . (strlen($items->Staff_Name) > 15 ? substr($items->Staff_Name, 0, 15) . "..." : $items->Staff_Name) . '</span>';

                // $row['Staff_Name'] .= '<span title="' . $items->appointment_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                       ' . $items->Staff_Name . '</span>';



                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->id);

                $forwardto = $appitem->getLast()->forwaded_to;
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;


                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /** Staff Advance Approval * */
    public function manageStaffAdvncAppAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $nodes = array();

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');
        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                $nodes[$field->id] = $field->name;
            }
        endif;

//        print_r($nodes);
//        exit;

        $this->view->nodes = $nodes;
        $this->view->mandnode = $nodes;
    }

    public function loadStaffAdvnctableHeaderAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $nodes = array();

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');
        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                $nodes[$field->id] = $field->name;
            }
        endif;

//        print_r($nodes);
//        exit;

        $this->view->nodes = $nodes;
        $this->view->mandnode = $nodes;
    }

    public function loadStaffAdvnctableDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        //$this->view->leaves = LeaveRequest::find('staffId = ' . $identity['id'] .' ORDER BY status');

        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {

            $params[$key] = $value;
        }

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;

        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));


        $selectedMasterArr = array();

        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;

        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;

        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $psql_deprt = OrganizationalStructureValues::findFirst('name LIKE"%' . $params['sSearch'] . '%"');


            if (isset($psql_deprt)) {
                $dep_id = isset($psql_deprt->id) ? $psql_deprt->id : '';
                if ($dep_id != '')
                    $queryParams1[] = 'stfinf.aggregate_key Like "%' . $dep_id . '%"';
            }
            $queryParams1[] = 'stfinf.appointment_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stfinf.Staff_Name Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'appm.approval_status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;


        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['tdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) . ' and' : '';



        //echo $conditionvals;
        $psql = 'SELECT appm.id,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,stfadv.id as stfadvnid,
                        stfadv.staffid,stfadv.amount ,stfadv.reason,stfinf.Staff_Name,
                        stfinf.aggregate_key,stfinf.Mobile_No,stfinf.appointment_no
                        FROM  ApprovalMaster appm, StaffAdvance stfadv, StaffInfo stfinf,
                        RecordTypes rcrdtyp, LedgerVoucher ldgrvch,
                         ApprovalTypes aptyp WHERE
           ' . $conditionvals . ' '
                . ' aptyp.id=appm.approval_type_id '
                . 'and appm.Item_id=ldgrvch.voucher_id '
                . 'and ldgrvch.record_type_item_id=stfadv.id '
                . 'and ldgrvch.record_type_id=rcrdtyp.id '
                . 'and stfadv.staffid=stfinf.id '
                . 'and aptyp.approval_type IN("Payment Voucher") '
                . 'and rcrdtyp.record_name IN("ADVANCE")';
//echo $psql;exit;

        $totapplications_qry = $psql;
        $orerby = ' ORDER BY appm.id desc';
        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $orerby = ' ORDER BY ' . $this->getSortColumnNameForStaffAdvnc($this->request->getPost('iSortCol_' . $i)) . ' ' . $orerby;
        }


        $psql .=$orerby . ' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        //   echo 'test';
//          echo $psql;exit;
        $applications = $this->modelsManager->executeQuery($psql);
        // echo 'test';exit;
//print_r(count($applications));
//        exit;

        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);



        $rowEntries = $this->formatStaffAdvncTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($totapplications),
            "iTotalDisplayRecords" => count($applications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForStaffAdvnc($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "appm.id";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "stfinf.Staff_Name";
            case 4:
                return "appm.approval_status";
            default:
                return "appm.id";
        }
    }

    public function formatStaffAdvncTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):


                $row = array();
                $identity = $this->auth->getIdentity();
                //$uid = $identity['id'];
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;

                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->id ? $items->id : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type) . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $row['requested_by'] = $items->requested_by ? StaffInfo::findFirstById($items->requested_by)->loginid : '';
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '-';
                $row['Approved_by'] = $items->Approved_by ? StaffInfo::findFirstById($items->Approved_by)->loginid : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '-';
                $row['labelclass'] = $labelclass;
                // $row['class'] = DivisionValues::findFirstById($items->Division_Class)->classname;


                $row['stfname'] = $items->Staff_Name ? $items->Staff_Name : '';
                $row['appointment_no'] = $items->appointment_no ? $items->appointment_no : '-';


                $staffgeninfo = StaffGeneralMaster::findFirstByStaffId($items->staffid);
                if ($staffgeninfo->photo) {
                    $row['Staff_Name'] = '<span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Staff_Name'] .= $this->tag->image(array(
                        "src" => $staffgeninfo->photo ? (FILES_URI . $staffgeninfo->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Staff_Name'] .= '</span>';
                } else {
                    $row['Staff_Name'] = ' <span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;"  >' . substr($items->Staff_Name, 0, 1) . '</span>';
                }
                $row['Staff_Name'] .= '<span title="' . $items->Staff_Name . ' (' . $items->appointment_no . ') ' . '" style="vertical-align: middle;position: relative;top: 10px;cursor:default;word-break: break-all;text-transform: uppercase !important;"> 
                                        ' . (strlen($items->Staff_Name) > 15 ? substr($items->Staff_Name, 0, 15) . "..." : $items->Staff_Name) . '</span>';
                //$row['Staff_Name'] .= '<span title="' . $items->appointment_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                      ' . $items->Staff_Name . '</span>';


                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';

//                print_r($aggregatevals);
//                exit;

                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }


                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->id);

                $forwardto = $appitem->getLast()->forwaded_to;
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;


                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /* Staff Salary Approval */

    public function staffSalaryManagAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->assets->addCss('css/edustyle.css');
    }

    public function staffSalaryHeaderAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function loadstaffSalaryDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = array();
        $params['fdate'] = $this->request->getPost('fdate') ? strtotime($this->request->getPost('fdate') . ' 00:00:00') : '';
        $params['tdate'] = $this->request->getPost('tdate') ? strtotime($this->request->getPost('tdate') . ' 23:59:59') : '';
        $params['appType'] = $this->request->getPost('appType') ? ($this->request->getPost('appType')) : '';
        $params['appStatus'] = $this->request->getPost('appStatus') ? ($this->request->getPost('appStatus')) : '';

        $orderphql = array();
        $queryParams = array();

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));

        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;
        $queryParams = '';
        $applications = '';
        $totapplications = '';
        if (count($selectedMasterArr) > 0) {
            $queryParams[] = ' appm.id IN (' . implode(',', $selectedMasterArr) . ')';
        }
        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = ' appm.requested_date >= "' . $params['fdate'] . '"';
            $queryParams[] = ' appm.requested_date <= "' . $params['tdate'] . '"';
        }


        if ($params['appStatus'] != ''):
            $queryParams[] = ' appm.approval_status  LIKE"%' . $params['appStatus'] . '%"';
            $queryParams[] = ' ldgrvs.voucher_id = "' . $params['appStatus'] . '"';

            $ledgerdet = OrganizationalStructureValues::findFirst('name LIKE"%' . $params['appStatus'] . '%"');

            if (isset($ledgerdet)) {
                $ledger_id = isset($ledgerdet->id) ? $ledgerdet->id : '';
                if ($ledger_id != '')
                    $queryParams[] = 'ldgrvs.creditledger Like "%' . $ledger_id . '%"';
                $queryParams[] = 'ldgrvs.debitledger Like "%' . $ledger_id . '%"';
            }


        endif;
        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) . ' and' : '';
        $phql = 'SELECT appm.id,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,ldgrvs.voucher_id,
                        ldgrvs.amount,ldgrvs.creditledger,ldgrvs.debitledger
                        FROM ApprovalMaster appm, LedgerVoucher ldgrvs, ApprovalTypes aptyp, 
                        RecordTypes rcrdtyp WHERE
           ' . $conditionvals . ' '
                . ' aptyp.id=appm.approval_type_id '
                . 'and appm.Item_id=ldgrvs.voucher_id '
                . 'and ldgrvs.record_type_id=rcrdtyp.id '
                . 'and aptyp.approval_type IN("Payment Voucher") '
                . 'and rcrdtyp.record_name IN("SALARY VOUCHER")';

//  print_r($phql);
//	exit;
        $totfinances_qry = $phql;

        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $phql .= ' ORDER BY ' . $this->getSortColumnNameForstaffSalaryApproval($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }
        $phql .= ' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');

//       print_r($phql);
//	exit;
        $finances = $this->modelsManager->executeQuery($phql);


        $totfinances = $this->modelsManager->executeQuery($totfinances_qry);
//          print_r(count($totfinances));
//	 exit;

        $rowEntries = $this->formatstaffSalaryApprovalTableData($finances);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($rowEntries),
            "iTotalDisplayRecords" => count($totfinances),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForstaffSalaryApproval($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "appm.id";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "ldgrvs.voucher_id";
            case 2:
                return "ldgrvs.creditledger";
            case 3:
                return "ldgrvs.debitledger";
            case 4:
                return "ldgrvs.amount";
            case 5:
                return "appm.requested_by";
            case 6:
                return "appm.requested_date";
            case 7:
                return "appm.approval_status";

            default:
                return "appm.id";
        }
    }

    public function formatstaffSalaryApprovalTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;


                $labelclass = ($items->approval_status == 'Approved') ? 'success' :
                        (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->id ? $items->id : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type)
                        . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $row['requested_by'] = $items->requested_by ? StaffInfo::findFirstById($items->requested_by)->loginid : '';
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '';
                $row['Approved_by'] = $items->Approved_by ? StaffInfo::findFirstById($items->Approved_by)->loginid : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '';
                $row['labelclass'] = $labelclass;

                $row['voucherid'] = $items->voucher_id ? $items->voucher_id : '';
                $row['voucher_crdt'] = $items->creditledger ? OrganizationalStructureValues::findFirstById($items->creditledger)->name : '';
                $row['voucher_dbt'] = $items->debitledger ? OrganizationalStructureValues::findFirstById($items->debitledger)->name : '';
                $row['voucher_amt'] = $items->amount ? $items->amount : '';

                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->id);

                $forwardto = $appitem->getLast()->forwaded_to;
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;

                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /** Transport Approval * */
    public function transportApprvlAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadTransportTblHdrAction() {


        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        // $this->view->divMName = ControllerBase::get_division_name_student();
        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');

        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadTransportDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {

            $params[$key] = $value;
        }

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;

        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;


        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;

        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $queryParams1[] = 'stuinfo.Student_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'appm.approval_status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;


        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['tdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';


        //echo $conditionvals;
        $psql = 'SELECT appm.id as appmid,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,stutrnsprt.id,stutrnsprt.subordinate_key,
                        stutrnsprt.student_info_id,stuinfo.Student_Name, stuinfo.photo,
                        stuinfo.Admission_no  FROM  ApprovalMaster appm 
INNER JOIN StudentTransport stutrnsprt on appm.Item_id=stutrnsprt.id
INNER JOIN ApprovalTypes aptyp  on  aptyp.id=appm.approval_type_id  and aptyp.approval_type IN("Transport")
INNER JOIN    StudentInfo stuinfo on stuinfo.id = stutrnsprt.student_info_id WHERE
           ' . $conditionvals;
//echo $psql;exit;

        $totapplications_qry = $psql;

//        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
//            $psql .= ' ORDER BY ' . $this->getSortColumnNameForTransport($this->request->getPost('iSortCol_' . $i)) . ' ' .
//                    $this->request->getPost('sSortDir_' . $i);
//        }


        $psql .=' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        //   echo 'test';
        //  echo $psql;exit;
        $applications = $this->modelsManager->executeQuery($psql);
        // echo 'test';exit;
//print_r(count($applications));
//        exit;

        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);



        $rowEntries = $this->formatTransportTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => count($totapplications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForTransport($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "appmid";
        }
        switch ($sortColumnIndex) {

//            case 0:
//                return "stu.Admission_no";


            default:
                return "appmid";
        }
    }

    public function formatTransportTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):

                $row = array();
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;


                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->appmid ? $items->appmid : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type) . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';


                $row['requested_by'] = $items->requested_by ? StaffInfo::findFirstById($items->requested_by)->loginid : '';
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '';
                $row['Approved_by'] = $items->Approved_by ? StaffInfo::findFirstById($items->Approved_by)->loginid : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '';
                $row['labelclass'] = $labelclass;
                // $row['class'] = DivisionValues::findFirstById($items->Division_Class)->classname;

                $studet = $items->student_info_id ? $items->student_info_id : '';

                /** aggregate val* */
                $stumappingdets = $studet ? ( StudentMapping::findFirst('student_info_id = ' . $studet)->aggregate_key ?
                                explode(',', StudentMapping::findFirst('student_info_id = ' . $studet)->aggregate_key) : '') : '';



                if ($stumappingdets != '') {
                    foreach ($stumappingdets as $stumappingdet) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stumappingdet);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }


                //  $fee_itmdet=  $items->fee_item_id ? FeeItems::findFirstById($items->fee_item_id) : '';

                $row['stuname'] = $items->Student_Name ? $items->Student_Name : '';


                if ($items->photo) {
                    $row['Student_Name'] = '<span title="' . $items->Student_Name . '" 
                                              class="mini-stat-icon"  >';
                    $row['Student_Name'] .= $this->tag->image(array(
                        "src" => $items->photo ? (FILES_URI . $items->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Student_Name'] .= '</span>';
                } else {
                    $row['Student_Name'] = ' <span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon tar "  >' . substr($items->Student_Name, 0, 1) . '</span>';
                }
                $row['Student_Name'] .= '<span title="' . $items->Admission_no . '"  style="vertical-align: middle;position: relative;top: 10px;cursor:default;word-break: break-all;text-transform: uppercase !important;"> 
                                        ' . $items->Student_Name . '</span>';


                $finalArr = explode(',', $items->subordinate_key);
                $labelAr = array();
                foreach ($finalArr as $agindvvalue):
                    $agindvValRec = OrganizationalStructureValues::findFirstById($agindvvalue);
                    $labelAr[] = $agindvValRec->name;
                endforeach;
                $label = implode(' >> ', $labelAr);

                $row['transportdet'] = $label;

                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->appmid);

                $forwardto = $appitem->getLast()->forwaded_to;
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;

                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /** Transport Delete Approval * */
    public function transportDeleteApprvlAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadTransportDeleteTblHdrAction() {


        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        // $this->view->divMName = ControllerBase::get_division_name_student();
        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');

        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadTransportDeleteDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {

            $params[$key] = $value;
        }

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
//echo $uid;
//exit;
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to = $uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));


        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;


        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;

        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $queryParams1[] = 'stuinfo.Student_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'appm.approval_status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;


        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['tdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';


        //echo $conditionvals;
        $psql = 'SELECT appm.id as appmid,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,stutrnsprt.id,stutrnsprt.new_nodeid,
                        stutrnsprt.student_info_id,stuinfo.Student_Name, stuinfo.photo,
                        stuinfo.Admission_no  FROM  ApprovalMaster appm 
INNER JOIN StudentTransportCancel stutrnsprt on appm.Item_id=stutrnsprt.id
INNER JOIN ApprovalTypes aptyp  on  aptyp.id=appm.approval_type_id  and aptyp.approval_type IN("Transport Cancel")
INNER JOIN    StudentInfo stuinfo on stuinfo.id = stutrnsprt.student_info_id WHERE
           ' . $conditionvals;
//echo $psql;exit;

        $totapplications_qry = $psql;

//        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
//            $psql .= ' ORDER BY ' . $this->getSortColumnNameForTransportDelete($this->request->getPost('iSortCol_' . $i)) . ' ' .
//                    $this->request->getPost('sSortDir_' . $i);
//        }


        $psql .=' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        //   echo 'test';
        //  echo $psql;exit;
        $applications = $this->modelsManager->executeQuery($psql);
        // echo 'test';exit;
//print_r(count($applications));
//        exit;

        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);



        $rowEntries = $this->formatTransportDeleteTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => count($totapplications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForTransportDelete($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "appmid";
        }
        switch ($sortColumnIndex) {

//            case 0:
//                return "stu.Admission_no";


            default:
                return "appmid";
        }
    }

    public function formatTransportDeleteTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):

                $row = array();
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;


                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->appmid ? $items->appmid : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type) . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';


                $row['requested_by'] = $items->requested_by ? StaffInfo::findFirstById($items->requested_by)->loginid : '';
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '';
                $row['Approved_by'] = $items->Approved_by ? StaffInfo::findFirstById($items->Approved_by)->loginid : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '';
                $row['labelclass'] = $labelclass;
                // $row['class'] = DivisionValues::findFirstById($items->Division_Class)->classname;

                $studet = $items->student_info_id ? $items->student_info_id : '';

                /** aggregate val* */
                $stumappingdets = $studet ? ( StudentMapping::findFirst('student_info_id = ' . $studet)->aggregate_key ?
                                explode(',', StudentMapping::findFirst('student_info_id = ' . $studet)->aggregate_key) : '') : '';



                if ($stumappingdets != '') {
                    foreach ($stumappingdets as $stumappingdet) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stumappingdet);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }


                //  $fee_itmdet=  $items->fee_item_id ? FeeItems::findFirstById($items->fee_item_id) : '';

                $row['stuname'] = $items->Student_Name ? $items->Student_Name : '';


                if ($items->photo) {
                    $row['Student_Name'] = '<span title="' . $items->Student_Name . '" 
                                              class="mini-stat-icon"  >';
                    $row['Student_Name'] .= $this->tag->image(array(
                        "src" => $items->photo ? (FILES_URI . $items->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Student_Name'] .= '</span>';
                } else {
                    $row['Student_Name'] = ' <span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon tar "  >' . substr($items->Student_Name, 0, 1) . '</span>';
                }
                $row['Student_Name'] .= '<span title="' . $items->Admission_no . '"  style="vertical-align: middle;position: relative;top: 10px;cursor:default;word-break: break-all;text-transform: uppercase !important;"> 
                                        ' . $items->Student_Name . '</span>';


                $finalArr = array_unique(preg_split("/[:-]+/", $items->new_nodeid)); //explode(',', $items->new_nodeid);
                $labelAr = array();

                foreach ($finalArr as $agindvvalue):
                    $agindvValRec = OrganizationalStructureValues::findFirstById($agindvvalue);
                    $labelAr[] = $agindvValRec->name;
                endforeach;
                $label = implode(' >> ', $labelAr);

                $row['transportdet'] = $label;

                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->appmid);

                $forwardto = $appitem->getLast()->forwaded_to;
//                echo 'test'. $forwardto;
//                exit;
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;

                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /** Fee Concession Approval * */
    public function feeConcessionApprvlAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadfeeConcessionTblHdrAction() {


        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        // $this->view->divMName = ControllerBase::get_division_name_student();
        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');

        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadfeeConcessionDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {

            $params[$key] = $value;
        }

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;

        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;


        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;

        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $queryParams1[] = 'stuconcefee.stu_fee_id Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stuinfo.Student_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'appm.approval_status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;


        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['tdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';


        //echo $conditionvals;
        $psql = 'SELECT appm.id as appmid,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,stuconcefee.stu_fee_id,
                        stuconcefee.concession_amt,stuconcefee.concession_status,stufeedet.student_id,
                        feeins.fee_amount,feemas.fee_node,stuconcefee.id, stuinfo.Student_Name, stuinfo.photo,
                        stuinfo.Admission_no  FROM  ApprovalMaster appm 
 INNER JOIN StudentFeeConcession stuconcefee on appm.Item_id=stuconcefee.id
INNER JOIN    StudentFeeTable stufeedet on stufeedet.id = stuconcefee.stu_fee_id
INNER JOIN  FeeMaster feemas on feemas.id=stufeedet.fee_master_id
INNER JOIN FeeInstalmentType feeinstyp on feeinstyp.id = stufeedet.fee_instalment_typ_id and feeinstyp.fee_master_id = stufeedet.fee_master_id
INNER JOIN FeeInstalments feeins ON  feeins.id = stufeedet.fee_instalment_id and feeins.fee_instalment_type_id = stufeedet.fee_instalment_typ_id
INNER JOIN ApprovalTypes aptyp  on  aptyp.id=appm.approval_type_id  and aptyp.approval_type IN("Concession")
INNER JOIN    StudentInfo stuinfo on stuinfo.id = stufeedet.student_id WHERE
           ' . $conditionvals . ' ORDER BY appm.approval_status DESC,appm.id DESC';
//echo $psql;exit;

        $totapplications_qry = $psql;

//        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
//            $psql .= ' ORDER BY ' . $this->getSortColumnNameForfeeConcession($this->request->getPost('iSortCol_' . $i)) . ' ' .
//                    $this->request->getPost('sSortDir_' . $i);
//        }


        $psql .=' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        //   echo 'test';
        //  echo $psql;exit;
        $applications = $this->modelsManager->executeQuery($psql);
        // echo 'test';exit;
//print_r(count($applications));
//        exit;

        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);



        $rowEntries = $this->formatfeeConcessionTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => count($totapplications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForfeeConcession($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "appmid";
        }
        switch ($sortColumnIndex) {

//            case 0:
//                return "stu.Admission_no";


            default:
                return "appmid";
        }
    }

    public function formatfeeConcessionTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):

                $row = array();
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;


                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->appmid ? $items->appmid : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type) . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';

                $row['feeconcesn_id'] = $items->id ? $items->id : '';
                $row['fee_id'] = $items->stu_fee_id ? $items->stu_fee_id : '';

                $row['requested_by'] = $items->requested_by ? StaffInfo::findFirstById($items->requested_by)->loginid : '';
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '';
                $row['Approved_by'] = $items->Approved_by ? StaffInfo::findFirstById($items->Approved_by)->loginid : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '';
                $row['labelclass'] = $labelclass;
                // $row['class'] = DivisionValues::findFirstById($items->Division_Class)->classname;

                $row['stuid'] = $items->student_id ? $items->student_id : '';

                $studet = $items->student_id ? $items->student_id : '';

                /** aggregate val* */
                $stumappingdets = $studet ? ( StudentMapping::findFirst('student_info_id = ' . $studet)->aggregate_key ?
                                explode(',', StudentMapping::findFirst('student_info_id = ' . $studet)->aggregate_key) : '') : '';



                if ($stumappingdets != '') {
                    foreach ($stumappingdets as $stumappingdet) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stumappingdet);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }


                //  $fee_itmdet=  $items->fee_item_id ? FeeItems::findFirstById($items->fee_item_id) : '';

                $row['stuname'] = $items->Student_Name ? $items->Student_Name : '';
                $aggregatevals = $items->fee_node ? explode('-', $items->fee_node) : '';

                $class = array();
                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $class [] = $orgnztn_str_det->name;
                    }
                }
                $row['feename'] = implode('>>', $class);
                $row['feeamt'] = $items->fee_amount ? $items->fee_amount : '';
                $row['feeconcesnamt'] = $items->concession_amt ? $items->concession_amt : '';


                if ($items->photo) {
                    $row['Student_Name'] = '<span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Student_Name'] .= $this->tag->image(array(
                        "src" => $items->photo ? (FILES_URI . $items->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Student_Name'] .= '</span>';
                } else {
                    $row['Student_Name'] = ' <span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;" >' . substr($items->Student_Name, 0, 1) . '</span>';
                }
                //$row['Student_Name'] .= '<span title="' . $items->Admission_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                      ' . $items->Student_Name . '</span>';

                $row['Student_Name'] .= '<span title="' . $items->Student_Name . ' (' . $items->Admission_no . ') ' . '"  style="vertical-align: middle;position: relative;top: 10px;cursor:default;word-break: break-all;text-transform: uppercase !important;"> 
                                      ' . (strlen($items->Student_Name) > 15 ? substr($items->Student_Name, 0, 15) . "..." : $items->Student_Name) . '</span>';


                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->appmid);

                $forwardto = $appitem->getLast()->forwaded_to;
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;

                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /** Fee Request Approval * */
    public function feeRequestApprvlAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadfeeRequestTblHdrAction() {


        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        // $this->view->divMName = ControllerBase::get_division_name_student();
        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');

        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadfeeRequestDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {

            $params[$key] = $value;
        }

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $condition = ($identity['name'] == 'Superadmin' ) ? '' : "forwaded_to =$uid ";
        $appItem = ApprovalItem::find(array(
                    'conditions' => $condition,
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;


        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;

        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $queryParams1[] = 'stuinfo.Student_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'appm.approval_status Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;


        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['tdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';


        //echo $conditionvals;
        $psql = 'SELECT appm.id as appmid,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,stufeereq.student_id,
                        feeins.fee_amount,feemas.fee_node,stufeereq.id, stuinfo.Student_Name, stuinfo.photo,
                        stuinfo.Admission_no  FROM  ApprovalMaster appm 
 INNER JOIN StudentFeeRequestTable stufeereq on appm.Item_id=stufeereq.id
INNER JOIN  FeeMaster feemas on feemas.id=stufeereq.fee_master_id
INNER JOIN FeeInstalmentType feeinstyp on feeinstyp.id = stufeereq.fee_instalment_typ_id and feeinstyp.fee_master_id = stufeereq.fee_master_id
INNER JOIN FeeInstalments feeins ON  feeins.id = stufeereq.fee_instalment_id and feeins.fee_instalment_type_id = stufeereq.fee_instalment_typ_id
INNER JOIN ApprovalTypes aptyp  on  aptyp.id=appm.approval_type_id  and aptyp.approval_type IN("Fee Assign")
INNER JOIN    StudentInfo stuinfo on stuinfo.id = stufeereq.student_id WHERE
           ' . $conditionvals . ' ORDER BY appm.approval_status DESC,appm.id DESC';
//echo $psql;exit;

        $totapplications_qry = $psql;

//        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
//            $psql .= ' ORDER BY ' . $this->getSortColumnNameForfeeRequest($this->request->getPost('iSortCol_' . $i)) . ' ' .
//                    $this->request->getPost('sSortDir_' . $i);
//        }


        $psql .=' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        //   echo 'test';
//          echo $psql;exit;
        $applications = $this->modelsManager->executeQuery($psql);
        // echo 'test';exit;
//print_r(count($applications));
//        exit;

        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);



        $rowEntries = $this->formatfeeRequestTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => count($totapplications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForfeeRequest($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "appmid";
        }
        switch ($sortColumnIndex) {

//            case 0:
//                return "stu.Admission_no";


            default:
                return "appmid";
        }
    }

    public function formatfeeRequestTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):

                $row = array();
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;


                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->appmid ? $items->appmid : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type) . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';

                $row['feeconcesn_id'] = $items->id ? $items->id : '';
                $row['fee_id'] = $items->stu_fee_id ? $items->stu_fee_id : '';

                $row['requested_by'] = $items->requested_by ? StaffInfo::findFirstById($items->requested_by)->loginid : '';
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '';
                $row['Approved_by'] = $items->Approved_by ? StaffInfo::findFirstById($items->Approved_by)->loginid : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '';
                $row['labelclass'] = $labelclass;
                // $row['class'] = DivisionValues::findFirstById($items->Division_Class)->classname;

                $row['stuid'] = $items->student_id ? $items->student_id : '';

                $studet = $items->student_id ? $items->student_id : '';

                /** aggregate val* */
                $stumappingdets = $studet ? ( StudentMapping::findFirst('student_info_id = ' . $studet)->aggregate_key ?
                                explode(',', StudentMapping::findFirst('student_info_id = ' . $studet)->aggregate_key) : '') : '';



                if ($stumappingdets != '') {
                    foreach ($stumappingdets as $stumappingdet) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stumappingdet);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }


                //  $fee_itmdet=  $items->fee_item_id ? FeeItems::findFirstById($items->fee_item_id) : '';

                $row['stuname'] = $items->Student_Name ? $items->Student_Name : '';
                $aggregatevals = $items->fee_node ? explode('-', $items->fee_node) : '';

                $class = array();
                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $class [] = $orgnztn_str_det->name;
                    }
                }
                $row['feename'] = implode('>>', $class);
                $row['feeamt'] = $items->fee_amount ? $items->fee_amount : '';

                if ($items->photo) {
                    $row['Student_Name'] = '<span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Student_Name'] .= $this->tag->image(array(
                        "src" => $items->photo ? (FILES_URI . $items->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Student_Name'] .= '</span>';
                } else {
                    $row['Student_Name'] = ' <span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;" >' . substr($items->Student_Name, 0, 1) . '</span>';
                }
                //$row['Student_Name'] .= '<span title="' . $items->Admission_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                      ' . $items->Student_Name . '</span>';

                $row['Student_Name'] .= '<span title="' . $items->Student_Name . ' (' . $items->Admission_no . ') ' . '"  style="vertical-align: middle;position: relative;top: 10px;cursor:default;word-break: break-all;text-transform: uppercase !important;"> 
                                      ' . (strlen($items->Student_Name) > 15 ? substr($items->Student_Name, 0, 15) . "..." : $items->Student_Name) . '</span>';


                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->appmid);

                $forwardto = $appitem->getLast()->forwaded_to;
                if (($identity['name'] == 'Superadmin' || $uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;

                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    //functions

    public function _getNonMandNodesForAssigning($id, $nodes = array()) {
        $fields = OrganizationalStructureMaster::find('parent_id=' . $id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
                if ($field->is_subordinate != 1) {
                    $nodes[$field->id] = $field->name;
                    // } else {
                    $nodes = $this->_getNonMandNodesForAssigning($field->id, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function _getMandNodesForAssigning($acdyrMas, $nodes = array()) {

        $nodes[$acdyrMas->id] = $acdyrMas->name;
        //  echo $acdyrMas->name;
        $fields = OrganizationalStructureMaster::find('parent_id=' . $acdyrMas->id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
                if ($field->is_subordinate != 1) {
//                    $nodes[$field->id] = $field->name;
                    $nodes = $this->_getMandNodesForAssigning($field, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function viewApprovalDetailsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $masterid = $this->request->getPost('approve_masid');
        $identity = $this->auth->getIdentity();
        $userid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $this->view->appHistory = ApprovalItem::find(
                        array(
                            "approval_master_id = " . $masterid,
                            "order" => "created_date DESC"));
        $this->view->appmaster = $appmaster = ApprovalMaster::findFirstById($masterid);
        $this->view->appType = ApprovalTypes::findFirstById($appmaster->approval_type_id);
        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->stfacdyrMas = $stfacdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment = 1');
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
        $nodes = array();
        if (count($stfacdyrMas) > 0):
            foreach ($stfacdyrMas as $field) {
                $nodes[$field->id] = $field->name;
            }
        endif;
        $this->view->staffnode = $nodes;
    }

}
