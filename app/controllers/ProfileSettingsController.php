<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class ProfileSettingsController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->assets->addJs('js/profilesettings/toggle-init.js');
//        $this->assets->addJs('js/profilesettings/student.js');
//        $this->assets->addJs('js/bootstrap-switch.js');
//        $this->assets->addCss('css/bootstrap-switch.css');
        $this->view->fieldlist = StudentProfileSettings::find('parent_id = 0');
        $this->view->admit_student_det = $update_admit_stu_val = Settings::findFirst('variableName ="admit_student"');
    }

    public function staffAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
      //  $this->assets->addJs('js/profilesettings/toggle-init.js');
     //   $this->assets->addJs('js/profilesettings/student.js');
     //   $this->assets->addJs('js/bootstrap-switch.js');
    //    $this->assets->addCss('css/bootstrap-switch.css');
        $this->view->fieldlist = StaffProfileSettings::find('parent_id = 0');
    }
    
    public function updateSettingsValueAction(){
        try {
            if ($this->request->isPost()) {
               
                $admit_student = $this->request->getPost('admit_student'); 
                
                $update_admit_stu_val = Settings::findFirst('variableName ="admit_student"');
                
                $update_admit_stu_val->variableValue = $admit_student;
               
                 
              if ($update_admit_stu_val->save()) {
                    $error = '';
                    $message['type'] = 'success';
                    $message['message'] = ' Admit Student Updated Successfuly!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($update_admit_stu_val->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            } else {
                $error = '';
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-error">Update failed! Invalid Input<br/>';
                print_r(json_encode($message));
                exit;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function updateMandatoryAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();

        try {
            if ($this->request->isPost()) {
                $fieldId = $this->request->getPost('itemID');
                $value = $this->request->getPost('value');
                $userType = $this->request->getPost('userType');
                $admit_student = $this->request->getPost('admit_student'); 
                
                $update_admit_stu_val = Settings::findFirst('variableName ="admit_student"');
                
                $update_admit_stu_val->variableValue = $admit_student;
                
               if($update_admit_stu_val->variableValue != $admit_student )
               {
                   $update_admit_stu_val->save();
               }
                
                if ($userType == 'Student')
                    $fieldItem = StudentProfileSettings::findFirstById($fieldId);
                else
                    $fieldItem = StaffProfileSettings::findFirstById($fieldId);

                $fieldItem->mandatory = $value;
                if ($fieldItem->save()) {
                    $error = '';
                    $message['type'] = 'success';
                    $message['message'] = $fieldItem->field_name . ' Updated Successfuly!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($fieldItem->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            } else {
                $error = '';
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-error">Update failed! Invalid Input<br/>';
                print_r(json_encode($message));
                exit;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function updateHideorShowAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();

        try {
            if ($this->request->isPost()) {
                $fieldId = $this->request->getPost('itemID');
                $value = $this->request->getPost('value');
                $userType = $this->request->getPost('userType');
                if ($userType == 'Student')
                    $fieldItem = StudentProfileSettings::findFirstById($fieldId);
                else
                    $fieldItem = StaffProfileSettings::findFirstById($fieldId);

                if ($fieldItem->parent_id == 0) {
                    if ($userType == 'Student')
                        $subfieldlist = StudentProfileSettings::find('parent_id = ' . $fieldId);
                    else
                        $subfieldlist = StaffProfileSettings::find('parent_id = ' . $fieldId);

                    if ($subfieldlist && count($subfieldlist) > 0):
                        foreach ($subfieldlist as $subfield) :
                            $subfield->hide_or_show = $value;
                            if (!$subfield->save()):
                                $error = '';
                                foreach ($subfield->getMessages() as $message) {
                                    $error .= $message;
                                }
                                $message['type'] = 'error';
                                $message['message'] = $error;
                                print_r(json_encode($message));
                                exit;
                            endif;
                        endforeach;
                    endif;
                }
                $fieldItem->hide_or_show = $value;
                if ($fieldItem->save()) {
                    $error = '';
                    $message['type'] = 'success';
                    $message['message'] = $fieldItem->field_name . ' Updated Successfuly!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($fieldItem->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            } else {
                $error = '';
                $message['type'] = 'error';
                $message['message'] = 'Update failed! Invalid Input<br/>';
                print_r(json_encode($message));
                exit;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        }
    }

}
