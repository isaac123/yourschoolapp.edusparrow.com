<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class ApprovalController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {

//        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Approval Management | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appointment/appointment.js');
        $this->assets->addJs('js/appscripts/application.js');
        $this->assets->addJs('js/appscripts/admission.js');
        $this->assets->addJs("js/transport/transport.js");
        $this->assets->addJs('js/appscripts/leave.js');
        $this->assets->addJs("js/attendance/attendance.js");
        $this->assets->addJs("js/feepayment/feepayment.js");
        $this->assets->addJs('js/studentroute/studentroute.js');
        $this->assets->addJs("js/voucher/voucher.js");
        $this->assets->addJs('js/payroll/advance.js');
        $this->assets->addJs('js/payroll/payroll.js');
    }

    public function isApprovalRequiredAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $this->tag->prependTitle("Approval Management | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/approval.js');
        if ($this->request->isPost()) {
            $typename = $this->request->getPost('type');
            $isreq = ApprovalTypes::findFirstByApprovalType($typename);
            if ($isreq->is_required && $isreq->is_required == 1) {
                if ($isreq->is_default_by_role) {
                    $param = array('role' => $isreq->is_default_by_role,
                        'appkey' => APPKEY,
                        'subdomain' => SUBDOMAIN,
                        'businesskey' => BUSINESSKEY);
                    $mail_data_string = json_encode($param);
//                      print_r($param); 
                    $response = IndexController::curlIt(USERAUTHAPI . 'getStaffByRole', $mail_data_string);
//                   print_r($response);;
                    $roleuser = json_decode($response);
//                    print_r($mailloginCreated);
//                    exit;
                    if (!$roleuser->status) {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                    if ($roleuser->status == 'ERROR') {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $roleuser->messages . '</div>';
                        print_r(json_encode($message));
                        exit;
                    } else if ($roleuser->status == 'SUCCESS' && $roleuser->data) {
                        $staffinfo = StaffInfo::findFirstByLoginid($roleuser->data);
                        $message['message'] = 'default';
                        $message['byrole'] = $isreq->is_default_by_role;
                        $message['forwardto'] = $staffinfo->id;
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    $message['type'] = 'success';
                    $message['message'] = 'yes';
                    print_r(json_encode($message));
                    exit;
                }
            } else {
                $message['type'] = 'success';
                $message['message'] = 'no';
                $message['appTypeid'] = $isreq->id;
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function approvalForwadAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Approval Management | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/approval.js');
    }

    public function addNewApprovalAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        try {
            if ($this->request->isPost()) {
                $approval_type_id = ApprovalTypes::findFirstByApprovalType($this->request->getPost('type'))->id;
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                $Item_id = $this->request->getPost('itemID');
                $requested_by = $uid; //$this->request->getPost('staffID');
                $requested_date = time();
                $approval_status = $this->request->getPost('status');
                $Approved_by = '';
                $Approval_date = '';
                $modified_by = $uid; //$this->request->getPost('staffID');
                $modified_date = time();
                $forwaded_to = $this->request->getPost('forwardTo');
                $comments = $this->request->getPost('comments');

                $appMaster = ApprovalMaster::findFirst("approval_type_id = $approval_type_id and Item_id = $Item_id") ?
                        ApprovalMaster::findFirst("approval_type_id = $approval_type_id and Item_id = $Item_id") :
                        new ApprovalMaster();
                if ($appMaster->id > 0) {
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">Duplicate request</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $appMaster->approval_type_id = $approval_type_id;
                    $appMaster->Item_id = $Item_id;
                    $appMaster->requested_by = $requested_by;
                    $appMaster->requested_date = $requested_date;
                    $appMaster->approval_status = $approval_status;
                    $appMaster->Approved_by = $Approved_by;
                    $appMaster->Approval_date = $Approval_date;
                    $appMaster->modified_by = $modified_by;
                    $appMaster->modified_date = $modified_date;
                    $appItem = new ApprovalItem();
                    $appItem->comments = $comments;
                    $appItem->created_by = $modified_by;
                    $appItem->created_date = $modified_date;
                    $appItem->forwaded_to = $forwaded_to;
                    $appItem->status = $approval_status;
                    if ($appMaster->save()) {
                        $appItem->ApprovalMaster = $appMaster;
                        if ($appItem->save()) {
                            $message['type'] = 'success';
                            $message['status'] = $approval_status;
                            $message['itemID'] = $Item_id;
                            $message['appType'] = $this->request->getPost('type');
                            $message['appTypeId'] = $approval_type_id;
                            $message['message'] = '<div class="alert alert-block alert-success fade in">Fowarded Successfully</div>';

                            print_r(json_encode($message));
                            exit;
                        } else {
                            foreach ($appItem->getMessages() as $message) {
                                $error .= $message;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        foreach ($appMaster->getMessages() as $message) {
                            $error .= $message;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function escalateAppItemAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {

            $appItem = new ApprovalItem();
            $appMaster = ApprovalMaster::findFirstById($this->request->getPost('appitemID'));
            $approval_status = "Escalated";
            $identity = $this->auth->getIdentity();
            $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;

            $appMaster->approval_status = $approval_status;
            $appMaster->modified_by = $uid;
            $appMaster->modified_date = time();

            $appItem->comments = $this->request->getPost('comments');
            $appItem->created_by = $uid;
            $appItem->created_date = time();
            $appItem->forwaded_to = $this->request->getPost('forwardTo');
            $appItem->ApprovalMaster = $appMaster;
            $appItem->status = $approval_status;

            if ($appMaster->save()) {
                if ($appItem->save()) {
                    $message['type'] = 'success';
                    $message['status'] = $approval_status;
                    $message['itemID'] = $appMaster->Item_id;
                    $message['appTypeId'] = ApprovalTypes::findFirstById($this->request->getPost('appType'))->id;
                    $message['appType'] = ApprovalTypes::findFirstById($this->request->getPost('appType'))->approval_type;
                    $message['message'] = '<div class="alert alert-block alert-success fade in">' . $approval_status . ' Successfully</div>';

                    print_r(json_encode($message));
                    exit;
                } else {
                    foreach ($appItem->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            } else {
                foreach ($appMaster->getMessages() as $message) {
                    $error .= $message;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function approveByAppItemAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {

            $appMaster = ApprovalMaster::findFirstById($this->request->getPost('appitemID'));
            $approval_status = "Approved";
            $identity = $this->auth->getIdentity();
            $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
            ;
            $appItem = new ApprovalItem();
            $appMaster->approval_status = $approval_status;
            $appMaster->Approved_by = $uid;
            $appMaster->Approval_date = time();
            $appMaster->modified_by = $uid;
            $appMaster->modified_date = time();

            $appItem->comments = $this->request->getPost('comments');
            $appItem->created_by = $uid;
            $appItem->created_date = time();
            $appItem->forwaded_to = $uid;
            $appItem->ApprovalMaster = $appMaster;
            $appItem->status = $approval_status;

            if ($appMaster->save()) {
                if ($appItem->save()) {
                    $message['type'] = 'success';
                    $message['status'] = $approval_status;
                    $message['itemID'] = $appMaster->Item_id;
                    $message['appTypeId'] = ApprovalTypes::findFirstById($this->request->getPost('appType'))->id;
                    $message['appType'] = ApprovalTypes::findFirstById($this->request->getPost('appType'))->approval_type;
                    $message['message'] = '<div class="alert alert-block alert-success fade in">' . $approval_status . ' Successfully</div>';
                    $message['swalmessage'] = $approval_status . ' Successfully!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    foreach ($appItem->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    $message['swalmessage'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            } else {
                foreach ($appMaster->getMessages() as $messages) {
                    $error .= $messages . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                $message['swalmessage'] = $error;
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function rejectByAppItemAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $appMaster = ApprovalMaster::findFirstById($this->request->getPost('appitemID'));
            $approval_status = "Rejected";
            $identity = $this->auth->getIdentity();
            $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
            $appItem = new ApprovalItem();

            $appMaster->approval_status = $approval_status;
            $appMaster->Approved_by = $uid;
            $appMaster->Approval_date = time();
            $appMaster->modified_by = $uid;
            $appMaster->modified_date = time();

            $appItem->comments = $this->request->getPost('comments');
            $appItem->created_by = $uid;
            $appItem->created_date = time();
            $appItem->forwaded_to = $uid;
            $appItem->ApprovalMaster = $appMaster;
            $appItem->status = $approval_status;
//            print_r($appItem);exit;
            if ($appMaster->save()) {
                if ($appItem->save()) {

                    $message['type'] = 'success';
                    $message['status'] = $approval_status;
                    $message['itemID'] = $appMaster->Item_id;
                    $message['appTypeId'] = ApprovalTypes::findFirstById($this->request->getPost('appType'))->id;
                    $message['appType'] = ApprovalTypes::findFirstById($this->request->getPost('appType'))->approval_type;
                    $message['message'] = '<div class="alert alert-block alert-success fade in">' . $approval_status . ' Successfully</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    foreach ($appItem->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            } else {
                foreach ($appMaster->getMessages() as $message) {
                    $error .= $message;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function viewForwardHistoryAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $params = array();
            foreach ($this->request->getPost() as $key => $value) {
                $params[$key] = $value;
            }
            $identity = $this->auth->getIdentity();
            $this->view->uid = $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
            if (isset($params['appitemID'])) {
                $appitemID = $this->request->getPost('appitemID');
                $this->view->appHistory = ApprovalItem::find(
                                array(
                                    "approval_master_id = " . $appitemID,
                                    "order" => "created_date DESC"));
                $this->view->appmaster = $appmaster = ApprovalMaster::findFirstById($appitemID);
                $this->view->appType = ApprovalTypes::findFirstById($appmaster->approval_type_id);
                $this->view->appItemaName = strtoupper($this->view->appType->approval_type) . "_" . $appmaster->id;
            } else if ($params['itemID']) {
                $itemID = $params['itemID'];
                $appTypId = $params['appTypId'];
                $this->view->appmaster = $appmaster = ApprovalMaster::findFirst('approval_type_id = ' . $appTypId .
                                ' and Item_id = ' . $itemID);
                $this->view->appHistory = ApprovalItem::find(
                                array(
                                    "approval_master_id = " . $appmaster->id,
                                    "order" => "created_date DESC"));
                $this->view->appType = ApprovalTypes::findFirstById($appmaster->approval_type_id);
                $this->view->appItemaName = strtoupper($this->view->appType->approval_type) . "_" . $appmaster->id;
            }
        }
    }

    public function getAllApprovalsByTypeIdAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Approval Management | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/approval.js');
        if ($this->request->isPost()) {

            $identity = $this->auth->getIdentity();
            $this->view->uid = $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
            $appTypeId = $this->request->getPost('appType');
            $this->view->appTypeList = ApprovalMaster::find('approval_type_id = ' . $appTypeId);
        }
    }

    public function loadSectionAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->classId = $this->request->getPost('classId');
        }
    }

    public function approvalTblHdrAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Application | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/approval.js');
    }

    public function loadApprovalDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = array();
        $params['fdate'] = $this->request->getPost('fdate') ? strtotime($this->request->getPost('fdate') . ' 00:00:00') : '';
        $params['tdate'] = $this->request->getPost('tdate') ? strtotime($this->request->getPost('tdate') . ' 23:59:59') : '';
        $params['appType'] = $this->request->getPost('appType') ? ($this->request->getPost('appType')) : '';
        $params['appStatus'] = $this->request->getPost('appStatus') ? ($this->request->getPost('appStatus')) : '';

        $orderphql = array();

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));

        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;
        $queryParams = '';
        $applications = '';
        $totapplications = '';
        if (count($selectedMasterArr) > 0) {
            $queryParams .= ' id IN (' . implode(',', $selectedMasterArr) . ')';

            if ($params['fdate'] != '' && $params['tdate'] != '') {
                $queryParams .= ' and requested_date >= "' . $params['fdate'] . '"';
                $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
            }

            if ($params['appType'] != ''):
                $queryParams .= ' and approval_type_id = "' . $params['appType'] . '"';
            endif;
            if ($params['appStatus'] != ''):
                $queryParams .= ' and approval_status = "' . $params['appStatus'] . '"';
            endif;
            for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
                $orderphql[] = $this->getSortColumnNameForApproval($this->request->getPost('iSortCol_' . $i)) . ' ' .
                        $this->request->getPost('sSortDir_' . $i);
            }

//        echo 'hai';     print_r($queryParams);exit;
            $applications = ApprovalMaster::find(array(
                        $queryParams,
                        "order" => "" . implode(',', $orderphql) . "",
                        "limit" => array(
                            "number" => $this->request->getPost('iDisplayLength'),
                            "offset" => $this->request->getPost('iDisplayStart')
                        )
            ));

            $totapplications = ApprovalMaster::find(array(
                        $queryParams,
                        "columns" => "COUNT(*)",
            ));
        } else {
            
        }


        $rowEntries = $this->formatApprovalTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => count($totapplications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForApproval($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "id";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "id";
            case 1:
                return "Item_id";
            case 2:
                return "requested_by";
            case 3:
                return "requested_date";
            case 4:
                return "approval_status";
            case 5:
                return "Approved_by";
            case 6:
                return "Approval_date";
            default:
                return "id";
        }
    }

    public function formatApprovalTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->id ? $items->id : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type) . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $reqby = StaffInfo::findFirstById($items->requested_by);
                $row['requested_by'] = $items->requested_by ? $reqby->Staff_Name . ' (' . $reqby->loginid . ')' : $items->requested_by;
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '';
                $appby = StaffInfo::findFirstById($items->Approved_by);
                $row['Approved_by'] = $items->Approved_by ? $appby->Staff_Name . ' (' . $appby->loginid . ')' : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '';
                $row['labelclass'] = $labelclass;
                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->id);

                $forwardto = $appitem->getLast()->forwaded_to;
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;

                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    public function loadApprovedAppAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Application | ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs('js/appscripts/approval.js');
//        $this->assets->addJs('js/appscripts/application.js');
    }

    public function applicationTblHdrAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->divMName = ControllerBase::get_division_name_student();
    }

    public function loadApplicationDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {
            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'subDivVal') {
                $params[$IsSubdiv[0]][] = $value;
            } else {

                $params[$key] = $value;
            }
        }

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;


        //print_r($applications);
        // exit;


        $orderphql = array();
        if (count($selectedMasterArr) > 0):
            $queryParams[] = ' appm.id IN  (' . implode(',', $selectedMasterArr) . ')';
        endif;

        if (isset($params['academicYrId'])):
            $queryParams[] = 'app.academic_year_id = ' . $params['academicYrId'];
        endif;

        if (isset($params['applicatio_no'])):
            $queryParams[] = 'app.application_no = "' . $params['applicatio_no'] . '"';
        endif;
        if (isset($params['name'])):
            $queryParams[] = 'app.Student_Name Like "%' . $params['name'] . '%"';
        endif;
        if (isset($params['divID'])):
            $queryParams[] = 'app.Division_Class = ' . $params['divID'];
        endif;
        if (isset($params['subDivVal']) && count($params['subDivVal']) > 0):
            $queryParams[] = 'app.Subdivision IN (' . implode(',', $params['subDivVal']) . ')';
        endif;
        if (isset($params['appStatus'])):
            $queryParams[] = 'appm.approval_status Like "%' . $params['appStatus'] . '%"';
        endif;

        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = 'appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '" and appm.requested_date <= "' . strtotime($params['fdate'] . ' 23:59:59') . '"';
            //  $queryParams .= ' and requested_date <= "' . $params['tdate'] . '"';
        }
        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) . ' and' : '';

        //echo $conditionvals;
        $psql = 'SELECT appm.id as appmid,app.id,app.academic_year_id,app.application_no,app.application_no,
             app.Student_Name,app.Division_Class,app.Subdivision,app.Gender,app.Date_of_Birth,
             app.Date_of_Birth,app.Date_of_Joining,app.Address1,app.Phone,
             app.Email,appm.Item_id,appm.approval_status,appm.approval_type_id
          FROM Application app, ApprovalMaster appm, ApprovalTypes aptyp WHERE
           ' . $conditionvals . ''
                . ' appm.Item_id=app.id '
                . 'and aptyp.id=appm.approval_type_id '
                . 'and aptyp.approval_type = "Application"';
//echo $psql;exit;
        $totapplications_qry = $psql;

        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $psql .= ' ORDER BY ' . $this->getSortColumnNameForApplication($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }


        $psql .=' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        $applications = $this->modelsManager->executeQuery($psql);


        $totapplications = $this->modelsManager->executeQuery($totapplications_qry);

        $rowEntries = $this->formatApplicationTableData($applications);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($applications),
            "iTotalDisplayRecords" => count($totapplications),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForApplication($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "app.application_no";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "app.application_no";
            case 1:
                return "app.academic_year_id";
            case 2:
                return "app.Student_Name";
            case 3:
                return "app.Gender";
            case 4:
                return "app.Division_Class";
            case 5:
                return "app.Date_of_Birth";
            case 6:
                return "app.Date_of_Joining";

            default:
                return "app.application_no";
        }
    }

    public function formatApplicationTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();


//                $app_sts = ($items->id) ? ApprovalMaster::findFirst('Item_id =' . $items->id) : '';
//               print_r('test'. $app_sts->approval_status);
//                exit;

                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->appmid ? $items->appmid : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';

                $row['application_id'] = $items->id ? $items->id : '';
                $row['address'] = $items->Address1 . ',' . $items->Address2;
                $row['mobile'] = $items->Phone ? $items->Phone : '';
                $row['Email'] = $items->Email ? $items->Email : '';
                $row['application_no'] = $items->application_no ? $items->application_no : '';
                $row['academicYr'] = AcademicYearMaster::findFirstById($items->academic_year_id)->academic_year_name;
                $row['Student_Name'] = $items->Student_Name ? $items->Student_Name : '';
                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';
                $row['class'] = DivisionValues::findFirstById($items->Division_Class)->classname;
                $row['dob'] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row['doj'] = ($items->Date_of_Joining) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row['labelclass'] = $labelclass;
                $row['approval_status'] = $items->approval_status;
                $row['dataclass'] = $dataclass;
                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->appmid);

                $forwardto = $appitem->getLast()->forwaded_to;
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                // print_r('uid: '. $uid .'-frto: '.$forwardto);
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;
                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /* Leave Approval */

    public function manageLeavesAppAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Leave Management | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/leave.js');
        $this->assets->addJs('js/appscripts/approval.js');

//        $this->assets->addJs("js/attendance/attendance.js");
        $identity = $this->auth->getIdentity();
        $this->view->uid = $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $this->view->leaves = LeaveRequest::find('staffId = ' . $uid . ' ORDER BY status');
        $this->view->form = new LeaveRequestForm();
    }

    public function loadLeavetableHeaderAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function loadLeavetableDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        //$this->view->leaves = LeaveRequest::find('staffId = ' . $identity['id'] .' ORDER BY status');

        $fromDate_first = 0;
        $fromDate = $this->request->getPost('fromDate');
        $toDate_first = 0;
        $toDate = $this->request->getPost('toDate');
        $levType_first = 0;
        $levType = $this->request->getPost('levType');

        $status_first = 0;
        $status = $this->request->getPost('status');

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $currentlgndet = Users::findFirst('id =' . $uid);  //->login;
        $stfid = $uid;  // StaffInfo::findFirstByLoginid($currentlgndet->login)->id;

        $uid = $uid;
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;

        if ($this->request->getPost('emptySearch') == 0) {
            $phql = "SELECT leav.id,leav.leaveType,leav.fromDate,leav.toDate,leav.duration,
				  aprvlmas.approval_status,leav.reason,leav.staffId,
				  aprvlmas.Item_id,aprvlmas.approval_type_id,aprvlmas.Approved_by,aprvlmas.Approval_date,
                                  aprvlmas.requested_by,aprvlmas.requested_date
				  FROM LeaveRequest leav, ApprovalMaster aprvlmas,  ApprovalTypes aptyp WHERE aprvlmas.Item_id=leav.id
                                  and aptyp.id=aprvlmas.approval_type_id and aptyp.approval_type = 'Leave'";
            if (count($selectedMasterArr) > 0):
                $phql .= ' and aprvlmas.id IN  (' . implode(',', $selectedMasterArr) . ')';
            endif;
        } else if (empty($fromDate) && empty($toDate) && empty($levType) && empty($status)) {
            $phql = "SELECT leav.id,leav.leaveType,leav.fromDate,leav.toDate,leav.duration,
				  aprvlmas.approval_status,leav.status,leav.reason,leav.staffId,
				  aprvlmas.Item_id,aprvlmas.approval_type_id,aprvlmas.Approved_by,aprvlmas.Approval_date,
                                  aprvlmas.requested_by,aprvlmas.requested_date
				  FROM LeaveRequest leav, ApprovalMaster aprvlmas,  ApprovalTypes aptyp WHERE aprvlmas.Item_id=leav.id
                                  and aptyp.id=aprvlmas.approval_type_id and aptyp.approval_type = 'Leave'";

            if (count($selectedMasterArr) > 0):
                $phql .= ' and aprvlmas.id IN  (' . implode(',', $selectedMasterArr) . ')';
            endif;
        } else if ($fromDate != '' && $toDate != '') {
            $phql = "SELECT leav.id,leav.leaveType,leav.fromDate,leav.toDate,leav.duration,
				  aprvlmas.approval_status,leav.status,leav.reason,leav.staffId,
				  aprvlmas.Item_id,aprvlmas.approval_type_id,aprvlmas.Approved_by,aprvlmas.Approval_date,
                                  aprvlmas.requested_by,aprvlmas.requested_date
				  FROM LeaveRequest leav, ApprovalMaster aprvlmas,  ApprovalTypes aptyp WHERE aprvlmas.Item_id=leav.id
                                  and aptyp.id=aprvlmas.approval_type_id and aptyp.approval_type = 'Leave' ";

            if (count($selectedMasterArr) > 0):
                $phql .= 'and aprvlmas.id IN  (' . implode(',', $selectedMasterArr) . ')';
                $phql .=" and ('" . strtotime(date('d-m-Y', strtotime($fromDate)) . " 00:00:00") . "'<=leav.fromDate";
            endif;

//            if ($currentlgndet->login != 'superadmin') {
//                // $phql .= " leav.staffId='".$stfid ."' and (('" . strtotime(date('d-m-Y', strtotime($fromDate)). " 00:00:00") . "'>=leav.fromDate and '" . strtotime(date('d-m-Y', strtotime($fromDate)). " 00:00:00") . "' <=leav. toDate)";
//                $phql .= " leav.staffId='" . $stfid . "' and ('" . strtotime(date('d-m-Y', strtotime($fromDate)) . " 00:00:00") . "'<=leav.fromDate";
//            } else {
//                //$phql .=" (('" . strtotime(date('d-m-Y', strtotime($fromDate)). " 00:00:00") . "'>=leav.fromDate and '" . strtotime(date('d-m-Y', strtotime($fromDate)). " 00:00:00") . "' <=leav. toDate)";
//
//                $phql .=" ('" . strtotime(date('d-m-Y', strtotime($fromDate)) . " 00:00:00") . "'<=leav.fromDate";
//            }
            $fromDate_first = 1;
        } else if ($levType) {
            $phql = "SELECT leav.id,leav.leaveType,leav.fromDate,leav.toDate,leav.duration,
				  aprvlmas.approval_status,leav.status,leav.reason,leav.staffId,
				  aprvlmas.Item_id,aprvlmas.approval_type_id,aprvlmas.Approved_by,aprvlmas.Approval_date,
                                  aprvlmas.requested_by,aprvlmas.requested_date
				  FROM LeaveRequest leav, ApprovalMaster aprvlmas,  ApprovalTypes aptyp WHERE aprvlmas.Item_id=leav.id
                                  and aptyp.id=aprvlmas.approval_type_id and aptyp.approval_type = 'Leave' ";

            if (count($selectedMasterArr) > 0):
                $phql .= 'and aprvlmas.id IN  (' . implode(',', $selectedMasterArr) . ')';
                $phql .=" and leav.leaveType='" . $levType . "'";
            endif;
//            if ($currentlgndet->login != 'superadmin') {
//                $phql .= " leav.staffId='" . $stfid . "' and leav.leaveType='" . $levType . "'";
//            } else {
//                $phql .=" leav.leaveType='" . $levType . "'";
//            }
            $levType_first = 1;
        } else if ($status) {
            $phql = "SELECT leav.id,leav.leaveType,leav.fromDate,leav.toDate,leav.duration,
				  aprvlmas.approval_status,leav.status,leav.reason,leav.staffId,
				  aprvlmas.Item_id,aprvlmas.approval_type_id,aprvlmas.Approved_by,aprvlmas.Approval_date,
                                  aprvlmas.requested_by,aprvlmas.requested_date
				  FROM LeaveRequest leav, ApprovalMaster aprvlmas,  ApprovalTypes aptyp WHERE aprvlmas.Item_id=leav.id
                                  and aptyp.id=aprvlmas.approval_type_id and aptyp.approval_type = 'Leave' ";

            if (count($selectedMasterArr) > 0):
                $phql .= 'and aprvlmas.id IN  (' . implode(',', $selectedMasterArr) . ')';
                $phql .=" and aprvlmas.approval_status LIKE '" . $status . "%'";
            endif;

//            if ($currentlgndet->login != 'superadmin') {
//                $phql .= " leav.staffId='" . $stfid . "' and aprvlmas.approval_status LIKE '" . $status . "%'";
//            } else {
//                $phql .=" aprvlmas.approval_status LIKE '" . $status . "%'";
//            }
            $status_first = 1;
        }

        if (!empty($fromDate) && $fromDate != "" && !$fromDate_first) {

            $phql .=" and '" . strtotime(date('d-m-Y', strtotime($fromDate)) . " 00:00:00") . "'<=leav.fromDate)";
        }
        if (!empty($toDate) && $toDate != "") {
            if (!$fromDate_first) {
                $phql .=" and '" . strtotime(date('d-m-Y', strtotime($toDate)) . " 00:00:00") . "' >=leav. toDate)";
            } else {
                $phql .=" and '" . strtotime(date('d-m-Y', strtotime($toDate)) . " 00:00:00") . "' >=leav. toDate)" .
                        "or ('" . strtotime(date('d-m-Y', strtotime($fromDate)) . " 00:00:00") . "'> leav.fromDate" .
                        " and '" . strtotime(date('d-m-Y', strtotime($fromDate)) . " 00:00:00") . "'<= leav.toDate" .
                        " and '" . strtotime(date('d-m-Y', strtotime($toDate)) . " 00:00:00") . "'>= leav.toDate)" .
                        "or ('" . strtotime(date('d-m-Y', strtotime($toDate)) . " 00:00:00") . "'< leav.toDate" .
                        " and '" . strtotime(date('d-m-Y', strtotime($fromDate)) . " 00:00:00") . "'<= leav.fromDate" .
                        " and '" . strtotime(date('d-m-Y', strtotime($toDate)) . " 00:00:00") . "'>= leav.fromDate)";
            }
        }


        if (!empty($levType) && $levType != "" && !$levType_first)
            $phql .=" and leav.leaveType='" . $levType . "'";

        if (!empty($status) && $status != "" && !$status_first)
            $phql .=" and aprvlmas.approval_status LIKE'" . $status . "%'";


        //$phql .=' GROUP BY leav.id';

        $phql2 = $phql;

        /* for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
          $phql .= ' ORDER BY ' . $this->getSortColumnName($this->request->getPost('iSortCol_' . $i)) . ' ' .
          $this->request->getPost('sSortDir_' . $i);
          } */
        $phql .= ' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');

//       print_r($phql);
//	exit;
        $result = $this->modelsManager->executeQuery($phql);


        $result2 = $this->modelsManager->executeQuery($phql2);
        //  print_r($result2);
//	 exit;

        $rowEntries = $this->formatLeaveTableData($result);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($rowEntries),
            "iTotalDisplayRecords" => count($result2),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    /* public function getSortColumnName($sortColumnIndex) {
      if (!isset($sortColumnIndex)) {
      return "id";
      }
      switch ($sortColumnIndex) {

      case 0:
      return "academic_year_id";
      case 1:
      return "appointment_no";
      case 2:
      return "Staff_Name";
      case 4:
      return "Department";
      case 5:
      return "loginid";
      default:
      return "id";
      }
      } */

    public function formatLeaveTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):

                $row = array();
                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' : 'info');
                $usrlginid = Users::findFirstById($items->requested_by)->login;
                $approvedby = ($items->requested) ? StaffInfo::findFirstByLoginid($usrlginid)->Staff_Name : '';
                $apprvlmsdet = ApprovalMaster::find("Item_id =" . $items->id . " and approval_type_id =" . $items->approval_type_id);
                $apprvlmsid = $apprvlmsdet->getLast()->id;
                $row['aprvlmasid'] = $apprvlmsid; // ApprovalMaster::findFirst("Item_id =" . $items->id)->id;
                $row['aprvltypid'] = $items->approval_type_id;

                $row['levtype'] = AttendanceSelectbox::findFirstById($items->leaveType)->attendancename;
                $row['frmdate'] = date('d-m-Y', $items->fromDate);
                $row['todate'] = date('d-m-Y', $items->toDate);
                $row['duration'] = $items->duration;
                $row['apprvdby'] = $approvedby;
                $row['apprvdate'] = (date('d-m-Y', $items->requested_date)) ? date('d-m-Y', $items->requested_date) : '';

                $row['statuscls'] = $labelclass;
                $row['status'] = $items->approval_status;

                if (!in_array($items->approval_status, array('Escalated', 'Approved', 'Rejected')))
                    $row['editture'] = 1;
                else
                    $row['editture'] = '';

                $row['id'] = $items->id;

                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $apprvlmsid);

                $forwardto = $appitem->getLast()->forwaded_to;
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                // print_r('uid: '. $uid .'-frto: '.$forwardto);
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;


                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /* Finance Approval */

    public function financeManagAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/approval.js');
        $this->assets->addJs("js/voucher/voucher.js");
        $this->assets->addJs("js/feepayment/feepayment.js");
        $this->assets->addJs('js/appscripts/admission.js');
        $this->assets->addJs('js/studentroute/studentroute.js');
        $this->assets->addJs("js/voucher/voucher.js");
        $this->assets->addJs('js/payroll/advance.js');
        $this->assets->addJs('js/payroll/payroll.js');
    }

    public function loadFinanceTblAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function loadFinanceDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = array();
        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;

            if ($key == 'fdate')
                $params[$key] = $value ? strtotime($value . ' 00:00:00') : '';

            if ($key == 'tdate')
                $params[$key] = $value ? strtotime($value . ' 23:59:59') : '';
        }


        $orderphql = array();
        $queryParams = array();

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;
        $queryParams = '';
        $applications = '';
        $totapplications = '';
        if (count($selectedMasterArr) > 0) {
            $queryParams[] = ' appm.id IN (' . implode(',', $selectedMasterArr) . ')';
        }
        if ((isset($params['fdate']) && $params['fdate'] != '') &&
                (isset($params['tdate']) && $params['tdate'] != '')) {
            $queryParams[] = ' ldgrvs.date >= "' . $params['fdate'] . '"';
            $queryParams[] = ' ldgrvs.date <= "' . $params['tdate'] . '"';
        }

        if ($params['appType'] != ''):
            $queryParams[] = ' appm.approval_type_id = "' . $params['appType'] . '"';
        endif;
        if ($params['appStatus'] != ''):
            $queryParams[] = ' appm.approval_status = "' . $params['appStatus'] . '"';
        endif;


        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $queryParams1[] = 'appm.Item_id Like "' . $params['sSearch'] . '"';
            $queryParams1[] = 'appm.approval_status  Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;
//        print_r($params['sSearch']);exit;
        $queryParams[] = ' aptyp.id=appm.approval_type_id ';
        $queryParams[] = ' appm.Item_id=ldgrvs.voucher_id ';
        $queryParams[] = ' aptyp.approval_type IN("Journal Voucher","Payment Voucher","Suspense Voucher","Contra Voucher") ';
        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
//        print_r($conditionvals);exit;
        $phql = 'SELECT appm.id,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,ldgrvs.voucher_id,
                        ldgrvs.amount,ldgrvs.creditledger,ldgrvs.debitledger
                        FROM ApprovalMaster appm, LedgerVoucher ldgrvs, ApprovalTypes aptyp WHERE
           ' . $conditionvals;


        $totfinances_qry = 'SELECT count(*) as cnt
                        FROM ApprovalMaster appm, LedgerVoucher ldgrvs, ApprovalTypes aptyp WHERE
           ' . $conditionvals;
        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $phql .= ' ORDER BY ' . $this->getSortColumnNameForFinanceApproval($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }
        $phql .= ' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');

        $finances = $this->modelsManager->executeQuery($phql);

        $totfinances = $this->modelsManager->executeQuery($totfinances_qry);

        $rowEntries = $this->formatFinanceApprovalTableData($finances);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($rowEntries),
            "iTotalDisplayRecords" => $totfinances[0]->cnt,
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForFinanceApproval($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "appm.id";
        }
        switch ($sortColumnIndex) {

            case 1:
                return "ldgrvs.voucher_id";
            case 3:
                return "ldgrvs.creditledger";
            case 4:
                return "ldgrvs.debitledger";
            case 5:
                return "ldgrvs.amount";
            case 6:
                return "appm.requested_by";
            case 7:
                return "appm.requested_date";
            case 8:
                return "appm.approval_status";

            default:
                return "appm.id";
        }
    }

    public function formatFinanceApprovalTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;


                $labelclass = ($items->approval_status == 'Approved') ? 'success' :
                        (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->id ? $items->id : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type)
                        . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $row['requested_by'] = $items->requested_by ? StaffInfo::findFirstById($items->requested_by)->loginid : '';
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '';
                $row['Approved_by'] = $items->Approved_by ? StaffInfo::findFirstById($items->Approved_by)->loginid : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '';
                $row['labelclass'] = $labelclass;

                $row['voucherid'] = $items->voucher_id ? $items->voucher_id : '';
                $row['voucher_crdt'] = $items->creditledger ? OrganizationalStructureValues::findFirstById($items->creditledger)->name : '';
                $row['voucher_dbt'] = $items->debitledger ? OrganizationalStructureValues::findFirstById($items->debitledger)->name : '';
                $row['voucher_amt'] = $items->amount ? $items->amount : '';

                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->id);

                $row['checkapprv'] = '';
                $row['checkapprvstatustd'] = '';
                $forwardto = $appitem->getLast()->forwaded_to;
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['checkapprv'] = '<input type="checkbox" class="select_chkbx" id="">';
                    $row['checkapprvstatustd'] = 'enabledcheckbx';
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;

                $row['voucherdet'] = ' <label class="label label-primary" style="cursor:pointer;"><a class="white_font" herf="javascript:;" 
                                                    onclick="voucherSettings.viewVoucherFromApprvl(this)"  
                                                    id="' . $items->voucher_id . '">' . $items->voucher_id . '</a></label>';

                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /* Fee Approval */

    public function feeManagAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/approval.js');
        $this->assets->addJs("js/voucher/voucher.js");
        $this->assets->addJs("js/feepayment/feepayment.js");
        $this->assets->addJs('js/appscripts/admission.js');
        $this->assets->addJs('js/studentroute/studentroute.js');
        $this->assets->addJs("js/voucher/voucher.js");
        $this->assets->addJs('js/payroll/advance.js');
        $this->assets->addJs('js/payroll/payroll.js');
    }

    public function loadFeeTblAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function loadFeeDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = array();
        $params['fdate'] = $this->request->getPost('fdate') ? strtotime($this->request->getPost('fdate') . ' 00:00:00') : '';
        $params['tdate'] = $this->request->getPost('tdate') ? strtotime($this->request->getPost('tdate') . ' 23:59:59') : '';
        $params['appType'] = $this->request->getPost('appType') ? ($this->request->getPost('appType')) : '';
        $params['appStatus'] = $this->request->getPost('appStatus') ? ($this->request->getPost('appStatus')) : '';
        $params['selectedstuID'] = $this->request->getPost('selectedstuID') ? ($this->request->getPost('selectedstuID')) : '';

        $orderphql = array();
        $queryParams = array();

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));

        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;
        // $queryParams = '';
        $applications = '';
        $totapplications = '';
        if (count($selectedMasterArr) > 0) {
            $queryParams[] = ' appm.id IN (' . implode(',', $selectedMasterArr) . ')';
        }
        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = ' appm.requested_date >= "' . $params['fdate'] . '"';
            $queryParams[] = ' appm.requested_date <= "' . $params['tdate'] . '"';
        }

        if ($params['appType'] != ''):
            $queryParams[] = ' appm.approval_type_id = "' . $params['appType'] . '"';
        endif;
        if ($params['appStatus'] != ''):
            $queryParams[] = ' appm.approval_status = "' . $params['appStatus'] . '"';
        endif;

        if ($params['selectedstuID'] != ''):
            $queryParams[] = ' stufee.student_id = "' . $params['selectedstuID'] . '"';
        endif;

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) . ' and' : '';
        $phql = 'SELECT appm.id,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,stufee.student_id,
                        stufee.fee_item_id,stufee.status
                        FROM ApprovalMaster appm, StudentFeeTable stufee, ApprovalTypes aptyp WHERE
           ' . $conditionvals . ' '
                . ' aptyp.id=appm.approval_type_id '
                . 'and appm.Item_id=stufee.id '
                . 'and aptyp.approval_type IN("Fee cancel","General Fees")';


        $totfinances_qry = $phql;

//        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
//            $phql .= ' ORDER BY ' . $this->getSortColumnNameForFeeApproval($this->request->getPost('iSortCol_' . $i)) . ' ' .
//                    $this->request->getPost('sSortDir_' . $i);
//        }
        $phql .= ' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');

//      print_r($phql);
//      exit;
        $finances = $this->modelsManager->executeQuery($phql);


        $totfinances = $this->modelsManager->executeQuery($totfinances_qry);
//          print_r(count($totfinances));
//	 exit;

        $rowEntries = $this->formatFeeApprovalTableData($finances);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($rowEntries),
            "iTotalDisplayRecords" => count($totfinances),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

//    public function getSortColumnNameForFeeApproval($sortColumnIndex) {
//        if (!isset($sortColumnIndex)) {
//            return "appm.id";
//        }
//        switch ($sortColumnIndex) {
//
//            case 0:
//                return "ldgrvs.voucher_id";
//            case 2:
//                return "ldgrvs.creditledger";
//            case 3:
//                return "ldgrvs.debitledger";
//            case 4:
//                return "ldgrvs.amount";
//            case 5:
//                return "appm.requested_by";
//            case 6:
//                return "appm.requested_date";
//            case 7:
//                return "appm.approval_status";
//
//            default:
//                return "appm.id";
//        }
//    }

    public function formatFeeApprovalTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;


                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->id ? $items->id : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type) . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $row['requested_by'] = $items->requested_by ? Users::findFirstById($items->requested_by)->login : '';
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '';
                $row['Approved_by'] = $items->Approved_by ? Users::findFirstById($items->Approved_by)->login : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '';
                $row['labelclass'] = $labelclass;


                $row['stuid'] = $items->student_id ? $items->student_id : '';

                $studet = $items->student_id ? StudentInfo::findFirstById($items->student_id) : '';


                $divisiondet = $studet->Division_Class ? DivisionValues::findFirstById($studet->Division_Class) : '';
                $subdivsndets = $studet->Division_Class ? SubDivisionValues::find(' id IN (' . $studet->Subdivision . ')') : '';

                $class_sec = 'Class-' . ($divisiondet->classname ? $divisiondet->classname : '');

                if (count($subdivsndets) > 0) {
                    foreach ($subdivsndets as $subdivsndet) {
                        $class_sec .= $subdivsndet->name ? '-' . $subdivsndet->name : '';
                    }
                }

                $fee_itmdet = $items->fee_item_id ? FeeItems::findFirstById($items->fee_item_id) : '';

                $row['stuname'] = $studet->Student_Name ? $studet->Student_Name : '';
                $row['class_sec'] = $class_sec ? $class_sec : '';
                $row['feetype'] = $fee_itmdet->fee_group_id ? ucwords(FeeGroup::findFirstById($fee_itmdet->fee_group_id)->group_name) : '';
                $row['feename'] = $fee_itmdet->fee_name ? $fee_itmdet->fee_name : '';
                $row['feeamt'] = $fee_itmdet->fee_amount ? $fee_itmdet->fee_amount : '';

                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->id);

                $forwardto = $appitem->getLast()->forwaded_to;
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;

                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /* Transport Approval */

    public function transportManagAction() {
//        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/approval.js');
        $this->assets->addJs("js/voucher/voucher.js");
        $this->assets->addJs("js/feepayment/feepayment.js");
        $this->assets->addJs('js/appscripts/admission.js');
        $this->assets->addJs('js/studentroute/studentroute.js');
        $this->assets->addJs("js/voucher/voucher.js");
        $this->assets->addJs('js/payroll/advance.js');
        $this->assets->addJs('js/payroll/payroll.js');
    }

    public function loadTransportTblAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function loadTransportDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = array();
        $params['fdate'] = $this->request->getPost('fdate') ? strtotime($this->request->getPost('fdate') . ' 00:00:00') : '';
        $params['tdate'] = $this->request->getPost('tdate') ? strtotime($this->request->getPost('tdate') . ' 23:59:59') : '';
        $params['appType'] = $this->request->getPost('appType') ? ($this->request->getPost('appType')) : '';
        $params['appStatus'] = $this->request->getPost('appStatus') ? ($this->request->getPost('appStatus')) : '';
        $params['selectedstuID'] = $this->request->getPost('selectedstuID') ? ($this->request->getPost('selectedstuID')) : '';

        $orderphql = array();
        $queryParams = array();

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));

        $selectedMasterArr = array();
        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;
        // $queryParams = '';
        $applications = '';
        $totapplications = '';
        if (count($selectedMasterArr) > 0) {
            $queryParams[] = ' appm.id IN (' . implode(',', $selectedMasterArr) . ')';
        }
        if ($params['fdate'] != '' && $params['tdate'] != '') {
            $queryParams[] = ' appm.requested_date >= "' . $params['fdate'] . '"';
            $queryParams[] = ' appm.requested_date <= "' . $params['tdate'] . '"';
        }

        if ($params['appType'] != ''):
            $queryParams[] = ' appm.approval_type_id = "' . $params['appType'] . '"';
        endif;
        if ($params['appStatus'] != ''):
            $queryParams[] = ' appm.approval_status = "' . $params['appStatus'] . '"';
        endif;

        if ($params['selectedstuID'] != ''):
            $queryParams[] = ' stufee.student_id = "' . $params['selectedstuID'] . '"';
        endif;

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) . ' and' : '';

        $phql = 'SELECT appm.id,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,stufee.student_id,
                        stufee.fee_item_id,stufee.status
                        FROM ApprovalMaster appm, StudentFeeTable stufee, ApprovalTypes aptyp WHERE
           ' . $conditionvals . ' '
                . ' aptyp.id=appm.approval_type_id '
                . 'and appm.Item_id=stufee.id '
                . 'and aptyp.approval_type IN("Transport")';


        $totfinances_qry = $phql;

//        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
//            $phql .= ' ORDER BY ' . $this->getSortColumnNameForTransportApproval($this->request->getPost('iSortCol_' . $i)) . ' ' .
//                    $this->request->getPost('sSortDir_' . $i);
//        }
        $phql .= ' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');

//      print_r($phql);
//      exit;
        $finances = $this->modelsManager->executeQuery($phql);


        $totfinances = $this->modelsManager->executeQuery($totfinances_qry);
//          print_r(count($totfinances));
//	 exit;

        $rowEntries = $this->formatTransportApprovalTableData($finances);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($rowEntries),
            "iTotalDisplayRecords" => count($totfinances),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

//    public function getSortColumnNameForTransportApproval($sortColumnIndex) {
//        if (!isset($sortColumnIndex)) {
//            return "appm.id";
//        }
//        switch ($sortColumnIndex) {
//
//            case 0:
//                return "ldgrvs.voucher_id";
//            case 2:
//                return "ldgrvs.creditledger";
//            case 3:
//                return "ldgrvs.debitledger";
//            case 4:
//                return "ldgrvs.amount";
//            case 5:
//                return "appm.requested_by";
//            case 6:
//                return "appm.requested_date";
//            case 7:
//                return "appm.approval_status";
//
//            default:
//                return "appm.id";
//        }
//    }

    public function formatTransportApprovalTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $identity = $this->auth->getIdentity();
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;


                $labelclass = ($items->approval_status == 'Approved') ? 'success' : (($items->approval_status == 'Rejected') ? 'danger' :
                                (($items->approval_status == 'Issued') ? 'warning' : 'primary'));
                $row['approval_id'] = $items->id ? $items->id : '';
                $row['approval_item'] = strtoupper(ApprovalTypes::findFirstById($items->approval_type_id)->approval_type) . "_" . $items->Item_id;
                $row['item_id'] = $items->Item_id ? $items->Item_id : '';
                $row['approval_type_id'] = $items->approval_type_id ? $items->approval_type_id : '';
                $row['requested_by'] = $items->requested_by ? Users::findFirstById($items->requested_by)->login : '';
                $row['requested_date'] = $items->requested_date ? date('d-m-Y', $items->requested_date) : '';
                $row['approval_status'] = $items->approval_status ? $items->approval_status : '';
                $row['Approved_by'] = $items->Approved_by ? Users::findFirstById($items->Approved_by)->login : '';
                $row['Approval_date'] = $items->Approval_date ? date('d-m-Y', $items->Approval_date) : '';
                $row['labelclass'] = $labelclass;


                $row['stuid'] = $items->student_id ? $items->student_id : '';

                $studet = $items->student_id ? StudentInfo::findFirstById($items->student_id) : '';


                $divisiondet = $studet->Division_Class ? DivisionValues::findFirstById($studet->Division_Class) : '';
                $subdivsndets = $studet->Division_Class ? SubDivisionValues::find(' id IN (' . $studet->Subdivision . ')') : '';

                $class_sec = 'Class-' . ($divisiondet->classname ? $divisiondet->classname : '');

                if (count($subdivsndets) > 0) {
                    foreach ($subdivsndets as $subdivsndet) {
                        $class_sec .= $subdivsndet->name ? '-' . $subdivsndet->name : '';
                    }
                }

                $fee_itmdet = $items->fee_item_id ? FeeItems::findFirstById($items->fee_item_id) : '';

                $row['stuname'] = $studet->Student_Name ? $studet->Student_Name : '';
                $row['class_sec'] = $class_sec ? $class_sec : '';
                $row['feetype'] = $fee_itmdet->fee_group_id ? ucwords(FeeGroup::findFirstById($fee_itmdet->fee_group_id)->group_name) : '';
                $row['feename'] = $fee_itmdet->fee_name ? $fee_itmdet->fee_name : '';
                $row['feeamt'] = $fee_itmdet->fee_amount ? $fee_itmdet->fee_amount : '';

                $row['Actions'] = '';
                $appitem = ApprovalItem::find('approval_master_id=' . $items->id);

                $forwardto = $appitem->getLast()->forwaded_to;
                if (($uid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))):
                    $row['Actions'] = '<div class="bs-glyphicons">
                                    <ul class="bs-glyphicons-list">
                                        <li  onclick="approvalSettings.forward(this)">
                                            <span class="glyphicon glyphicon-share-alt " title="Forward"></span>
                                        </li> <li onclick="approvalSettings.approve(this)">
                                            <span class="glyphicon glyphicon-ok" title="Approve" ></span>
                                        </li> <li onclick="approvalSettings.reject(this)">
                                            <span class="glyphicon glyphicon-remove" title="Reject" ></span>
                                        </li>
                                    </ul>                                    
                                </div>';

                endif;

                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    public function bulkApprovalRejectAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $params = array();
        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }
//        print_r($params);
//        exit;
        $queryParams = array();
        $appItem = ApprovalItem::find(array(
                    'conditions' => "forwaded_to =$uid ",
                    'columns' => "MAX(id) as id,approval_master_id",
                    'order' => "id DESC",
                    'group' => "approval_master_id"
        ));
        $selectedMasterArr = array();

        if (count($appItem) > 0):
            foreach ($appItem as $appItm) {
                $selectedMasterArr[] = $appItm->approval_master_id;
            }
        endif;
        if (count($selectedMasterArr) > 0) {
            $queryParams[] = ' appm.id IN (' . implode(',', $selectedMasterArr) . ')';
        }


        $queryParams[] = ' aptyp.id=appm.approval_type_id ';
        $queryParams[] = ' appm.Item_id=ldgrvs.voucher_id ';
        $queryParams[] = ' aptyp.approval_type IN("Journal Voucher","Payment Voucher","Suspense Voucher") ';

        if (isset($params['ids']) && $params['ids'] != '') {
            $queryParams[] = 'appm.id IN (' . implode(',', $params['ids']) . ')';
        } else {
            if ($params['fdate'] != '' && $params['tdate'] != '') {
                $queryParams[] = ' appm.requested_date >= "' . strtotime($params['fdate'] . ' 00:00:00') . '"';
                $queryParams[] = ' appm.requested_date <= "' . strtotime($params['tdate'] . ' 00:00:00') . '"';
            }

            if ($params['appType'] != ''):
                $queryParams[] = ' appm.approval_type_id = "' . $params['appType'] . '"';
            endif;
            if ($params['appStatus'] != ''):
                $queryParams[] = ' appm.approval_status = "' . $params['appStatus'] . '"';
            endif;

            if (isset($params['sSearch']) && $params['sSearch'] != ''):
                $queryParams1[] = 'appm.Item_id Like "' . $params['sSearch'] . '"';
                $queryParams1[] = 'appm.approval_status  Like "%' . $params['sSearch'] . '%"';
                $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
            endif;
        }

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
        $phql = 'SELECT appm.id,appm.approval_type_id,appm.Approved_by,
                 appm.Approval_date,appm.Item_id,appm.approval_status,
                        appm.requested_by,appm.requested_date,ldgrvs.voucher_id,
                        ldgrvs.amount,ldgrvs.creditledger,ldgrvs.debitledger
                        FROM ApprovalMaster appm, LedgerVoucher ldgrvs, ApprovalTypes aptyp WHERE
           ' . $conditionvals;

        $finances = $this->modelsManager->executeQuery($phql);
        $i = 0;
        foreach ($finances as $fvalue) {
            if ($this->request->isPost()) {

                $appitem = ApprovalItem::find('approval_master_id=' . $fvalue->id);
                $forwardto = $appitem->getLast()->forwaded_to;
                if (($uid == $forwardto) && !(in_array($fvalue->approval_status, array('Approved', 'Rejected')))):
                    $appmasid = $fvalue->id;
                    $appmapitemid = $fvalue->Item_id;
                    $approval_status = isset($params['statusApproval']) ? $params['statusApproval'] : '';
                    $comments = isset($params['rejectionComments']) ? $params['rejectionComments'] : '';

                    $appMaster = ApprovalMaster::findFirstById($appmasid);
                    $appItem = new ApprovalItem();

                    $appMaster->approval_status = $approval_status;
                    $appMaster->Approved_by = $uid;
                    $appMaster->Approval_date = time();
                    $appMaster->modified_by = $uid;
                    $appMaster->modified_date = time();

                    $appItem->comments = $comments;
                    $appItem->created_by = $uid;
                    $appItem->created_date = time();
                    $appItem->forwaded_to = $uid;
                    $appItem->ApprovalMaster = $appMaster;
                    $appItem->status = $approval_status;

                    if ($appMaster->save()) {
                        if (!$appItem->save()) {
                            foreach ($appItem->getMessages() as $messages) {
                                $error .= $messages . "\n";
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            $message['swalmessage'] = $error;
                            print_r(json_encode($message));
                            exit;
                        } else {
                            $ledgvoucher = LedgerVoucher::findFirstByVoucherId($appmapitemid);
                            $ledgvoucher->ledgerstatus = $approval_status;

                            if ($ledgvoucher) {

                                if (!$ledgvoucher->save()) {
                                    foreach ($ledgvoucher->getMessages() as $messages) {
                                        $error .= $messages;
                                    }
                                    $message['type'] = 'error';
                                    $message['message'] = $error;
                                    print_r(json_encode($message));
                                    exit;
                                } else {
                                    if ($ledgvoucher->vouchertype === "JOURNAL" && $ledgvoucher->ledgerstatus === "Approved") {
                                        //Add a transaction
                                        $param = array();
                                        $param['transactiontype'] = $ledgvoucher->vouchertype;
                                        $param['debit'] = $ledgvoucher->debitledger;
                                        $param['credit'] = $ledgvoucher->creditledger;
                                        $param['amount'] = $ledgvoucher->amount;
                                        $param['created_by'] = $uid;
                                        $param['created_date'] = time();
                                        $param['details'] = $ledgvoucher->reason;
                                        $param['voucherid'] = $ledgvoucher->voucher_id;
                                        $param['voucher_date'] = $ledgvoucher->date;
                                        $return = ControllerBase::add_transaction($param);

                                        if ($return == "success") {
                                            $ledgvoucher->ledgerstatus = 'Closed';
                                            $ledgvoucher->closedby = $uid;
                                            $ledgvoucher->closed_date = time();
                                            $ledgvoucher->closedcomments = $comments;
                                            if (!$ledgvoucher->save()) {
                                                foreach ($ledgvoucher->getMessages() as $messages) {
                                                    $error .= $messages;
                                                }
                                                $message['type'] = 'error';
                                                $message['message'] = $error;
                                                print_r(json_encode($message));
                                                exit;
                                            }
                                        } else {
                                            $messages['type'] = 'error';
                                            $messages['message'] = "Transaction update failed";
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        foreach ($appMaster->getMessages() as $messages) {
                            $error .= $messages . "\n";
                        }
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        $message['swalmessage'] = $error;
                        print_r(json_encode($message));
                        exit;
                    }
                endif;
            }
            $i++;
        }
        $message['type'] = 'success';
        $message['message'] = '<div class="alert alert-block alert-success fade in">' . $i . ' record(s) ' . $approval_status . ' successfully!</div>';
        $message['swalmessage'] = $error;
        print_r(json_encode($message));
        exit;
    }

}
