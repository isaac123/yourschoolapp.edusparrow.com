<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class AnnouncementController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
        /*$this->assets->addJs('js/appscripts/announcement/announcement.js');
        $this->assets->addCss('css/appstyles/dashboard-page.css');
        $this->assets->addCss('css/appstyles/timeline/dayscroll/fonts/asset/dayscroll.css');
        $this->assets->addCss('css/appstyles/timeline/dayscroll/timer.css');
        $this->assets->addCss('js/lib/bootstrap_date-time/css/bootstrap-datetimepicker.css');
        $this->assets->addCss('css/appstyles/task-page.css');*/
    }

    public function announcementUserListAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $ids = $this->request->getPost('ids');
        if ($ids) {
            $userlist = explode(',', $ids);
            foreach ($userlist as $value) {
                $allIds = explode('_', $value);
                if ($allIds[0] == 'class') {
                    $classroom = ClassroomMaster::findFirstById($allIds[1]);
                    $allnames['id'] = 'class_' . $classroom->id;
                    $allnames['name'] = $classroom->name;
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'student') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'student_' . $studinfo->id;
                    $allnames['name'] = $studinfo->Student_Name;
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'parent') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'parent_' . $studinfo->id;
                    $allnames['name'] = $studinfo->Student_Name . "'s Parent";
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'staff') {
                    $stfinfo = StaffInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'staff_' . $stfinfo->id;
                    $allnames['name'] = $stfinfo->Staff_Name;
                    $all[] = $allnames;
                } if ($allIds[0] == 'node' || $allIds[0] == 'school') {
                    $orgval = OrganizationalStructureValues::findFirstById($allIds[1]);
                    $allnames['id'] = $allIds[0] . '_' . $orgval->id;
                    $allnames['name'] = $orgval->name;
                    $all[] = $allnames;
                }
            }
            $this->view->userids = json_encode($all);
        }
    }

    public function saveAnnouncementAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        $identity = $this->auth->getIdentity();
        if (in_array("Staff", $identity['role_name'])) {
            $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
            foreach ($this->request->getPost() as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'file' && $value) {
                    $params['files'][] = $value;
                } else {
                    $params[$key] = $value;
                }
            }
            $announcement = $params['announcement_id'] ? Announcement::findFirstById($params['announcement_id']) : new Announcement();
            $announcement->date = strtotime($params['announcement_date']);
            $announcement->message = $params['announcement_text'];
            $announcement->comments = $params['announcement_text'];
            $announcement->type = $params['announcement_type'];
            $announcement->selected_userlist = $params['allUserIds'];
            $announcement->created_by = $uid;
            $announcement->created_date = time();
            $announcement->modified_by = $uid;
            $announcement->modified_date = time();
            if ($params['announcement_id']):
                $announcement->status = '';
            endif;
            if ($announcement->save()) {
                if ($params['files']) {
                    foreach ($params['files'] as $file) {
                        $expld = explode('_', $file);
                        if (!is_dir(UPLOAD_DIR . 'announcement/announcement_' . $announcement->id)) {
                            if (!mkdir(UPLOAD_DIR . 'announcement/announcement_' . $announcement->id, 0777, true)) {
                                die('Failed to create folders...');
                            }
                        }
                        if (!is_file(UPLOAD_DIR . 'announcement/announcement_' . $announcement->id . '/' . $expld[0])) {
                            if (is_file(UPLOAD_DIR . 'temp/announcement/' . $expld[0])) {
                                copy(UPLOAD_DIR . 'temp/announcement/' . $expld[0], UPLOAD_DIR . 'announcement/announcement_' . $announcement->id . '/' . $expld[0]);
                            }
                        }
                    }
                    $announcement->files = 'announcement_' . $announcement->id;
                    if (is_dir(UPLOAD_DIR . 'temp/announcement')) {
                        ContentSharingController::deleteDir(UPLOAD_DIR . 'temp/announcement');
                    }
                    $announcement->save();
                }
                $userlist = explode(',', $announcement->selected_userlist);
                foreach ($userlist as $value) {
                    $allIds = explode('_', $value);
                    if ($allIds[0] == 'class') {
                        $classroom = ClassroomMaster::findFirstById($allIds[1]);
                        $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $allIds[1]);
                        $stuquery = 'SELECT stumap.id,stumap.student_info_id,stuinfo.loginid'
                                . ' FROM StudentMapping stumap LEFT JOIN'
                                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                                . ' and stuinfo.id IN (' . $classroomstu->students_id . ') ';
                        $students = $this->modelsManager->executeQuery($stuquery);
                        foreach ($students as $student) {
                            if (!$loginIds[$student->loginid])
                                $loginIds[$student->loginid] = $student->loginid . '_Announcement_class';
                        }
                    }
                    if ($allIds[0] == 'student') {
                        $studinfo = StudentInfo::findFirstById($allIds[1]);
                        $loginIds[$studinfo->loginid] = $studinfo->loginid . '_Intimation_student';
                    }
                    if ($allIds[0] == 'parent') {
                        $studinfo = StudentInfo::findFirstById($allIds[1]);
                        $loginIds[$studinfo->loginid] = $studinfo->loginid . '_Intimation_parent';
                    }
                    if ($allIds[0] == 'staff') {
                        $stfinfo = StaffInfo::findFirstById($allIds[1]);
                        $loginIds[$stfinfo->loginid] = $stfinfo->loginid . '_Intimation_staff';
                    }
                    if ($allIds[0] == 'node') {
                        $stuquery = 'SELECT stuinfo.loginid,stumap.id,stumap.student_info_id,stuinfo.Student_Name,stuinfo.id'
                                . ' FROM StudentMapping stumap INNER JOIN'
                                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" '
                                . ' and FIND_IN_SET(' . $allIds[1] . ',stumap.aggregate_key)';
                        $node_studet = $this->modelsManager->executeQuery($stuquery);

                        if (count($node_studet) > 0) {
                            foreach ($node_studet as $stu) {
                                if (!$loginIds[$stu->loginid])
                                    $loginIds[$stu->loginid] = $stu->loginid . '_Announcement_node';
                            }
                        }
                        $staffs = StaffInfo::find('FIND_IN_SET(' . $allIds[1] . ',aggregate_key)');
                        if (count($staffs) > 0) {
                            foreach ($staffs as $staf) {
                                if (!$loginIds[$staf->loginid])
                                    $loginIds[$staf->loginid] = $staf->loginid . '_Announcement_node';
                            }
                        }
                    }
                    if ($allIds[0] == 'school') {
                        $stuquery = 'SELECT stuinfo.loginid,stumap.id,stumap.student_info_id,stuinfo.Student_Name,stuinfo.id'
                                . ' FROM StudentMapping stumap INNER JOIN'
                                . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" ';
                        $node_studet = $this->modelsManager->executeQuery($stuquery);
                        if (count($node_studet) > 0) {
                            foreach ($node_studet as $stu) {
                                if (!$loginIds[$stu->loginid])
                                    $loginIds[$stu->loginid] = $stu->loginid . '_Circular_school';
                            }
                        }
                        $staffs = StaffInfo::find('status IN("Appointed","Relieving initiated")');
                        if (count($staffs) > 0) {
                            foreach ($staffs as $staf) {
                                if (!$loginIds[$staf->loginid])
                                    $loginIds[$staf->loginid] = $staf->loginid . '_Circular_school';
                            }
                        }
                    }
                }
                foreach ($loginIds as $ann_ids) {
                    $details = array();
                    $details = explode('_', $ann_ids);
                    $announcementto = $params['announcement_id'] ?
                            (AnnouncementTolist::findFirst('announcement_id=' . $params['announcement_id'] . ' and to = "' . $details[0] . '" and to_name="' . $details[1] . '_' . $details[2] . '"') ?
                                    AnnouncementTolist::findFirst('announcement_id=' . $params['announcement_id'] . ' and to = "' . $details[0] . '" and to_name="' . $details[1] . '_' . $details[2] . '"') : new AnnouncementTolist()) : new AnnouncementTolist();
                    $announcementto->announcement_id = $announcement->id;
                    $announcementto->to = $details[0];
                    $announcementto->to_name = $details[1] . '_' . $details[2];
                    $announcementto->created_by = $uid;
                    $announcementto->created_date = time();
                    $announcementto->modified_by = $uid;
                    $announcementto->modified_date = time();
                    if ($params['announcement_id']):
                        $announcementto->status = '';
                    endif;
                    $time = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                    if (!$announcementto->save()) {
                        foreach ($announcementto->getMessages() as $msg) {
                            $error .= $msg . '<br>';
                        }
                        $messages['type'] = 'error';
                        $messages['message'] = $error;
                        print_r(json_encode($messages));
                        exit;
                    }
                }
                $messages['type'] = 'success';
                $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                $messages['message'] = ' Announcement Added Successfully';
                print_r(json_encode($messages));
                exit;
            } else {
                foreach ($announcement->getMessages() as $msg) {
                    $error .= $msg . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        }
    }

    public function assignTastToAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $ids = $this->request->getPost('ids');
        if ($ids) {
            $allIds = explode('_', $ids);
            $stfinfo = StaffInfo::findFirstById($allIds[1]);
            $name = $stfinfo->Staff_Name . " (" . $stfinfo->loginid . ")";

            $this->view->name = $name;
        }
    }

    public function saveTaskAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        foreach ($this->request->getPost() as $key => $value) {
            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'file' && $value) {
                $params['files'][] = $value;
            } else {
                $params[$key] = $value;
            }
        }
        $st = strtotime($params['from_date']);
        $et = strtotime($params['to_date']);
        if ($st > $et) {
            $messages['type'] = 'error';
            $messages['message'] = 'Invalid date range';
            print_r(json_encode($messages));
            exit;
        }
        $taskmas = $params['task_id'] ? TaskMaster::findFirstById($params['task_id']) : new TaskMaster();
        $taskmas->message = $params['task_text'];
        $taskmas->status = $params['task_id'] ? $taskmas->status : "Assigned";
        $taskmas->initiator_id = $uid;
        $taskmas->from_date = strtotime($params['from_date']);
        $taskmas->to_date = strtotime($params['to_date']);
        $taskmas->type = $params['task_type'];
        $taskmas->action = 0;
        $taskmas->created_by = $uid;
        $taskmas->created_on = time();
        $taskmas->modified_by = $uid;
        $taskmas->modified_on = time();
        if ($taskmas->save()) {
            if ($params['files']) {
                foreach ($params['files'] as $file) {
                    $expld = explode('_', $file);
                    if (!is_dir(UPLOAD_DIR . 'task/task_' . $taskmas->id)) {
                        if (!mkdir(UPLOAD_DIR . 'task/task_' . $taskmas->id, 0777, true)) {
                            die('Failed to create folders...');
                        }
                    }
                    if (!is_file(UPLOAD_DIR . 'task/task_' . $taskmas->id . '/' . $expld[0])) {
                        if (is_file(UPLOAD_DIR . 'temp/task/' . $expld[0])) {
                            copy(UPLOAD_DIR . 'temp/task/' . $expld[0], UPLOAD_DIR . 'task/task_' . $taskmas->id . '/' . $expld[0]);
                        }
                    }
                }
                $taskmas->files = 'task_' . $taskmas->id;
                if (is_dir(UPLOAD_DIR . 'temp/task')) {
                    ContentSharingController::deleteDir(UPLOAD_DIR . 'temp/task');
                }
                $taskmas->save();
            }
            if ($params['task_id']) {
                $activities = TaskActivities::find('task_id=' . $params['task_id']);
                foreach ($activities as $act) {
                    $act->created_by = $uid;
                    $act->created_date = time();
                    if (!$act->save()) {
                        foreach ($activities->getMessages() as $msg) {
                            $error .= $msg . '<br>';
                        }
                        $messages['type'] = 'error';
                        $messages['message'] = $error;
                        print_r(json_encode($messages));
                        exit;
                    }
                    $messages['type'] = 'success';
                    $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                    $messages['message'] = ' Task  Assigned Successfully';
                    print_r(json_encode($messages));
                    exit;
                }
            } else {
                $userlist = explode('_', $params['taskUserIds']);
                if ($userlist[0] == 'student') {
                    $studinfo = StudentInfo::findFirstById($userlist[1]);
                    $loginIds = $studinfo->id;
                } else if ($userlist[0] == 'staff') {
                    $stfinfo = StaffInfo::findFirstById($userlist[1]);
                    $loginIds = $stfinfo->id;
                }
                $activities = new TaskActivities();
                $activities->task_id = $taskmas->id;
                $activities->initiator_id = $uid;
                $activities->task_from = $uid;
                $activities->task_to = $loginIds;
                $activities->type = $userlist[0];
                $activities->status = "Assigned";
                $activities->action = 0;
                $activities->created_by = $uid;
                $activities->created_date = time();
                if ($activities->save()) {
                    $messages['type'] = 'success';
                    $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                    $messages['message'] = ' Task  Assigned Successfully';
                    print_r(json_encode($messages));
                    exit;
                } else {
                    foreach ($activities->getMessages() as $msg) {
                        $error .= $msg . '<br>';
                    }
                    $messages['type'] = 'error';
                    $messages['message'] = $error;
                    print_r(json_encode($messages));
                    exit;
                }
            }
        } else {
            foreach ($taskmas->getMessages() as $msg) {
                $error .= $msg . '<br>';
            }
            $messages['type'] = 'error';
            $messages['message'] = $error;
            print_r(json_encode($messages));
            exit;
        }
    }

    public function acceptTaskAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $taskact_id = $this->request->getPost('taskid');
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $taskactiv = TaskActivities::findFirstById($taskact_id);
        $taskmaster = TaskMaster::findFirstById($taskactiv->task_id);
        $taskmaster->status = 'Accepted';
        $taskmaster->modified_by = $uid;
        $taskmaster->modified_on = time();
        if ($taskmaster->save()) {
            $activities = new TaskActivities();
            $activities->task_id = $taskmaster->id;
            $activities->initiator_id = $taskactiv->initiator_id;
            $activities->task_from = $taskactiv->task_from;
            $activities->task_to = $taskactiv->task_to;
            $activities->type = $taskactiv->type;
            $activities->status = "Accepted";
            $activities->action = 0;
            $activities->created_by = $uid;
            $activities->created_date = time();
            if ($activities->save()) {
                $messages['type'] = 'success';
                $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                $messages['message'] = ' Task  Accepted Successfully';
                print_r(json_encode($messages));
                exit;
            } else {
                foreach ($activities->getMessages() as $msg) {
                    $error .= $msg . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        } else {
            foreach ($taskmaster->getMessages() as $msg) {
                $error .= $msg . '<br>';
            }
            $messages['type'] = 'error';
            $messages['message'] = $error;
            print_r(json_encode($messages));
            exit;
        }
    }

    public function rejectTaskAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $taskact_id = $this->request->getPost('taskid');
        $status = $this->request->getPost('status');
        $stat = $status == 'Finish' ? 'Completed' : 'Rejected';
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $taskactiv = TaskActivities::findFirstById($taskact_id);
        $taskmaster = TaskMaster::findFirstById($taskactiv->task_id);
        $taskmaster->status = $stat;
        $taskmaster->modified_by = $uid;
        $taskmaster->modified_on = time();
        if ($taskmaster->save()) {
            $activities = new TaskActivities();
            $activities->task_id = $taskmaster->id;
            $activities->task_from = $taskactiv->task_from;
            $activities->task_to = $taskactiv->task_to;
            $activities->initiator_id = $taskactiv->initiator_id;
            $activities->type = $taskactiv->type;
            $activities->status = $stat;
            $activities->action = 0;
            $activities->created_by = $uid;
            $activities->created_date = time();
            if ($activities->save()) {
                $messages['type'] = 'success';
                $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                $messages['message'] = ' Task ' . $stat . ' Successfully';
                print_r(json_encode($messages));
                exit;
            } else {
                foreach ($activities->getMessages() as $msg) {
                    $error .= $msg . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        } else {
            foreach ($taskmaster->getMessages() as $msg) {
                $error .= $msg . '<br>';
            }
            $messages['type'] = 'error';
            $messages['message'] = $error;
            print_r(json_encode($messages));
            exit;
        }
    }

    public function forwardTaskAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->taskact_id = $taskact_id = $this->request->getPost('taskid');
    }

    public function forwarAssignedTaskAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $taskact_id = $this->request->getPost('taskid');
        $user_id = $this->request->getPost('userid');
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $taskactivity = TaskActivities::findFirstById($taskact_id);
        $taskmaster = TaskMaster::findFirstById($taskactivity->task_id);
        $taskmaster->status = 'Forwarded';
        $taskmaster->modified_by = $uid;
        $taskmaster->modified_on = time();
        if ($taskmaster->save()) {
            $taskactiv = new TaskActivities();
            $userlist = explode('_', $user_id);
            if ($userlist[0] == 'student') {
                $studinfo = StudentInfo::findFirstById($userlist[1]);
                $loginIds = $studinfo->id;
            } else if ($userlist[0] == 'staff') {
                $stfinfo = StaffInfo::findFirstById($userlist[1]);
                $loginIds = $stfinfo->id;
            }
            $taskactiv->task_id = $taskmaster->id;
            $taskactiv->task_from = $uid;
            $taskactiv->task_to = $loginIds;
            $taskactiv->initiator_id = $taskactivity->initiator_id;
            $taskactiv->type = $userlist[0];
            $taskactiv->action = 0;
            $taskactiv->status = 'Forwarded';
            $taskactiv->created_by = $uid;
            $taskactiv->created_date = time();
            if ($taskactiv->save()) {
                $messages['type'] = 'success';
                $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                $messages['message'] = ' Task  Forwarded Successfully';
                print_r(json_encode($messages));
                exit;
            } else {
                foreach ($taskactiv->getMessages() as $msg) {
                    $error .= $msg . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        } else {
            foreach ($taskmaster->getMessages() as $msg) {
                $error .= $msg . '<br>';
            }
            $messages['type'] = 'error';
            $messages['message'] = $error;
            print_r(json_encode($messages));
            exit;
        }
    }

    function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'yr',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hr',
            'i' => 'min',
            's' => 'sec',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public function saveCommentsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $id = $this->request->getPost('taskactid');
        $comments = $this->request->getPost('comments');
        $type = $this->request->getPost('type');
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        if ($type == 'task' || $type == 'viewtask') {
            $taskactiv = TaskActivities::findFirstById($id);
            $activities = new TaskActivities();
            $activities->task_id = $taskactiv->task_id;
            $activities->task_from = $uid;
            $activities->task_to = $taskactiv->task_to;
            $activities->initiator_id = $taskactiv->initiator_id;
            $activities->type = $taskactiv->type;
            $activities->status = 'Comments';
            $activities->action = 0;
            $activities->comments = $comments;
            $activities->created_by = $uid;
            $activities->created_date = time();
            if ($activities->save()) {
                $messages['type'] = 'success';
                $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                $messages['message'] = ' Comments Updated';
                print_r(json_encode($messages));
                exit;
            } else {
                foreach ($activities->getMessages() as $msg) {
                    $error .= $msg . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        } if ($type == 'announcement' || $type == 'announc') {
            $new_ann = new AnnouncementComments();
            $new_ann->master_id = $id;
            $new_ann->comments = $comments;
            $new_ann->comments_by = $identity['name'];
            $new_ann->created_by = $uid;
            $new_ann->created_date = time();
            $new_ann->modified_by = $uid;
            $new_ann->modified_date = time();
            if ($new_ann->save()) {
                $messages['type'] = 'success';
                $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                $messages['message'] = ' Comments Updated';
                print_r(json_encode($messages));
                exit;
            } else {
                foreach ($new_ann->getMessages() as $msg) {
                    $error .= $msg . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        } if ($type == 'events' || $type == 'viewevent') {
            $new_event = new EventsComments();
            $new_event->master_id = $id;
            $new_event->comments = $comments;
            $new_event->comments_by = $identity['name'];
            $new_event->created_by = $uid;
            $new_event->created_date = time();
            $new_event->modified_by = $uid;
            $new_event->modified_date = time();
            if ($new_event->save()) {
                $messages['type'] = 'success';
                $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                $messages['message'] = ' Comments Updated';
                print_r(json_encode($messages));
                exit;
            } else {
                foreach ($new_event->getMessages() as $msg) {
                    $error .= $msg . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        }
    }

    public function anouncementAcknwldgAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $announcementid = $this->request->getPost('announcement_id');
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $announcementto = AnnouncementTolist::findFirst('announcement_id=' . $announcementid . ' and to="' . $identity['name'] . '"');
        $announcementto->status = 'Acknowledge';
        $announcementto->modified_by = $uid;
        $announcementto->modified_date = time();
        if ($announcementto->save()) {
            $announc_item = AnnouncementTolist::find('announcement_id=' . $announcementid);
            foreach ($announc_item as $item) {
                $allloginIds[] = $item->to;
                if ($item->status == 'Acknowledge'):
                    $ackldgid[] = $item->to;
                endif;
            }
            $valu1 = array_unique($allloginIds);
            $valu2 = array_unique($ackldgid);
            $pending = array_diff($valu1, $valu2);
            $announcement = Announcement::findFirstById($announcementid);
            $announcement->modified_by = $uid;
            $announcement->modified_date = time();
            if (count($pending) == 0) {
                $announcement->status = 'Acknowledge';
            }
            if ($announcement->save()) {
                $messages['type'] = 'success';
                $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                $messages['message'] = ' Circular Acknowledged Successfully';
                print_r(json_encode($messages));
                exit;
            } else {
                foreach ($announcement->getMessages() as $msg) {
                    $error .= $msg . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        } else {
            foreach ($announcementto->getMessages() as $msg) {
                $error .= $msg . '<br>';
            }
            $messages['type'] = 'error';
            $messages['message'] = $error;
            print_r(json_encode($messages));
            exit;
        }
    }

    public function viewAnnouncementAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->announcementid = $announcementid = $this->request->getPost('announcement_id');
        $this->view->edit = $edit = $this->request->getPost('edit');
        $this->view->announcement = $announcement = Announcement::findFirstById($announcementid);
        $this->view->annclist = $annclist = AnnouncementTolist::find('announcement_id=' . $announcementid . ' ORDER BY modified_date DESC');
        $this->view->ack_list = $ack_list = AnnouncementTolist::find('announcement_id=' . $announcementid . ' and  status = "Acknowledge" ORDER BY modified_date DESC');
        $this->view->annun_comments = $annun_comments = AnnouncementComments::find('master_id=' . $announcementid . ' ORDER BY modified_date desc');
        $this->view->identity = $identity = $this->auth->getIdentity();
        $this->view->uid = $uid = StaffInfo::findFirstByLoginid($identity['name']);
        $this->view->stf_name = $stf_name = StaffInfo::findFirstById($announcement->created_by)->Staff_Name;
    }

    public function viewTasksAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $activityid = $this->request->getPost('taskactivity_id');
        $this->view->edit = $edit = $this->request->getPost('edit');
        $activity = TaskActivities::findFirstById($activityid);
        $this->view->task = $task = TaskMaster::findFirstById($activity->task_id);
    }

    public function downloadTaskFileAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $taskid = $this->request->getPost('task_id');
            $task = TaskMaster::findFirstById($taskid);
            $file = UPLOAD_DIR . $task->files;
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . $task->files . '";');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                exit;
            }
        }
    }

    public function deleteTaskAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $taskact_id = $this->request->getPost('taskid');
        $type = $this->request->getPost('type');
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $taskactiv = TaskActivities::findFirstById($taskact_id);
        $taskmaster = TaskMaster::findFirstById($taskactiv->task_id);
        if ($type == 'gaveout') {
            $taskmaster->action = 1;
            if ($taskmaster->save()) {
                $messages['type'] = 'success';
                $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                $messages['message'] = ' Task  removed Successfully';
                print_r(json_encode($messages));
                exit;
            }
        } else {
            $taskactiv->action = 1;
            $taskactiv->status = 'Declined';
            if ($taskactiv->save()) {
                $messages['type'] = 'success';
                $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                $messages['message'] = ' Task  removed Successfully';
                print_r(json_encode($messages));
                exit;
            } else {
                foreach ($activities->getMessages() as $msg) {
                    $error .= $msg . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        }
    }

    public function annuncFileUploadAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $storeFolder = UPLOAD_DIR . 'temp/announcement';
        $ds = DIRECTORY_SEPARATOR;
        $files = scandir($storeFolder);
        $result = array();
        if (false !== $files) {
            foreach ($files as $file) {
                if ('.' != $file && '..' != $file) {
                    $obj['name'] = $file;
                    $obj['size'] = AnnouncementController::formatSizeUnits(filesize($storeFolder . $ds . $file));
                    $result[] = $obj;
                }
            }
        }
        $this->view->files = $result;
    }

    function formatSizeUnits($bytes) {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function dashboardfileUploadAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $header = $this->request->getPost('type');
        if ($this->request->hasFiles() == true && count($this->request->getUploadedFiles()) < 10) {
            $arr = array();
            foreach ($this->request->getUploadedFiles() as $file) {
                $ext = explode('.', $file->getName());
                $size = explode('.', $file->getSize());
                $type = explode('.', $file->getType());
                $filename = $ext[0] . '.' . $ext[1];
                $newdir = $header;
                if (!is_dir(UPLOAD_DIR . 'temp/' . $newdir)) {
                    if (!mkdir(UPLOAD_DIR . 'temp/' . $newdir, 0777, true)) {
                        die('Failed to create folders...');
                    }
                }
                $file->moveTo(UPLOAD_DIR . 'temp/' . $newdir . '/' . $filename);
            }
        }
    }

    public function dashboardFileDownloadAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $params = $this->request->getPost('filename');
            $xpld = explode('_', $params);
            $file = UPLOAD_DIR . $xpld[0] . '/' . $xpld[0] . '_' . $xpld[1] . '/' . $xpld[2];
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . $xpld[2] . '";');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                exit;
            }
        }
    }

    public function taskFileUploadAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $storeFolder = UPLOAD_DIR . 'temp/task';
        $ds = DIRECTORY_SEPARATOR;
        $files = scandir($storeFolder);
        $result = array();
        if (false !== $files) {
            foreach ($files as $file) {
                if ('.' != $file && '..' != $file) {
                    $obj['name'] = $file;
                    $obj['size'] = AnnouncementController::formatSizeUnits(filesize($storeFolder . $ds . $file));
                    $result[] = $obj;
                }
            }
        }
        $this->view->files = $result;
    }

    public function removeUploadedFileAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $name = $this->request->getPost('name');
        $type = $this->request->getPost('type');
        $explde = explode('_', $type);
        if ($name) {
            if (is_file(UPLOAD_DIR . $explde[0] . '/' . $type . '/' . $name)) {
                unlink(UPLOAD_DIR . $explde[0] . '/' . $type . '/' . $name);
            } else {
                unlink(UPLOAD_DIR . 'temp/' . $explde[0] . '/' . $name);
            }
            $del = "success";
            print_r(json_encode($del));
            exit;
        } else {
            $del = "error";
            print_r(json_encode($del));
            exit;
        }
    }

    public function editAnnouncementsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->announcementid = $announcementid = $this->request->getPost('announcement_id');
        $this->view->announcement = $announcement = Announcement::findFirstById($announcementid);
        $storeFolder = UPLOAD_DIR . 'announcement';
        $ds = DIRECTORY_SEPARATOR;
        $result = array();
        $uploadedfile = $announcement->files ? explode(',', $announcement->files) : '';
        if ($uploadedfile) {
            $storeFolder = UPLOAD_DIR . 'announcement/announcement_' . $announcementid;
            $ds = DIRECTORY_SEPARATOR;
            $files = scandir($storeFolder);
            $result = array();
            if (false !== $files) {
                foreach ($files as $file) {
                    if ('.' != $file && '..' != $file) {
                        $obj['name'] = $file;
                        $obj['size'] = AnnouncementController::formatSizeUnits(filesize($storeFolder . $ds . $file));
                        $result[] = $obj;
                    }
                }
            }
            $this->view->files = $result;
        }
        $all = array();
        if ($announcement->selected_userlist) {
            $userlist = explode(',', $announcement->selected_userlist);
            foreach ($userlist as $value) {
                $allIds = explode('_', $value);
                if ($allIds[0] == 'class') {
                    $classroom = ClassroomMaster::findFirstById($allIds[1]);
                    $allnames['id'] = 'class_' . $classroom->id;
                    $allnames['name'] = $classroom->name;
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'student') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'student_' . $studinfo->id;
                    $allnames['name'] = $studinfo->Student_Name;
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'parent') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'parent_' . $studinfo->id;
                    $allnames['name'] = $studinfo->Student_Name . "'s Parent";
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'staff') {
                    $stfinfo = StaffInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'staff_' . $stfinfo->id;
                    $allnames['name'] = $stfinfo->Staff_Name;
                    $all[] = $allnames;
                } if ($allIds[0] == 'node' || $allIds[0] == 'school') {
                    $orgval = OrganizationalStructureValues::findFirstById($allIds[1]);
                    $allnames['id'] = $allIds[0] . '_' . $orgval->id;
                    $allnames['name'] = $orgval->name;
                    $all[] = $allnames;
                }
            }
            $this->view->userids = json_encode($all);
        }
    }

    public function editTaskAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->activityid = $activityid = $this->request->getPost('taskactivity_id');
        $this->view->taskactivity = $taskactivity = TaskActivities::findFirstById($activityid);
        $this->view->task = $task = TaskMaster::findFirstById($taskactivity->task_id);
        $uploadedfile = $task->files ? explode(',', $task->files) : '';
        $storeFolder = UPLOAD_DIR . 'task';
        $ds = DIRECTORY_SEPARATOR;
        $result = array();
        if ($uploadedfile) {
            $storeFolder = UPLOAD_DIR . 'task/task_' . $task->id;
            $ds = DIRECTORY_SEPARATOR;
            $files = scandir($storeFolder);
            $result = array();
            if (false !== $files) {
                foreach ($files as $file) {
                    if ('.' != $file && '..' != $file) {
                        $obj['name'] = $file;
                        $obj['size'] = AnnouncementController::formatSizeUnits(filesize($storeFolder . $ds . $file));
                        $result[] = $obj;
                    }
                }
            }
            $this->view->files = $result;
        }
    }

}
