<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class MobappController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('public');
    }

    public function indexAction() {

        $this->tag->prependTitle("App Download | ");
        $this->view->setTemplateAfter('private');
    }

    public function mainExamAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $activityid = $this->request->get('activityid');
        if ($activityid):
            $exammrks = MainexamMarks::findFirst('mainexam_marks_id=' . $activityid);
            $examid = $exammrks->mainexam_id;
        endif;
        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $aggregate_key = StudentMapping::findFirst('student_info_id =' . $stud_info->id)->aggregate_key;
        $params['aggregateids'] = explode(',', $aggregate_key);
        $subjids = ControllerBase::getGrpSubjMasPossiblities($params['aggregateids']);
        $subjectsid = GroupSubjectsTeachers::find('id IN (' . implode(',', $subjids) . ')');
        if (count($subjectsid) > 0) {
            foreach ($subjectsid as $svalue) {
                $subj_Ids[] = $svalue->subject_id;
            }
            $subj_Ids = array_unique($subj_Ids);
        }
        $get_Query_For_Exmmak = ControllerBase::buildExamQuery($aggregate_key);
        $mainexamsdets = Mainexam::find(implode(' or ', $get_Query_For_Exmmak));
        $exam = array();
        if (count($mainexamsdets) > 0) {
            foreach ($mainexamsdets as $exm) {
                $set = $array = $col = $item = array();
                $array['name'] = $exm->exam_name;
                $array['id'] = $exm->id;
                $expandid[] = $exm->id;
                $col['id'] = '1';
                $col['name'] = 'Subject';
                $set['columns'][] = $col;
                $col['id'] = '2';
                $col['name'] = 'Mark';
                $set['columns'][] = $col;
                $col['id'] = '3';
                $col['name'] = 'Outof';
                $set['columns'][] = $col;
                if (count($subj_Ids) > 0) {
                    foreach ($subj_Ids as $subids) {
                        $subjagg = array();
                        $subjagg = ControllerBase::getAllSubjectAndSubModules(array($subids));
                        if (count($subjagg) > 0) {
                            foreach ($subjagg as $value) {
                                $mainexMark = MainexamMarks::findFirst('mainexam_id=' . $exm->id
                                                . ' and grp_subject_teacher_id IN ( ' . implode(',', $subjids) . ')'
                                                . ' and student_id = ' . $stud_info->id . ' and subject_id = ' . $value);
                                $item[1]['text'] = OrganizationalStructureValues::findFirst('id = ' . $value)->name;
                                $subj_arr = $this->_getMandNodesForExam(OrganizationalStructureValues::findFirst('id = ' . $value));
                                if (count($subj_arr) > 0) {
                                    $reverse = array_reverse($subj_arr);
                                    $item[1]['subtext'] = implode('>', $reverse);
                                } else {
                                    $item[1]['subtext'] = "";
                                }
                                if ($mainexMark) {
                                    $item[2]['text'] = (($mainexMark->inherited_marks) ? $mainexMark->inherited_marks : 0 ) + (($mainexMark->marks) ? $mainexMark->marks : 0);
                                    $item[3]['text'] = (($mainexMark->inherited_outof ) ? $mainexMark->inherited_outof : 0 ) + (($mainexMark->outof) ? $mainexMark->outof : 0);
                                } else {
                                    $item[2]['text'] = '';
                                    $item[3]['text'] = '';
                                }
                                $set['items'][] = $item;
                            }
                        }
                    }
                }
                $array['set'][] = $set;
                $exam[] = $array;
            }
            $mainexams['list'] = $exam;
            $mainexams['id'] = $examid ? $examid : $expandid[0];
        }
        print_r(json_encode($mainexams));
        exit;
    }

    public function _getMandNodesForExam($acdyrMas, $nodes = array()) {
        $fields = OrganizationalStructureValues::findFirstById($acdyrMas->parent_id);
        $iscycle = OrganizationalStructureMaster::findFirstById($fields->org_master_id);
        if ($iscycle->module == "Subject"):
            $nodes[] = $fields->name;
        endif;
        if (isset($fields->parent_id)):
            $nodes = MobappController::_getMandNodesForExam($fields, $nodes);
        endif;
        return $nodes;
    }

    public function classTestAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $activityid = $this->request->get('activityid');
        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $aggregate_key = StudentMapping::findFirst('student_info_id =' . $stud_info->id)->aggregate_key;
        $aggregateids = explode(',', $aggregate_key);
        $subjids = ControllerBase::getGrpSubjMasPossiblities($aggregateids);
        $subjectsid = GroupSubjectsTeachers::find('id IN (' . implode(',', $subjids) . ')');
        if (count($subjectsid) > 0) {
            foreach ($subjectsid as $svalue) {
                $subj_Ids[] = $svalue->subject_id;
            }
            $test = $expandid = array();
            $subj_Ids = array_unique($subj_Ids);
            foreach ($subj_Ids as $value) {
                $set = $array = $col = $item = array();
                $queryParams = $subjagg = array();
                $subjagg = ControllerBase::getAllSubjectAndSubModules(array($value));
                if ($subjagg && count($subjagg) > 0) {
                    $queryParams[] = 'subjct_modules IN(' . implode(',', $subjagg) . ')';
                }
                if (count($subjids) > 0) {
                    $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
                }
                $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
                $classtests = ClassTest::find($conditionvals);
                $array = array();
                if (count($classtests) > 0) {
                    $array['id'] = $value;
                    $array['name'] = OrganizationalStructureValues::findFirstById($value)->name;
                    $col['id'] = '1';
                    $col['name'] = 'Test';
                    $set['columns'][] = $col;
                    $col['id'] = '2';
                    $col['name'] = 'Mark';
                    $set['columns'][] = $col;
                    $col['id'] = '3';
                    $col['name'] = 'Outof';
                    $set['columns'][] = $col;
                    foreach ($classtests as $classtest) {
                        $stumark = ClassTestMarks::findFirst('class_test_id = ' . $classtest->class_test_id . ' and student_id =' . $stud_info->id);
                        $expandid[] = $stumark->classtest_marks_id;
                        $item[1]['text'] = $classtest->class_test_name;
                        $item[2]['text'] = ($stumark ? $stumark->marks : '');
                        $item[3]['text'] = $stumark->outof ? $stumark->outof : '';
                        $set['items'][] = $item;
                    }
                    $array['set'][] = $set;
                    $test[] = $array;
                }
            }
        }
        $expandedids = $activityid ? $activityid : $expandid[0];
        if ($expandedids) {
            $classtestmarks = ClassTestMarks::findFirst('classtest_marks_id=' . $expandedids);
            $tests = ClassTest::findFirst('class_test_id=' . $classtestmarks->class_test_id);
            $field = $this->_getSubMandNodesId(OrganizationalStructureValues::findFirstById($tests->subjct_modules));
            $tstsubid = (count($field) > 0) ? end($field) : $tests->subjct_modules;
        }
        $overall['id'] = $tstsubid ? $tstsubid : '';
        $overall['list'] = $test;
        print_r(json_encode($overall));
        exit;
    }

    public function assignmentAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $activityid = $this->request->get('activityid');
        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $aggregate_key = StudentMapping::findFirst('student_info_id =' . $stud_info->id)->aggregate_key;
        $params['aggregateids'] = explode(',', $aggregate_key);
        $subjpids = ControllerBase::getAlSubjChildNodes($params['aggregateids']);
        $subjids = ControllerBase::getGrpSubjMasPossiblities($params['aggregateids']);
        $subjectsid = GroupSubjectsTeachers::find('id IN (' . implode(',', $subjids) . ')');
        if (count($subjectsid) > 0) {
            foreach ($subjectsid as $svalue) {
                $subj_Ids[] = $svalue->subject_id;
            }
            $subj_Ids = array_unique($subj_Ids);
            foreach ($subj_Ids as $value) {
                $queryParams = $subjagg = array();
                $subjagg = ControllerBase::getAllSubjectAndSubModules(array($value));
                if ($subjagg && count($subjagg) > 0) {
                    $queryParams[] = 'subjct_modules IN(' . implode(',', $subjagg) . ')';
                }
                if (count($subjids) > 0) {
                    $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
                }
                $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) . ' ORDER BY created_date DESC' : '';
                $assignments = AssignmentsMaster::find($conditionvals);
                $array = array();
                $array['id'] = $value;
                $array['name'] = OrganizationalStructureValues::findFirstById($value)->name;
                if (count($assignments) > 0) {
                    foreach ($assignments as $assignment) {
                        $expandid[] = $assignment->id;
                        $stud_assignment = StudentAssignments::findFirst('student_id=' . $stud_info->id . ' and assignment_id=' . $assignment->id);
                        $assmarks = AssignmentMarks::findFirst('assignment_id = ' . $assignment->id . ' and student_id = ' . $stud_info->id);
                        $set = $col = array();
                        $col['id'] = $assignment->id;
                        $col['topic'] = $assignment->topic;
                        $col['topic'] .= $assmarks ? ' - Marks : ' . $assmarks->marks . '/' . $assmarks->outof : '';
                        $col['submissiondate'] = date('d F Y', $assignment->submission_date);
                        $col['description'] = $assignment->desc;
                        $col['submit'] = $stud_assignment ? 'Submitted' : 'Submit';
                        $col['is_submitted'] = $stud_assignment ? '1' : '0';
                        $col['color'] = $col['submit'] == 'Submitted' ? 'royal cursor' : 'balanced';
                        $set['columns'][] = $col;
                        $array['set'][] = $set;
                    }
                    $exm[] = $array;
                }
            }
        }
        $expandedids = $activityid ? $activityid : $expandid[0];
        if ($expandedids) {
            $markid = explode('_', $expandedids);
            if (count($markid) > 1) {
                $assgn_marks = AssignmentMarks::findFirstById($markid[1]);
                $assgn_subid = AssignmentsMaster::findFirstById($assgn_marks->assignment_id);
            } else {
                $assgn_subid = AssignmentsMaster::findFirstById($markid[0]);
            }
            $fields = $this->_getSubMandNodesId(OrganizationalStructureValues::findFirstById($assgn_subid->subjct_modules));
            $asssubid = count($fields) > 0 ? end($fields) : $assgn_subid->subjct_modules;
        }
        $ass['id'] = $asssubid ? $asssubid : '';
        $ass['list'] = $exm;
        print_r(json_encode($ass));
        exit;
    }

    public function homeWorkAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $date = $this->request->get('hdate');
        $actid = $this->request->get('actid');
        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $aggregate = StudentMapping::findFirst('student_info_id =' . $stud_info->id)->aggregate_key;
        $aggregate_key = explode(',', $aggregate);
        $subjids = ControllerBase::getGrpSubjMasPossiblities($aggregate_key);
        $subjectsid = GroupSubjectsTeachers::find('id IN (' . implode(',', $subjids) . ')');
        if (count($subjectsid) > 0) {
            foreach ($subjectsid as $svalue) {
                $subj_Ids[] = $svalue->subject_id;
            }
        }
        $queryParams = $set = array();
        if ($date) {
            $queryParams[] = 'hmwrkdate >= "' . strtotime($date . ' 00:00:00')
                    . '" and hmwrkdate <= "' . strtotime($date . ' 23:59:59') . '"';
        } if ($actid) {
            $homeworkbyactid = HomeWorkTable::findFirstById($actid)->hmwrkdate;
            $queryParams[] = 'hmwrkdate = "' . strtotime(date('d-m-Y', $homeworkbyactid) . ' 00:00:00')
                    . '" and hmwrkdate <= "' . strtotime(date('d-m-Y', $homeworkbyactid) . ' 23:59:59') . '"';
        }
        if ($date == '' && $actid == ''):
            $currentdate = date('d-m-Y', time());
            $queryParams[] = 'hmwrkdate >= "' . strtotime($currentdate . ' 00:00:00')
                    . '" and hmwrkdate <= "' . strtotime($currentdate . ' 23:59:59') . '"';
        endif;
        if (count($subj_Ids) > 0) {
            $queryParams[] = 'subjct_modules IN(' . implode(',', $subj_Ids) . ')';
        }
        if (count($subjids) > 0) {
            $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
        }
        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
        $homework = HomeWorkTable::find($conditionvals);
        if (count($homework) > 0) {
            $col['id'] = '1';
            $col['name'] = 'Subject';
            $set['columns'][] = $col;
            $col['id'] = '2';
            $col['name'] = 'Homework';
            $set['columns'][] = $col;
            foreach ($homework as $homeworkval) {
                $subj_arr = $this->_getMandNodesForExam(OrganizationalStructureValues::findFirst('id = ' . $homeworkval->subjct_modules));
                if (count($subj_arr) > 0) {
                    $item[1]['text'] = end($subj_arr);
                } else {
                    $item[1]['text'] = OrganizationalStructureValues::findFirst('id = ' . $homeworkval->subjct_modules)->name;
                }
                $item[1]['classcol']=  'col-25';
                $item[2]['text'] = $homeworkval->title;
                $item[2]['subtext'] = $homeworkval->homework;
                $item[2]['classcol']=  'col-75';
                $set['items'][] = $item;
                $set['showdt'] = $homeworkval->hmwrkdate * 1000;
            }
        }
        print_r(json_encode($set));
        exit;
    }

    public function RatingCategoryAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $aggregate = StudentMapping::findFirst('student_info_id =' . $stud_info->id)->aggregate_key;
        $res = ControllerBase::buildExamQuery($aggregate);
        $rates = RatingDivision::find(implode(' or ', $res));
        $ratings = array();
        $i = 0;
        if (count($rates) > 0) {
            foreach ($rates as $rating) {
                $rating_Ids['id'] = $rating->id;
                $rating_Ids['text'] = $rating->rating_name;
                $rating_Ids['checked'] = false;
                $rating_Ids['icon'] = null;
                $ratings[] = $rating_Ids;
            }
        }
        print_r(json_encode($ratings));
        exit;
    }

    public function getSubjectAction() {
        $loginid = $this->request->get('loginid');
        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $aggregate = StudentMapping::findFirst('student_info_id =' . $stud_info->id)->aggregate_key;
        $aggregateids = explode(',', $aggregate);
        $subjpids = ControllerBase::getAlSubjChildNodes($aggregateids);
        $subjids = ControllerBase::getGrpSubjMasPossiblities($aggregateids);
        $subjectsid = GroupSubjectsTeachers::find('id IN (' . implode(',', $subjids) . ')');
        if (count($subjectsid) > 0) {
            foreach ($subjectsid as $svalue) {
                $subj_Ids[] = $svalue->subject_id;
            }
            $subject = array();
            foreach ($subj_Ids as $sub) {
                $name = OrganizationalStructureValues::findFirstById($sub)->name;
                $subjs['id'] = $sub;
                $subjs['text'] = $name;
                $subjs['checked'] = false;
                $subjs['icon'] = null;
                $subject[] = $subjs;
            }
        }
        print_r(json_encode($subject));
        exit;
    }

    public function subjectRatingAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $category = $this->request->get('division_id');
        $subject = $this->request->get('subject_id');
        $activityid = $this->request->get('activityid');
        if ($activityid):
            $subrating = StudentSubteacherRating::findFirst('rating_id=' . $activityid);
            $subids = $this->_getSubMandNodesId(OrganizationalStructureValues::findFirstById($subrating->subjct_modules));
            $subject = count($subids) > 0 ? end($subids) : $subrating->subjct_modules;
            $category = $subrating->rating_division_id;
        endif;
        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $set = $array = $col = $item = $subjagg = array();
        $subjagg = ControllerBase::getAllSubjectAndSubModules(array($subject));
        $ratingCategorys = RatingCategoryMaster::find();
        if (count($ratingCategorys) > 0) {
            $col['id'] = '1';
            $col['name'] = 'Category';
            $set['columns'][] = $col;
            $col['id'] = '2';
            $col['name'] = 'Points';
            $set['columns'][] = $col;
            $col['id'] = '3';
            $col['name'] = 'Total';
            $set['columns'][] = $col;
            foreach ($ratingCategorys as $ratingCategory) {
                $ratingPoints = 0;
                $ratingValues = RatingCategoryValues::find('rating_category = ' . $ratingCategory->id);
                foreach ($ratingValues as $rvalue) {
                    $studentRating = StudentSubteacherRating::find(
                                    'rating_division_id =' . $category
                                    . ' and rating_category =' . $ratingCategory->id
                                    . ' and rating_value = ' . $rvalue->id
                                    . ' and student_id = ' . $stud_info->id
                                    . ' and subjct_modules IN (' . implode(',', $subjagg) . ')'
                    );
                    if (count($studentRating) > 0) {
                        foreach ($studentRating as $submodrat) {
                            $ratingPoints += ($rvalue->rating_level_value / 100) * $ratingCategory->category_weightage;
                        }
                    }
                }
                $item[1]['text'] = $ratingCategory->category_name;
                $item[2]['text'] = $ratingPoints ? $ratingPoints : '';
                $item[3]['text'] = $ratingCategory->category_weightage ? $ratingCategory->category_weightage : '';
                $set['items'][] = $item;
            }
        }
        $arr['cls'] = $set;
        $arr['ratname'] = RatingDivision::findFirst('id=' . $category)->rating_name;
        $arr['subname'] = OrganizationalStructureValues::findFirstById($subject)->name;
        print_r(json_encode($arr));
        exit;
    }

    public function ClassRatingAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $division = $this->request->get('division_id');
        $activityid = $this->request->get('activityid');
        if ($activityid):
            $division = StudentClassteacherRating::findFirst('rating_id=' . $activityid)->rating_division_id;
        endif;
        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $ratingCategorys = RatingCategoryMaster::find();
        $set = $col = $item = array();
        if (count($ratingCategorys) > 0) {
            $col['id'] = '1';
            $col['name'] = 'Category';
            $set['columns'][] = $col;
            $col['id'] = '2';
            $col['name'] = 'Points';
            $set['columns'][] = $col;
            $col['id'] = '3';
            $col['name'] = 'Total';
            $set['columns'][] = $col;
            foreach ($ratingCategorys as $ratingCategory) {
                $item = array();
                $ratingPoints = 0;
                $ratingValues = RatingCategoryValues::find('rating_category = ' . $ratingCategory->id);
                foreach ($ratingValues as $rvalue) {
                    $studentRating = StudentClassteacherRating::findFirst(
                                    'rating_division_id =' . $division
                                    . ' and rating_category =' . $ratingCategory->id
                                    . ' and rating_value = ' . $rvalue->id
                                    . ' and student_id = ' . $stud_info->id
                    );
                    if ($studentRating) {
                        $ratingPoints += ($rvalue->rating_level_value / 100) * $ratingCategory->category_weightage;
                    }
                }
                $item[1]['text'] = $ratingCategory->category_name;
                $item[2]['text'] = $ratingPoints ? $ratingPoints : '';
                $item[3]['text'] = $ratingCategory->category_weightage ? $ratingCategory->category_weightage : '';
                $set['items'][] = $item;
            }
        }
        $arr['cls'] = $set;
        $arr['name'] = RatingDivision::findFirst('id=' . $division)->rating_name;
        print_r(json_encode($arr));
        exit;
    }

    public function getStudentAttendanceForMonthAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $param['user_id'] = $loginid;
        $param['user_type'] = 'student';
        $obj = new Cassandra();
        $attendance = array();
        $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);
        if ($res) {
            $buildquery = "select * from day_attendance where"
                    . " user_id = '" . $loginid . "'";
            $stuinfo = StudentInfo::findFirstByLoginid($loginid);
            if ($result = $obj->query($buildquery)) {
                if (count($result) > 0) {
                    for ($i = 0; $i < count($result); $i++):
                        $arr = array();
                        $abbrevation = AttendanceSelectbox::findFirstById($result[$i]['value']);
                        $pos = strpos($result[$i]['period'], '_');
                        if ($pos === false) {
                            $attPeriod = PeriodMaster::findFirstById($result[$i]['period']);
                            $periodname = $attPeriod->period;
                            $starttime = $attPeriod->start_time;
                            $endtime = $attPeriod->end_time;
                        } else {
                            $userid = "Student_" . $stuinfo->id;
                            $v = explode('_', $result[$i]['period']);
                            $eveid = $v[0] . '.' . $v[1] . ' ' . $v[2];
                            $userbuildquery = "select * from user_cal where user_id = '" . $userid . "' and event_id = '" . $eveid . "'";
                            if ($sresult = $obj->query($userbuildquery)) {
                                $evedata = json_decode($sresult[0]['event_data']);
                                $periodname = $evedata->title;
                                $starttime = $sresult[0]['start'];
                                $endtime = $sresult[0]['end'];
                            }
                        }
                        $arr['date'] = date('Y-m-d', $result[$i]['date']);
                        $arr['fromtime'] = date('H:i', $starttime);
                        $arr['totime'] = date('H:i', $endtime);
                        $arr['title'] = $periodname;
                        $arr['value'] = $abbrevation->attendancename;
                        $attendance[] = $arr;

                    endfor;
                }
            }
        }
        print_r(json_encode($attendance));
        exit;
    }

    public function overallProgressAction() {
        $device_token = $this->request->get('device_token');
        $device_type = $this->request->get('device_type');
        $uuid = $this->request->get('uuid');
        $user_type = $this->request->get('user_type');
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        // if ($user_type != "common") {
        $totalarray = $array = array();
        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $aggregate_key = StudentMapping::findFirst('student_info_id =' . $stud_info->id)->aggregate_key;
        $res = ControllerBase::buildExamQuery($aggregate_key);
        $mainexamdet = Mainexam ::find(implode(' or ', $res));
        $subj_Ids = array();
        $subjpids = ControllerBase::getAlSubjChildNodes(explode(',', $aggregate_key));
        $subjids = ControllerBase::getGrpSubjMasPossiblities(explode(',', $aggregate_key));
        $subjectsid = GroupSubjectsTeachers::find('id IN (' . implode(',', $subjids) . ')');
        if (count($subjectsid) > 0) {
            foreach ($subjectsid as $svalue) {
                $subj_Ids[] = $svalue->subject_id;
            }
        }
        $exm_unread = $ass_unread = $rating_unread = $home_unread = $test_unread = $att_unread = $circular_unread = $event_unread = 0;
        $notify = array();
        $notify['aggregate_key'] = SUBDOMAIN;
        $notify['targetor_id'] = $loginid;
        $notify['device_token'] = $device_token;
        $notify['device_type'] = $device_type;
        $notify['uuid'] = $uuid;
        $notify['category'] = 'mine';
        $notification_data = json_encode($notify);
        $responseParam = IndexController::curlIt(COMMAPI . 'GetNotificationAggregate.php', $notification_data);
        $content = json_decode($responseParam);
        if (count($content) > 0) {
            foreach ($content as $unread) {
                if ($unread->title == 'exams') {
                    $exm_unread = $unread->count;
                }if ($unread->title == 'assignment') {
                    $ass_unread = $unread->count;
                }if ($unread->title == 'rating') {
                    $rating_unread = $unread->count;
                }if ($unread->title == 'homework') {
                    $home_unread = $unread->count;
                }if ($unread->title == 'tests') {
                    $test_unread = $unread->count;
                }if ($unread->title == 'attendance') {
                    $att_unread = $unread->count;
                }if ($unread->title == 'intimation') {
                    $circular_unread = $unread->count;
                }if ($unread->title == 'events') {
                    $events_unread = $unread->count;
                }
            }
        }
        $subj_Ids = array_unique($subj_Ids);
        $overallexmcut = $studentexamcnt = 0;
        if (count($mainexamdet) > 0) {
            foreach ($mainexamdet as $mainex) {
                $overalclsout = $studentpercentforchart = 0;
                if (count($subj_Ids) > 0) {
                    foreach ($subj_Ids as $sub) {
                        $subjagg = $suject = array();
                        $subjagg = ControllerBase::getAllSubjectAndSubModules(array($sub));
                        $suject = $this->find_childtreevaljson($sub);
                        $cnt = count($suject);
                        $overalstuout = $overalstutotalmarks = 0;
                        $mainexamMarks = MainexamMarks::find('mainexam_id = ' . $mainex->id . ''
                                        . ' and grp_subject_teacher_id IN ( ' . implode(',', $subjids) . ') and student_id = ' . $stud_info->id . ' and subject_id IN ( ' . implode(',', $subjagg) . ')');
                        if (count($mainexamMarks) > 0) {
                            foreach ($mainexamMarks as $mainexMark) {
                                $exams[$mainex->id] = $mainexMark->mainexam_marks_id;
                                $obtainedmark = (($mainexMark->inherited_marks) ? $mainexMark->inherited_marks : 0 ) + (($mainexMark->marks) ? $mainexMark->marks : 0);
                                $obtainedoutOf = (($mainexMark->inherited_outof ) ? $mainexMark->inherited_outof : 0 ) + (($mainexMark->outof) ? $mainexMark->outof : 0);
                                if (($obtainedoutOf > 0)) {
                                    $studentpercentforchart += ($obtainedmark / $obtainedoutOf * 100);
                                }
                            }
                            $overalclsout += $cnt;
                        }
                    }
                }
                if ($overalclsout > 0) {
                    $studentexamcnt += round($studentpercentforchart / $overalclsout, 2);
                }
                $overallexmcut ++;
            }
            if ($overallexmcut > 0) {
                $examcount = round($studentexamcnt / count($exams));
            }
        }
        $array['id'] = 1;
        $array['Title'] = "Exam";
        $array['Description'] = $examcount > 0 ? $examcount . "%  overall in " . $overallexmcut . " Exam(s)" : ' No Exam(s) ';
        $array['icon'] = "ion-ribbon-a";
        $array['dangertext'] = "";
        $array['unread'] = $exm_unread ? '(' . $exm_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/timeline/mine/exams";
        $totalarray['columns'][] = $array;
        $queryParams = array();
        if (count($subj_Ids) > 0) {
            $queryParams[] = 'subjct_modules IN(' . implode(',', $subj_Ids) . ')';
        }
        if (count($subjids) > 0) {
            $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
        }
        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
        $assignmentmas = AssignmentsMaster::find($conditionvals);
        $ass_pending = 0;
        if (count($assignmentmas) > 0) {
            foreach ($assignmentmas as $assmnt) {
                $stud_assignment = StudentAssignments::findFirst('student_id=' . $stud_info->id . ' and assignment_id=' . $assmnt->id);
                $ass_pending += $stud_assignment ? 0 : 1;
            }
        }
        $overalsubout = 0;
        if (count($subj_Ids) > 0) {
            foreach ($subj_Ids as $sub) {
                $subjagg = array();
                $subjagg = ControllerBase::getAllSubjectAndSubModules(array($sub));
                $overalstuout = $overalstutotalmarks = 0;
                $assignments = AssignmentsMaster::find('is_evaluation = 1 and grp_subject_teacher_id IN ( ' . implode(',', $subjids) . ') and subjct_modules IN ( ' . implode(',', $subjagg) . ')');
                if (count($assignments) > 0) {
                    foreach ($assignments as $assignment) {
                        $assmarks = AssignmentMarks::findFirst('assignment_id = ' . $assignment->id . ' and student_id = ' . $stud_info->id);
                        if ($assmarks) {
                            $totassign[$sub] = $assmarks->id;
                            $obtainedmark = ($assmarks->marks) ? $assmarks->marks : 0;
                            $obtainedOutOf = ($assmarks->outof) ? $assmarks->outof : 0;
                            if (($obtainedOutOf) > 0) {
                                $overalstutotalmarks += ($obtainedmark / $obtainedOutOf * 100);
                            }
                            $overalstuout ++;
                        }
                    }
                    if ($overalstuout > 0) {
                        $studentpercentforchart += round($overalstutotalmarks / $overalstuout, 2);
                    }
                    $overalsubout ++;
                }
            }
        }
        if ($overalsubout > 0) {
            $assgncount = round($studentpercentforchart / count($totassign));
        }
        $array['id'] = 2;
        $array['Title'] = "Assignments";
        $array['Description'] = count($assignmentmas) > 0 ? (count($assignmentmas) . " Assignment(s).") . ($ass_pending != 0 ? $ass_pending . " Pending. " : '')
                . ($assgncount > 0 ? $assgncount . '% overall' : '') : ' No Assignment(s)';
        $array['icon'] = "ion-ios-paper-outline";
        $array['dangertext'] = "";
        $array['unread'] = $ass_unread ? '(' . $ass_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/timeline/mine/assignment";
        $totalarray['columns'][] = $array;

        $division = RatingDivision::find(implode(' or ', $res));
        $divcnt = $totratpts = 0;
        if (count($division) > 0) {
            foreach ($division as $div) {
                $ratingCategorys = RatingCategoryMaster::find();
                foreach ($ratingCategorys as $col) {
                    $rating_record = StudentClassteacherRating::findfirst('student_id = ' . $stud_info->id
                                    . ' and rating_category =' . $col->id
                                    . ' and rating_division_id = ' . $div->id);
                    if ($rating_record) {
                        $divsin_cnt[$div->id] = $rating_record->rating_value;
                        $ratval = $rating_record ? RatingCategoryValues::findFirstById($rating_record->rating_value) : '';
                        $ratpts = $rating_record ? (($col->category_weightage * $ratval->rating_level_value) / 100) : 0;
                        $totratpts += $ratpts;
                    }
                }
                $divcnt++;
            }
        }
        $clsratpercnt = count($divsin_cnt) > 0 ? round($totratpts / count($divsin_cnt)) : 0;
        $divcnt = 0;
        if (count($subj_Ids) > 0) {
            foreach ($subj_Ids as $sub) {
                $subjagg = array();
                $subjagg = ControllerBase::getAllSubjectAndSubModules(array($sub));
                $suject = $this->find_childtreevaljson($sub);
                $subcnt = count($suject);
                $totratpts = 0;
                if (count($division) > 0) {
                    foreach ($division as $div) {
                        $ratingCategorys = RatingCategoryMaster::find();
                        if (count($ratingCategorys) > 0) {
                            foreach ($ratingCategorys as $col) {
                                $rating_record = StudentSubteacherRating::find('student_id = ' . $stud_info->id
                                                . ' and rating_category =' . $col->id
                                                . ' and rating_division_id = ' . $div->id
                                                . ' and subject_master_id IN (' . implode(',', $subjids) . ')'
                                                . ' and subjct_modules IN (' . implode(',', $subjagg) . ')');
                                if (count($rating_record) > 0) {
                                    foreach ($rating_record as $value) {
                                        $cntdiv[$div->id] = $value->rating_division_id;
                                        $cntsub[$value->subjct_modules] = $value->subjct_modules;
                                        $ratval = $rating_record ? RatingCategoryValues::findFirstById($value->rating_value) : '';
                                        $ratpts = $rating_record ? (($col->category_weightage * $ratval->rating_level_value) / 100) : 0;
                                        $totratpts += $ratpts;
                                    }
                                }
                            }
                        }
                    }
                }
                $ratpercnt += count($cntdiv) > 0 ? round($totratpts / count($cntdiv), 2) : 0;
                $overallsubcnt += $subcnt;
            }
        }
        $subratpercnt = count($cntsub) > 0 ? round($ratpercnt / count($cntsub)) : 0;
        $array['id'] = 3;
        $array['Title'] = "Remarks / Ratings";
        $array['Description'] = ($clsratpercnt > 0 ? $clsratpercnt . " pts in Class. " : 'No Class Rating(s). ') . ' ' .
                ($subratpercnt > 0 ? $subratpercnt . " pts overall by Subject" : ' No Subject Rating(s).');
        $array['icon'] = "ion-ios-star-half";
        $array['dangertext'] = "";
        $array['unread'] = $rating_unread ? '(' . $rating_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/timeline/mine/rating";
        $totalarray['columns'][] = $array;

        $queryParams = array();
        $queryParams[] = 'hmwrkdate >= "' . strtotime(date('d-m-Y', time()) . ' 00:00:00')
                . '" and hmwrkdate <= "' . strtotime(date('d-m-Y', time()) . ' 23:59:59') . '"';
        if (count($subj_Ids) > 0) {
            $queryParams[] = 'subjct_modules IN(' . implode(',', $subj_Ids) . ')';
        }
        if (count($subjids) > 0) {
            $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
        }
        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
        $homework = HomeWorkTable::find($conditionvals);
        $array['id'] = 4;
        $array['Title'] = "Homework";
        $array['Description'] = count($homework) > 0 ? count($homework) . " Homework(s) added today" : ' No Homework(s) added today';
        $array['icon'] = "ion-ios-pricetags-outline";
        $array['dangertext'] = "";
        $array['unread'] = $home_unread ? '(' . $home_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/timeline/mine/homework";
        $totalarray['columns'][] = $array;

        $overalsubout = $studentpercentforchart = 0;
        if (count($subj_Ids) > 0) {
            foreach ($subj_Ids as $sub) {
                $subjagg = array();
                $subjagg = ControllerBase::getAllSubjectAndSubModules(array($sub));
                $overalstuout = $overalstutotalmarks = 0;
                $classtests = ClassTest::find('grp_subject_teacher_id IN ( ' . implode(',', $subjids) . ') and subjct_modules IN ( ' . implode(',', $subjagg) . ')');
                if (count($classtests) > 0) {
                    foreach ($classtests as $classtest) {
                        $clsTstMarks = ClassTestMarks::findFirst('class_test_id = ' . $classtest->class_test_id . ' and student_id = ' . $stud_info->id);
                        if ($clsTstMarks) {
                            $tes[$sub] = $clsTstMarks->classtest_marks_id;
                            $obtainedmark = ($clsTstMarks->marks) ? $clsTstMarks->marks : 0;
                            $obtainedOutOf = ($clsTstMarks->outof) ? $clsTstMarks->outof : 0;
                            if (($obtainedOutOf) > 0) {
                                $overalstutotalmarks += ($obtainedmark / $obtainedOutOf * 100);
                            }
                            $overalstuout ++;
                        }
                    }
                    if ($overalstuout > 0) {
                        $studentpercentforchart += round($overalstutotalmarks / $overalstuout, 2);
                    }

                    $overalsubout ++;
                }
            }
        }
        if ($overalsubout > 0) {
            $testcount = round($studentpercentforchart / count($tes));
        }
        $array['id'] = 5;
        $array['Title'] = "Classtest";
        $array['Description'] = $testcount > 0 ? ceil($testcount) . "% overall in all Subjects" : ' No Classtest(s)';
        $array['icon'] = "ion-ios-book-outline";
        $array['dangertext'] = "";
        $array['unread'] = $test_unread ? '(' . $test_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/timeline/mine/tests";
        $totalarray['columns'][] = $array;
        $announcment = array();
        $announcements = Announcement::find();
        $ann_cnt = 0;
        if (count($announcements) > 0) {
            foreach ($announcements as $announc) {
                $l = 0;
                $ann_items = AnnouncementTolist::find('announcement_id=' . $announc->id . ' and to="' . $loginid . '"');
                if (count($ann_items) > 0) {
                    foreach ($ann_items as $a) {
                        $expld = explode('_', $a->to_name);
                        if ($expld[0] == 'Intimation' && $l == 0):
                            $ann_cnt++;
                            $l = 1;
                        endif;
                    }
                }
            }
        }
        $events = Events::find();
        $eve_cnt = 0;
        if (count($events) > 0) {
            foreach ($events as $event) {
                $l = 0;
                $event_items = EventsList::find('event_id=' . $event->id . ' and events_to="' . $loginid . '"');
                if (count($event_items) > 0) {
                    foreach ($event_items as $eve) {
                        $expld = explode('_', $eve->events_toname);
                        if ($expld[0] == 'Events' && $l == 0):
                            $eve_cnt++;
                            $l = 1;
                        endif;
                    }
                }
            }
        }

        $param = array();
        $param['user_id'] = $loginid;
        $param['user_type'] = 'student';
        $overallPercent = ReportsController::getAttendancePercentMonthly($param);
        $monthlyPercent = $overallPercent[0];
        $param['monthhead'] = ($overallPercent[1]);
        $param['valhead'] = ($overallPercent[2]);
        $param['legend'] = ($overallPercent[3]);
        $mntharr = array_unique($param['monthhead']);
        usort($mntharr, 'ReportsController::month_compare');
        $currentyr = ControllerBase::get_current_academic_year();
        $year = explode('-', $currentyr->name);
        $monthval = '';
        for ($m = 1; $m <= date('m'); $m++) {
            if (($m == 11) || ($m == 12)) {
                $monthval[] = date($m . '-' . '2016', mktime(0, 0, 0, $m, 1, date('2016')));
            } else {
                $monthval[] = date('0' . $m . '-' . '2016', mktime(0, 0, 0, $m, 1, date('2016')));
            }
        }
        if (count($mntharr) > 0) {
            $valhead = array_unique($param['valhead']);
            if (count($monthval) > 0) {
                foreach ($monthval as $hvalue) {
                    $atttaken = $stu_att_totByVal = 0;
                    if (count($valhead) > 0) {
                        foreach ($valhead as $vvalue) {
                            $student_att_props_val = AttendanceSelectbox::findFirst("attendance_for = 'student'"
                                            . ' and attendanceid= "' . $vvalue . '"');
                            $noofperiods = PeriodMaster::find('LOCATE(node_id,"' . str_replace(',', '-', $aggregate_key) . '" )' . " and user_type = 'student'");
                            $counttaken = $monthlyPercent[$loginid][$hvalue][$vvalue] ? ($monthlyPercent[$loginid][$hvalue][$vvalue] / $noofperiods) : 0;

                            $atttaken += ($counttaken / count($noofperiods));
                            $stu_att_totByVal+= ($counttaken / count($noofperiods)) * $student_att_props_val->attendancevalue;
                        }
                    }
                    $monthlyPercent[$loginid][$hvalue]['total'] = $atttaken;
                    $monthlyPercent[$loginid][$hvalue]['percent'] = $atttaken > 0 ? (round($stu_att_totByVal / $atttaken * 100, 2)) : '';
                }
            }
        }
        if (count($monthlyPercent) > 0) {
            foreach ($monthlyPercent as $key => $mnthval) {
                foreach ($mnthval as $key => $val) {
                    $attpercent += $val['percent'];
                }
            }
        }
        $array['id'] = 6;
        $array['Title'] = "Attendance";
        $array['Description'] = $attpercent ? round(($attpercent / date('m')), 2) . "% Attendance overall " : 'Attendance not yet taken';
        $array['icon'] = "ion-ios-calendar-outline";
        $array['dangertext'] = "";
        $array['unread'] = $att_unread ? '(' . $att_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/timeline/mine/attendance";
        $totalarray['columns'][] = $array;

        $array['id'] = 7;
        $array['Title'] = "Intimation";
        $array['Description'] = $ann_cnt > 0 ? $ann_cnt . " Circular" : 'No  Circular(s)';
        $array['icon'] = "ion-ios-bell-outline";
        $array['dangertext'] = "";
        $array['unread'] = $circular_unread ? '(' . $circular_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/timeline/mine/Intimation";
        $totalarray['columns'][] = $array;

        $array['id'] = 8;
        $array['Title'] = "Events";
        $array['Description'] = $eve_cnt > 0 ? $eve_cnt . " Events" : 'No  Event(s)';
        $array['icon'] = "ion-ios-calendar";
        $array['dangertext'] = "";
        $array['unread'] = $events_unread ? '(' . $events_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/timeline/mine/events";
        $totalarray['columns'][] = $array;

        $array['id'] = 9;
        $array['Title'] = "Fees";
        $array['Description'] = 'Coming Soon...';
        $array['icon'] = "ion-social-usd";
        $array['dangertext'] = "";
        $array['unread'] = '';
        $array['href'] = '';
        $totalarray['columns'][] = $array;
        $total['col1'] = $totalarray;
        $cnt = $exm_unread + $ass_unread + $rating_unread + $home_unread + $test_unread + $att_unread;
        $total['col2'] = $cnt > 0 ? '(' . $cnt . ')' : '';
        print_r(json_encode($total));
        exit;
        /* } else {
          $totalarray = $array = array();
          $array['id'] = 1;
          $array['Title'] = "Fees";
          $array['Description'] = 'Library dues Rs.160 pending since yesterday.';
          $array['icon'] = "ion-social-usd";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = '';
          $totalarray['columns'][] = $array;
          $array['id'] = 2;
          $array['Title'] = "Attendance";
          $array['Description'] = '72% attendance overall. 50% in maths.';
          $array['icon'] = "ion-ios-calendar-outline";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = '';
          $array['id'] = 3;
          $array['Title'] = "Marks";
          $array['Description'] = '80% overall in 2nd unit tests.';
          $array['icon'] = "ion-ribbon-a";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = '';
          $totalarray['columns'][] = $array;
          $array['id'] = 4;
          $array['Title'] = "Assignments";
          $array['Description'] = '2 new assignments. 3 pending.';
          $array['icon'] = "ion-ios-paper-outline";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = '';
          $totalarray['columns'][] = $array;
          $array['id'] = 5;
          $array['Title'] = "Support";
          $array['Description'] = 'Your new support ticket noticed by class teacher. 1 reply unread.';
          $array['icon'] = "ion-ios-email-outline";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = '';
          $totalarray['columns'][] = $array;
          $array['id'] = 6;
          $array['Title'] = "Remarks/Ratings";
          $array['Description'] = 'New remarks received from class teacher.';
          $array['icon'] = "ion-ios-star-half";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = '';
          $totalarray['columns'][] = $array;
          $total['col1'] = $totalarray;
          print_r(json_encode($total));
          exit;
          } */
    }

    public function find_childtreevaljson($anode, $main = array()) {
        $exist = OrganizationalStructureValues::find('parent_id =' . $anode);
        if (count($exist) > 0) {
            foreach ($exist as $chl) {
                $main = $this->find_childtreevaljson($chl->id, $main);
            }
        } else {
            $main[] = OrganizationalStructureValues::findFirst('id =' . $anode)->id;
        }
        return $main;
    }

    public function getallNotificationAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $title = $this->request->get('title');
        $device_token = $this->request->get('device_token');
        $device_type = $this->request->get('device_type');
        if ($device_token && $device_type && $loginid) {
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['targetor_id'] = $loginid;
            $notify['device_token'] = $device_token;
            $notify['device_type'] = $device_type;
            if ($title == 'exams') {
                $notify['title'] = 'exam';
            } else if ($title == 'tests') {
                $notify['title'] = 'Classtest';
            } else {
                $notify['title'] = $title;
            }
            $notify['category'] = 'Mine';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'GetNotificationList.php', $notification_data);
            $result = json_decode($responseParam);
            if (count($result) > 0) {
                foreach ($result as $value) {
                    $columns = array();
                    $columns['id'] = $value->notify_id;
                    $columns['activityid'] = $value->activity_id;
                    $columns['Title'] = $value->message;
                    $columns['Description'] = AnnouncementController::time_elapsed_string($value->date);
                    $columns['readState'] = "Unread";
                    $columns['notifyid'] = $value->notify_id;
                    $array['columns'][] = $columns;
                }
            }
            print_r(json_encode($array));
            exit;
        }
    }

    public function _getSubMandNodesId($acdyrMas, $nodes = array()) {
        $fields = OrganizationalStructureValues::findFirstById($acdyrMas->parent_id);
        $iscycle = OrganizationalStructureMaster::findFirstById($fields->org_master_id);
        if ($iscycle->module == "Subject"):
            $nodes[] = $fields->id;
        endif;
        if (($fields->parent_id) > 0):
            $nodes = $this->_getSubMandNodesId($fields, $nodes);
        endif;
        return $nodes;
    }

    public function annoucementNotificationAction() {
        $loginid = $this->request->get('loginid');
        $id = $this->request->get('circularid');
        $title = $this->request->get('title');
        //$split = explode(' ', $title);
        $replmine = str_replace("Mine", "", $title, $ismine);
        $replmyclass = str_replace("My class", "", $title, $ismyclass);
        $replmyschl = str_replace("My school", "", $title, $ismyschl);
        $type = ($ismine > 0) ? 'Intimation' : (($ismyclass>0) ? 'Announcement' : 'Circular');
        $result = $arr = array();
        if ($id) {
            $announ_item = AnnouncementTolist::findFirstById($id);
            $announ_mas = Announcement::findFirstById($announ_item->announcement_id);
            if ($announ_item->status == 'Acknowledge') {
                $columns['color'] = 'royal cursor';
                $columns['status'] = 'Acknowledged';
                $columns['isack'] = '1';
            } else {
                $columns['color'] = 'balanced';
                $columns['status'] = 'Acknowledge';
                $columns['isack'] = '0';
            }
            $columns['id'] = $id;
            $columns['circularon'] = date('Y-m-d h:i:s', $announ_mas->date);
            $columns['circulardt'] = date('d/m/Y', $announ_mas->date);
            $columns['circulartme'] = date('H:i A', $announ_mas->date);
            $columns['description'] = $announ_mas->message;
            $columns['circularby'] = StaffInfo::findFirstById($announ_mas->created_by)->Staff_Name;
            $result[] = $columns;
        } else {
            $announ_item = Announcement::find(array(
                        'order' => " modified_date DESC"
            ));
            if (count($announ_item) > 0):
                foreach ($announ_item as $items) {
                    $ann_items = AnnouncementTolist::findFirst('announcement_id=' . $items->id . ' and to="' . $loginid . '"');
                    if ($ann_items) {
                        $expld = explode('_', $ann_items->to_name);
                        if ($expld[0] == $type):
                            if ($ann_items->status == 'Acknowledge') {
                                $columns['color'] = 'royal cursor';
                                $columns['status'] = 'Acknowledged';
                                $columns['isack'] = '1';
                            } else {
                                $columns['color'] = 'balanced';
                                $columns['status'] = 'Acknowledge';
                                $columns['isack'] = '0';
                            }
                            $columns['id'] = $ann_items->id;
                            $columns['circularon'] = date('Y-m-d h:i:s', $items->date);
                            $columns['circulardt'] = date('d/m/Y', $items->date);
                            $columns['circulartme'] = date('H:i A', $items->date);
                            $columns['description'] = $items->message;
                            $columns['circularby'] = StaffInfo::findFirstById($items->created_by)->Staff_Name;
                            $result[] = $columns;
                        endif;
                    }
                }
            endif;
        }
        $arr['final'] = $result;
        $arr['title'] = $type;
        print_r(json_encode($arr));
        exit;
    }

    public function myClassAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $device_token = $this->request->get('device_token');
        $device_type = $this->request->get('device_type');

        if ($device_type || $device_token || $loginid) {
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['targetor_id'] = $loginid;
            $notify['device_type'] = $device_type;
            $notify['device_token'] = $device_token;
            $notify['category'] = 'Myclass';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'GetNotificationList.php', $notification_data);
            $result = json_decode($responseParam);
            if (count($result) > 0) {
                foreach ($result as $value) {
                    $columns = array();
                    $columns['id'] = $value->notify_id;
                    $columns['activityid'] = $value->activity_id;
                    $columns['Title'] = $value->title;
                    $columns['Description'] = $value->message;
                    $columns['readState'] = "";
                    if ($value->title == 'test') {
                        $columns['icon'] = "ion-ios-paper-outline";
                        $columns['href'] = "#/myapp/maintab/home/timeline/tests";
                    } if ($value->title == 'mainexam') {
                        $columns['icon'] = "ion-ribbon-a";
                        $columns['href'] = "#/myapp/maintab/home/timeline/exams";
                    }
                    $columns['warningtext'] = "";
                    $array['columns'][] = $columns;
                }
            }
            print_r(json_encode($array));
            exit;
        }
    }

    public function viewEventsAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $eventitemid = $this->request->get('item_id');
        $title = $this->request->get('title');
        //$split = explode(' ', $title);
        //$type = $split[0] == 'Mine' ? 'Events' : ($split[1] == 'class' ? 'ClassEvents' : 'SchoolEvents');
       
        $replmine = str_replace("Mine", "", $title, $ismine);
        $replmyclass = str_replace("My class", "", $title, $ismyclass);
        $replmyschl = str_replace("My school", "", $title, $ismyschl);
        $type = ($ismine > 0) ? 'Events' : (($ismyclass>0) ? 'ClassEvents' : 'SchoolEvents');

         $result = $arr = array();
        if ($eventitemid) {
            $list = EventsList::findFirstById($eventitemid);
            $events = Events::findFirstById($list->event_id);
            $getcount = EventsList::find('event_id=' . $list->event_id);
            $loginids = $acceptid = array();
            foreach ($getcount as $value) {
                $loginids[] = $value->events_to;
                if ($value->status == 'Accepted'):
                    $acceptid[] = $value->events_to;
                endif;
            }
            if ($list->status == 'Accepted') {
                $array['color'] = 'royal cursor';
                $array['status'] = 'Accepted';
                $array['isack'] = '1';
            } else {
                $array['color'] = 'balanced';
                $array['status'] = 'Accept';
                $array['isack'] = '0';
            }
            $pending = array_diff($loginids, $acceptid);
            $array['eventid'] = $list->id;
            $array['eveHeader'] = 'Event Details';
            $array['evetypehead'] = 'Type';
            $array['eveStEnd'] = 'DateTime';
            $array['locaShrt'] = 'Location';
            $array['eveTitle'] = $events->title ? $events->title : 'Event Details';
            $array['eveDetails'] = $events->description;
            $array['eveimgSrc'] = $events->upload ? ($this->url->get(UPLOAD_URI . $events->upload)) : 'img/material4.jpg';
            $array['eventAddeddt'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', $events->created_on));
            //strlen($events->location) > 15 ? substr($events->location, 0, 15) : $events->location;
            $array['location'] = $events->location;
            $evetype = array('', 'Seminars', 'Workshops', 'Competitions', 'Events');
            $array['evetype'] = $evetype[$events->type];
            if ((date('M d', $events->from_date)) == (date('M d', $events->to_date))) {
                $array['eveStEndWitTim'] = date('M d', $events->from_date) . ' ' . date('H:i A', $events->from_date);
            } else {
               // $array['eveStEnd'] = date('M d', $events->from_date) . ($events->to_date ? ' - ' . date('M d', $events->to_date) : '');
                $array['eveStEndWitTim'] = date('M d', $events->from_date) . ' ' . date('H:i A', $events->from_date)
                        . ( $events->to_date ? '  to ' . date('M d', $events->to_date) . ' ' . date('H:i A', $events->to_date) : '' );
            }

            $array['invited'] = count($loginids);
            $array['accepted'] = count($acceptid);
            $array['pending'] = count($pending);
            $result[] = $array;
        } else {
            $eventsmaster = Events::find(array(
                        'order' => " modified_on DESC"
            ));
            if (count($eventsmaster) > 0):
                foreach ($eventsmaster as $events) {
                    $list = EventsList::findFirst('event_id=' . $events->id . ' and events_to="' . $loginid . '"');
                    if ($list) {
                        $expld = explode('_', $list->events_toname);
                        if ($expld[0] == $type):
                            $getcount = EventsList::find('event_id=' . $events->id);
                            $loginids = $acceptid = array();
                            foreach ($getcount as $value) {
                                $loginids[] = $value->events_to;
                                if ($value->status == 'Accepted'):
                                    $acceptid[] = $value->events_to;
                                endif;
                            }
                            $pending = array_diff($loginids, $acceptid);
                            if ($list->status == 'Accepted') {
                                $array['color'] = 'royal cursor';
                                $array['status'] = 'Accepted';
                                $array['isack'] = '1';
                            } else {
                                $array['color'] = 'balanced';
                                $array['status'] = 'Accept';
                                $array['isack'] = '0';
                            }
                            $array['eventid'] = $list->id;
                            $array['eveHeader'] = 'Event Details';
                            $array['evetypehead'] = 'Type';
                            $array['eveStEnd'] = 'DateTime';
                            $array['locaShrt'] = 'Location';
                            $array['eveTitle'] = $events->title ? $events->title : 'Event Details';
                            $array['eveDetails'] = $events->description;
                            $array['eveimgSrc'] = $events->upload ? ($this->url->get(UPLOAD_URI . $events->upload)) : 'img/material4.jpg';
                            $array['eventAddeddt'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', $events->created_on));
                            //$array['locaShrt'] = strlen($events->location) > 15 ? substr($events->location, 0, 15) : $events->location;
                            $array['location'] = $events->location;
                            $evetype = array('', 'Seminars', 'Workshops', 'Competitions', 'Events');
                            $array['evetype'] = $evetype[$events->type];
                            if ((date('M d', $events->from_date)) == (date('M d', $events->to_date))) { 
                                $array['eveStEndWitTim'] = date('M d', $events->from_date) . ' ' . date('H:i A', $events->from_date);
                            } else {
                               // $array['eveStEnd'] = date('M d', $events->from_date) . ($events->to_date ? ' - ' . date('M d', $events->to_date) : '');
                                $array['eveStEndWitTim'] = date('M d', $events->from_date) . ' ' . date('H:i A', $events->from_date)
                                        . ( $events->to_date ? '  to ' . date('M d', $events->to_date) . ' at ' . date('H:i A', $events->to_date) : '' );
                            }

                            $array['invited'] = count($loginids);
                            $array['accepted'] = count($acceptid);
                            $array['pending'] = count($pending);
                            $result[] = $array;
                        endif;
                    }
                }
            endif;
        }
        $arr['final'] = $result;
        $arr['title'] = $type;
        print_r(json_encode($arr));
        exit;
    }

    public function mySchoolAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $device_token = $this->request->get('device_token');
        $device_type = $this->request->get('device_type');

        if ($device_token || $device_type || $loginid) {
            $notify['aggregate_key'] = SUBDOMAIN;
            $notify['targetor_id'] = $loginid;
            $notify['device_type'] = $device_type;
            $notify['device_token'] = $device_token;
            $notify['category'] = 'Myschool';
            $notification_data = json_encode($notify);
            $responseParam = IndexController::curlIt(COMMAPI . 'GetNotificationList.php', $notification_data);
            $result = json_decode($responseParam);
            if (count($result) > 0) {
                foreach ($result as $value) {
                    $columns = array();
                    $columns['id'] = $value->notify_id;
                    $columns['activityid'] = $value->activity_id;
                    $columns['Title'] = $value->title;
                    $columns['Description'] = $value->message;
                    $columns['readState'] = "";
                    $columns['icon'] = "ion-ios-paper-outline";
                    $columns['warningtext'] = "";
                    $array['columns'][] = $columns;
                }
            }
            print_r(json_encode($array));
            exit;
        }
    }

    public function calMineEventTypeAction() {
        $type = array();
        $evetty = array("", "Seminars", "Workshops", "Competitions", "Events", "Assignments", "Homeworks", "Exams", "Class Tests");
        for ($i = 1; $i <= 8; $i++) {
            $type['id'] = $i;
            $type['text'] = $evetty[$i];
            $type['checked'] = false;
            $type['icon'] = null;
            $events[] = $type;
        }
        print_r(json_encode($events));
        exit;
    }

    public function getCalendarMineEventsAction() {
        $types = $this->request->get('type');
        $type = $types? explode(',', $types): array();
        $user_type = $this->request->get('user_type');
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        //if ($user_type != "common") {
        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $classroomids = $subj_Ids = $subjids = array();
        $studentClassrooms = ClassgroupStudents::find("find_in_set('$stud_info->id', students_id)>0");
        foreach ($studentClassrooms as $cvalue) {
            $classroomids[] = $cvalue->classroom_master_id;
        }
        $fetchGroupSubjClass = GroupSubjectsTeachers::find("classroom_master_id IN (" . implode(',', $classroomids) . ")");
        foreach ($fetchGroupSubjClass as $svalue) {
            $subj_Ids[] = $svalue->subject_id;
            $subjids[] = $svalue->id;
        }
        if (count($subj_Ids) > 0) {
            $queryParams[] = 'subjct_modules IN(' . implode(',', $subj_Ids) . ')';
        }
        if (count($subjids) > 0) {
            $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
        }
        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
        $assignments = AssignmentsMaster::find($conditionvals);
        $homeworks = HomeWorkTable::find($conditionvals);
        $calen = array();
        if (in_array(5, $type)|| !$type) {
            if (count($assignments) > 0) {
                foreach ($assignments as $assignment) {
                    $array['id'] = 'assignment_' . $assignment->id;
                    $subjini = OrganizationalStructureValues::findFirst('id = ' . $assignment->subjct_modules);
                    $subj_arr = MobappController::_getMandNodesForExam($subjini);
                    if (count($subj_arr) > 0) {
                        $reverse = array_reverse($subj_arr);
                        $array['title'] = implode(' | ', array('Assignment',(implode('>', $reverse))));
                    } else { 
                        $array['title'] = implode(' | ', array('Assignment',$subjini->name));
                    }
                    $array['value'] = $assignment->topic;
                    $array['sortdate'] = $assignment->submission_date;
                    $array['date'] = date('Y/m/d', $assignment->submission_date);
                    $array['time'] = date('H:i', $assignment->submission_date);
                    $calen[] = $array;
                }
            }
        }
        if (in_array(6, $type)|| !$type) {
            if (count($homeworks) > 0) {
                foreach ($homeworks as $homework) {
                    $array['id'] = 'homework_' . $homework->id;
                    $subjini = OrganizationalStructureValues::findFirst('id = ' . $homework->subjct_modules);
                    $subj_arr = MobappController::_getMandNodesForExam($subjini);
                    if (count($subj_arr) > 0) {
                        $reverse = array_reverse($subj_arr);
                        $array['title'] = implode(' | ', array('Homework',(implode('>', $reverse))));
                    } else { 
                        $array['title'] = implode(' | ', array('Homework',$subjini->name));
                    }
                    $array['value'] = $homework->homework;
                    $array['sortdate'] = $homework->hmwrkdate;
                    $array['date'] = date('Y/m/d', $homework->hmwrkdate);
                    $array['time'] = date('H:i', $homework->hmwrkdate);
                    $calen[] = $array;
                }
            }
        }

        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $aggregate_key = StudentMapping::findFirst('student_info_id =' . $stud_info->id)->aggregate_key;


        /* Get all subject and subj-clsrm combination */
        $classroomids = $subj_Ids = $subjids = array();
        $studentClassrooms = ClassgroupStudents::find("find_in_set('$stud_info->id', students_id)>0");
        if (count($studentClassrooms) > 0) {
            foreach ($studentClassrooms as $cvalue) {
                $classroomids[] = $cvalue->classroom_master_id;
            }
        }
        $fetchGroupSubjClass = GroupSubjectsTeachers::find("classroom_master_id IN (" . implode(',', $classroomids) . ")");
        if (count($fetchGroupSubjClass) > 0) {
            foreach ($fetchGroupSubjClass as $svalue) {
                $subj_Ids[] = $svalue->subject_id;
                $subjids[] = $svalue->id;
            }
        }

        /* Get all examz */
        $get_Query_For_Exmmak = ControllerBase::buildExamQuery($aggregate_key);
        $mainexamsdets = Mainexam::find(implode(' or ', $get_Query_For_Exmmak));

        /* Build query conditions */
        if (count($subj_Ids) > 0) {
            $queryParams[] = 'subjct_modules IN(' . implode(',', $subj_Ids) . ')';
        }
        if (count($subjids) > 0) {
            $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
        }
        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';


        /* Get Class test list */
        $classtests = ClassTest::find($conditionvals);
        if (in_array(7, $type) || !$type) {
            if (count($mainexamsdets) > 0) {
                foreach ($mainexamsdets as $exam) {
                    $array['id'] = 'exam_' . $exam->id;
                    $array['title'] = 'Exam';
                    $array['value'] = $exam->exam_name . ' (' . $exam->examCode . ' )';
                    $array['sortdate'] = $exam->created_date;
                    $array['date'] = date('Y/m/d', $exam->created_date);
                    $array['time'] = date('H:i', $exam->created_date);
                    $calen[] = $array;
                }
            }
        }
        if (in_array(8, $type) || !$type) {
            if (count($classtests) > 0) {
                foreach ($classtests as $test) {
                    $array['id'] = 'classtest_' . $test->class_test_id;
                    $subjini = OrganizationalStructureValues::findFirst('id = ' . $test->subjct_modules);
                    $subj_arr = $this->_getMandNodesForExam($subjini);
                    if (count($subj_arr) > 0) {
                        $reverse = array_reverse($subj_arr); 
                        $array['title'] = implode(' | ', array('Class Test',(implode('>', $reverse))));
                    } else { 
                        $array['title'] = implode(' | ', array('Class Test',$subjini->name));
                    }
                    $array['value'] = $test->class_test_name;
                    $array['sortdate'] = $test->created_date;
                    $array['date'] = date('Y/m/d', $test->created_date);
                    $array['time'] = date('H:i', $test->created_date);
                    $calen[] = $array;
                }
            }
        }
        $events = count($type)>0 ? Events::find('type IN (' . $types . ')') : Events::find();
        if (count($events) > 0) {
            foreach ($events as $event) {
                $eventlist = EventsList::findFirst('event_id=' . $event->id . ' and (status = "Accepted" or status is NULL or status = "")');
                $listdet = explode(',', $eventlist->events_to);
                if (in_array($loginid, $listdet)) {
                    $tit = array();
                    $array['id'] = 'event_' . $event->id;
                    $evetype = array('', 'Seminars', 'Workshops', 'Competitions', 'Events');
                    $tit[] = $evetype[$event->type];
                    if($event->title) $tit[] = $event->title; 
                    $array['title'] = implode(' | ', $tit);
                    $array['value'] = $event->description ? $event->description : ($event->type . ($event->location ? ' | ' . $event->location : ''));
                    $array['date'] = date('Y/m/d', $event->from_date);
                    $array['time'] = date('H:i', $event->from_date);
                    $array['sortdate'] = $event->from_date;
                    $array['totime'] = $event->to_date ? date('H:i', $event->to_date) : '';
                    $calen[] = $array;
                }
            }
        }
        /* } else {
          $calen = array();
          } */
        $timelinearr = $calen;
        $result = usort($timelinearr, 'MobappController::date_compare');
        print_r(json_encode(array_reverse($timelinearr)));
        exit;
    }

    public function date_compare($a, $b) {
        $t1 = ($a['sortdate']);
        $t2 = ($b['sortdate']);
        return $t2 - $t1;
    }

    public function viewCalendarMineEventsAction() {
        $eventid = $this->request->get('id');
        $explod = explode('_', $eventid);
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        if ($explod[0] == 'assignment') {
            $assignment = AssignmentsMaster::findFirst('id=' . $explod[1]);
            $array['eveTitle'] = $assignment->topic;
            $array['eveHeader'] = 'Assignment'; 
            $array['eveStEnd'] = 'Deadline'; 
            $array['evetypehead'] = 'Subject';
            $array['evetype'] =  OrganizationalStructureValues::findFirst('id = ' . $assignment->subjct_modules)->name ;
            $array['eveDetails'] = $assignment->desc;
            $array['eventAddeddt'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', $assignment->created_date)); 
            $array['eveStEndWitTim'] = date('M d', $assignment->submission_date) . ' at ' . date('H:i A', $assignment->submission_date);
        } if ($explod[0] == 'homework') {
            $homework = HomeWorkTable::findFirst('id=' . $explod[1]);
            $array['eveTitle'] = OrganizationalStructureValues::findFirst('id = ' . $homework->subjct_modules)->name;
            $array['eveHeader'] = 'Homework'; 
            $array['eveStEnd'] = 'DateTime'; 
            $array['eveDetails'] = $homework->homework;
            $array['eventAddeddt'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', $homework->created_date));
            //$array['eveStEnd'] = date('M d', $homework->hmwrkdate);
            $array['eveStEndWitTim'] = date('M d', $homework->hmwrkdate) . ' at ' . date('H:i A', $homework->hmwrkdate);
        }
        if ($explod[0] == 'classtest') {
            $classtest = ClassTest::findFirst('class_test_id=' . $explod[1]);
            $array['eveTitle'] = OrganizationalStructureValues::findFirst('id = ' . $classtest->subjct_modules)->name;
            $array['eveHeader'] = 'Classtest'; 
            $array['eveStEnd'] = 'DateTime'; 
            $array['eveDetails'] = $classtest->class_test_name;
            $array['eventAddeddt'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', $classtest->created_date));
            //$array['eveStEnd'] = date('M d', $classtest->created_date);
            $array['eveStEndWitTim'] = date('M d', $classtest->created_date) . ' at ' . date('H:i A', $classtest->created_date);
        }if ($explod[0] == 'exam') {
            $exam = Mainexam::findFirst('id=' . $explod[1]);
            $array['eveTitle'] = $exam->exam_name;
            $array['eveHeader'] = 'Exam'; 
            $array['eveStEnd'] = 'DateTime';
            $array['eveDetails'] = $exam->exam_name . '(' . $exam->examCode . ')';
            $array['eventAddeddt'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', $exam->created_date));
            //$array['eveStEnd'] = date('M d', $exam->created_date);
            $array['eveStEndWitTim'] = date('M d', $exam->created_date) . ' at ' . date('H:i A', $exam->created_date);
        }if ($explod[0] == 'event') {
            $events = Events::findFirstById($explod[1]);
            $list = EventsList::findFirst('event_id=' . $explod[1] . ' and events_to="' . $loginid . '"');
            $getcount = EventsList::find('event_id=' . $list->event_id);
            $loginids = $acceptid = array();
            foreach ($getcount as $value) {
                $loginids[] = $value->events_to;
                if ($value->status == 'Accepted'):
                    $acceptid[] = $value->events_to;
                endif;
            }
            if ($list->status == 'Accepted') {
                $array['color'] = 'royal cursor';
                $array['status'] = 'Accepted';
            } else {
                $array['color'] = 'balanced';
                $array['status'] = 'Accept';
            }
            $pending = array_diff($loginids, $acceptid);
            $array['eventid'] = $list->id;
            $array['eveHeader'] = 'Event Details';
            $array['evetypehead'] = 'Type';
            $array['eveStEnd'] = 'DateTime';
            $array['locaShrt'] = 'Location';
            $array['eveTitle'] = $events->title;
            $array['eveDetails'] = $events->description;
            $array['eveimgSrc'] = $events->upload ? $this->url->get(UPLOAD_URI . $events->upload) : '';
            $array['eventAddeddt'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', $events->created_on));
            //$array['locaShrt'] = strlen($events->location) > 15 ? substr($events->location, 0, 15) : $events->location;
            $array['location'] = $events->location;
            $evty = array('Event type', 'Seminars', 'Workshops', 'Competitions', 'Events');
            $array['evetype'] = $evty[$events->type];
            if ((date('M d', $events->from_date)) == (date('M d', $events->to_date))) {
                 $array['eveStEndWitTim'] = date('M d', $events->from_date) . ' at ' . date('H:i A', $events->from_date);
            } else {
               // $array['eveStEnd'] = date('M d', $events->from_date) . ($events->to_date ? ' - ' . date('M d', $events->to_date) : '');
                $array['eveStEndWitTim'] = date('M d', $events->from_date) . ' at ' . date('H:i A', $events->from_date)
                        . ( $events->to_date ? '  to ' . date('M d', $events->to_date) . ' at ' . date('H:i A', $events->to_date) : '' );
            }
            $array['invited'] = count($loginids);
            $array['accepted'] = count($acceptid);
            $array['pending'] = count($pending);
        }

        print_r(json_encode($array));
        exit;
    }

    public function assignmentAcknowledgeAction() {
        $loginid = $this->request->get('loginid');
        $studid = StudentInfo::findFirstByLoginid($loginid)->id;
        $assignment = $this->request->get('assignment_id');
        $assignments = AssignmentsMaster::findFirstById($assignment);
        $studassignment = StudentAssignments::findFirst('student_id=' . $studid . ' and assignment_id=' . $assignment) ?
                StudentAssignments::findFirst('student_id=' . $studid . ' and assignment_id=' . $assignment) : new StudentAssignments();
        $studassignment->student_id = $studid;
        $studassignment->assignment_id = $assignment;
        $studassignment->modified_by = $studid;
        $studassignment->modified_on = time();
        if ($studassignment->save()) {
            $msg['status'] = 'success';
        } else {
            $msg['status'] = 'fail';
        }
        print_r(json_encode($msg));
        exit;
    }

    public function circularAcknowledgeAction() {
        $loginid = $this->request->get('loginid');
        $circular = $this->request->get('circular_id');
        $announcementto = AnnouncementTolist::findFirstById($circular);
        $announcementto->status = 'Acknowledge';
        $uid = StudentInfo::findFirstByLoginid($loginid)->id;
        $announcementto->modified_by = $uid;
        $announcementto->modified_date = time();
        if ($announcementto->save()) {
            $ann_items = AnnouncementTolist::find('announcement_id=' . $announcementto->announcement_id);
            $allloginIds = $ackldgid = array();
            foreach ($ann_items as $items) {
                $allloginIds[] = $items->to;
                if ($items->status == 'Acknowledge'):
                    $ackldgid[] = $items->to;
                endif;
            }
            $valu1 = array_unique($allloginIds);
            $valu2 = array_unique($ackldgid);
            $pending = array_diff($valu1, $valu2);
            $announcement = Announcement::findFirstById($announcementto->announcement_id);
            $announcement->modified_date = time();
            if (count($pending) == 0) {
                $announcement->status = 'Acknowledge';
            }
            if ($announcement->save()) {
                $message['status'] = "success";
            } else {
                $message['status'] = "error";
            }
            print_r(json_encode($message));
            exit;
        }
    }

    public function myClassViewAction() {
        $device_token = $this->request->get('device_token');
        $device_type = $this->request->get('device_type');
        $uuid = $this->request->get('uuid');
        $user_type = $this->request->get('user_type');
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        //if ($user_type != "common") {
        $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $aggregate_key = StudentMapping::findFirst('student_info_id =' . $stud_info->id)->aggregate_key;
        $subj_Ids = array();
        $subjpids = ControllerBase::getAlSubjChildNodes(explode(',', $aggregate_key));
        $subjids = ControllerBase::getGrpSubjMasPossiblities(explode(',', $aggregate_key));
        $subjectsid = GroupSubjectsTeachers::find('id IN (' . implode(',', $subjids) . ')');
        foreach ($subjectsid as $svalue) {
            $subj_Ids[] = $svalue->subject_id;
        }
        $exm_unread = $test_unread = $circular_unread = $event_unread = 0;
        $notify = array();
        $notify['aggregate_key'] = SUBDOMAIN;
        $notify['targetor_id'] = $loginid;
        $notify['device_token'] = $device_token;
        $notify['device_type'] = $device_type;
        $notify['uuid'] = $uuid;
        $notify['category'] = 'Myclass';
        $notification_data = json_encode($notify);
        $responseParam = IndexController::curlIt(COMMAPI . 'GetNotificationAggregate.php', $notification_data);
        $content = json_decode($responseParam);
        if (count($content) > 0) {
            foreach ($content as $unread) {
                if (strtolower($unread->title) == 'mainexam') {
                    $exm_unread = $unread->count;
                }if (strtolower($unread->title) == 'classtest') {
                    $test_unread = $unread->count;
                }if (strtolower($unread->title) == 'announcement') {
                    $circular_unread = $unread->count;
                }if (strtolower($unread->title) == 'classevents') {
                    $event_unread = $unread->count;
                }
            }
        }
        $totalarray = array();
        $res = ControllerBase::buildExamQuery($aggregate_key);
        $mainexamdet = Mainexam ::find(implode(' or ', $res));
        $array['id'] = 1;
        $array['Title'] = "Exam";
        $array['Description'] = count($mainexamdet) > 0 ? count($mainexamdet) . '  Exam(s)' : 'No Exam(s)';
        $array['icon'] = "ion-ribbon-a";
        $array['dangertext'] = "";
        $array['unread'] = $exm_unread ? '(' . $exm_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/classtimeline/myclass/mainexam";
        $totalarray['columns'][] = $array;
        if (count($subj_Ids) > 0) {
            foreach ($subj_Ids as $sub) {
                $subjagg = array();
                $subjagg = ControllerBase::getAllSubjectAndSubModules(array($sub));
                $overalstuout = $overalstutotalmarks = 0;
                $classtests = ClassTest::find('grp_subject_teacher_id IN ( ' . implode(',', $subjids) . ') and subjct_modules IN ( ' . implode(',', $subjagg) . ')');
                $testcnt += count($classtests);
            }
        }
        $array['id'] = 2;
        $array['Title'] = "Classtest";
        $array['Description'] = $testcnt ? $testcnt . ' Classtest(s)' : 'No Classtest(s)';
        $array['icon'] = "ion-ios-book-outline";
        $array['dangertext'] = "";
        $array['unread'] = $test_unread ? '(' . $test_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/classtimeline/myclass/classtest";
        $totalarray['columns'][] = $array;

        $announcment = array();
        $announcements = Announcement::find();
        if (count($announcements) > 0) {
            $ann_cnt = 0;
            foreach ($announcements as $announc) {
                $l = 0;
                $ann_items = AnnouncementTolist::find('announcement_id=' . $announc->id . ' and to="' . $loginid . '"');
                if (count($ann_items) > 0) {
                    foreach ($ann_items as $a) {
                        $expld = explode('_', $a->to_name);
                        if ($expld[0] == 'Announcement' && $l == 0):
                            $ann_cnt++;
                            $l = 1;
                        endif;
                    }
                }
            }
        }
        $array['id'] = 3;
        $array['Title'] = "Announcement";
        $array['Description'] = $ann_cnt > 0 ? $ann_cnt . " Circular(s)" : 'No Circular(s)';
        $array['icon'] = "ion-ios-bell-outline";
        $array['dangertext'] = "";
        $array['unread'] = $circular_unread ? '(' . $circular_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/classtimeline/myclass/announcement";
        $totalarray['columns'][] = $array;

        $events = Events::find();
        if (count($events) > 0) {
            $eve_cnt = 0;
            foreach ($events as $event) {
                $l = 0;
                $event_items = EventsList::find('event_id=' . $event->id . ' and events_to="' . $loginid . '"');
                if (count($event_items) > 0) {
                    foreach ($event_items as $eve) {
                        $expld = explode('_', $eve->events_toname);
                        if ($expld[0] == 'ClassEvents' && $l == 0):
                            $eve_cnt++;
                            $l = 1;
                        endif;
                    }
                }
            }
        }
        $array['id'] = 4;
        $array['Title'] = "Class Events";
        $array['Description'] = $eve_cnt ? $eve_cnt . ' Event(s)' : ' No Event(s)';
        $array['icon'] = "ion-ios-calendar-outline";
        $array['dangertext'] = "";
        $array['unread'] = $event_unread ? '(' . $event_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/classtimeline/myclass/classevents";
        $totalarray['columns'][] = $array;

        $total['col1'] = $totalarray;
        $cnt = $exm_unread + $test_unread + $circular_unread + $event_unread;
        $total['col2'] = $cnt > 0 ? '(' . $cnt . ')' : '';

        print_r(json_encode($total));
        exit;
        /* } else {
          $totalarray = $array = array();
          $array['id'] = 1;
          $array['Title'] = "Time Table";
          $array['Description'] = "Last revised - never.";
          $array['icon'] = "ion-ios-timer-outline";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = "";
          $totalarray['columns'][] = $array;
          $array['id'] = 2;
          $array['Title'] = "Special Classes";
          $array['Description'] = "2 new special classes announced.";
          $array['icon'] = "ion-ios-book-outline";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = "";
          $totalarray['columns'][] = $array;
          $array['id'] = 3;
          $array['Title'] = "Exams";
          $array['Description'] = "Semester exams scheduled.";
          $array['icon'] = "ion-ribbon-a";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = "";
          $totalarray['columns'][] = $array;
          $array['id'] = 4;
          $array['Title'] = "Circular - Class Teacher";
          $array['Description'] = "New faculty for chemistry assigned.";
          $array['icon'] = "ion-ios-bell-outline";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = "";
          $totalarray['columns'][] = $array;
          $array['id'] = 5;
          $array['Title'] = "Subjects";
          $array['Description'] = "2 new content updations.";
          $array['icon'] = "ion-ios-list";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = "";
          $totalarray['columns'][] = $array;
          $array['id'] = 6;
          $array['Title'] = "Reminders";
          $array['Description'] = "Emily celebrates her birthday tomorrow.";
          $array['icon'] = "ion-ios-calendar-outline";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = "";
          $totalarray['columns'][] = $array;
          $array['id'] = 7;
          $array['Title'] = "Registrations";
          $array['Description'] = "Registrations open for exchange program.";
          $array['icon'] = "ion-ios-personadd";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = "";
          $totalarray['columns'][] = $array;
          $total['col1'] = $totalarray;
          print_r(json_encode($total));
          exit;
          } */
    }

    public function mySchoolViewAction() {
        $device_token = $this->request->get('device_token');
        $device_type = $this->request->get('device_type');
        $uuid = $this->request->get('uuid');
        $user_type = $this->request->get('user_type');
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        // if ($user_type != 'common') {
        $circular_unread = $event_unread = 0;
        $notify = array();
        $notify['aggregate_key'] = SUBDOMAIN;
        $notify['targetor_id'] = $loginid;
        $notify['device_token'] = $device_token;
        $notify['device_type'] = $device_type;
        $notify['uuid'] = $uuid;
        $notify['category'] = 'Myschool';
        $notification_data = json_encode($notify);
        $responseParam = IndexController::curlIt(COMMAPI . 'GetNotificationAggregate.php', $notification_data);
        $content = json_decode($responseParam);
        if (count($content) > 0) {
            foreach ($content as $unread) {
                if (strtolower($unread->title) == 'circular') {
                    $circular_unread = $unread->count;
                }if (strtolower($unread->title) == 'schoolevents') {
                    $event_unread = $unread->count;
                }
            }
        }
        $totalarray = array();
        $announcment = array();
        $announcements = Announcement::find();
        if (count($announcements) > 0) {
            $ann_cnt = 0;
            foreach ($announcements as $announc) {
                $l = 0;
                $ann_items = AnnouncementTolist::find('announcement_id=' . $announc->id . ' and to="' . $loginid . '"');
                if (count($ann_items) > 0) {
                    foreach ($ann_items as $a) {
                        $expld = explode('_', $a->to_name);
                        if ($expld[0] == 'Circular' && $l == 0):
                            $ann_cnt++;
                            $l = 1;
                        endif;
                    }
                }
            }
        }
        $array['id'] = 1;
        $array['Title'] = "Circular";
        $array['Description'] = $ann_cnt > 0 ? $ann_cnt . " Circular(s)" : 'No Circular(s)';
        $array['icon'] = "ion-ios-bell-outline";
        $array['dangertext'] = "";
        $array['unread'] = $circular_unread ? '(' . $circular_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/schooltimeline/myschool/Circular";
        $totalarray['columns'][] = $array;

        $events = Events::find();
        if (count($events) > 0) {
            $eve_cnt = 0;
            foreach ($events as $event) {
                $l = 0;
                $event_items = EventsList::find('event_id=' . $event->id . ' and events_to="' . $loginid . '"');
                if (count($event_items) > 0) {
                    foreach ($event_items as $eve) {
                        $expld = explode('_', $eve->events_toname);
                        if ($expld[0] == 'SchoolEvents' && $l == 0):
                            $eve_cnt++;
                            $l = 1;
                        endif;
                    }
                }
            }
        }
        $array['id'] = 2;
        $array['Title'] = "School Events";
        $array['Description'] = $eve_cnt > 0 ? $eve_cnt . ' Event(s)' : ' No Event(s)';
        $array['icon'] = "ion-ios-calendar-outline";
        $array['dangertext'] = "";
        $array['unread'] = $event_unread ? '(' . $event_unread . ')' : '';
        $array['href'] = "#/myapp/maintab/home/schooltimeline/myschool/schoolevents";
        $totalarray['columns'][] = $array;

        $array['id'] = 3;
        $array['Title'] = "Library";
        $array['Description'] = 'Coming Soon...';
        $array['icon'] = "ion-ios-book";
        $array['dangertext'] = "";
        $array['unread'] = '';
        $array['href'] = "";
        $totalarray['columns'][] = $array;
        $array['id'] = 4;
        $array['Title'] = "Transport";
        $array['Description'] = 'Coming Soon..';
        $array['icon'] = "ion-android-car";
        $array['dangertext'] = "";
        $array['unread'] = '';
        $array['href'] = "";
        $totalarray['columns'][] = $array;

        $total['col1'] = $totalarray;
        $cnt = $circular_unread + $event_unread;
        $total['col2'] = $cnt > 0 ? '(' . $cnt . ')' : '';
        print_r(json_encode($total));
        exit;
        /* } else {
          $array['id'] = 1;
          $array['Title'] = "Circular - Principal";
          $array['Description'] = 'Sports day preparations to start from  01/01/16.';
          $array['icon'] = "ion-ios-bell-outline";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = "";
          $totalarray['columns'][] = $array;
          $array['id'] = 2;
          $array['Title'] = "Circular - Library";
          $array['Description'] = 'New books added in reference section - Encyclopedia of Anatomy,History Of...';
          $array['icon'] = "ion-ios-bell-outline";
          $array['dangertext'] = "";
          $array['unread'] = '';
          $array['href'] = "";
          $totalarray['columns'][] = $array;
          $total['col1'] = $totalarray;
          print_r(json_encode($total));
          exit;
          } */
    }

    public function eventAcceptanceAction() {
        $loginid = $this->request->get('loginid');
        $eventItem = $this->request->get('event_id');
        $eventItems = EventsList::findFirstById($eventItem);
        $eventItems->status = 'Accepted';
        $eventItems->modified_date = time();
        if ($eventItems->save()) {
            $ann_items = AnnouncementTolist::find('event_id=' . $eventItems->event_id);
            $allloginIds = $ackldgid = array();
            foreach ($ann_items as $items) {
                $allloginIds[] = $items->to;
                if ($items->status == 'Acknowledge'):
                    $ackldgid[] = $items->to;
                endif;
            }
            $valu1 = array_unique($allloginIds);
            $valu2 = array_unique($ackldgid);
            $pending = array_diff($valu1, $valu2);
            $event = Events::findFirstById($eventItems->event_id);
            if (count($pending) == 0) {
                $event->status = 'Accepted';
            }
            $event->modified_date = time();
            if ($event->save()) {
                $message['status'] = "success";
            } else {
                $message['status'] = "error";
            }
            print_r(json_encode($message));
            exit;
        }
    }

    public function profileViewAction() {
        $loginid = $this->request->get('loginid');
        $studinfo = StudentInfo::findFirst('loginid =' . "'$loginid'");
        $stulist_mapping_list = StudentMapping::findFirst('student_info_id =' . $studinfo->id);
        $stu_mapdets = ($stulist_mapping_list->aggregate_key) ? explode(',', $stulist_mapping_list->aggregate_key) : '';
        $maping_values = '';
        if ($stu_mapdets != '') {
            foreach ($stu_mapdets as $stu_mapdet) {
                $subdiv = array();
                $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stu_mapdet);
                $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                $subdiv['head'] = $orgnztn_str_mas_det->name;
                $subdiv['row'] = $orgnztn_str_det->name;
                $sub[] = $subdiv;
                if ($orgnztn_str_mas_det->module != "StudentCycleNode" && $orgnztn_str_mas_det->mandatory_for_admission == 1) {
                    $subhed[] = $orgnztn_str_det->name;
                }
            }
        }
        $personal['headsubtext'] = implode(' ', $subhed);
        $personal['Name'] = $studinfo->Student_Name ? $studinfo->Student_Name : '-';
        $personal['Admission_No'] = $studinfo->Admission_no ? strtoupper($studinfo->Admission_no) : '-';
        $personal['Application_No'] = $studinfo->application_no ? $studinfo->application_no : '-';
        $personal['Dob'] = $studinfo->Date_of_Birth ? date('d-m-Y', $studinfo->Date_of_Birth) : '-';
        $personal['Gender'] = ($studinfo->Gender == 1) ? 'Female' : 'Male';
        $personal['Date_of_Joining'] = $studinfo->Date_of_Joining ? date('d-m-Y', $studinfo->Date_of_Joining) : '-';
        $personal['Address1'] = $studinfo->Address1 ? $studinfo->Address1 : '-';
        $personal['Address2'] = $studinfo->Address2 ? $studinfo->Address2 : '-';
        $personal['State'] = $studinfo->State ? $studinfo->State : '-';
        $personal['Email'] = $studinfo->Email ? $studinfo->Email : '-';
        $personal['Country'] = $studinfo->Country ? CountryMaster::findFirstByCountryCode($studinfo->Country)->country_name : '-';
        $personal['Pin'] = $studinfo->Pin ? $studinfo->Pin : '-';
        $personal['Phone'] = $studinfo->Phone ? $studinfo->Phone : '-';
        $personal['Place_of_birth'] = $studinfo->place_of_birth ? $studinfo->place_of_birth : '-';
        $personal['Nationality'] = $studinfo->nationality ? CountryMaster::findFirstByCountryCode($studinfo->nationality)->country_name : '-';
        $personal['Caste'] = $studinfo->caste ? $studinfo->caste : '-';
        $personal['Caste_category'] = $studinfo->caste_category ? $studinfo->caste_category : '-';
        $personal['Religion'] = $studinfo->religion ? $studinfo->religion : '-';
        $personal['First_language'] = $studinfo->first_language ? $studinfo->first_language : '-';
        $personal['Previous_School_Name'] = $studinfo->previous_school_name ? $studinfo->previous_school_name : '-';
        $personal['pre_State'] = $studinfo->previous_school_state ? $studinfo->previous_school_state : '-';
        $personal['pre_Country'] = $studinfo->previous_school_country ? CountryMaster::findFirstByCountryCode($studinfo->previous_school_country)->country_name : '-';
        $personal['Attended_from'] = $studinfo->attended_from ? date('d-m-Y', $studinfo->attended_from) : '-';
        $personal['Attended_till'] = $studinfo->attended_from ? date('d-m-Y', $studinfo->attended_to) : '-';
        $personal['Comments'] = $studinfo->comments ? $studinfo->comments : '-';
        $personal['Achievements'] = $studinfo->achievements ? $studinfo->achievements : '-';
        $personal['Other_details'] = $studinfo->other_details ? $studinfo->other_details : '-';
        $personal['Acdemic_det'] = $sub ? $sub : '-';
        $personal['father_name'] = $studinfo->father_name ? $studinfo->father_name : '-';
        $personal['other_guardian_name'] = $studinfo->other_guardian_name ? $studinfo->other_guardian_name : '-';
        $personal['f_occupation'] = $studinfo->f_occupation ? $studinfo->f_occupation : '-';
        $personal['g_occupation'] = $studinfo->g_occupation ? $studinfo->g_occupation : '-';
        $personal['f_designation'] = $studinfo->f_designation ? $studinfo->f_designation : '-';
        $personal['g_designation'] = $studinfo->g_designation ? $studinfo->g_designation : '-';
        $personal['f_phone_no'] = $studinfo->f_phone_no ? $studinfo->f_phone_no : '-';
        $personal['g_phone'] = $studinfo->g_phone ? $studinfo->g_phone : '-';
        $personal['f_phone_no_status'] = ($studinfo->f_phone_no_status == 1) ? 'Primary' : 'Secondary';
        $personal['g_phone_no_status'] = ($studinfo->g_phone_no_status == 1) ? 'Primary' : 'Secondary';
        $personal['mother_name'] = $studinfo->mother_name ? $studinfo->mother_name : '-';
        $personal['sibling'] = $studinfo->sibling ? $studinfo->sibling : '-';
        $personal['m_occupation'] = $studinfo->m_occupation ? $studinfo->m_occupation : '-';
        $personal['person_school_fee'] = $studinfo->person_school_fee ? $studinfo->person_school_fee : '-';
        $personal['m_designation'] = $studinfo->m_designation ? $studinfo->m_designation : '-';
        $personal['circumtances'] = $studinfo->circumtances ? $studinfo->circumtances : '-';
        $personal['m_phone_no'] = $studinfo->m_phone_no ? $studinfo->m_phone_no : '-';
        $personal['family_income'] = $studinfo->family_income ? $studinfo->family_income : '-';
        $personal['m_phone_no_status'] = ($studinfo->m_phone_no_status == 1) ? 'Primary' : 'Secondary';
        $personal['blood_group'] = $studinfo->blood_group ? $studinfo->blood_group : '-';
        $personal['chronic'] = $studinfo->chronic ? $studinfo->chronic : '-';
        $personal['height'] = $studinfo->height ? $studinfo->height : '-';
        $personal['allergic'] = $studinfo->allergic ? $studinfo->allergic : '-';
        $personal['weight'] = $studinfo->weight ? $studinfo->weight : '-';
        $personal['family_doctor'] = $studinfo->family_doctor ? $studinfo->family_doctor : '-';
        $personal['transport'] = ($studinfo->transport && $studinfo->transport == 2 ) ? 'Available' : (($studinfo->transport == 1) ? 'No' : '-');
        if ($studinfo->photo) {
            $photo_url = FILES_URI . $studinfo->photo;
        } else if ($studinfo->Gender == '1') {
            $photo_url = "images/girl_user.png";
        } else if ($studinfo->Gender == '2') {
            $photo_url = "images/boy_user.png";
        } else {
            $photo_url = "images/User.png";
        }
        $personal['photourl'] = $this->url->get($photo_url);
        print_r(json_encode($personal));
        exit;
    }

    /* public function getAvailableProfilesAction(){
      $loginid = $this->request->get('loginid');
      $studinfo = StudentInfo::findFirst('loginid =' . "'$loginid'");
      $Phone = $studinfo->Phone ? $studinfo->Phone : '-';
      if($Phone  != '-'){
      $studPhoinfo = StudentInfo::find('Phone =' . "'$Phone'");
      if(count($studPhoinfo)>0){
      foreach($studPhoinfo as $profile){
      $stulist_mapping_list = StudentMapping::findFirst('student_info_id =' . $profile->id);
      $stu_mapdets = ($stulist_mapping_list->aggregate_key) ? explode(',', $stulist_mapping_list->aggregate_key) : '';
      $maping_values = '';
      if ($stu_mapdets != '') {$sub= array();
      foreach ($stu_mapdets as $stu_mapdet) {
      $subdiv = array();
      $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stu_mapdet);
      $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
      $subdiv['head'] = $orgnztn_str_mas_det->name;
      $subdiv['row'] = $orgnztn_str_det->name;
      $sub[] = $subdiv;
      }
      }$list= array();

      $list['id'] = $profile->id;
      $list['name'] = $profile->Student_Name;
      $list['loginid'] = $profile->loginid;
      $list['acddet']  = $sub ? $sub : '-';
      $lists[] = $list;
      }  $message['type'] = 'success';
      $message['message'] = $lists;
      print_r(json_encode($message));
      exit;
      } else {
      $message['type'] = 'error';
      $message['message'] = 'Profile not found';
      print_r(json_encode($message));
      exit;
      }

      }

      } */

    public function getAvailableProfilesAction() {

        $getpostDta = $this->request->getJsonRawBody();
        $loginid = $getpostDta->login;
        $profile = StudentInfo::findFirst('loginid =' . "'$loginid'");
        if ($profile) {
            $stulist_mapping_list = StudentMapping::findFirst('student_info_id =' . $profile->id);
            $stu_mapdets = ($stulist_mapping_list->aggregate_key) ? explode(',', $stulist_mapping_list->aggregate_key) : '';
            $maping_values = '';
            if ($stu_mapdets != '') {
                $sub = array();
                $sub[] = array('head' => 'Student Name', 'row' => $profile->Student_Name);
                foreach ($stu_mapdets as $stu_mapdet) {
                    $subdiv = array();
                    $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stu_mapdet);
                    $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                    $subdiv['head'] = $orgnztn_str_mas_det->name;
                    $subdiv['row'] = $orgnztn_str_det->name;
                    $sub[] = $subdiv;

                    if ($orgnztn_str_mas_det->module != "StudentCycleNode" && $orgnztn_str_mas_det->mandatory_for_admission == 1) {
                        $subhed[] = $orgnztn_str_det->name;
                    }
                }
            }
            $schoolvar = ControllerBase::_getSchoolVariables();
            $list = array();
            $list['id'] = $profile->id;
            $list['name'] = $profile->Student_Name;
            $list['loginid'] = $profile->loginid;
            $list['school'] = $schoolvar['school_name'];
            $list['img'] = $profile->photo ? $this->url->get(FILES_URI . $profile->photo) : $this->url->get('images/user.jpg');
            $list['acddet'] = $sub ? $sub : '-';
            $list['headsubtext'] = implode(' ', $subhed);
            $message['type'] = 'success';
            $message['message'] = $list;
            print_r(json_encode($message));
            exit;
        } else {
            $message['type'] = 'error';
            $message['message'] = 'Profile not found';
            print_r(json_encode($message));
            exit;
        }
    }

    public function mainexamViewAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $actid = $this->request->get('activity');
        if ($actid) {
            $mainexamsdets = Mainexam::findFirstById($actid);
            $result = array();
            $columns['name'] = $mainexamsdets->exam_name . ' (' . $mainexamsdets->examCode . ' )';
            $columns['date'] = date('Y-m-d H:i:s', $mainexamsdets->mark_record_start_date);
            $result[] = $columns;
        } else {
            $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
            $aggregate_key = StudentMapping::findFirst('student_info_id =' . $stud_info->id)->aggregate_key;
            $get_Query_For_Exmmak = ControllerBase::buildExamQuery($aggregate_key);
            $mainexamsdets = Mainexam::find(implode(' or ', $get_Query_For_Exmmak) . ' ORDER BY modified_date DESC');
            $result = array();
            if (count($mainexamsdets) > 0) {
                foreach ($mainexamsdets as $exams) {
                    $columns['name'] = $exams->exam_name . ' (' . $exams->examCode . ' )';
                    $columns['date'] = date('Y-m-d H:i:s', $exams->mark_record_start_date);
                    $result[] = $columns;
                }
            }
        }
        print_r(json_encode($result));
        exit;
    }

    public function classTestViewAction() {
        $loginid = (SUBDOMAIN != "common") ? $this->request->get('loginid') : (StudentInfo::findFirst()->loginid);
        $actid = $this->request->get('activity');
        if ($actid) {
            $result = $columns = array();
            $classtests = ClassTest::findFirst('class_test_id=' . $actid);
            $columns['subject'] = OrganizationalStructureValues::findFirstById($classtests->subjct_modules)->name;
            $arr['name'] = $classtests->class_test_name;
            $arr['date'] = date('Y-m-d H:i:s', ($classtests->modified_date ? $classtests->modified_date : $classtests->created_date));
            $columns['details'][] = $arr;
            $result[] = $columns;
        } else {
            $stud_info = StudentInfo::findFirst('loginid =' . "'$loginid'");
            $aggregate_key = StudentMapping::findFirst('student_info_id =' . $stud_info->id)->aggregate_key;
            $aggregateids = explode(',', $aggregate_key);
            $subjids = ControllerBase::getGrpSubjMasPossiblities($aggregateids);
            $subjectsid = GroupSubjectsTeachers::find('id IN (' . implode(',', $subjids) . ')');
            $result = array();
            if (count($subjectsid) > 0) {
                foreach ($subjectsid as $svalue) {
                    $subj_Ids[] = $svalue->subject_id;
                }
                $subj_Ids = array_unique($subj_Ids);
                foreach ($subj_Ids as $value) {
                    $queryParams = $subjagg = $columns = $arr = array();
                    $subjagg = ControllerBase::getAllSubjectAndSubModules(array($value));
                    if ($subjagg && count($subjagg) > 0) {
                        $queryParams[] = 'subjct_modules IN(' . implode(',', $subjagg) . ')';
                    }
                    if (count($subjids) > 0) {
                        $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
                    }
                    $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
                    $classtests = ClassTest::find($conditionvals);
                    if (count($classtests) > 0) {
                        $columns['subject'] = OrganizationalStructureValues::findFirstById($value)->name;
                        foreach ($classtests as $tests) {
                            $arr['name'] = $tests->class_test_name;
                            $arr['date'] = date('Y-m-d H:i:s', ($tests->modified_date ? $tests->modified_date : $tests->created_date));
                            $columns['details'][] = $arr;
                        }
                        $result[] = $columns;
                    }
                }
            }
        }
        print_r(json_encode($result));
        exit;
    }

}
