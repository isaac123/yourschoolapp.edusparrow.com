<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class ClassroomAdminController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Class Room Admin| ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs('js/jquery-ui.js');
//        $this->assets->addJs("js/attendancesettings/settings.js");
//        $this->assets->addJs("js/classroomadmin/classroomadmin.js");
//        $this->assets->addJs('js/classroom/classroom.js');
//        $this->assets->addJs("js/exam/exam.js");
//        $this->assets->addJs("js/mark/mark.js");
//        $this->assets->addJs("js/rating/rating.js");
    }

}
