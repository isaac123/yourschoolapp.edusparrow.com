<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class LedgerController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    //Add New Ledger
    public function indexAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Ledger | ");
        $this->assets->addCss('css/edustyle.css');
        // $this->assets->addJs('js/ledger/ledger.js');
        $this->view->form = new LedgerAddForm();
        $rootid = LedgerMaster::findFirstByParentid(0)->lid;
        $reslt = $this->find_childledger($rootid, '');
        $output.=$reslt;
        $this->view->ledgerlist = $output;
    }

    public function find_childledger($aid, $choutput) {
        $exist = LedgerMaster::find('parentid =' . $aid);
        $attribute = 'display:none';
        if (count($exist) > 0) {
            $choutput.='<div class="tree-folder" style="display:block;"><div class="tree-folder-header">'
                    . '<i class="fa fa-folder"></i>' . $this->getledgernamefor($aid) . '</div>'
                    . '<div class="tree-folder-content" style="' . $attribute . '">';
            foreach ($exist as $chl) {
                $choutput = $this->find_childledger($chl->lid, $choutput);
            }
            $choutput.='</div></div>';
        } else {
            $choutput.='<div class="tree-item"><i class="tree-dot"></i><div class="tree-item-name">' . $this->getledgernamefor($aid) . '</div></div>';
        }


        return $choutput;
    }

    public function getledgernamefor($ledgerid) {
        $ledgname = OrganizationalStructureValues::findFirstById($ledgerid)->name;
        if ($ledgname != '') {
            return $ledgname;
        }
    }

    public function saveNewLedgerAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $messages = array();
        $identity = $this->auth->getIdentity();
        $usrnm = $identity['name'];
        try {
            if ($this->request->isPost()) {
                $parentledgId = $this->request->getPost('lid');
                $parentledgtyp = LedgerMaster::findFirstByLid($parentledgId)->type;
                $ledgername = $this->request->getPost('ledgername');
//                echo 'ledgername =' . "'" . $ledgername . "' and parentid =". $parentledgId;exit;
                $ledgermaster = (LedgerMaster::findFirst('ledgername =' . "'" . $ledgername . "' and parentid =" . $parentledgId)) ?
                        LedgerMaster::findFirst('ledgername =' . "'" . $ledgername . "' and parentid =" . $parentledgId) : new LedgerMaster();
                $ledgermaster->balance = 0;
                $ledgermaster->type = $parentledgtyp;
                $ledgermaster->created_by = $usrnm;
                $ledgermaster->created_date = time();
                $ledgermaster->parentid = $parentledgId;
                $ledgermaster->ledgername = $ledgername;
                if ($ledgermaster->save()) {
                    $messages['type'] = 'success';
                    $messages['message'] = 'Ledger Saved Successfully';
                    print_r(json_encode($messages));
                    exit;
                } else {
                    $error = '';
                    foreach ($ledgermaster->getMessages() as $message) {
                        $error .= $message;
                    }
                    $messages['type'] = 'error';
                    $messages['message'] = $error;
                    print_r(json_encode($messages));
                    exit;
                }
            }
        } catch (Exception $e) {

            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function saveNewLedgerOld($param) {
        try {
            $identity = $this->auth->getIdentity();
            $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
            $parentledgId = $param['lid'];
            $parentledgtyp = LedgerMaster::findFirstByLid($parentledgId)->type;
            $ledgername = $param['ledgername'];
//                echo 'ledgername =' . "'" . $ledgername . "' and parentid =". $parentledgId;exit;  
            $ledgermaster = (LedgerMaster::findFirst('ledgername =' . "'" . $ledgername . "' and parentid =" . $parentledgId)) ?
                    LedgerMaster::findFirst('ledgername =' . "'" . $ledgername . "' and parentid =" . $parentledgId) : new LedgerMaster();
            $ledgermaster->balance = 0;
            $ledgermaster->type = $parentledgtyp;
            $ledgermaster->created_by = $uid;
            $ledgermaster->created_date = time();
            $ledgermaster->parentid = $parentledgId;
            $ledgermaster->ledgername = $ledgername;
            if ($ledgermaster->save()) {
                return $ledgermaster->lid;
            } else {
                $error = '';
                foreach ($ledgermaster->getMessages() as $message) {
                    $error .= $message;
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function saveNewLedger($param) {
        try {
            $masterid = OrganizationalStructureMaster::find('module="Ledger"');
            foreach ($masterid as $value) {
                $orgids[] = $value->id;
            }
            $identity = $this->auth->getIdentity();
            $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
            $parentledgId = $param['lid'];
            $ledgername = $param['ledgername'];
            $orgValues = (OrganizationalStructureValues::findFirst('name =' . "'" . $ledgername . "'"
                            . " and org_master_id IN (" . implode(',', $orgids) . ")"
                            . " and parent_id =" . $parentledgId)) ?
                    OrganizationalStructureValues::findFirst('name =' . "'" . $ledgername . "'"
                            . " and org_master_id IN (" . implode(',', $orgids) . ")"
                            . " and parent_id =" . $parentledgId) : new OrganizationalStructureValues();
            $pareledg = OrganizationalStructureValues::findFirstById($parentledgId);
            $pareMasledg = OrganizationalStructureMaster::findFirstByParentId($pareledg->org_master_id);
            $orgValues->org_master_id = $pareMasledg->id;
            $orgValues->parent_id = $parentledgId;
            $orgValues->name = $ledgername;
            $orgValues->status = $pareledg->status;
            $orgValues->orgname = $pareMasledg->name;
            $orgValues->nodecolor = $pareMasledg->color ? $pareMasledg->color : ( '#a9d86e');
            $orgValues->nodefont = $pareMasledg->nodefont ? $pareMasledg->nodefont : ('bank');

            if ($orgValues->save()) {
                return $orgValues->id;
            } else {
                $error = '';
                foreach ($orgValues->getMessages() as $message) {
                    $error .= $message . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message . '<br>';
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function transactionLedgerDetsAction() {
        if ($this->request->isGet()) {
            $this->view->voucherid = $voucherid = $this->request->get('voucherid');
            $this->view->date = $date = $this->request->get('date') ? $date = $this->request->get('date') : '';
            $this->view->frmdate = $frmdate = $this->request->get('frmdate') ? $this->request->get('frmdate') : '';
            $this->view->todate = $todate = $this->request->get('todate') ? $this->request->get('todate') : '';
//            $this->view->end_date = $date?strtotime($date):strtotime($todate);
//            $this->view->ledgerlist = '';
        }
    }

    public function loadtransactionLedgerDetsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->voucherid = $voucherid = $this->request->getPost('ledgerid_det');
            $this->view->date = $date = $this->request->getPost('voucherdate');
            $this->view->frmdate = $frmdate = $this->request->get('fromdate') ? $this->request->get('fromdate') : '';
            $this->view->todate = $todate = $this->request->get('todate') ? $this->request->get('todate') : '';
//            print_r($this->request->get());
//            echo $date.' '.$frmdate.' '.$todate;exit;
            if ($date != '') {
                $start_date = '';
                $end_date = strtotime($date . " 23:59:59");
            } else {
                $start_date = strtotime($frmdate . " 00:00:00");
                $end_date = strtotime($todate . " 23:59:59");
            }
            $reslt = LedgerController::_getLedgeramtHtml($start_date, $end_date, $voucherid, '');

            $output.=$reslt;
            $this->view->ledgerlist = $output;
        }
    }

    public function _getLedgeramtHtml($start_date, $end_date, $aid, $htmlchoutput) {
        $typeled = LedgerMaster::findFirst('lid = ' . $aid);
        if ($start_date == '') {
            $amount = ControllerBase::_getBalnceForDate($end_date, $aid);
            $url = $this->url->get('ledger/viewtransactionLedgerDets') . '?ledger_id=' . $aid . '&&date=' . (date('d-m-Y', $end_date));
        } else {
            $amount = ControllerBase::_getOpeningBalnceFromTo($start_date, $end_date, $aid);
            $url = $this->url->get('ledger/viewtransactionLedgerDets') . '?ledger_id=' . $aid
                    . '&&frmdate=' . (date('d-m-Y', $start_date)) . '&&todate=' . ( date('d-m-Y', $end_date) );
        }
        $chld = LedgerMaster::find('parentid = ' . $aid);
        $attribute = 'display:none';
        if (count($chld) > 0) {
            $htmlchoutput.='<div class="tree-folder" style="display:block;"><div class="tree-folder-header"><i class="fa fa-folder"></i>'
                    . '<a target="_blank" href="' . $url . '" > '
                    . '' . $typeled->ledgername . '(' . $amount . ')' . ' </a></div><div class="tree-folder-content" style="' . $attribute . '">';
            foreach ($chld as $chl) {
                $htmlchoutput = LedgerController::_getLedgeramtHtml($start_date, $end_date, $chl->lid, $htmlchoutput);
            }
            $htmlchoutput.='</div></div>';
        } else {
            $htmlchoutput.='<div class="tree-item"><i class="tree-dot"></i><div class="tree-item-name">'
                    . '<a target="_blank" href="' . $url . '" > '
                    . '' . $typeled->ledgername . '(' . $amount . ')' . '</a></div></div>';
        }

        return $htmlchoutput;
    }

    public function viewtransactionLedgerDetsAction() {
        if ($this->request->isGet()) {
            $this->view->ledger_id = $ledger_id = $this->request->get('ledger_id');
            $this->view->date = $date = $this->request->get('date');
            $this->view->frmdate = $frmdate = $this->request->get('frmdate') ? $this->request->get('frmdate') : '';
            $this->view->todate = $todate = $this->request->get('todate') ? $this->request->get('todate') : '';
        }
    }

    public function loadTransHeaderAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->ledger_id = $ledger_id = $this->request->getPost('ledgerid_det');
        $this->view->ledger = LedgerMaster::findFirstByLid($ledger_id);
        $this->view->trnsdate = $trnsdate = ($this->request->getPost('trnsdate'));
          $this->view->narration = $narration = ($this->request->getPost('narration'));
        if ($trnsdate != '') {
            $this->view->startdate = '';
            $this->view->enddate = $trnsdate;
        } else {
            $this->view->startdate = $startdate = $this->request->get('fromdate') ? $this->request->get('fromdate') : '';
            $this->view->enddate = $todate = $this->request->get('todate') ? $this->request->get('todate') : '';
        }
    }

    public function loadviewtransactionLedgerDetsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $this->view->ledger_id = $ledger_id = $this->request->getPost('ledgerid_det');
            $this->view->trnsdate = $trnsdate = ($this->request->getPost('trnsdate'));
            $this->view->frmdate = $frmdate = $this->request->get('fromdate') ? ($this->request->get('fromdate')) : '';
            $this->view->todate = $todate = $this->request->get('todate') ? ($this->request->get('todate')) : '';

            if ($trnsdate != '') {
                $start_date = '';
                $end_date = strtotime($trnsdate . " 23:59:59");
            } else {
                $start_date = strtotime($frmdate . " 00:00:00");
                $end_date = strtotime($todate . " 23:59:59");
            }
            foreach ($this->request->getPost() as $key => $value) {
                $params[$key] = $value;
            }
            $transtotal = ControllerBase::_getLegerTransacTotal($start_date, $end_date, $ledger_id);
            $restrans = ControllerBase::_getLegerTransac($start_date, $end_date, $ledger_id);
            if ($restrans) {
                $tableData = array(
                    "sEcho" => intval($this->request->getPost('sEcho')),
                    "iTotalRecords" => count($restrans['data']),
                    "iTotalDisplayRecords" => intval($transtotal),
                    "aaData" => $restrans['data'],
                    "iDataHead" => $restrans['header'],
                );
            } else {
                $tableData = array(
                    "sEcho" => intval($this->request->getPost('sEcho')),
                    "iTotalRecords" => 0,
                    "iTotalDisplayRecords" => 0,
                    "aaData" => []
                );
                echo json_encode($tableData);
                exit;
            }
        }
    }

    public function loadInitialBalanceAction() {

        $this->tag->prependTitle("Initial Balance | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/ledger/ledger.js');
        $fields = OrganizationalStructureMaster::findFirst('cycle_node =1 and module LIKE "Ledger"');
        $master = OrganizationalStructureValues::findFirst('org_master_id =' . $fields->id . ' and status = "C" ');
        $reslt = $this->find_childledgeredit($master->id, '');
        $output.=$reslt;
        $this->view->ledgerlist = $output;
    }

    public function pushLedgertoOrgMasterAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $cledgers = LedgerMaster::find('parentid=1');
        $arraylevl = array(32, 33, 34, 35, 36);
        $rootid = 2538;
        $c = array();
        foreach ($cledgers as $ledger) {
            $name = $ledger->ledgername;
            $orgid = $arraylevl[count($c)];
            $parentid = $rootid;
            $orgnew = new OrganizationalStructureValues();
            $orgnew->name = $name;
            $orgnew->org_master_id = $orgid;
            $orgnew->parent_id = $parentid;
            if (!$orgnew->save()) {
                echo 'Failed!<pre>';
                print_r($orgnew->getMessages());
                exit;
            }
            $nextchild = LedgerMaster::find('parentid=' . $ledger->lid);
            if (count($nextchild) > 0) {

                foreach ($nextchild as $nxtledger) {
                    $this->pushRecursiveledgers($nxtledger);
                }
            }
        }
    }

    public function pushRecursiveledgers($ledger, $c = array(), $newparent) {

        $arraylevl = array(32, 33, 34, 35, 36);
        $nextchild = LedgerMaster::find('parentid=' . $ledger->lid);
        if (count($nextchild) > 0) {
            foreach ($nextchild as $nxtledger) {
                $name = $ledger->ledgername;
                $orgid = $arraylevl[count($c)];
                $parentid = $newparent;
                $orgnew = new OrganizationalStructureValues();
                $orgnew->name = $name;
                $orgnew->org_master_id = $orgid;
                $orgnew->parent_id = $parentid;
                if (!$orgnew->save()) {
                    echo 'Failed!<pre>';
                    print_r($orgnew->getMessages());
                    exit;
                }
                $this->pushRecursiveledgers($nxtledger);
            }
        } else {
            $name = $ledger->ledgername;
            $orgid = $arraylevl[count($c)];
            $parentid = $rootid;
            $orgnew = new OrganizationalStructureValues();
            $orgnew->name = $name;
            $orgnew->org_master_id = $orgid;
            $orgnew->parent_id = $parentid;
            if (!$orgnew->save()) {
                echo 'Failed!<pre>';
                print_r($orgnew->getMessages());
                exit;
            }
        }
    }

    public function find_childledgeredit($aid, $choutput) {
        $exist = OrganizationalStructureValues::find('parent_id =' . $aid);
        $iniblns = InitialBalance::findFirstByLid($aid);
        $crb = $iniblns->opening_credit ? $iniblns->opening_credit : '0.00';
        $drb = $iniblns->opening_debit ? $iniblns->opening_debit : '0.00';
        $attribute = 'display:none';
        if (count($exist) > 0) {
            $choutput.='<div class="tree-folder" style="display:block; margin: 15px;"><div class="tree-folder-header">'
                    . '<i class="fa fa-folder pull-left"></i><div class="col-md-6">
                        <label class="col-sm-6 control-label">' . $this->getledgernamefor($aid) . '</label>
                        <div class="col-sm-3 ">
                            <input type="text" class="form-control input-sm " value="' . $crb . '"   id="ocr_' . $aid . '" placeholder="Opening Cr ">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control input-sm  " value="' . $drb . '"   id="odr_' . $aid . '" placeholder="Opening Dr">
                        </div>
                    </div></div>'
                    . '<div class="tree-folder-content" style="' . $attribute . '">';
            foreach ($exist as $chl) {
                $choutput = $this->find_childledgeredit($chl->id, $choutput);
            }
            $choutput.='</div></div>';
        } else {
            $choutput.='<div class="tree-item" style=" margin: 15px;"><i class="tree-dot  pull-left"></i><div class="tree-item-name"><div class="col-md-6">
                        <label class="col-sm-6 control-label">' . $this->getledgernamefor($aid) . '</label>
                        <div class="col-sm-3 ">
                            <input type="text" class="form-control input-sm  " value="' . $crb . '"  name="ledger_' . $aid . '[]" id="ocr_' . $aid . '" placeholder="Opening Cr">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control input-sm  "  value="' . $drb . '"  name="ledger_' . $aid . '[]" id="odr_' . $aid . '" placeholder="Opening Dr">
                        </div>
                    </div></div></div>';
        }


        return $choutput;
    }

    public function saveinitialBalanceAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        if ($this->request->isPost()) {
            $params = $data = $message = array();
            foreach ($this->request->getPost() as $key => $value) {
                $params[$key] = $value;
            }
            if (count($params) > 0) {
                $sql = "UPDATE InitialBalance SET opening_credit='0.00', opening_debit='0.00'";
                $execupdt = $this->modelsManager->executeQuery($sql);
                if (!$execupdt) {
                    $error = '';
                    foreach ($execupdt->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
                foreach ($params as $key => $pvalue) {
                    $lid = explode('_', $key);
                    $lidbln = InitialBalance::findFirstByLid($lid[1]) ? InitialBalance::findFirstByLid($lid[1]) : new InitialBalance();
                    $lidbln->lid = $lid[1];
                    $lidbln->opening_credit = $pvalue['ocr'] ? $pvalue['ocr'] : '0.00';
                    $lidbln->opening_debit = $pvalue['odr'] ? $pvalue['odr'] : '0.00';
                    $lidbln->closing_credit = '0.00';
                    $lidbln->closing_debit = '0.00';
                    $lidbln->created_by = $uid;
                    $lidbln->created_on = time();
                    $lidbln->modified_by = $uid;
                    $lidbln->modified_on = time();
                    if (!$lidbln->save()) {
                        $error = '';
                        foreach ($lidbln->getMessages() as $messages) {
                            $error .= $messages . "\n";
                        }
                        $message['type'] = 'error';
                        $message['message'] = $error;
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
            $message['type'] = 'success';
            $message['message'] = 'Saved Successfully.';
            print_r(json_encode($message));
            exit;
        }
    }

    public function loadLedgerExcelReportAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        $data = json_decode($this->request->getPost('params'));
        foreach ($data as $key => $value) {
            $params[$key] = $value;
        }
        $start_date = strtotime($params['start_date'] . " 00:00:00");
        $end_date = strtotime($params['end_date'] . " 23:59:59");
        $transtotal = ControllerBase::_getLegerTransacTotal($start_date, $end_date, $params['ledger_id']);
        $result = ControllerBase::_getLegerTransac($start_date, $end_date, $params['ledger_id']);
        $report = $result['header'];
        $header = array();
        $total = array();
        $transction = array();
        $ledger = array();
        $header[] = 'Transaction ID';
        $header[] = 'Date';
        $header[] = 'Voucher';
        $header[] = 'Head';
        $header[] = 'Debit Amount';
        $header[] = 'Credit Amount';
        $header[] = 'Closing Balance';
        $header[] = 'Comments';
        $ledger[] = $report['ledger_name'];
        $transction[] = ' No of Transactions : ' . count($result['data']);
        $transction[] = ' Opening Balance : ' . $report['openingbalance'];
        $transction[] = ' Closing Balance : ' . $report['closingbalance'];
        $total[0] = $total[1] = $total[2] = '';
        $total[] = ' Total';
        $total[] = $report['totaldamount'];
        $total[] = $report['totalcamount'];
        $reportdata = array();
        $count = count($result['data']);
        $data1 = $result['data'];
        $global = 0;
        for ($i = $global; $i < $count; $i++) {
            $reportval = array();
            $reportval[] = $data1[$i]['transid'];
            $reportval[] = $data1[$i]['date'];
            $reportval[] = $data1[$i]['voucherid'];
            $reportval[] = $data1[$i]['head'];
            $reportval[] = $data1[$i]['debitamt'];
            $reportval[] = $data1[$i]['creditamt'];
            $reportval[] = $data1[$i]['clbnc'];
            $reportval[] = $data1[$i]['details'];
            $reportdata[] = $reportval;
        }
        $filename = 'Ledgerwise_Transaction_Report_' . date('d-m-Y') . '.csv';
        $counter = 0;
        $unlink_file = DOWNLOAD_DIR . 'Ledgerwise_Transaction_Report_' . date('d-m-Y') . '.csv';
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }
        $fp = fopen(DOWNLOAD_DIR . $filename, 'a');

        $delimiter = ",";
        $emptyarr = array();
        fputcsv($fp, $ledger, $delimiter);
        fputcsv($fp, $transction, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $header, $delimiter);
        foreach ($reportdata as $reportsval) {
            fputcsv($fp, $reportsval, $delimiter);
        }
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $total, $delimiter);
        fclose($fp);
        $file = DOWNLOAD_DIR . $filename;
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function loadDaybookExcelReportAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        $data = json_decode($this->request->getPost('params'));
        foreach ($data as $key => $value) {
            $params[$key] = $value;
        }
        $start_date = $params['start_date'];
        $end_date = $params['end_date'];
        $csql = "SELECT l.* FROM LedgerVoucher l inner join Transaction t on t.voucherid = l.voucher_id
                     WHERE t.voucher_date between '$start_date' and '$end_date' ORDER BY l.date  ,  t.transactionid ASC";
        $cledgers = $this->modelsManager->executeQuery($csql);
        $header = array();
        $header[] = 'Date';
        $header[] = 'Head/Particulars';
        $header[] = 'Vch Type';
        $header[] = 'Vch No';
        $header[] = 'Debit';
        $header[] = 'Credit';
        $date[] = date('d F Y', $start_date) . ' to ' . date('d F Y', $end_date);
        $reportdata = array();
        foreach ($cledgers as $data) {
            $reportval = array();
            $reportval[] = $vocher[1];
            $reportval[] = $vocher[0];
            $reportval[] = $data1[$i]['debitamt'] == '-' ? '' : $data1[$i]['debitmoney'];
            $reportval[] = $data1[$i]['creditamt'] == '-' ? '' : $data1[$i]['creditmoney'];
            $reportdata[] = $reportval;
        }
        $filename = 'Day_Book_Report_' . date('d-m-Y') . '.csv';
        $counter = 0;
        $unlink_file = DOWNLOAD_DIR . 'Day_Book_Report_' . date('d-m-Y') . '.csv';
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }
        $fp = fopen(DOWNLOAD_DIR . $filename, 'a');

        $delimiter = ",";
        $emptyarr = array();
        fputcsv($fp, $ledger, $delimiter);
        fputcsv($fp, $date, $delimiter);
        fputcsv($fp, $transction, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $header, $delimiter);
        foreach ($reportdata as $reportsval) {
            fputcsv($fp, $reportsval, $delimiter);
        }
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $total, $delimiter);
        fclose($fp);
        $file = DOWNLOAD_DIR . $filename;
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function find_childledgerarr($aid, $choutput = array()) {
        $choutput[] = LedgerController::getledgernamefor($aid);
        $parentid = OrganizationalStructureValues::findFirstById($aid)->parent_id;
        if (!empty($parentid))
            $choutput = LedgerController::find_childledgerarr($parentid, $choutput);

        return $choutput;
    }

}
