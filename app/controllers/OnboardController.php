<?php

use Phalcon\Mvc\View;

class OnboardController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
    }

    public function indexAction() {
        $this->view->setTemplateAfter('onboard');
        $this->tag->prependTitle("Settings | ");
        $this->assets->addJs("js/appscripts/onboard/setup.js");
        $this->assets->addCss("css/appstyles/form-wizard.css");
        $this->view->form = $form = new SchoolSettingsForm();
        $this->view->svar = ControllerBase::_getSchoolVariables();
        $country = CountryMaster::find();
        $actc = $actcon = array();
        foreach ($country as $c) {
            $actc['value'] = $c->id;
            $actc['label'] = $c->country_name;
            $actcon[] = $actc;
        }
        $this->view->country = json_encode($actcon);
    }

    function addBasicInfoAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $form = new SchoolSettingsForm();
        $message = array();
        $identity = $this->auth->getIdentity();
        try {
            if ($this->request->isPost()) {
                if ($form->isValid($this->request->getPost()) == false) {
                    $error = '';
                    foreach ($this->request->getPost() as $key => $val) {
                        foreach ($form->getMessagesFor($key) as $messages) {
                            $keys[] = $key;
                            $error .= "<span class='text text-danger fade in'> " . $messages . "</span><br>";
                        }
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    $message['keys'] = json_encode($keys);
                    print_r(json_encode($message));
                    exit;
                } else {
// Check if the user has uploaded files
                    $set_variable = Settings::findFirstByVariableName('school_logo') ?
                            Settings::findFirstByVariableName('school_logo') : new Settings();
                    if ($this->request->hasFiles() == true) {
// Print the real file names and sizes
                        foreach ($this->request->getUploadedFiles() as $file) {
                            $ext = explode('/', $file->getType());
                            if ($ext[0] == 'image') {
                                $filename = time() . '_' . strtolower($file->getname());
                                $file->moveTo(FILES_DIR . $filename);
                                $set_variable->variableName = 'school_logo';
                                $set_variable->variableValue = $filename;
                                $set_variable->variableDescription = 'School Logo';

                                if (!$set_variable->save()) {
                                    $error = '';
                                    foreach ($set_variable->getMessages() as $amessage) {
                                        $error .= "<span class='text text-danger fade in'> " . $amessage->getMessage() . "</span><br>";
                                    }
                                    $message['type'] = 'error';
                                    $message['message'] = $error;
                                    print_r(json_encode($message));
                                    exit;
                                }
                            } else {
                                $message['type'] = 'error';
                                $message['message'] = "<span class='text text-danger fade in'> Invalid file extension</span>";
                                print_r(json_encode($message));
                                exit;
                            }
                        }
                    } else if ($this->request->hasFiles() == false && !$set_variable->variableValue) {
// echo 'noimage';
                        $message['type'] = 'error';
                        $message['message'] = "<span class='text text-danger fade in'> Logo is required</span>";
                        print_r(json_encode($message));
                        exit;
                    }

                    $stringLen = strlen($this->request->getPost('max_stu'));
                    $strpad = str_pad('', $stringLen, '*', STR_PAD_RIGHT);
                    $sstringLen = strlen($this->request->getPost('max_stf'));
                    $sstrpad = str_pad('', $sstringLen, '*', STR_PAD_RIGHT);
//print_r($strpad);exit;
                    $params = array();
                    $params['stupadding'] = $strpad;
                    $params['stfpadding'] = $sstrpad;
                    foreach ($this->request->getPost() as $key => $value) {
                        if (!in_array($key, array('file1', 'file'))) {
                            $params[$key] = $value;
                        }
                    }
//                    print_r($params); exit;
                    foreach ($params as $pkey => $pvalue) {
                        $set_sname_variable = Settings::findFirstByVariableName($pkey) ?
                                Settings::findFirstByVariableName($pkey) : new Settings();
                        $set_sname_variable->variableName = $pkey;
                        $set_sname_variable->variableValue = $pvalue;
                        $set_sname_variable->variableDescription = $pkey;
                        if (!$set_sname_variable->save()) {
                            $error = '';
                            foreach ($set_sname_variable->getMessages() as $amessage) {
                                $error .= "<span class='text text-danger fade in'> " . $amessage->getMessage() . "</span><br>";
                            }
                            $message['type'] = 'error';
                            $message['message'] = $error;
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                    $canvasParam['tempType'] = 'School'; //$this->request->getPost('tempType');
                    $canvasParam['tempCountry'] = 'India'; //$this->request->getPost('tempCountry');
                    $checkcanvas = OrganizationalStructureMaster::findFirst();
//                    $checkvalcanvas = OrganizationalStructureValues::findFirst();
                    if (!$checkcanvas) {
                        $res = $this->loadCanvas($canvasParam);
                        if ($res == 1) {
                            $message['type'] = 'success';
                            $message['message'] = 'Saved Successfully';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        $message['type'] = 'success';
                        $message['message'] = 'Saved Successfully';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {

            $error = '';
            foreach ($e->getMessages() as $amessage) {
                $error .= "<span class='text text-danger fade in'> " . $amessage->getMessage() . "</span><br>";
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        }
    }

    public function loadCanvas($canvasParam) {
        $temptype = $canvasParam['tempType'];
        $tempCountry = $canvasParam['tempCountry'];
        $structURl = ORGTEMPL_DIR . $temptype . '/' . $tempCountry . '/org.xml';
//        $valueURl = ORGTEMPL_DIR . $temptype . '/' . $tempCountry . '/orgVal.xml';
//        echo '<pre>';
        libxml_use_internal_errors(true);
        $sxe = simplexml_load_file($structURl);
        if (!$sxe) {
            print_r(libxml_get_errors());
        } else {
            $addedres = OrganizationalStructureController::recursiveLoadXml($sxe);
            if (!$addedres) {
                $message['type'] = 'error';
                $message['message'] = 'Problem in adding';
                print_r(json_encode($message));
                exit;
            }
        }

//        $svxe = simplexml_load_file($valueURl);
//        if (!$svxe) {
//            print_r(libxml_get_errors());
//        } else {
//            $vaddedres = OrganizationalStructureController::recursiveLoadValXml($svxe);
//            if (!$vaddedres) {
//                $message['type'] = 'error';
//                $message['message'] = 'Problem in adding';
//                print_r(json_encode($message));
//                exit;
//            }
//        }

        return 1;
    }

    public function loadDistinctCombinationsAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        echo '<pre>';
        $groupnodes = $hearderndes = array();
        $currectacdyr = ControllerBase::getCurrentAcademicYear();
        $organizationalMas = OrganizationalStructureMaster::find('mandatory_for_admission = 1');
        foreach ($organizationalMas as $mvalue) {
            $orgval = OrganizationalStructureValues::find('org_master_id = ' . $mvalue->id);
            foreach ($orgval as $ovalue) {
                if ($mvalue->cycle_node == 1) {
                    if ($ovalue->status == 'C')
                        $groupnodes[] = $ovalue->id;
//                        $hearderndes[$ovalue->id] = ControllerBase::getNameForKeysFullSearch($ovalue->id);
                } else {
                    $groupnodes[] = $ovalue->id;
                }
            }
        }

        $groupcombiStu = ControllerBase::getCommonIdsForKeys(implode(',', $groupnodes));
//        print_r($groupcombiStu);exit;
        foreach ($groupcombiStu as $fagvalue) {
            $newagg = explode('-', $fagvalue);
            $rootnode = array_shift($newagg);
            $rootname = ControllerBase::getNameForKeysFullSearch($rootnode);
            $actualvaln[$rootname[0]][] = $fagvalue;
        }
//        print_r($actualvaln);exit;
//        $hearderndesStu = $hearderndes;

        $this->view->groupcombiStu = $actualvaln;
    }

    public function sortArr($a, $b) {
        $t1 = ($a['0']);
        $t2 = ($b['0']);
        return strcmp($t2, $t1);
    }

    public function checkCountStuAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            if ($this->request->hasFiles() == true) {
                // Print the real file names and sizes
                foreach ($this->request->getUploadedFiles() as $file) {
                    $ext = explode('.', $file->getName());
                    if ($ext[1] == 'xlsx') {
                        $filename = 'Student_excel' . '_' . date('Y-m-d-h-i-s') . '.' . $ext[1];
                        $file->moveTo(UPLOAD_DIR . $filename);
                    } else {
                        $message['type'] = 'error';
                        $message['message'] = "<span class='text text-danger fade in'>Invalid file extension</span><br>";
                        print_r(json_encode($message));
                        exit;
                    }
                }
                if (file_exists(UPLOAD_DIR . $filename)) {
                    $objPHPExcel = PHPExcel_IOFactory::load(UPLOAD_DIR . $filename);
                    $maxCol = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
                    $maxRow = $objPHPExcel->getActiveSheet()->getHighestDataRow();
//               $objPHPExcel->getActiveSheet()->sortCellCollection();
                    $row = $objPHPExcel->getActiveSheet()->rangeToArray('A' . 1 . ':' . $maxCol . 1);

                    $header = ($row[0]);

                    $admissionNumPrfix = Settings::findFirstByVariableName('admission_prefix') ?
                            Settings::findFirstByVariableName('admission_prefix') : new Settings();
                    $admissionNumSufix = Settings::findFirstByVariableName('admission_sufix') ?
                            Settings::findFirstByVariableName('admission_sufix') : new Settings();
                    $prefix = $admissionNumPrfix ? $admissionNumPrfix->variableValue : '';
                    $sufix = $admissionNumSufix ? $admissionNumSufix->variableValue : '';
                    $stringLen = ControllerBase::getStudentPad('student');
                    $admissionNumFormat = $prefix . $stringLen . $sufix;

                    if ((!$admissionNumFormat)) {
                        $error.= "Admission prefix not set yet.<br>";
                    } else {

                        $asterikstr = str_replace("*", "", $admissionNumFormat, $asterikcount);
                        $formartArr = explode('*', $admissionNumFormat);
                        //print_r($formartArr);
                        $regex = '/^';
                        $i = 0;
                        foreach ($formartArr as $formatStr):
                            if ($formatStr != '') {
                                $regex .= $formatStr;
                            } else {
                                if ($i == 0)
                                    $regex .= '[0-9]{' . $asterikcount . '}';
                                $i = 1;
                            }
                        endforeach;

                        $regex .= '$/';
                    }

                    $headercheck = array("Admission number(" . $admissionNumFormat . ")",
                        "Student Name", "Gender(Female/Male)", "Date of Birth(dd/mm/yyyy)", "Date of Joining(dd/mm/yyyy)");
                    $ardiff = array_diff($header, $headercheck);
//                    print_r($header);print_r($headercheck);exit;
                    $i = 0;
                    $error = $validation_log = '';
                    $inv_dupli_row = array();
                    if (count($ardiff) == 0) {
                        $j = 0;
                        for ($i = 2; $i <= $maxRow; $i++) {
                            $j++;
                            $row = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $i . ':' . $maxCol . $i);
                            $row = $row[0];
                            $isempty = implode("", $row);
                            if ($isempty) {
                                //validation begins
                                if (($row[1] == '') || ($row[2] == '') || ($row[3] == '') || ($row[4] == '')) {
                                    $error.= "Missing Value in " . ($i) . ' row.<br>';
                                }

                                //format check
                                if ($row[0] && !preg_match($regex, $row[0])) {
                                    $error.= "Invalid  Admission Number Format in " . ($i) . ' row.<br>';
                                }
                                $admnno = StudentInfo::find("Admission_no = '" . $row[0] . "'");
                                //print_r($inv_dupli_row);echo ",".$row[0].";".in_array($row[0], $inv_dupli_row)."<br>";
                                if (count($admnno) > 0 || in_array($row[0], $inv_dupli_row)) {
                                    $error.= "Admission Number in row" . ($i) . ' already exists.<br>';
                                } else {
                                    if ($row[0])
                                        array_push($inv_dupli_row, $row[0]);
                                }
                                /* Staff Name Validation  */
                                if (!preg_match('/^[a-z][a-z ]*$/i', $row[1])) {
                                    $error.= "Invalid  Student Name in " . ($i) . ' row. (<i> Hint : Only alphabets and space are allowed!</i>)<br>';
                                }

                                /*  Gender Validation  */
                                if (!in_array(strtolower($row[2]), array('female', 'male'))) {
                                    $error.= "Invalid  Gender in " . ($i) . ' row.<br>';
                                }

                                /*  Birth Date Validation  */
                                if (!preg_match("/^([1-9]|0[1-9]|[1-2][0-9]|3[0-1])\/([1-9]|0[1-9]|1[0-2])\/[0-9]{4}$/", $row[3])) {
                                    $error.= "Invalid  Date of Birth in " . ($i) . ' row.(<i> Hint : date format is dd/mm/yyyy </i>)<br>';
                                }
                                /*  Appointment Date Validation  */
                                if (!preg_match("/^([1-9]|0[1-9]|[1-2][0-9]|3[0-1])\/([1-9]|0[1-9]|1[0-2])\/[0-9]{4}$/", $row[4])) {
                                    $error.= "Invalid  Date of Joining in " . ($i) . ' row.(<i> Hint : date format is dd/mm/yyyy </i>)<br>';
                                }

                                $birthDt = date_timestamp_get(date_create_from_format('d/m/Y', $row[3]));
                                $AppDt = date_timestamp_get(date_create_from_format('d/m/Y', $row[4]));
                                if ($birthDt > $AppDt) {
                                    $error.= "Invalid  Birth/Admission Date in " . ($i) . ' row.(<i> Hint : Birth date greater than Date of joining </i>)<br>';
                                }
                                if ($birthDt > time() || $AppDt > time()) {
                                    $error.= "Invalid  Birth/Admission Date in " . ($i) . ' row.(<i> Hint : Birth date or Date of joining is future date </i>)<br>';
                                }
                            }
                        }
//                print_r($error);exit;
                        if ($error != '') {

                            $message['type'] = 'error';
                            $message['message'] = "<span class='text text-danger fade in'>Aborted!<br>" . $error . "</span><br>";
                            print_r(json_encode($message));
                            exit;
                        }
                        if ($j > 0) {
                            $message['type'] = 'success';
                            $message['filename'] = $filename;
                            $message['message'] = "<div class='alert alert-success'>Validation Completed  !<br>Total Students [ ($j)] !</div>";
                            print_r(json_encode($message));
                            exit;
                        } else {

                            $message['type'] = 'error';
                            $message['message'] = "<span class='text text-danger fade in'>Aborted!<br>Validation failed!<br>Empty file</span><br>";
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        $message['type'] = 'error';
                        $message['message'] = "<span class='text text-danger fade in'>Invalid File. Import failed! (<i> Hint : Column header mismatch</i>)</span><br>";
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    $message['type'] = 'error';
                    $message['message'] = "<span class='text text-danger fade in'>File not found</span><br>";
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function studentUploadAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $filename = $this->request->getPost('filenamestu');
            $aggreval = $this->request->getPost('aggreval');
            if (file_exists(UPLOAD_DIR . $filename)) {
                $objPHPExcel = PHPExcel_IOFactory::load(UPLOAD_DIR . $filename);
                $maxCol = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
                $maxRow = $objPHPExcel->getActiveSheet()->getHighestDataRow();


                $row = $objPHPExcel->getActiveSheet()->rangeToArray('A' . 1 . ':' . $maxCol . 1);
                $header = ($row[0]);
                $admissionNumPrfix = Settings::findFirstByVariableName('admission_prefix') ?
                        Settings::findFirstByVariableName('admission_prefix') : new Settings();
                $admissionNumSufix = Settings::findFirstByVariableName('admission_sufix') ?
                        Settings::findFirstByVariableName('admission_sufix') : new Settings();
                $prefix = $admissionNumPrfix ? $admissionNumPrfix->variableValue : '';
                $sufix = $admissionNumSufix ? $admissionNumSufix->variableValue : '';
                $stringLen = ControllerBase::getStudentPad('student');
                $admissionNumFormat = $prefix . $stringLen . $sufix;
                if ((!$admissionNumFormat)) {
                    $message['type'] = 'error';
                    $message['message'] = "<div class='alert alert-error'>Admission prefix not set yet.<br/></div>";
                    print_r(json_encode($message));
                    exit;
                } else {

                    $asterikstr = str_replace("*", "", $admissionNumFormat, $asterikcount);
                    $formartArr = explode('*', $admissionNumFormat);
                    //print_r($formartArr);
                    $regex = '/^';
                    $i = 0;
                    foreach ($formartArr as $formatStr):
                        if ($formatStr != '') {
                            $regex .= $formatStr;
                        } else {
                            if ($i == 0)
                                $regex .= '[0-9]{' . $asterikcount . '}';
                            $i = 1;
                        }
                    endforeach;

                    $regex .= '$/';
                }

                $headercheck = array();
                $headercheck = array("Admission number(" . $admissionNumFormat . ")", "Student Name", "Gender(Female/Male)", "Date of Birth(dd/mm/yyyy)", "Date of Joining(dd/mm/yyyy)");

                $ardiff = array_diff($header, $headercheck);
                $i = 0;
                $error = $validation_log = '';
                $inv_dupli_row = array();
//                        echo'hai';    print_r(($ardiff)); echo'hai';exit;
                if (count($ardiff) == 0) {

                    $sheetdata = $objPHPExcel->getActiveSheet()->rangeToArray('A' . 2 . ':' . $maxCol . $maxRow, null, false, true, false);
                    $ee = usort($sheetdata, 'OnboardController::sortArr');
//                    print_r($sheetdata);  exit;
                    foreach ($sheetdata as $skey => $svalue) {
                        $isempty = implode("", $svalue);
                        if ($isempty) {
                            $row = $svalue;
//                            print_r($row);exit;
                            //validation begins
                            if (($row[1] == '') || ($row[2] == '') || ($row[3] == '') || ($row[4] == '')) {
                                $error.= "Missing Value in row " . ($i) . ".<br/>";
                            }
                            /* Appointment No Validation */

                            //format check
                            if ($row[0] && !preg_match($regex, $row[0])) {
                                $error.= "Invalid  Admission Number Format in row " . ($i) . ".<br/>";
                            }
                            $admnno = StudentInfo::find("Admission_no = '" . $row[0] . "'");
                            //print_r($inv_dupli_row);echo ",".$row[0].";".in_array($row[0], $inv_dupli_row)."<br>";
                            if (count($admnno) > 0 || in_array($row[0], $inv_dupli_row)) {
                                $error.= "Admission Number in row" . ($i) . ' already exists.<br/>';
                            } else {
                                if ($row[0])
                                    array_push($inv_dupli_row, $row[0]);
                            }
                            /* Staff Name Validation  */
                            if (!preg_match('/^[a-z][a-z ]*$/i', $row[1])) {
                                $error.= "Invalid  Student Name in row " . ($i) . " . (<i> Hint : Only alphabets and space are allowed!</i>)<br/>";
                            }

                            /*  Gender Validation  */
                            if (!in_array(strtolower($row[2]), array('female', 'male'))) {
                                $error.= "Invalid  Gender in row " . ($i) . ".<br/>";
                                ;
                            }

                            /*  Birth Date Validation  */
                            if (!preg_match("/^([1-9]|0[1-9]|[1-2][0-9]|3[0-1])\/([1-9]|0[1-9]|1[0-2])\/[0-9]{4}$/", $row[3])) {
                                $error.= "Invalid  Date of Birth in row " . ($i) . ".(<i> Hint : date format is dd/mm/yyyy </i>)<br/>";
                            }
                            /*  Appointment Date Validation  */
                            if (!preg_match("/^([1-9]|0[1-9]|[1-2][0-9]|3[0-1])\/([1-9]|0[1-9]|1[0-2])\/[0-9]{4}$/", $row[4])) {
                                $error.= "Invalid  Date of Joining in row " . ($i) . ".(<i> Hint : date format is dd/mm/yyyy </i>)<br/>";
                            }

                            $birthDt = date_timestamp_get(date_create_from_format('d/m/Y', $row[3]));
                            $AppDt = date_timestamp_get(date_create_from_format('d/m/Y', $row[4]));
                            if ($birthDt > $AppDt) {
                                $error.= "Invalid  Birth/joining Date in row " . ($i) . ".(<i> Hint : Birth date greater than Date of joining </i>)<br/>";
                            }
                            if ($birthDt > time() || $AppDt > time()) {
                                $error.= "Invalid  Birth/joining Date in row " . ($i) . ".(<i> Hint : Birth date or Date of joining is future date </i>)<br/>";
                            }
                        }
                    }
//                echo 'ad';
//                print_r($error);exit;
                    if ($error != '') {
                        $message['type'] = 'error';
                        $message['message'] = "<div class='alert alert-error'>Import aborted!<br/>" . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    } else {
                        try {
                            $j = 0;

                            foreach ($sheetdata as $skey => $svalue) {
                                $param = $Pparam = array();
                                $isempty = implode("", $svalue);
                                if ($isempty) {
                                    $j++;
                                    $row = $svalue;
                                    ##generate login id
                                    if (!$row[0]) {
                                        $stumax = StudentInfo::find();
                                        $adminNumArr = array();
                                        $admission_prefix = $admissionNumFormat;
                                        foreach ($stumax as $stuAdmin) {
                                            $adminNumArr[] = $adminNumArr[] = $stuAdmin->Admission_no; //ltrim($stuAdmin->loginid, 's');
                                        }
//                 print_r($adminNumArr);
                                        $arraySorted = natsort($adminNumArr);
//                print_r($adminNumArr);
                                        $lastId = array_pop($adminNumArr);

                                        $asterikstr = str_replace("*", "", $admission_prefix, $asterikcount);
                                        $formartArr = explode('*', $admission_prefix);
                                        $nextAdminNumber = str_replace($formartArr, "", $lastId);
                                        $nextgennum = $nextAdminNumber + 1;
                                        $nextgennum = str_pad($nextgennum, $asterikcount, "0", STR_PAD_LEFT);

                                        $ci = 0;
                                        $nextAdminNo = '';
                                        foreach ($formartArr as $formatStr):
                                            if ($formatStr != '') {
                                                $nextAdminNo .= $formatStr;
                                            } else {
                                                if ($ci == 0)
                                                    $nextAdminNo .= $nextgennum;
                                                $ci = 1;
                                            }
                                        endforeach;
                                        ##generate login id end      
                                    }else {
                                        $nextAdminNo = $row[0];
                                    }
                                    ##Student Login  
                                    $param = array(
                                        'appkey' => APPKEY,
                                        'subdomain' => SUBDOMAIN,
                                        'businesskey' => BUSINESSKEY);
                                    $param['slogin'] = 's' . $nextAdminNo;
                                    $param['plogin'] = 'p' . $nextAdminNo;
                                    $param['password'] = $row[3];
                                    $param['email'] = 's' . $nextAdminNo . '@' . SUBDOMAIN . '.edusparrow.com';
//                                    print_r($param);exit;
                                    $data_string = json_encode($param);
                                    $response = IndexController::curlIt(USERAUTHAPI . 'createStudentLogin', $data_string);
//                                echo 'asd';
//                                print_r($response);
                                    $loginCreated = json_decode($response);
//                                    print_r($loginCreated);
                                    if (!$loginCreated->status) {
                                        $message['type'] = 'error';
                                        $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                                        print_r(json_encode($message));
                                        exit;
                                    }
                                    ##Student LOGIN CREATION END
                                    if ($loginCreated->status == 'ERROR') {
                                        $message['type'] = 'error';
                                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $loginCreated->messages . '</div>';
                                        print_r(json_encode($message));
                                        exit;
                                    } else if ($loginCreated->status == 'SUCCESS' && $loginCreated->data->id > 0) {
/*
                                        ##MAIL LOGIN
                                        $param = array(
                                            'domain' => SUBDOMAIN,
                                            'login' => 's' . $nextAdminNo,
                                            'password' => $row[3]);
                                        $mail_data_string = json_encode($param);
//                                    print_r($param);
                                        $response = IndexController::curlIt(MAILSERVICEAPI . 'createWebMailUser', $mail_data_string);
//                                    print_r($response);
                                        $mailloginCreated = json_decode($response);
//                                        print_r($mailloginCreated);
//                    exit;
                                        if (!$mailloginCreated->status) {
                                            $message['type'] = 'error';
                                            $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                                            print_r(json_encode($message));
                                            exit;
                                        }
                                        if ($mailloginCreated->status == 'ERROR') {
                                            $message['type'] = 'error';
                                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $mailloginCreated->messages . '</div>';
                                            print_r(json_encode($message));
                                            exit;
                                        } else if ($mailloginCreated->status == 'SUCCESS' && $mailloginCreated->data->id) {
                                            
                                        }*/
$res = $this->_admitStudent($row, $mailloginCreated, $loginCreated, $aggreval);
                                            if ($res['type'] != 'success') {
                                                print_r($res['type']);
                                                exit;
                                            } else {
//                                                echo'dome';
                                            }
                                    }
                                }
                            }

                            if ($j > 0) {

                                $message['type'] = 'success';
                                $message['message'] = "<div class='alert alert-success'>Import Completed  !<br/>Total Records [ ($j) ]Inserted! </div>";
                                print_r(json_encode($message));
                                exit;
                            } else {
                                $message['type'] = 'error';
                                $message['message'] = "<div class='alert alert-error'>Import failed!<br/>Empty file</div>";
                                print_r(json_encode($message));
                                exit;
                            }
                        } catch (Exception $ex) {
                            $message['type'] = 'error';
                            $message['message'] = "<div class='alert alert-error'>Import failed!<br/>" . $ex . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                } else {
                    $message['type'] = 'error';
                    $message['message'] = "<div class='alert alert-error'> Invalid File. Import failed! (<i> Hint : Column header mismatch</i>)<br/></div>";
                    print_r(json_encode($message));
                    exit;
                }
            }
        } else {
            $message['type'] = 'error';
            $message['message'] = "<div class='alert alert-error'> Browse atleast one file</div>";
            print_r(json_encode($message));
            exit;
        }
//        }
    }

    public function _admitStudent($row, $mailloginCreated, $loginCreated, $aggreval) {

//        $identity = $this->auth->getIdentity();
        $uid = '1'; //StaffInfo::findFirstByLoginid($identity['name'])->id;
        ## Admit Student

        $stuInfo = new StudentInfo();
        $stuMapping = new StudentMapping();
        $stuInfo->assign(array(
            'Admission_no' => ltrim($loginCreated->data->stulogin, 's'),
            'application_no' => 0,
            'Student_Name' => $row[1],
            'Gender' => ('female' == strtolower($row[2])) ? '1' : '2',
            'Date_of_Birth' => date_timestamp_get(date_create_from_format('d/m/Y', $row[3])),
            'Date_of_Joining' => date_timestamp_get(date_create_from_format('d/m/Y', $row[4])),
            'Email' => $mailloginCreated->data->id,
            'loginid' => $loginCreated->data->stulogin,
            'parent_loginid' => $loginCreated->data->parentlogin,
        ));

        $stuInfo->created_by = $uid;
        $stuInfo->created_date = time();
        $stuInfo->modified_by = $uid;
        $stuInfo->modified_date = time();
//        print_r($stuInfo);exit;
        if ($stuInfo->save()) {
            $stuMapping->student_info_id = $stuInfo->id;
            $stuMapping->aggregate_key = $aggreval;
            $stuMapping->status = 'Inclass';

            $stuAcademic = new StudentHistory();
            $stuAcademic->student_info_id = $stuInfo->id;
            $stuAcademic->aggregate_key = $aggreval;
            $stuAcademic->status = 'Inclass';
//            print_r($stuAcademic);
            if (!$stuAcademic->save()) {
                $error = '';
                foreach ($stuAcademic->getMessages() as $messages) {
                    $error .= $messages;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            } else if (!$stuMapping->save()) {
                $error = '';
                foreach ($stuMapping->getMessages() as $messages) {
                    $error .= $messages;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            } else {
                $message['type'] = 'success';
                return $message;
            }
        } else {
            $error = '';
            foreach ($stuInfo->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function loadStudentsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $aggreval = $this->request->getPost('aggreval');
            $aggname = $this->request->getPost('aggname');
            $res = ControllerBase::buildStudentQuery($aggreval);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,stuinfo.loginid, '
                    . ' stumap.subordinate_key,stumap.status,stuinfo.Student_Name,stuinfo.photo,stuinfo.Admission_no'
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id '
                    . ' WHERE stumap.status = "Inclass" '
                    . ' and (' . implode(' or ', $res) . ')'
                    . ' ORDER BY stuinfo.Student_Name ASC';
            $classStudents = $this->modelsManager->executeQuery($stuquery);
            $this->view->students = $classStudents;
//            print_r(count($stuquery));exit;
            $this->view->aggname = $aggname;
        }
    }

    public function downloadSampleExcelAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $usertyp = $this->request->getPost('usertyp');
            $title = ($usertyp == 'student') ? 'StudentExcelFormat' : 'StaffExcelFormat';
            $filename = $title . '.xlsx';
            $variablename = ($usertyp == 'student') ? 'admission' : 'appointment';

            $stringLen = ControllerBase::getStudentPad('student');
            $sstringLen = ControllerBase::getStudentPad('staff');
            $astriekcnt = ($usertyp == 'student') ? $stringLen : $sstringLen;
            $extracol = ($usertyp == 'student') ? '' : 'Category(Teaching/Non-Teaching)';
            $excel = new PHPExcel();
            $excel->setActiveSheetIndex(0);
            $activesheet = $excel->getActiveSheet();
            $activesheet->setTitle($title);
            $admissionNumPrfix = Settings::findFirstByVariableName($variablename . '_prefix') ?
                    Settings::findFirstByVariableName($variablename . '_prefix') : new Settings();
            $admissionNumSufix = Settings::findFirstByVariableName($variablename . '_sufix') ?
                    Settings::findFirstByVariableName($variablename . '_sufix') : new Settings();
            $prefix = $admissionNumPrfix ? $admissionNumPrfix->variableValue : '';
            $sufix = $admissionNumSufix ? $admissionNumSufix->variableValue : '';
            $admissionNumFormat = $prefix . $astriekcnt . $sufix;
            $start_cell = 'A';
            $cell = 1;
            $activesheet->setCellValue($start_cell++ . $cell, ucfirst($variablename) . " number(" . $admissionNumFormat . ")");
            $activesheet->setCellValue($start_cell++ . $cell, ucfirst($usertyp) . ' Name');
            $activesheet->setCellValue($start_cell++ . $cell, 'Gender(Female/Male)');
            $activesheet->setCellValue($start_cell++ . $cell, 'Date of Birth(dd/mm/yyyy)');
            $activesheet->setCellValue($start_cell . $cell, 'Date of Joining(dd/mm/yyyy)');
            if ($extracol) {
                $activesheet->setCellValue( ++$start_cell . $cell, $extracol);
            }
            $activesheet->getStyle('A1:' . $start_cell . $cell)->getFont()->setBold(true);
            $file = DOWNLOAD_DIR . 'usertemplates/' . $filename;
            unlink($file);
            $objWriter = new PHPExcel_Writer_Excel2007($excel);
            $objWriter->save($file);
            header('Content-Description: File Transfer');
            header('Content-Type:application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function downloadSAppStuAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $aggreval = $this->request->getPost('agg');
            $aggname = $this->request->getPost('aggname');
            $res = ControllerBase::buildStudentQuery($aggreval);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,stuinfo.loginid, '
                    . ' stumap.subordinate_key,stumap.status,stuinfo.Student_Name,stuinfo.photo,stuinfo.Admission_no, '
                    . ' stuinfo.Date_of_Birth,stuinfo.Date_of_Joining,stuinfo.Email,stuinfo.Gender  '
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id '
                    . ' WHERE stumap.status = "Inclass" '
                    . ' and (' . implode(' or ', $res) . ')'
                    . ' ORDER BY stuinfo.Student_Name ASC';
            $classStudents = $this->modelsManager->executeQuery($stuquery);
            $title[] = array('');
            $title[] = array('Student Details ');
            $header = array("Admission number", "Student Name", "Gender", "Date of Birth", "Date of Joining", "Status", "Email", "loginid", "Password");
            $exerows = $this->formatStuTableDataexcel($classStudents);
            $reports[] = $exerows;
            $filename = "StudentList-" . $aggname . "-" . date('m-d-Y H:i:s') . ".csv";
            ControllerBase::exportExcel($filename, $reports, $header, $title);
        }
    }

    public function formatStuTableDataexcel($result) {
        $rowEntries = array();
        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $row['Admission_no'] = $items->Admission_no ? $items->Admission_no : '';
                $row['Student_Name'] = $items->Student_Name ? $items->Student_Name : '';
                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';
                $row['dob'] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row['doj'] = ($items->Date_of_Joining) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row['status'] = $items->status;
                $row['Email'] = $items->Email ? $items->Email : '';
                $row['Login'] = $items->loginid ? $items->loginid : '';
                $row['password'] = ($items->Date_of_Birth) ? date('d/m/Y', $items->Date_of_Birth) : '';
                $rowEntries[] = array_values($row);
            endforeach;
        }
        return $rowEntries;
    }

    public function loadDistinctCombintionStfAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $currstaffcycle = ControllerBase::getCurrentStaffCycle();
        $organizationalMas = OrganizationalStructureMaster::find('mandatory_for_appointment = 1');
        foreach ($organizationalMas as $mvalue) {
            $orgval = OrganizationalStructureValues::find('org_master_id = ' . $mvalue->id);
            foreach ($orgval as $ovalue) {
                if ($mvalue->cycle_node == 1) {
                    if ($ovalue->status == 'C') {
                        $groupnodesstf[] = $ovalue->id;
                    }
                } else {
                    $groupnodesstf[] = $ovalue->id;
                }
            }
        }
        $groupcombiStf = ControllerBase::getCommonIdsForKeys(implode(',', $groupnodesstf));
        foreach ($groupcombiStf as $fagvalue) {
            $newagg = explode('-', $fagvalue);
            $rootnode = array_shift($newagg);
            $rootname = ControllerBase::getNameForKeysFullSearch($rootnode);
            $actualvaln[$rootname[0]][] = $fagvalue;
        }
        $this->view->groupcombiStf = $actualvaln;
    }

    public function checkCountStaffAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            if ($this->request->hasFiles() == true) {
                // Print the real file names and sizes
                foreach ($this->request->getUploadedFiles() as $file) {
                    $ext = explode('.', $file->getName());
                    if ($ext[1] == 'xlsx') {
                        $filename = 'Staff_excel' . '_' . date('Y-m-d-h-i-s') . '.' . $ext[1];
                        $file->moveTo(UPLOAD_DIR . $filename);
                    } else {
                        $message['type'] = 'error';
                        $message['message'] = "<span class='text text-danger fade in'>Invalid file extension</span><br>";
                        print_r(json_encode($message));
                        exit;
                    }
                }
                if (file_exists(UPLOAD_DIR . $filename)) {
                    $objPHPExcel = PHPExcel_IOFactory::load(UPLOAD_DIR . $filename);
                    $maxCol = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
                    $maxRow = $objPHPExcel->getActiveSheet()->getHighestDataRow();
                    $row = $objPHPExcel->getActiveSheet()->rangeToArray('A' . 1 . ':' . $maxCol . 1);
                    $header = ($row[0]);
                    $admissionNumPrfix = Settings::findFirstByVariableName('appointment_prefix') ?
                            Settings::findFirstByVariableName('appointment_prefix') : new Settings();
                    $admissionNumSufix = Settings::findFirstByVariableName('appointment_sufix') ?
                            Settings::findFirstByVariableName('appointment_sufix') : new Settings();
                    $prefix = $admissionNumPrfix ? $admissionNumPrfix->variableValue : '';
                    $sufix = $admissionNumSufix ? $admissionNumSufix->variableValue : '';
                    $sstringLen = ControllerBase::getStudentPad('staff');
                    $admissionNumFormat = $prefix . $sstringLen . $sufix;

                    if ((!$admissionNumFormat)) {
                        $error.= "Appointment prefix not set yet.<br/>";
                    } else {

                        $asterikstr = str_replace("*", "", $admissionNumFormat, $asterikcount);
                        $formartArr = explode('*', $admissionNumFormat);
                        //print_r($formartArr);
                        $regex = '/^';
                        $i = 0;
                        foreach ($formartArr as $formatStr):
                            if ($formatStr != '') {
                                $regex .= $formatStr;
                            } else {
                                if ($i == 0)
                                    $regex .= '[0-9]{' . $asterikcount . '}';
                                $i = 1;
                            }
                        endforeach;

                        $regex .= '$/';
                    }

                    $headercheck = array("Appointment number(" . $admissionNumFormat . ")",
                        "Staff Name", "Gender(Female/Male)", "Date of Birth(dd/mm/yyyy)", "Date of Joining(dd/mm/yyyy)",
                        "Category(Teaching/Non-Teaching)");
                    $ardiff = array_diff($header, $headercheck);
//                     print_r($header);print_r($headercheck);exit;
                    $i = 0;
                    $error = $validation_log = '';
                    $inv_dupli_row = array();
                    if (count($ardiff) == 0) {
                        $j = 0;
                        for ($i = 2; $i <= $maxRow; $i++) {
                            $j++;
                            $row = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $i . ':' . $maxCol . $i);
                            $row = $row[0];
                            $isempty = implode("", $row);
                            if ($isempty) {
                                //validation begins
                                if (($row[1] == '') || ($row[2] == '') || ($row[3] == '') || ($row[4] == '') || ($row[5] == '')) {
                                    $error.= "Missing Value in row " . ($i) . ".<br/>";
                                }
                                /* Appointment No Validation */

                                //format check
                                if ($row[0] && !preg_match($regex, $row[0])) {
                                    $error.= "Invalid  Appointmet Number Format in row " . ($i) . ".<br/>";
                                    ;
                                }
                                $admnno = StaffInfo::find("appointment_no = '" . $row[0] . "'");
                                //print_r($inv_dupli_row);echo ",".$row[0].";".in_array($row[0], $inv_dupli_row)."<br>";
                                if (count($admnno) > 0 || in_array($row[0], $inv_dupli_row)) {
                                    $error.= "Appointment Number in row" . ($i) . ' already exists.<br/>';
                                } else {
                                    if ($row[0])
                                        array_push($inv_dupli_row, $row[0]);
                                }

                                /* Staff Name Validation  */
                                if (!preg_match('/^[a-z][a-z ]*$/i', $row[1])) {
                                    $error.= "Invalid  Staff Name in row " . ($i) . " . (<i> Hint : Only alphabets and space are allowed!</i>)<br/>";
                                }

                                /*  Gender Validation  */
                                if (!in_array(strtolower($row[2]), array('female', 'male'))) {
                                    $error.= "Invalid  Gender in row " . ($i) . ".<br/>";
                                    ;
                                }

                                /*  Birth Date Validation  */
                                if (!preg_match("/^([1-9]|0[1-9]|[1-2][0-9]|3[0-1])\/([1-9]|0[1-9]|1[0-2])\/[0-9]{4}$/", $row[3])) {
                                    $error.= "Invalid  Date of Birth in row " . ($i) . ".(<i> Hint : date format is dd/mm/yyyy </i>)<br/>";
                                }

                                /*  Appointment Date Validation  */
                                if (!preg_match("/^([1-9]|0[1-9]|[1-2][0-9]|3[0-1])\/([1-9]|0[1-9]|1[0-2])\/[0-9]{4}$/", $row[4])) {
                                    $error.= "Invalid  Date of Joining in row " . ($i) . ".(<i> Hint : date format is dd/mm/yyyy </i>)<br/>";
                                }


                                $birthDt = date_timestamp_get(date_create_from_format('d/m/Y', $row[3]));
                                $AppDt = date_timestamp_get(date_create_from_format('d/m/Y', $row[4]));
                                if ($birthDt > $AppDt) {
                                    $error.= "Invalid  Birth/joining Date in row " . ($i) . ".(<i> Hint : Birth date greater than Date of joining </i>)<br/>";
                                }
                                if ($birthDt > time() || $AppDt > time()) {
                                    $error.= "Invalid  Birth/joining Date in row " . ($i) . ".(<i> Hint : Birth date or Date of joining is future date </i>)<br/>";
                                }

                                if (!in_array($row[5], array("Teaching", "Non-Teaching"))) {
                                    $error.= "Invalid Category Date in row " . ($i) . ".(<i> Hint : Options between 'Teaching'or 'Non-Teaching' </i>)<br/>";
                                }
                            }
                        }
//                print_r($error);exit;
                        if ($error != '') {

                            $message['type'] = 'error';
                            $message['message'] = "<span class='text text-danger fade in'>Aborted!<br>" . $error . "</span><br>";
                            print_r(json_encode($message));
                            exit;
                        }
                        if ($j > 0) {
                            $message['type'] = 'success';
                            $message['filename'] = $filename;
                            $message['message'] = "<div class='alert alert-success'>Validation Completed  !<br/>Total Students [ ($j)] !</div>";
                            print_r(json_encode($message));
                            exit;
                        } else {

                            $message['type'] = 'error';
                            $message['message'] = "<div class='alert alert-error'>Validation failed!<br/>Empty file</div>";
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        $message['type'] = 'error';
                        $message['message'] = "<span class='text text-danger fade in'>Invalid File. Import failed! (<i> Hint : Column header mismatch</i>)</span><br>";
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    $message['type'] = 'error';
                    $message['message'] = "<span class='text text-danger fade in'>File not found</span><br>";
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function staffUploadAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $filename = $this->request->getPost('filenamestu');
            $aggreval = $this->request->getPost('aggreval');
            try {
                if (file_exists(UPLOAD_DIR . $filename)) {
                    $objPHPExcel = PHPExcel_IOFactory::load(UPLOAD_DIR . $filename);
                    $maxCol = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
                    $maxRow = $objPHPExcel->getActiveSheet()->getHighestDataRow();
                    $row = $objPHPExcel->getActiveSheet()->rangeToArray('A' . 1 . ':' . $maxCol . 1);
                    $header = ($row[0]);
                    //vALIDATION bEGINS

                    /*                     * * Hearder Validation ** */

                    $admissionNumPrfix = Settings::findFirstByVariableName('appointment_prefix') ?
                            Settings::findFirstByVariableName('appointment_prefix') : new Settings();
                    $admissionNumSufix = Settings::findFirstByVariableName('appointment_sufix') ?
                            Settings::findFirstByVariableName('appointment_sufix') : new Settings();
                    $prefix = $admissionNumPrfix ? $admissionNumPrfix->variableValue : '';
                    $sufix = $admissionNumSufix ? $admissionNumSufix->variableValue : '';
                    $sstringLen = ControllerBase::getStudentPad('staff');
                    $admissionNumFormat = $prefix . $sstringLen . $sufix;

                    if ((!$admissionNumFormat)) {
                        $error.= "Appointment prefix not set yet.<br/>";
                    } else {

                        $asterikstr = str_replace("*", "", $admissionNumFormat, $asterikcount);
                        $formartArr = explode('*', $admissionNumFormat);
                        //print_r($formartArr);
                        $regex = '/^';
                        $i = 0;
                        foreach ($formartArr as $formatStr):
                            if ($formatStr != '') {
                                $regex .= $formatStr;
                            } else {
                                if ($i == 0)
                                    $regex .= '[0-9]{' . $asterikcount . '}';
                                $i = 1;
                            }
                        endforeach;

                        $regex .= '$/';
                    }
                    $headercheck = array("Appointment number(" . $admissionNumFormat . ")",
                        "Staff Name", "Gender(Female/Male)", "Date of Birth(dd/mm/yyyy)", "Date of Joining(dd/mm/yyyy)",
                        "Category(Teaching/Non-Teaching)");
                    $ardiff = array_diff($header, $headercheck);
                    if (count($ardiff) != 0) {
                        echo "<div class='alert alert-error'> Invalid Columns. Import failed!<br/></div>";
                        exit;
                    }

                    $i = 0;
                    $error = $validation_log = '';
                    $inv_dupli_row = array();
                    if (count($ardiff) == 0) {
                        $sheetdata = $objPHPExcel->getActiveSheet()->rangeToArray('A' . 2 . ':' . $maxCol . $maxRow, null, false, true, false);
                        $ee = usort($sheetdata, 'OnboardController::sortArr');
//                    print_r($sheetdata);  exit;
                        foreach ($sheetdata as $skey => $svalue) {
                            $isempty = implode("", $svalue);
                            if ($isempty) {
                                $row = $svalue;

                                /*                                 * * Missing Value Validation ** */
                                if (($row[1] == '') || ($row[2] == '') || ($row[3] == '') || ($row[4] == '') || ($row[5] == '')) {
                                    $error.= "Missing Value in row " . ($i) . ".<br/>";
                                }

                                /* Appointment No Validation */

                                //format check
                                if ($row[0] && !preg_match($regex, $row[0])) {
                                    $error.= "Invalid  Appointmet Number Format in row " . ($i) . ".<br/>";
                                }
                                $admnno = StaffInfo::find("appointment_no = '" . $row[0] . "'");
                                //print_r($inv_dupli_row);echo ",".$row[0].";".in_array($row[0], $inv_dupli_row)."<br>";
                                if (count($admnno) > 0 || in_array($row[0], $inv_dupli_row)) {
                                    $error.= "Appointment Number in row" . ($i) . ' already exists.<br/>';
                                } else {
                                    if ($row[0])
                                        array_push($inv_dupli_row, $row[0]);
                                }
                                /* Staff Name Validation  */
                                if (!preg_match('/^[a-z][a-z ]*$/i', $row[1])) {
                                    $error.= "Invalid  Staff Name in row " . ($i) . ". (<i> Hint : Only alphabets and space are allowed!</i>)<br/>";
                                }

                                /*  Gender Validation  */
                                if (!in_array(strtolower($row[2]), array('female', 'male'))) {
                                    $error.= "Invalid  Gender in row " . ($i) . ".<br/>";
                                    ;
                                }

                                /*  Birth Date Validation  */
                                if (!preg_match("/^([1-9]|0[1-9]|[1-2][0-9]|3[0-1])\/([1-9]|0[1-9]|1[0-2])\/[0-9]{4}$/", $row[3])) {
                                    $error.= "Invalid  Date of Birth in row " . ($i) . ".(<i> Hint : date format is dd/mm/yyyy </i>)<br/>";
                                }

                                /*  Appointment Date Validation  */
                                if (!preg_match("/^([1-9]|0[1-9]|[1-2][0-9]|3[0-1])\/([1-9]|0[1-9]|1[0-2])\/[0-9]{4}$/", $row[4])) {
                                    $error.= "Invalid  Date of Joining in row " . ($i) . ".(<i> Hint : date format is dd/mm/yyyy </i>)<br/>";
                                }


                                $birthDt = date_timestamp_get(date_create_from_format('d/m/Y', $row[3]));
                                $AppDt = date_timestamp_get(date_create_from_format('d/m/Y', $row[4]));
                                if ($birthDt > $AppDt) {
                                    $error.= "Invalid  Birth/joining Date in row " . ($i) . ".(<i> Hint : Birth date greater than Date of joining </i>)<br/>";
                                }
                                if ($birthDt > time() || $AppDt > time()) {
                                    $error.= "Invalid  Birth/joining Date in row " . ($i) . ".(<i> Hint : Birth date or Date of joining is future date </i>)<br/>";
                                }
                                if (!in_array($row[5], array("Teaching", "Non-Teaching"))) {
                                    $error.= "Invalid Category Date in row " . ($i) . ".(<i> Hint : Options between 'Teaching'or 'Non-Teaching' </i>)<br/>";
                                }
                            }
                        }
                        if ($error != '') {
                            echo "<div class='alert alert-error'>Import aborted!<br/>" . $error . '</div>';
                            exit;
                        } else {
                            try {
                                $j = 0;
                                foreach ($sheetdata as $skey => $svalue) {
                                    $isempty = implode("", $svalue);
                                    if ($isempty) {
                                        $row = $svalue;
                                        if (!$row[0]) {
                                            #generate nextapp no
                                            $stumax = StaffInfo::find('appointment_no != "NULL"');
                                            $adminNumArr = array();
                                            $appointment_prefix = $admissionNumFormat;

                                            foreach ($stumax as $stuAdmin) {
                                                $adminNumArr[] = $stuAdmin->appointment_no; // ltrim($stuAdmin->loginid, 'e');
                                            }

                                            $arraySorted = natsort(($adminNumArr));
//                                            print_r($adminNumArr);
                                            $lastId = array_pop($adminNumArr);
                                            $asterikcount = 0;
                                            $asterikstr = str_replace("*", "", $appointment_prefix, $asterikcount);
                                            $formartArr = explode('*', $appointment_prefix);
                                            $nextAdminNumber = str_replace($formartArr, "", $lastId);
                                            $nextgennum = $nextAdminNumber + 1;
                                            $countOfZeros = abs(strlen($nextgennum) - $asterikcount);
                                            $nextgennum = str_pad($nextgennum, $asterikcount, "0", STR_PAD_LEFT);
                                            $ci = 0;
                                            $nextAdminNo = '';
                                            foreach ($formartArr as $formatStr):
                                                if ($formatStr != '') {
                                                    $nextAdminNo .= $formatStr;
                                                } else {
                                                    if ($ci == 0)
                                                        $nextAdminNo .= $nextgennum;
                                                    $ci = 1;
                                                }
                                            endforeach;
                                            $appointment_no = $nextAdminNo;
                                            #generate next app no end
                                        }else {
                                            $appointment_no = $row[0];
                                        }
//                                        echo $appointment_no.'<br>';
                                        $j++;
                                        $param = array(
                                            'appkey' => APPKEY,
                                            'subdomain' => SUBDOMAIN,
                                            'businesskey' => BUSINESSKEY);
                                        $param['slogin'] = 'e' . $appointment_no;
                                        $param['password'] = $row[3];
                                        $param['email'] = 'e' . $appointment_no . '@' . SUBDOMAIN . '.edusparrow.com';
                                        $param['roles'][] = 'Staff';
                                        if ($row[5] == "Teaching")
                                            $param['roles'][] = 'Classroom';
                                        $data_string = json_encode($param);
//                                        print_r($param);
//                                        exit;
                                        $response = IndexController::curlIt(USERAUTHAPI . 'createStaffLogin', $data_string);
//                   print_r($response);exit;
                                        $loginCreated = json_decode($response);
                                        if (!$loginCreated->status) {
                                            $message['type'] = 'error';
                                            $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                                            print_r(json_encode($message));
                                            exit;
                                        } else if ($loginCreated->status == 'ERROR') {
                                            $message['type'] = 'error';
                                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $loginCreated->messages . '</div>';
                                            print_r(json_encode($message));
                                            exit;
                                        } else if ($loginCreated->status == 'SUCCESS' && $loginCreated->data->id > 0) {
                                          /*
					  ##MAIL LOGIN
                                            $param = array(
                                                'domain' => SUBDOMAIN,
                                                'login' => 'e' . $appointment_no,
                                                'password' => $row[3]);
                                            $mail_data_string = json_encode($param);
//                      print_r($param); 
                                            $response = IndexController::curlIt(MAILSERVICEAPI . 'createWebMailUser', $mail_data_string);
//                   print_r($response);;
                                            $mailloginCreated = json_decode($response);
//                                            print_r($mailloginCreated);
//                    exit;
                                            if (!$mailloginCreated->status) {
                                                $message['type'] = 'error';
                                                $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                                                print_r(json_encode($message));
                                                exit;
                                            }
                                            if ($mailloginCreated->status == 'ERROR') {
                                                $message['type'] = 'error';
                                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $mailloginCreated->messages . '</div>';
                                                print_r(json_encode($message));
                                                exit;
                                            } else if ($mailloginCreated->status == 'SUCCESS' && $mailloginCreated->data->id) {

                                            }*/


                                                $staff = new StaffInfo();
                                                $staff->assign(array(
                                                    'appointment_no' => $appointment_no,
                                                    'aggregate_key' => $aggreval,
                                                    'Staff_Name' => $row[1],
                                                    'Gender' => ('female' == strtolower($row[2])) ? '1' : '2',
                                                    'Date_of_Birth' => date_timestamp_get(date_create_from_format('d/m/Y', $row[3])),
                                                    'Date_of_Joining' => date_timestamp_get(date_create_from_format('d/m/Y', $row[4])),
                                                    'Email' => 'e' . $appointment_no . '@' . SUBDOMAIN . '.edusparrow.com',
                                                    'loginid' => 'e' . $appointment_no,
                                                    'status' => 'Appointed'
                                                ));
                                                if ($staff->save()) {
                                                    $message['type'] = 'success';
                                                } else {
                                                    foreach ($staff->getMessages() as $messages) {
                                                        $error .= $messages . '<br>';
                                                    }
                                                    $message['type'] = 'error';
                                                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                                    print_r(json_encode($message));
                                                    exit;
                                                }
                                        }
                                    }
                                }
                                if ($j > 0) {
                                    $message['type'] = 'success';
                                    $message['message'] = "<div class='alert alert-success'>Import Completed for Staff!<br/>Total Records [($j)]Inserted!</div>";
                                    print_r(json_encode($message));
                                    exit;
                                } else {
                                    $message['type'] = 'error';
                                    $message['message'] = "<div class='alert alert-error'>Import failed!<br/>Empty file</div>";
                                    print_r(json_encode($message));
                                    exit;
                                }
                            } catch (Exception $ex) {
                                $message['type'] = 'error';
                                $message['message'] = "<div class='alert alert-error'>Import failed!<br/>" . $ex . '</div>';
                                print_r(json_encode($message));
                                exit;
                            }
                        }
                    } else {
                        $message['type'] = 'error';
                        $message['message'] = "<div class='alert alert-error'> Invalid File. Import failed!<br/></div>";
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    $message['type'] = 'error';
                    $message['message'] = "<div class='alert alert-error'> File not found</div>";
                    print_r(json_encode($message));
                    exit;
                }
            } catch (Exception $e) {
                $message['type'] = 'error';
                $message['message'] = "<div class='alert alert-error'> Invalid File. Import failed!<br/></div>";
                print_r(json_encode($message));
                exit;
            }
        } else {
            $message['type'] = 'error';
            $message['message'] = "<div class='alert alert-error'> Browse atleast one file</div>";
            print_r(json_encode($message));
            exit;
        }
    }

    public function downloadSAppStfAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $aggreval = $this->request->getPost('agg');
            $aggname = $this->request->getPost('aggname');
            $res = ControllerBase::buildStudentQuery($aggreval);
            $stuquery = 'SELECT stfinfo.id,stfinfo.aggregate_key,stfinfo.loginid, '
                    . '  stfinfo.status,stfinfo.Staff_Name,stfinfo.appointment_no, '
                    . ' stfinfo.Date_of_Birth,stfinfo.Date_of_Joining,stfinfo.Email,stfinfo.Gender  '
                    . ' FROM   StaffInfo stfinfo '
                    . ' WHERE  (' . implode(' or ', $res) . ')'
                    . ' ORDER BY stfinfo.Staff_Name ASC';
            $classStudents = $this->modelsManager->executeQuery($stuquery);
//            print_r($classStudents); exit;
            $title[] = array('Staff Details ');
            $title[] = array('');
            $header = array("Appointment number", "Staff Name", "Gender", "Date of Birth", "Date of Joining", "Status", "Email", "loginid", "Password");
            $exerows = $this->formatStfTableDataexcel($classStudents);
            $reports[] = $exerows;
            $filename = "Stafft-" . $aggname . "-" . date('m-d-Y H:i:s') . ".csv";
            ControllerBase::exportExcel($filename, $reports, $header, $title);
        }
    }

    public function formatStfTableDataexcel($result) {
        $rowEntries = array();
        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $row['Appointment_no'] = $items->appointment_no ? $items->appointment_no : '';
                $row['Staff_Name'] = $items->Staff_Name ? $items->Staff_Name : '';
                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';
                $row['dob'] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row['doj'] = ($items->Date_of_Joining) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row['status'] = $items->status;
                $row['Email'] = $items->Email ? $items->Email : '';
                $row['Login'] = $items->loginid ? $items->loginid : '';
                $row['password'] = ($items->Date_of_Birth) ? date('d/m/Y', $items->Date_of_Birth) : '';
                $rowEntries[] = array_values($row);
            endforeach;
        }
        return $rowEntries;
    }

    public function loadStaffListAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $aggreval = $this->request->getPost('aggreval');
            $aggname = $this->request->getPost('aggname');
            $res = ControllerBase::buildStudentQuery($aggreval);

            $stuquery = 'SELECT stfinfo.id,stfinfo.aggregate_key,stfinfo.loginid,stgen.photo, '
                    . '  stfinfo.status,stfinfo.Staff_Name,stfinfo.appointment_no, '
                    . ' stfinfo.Date_of_Birth,stfinfo.Date_of_Joining,stfinfo.Email,stfinfo.Gender  '
                    . ' FROM   StaffInfo stfinfo '
                    . ' LEFT JOIN StaffGeneralMaster stgen on stgen.staff_id = stfinfo.id'
                    . ' WHERE  (' . implode(' or ', $res) . ')'
                    . ' ORDER BY stfinfo.Staff_Name ASC';
            $classStudents = $this->modelsManager->executeQuery($stuquery);
//            print_r(count($classStudents));exit;
            $this->view->staff = $classStudents;
            $this->view->aggname = $aggname;
        }
    }

    public function saveSettingsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $set_addr_variable = Settings::findFirstByVariableName('settings_completed') ?
                Settings::findFirstByVariableName('settings_completed') : new Settings();
        $set_addr_variable->assign(array(
            'variableName' => 'settings_completed',
            'variableValue' => 'yes',
            'variableDescription' => 'Basic School Settings Completed'
        ));
        if ($set_addr_variable->save()) {
            	$message['type'] = 'success';
                print_r(json_encode($message));
                exit;
        } else {
            $message['type'] = 'error';
                $message['message'] =  'Sorry Problem occured!. Please try again later';
                print_r(json_encode($message));
                exit;
        }
    }

    public function loadAssigningTableAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        echo '<pre>';
        $groupnodes = $hearderndes = $nouse = array();
        $organizationalMas = OrganizationalStructureMaster::find('mandatory_for_admission = 1');
        foreach ($organizationalMas as $mvalue) {
            $orgval = OrganizationalStructureValues::find('org_master_id = ' . $mvalue->id);
            foreach ($orgval as $ovalue) {
                if ($mvalue->cycle_node == 1) {
                    if ($ovalue->status == 'C') {
                        $groupnodes[] = $ovalue->id;
                    } else {
                        $nouse[] = $ovalue->id;
                    }
//                        $hearderndes[$ovalue->id] = ControllerBase::getNameForKeysFullSearch($ovalue->id);
                } else {
                    if (!in_array($ovalue->parent_id, $nouse)) {
                        $groupnodes[] = $ovalue->id;
                    } else {
                        $nouse[] = $ovalue->id;
                    }
                }
            }
        }

        $groupcombiStu = ControllerBase::getCommonIdsForKeys(implode(',', $groupnodes));
//        print_r($groupcombiStu);exit;
        foreach ($groupcombiStu as $fagvalue) {
            $newagg = explode('-', $fagvalue);
            $rootnode = array_shift($newagg);
            $rootname = ControllerBase::getNameForKeysFullSearch($rootnode);
            $actualvaln[$rootnode] = $rootname[0];
//            $actualvaln[$rootnode]['classes'][] = $fagvalue;
        }
//        print_r($actualvaln);exit;
//        $hearderndesStu = $hearderndes;

        $this->view->groupcombiStu = $actualvaln;
    }

    public function loadCombiTableAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        echo '<pre>';
        if ($this->request->isPost()) {
            $schoolid = $this->request->getPost('schoolid');
            $groupnodes = $use = $actualvaln = array();
            $use[] = $schoolid;
            $organizationalMas = OrganizationalStructureMaster::find('mandatory_for_admission = 1');
            foreach ($organizationalMas as $mvalue) {
                $orgval = OrganizationalStructureValues::find('org_master_id = ' . $mvalue->id);
                foreach ($orgval as $ovalue) {
                    if (in_array($ovalue->parent_id, $use)) {
                        $groupnodes[] = $ovalue->id;
                        $use[] = $ovalue->id;
                    }
                }
            }
            $groupcombiStu = ControllerBase::getCommonIdsForKeys(implode(',', $groupnodes));
//        print_r($groupcombiStu);exit;
            $this->view->groupcombiStu = $groupcombiStu;
        }
    }

    public function recursiveLoadCombination($aid, $nodes) {
        $fields = OrganizationalStructureMaster::find('parent_id=' . $aid);
        if (count($fields) > 0):
            foreach ($fields as $field) {
                if ($field->mandatory_for_admission != 1 && $field->is_subordinate != 1) {
                    $nodes[$field->id] = $field->name;
                } else {
                    $nodes = OnboardController::recursiveLoadCombination($field->id, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function loadStudentListByCombiAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $aggreval = $this->request->getPost('aggreval');
            $aggname = $this->request->getPost('aggname');
            $res = ControllerBase::buildStudentQuery($aggreval);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,stuinfo.loginid, '
                    . ' stumap.subordinate_key,stumap.status,stuinfo.Student_Name,stuinfo.photo,stuinfo.Admission_no'
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id '
                    . ' WHERE stumap.status = "Inclass" '
                    . ' and (' . implode(' or ', $res) . ')'
                    . ' ORDER BY stuinfo.Student_Name ASC';
            $classStudents = $this->modelsManager->executeQuery($stuquery);
            $this->view->students = $classStudents;
//            print_r(count($stuquery));exit;
            $this->view->aggname = $aggname;
        }
    }

    public function loadUploadViewAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function loadStudentsOnboardAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $params = $queryParams = array();
            foreach ($this->request->getPost() as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'aggregate' && $value != '') {
                    $params['aggregateids'][] = $value;
                } else {

                    $params[$key] = $value;
                }
            }
//            array_shift( $params['aggregateids']);
//            array_shift( $params['aggregateids']);
            $this->view->name = $name = ControllerBase::getNameForKeys(implode(',', $params['aggregateids']));
            $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
            $this->view->nodes = AssigningController::_getNonMandNodesForAssigning($acdyrMas->id);
            $this->view->academicyear = ControllerBase::get_current_academic_year();
            $this->view->aggregateid = implode(',', $params['aggregateids']);
            $this->view->students = StudentMapping::find('aggregate_key LIKE "' . implode(',', $params['aggregateids']) . '%" and status != "Relieved"');
        }
    }

    public function studentAssigningAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function loadStaffUploadDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function loadStaffOnboardAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $params = $queryParams = array();
            foreach ($this->request->getPost() as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'aggregate' && $value != '') {
                    $params['aggregateids'][] = $value;
                } else {

                    $params[$key] = $value;
                }
            }
            $this->view->name = $name = ControllerBase::getNameForKeysFullSearch(implode(',', $params['aggregateids']));
            $this->view->aggregateid = implode(',', $params['aggregateids']);
            $this->view->staff = StaffInfo::find('aggregate_key LIKE "' . implode(',', $params['aggregateids']) . '%" and status != "Relieved"');
        }
    }

    public function loadTempAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->stuCycleNode = $stuCycleNode = OrganizationalStructureMaster::findFirst('cycle_node = 1 and module = "StudentCycleNode"');
        $this->view->curAcd = $curAcd = OrganizationalStructureValues::findFirst('org_master_id = ' . $stuCycleNode->id . ' and status = "C"');
        $this->view->nxtAcd = $nxtAcd = OrganizationalStructureValues::findFirst('org_master_id = ' . $stuCycleNode->id . ' and status = "N"');
        $this->view->orgmasVal = $orgmasVal = OrganizationalStructureMaster::find('basic_level = 1 and specific = 0');
    }

    public function basicTreeAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $params = array();
        foreach ($this->request->getPost() as $key => $value) {
            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'aggregate' && $value != '') {
                $params['aggregateids'][$IsSubdiv[1]] = $value;
            } else {

                $params[$key] = $value;
            }
        }
        $this->view->orgmasVal = $orgmasVal = OrganizationalStructureMaster::find('basic_level = 1');
        $this->view->params = $params;
    }

    public function generateValueTreeAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $paramarr = $this->request->getPost();
            $givnMas = OrganizationalStructureMaster::findFirst('parent_id=0');
            $res = OnboardController::addValuesRecursively($paramarr, 0, $givnMas->id);
            if ($res == 1) {
                $message['type'] = 'success';
                print_r(json_encode($message));
                exit;
            } else {
                $message['type'] = 'error';
                $message['message'] = 'Problem in Creation';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function addValuesRecursively($params, $parent_id = 0, $aid = 0) {
        $givnMas = OrganizationalStructureMaster::findFirst('id=' . $aid);
        //echo 'P'.$parent_id.'i';
        $paramarr = array();
        if ($givnMas->module == 'structural') {
            $paramarr['name'] = $givnMas->name;
            $paramarr['status'] = 'C';
            $paramarr['rangeFrom'] = '';
            $paramarr['rangeTo'] = '';
            $paramarr['mas_id'] = $givnMas->id;
            $paramarr['orgname'] = $givnMas->name;
            $paramarr['nodecolor'] = $givnMas->color;
            $paramarr['nodefont'] = $givnMas->nodefont;
            $paramarr['parent_id'] = $parent_id;
            $orgmasdet = OnboardController::saveValNode($paramarr);
            if (!$orgmasdet) {
                foreach ($orgmasdet->getMessages() as $messages) {
                    $error .= $messages . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            } else {
                $parent_id = $orgmasdet; //'strutparent_id';//
            }
        } else if ($givnMas->module == 'StudentCycleNode') {
            $acdparent = $parent_id;
            if (isset($params['cur_range_from'])) {

                //current Year
                $fromcur = strtotime('01-' . $params['cur_range_from']);
                $tomcur = strtotime('01-' . $params['cur_range_from'] . ' +1 year');

                $paramarr['name'] = date('Y', $fromcur) . '-' . date('Y', $tomcur);
                $paramarr['rangeFrom'] = $fromcur;
                $paramarr['rangeTo'] = $tomcur;
                $paramarr['status'] = 'C';
                $paramarr['mas_id'] = $givnMas->id;
                $paramarr['orgname'] = $givnMas->name;
                $paramarr['nodecolor'] = $givnMas->color;
                $paramarr['nodefont'] = $givnMas->nodefont;
                $paramarr['parent_id'] = $acdparent;

                $orgmasdet = OnboardController::saveValNode($paramarr);
                if (!$orgmasdet) {
                    foreach ($orgmasdet->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                } else {
                    $parent_id = $orgmasdet; //'stucyCrparent_id';//
                }
                //next year
                $fromnxt = $tomcur;
                $tomnxt = strtotime(date('Y-m-d', $fromnxt) . ' +1 year');
                $paramarr['name'] = date('Y', $fromnxt) . '-' . date('Y', $tomnxt);
                $paramarr['rangeFrom'] = $fromnxt;
                $paramarr['rangeTo'] = $tomnxt;
                $paramarr['status'] = 'N';
                $paramarr['mas_id'] = $givnMas->id;
                $paramarr['orgname'] = $givnMas->name;
                $paramarr['nodecolor'] = $givnMas->color;
                $paramarr['nodefont'] = $givnMas->nodefont;
                $paramarr['parent_id'] = $acdparent;

                $orgmasdet = OnboardController::saveValNode($paramarr);
                if (!$orgmasdet) {
                    foreach ($orgmasdet->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        } else if ($givnMas->module == 'Department') {
            if ($givnMas->cycle_node == '1') {
                $paramarr['name'] = 'Staff Cycle';
                $paramarr['status'] = 'C';
                $paramarr['rangeFrom'] = '';
                $paramarr['rangeTo'] = '';
                $paramarr['mas_id'] = $givnMas->id;
                $paramarr['orgname'] = $givnMas->name;
                $paramarr['nodecolor'] = $givnMas->color;
                $paramarr['nodefont'] = $givnMas->nodefont;
                $paramarr['parent_id'] = $parent_id;

                $orgmasdet = OnboardController::saveValNode($paramarr);
                if (!$orgmasdet) {
                    foreach ($orgmasdet->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                } else {

                    $parent_id = $orgmasdet; //'stfcylparent_id';//
                }
            } else {
                $paramarr['name'] = 'Department1';
                $paramarr['status'] = 'C';
                $paramarr['rangeFrom'] = '';
                $paramarr['rangeTo'] = '';
                $paramarr['mas_id'] = $givnMas->id;
                $paramarr['orgname'] = $givnMas->name;
                $paramarr['nodecolor'] = $givnMas->color;
                $paramarr['nodefont'] = $givnMas->nodefont;
                $paramarr['parent_id'] = $parent_id;

                $orgmasdet = OnboardController::saveValNode($paramarr);
                if (!$orgmasdet) {
                    foreach ($orgmasdet->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                } else {

                    $parent_id = $orgmasdet; //'stfdeptparent_id';//
                }
            }
        } else if ($givnMas->module == 'Group') {
            if ($givnMas->upload_flag == '1') {

                foreach ($params['aggr'] as $keyagg => $valagg) {
                    $paramarr = array();
                    $paramarr['name'] = $params['aggr'][$keyagg][$givnMas->id];
                    $paramarr['rangeFrom'] = '';
                    $paramarr['rangeTo'] = '';
                    $paramarr['status'] = 'C';
                    $paramarr['mas_id'] = $givnMas->id;
                    $paramarr['orgname'] = $givnMas->name;
                    $paramarr['nodecolor'] = $givnMas->color;
                    $paramarr['nodefont'] = $givnMas->nodefont;
                    $paramarr['parent_id'] = $parent_id;

                    $orgmasdet = OnboardController::saveValNode($paramarr);
                    if (!$orgmasdet) {
                        foreach ($orgmasdet->getMessages() as $messages) {
                            $error .= $messages . "\n";
                        }
                        $message['type'] = 'error';
                        $message['message'] = $error;
                        print_r(json_encode($message));
                        exit;
                    } else {

                        $cparent_id = $orgmasdet; //'g'. $paramarr['name'].'parent_id';//
                    }


//print_r($paramarr);
                    $chldexistupl = OrganizationalStructureMaster::find('parent_id =' . $givnMas->id);

                    if (count($chldexistupl) > 0) {
                        //recursive call
                        foreach ($chldexistupl as $chl) {
                            $paramarr = array();
                            if ($params['aggr'][$keyagg][$chl->id] > 0) {
                                for ($i = 0; $i < $params['aggr'][$keyagg][$chl->id]; $i++) {
                                    $paramarr['name'] = $chl->name . ($i + 1);
                                    $paramarr['rangeFrom'] = '';
                                    $paramarr['rangeTo'] = '';
                                    $paramarr['status'] = 'C';
                                    $paramarr['mas_id'] = $chl->id;
                                    $paramarr['orgname'] = $chl->name;
                                    $paramarr['nodecolor'] = $chl->color;
                                    $paramarr['nodefont'] = $chl->nodefont;
                                    $paramarr['parent_id'] = $cparent_id;

                                    $orgmasdet = OnboardController::saveValNode($paramarr);
                                    if (!$orgmasdet) {
                                        foreach ($orgmasdet->getMessages() as $messages) {
                                            $error .= $messages . "\n";
                                        }
                                        $message['type'] = 'error';
                                        $message['message'] = $error;
                                        print_r(json_encode($message));
                                        exit;
                                    }
                                }
                            }
                        }
                    } else {
                        
                    }
                }
            }
        }
        $chldexist = OrganizationalStructureMaster::find('parent_id =' . $givnMas->id);

        if (count($chldexist) > 0) {
            //recursive call
            foreach ($chldexist as $chl) {
                $res = OnboardController::addValuesRecursively($params, $parent_id, $chl->id);
            }
        } else {
            
        }
        return 1;
    }

    public function saveValNode($paramarr) {
//print_r($paramarr);

                    $orgValues = OrganizationalStructureValues::findFirst(
                                    "name= '".$paramarr['name']."' "
                                    . " and parent_id = ".$paramarr['parent_id']
                                    . " and org_master_id =  ".$paramarr['mas_id']);

        $orgmasdet =  $orgValues? $orgValues:new OrganizationalStructureValues();
        $orgmasdet->name = $paramarr['name'];
        $orgmasdet->status = $paramarr['status'];
        $orgmasdet->rangefrom = $paramarr['rangeFrom'];
        $orgmasdet->rangeto = $paramarr['rangeTo'];
        $orgmasdet->org_master_id = $paramarr['mas_id'];
        $orgmasdet->orgname = $paramarr['orgname'];
        $orgmasdet->nodecolor = $paramarr['nodecolor'];
        $orgmasdet->nodefont = $paramarr['nodefont'];
        $orgmasdet->parent_id = $paramarr['parent_id'];

        return ($orgmasdet->save()) ? $orgmasdet->id : '';
    }

    public function checkValueTreeAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $orgmasdet = OrganizationalStructureValues::findFirst();
        if ($orgmasdet) {
            $message['type'] = 'orgtree';
            print_r(json_encode($message));
            exit;
        } else {
            $message['type'] = 'orgform';
            print_r(json_encode($message));
            exit;
        }
    }

    public function loadSecondaryTempAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $currectacdyr = ControllerBase::getCurrentAcademicYear();
        $organizationalMas = OrganizationalStructureMaster::findFirst('upload_flag =1');
        $this->view->gradeList = $gradeList = OrganizationalStructureValues::find('org_master_id = ' . $organizationalMas->id . ' and parent_id = ' . $currectacdyr->id);
    }

    public function generateLangCombiAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $paramarr = $this->request->getPost();
            $combiArr = array();
            foreach ($paramarr as $key => $prm) {
                $data = array();
                foreach ($prm as $skey => $set) {
                    $innereset = array();
                    $j = 1;
                    for ($i = 0; $i < $set; $i++) {
                        $arr = array('eth', 'First', 'Second', 'Third', 'Fouth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Ninth', 'Tenth', 'Eleventh', 'Twelfth', 'Thirteenth', 'Fourteenth', 'Fifteenth', 'Sixteenth', 'Seventeenth', 'Eighteenth', 'Nineteenth', 'Twentieth');
                        $innereset[] = $arr[$skey] . ' Language ' . ($j++);
                    }
                    $data[] = $innereset;
                }
                $combos = OnboardController::_combos($data);
                $combiArr[$key] = $combos;
            }
        }
        //print_r($combiArr);exit;
        foreach ($combiArr as $lkey => $lval) {
            $grade = OrganizationalStructureValues::findFirstById($lkey);
            $lang = OrganizationalStructureMaster::findFirst('parent_id =' . $grade->org_master_id . ' and secondary_level = "1" and specific ="1" and getSubjects ="1"');
            $subjlang = OrganizationalStructureMaster::findFirst('parent_id =' . $grade->org_master_id . ' and module = "Subject"');

            foreach ($lval as $lnode => $lsubjval) { 
		$arrcombi = array();
		foreach($lsubjval as $skey => $sv) {
			$arrcombi[] = $skey+1;
		}
                $paramarr = array();
                $paramarr['name'] = $lang->name.' combination '.($lnode+1).' ('. (implode('-',$arrcombi)).')';
                $paramarr['rangeFrom'] = '';
                $paramarr['rangeTo'] = '';
                $paramarr['status'] = 'C';
                $paramarr['mas_id'] = $lang->id;
                $paramarr['orgname'] = $lang->name;
                $paramarr['nodecolor'] = $lang->color;
                $paramarr['nodefont'] = $lang->nodefont;
                $paramarr['parent_id'] = $lkey;
//print_r($paramarr);exit;
                $orgmasdet = OnboardController::saveValNode($paramarr);
                if (!$orgmasdet) {
                    foreach ($orgmasdet->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                } else {
                    foreach ($lsubjval as $subjs) {
                        $paramarr = array();
                        $paramarr['name'] = $subjs;
                        $paramarr['rangeFrom'] = '';
                        $paramarr['rangeTo'] = '';
                        $paramarr['status'] = 'C';
                        $paramarr['mas_id'] = $subjlang->id;
                        $paramarr['orgname'] = $subjlang->name;
                        $paramarr['nodecolor'] = $subjlang->color;
                        $paramarr['nodefont'] = $subjlang->nodefont;
                        $paramarr['parent_id'] = $lkey;

                        $orgmasdet = OnboardController::saveValNode($paramarr);
                        if (!$orgmasdet) {
                            foreach ($orgmasdet->getMessages() as $messages) {
                                $error .= $messages . "\n";
                            }
                            $message['type'] = 'error';
                            $message['message'] = $error;
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                }
            }
        }
        $message['type'] = 'success';
        print_r(json_encode($message));
        exit;
    }

    public function _combos($data, $all = array(), $group = array(), $val = null, $i = 0) {
        if (isset($val)) {
            array_push($group, $val);
        }
        if ($i >= count($data)) {
            array_push($all, $group);
        } else {
            foreach ($data[$i] as $v) {
                $all = OnboardController::_combos($data, $all, $group, $v, $i + 1);
            }
        }

        return $all;
    }

    public function generateClassRoomsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $clsgrps = ClassroomMaster::findFirst();
        if (!$clsgrps) {
            //current year
            $currectacdyr = ControllerBase::getCurrentAcademicYear();
//upload field
            $organizationalMas = OrganizationalStructureMaster::findFirst('upload_flag =1');
            $gradeList = OrganizationalStructureValues::find('org_master_id = ' . $organizationalMas->id . ' and parent_id = ' . $currectacdyr->id);

            foreach ($gradeList as $grade) {
//            $classCombi = $classCombiname = array();
//            $classCombi[] = $grade->id;
//            $classCombiname[] = $grade->name;
                $child = OrganizationalStructureValues::find('parent_id = ' . $grade->id);
                if (count($child) > 0) {
                    foreach ($child as $chld) {
			
            		$chldMas = OrganizationalStructureMaster::findFirstById($chld->org_master_id);
		if($chldMas->is_subordinate != 1 && $chldMas->secondary_level !=1 ){
//                    $classCombi[] = $grade->id.'-'.$chld->id;
//                    $classCombiname[] = $grade->id.'-'.$chld->id;

                        $newnodevalue = $grade->name . '-' . $chld->name;
                        $aggregate_key = $currectacdyr->id.'-'.$grade->id . '-' . $chld->id;
                        //echo "name= '$newnodevalue' and aggregated_nodes_id LIKE '$aggregate_key'";exit;
                        $orgValues = ClassroomMaster::findFirst("name= '$newnodevalue' and aggregated_nodes_id LIKE '$aggregate_key'");

                        if ($orgValues) {
                            $messages['type'] = 'error';
                            $messages['message'] = 'Node name already exist';
                            print_r(json_encode($messages));
                            exit;
                        } else {
                            $orgValues = new ClassroomMaster();
                            $orgValues->name = $newnodevalue;
                            $orgValues->aggregated_nodes_id = $aggregate_key;
                            $stuids = array();
                            if ($orgValues->save()) {
                                $classroomid = $orgValues->id;
                                $stuquery = ControllerBase::buildStudentQuery($orgValues->aggregated_nodes_id);
                                $students = StudentMapping::find('status = "Inclass" and ' . implode(' or ', $stuquery));
                                foreach ($students as $stu) {
                                    $stuids[] = $stu->student_info_id;
                                }
                                if (count($stuids) > 0) {
                                    $stuClassgrp = ClassgroupStudents::findFirst("classroom_master_id = $classroomid") ?
                                            ClassgroupStudents::findFirst("classroom_master_id = $classroomid") :
                                            new ClassgroupStudents();
                                    $stuClassgrp->classroom_master_id = $classroomid;
                                    $stuClassgrp->students_id = implode(',', $stuids);
//                print_r($stuClassgrp); exit;
                                    if (!$stuClassgrp->save()) {
                                        $error = '';
                                        foreach ($stuClassgrp->getMessages() as $messages) {
                                            $error .= $messages;
                                        }
                                        echo $error;
                                        exit;
                                    }
                                }
                            }
                        }
			}//if closing
                    }
                }
            }
        }
        

            $message['type'] = 'success';
            print_r(json_encode($message));
            exit;
    }

}

?>
