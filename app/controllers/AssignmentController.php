<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class AssignmentController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->tag->prependTitle("Assignment | ");
        $this->assets->addCss('css/edustyle.css');
    }

    /** start here * */
    public function addAssignmentAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $identity = $this->auth->getIdentity();
        if (in_array('Staff', $identity['role_name'])) {
            $param['type'] = 'mainexam_mark';
            $getCombination = ControllerBase::loadCombinations($param);
//        print_r($getCombination);exit;
            $this->view->combinations = $getCombination[1];
            $this->view->type = $getCombination[2];
            $this->view->errorMessage = $getCombination[0];
        } else {
            $this->view->errorMessage = 'Only teachers have access to this page!';
        }
    }

    public function loadSubtreesAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost() && $this->request->getPost('orgvalueid') != '') {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
            $this->view->org_value = $org_value = OrganizationalStructureValues::find('parent_id = ' . $orgvalueid
                            . ' GROUP BY  org_master_id ');
        }
    }

    public function saveAssignmentAction() {
        $assignmentform = new AssignmentForm();
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $message = array();
        try {
            if ($this->request->isPost()) {
                if ($assignmentform->isValid($this->request->getPost()) == false) {
                    $error = '';
                    foreach ($assignmentform->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $err = 0;
                    $errmsg = '';
                    $params = array();
                    foreach ($this->request->getPost() as $key => $value) {
                        $IsSubdiv = explode('_', $key);
                        if ($IsSubdiv[0] == 'aggregate' && $value != '') {
                            $params['aggregateids'][] = $value;
                        } else {
                            $params[$key] = $value;
                        }
                    }
                    $assignmentId = $params['assignmentId'];
                    $aggregate_keys = isset($assignmentId) ? $params['subject_modules'] : $params['aggregateids'][count($params['aggregateids']) - 1];
                    $subject_master_id = $params['subject_masterid'];
                    $is_upload = $params['is_upload'];
                    $is_evaluation = $params['is_evaluation'];

                    $assignment = isset($assignmentId) ? AssignmentsMaster::findFirstById($assignmentId) : new AssignmentsMaster();

                    $subject = GroupSubjectsTeachers::findfirst('id = ' . $subject_master_id);
                    $assignment->topic = $params['AssignmentTopic'];
                    $assignment->desc = $params['AssignmentDesc'];
                    $assignment->comments = $params['AssignmentComments'];
                    $assignment->created_by = $uid;
                    $assignment->created_date = time();
                    $assignment->subjct_modules = $aggregate_keys;
                    $assignment->submission_date = strtotime($params['AssignmentDeadline']);
                    $assignment->status = '1';
                    $assignment->is_evaluation = ($is_evaluation == 'on') ? 1 : 0;
                    $assignment->is_upload = ($is_upload == 'on') ? 1 : 0;
                    $assignment->grp_subject_teacher_id = $subject->id;
//                    print_r($assignment);exit;

                    if ($assignment->save()) {

                        $message['type'] = 'success';
                        $message['ItemID'] = $assignment->id;
                        $message['message'] = '<div class="alert alert-block alert-success fade in">Assignment saved Successfully</div>';
                        print_r(json_encode($message));
                        exit;
                    } else {
                        $error = '';
                        foreach ($assignment->getMessages() as $msg) {
                            $error .= $msg;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {

            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function loadAssignmentAddPanelAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $identity = $this->auth->getIdentity();
        $this->view->uid = $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $this->assets->addCss('css/edustyle.css');
        if ($this->request->isPost()) {
            $subject_masterid = $this->request->getPost('subject_masterid');
            $this->view->cdate = $this->request->getPost('currentDate');
            $subject_master = GroupSubjectsTeachers::findfirst('id = ' . $subject_masterid);
            $this->view->subject_id = $subject_master->subject_id;

            $classmaster = ClassroomMaster::findFirstById($subject_master->classroom_master_id);
            $this->view->combinations = $classmaster->name;
            $subjects_mas = OrganizationalStructureMaster::find(array(
                        'columns' => 'GROUP_CONCAT(id) as ids',
                        ' module = "Subject" and is_subordinate = 1 '
            ));
            $subidsall = HomeWorkController::_getAllChildren($subject_master->subject_id, $subjects_mas[0]->ids);

//            $aggregiven = ControllerBase::getCommonIdsForKeys($subject_master->aggregated_nodes_id);
//            $get_Query_For_Subjct = ControllerBase::buildSubjectQuery($subject_master->aggregated_nodes_id);
//            $subdivval = GroupSubjectsTeachers::find(implode(' or ', $get_Query_For_Subjct));
//            $grpsubids = $subjIds = $id_val = array();
//            foreach ($subdivval as $nodes) {
//                $res = ControllerBase::getCommonIdsForKeys($nodes->aggregated_nodes_id);
////                print_r($res);
//                foreach ($res as $set) {
//                    $arrayfdiff = array_diff(explode('-', $aggregiven), explode('-', $set));
//                    if (count($arrayfdiff) == 0) {
//                        $grpsubids[] = $nodes->id;
//                    }
//                }
//            }
//            $subjids = array_unique($grpsubids);
            $queryParams[] = 'grp_subject_teacher_id IN(' . $subject_masterid . ')';
            if ($subject_master->subject_id > 0) {
                $queryParams[] = 'subjct_modules IN (' . implode(',', $subidsall) . ')';
            }

            $this->view->assignmentslist = AssignmentsMaster::find(implode(' and ', $queryParams));
            $this->view->form = new AssignmentForm();
        }
    }

    public function loadAssignmentStudentsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_students = 1;
        if ($this->request->isPost()) {
            $test_id = $this->request->getPost('assignment_id');
            $master_id = $this->request->getPost('subject_masterid');

            $classtest = AssignmentsMaster::findfirst('id = ' . $test_id);
            $grpSubjTeach = GroupSubjectsTeachers::findFirstById($master_id);

            $this->view->message = 'Update marks for  assignment ' . $classtest->topic . ' ';
            $this->view->grp_subject_teacher_id = $master_id;
            $classmaster = ClassroomMaster::findFirstById($grpSubjTeach->classroom_master_id);
//            $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);

            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//            $orgvaldet = OrganizationalStructureValues::findFirstById($grpSubjTeach->subject_id);
//            $subj_arr = $this->_getMandNodesForExam($orgvaldet);
//            $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", stumap.aggregate_key)>0)', array_keys($subj_arr));
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key, stumap.subordinate_key,stumap.status'
                    . ' ,stuinfo.Gender,stuinfo.photo,stuinfo.rollno,stuinfo.Student_Name '
                    . ' FROM StudentMapping stumap '
                    . ' LEFT JOIN StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id '
                    . ' WHERE stumap.status = "Inclass" '
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                    . ' and (' . implode(' or ', $res) . ') '
//                    . ' and (' . implode(' and ', $subjQuery) . ') '
                    . ' ORDER BY stuinfo.Admission_no ASC';

            $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);
            $i = 0;
            if (count($students) > 0) {

                foreach ($students as $student) {
                    $displaymark = "";
                    $testmark = AssignmentMarks::findfirst('assignment_id = ' . $test_id
                                    . ' and student_id = ' . $student->student_info_id);
                    $studentAss = StudentAssignments::findFirst('student_id  = ' . $student->student_info_id
                                    . ' and assignment_id= ' . $test_id);
//                    $sturollno = StudentInfo::findfirst('id = ' . $student->student_info_id);
                    if ($testmark) {
                        $displaymark = $testmark->marks;
                        $out_of = $testmark->outof;
                    }
                    if ($classtest->is_upload == 1) {
                        $filename = $studentAss->upload_file_name;
                        $modified_on = $studentAss->modified_on;
                    }
                    $gender = ($student->Gender == 1 ? 'Female' : 'Male');
                    $rows[$i++] = array($student->Student_Name,
                        $student->student_info_id, $displaymark, $out_of, $student->rollno,
                        $filename, $modified_on, $classtest->submission_date,
                        $student->photo,
                        $gender);
                }
                $this->view->rows = $rows;
                $this->view->is_evaluation = $classtest->is_evaluation;
                $this->view->is_upload = $classtest->is_upload;
                $this->view->out_of = $out_of;
            } else {
                $this->view->display_students = 0;
            }
        }
    }

    public function getAssignmentListByCombinationAction($params) {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

//        $student_id = '82'; //$params['student_id'];//
//        $master_id = '1'; //$params['master_id'];//
//        $academicYrId = '1'; //$params['academic_year_id'];//
//        print_r($master_id);exit;
        //if ($this->request->isPost()) {

        $master_id = $this->request->getPost('subject_masterid'); //'4';
        $this->view->master_id = $master_id;
        $classtestCombi = GroupSubjectsTeachers::findFirstById($master_id);

        $subjects_mas = OrganizationalStructureMaster::find(array(
                    'columns' => 'GROUP_CONCAT(id) as ids',
                    ' module = "Subject" and is_subordinate = 1 '
        ));
        $subidsall = HomeWorkController::_getAllChildren($classtestCombi->subject_id, $subjects_mas[0]->ids);

        $classmaster = ClassroomMaster::findFirstById($classtestCombi->classroom_master_id);
        $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
////        $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
//        $orgvaldet = OrganizationalStructureValues::findFirstById($classtestCombi->subject_id);
//        $subj_arr = $this->_getMandNodesForExam($orgvaldet);
//        $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", stumap.aggregate_key)>0)', array_keys($subj_arr));
        $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key, stumap.subordinate_key,stumap.status'
                . ' FROM StudentMapping stumap '
                . ' LEFT JOIN StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id '
                . ' WHERE stumap.status = "Inclass" '
                . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                . ' and (' . implode(' or ', $res) . ') '
//                . ' and (' . implode(' and ', $subjQuery) . ') '
                . ' ORDER BY stuinfo.Admission_no ASC';
        $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);

        $i = 0;
        $cosolidatedMarks = $overaloutof = array();
        if (count($students) > 0) {
            foreach ($students as $student) {
                $classTests = AssignmentsMaster::find('grp_subject_teacher_id =' . $master_id . ' AND subjct_modules IN (' . implode(',', $subidsall) . ')');
                //  $classTests = AssignmentsMaster::find('subjct_modules IN (' . implode(',', $subidsall) . ')');
                foreach ($classTests as $classTest) {
                    $clsTstMarks = AssignmentMarks::findFirst('assignment_id = ' . $classTest->id
                                    . ' and student_id = ' . $student->id);
                    $obtainedmark = ($clsTstMarks->marks) ? $clsTstMarks->marks : 0;
                    $obtainedOutOf = ($clsTstMarks->outof) ? $clsTstMarks->outof : 0;
                    $overaloutof[$classTest->id] = $obtainedOutOf > 0 ? $obtainedOutOf : $overaloutof[$classTest->id];
                    $cosolidatedMarks[$student->id][$classTest->id] = array(
                        'ObtainedMarks' => $obtainedmark
                    );

                    $yaxis[$classTest->id] = array(
                        'assignmenttopic' => $classTest->topic,
                        'outoff' => $overaloutof[$classTest->id]);
                }
            }
            $this->view->tests = $yaxis;
            $this->view->studentMarkList = $cosolidatedMarks;
        }
        // }
    }

    public function checkLinkageForAssignmentAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        if ($this->request->isPost()) {
            $assignmentId = $this->request->getPost('assignmentId');
            $test = AssignmentsMaster::findFirstById($assignmentId);
            $message['message'] = '';
            if ($test->is_upload == 1) {
                $message['type'] = 'warning';
                $message['message'] .= " \n Deleting this assignment will lead to following action \n"
                        . " 1. File submitted(uploaded) will be deleted \n.";
            }
            $mainexMark = AssignmentMarks::findFirstByAssignmentId($assignmentId);
            if ($mainexMark) {
                $message['type'] = 'warning';
                $message['message'] = "Marks added to students for this assignment will also be delted. "
                        . "\n Do you really wanna delete this assignment?";
                print_r(json_encode($message));
                exit;
            }

            $message['type'] = 'success';
            $message['message'] = "No action performed yet and can be delete!";
            print_r(json_encode($message));
            exit;
        }
    }

    public function deleteAssignmentAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $this->view->viewclasstest = 0;

        if ($this->request->isPost()) {
            $res = 1;
            $assignment_id = $this->request->getPost('assignment_id');
            $test = AssignmentsMaster::findFirstById($assignment_id);
            $testMarks = AssignmentMarks::find('assignment_id = ' . $assignment_id);
            if (!$testMarks->delete()) {
                $error = '';
                foreach ($testMarks->getMessages() as $message) {
                    $error .= $message;
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
            if (!$test->delete()) {
                $error = '';
                foreach ($test->getMessages() as $msg) {
                    $error .= $msg;
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            } else if ($res == 1) {
                $message['type'] = 'success';
                $message['message'] = "Assignment Deleted Successfully!";
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function storeAssignmentAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $message = array();
        if ($this->request->isPost()) {
            $assignment_id = $this->request->getPost('assignment_id');
            if ($this->request->hasFiles() == true) {
                // Print the real file names and sizes
                foreach ($this->request->getUploadedFiles() as $file) {
                    $ext = explode('.', $file->getname());
                    if (($ext[1] == 'doc') || ($ext[1] == 'docx') || ($ext[1] == 'xls') ||
                            ($ext[1] == 'xlsx') || ($ext[1] == 'pdf') || ($ext[1] == 'pdf') || ($ext[1] == 'txt')) {
                        $fname = explode('.', $file->getname());
                        $filename = strtolower($fname[0]) . '_' . time() . '.' . $fname[1];
                        $file->moveTo(ASSIGNMENT_DIR . $filename);
                        $student = StudentInfo::findFirstByLoginid($identity['name']);
                        $assignment = StudentAssignments::findfirst('assignment_id = ' . $assignment_id
                                        . ' and student_id = ' . $student->id);

                        if ($assignment) {
                            $assignment->upload_file_name = $filename;
                            $assignment->modified_by = $student->id;
                            $assignment->modified_on = time();
                        } else {
                            $assignment = new StudentAssignments();
                            $assignment->student_id = $student->id;
                            $assignment->assignment_id = $assignment_id;
                            $assignment->upload_file_name = $filename;
                            $assignment->created_by = $identity['id'];
                            $assignment->created_on = time();
                            $assignment->modified_by = $identity['id'];
                            $assignment->modified_on = time();
                        }
//                        print_r($assignment);exit;
                        if ($assignment->save()) {
                            $message['type'] = 'success';
                            $message['stuid'] = $assignment->student_id;
                            $message['message'] = '<div class="alert alert-block alert-success fade in">Assignment Uploaded Successfully</div>';
                            print_r(json_encode($message));
                            exit;
                        } else {
                            $message['type'] = 'error';
                            $message['stuid'] = $student->id;
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">Invalid file extension</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">Invalid file extension</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            } else {
                //echo 'noimage';
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Please select a file to upload!</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    /** end here * */
    public function _calculateMarksToLinkedExamsNEW($params) {
        try {
//            $initType = $params['type']; //class test or mainexam
            $studentId = $params['stuId']; //student Id
            $examTwoId = $params['x2']; // linked test Id
            $combiId = $params['CombiId'] ? $params['CombiId'] : ''; //combination master Id  
            $ObtainedMarks = $params['obtainedMarks']; //entered   
            $obtainedOutOf = $params['obtainedOutOf']; //entered   

            $examTwo = Mainexam::findFirstById($examTwoId); // linked test
            $examTwoMark = MainexamMarks::findFirst('mainexam_id = ' . $examTwoId .
                            ' and student_id = ' . $studentId .
                            ' and combinationId = ' . $combiId) ?
                    MainexamMarks::findFirst('mainexam_id = ' . $examTwoId .
                            ' and student_id = ' . $studentId .
                            ' and combinationId = ' . $combiId) :
                    new MainexamMarks();

            $mark = $ObtainedMarks ? $ObtainedMarks : ($examTwoMark->marks ? $examTwoMark->marks : 0);
            $calcOutOf = $obtainedOutOf ? $obtainedOutOf : ($examTwoMark->outof ? $examTwoMark->outof : 0);
//            echo $mark.'/'.$calcOutOf.'<br>';
            if ((floatval($calcOutOf) < floatval($mark))) {
                $message['type'] = 'error';
                $message['message'] = 'Student Mark exceeds total marks';
                print_r(json_encode($message));
                exit;
            }

            //linked main ex updation start
            $getAllRefMains = Mainexam::find('mainexam_id = ' . $examTwoId);
            $getAllRefTst = ClassTest::find('mainexam_id = ' . $examTwoId .
                            ' and subject_master_id = ' . $combiId);
            $getAllRefAssigns = AssignmentsMaster::find('mainexam_id = ' . $examTwoId .
                            ' and subject_master_id = ' . $combiId);
            $linkedmainIheritedTotal = 0;
            $linkedmainIheritedTOutOf = 0;
            $linkedmainrefOutOf = 0;
            foreach ($getAllRefMains as $mainrefex) {
                $refmainmarks = MainexamMarks::findFirst('mainexam_id = ' . $mainrefex->id .
                                ' and student_id = ' . $studentId .
                                ' and combinationId = ' . $combiId) ?
                        MainexamMarks::findFirst('mainexam_id = ' . $mainrefex->id .
                                ' and student_id = ' . $studentId .
                                ' and combinationId = ' . $combiId) :
                        new MainexamMarks();

                if ($refmainmarks->mainexam_marks_id > 0) {
                    $linkedmainIheritedTotal += (($refmainmarks->marks ? $refmainmarks->marks : 0) +
                            ($refmainmarks->inherited_marks ? $refmainmarks->inherited_marks : 0));
                    $linkedmainIheritedTOutOf += (($refmainmarks->outof ? $refmainmarks->outof : 0) +
                            ($refmainmarks->inherited_outof ? $refmainmarks->inherited_outof : 0));
                    $linkedmainrefOutOf += $mainrefex->mainexam_outof;
                }
            }
            foreach ($getAllRefTst as $reftest) {
//                echo 'class_test_id = ' . $reftest->class_test_id .
//                                ' and student_id = ' . $studentId;
                $reftestmarks = ClassTestMarks::findFirst('class_test_id = ' . $reftest->class_test_id .
                                ' and student_id = ' . $studentId) ?
                        ClassTestMarks::findFirst('class_test_id = ' . $reftest->class_test_id .
                                ' and student_id = ' . $studentId) :
                        new ClassTestMarks();
                if ($reftestmarks->classtest_marks_id > 0) {
                    $linkedmainIheritedTotal += ($reftestmarks->marks ? $reftestmarks->marks : 0);
                    $linkedmainIheritedTOutOf += ($reftestmarks->outof ? $reftestmarks->outof : 0);
                    $linkedmainrefOutOf += $reftest->mainexam_outof;
                }
            }

            foreach ($getAllRefAssigns as $refassign) {
//                echo 'class_test_id = ' . $reftest->class_test_id .
//                                ' and student_id = ' . $studentId;
                $reftestmarks = AssignmentMarks::findFirst('assignment_id = ' . $refassign->id .
                                ' and student_id = ' . $studentId) ?
                        AssignmentMarks::findFirst('assignment_id = ' . $refassign->id .
                                ' and student_id = ' . $studentId) :
                        new AssignmentMarks();
                if ($reftestmarks->id > 0) {
                    $linkedmainIheritedTotal += ($reftestmarks->marks ? $reftestmarks->marks : 0);
                    $linkedmainIheritedTOutOf += ($reftestmarks->outof ? $reftestmarks->outof : 0);
                    $linkedmainrefOutOf += $refassign->mainexam_outof;
                }
            }
            $inhertMarkCalc = (($linkedmainIheritedTotal / $linkedmainIheritedTOutOf) * $linkedmainrefOutOf);
//            print_r($linkedmainIheritedTotal . ' / ' . $linkedmainIheritedTOutOf . ' * ' . $linkedmainrefOutOf . '<br>');
//            echo 'hai';
//            exit;
//            
//            print_r($mark.'/'.$calcOutOf);exit;
            if (($inhertMarkCalc > 0 || $examTwoMark->marks > 0 || $mark > 0 || $calcOutOf > 0)) {
                $examTwoMark->inherited_marks = $inhertMarkCalc;
                $examTwoMark->mainexam_id = $examTwoId;
                $examTwoMark->student_id = $studentId;
                $examTwoMark->marks = $mark;
                $examTwoMark->inherited_outof = $linkedmainrefOutOf;
                $examTwoMark->outof = round($calcOutOf, 2);
                $examTwoMark->combinationId = $combiId;
//                print_r($examTwoMark->student_id.' : '.$examTwoMark->marks);
//                 print_r($examTwoMark->outof );
                if (!$examTwoMark->save()) {
//                print_r($examTwoMark->getMessages());exit;
                    return $examTwoMark->getMessages();
                } else {
//                    print_r($examTwoMark->outof );
                    if ($examTwo->mainexam_id) {

                        $recursiveParam = array(
                            'type' => 'main',
                            'stuId' => $studentId,
                            'x1' => $examTwoId,
                            'x2' => $examTwo->mainexam_id,
                            'CombiId' => $combiId
                        );
                        $this->_calculateMarksToLinkedExamsNEW($recursiveParam);
                    }
                }
            }
            return 1;
        } catch (Exception $ex) {

            return $ex;
        }
    }

    public function _getMandNodesForExam($acdyrMas, $nodes = array()) {
        $fields = OrganizationalStructureValues::findFirstById($acdyrMas->parent_id);
        $iscycle = OrganizationalStructureMaster::findFirstById($acdyrMas->org_master_id);
//        $query = 'id =' . $fields->org_master_id . ' AND module !="Subject"';
//        $org_mas = OrganizationalStructureMaster::find(array(
//                    $query,
//                    "columns" => "COUNT(*) as cnt"
//        ));
//        echo $iscycle->id.$iscycle->promotion_status.$name->id.'<br>';
        if (!in_array($iscycle->module, array("structural", "StaffCycleNode", "Ledger"))) {
            if ($iscycle->module != "Subject"):
                $nodes[$acdyrMas->id] = $acdyrMas->name;

            endif;
            if (isset($fields->parent_id)):
//            echo $fields->parent_id;
                $nodes = $this->_getMandNodesForExam($fields, $nodes);
            endif;
        }

        return $nodes;
    }

    public function addNewAssignmentAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $identity = $this->auth->getIdentity();
        $this->view->uid = $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $this->view->form = new AssignmentForm();
        $this->view->display = 0;
        if ($this->request->isPost()) {
            $subject_masterid = $this->request->getPost('subject_masterid');
            $this->view->subject = $subject_master = GroupSubjectsTeachers::findfirst('id = ' . $subject_masterid);
            $records = Mainexam::find("LOCATE(  node_id ,  '$subject_master->aggregated_nodes_id' ) ");
            if (count($records) > 0) {
                $this->view->display = 1;
                foreach ($records as $record) {
                    $mainexams[$record->id] = $record->exam_name;
                }
            }
            $this->view->mainexams = $mainexams;
        }
    }

    public function viewEditAssignmentAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display = 0;
        $this->assets->addCss('css/edustyle.css');
        $this->view->selectedAssignment = $this->request->getPost('selectedAssignment');
        $this->view->action = $this->request->getPost('action');
        $this->view->assignmentslist = AssignmentsMaster::findFirstById($this->view->selectedAssignment);
        $subject_master = GroupSubjectsTeachers::findfirst('id = ' . $this->view->assignmentslist->grp_subject_teacher_id);
        $identity = $this->auth->getIdentity();
        $this->view->uid = $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $this->view->form = new AssignmentForm($this->view->assignmentslist, array($this->view->action => true));

        $records = Mainexam::find("LOCATE(  node_id ,  '$subject_master->aggregated_nodes_id' ) ");

        if (count($records) > 0) {
            $this->view->display = 1;
            foreach ($records as $record) {
                $mainexams[$record->id] = $record->exam_name;
            }
        }

        $this->view->mainexams = $mainexams;
    }

    public function updateAssignmentMarkAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $test_id = $this->request->getPost('test_id');
            $student_id = $this->request->getPost('student_id');
            $mark = $this->request->getPost('mark');
            $total_mark = $this->request->getPost('total_mark');
            $record = AssignmentMarks::findfirst('assignment_id = ' . $test_id . 'and student_id = ' . $student_id);
            if (!$record) {
                $record = new AssignmentMarks();
                //echo 'new';
            }
            $res = 1;
            $linkedReccord = AssignmentsMaster::findFirst('id = ' . $test_id);
            $grpSubjTeach = GroupSubjectsTeachers::findFirstById($linkedReccord->grp_subject_teacher_id);
            $record->marks = $mark;
            $record->assignment_id = $test_id;
            $record->student_id = $student_id;
            $record->outof = $total_mark;
            if (!$record->save()) {
                foreach ($record->getMessages() as $message) {
                    $error .= $message;
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            } else {

                $testid = 'A_' . $test_id;
                $subject_id = $linkedReccord->subjct_modules;

                $condition = 'subject_id = ' . $subject_id . ' AND (FIND_IN_SET("' . $testid . '",exam_ids) > 0)';

//                echo $condition; exit;
                $formula = FormulaTable::findFirst($condition);
                $exam_ids = isset($formula->exam_ids) ? explode(',', $formula->exam_ids) : '';
                $student_id_arr = array();
                if ($exam_ids != '') {
                    // echo 'vv';exit;
                    foreach ($exam_ids as $exam_id) {
                        $expldval = explode('_', $exam_id);
                        if ($expldval[0] == 'M') {
                            $refmainmarks = MainexamMarks::find('mainexam_id = ' . $expldval[1] .
                                            ' and subject_id = ' . $subjectid .
                                            ' and grp_subject_teacher_id = ' . $master_id) ?
                                    MainexamMarks::find('mainexam_id = ' . $expldval[1] .
                                            ' and subject_id = ' . $subjectid .
                                            ' and grp_subject_teacher_id = ' . $master_id) :
                                    new MainexamMarks();

                            if ($refmainmarks) {
                                foreach ($refmainmarks as $refmainmark) {
                                    $student_id_arr[] = $refmainmark->student_id;
                                }
                            }
                        }
                        if ($expldval[0] == 'C') {
                            $reftest = ClassTestMarks::find('class_test_id = ' . $expldval[1]);
//                           echo 'in';      
                            if ($reftest) {
                                foreach ($reftest as $reftestmark) {
                                    $student_id_arr[] = $reftestmark->student_id;
                                }
                            }
                        }
                        if ($expldval[0] == 'A') {
                            $refassign = AssignmentMarks::find('assignment_id =' . $expldval[1]);
                            if ($refassign) {
                                foreach ($refassign as $refassignmark) {
                                    $student_id_arr[] = $refassignmark->student_id;
                                }
                            }
                        }
                    }

//             print_r($student_id_arr);
//            $clsTstRec = ClassTest::find('mainexam_id = ' . $mainExId);
//                foreach ($mainexRec as $mainEx) {
//                    $student_id_arr[] = $mainEx->student_id;
//                }
                    $stuarr = array_unique($student_id_arr);
//                 print_r($stuarr);
//                exit;
                    $res = 1;
                    foreach ($stuarr as $student_id) {
                        $recursiveParam = array(
                            'stuId' => $student_id,
                            'x2' => $formula->mainexm_id,
                            'obtainedMarks' => '',
                            'obtainedOutOf' => '',
                            'subject_id' => $subject_id,
                            'clastst_combi_id' => $grpSubjTeach->id
                        );

//                    print_r($recursiveParam);exit;

                        $res = MarkController::_calculateMarksToLinkedExamsNEW($recursiveParam);
                    }
                    if ($res == 1) {
                        $message['type'] = 'success';
                        $message['message'] = 'Successfully updated';
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    $message['type'] = 'success';
                    $message['message'] = 'Successfully updated';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function updateAssTestTotMarkAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $res = 1;
            $test_id = $this->request->getPost('test_id');
            $total_mark = $this->request->getPost('total_mark');
            $record = AssignmentMarks::find('assignment_id = ' . $test_id);
            $linkedReccord = AssignmentsMaster::findFirstById($test_id);
            $grpSubjTeach = GroupSubjectsTeachers::findFirstById($linkedReccord->grp_subject_teacher_id);
            if (count($record) > 0) {
                foreach ($record as $stumark) {

                    $stumark->assignment_id = $test_id;
                    $stumark->outof = $total_mark;
                    if (!$stumark->save()) {
                        $err = 1;
                        foreach ($record->getMessages() as $message) {
                            $error .= $message;
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    } else {

                        if ($linkedReccord && $linkedReccord->mainexam_id) {
                            $recursiveParam = array(
                                'stuId' => $stumark->student_id,
                                'x2' => $linkedReccord->mainexam_id,
                                'obtainedMarks' => '',
                                'subject_id' => $grpSubjTeach->subject_id,
                                'obtainedOutOf' => '',
                                'clastst_combi_id' => $grpSubjTeach->id
                            );
//                    print_r($recursiveParam);exit;
                            $res = $this->_calculateMarksToLinkedExamsNEW($recursiveParam);
                        }
                    }
                }
            }

            if ($res == 1) {
                $message['type'] = 'success';
                $message['message'] = 'Successfully updated';
                print_r(json_encode($message));
            }
        }
    }

    public function getSectionByClassAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->assets->addCss('css/edustyle.css');
        if ($this->request->isPost()) {
            $this->view->classId = $this->request->getPost('classId');
        }
    }

    public function getSubjectsByClassAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->assets->addCss('css/edustyle.css');
        if ($this->request->isPost()) {
            $this->view->classId = $this->request->getPost('class_id');

            $this->view->subdivid = $this->request->getPost('subDivVal');

            $this->view->subjects = SubDivisionSubjects::find('divValID =' . $this->view->classId . ' and subDivValId=' . $this->view->subdivid);
        }
    }

    public function getAssignmentListExcelReportAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        $data = json_decode($this->request->getPost('params'));
        foreach ($data as $key => $value) {
            $params[$key] = $value;
        }
        $master_id = $params['subject_masterid'];

        $classtestCombi = GroupSubjectsTeachers::findFirstById($master_id);

        $subjects_mas = OrganizationalStructureMaster::find(array(
                    'columns' => 'GROUP_CONCAT(id) as ids',
                    ' module = "Subject" and is_subordinate = 1 '
        ));
        $subidsall = HomeWorkController::_getAllChildren($classtestCombi->subject_id, $subjects_mas[0]->ids);

        $classmaster = ClassroomMaster::findFirstById($classtestCombi->classroom_master_id);
        $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//        $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
//        $orgvaldet = OrganizationalStructureValues::findFirstById($classtestCombi->subject_id);
//        $subj_arr = $this->_getMandNodesForExam($orgvaldet);
//        $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", stumap.aggregate_key)>0)', array_keys($subj_arr));
        $stuquery = 'SELECT stumap.id,stumap.student_info_id,stuinfo.Student_Name,stumap.aggregate_key, stumap.subordinate_key,stumap.status'
                . ' FROM StudentMapping stumap '
                . ' LEFT JOIN StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id '
                . ' WHERE stumap.status = "Inclass" '
                . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                . ' and (' . implode(' or ', $res) . ') '
//                . ' and (' . implode(' and ', $subjQuery) . ') '
                . ' ORDER BY stuinfo.Admission_no ASC';

        $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);

        $i = 0;
        $cosolidatedMarks = $overaloutof = $Name = array();
        if (count($students) > 0) {
            foreach ($students as $student) {
                $classTests = AssignmentsMaster::find('grp_subject_teacher_id =' . $master_id . ' AND subjct_modules IN (' . implode(',', $subidsall) . ')');
                foreach ($classTests as $classTest) {
                    $clsTstMarks = AssignmentMarks::findFirst('assignment_id = ' . $classTest->id
                                    . ' and student_id = ' . $student->id);
                    $obtainedmark = ($clsTstMarks->marks) ? $clsTstMarks->marks : 0;
                    $obtainedOutOf = ($clsTstMarks->outof) ? $clsTstMarks->outof : 0;
                    $overaloutof[$classTest->id] = $obtainedOutOf > 0 ? $obtainedOutOf : $overaloutof[$classTest->id];
                    $cosolidatedMarks[$student->id][$classTest->id] = array(
                        'ObtainedMarks' => $obtainedmark
                    );

                    $yaxis[$classTest->id] = array(
                        'assignmenttopic' => $classTest->topic,
                        'outoff' => $overaloutof[$classTest->id]);
                }
                $Name[] = $student->Student_Name;
                $aggr_key[] = $student->aggregate_key;
            }
//            $this->view->tests = $yaxis;
//            $this->view->studentMarkList = $cosolidatedMarks;
        }
        $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->mandnode = $this->_getMandNodesForAssigning($acdyrMas);

        $header[] = 'Student Name';
        $clsscnt = 0;
        foreach ($this->mandnode as $node) {
            if ($clsscnt != 0) {
                $header[] = ucfirst($node);
            }
            $clsscnt++;
        }
        if (count($yaxis) > 0):
            foreach ($yaxis as $test):
                $header[] = $test['assignmenttopic'] . '(' . $test['outoff'] . ')';
            endforeach;
        endif;

        $reportdata = array();
        $i = 0;
        if (count($cosolidatedMarks) > 0) {
            foreach ($cosolidatedMarks as $stum) {
                $reportval = array();
                $reportval[] = $Name[$i];
                $aggregatevals = $aggr_key[$i] ? explode(',', $aggr_key[$i]) : '';
                $class_arr = array();
                $aggcnt = 0;
                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {
                        if ($aggcnt != 0) {
                            $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                            $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                            $class_arr[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                        }
                        $aggcnt++;
                    }
//                        array_shift($class_arr);
                    $aggcntval = 0;
                    foreach ($this->mandnode as $key => $mandnodeval) {
                        if ($aggcntval != 0) {
                            $reportval[] = isset($class_arr[$key]) ? $class_arr[$key] : '-';
                        }
                        $aggcntval++;
                    }
                }
                if (count($yaxis) > 0):
                    foreach ($yaxis as $key => $test):
                        $reportval[] = $stum[$key]['ObtainedMarks'];
                    endforeach;
                endif;
                $reportdata[] = $reportval;
                $i++;
            }
        }

        $filename = 'Student_Assignment_List_' . date('d-m-Y') . '.csv';

        $counter = 0;
        $unlink_file = DOWNLOAD_DIR . 'Student_Assignment_List_' . date('d-m-Y') . '.csv';
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }

        $fp = fopen(DOWNLOAD_DIR . $filename, 'a');

        $delimiter = ",";
        $emptyarr = array();
        fputcsv($fp, $subject_head, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $header, $delimiter);
        foreach ($reportdata as $reportsval) {
//            $fp = fopen(DOWNLOAD_DIR . $filename, 'a');
            fputcsv($fp, $reportsval, $delimiter);
        }
        fclose($fp);

        $file = DOWNLOAD_DIR . $filename;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

    public function _getMandNodesForAssigning($acdyrMas, $nodes = array()) {

        $nodes[$acdyrMas->id] = $acdyrMas->name;
//  echo $acdyrMas->name;
        $fields = OrganizationalStructureMaster::find('parent_id=' . $acdyrMas->id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
//if ($field->mandatory_for_admission == 1) {
                if ($field->is_subordinate != 1) {
//                    $nodes[$field->id] = $field->name;
                    $nodes = $this->_getMandNodesForAssigning($field, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function _getNonMandNodesForAssigning($id, $nodes = array()) {
        $fields = OrganizationalStructureMaster::find('parent_id=' . $id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
// if ($field->mandatory_for_admission != 1) {
                if ($field->is_subordinate != 1) {
                    $nodes[$field->id] = $field->name;
// } else {
                    $nodes = $this->_getNonMandNodesForAssigning($field->id, $nodes);
                }
            }
        endif;
        return $nodes;
    }

}
