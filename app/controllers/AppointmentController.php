<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class AppointmentController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function newAppointmentAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Appointment | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appointment/appointment.js');
        $this->assets->addJs('js/approval-view/approvalview.js');
        $this->view->form = $form = new StaffProfileForm(null);
    }

    public function loadSubtreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost() && $this->request->getPost('orgvalueid') != '') {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
//            echo 'parent_id = ' . $orgvalueid . ' and  cycle_node =2 GROUP BY  org_master_id ';exit;
            $this->view->org_value = $org_value = OrganizationalStructureValues::find('parent_id = ' . $orgvalueid
                            . ' GROUP BY  org_master_id ');
//             $this->view->org_mas_value = $org_mas_value = OrganizationalStructureMaster::find('parent_id = '.$orgvalueid);
        }
    }

    public function saveStaffAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $form = new StaffProfileForm(null);
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        try {
            if ($this->request->isPost()) {
                $params = $queryParams = array();
                foreach ($this->request->getPost() as $key => $value) {
                    $IsSubdiv = explode('_', $key);
                    if ($IsSubdiv[0] == 'aggregate' && $value) {
                        $params['aggregateids'][] = $value;
                    } else {

                        $params[$key] = $value;
                    }
                }
                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $messages) {
                        $error .= $messages . '<br>';
                    }

                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                } else {

                    //photo check

                    $fieldphoto = StaffProfileSettings::findFirst('column_name ="photo"');
                    $addphoto1 = StaffProfileSettings::findFirst('column_name ="address_proof1"');
                    $addphoto2 = StaffProfileSettings::findFirst('column_name ="address_proof2"');
                    $addphoto3 = StaffProfileSettings::findFirst('column_name ="address_proof3"');

                    $staff = new StaffInfo();
                    $staff_add_info = new StaffGeneralMaster();
                    if ($this->request->hasFiles() == true && count($this->request->getUploadedFiles()) < 5) {
                        // Print the real file names and sizes
                        foreach ($this->request->getUploadedFiles() as $file) {
                            $ext = explode('/', $file->getType());
                            if ($ext[0] == 'image') {
                                $filename = md5(uniqid(rand(), true)) . '-' . strtolower($file->getname());
                                $file->moveTo(FILES_DIR . $filename);
                                $fieldname = $file->getKey();
                                $staff_add_info->{$fieldname} = $filename;
                            } else {
                                $errmsg .='Invalid file extension <br>';
                                $err = 1;
                            }
                        }//$fieldphoto->mandatory != 0 && empty($additionalDetails->photo)
                    } else if ((empty($staff_add_info->photo) && $fieldphoto->mandatory != 0 && $fieldphoto->hide_or_show != 0) ||
                            (empty($staff_add_info->address_proof1) && $addphoto1->mandatory != 0 && $addphoto1->hide_or_show != 0 ) ||
                            (empty($staff_add_info->address_proof2) && $addphoto2->mandatory != 0 && $addphoto2->hide_or_show != 0) ||
                            (empty($staff_add_info->address_proof3) && $addphoto3->mandatory != 0 && $addphoto3->hide_or_show != 0)) {
                        $errmsg .='Photo is required <br>';
                        $err = 1;
                    }
                    if (!preg_replace('/^[0-9]/', '', $params['pin'])) {
                        $errmsg .='Please ender valid pin code <br>';
                        $err = 1;
                    }

                    if ($err == 1) {

                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $errmsg . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }

                    $gender = ($params['gender'] == 1) ? 'Female' : 'Male';
                    //Appointment generation
//                    $appointment_prefix = (Settings::findFirstByVariableName('appointment_prefix')->variableValue);

                    $admissionNumPrfix = Settings::findFirstByVariableName('appointment_prefix') ?
                            Settings::findFirstByVariableName('appointment_prefix') : new Settings();
                    $admissionNumSufix = Settings::findFirstByVariableName('appointment_sufix') ?
                            Settings::findFirstByVariableName('appointment_sufix') : new Settings();
                    $prefix = $admissionNumPrfix ? $admissionNumPrfix->variableValue : '';
                    $sufix = $admissionNumSufix ? $admissionNumSufix->variableValue : '';
                    $stringLen = ControllerBase::getStudentPad('staff');
                    $appointment_prefix = $prefix . $stringLen . $sufix;
//        $stfmax = StaffInfo::find()->getLast()->id;
                    $stumax = StaffInfo::find();
                    $adminNumArr = array();

                    foreach ($stumax as $stuAdmin) {
                        $adminNumArr[] = $stuAdmin->appointment_no; //ltrim($stuAdmin->loginid, 's');
                    }

                    $arraySorted = natsort(array_reverse($adminNumArr));
                    $lastId = array_pop($adminNumArr);
                    $asterikcount = 0;
                    $asterikstr = str_replace("*", "", $appointment_prefix, $asterikcount);
                    $formartArr = explode('*', $appointment_prefix);
                    $nextAdminNumber = str_replace($formartArr, "", $lastId);
                    $nextgennum = $nextAdminNumber + 1;
                    $countOfZeros = abs(strlen($nextgennum) - $asterikcount);
                    $nextgennum = str_pad($nextgennum, $asterikcount, "0", STR_PAD_LEFT);
                    $i = 0;
                    $nextAdminNo = '';
                    foreach ($formartArr as $formatStr):
                        if ($formatStr != '') {
                            $nextAdminNo .= $formatStr;
                        } else {
                            if ($i == 0)
                                $nextAdminNo .= $nextgennum;
                            $i = 1;
                        }
                    endforeach;
                    $appointment_no = $nextAdminNo; //$appointment_prefix . ($stfmax ? ($stfmax + 1) : 1);
//echo  $appointment_no;exit;
                    $staff->assign(array(
                        'appointment_no' => $appointment_no,
                        'aggregate_key' => implode(',', $params['aggregateids']),
                        'Staff_Name' => $params['staffName'],
                        'Gender' => $params['gender'],
                        'Date_of_Birth' => $params['dob'] ? strtotime($params['dob']) : '',
                        'Date_of_Joining' => strtotime($params['doa']),
                        'Mobile_No' => $params['phone'],
                        'Email' => $params['email'],
                        'Address1' => $params['address1'],
                        'Address2' => $params['address2'],
                        'State' => $params['state'],
                        'Country' => $params['country'],
                        'Pin' => $params['pin'],
                        'Phone' => $params['phone'],
//                        'loginid' => $loginCreated->data->stflogin,
                        'status' => 'Requested',
                        'biometric_id' => $params['biometric_id'],
                        'pf_no' => $params['pf_no'],
                        'esi_no' => $params['esi_no'],
                    ));

//                print_r($staff);
//                print_r($staff->save());
//                print_r($staff->aggregate_key);
//                print_r($staff->getMessages());exit;
                    if ($staff->save()) {
                        $staff_add_info->staff = $staff;
                        $staff_add_info->catg_id = $params['category'] ? $params['category'] : '';
                        $staff_add_info->nationality = $params['nationality'] ? $params['nationality'] : '';
                        $staff_add_info->qualification = $params['qualification'] ? $params['qualification'] : '';
                        $staff_add_info->blood_group = $params['blood_grp'] ? $params['blood_grp'] : '';
                        $staff_add_info->medical_details = $params['othrMedDet'] ? $params['othrMedDet'] : '';
                        $staff_add_info->spouse_name = $params['sname'] ? $params['sname'] : '';
                        $staff_add_info->s_occupation = $params['soccup'] ? $params['soccup'] : '';
                        $staff_add_info->s_designation = $params['sdesign'] ? $params['sdesign'] : '';
                        $staff_add_info->s_phoneno = $params['sphone'] ? $params['sphone'] : '';
                        $staff_add_info->parent_name = $params['gname'] ? $params['gname'] : '';
                        $staff_add_info->p_occupation = $params['goccup'] ? $params['goccup'] : '';
                        $staff_add_info->p_designation = $params['gdesign'] ? $params['gdesign'] : '';
                        $staff_add_info->p_phoneno = $params['gphone'] ? $params['gphone'] : '';
                        $staff_add_info->classes_handling = $params['classHandl'] ? $params['classHandl'] : '';
                        $staff_add_info->subjects_handling = $params['subjHandl'] ? $params['subjHandl'] : '';
                        $staff_add_info->extra_curricular = $params['exActi'] ? $params['exActi'] : '';
                        $staff_add_info->co_curricular = $params['coActi'] ? $params['coActi'] : '';
                        $staff_add_info->additional_qualifications = $params['addQuali'] ? $params['addQuali'] : '';
                        $staff_add_info->experience = $params['prevExp'] ? $params['prevExp'] : '';
                        $staff_add_info->organization = $params['Org'] ? $params['Org'] : '';
                        $staff_add_info->designation = $params['designation'] ? $params['designation'] : '';
                        $staff_add_info->comments = $params['comments'] ? $params['comments'] : '';

                        $staff_add_info->created_by = $uid;
                        $staff_add_info->created_date = time();
                        $staff_add_info->modified_by = $uid;
                        $staff_add_info->modified_date = time();
                        if ($staff_add_info->save()) {
                            $message['type'] = 'success';
                            $message['ItemID'] = $staff->id;
                            $message['message'] = '<div class="alert alert-block alert-success fade in">New appointment requested successfully</div>';
                            print_r(json_encode($message));
                            exit;
                        } else {
                            foreach ($staff_add_info->getMessages() as $messages) {
                                $error .= $messages . '<br>';
                            }

                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {

                        foreach ($staff->getMessages() as $messages) {
                            $error .= $messages . '<br>';
                        }

                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {

            foreach ($e->getMessages() as $messages) {
                $error .= $messages . '<br>';
            }

            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
            /* $this->flashSession->error($e->getMessages());
              return $this->forward('staff/index'); */
        }
    }

    public function appointStaffAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $staffid = $this->request->getPost('itemID');
        try {
            if ($this->request->isPost()) {
                $staff = StaffInfo::findFirstById($staffid);
//                print_r($staff);exit;
                $param = array(
                    'appkey' => APPKEY,
                    'subdomain' => SUBDOMAIN,
                    'businesskey' => BUSINESSKEY);
                $param['slogin'] = 'e' . $staff->appointment_no;
                $param['password'] = date('d/m/Y', ($staff->Date_of_Birth));
                $param['email'] = $staff->Email;
                $data_string = json_encode($param);
                $response = IndexController::curlIt(USERAUTHAPI . 'createStaffLogin', $data_string);
//                   print_r($response);;
                $loginCreated = json_decode($response);
//                print_r($loginCreated);
                if (!$loginCreated->status) {
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                    print_r(json_encode($message));
                    exit;
                } else if ($loginCreated->status == 'ERROR') {
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $loginCreated->messages . '</div>';
                    print_r(json_encode($message));
                    exit;
                } else if ($loginCreated->status == 'SUCCESS' && $loginCreated->data->id > 0) {
                    ##MAIL LOGIN
                    $param = array(
                        'domain' => SUBDOMAIN,
                        'login' => 'e' . $staff->appointment_no,
                        'password' => date('d/m/Y', ($staff->Date_of_Birth)));
                    $mail_data_string = json_encode($param);
//                      print_r($param); 
                    $response = IndexController::curlIt(MAILSERVICEAPI . 'createWebMailUser', $mail_data_string);
//                   print_r($response);;
                    $mailloginCreated = json_decode($response);
//                    print_r($mailloginCreated);
//                    exit;
                    if (!$mailloginCreated->status) {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                    if ($mailloginCreated->status == 'ERROR') {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $mailloginCreated->messages . '</div>';
                        print_r(json_encode($message));
                        exit;
                    } else if ($mailloginCreated->status == 'SUCCESS' && $mailloginCreated->data->id) {

                        $staff->status = "Appointed";
                        $staff->loginid = $loginCreated->data->stflogin;
//                    print_r($staff);exit;

                        if ($staff->save()) {
                            /*
                              $params['username'] = $staff->loginid;
                              $params['password'] = date('d/m/Y', $staff->Date_of_Birth);
                              $params['fullname'] = $staff->Staff_Name;
                              $params['email_address'] = $staff->Email;
                              $params['rolenames'] = 'Staff';
                              $params['aggregateid'] = $staff->aggregate_key;
                              //                            print_r($params);
                              // print_r(CALENDARAPI . 'createUserPricipal');
                              $data_string = json_encode($params);
                              $responseParam = IndexController::curlIt(CALENDARAPI . 'createUserPricipal', $data_string);
                              //                             print_r($responseParam);exit;
                              $calloginCreated = json_decode($responseParam);
                              if (!$calloginCreated->status) {
                              $message['type'] = 'error';
                              $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                              print_r(json_encode($message));
                              exit;
                              } else if ($calloginCreated->status == 'ERROR') {
                              $message['type'] = 'error';
                              $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $loginCreated->messages . '</div>';
                              print_r(json_encode($message));
                              exit;
                              } else if ($calloginCreated->status == 'SUCCESS' && $calloginCreated->data->id > 0) { */
                            $message['type'] = 'success';
                            $message['message'] = $messageparam = '<div class="alert alert-block alert-success fade in"> Staff Appointed Successfully !<br><br>'
                                    . '<table class="table  table-hover general-table table-bordered "><tr><td>Name </td><td>'
                                    . $staff->Staff_Name . '</td></tr>
                                          <tr><td>Login Id </td><td>' . $staff->loginid . '</td></tr>
                                          </table> </div>';
                            print_r(json_encode($message));
                            exit;
//                            }
//                        $dispatcherParams['username'] = $staff->loginid;
//                        $dispatcherParams['password'] = date('d/m/Y', $staff->Date_of_Birth);
//                        $dispatcherParams['fullname'] = $staff->Staff_Name;
//                        $dispatcherParams['email_address'] = $staff->Email;
//                        $dispatcherParams['messageParams'] = $messageparam;
//                        $dispatcherParams['rolenames'] = 'Staff';
//                        $dispatcherParams['aggregateid'] = $staff->aggregate_key;
//
//                        $title = $this->dispatcher->setParams($dispatcherParams);
                        } else {

                            foreach ($staff->getMessages() as $messages) {
                                $error .= $messages . '<br>';
                            }

                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                }
            }
        } catch (Exception $e) {

            foreach ($e->getMessages() as $messages) {
                $error .= $messages . '<br>';
            }

            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
            /* $this->flashSession->error($e->getMessages());
              return $this->forward('staff/index'); */
        }
    }

    public function changeApprovalStatusAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        try {
            if ($this->request->isPost()) {

                $appln = StaffInfo::findFirstById($this->request->getPost('itemID'));
                $appln->status = $this->request->getPost('status');
                if ($appln->save()) {
                    $message['type'] = 'success';
                    $message['message'] = $appln->status . ' Successfully!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    foreach ($appln->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        }
    }

}
