<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class ProfileController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function studentProfileAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Application | ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs('js/appscripts/application.js');
        $this->assets->addJs('js/appscripts/studentprofile.js');
        $this->view->frmreset = $this->request->getPost('frmreset') ? $this->request->getPost('frmreset') : '';
        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadtableHeaderAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadSubtreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost() && $this->request->getPost('orgvalueid') != '') {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
            $this->view->org_value = $org_value = OrganizationalStructureValues::find('parent_id = ' . $orgvalueid
                            . ' GROUP BY  org_master_id ');
        }
    }

    public function loadtableDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = $queryParams1 = array();
        $orderphql = array();
        foreach ($this->request->getPost() as $key => $value) {
            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'aggregate' && $value) {
                $params['aggregateids'][] = $value;
            } else {

                $params[$key] = $value;
            }
        }
        $currectacdyr = ControllerBase::get_current_academic_year();
        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $queryParams1[] = 'stuinfo.Admission_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stuinfo.Student_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stuinfo.Gender Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stuinfo.loginid Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stuinfo.Email Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stuhistory.status Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stuinfo.Phone Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stuinfo.m_phone_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stuinfo.f_phone_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'stuinfo.g_phone Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;
//        $queryParams[] = 'stuhistory.aggregate_key Like "%' . $currectacdyr->id . '%"';
        if (isset($params['aggregateids']) && count($params['aggregateids']) > 0):
//            $queryParams[] = 'stuhistory.aggregate_key Like "%' . implode(',', $params['aggregateids']) . '%"';
            foreach ($params['aggregateids'] as $value) {
                $queryParams[] = 'FIND_IN_SET (' . $value . ', stuhistory.aggregate_key ) ';
            }
        endif;

        if (isset($params['loginid']) && $params['loginid'] != ''):
            $queryParams[] = 'stuinfo.loginid  Like "%' . $params['loginid'] . '%"';
        endif;

        if (isset($params['name']) && $params['name'] != ''):
            $queryParams[] = 'stuinfo.Student_Name  Like "%' . $params['name'] . '%"';
        endif;

        if (isset($params['status']) && $params['status'] != ''):
            $queryParams[] = 'stuhistory.status  Like "%' . $params['status'] . '%"';
        endif;

        $whereQuery = count($queryParams) > 0 ? ('WHERE ' . implode(' and ', $queryParams)) : '';
//        echo $whereQuery;exit;
        $phql_limited = 'SELECT stuinfo.id,stuinfo.Admission_no,stuinfo.application_no,stuinfo.Student_Name,
            		stuinfo.loginid,stuinfo.parent_loginid,stuinfo.Date_of_Birth,stuinfo.Gender,
                        stuhistory.aggregate_key,stuhistory.status,stuinfo.photo
                                        FROM StudentInfo stuinfo
                                        JOIN StudentHistory stuhistory ON stuinfo.id=stuhistory.student_info_id ' . $whereQuery;

        $phql_full = 'SELECT COUNT(*) as cnt
            FROM StudentInfo stuinfo
            JOIN StudentHistory stuhistory ON stuinfo.id=stuhistory.student_info_id ' . $whereQuery;

        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $orderphql[] = $this->getSortColumnName($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }

        $phql_limited .= ' ORDER BY stuinfo.Student_Name,' . implode(',', $orderphql);
        $phql_limited .= ' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');
        $expectedres = $this->modelsManager->executeQuery($phql_limited);
//        print_r($expectedres);exit;
        $expectedcount = $this->modelsManager->executeQuery($phql_full);


        $rowEntries = $this->formatTableData($expectedres);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($expectedres),
            "iTotalDisplayRecords" => $expectedcount[0]['cnt'],
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnName($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "stuinfo.Admission_no";
        }
        $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $mandnode = $this->_getMandNodesForAssigning($acdyrMas);
        $case = 4 + count($mandnode);
        switch ($sortColumnIndex) {

            case 0:
                return "stuinfo.Student_Name";
            case 1:
                return "stuinfo.Admission_no";
            case 2:
                return "stuinfo.Date_of_Birth";
            case 3:
                return "stuinfo.Gender";

            case $case:
                return "stuinfo.loginid";
            case $case + 1:
                return "stuinfo.parent_loginid";
            case $case + 2:
                return "stuhistory.status";
            default:
                return "stu.Admission_no";
        }
    }

    public function formatTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):

                $row = array();
                $row['Admission_no'] = $items->Admission_no;
                $row['Student_Name'] = $items->Student_Name;
                $row['Date_of_Birth'] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';
                $row['stulogin'] = $items->loginid;
                $row['parntlogin'] = $items->parent_loginid;
                $row['email'] = $items->loginid ? $items->loginid . '@' . SUBDOMAIN . '.edusparrow.com' : '-';
                $row['status'] = $items->status;
                $row['stuid'] = $items->id;
                $row['Actions'] = (in_array($items->status, array('Inclass', 'Admitted'))) ? '<a href="javascript:;" onclick="studentprofileSettings.loadeditprofile(' . $items->id . ')"> 
                                                <span class="fa fa-edit mini-stat-icon-action pink"  title="Edit Profile">                                         
                                                </span>
                                            </a>  ' : '';

                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';

                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }

                if ($items->photo) {
                    $row['Student_Name'] = '<span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Student_Name'] .= $this->tag->image(array(
                        "src" => $items->photo ? (FILES_URI . $items->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Student_Name'] .= '</span>';
                } else {
                    $row['Student_Name'] = ' <span title="' . $items->Admission_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;" >' . substr($items->Student_Name, 0, 1) . '</span>';
                }
                $row['Student_Name'] .= '<span title="' . $items->Student_Name . '"  class="name"> 
                                      ' . (strlen($items->Student_Name) > 15 ? substr($items->Student_Name, 0, 15) . "..." : $items->Student_Name) . '</span>';
                // $row['Student_Name'] .= '<span title="' . $items->Admission_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                       ' . $items->Student_Name. '</span>';


                $row['Print'] = '-';
                if ($items->status == 'Relieved') {
                    $row['Print'] = '<a target="_blank" href="' . $this->url->get() . 'profile/studentTc?studentid=' . $items->id . '" >
                                 <span class="mini-stat-icon tar"><i class="fa fa-print"></i></span> </a>';
                }


//                $row['Actions'] = '<a href="' . $this->url->get('profile/editStudentProfile?student_id=' . $items->id) . '"> 
//                                                <span class="fa fa-pencil"  title="Edit Profile">                                         
//                                                </span>
//                                            </a>  ';
//                $row['view'] = '<a href="javascript:;" onclick="studentprofileSettings.loadacademicprofile(' . $items->id . ')"> 
//                                                <span class="fa fa-mortar-board"  title="View academic profile">                                         
//                                                </span>
//                                            </a>  ';
                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    public function editStudentProfileAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Student Profile | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/appscripts/studentprofile.js');


        $this->view->student = $student = StudentInfo::findFirstById($this->request->get('student_id'));
        $this->view->student_id = $student->id;
        $this->view->academicdet = StudentMapping::findFirst('student_info_id=' . $student->id);

        if ($this->request->isGet()) {
            $this->view->form = new StudentProfileForm($this->view->student, array('edit' => true));
        }
        $hideOrShowFields = StudentProfileSettings::find("hide_or_show = 1");
        foreach ($hideOrShowFields as $hideOrShow) {
            $fieldArr[$hideOrShow->id] = $hideOrShow->form_name;
        }
    }

    public function updatestudentAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();

        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        try {
            if ($this->request->isPost()) {

//                 print_r($this->request->getUploadedFiles()); 	
//                 print_r($this->request->getPost()); 			  	   
//                exit;
                $params = $queryParams = array();
                foreach ($this->request->getPost() as $key => $value) {
                    $IsSubdiv = explode('_', $key);
                    if ($IsSubdiv[0] == 'aggregate' && $value) {
                        $params['aggregateids'][] = $value;
                    } else {

                        $params[$key] = $value;
                    }
                }
                $sform = new StudentProfileForm(null);
//                print_r($this->request->getPost());
//                exit;
                if ($sform->isValid($this->request->getPost()) == false) {
                    foreach ($sform->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = '' . $error . '';
                    print_r(json_encode($message));
                    exit;
                } else {

                    $errmessage = '';
                    $admission = StudentInfo::findFirstById($this->request->getPost('student_id'));
                    if ($this->request->hasFiles() == true) {
                        //photo check
                        $fieldphoto = StudentProfileSettings::findFirst('column_name ="photo"');
                        $fieldfamphoto = StudentProfileSettings::findFirst('column_name ="family_photo"');

                        // Print the real file names and sizes
                        $othr_photos = array();

//                        print_r($this->request->getUploadedFiles());
//                        exit;

                        foreach ($this->request->getUploadedFiles() as $file) {
                            $ext = explode('/', $file->getType());
//                            	echo $ext .'<br>';



                            $fieldname = $file->getKey();
                            $expld_field = explode('_', $fieldname);
                            ;
//                                print_r($fieldname);
                            if ($expld_field[0] == 'other') {
                                //echo 'in';
                                $filename = date('d-m-Y') . '@' . strtolower($file->getname());
                                $file->moveTo(FILES_DIR . $filename);
                                $othr_photos[] = $filename;
                            } else {
                                if ($ext[0] == 'image') {
                                    //echo 'else';
                                    $filename = md5(uniqid(rand(), true)) . '-' . strtolower($file->getname());
                                    $file->moveTo(FILES_DIR . $filename);
                                    $admission->{$fieldname} = $filename;
                                } else {
                                    $errmessage .= 'Invalid file extension ' . "\n";
                                }
                            }
                        }
                        $admission->other_photos = (count($othr_photos) > 0) ? implode(',', $othr_photos) : '';
                    } else {
                        if (($fieldphoto->hide_or_show != 0 && $fieldphoto->mandatory != 0 && empty($admission->photo)) ||
                                ($fieldfamphoto->hide_or_show != 0 && $fieldphoto->mandatory != 0 && empty($admission->family_photo))) {
                            $errmessage .='Photo is required ' . "\n";
                        }
                    }


                    if ($errmessage != '') {
                        $message['type'] = 'error';
                        $message['message'] = $errmessage;
                        print_r(json_encode($message));
                        exit;
                    }
//                    $admission->Admission_no = $params['admission_no'];
                    $admission->Student_Name = $params['studentName'];
                    $admission->Gender = $params['gender'];
                    $admission->Date_of_Birth = strtotime($params['dob']);
                    $admission->Date_of_Joining = strtotime($params['doj']);
                    $admission->Address1 = $params['address1'];
                    $admission->Address2 = $params['address2'];
                    $admission->State = $params['state'];
                    $admission->Country = $params['country'];
                    $admission->Pin = $params['pin'];
                    $admission->Phone = $params['phone']; //used for mobile login
                    $admission->Email = $params['email'];
                    $admission->place_of_birth = $params['pob'] ? $params['pob'] : '';
                    $admission->nationality = $params['nationality'] ? $params['nationality'] : '';
                    $admission->religion = $params['religion'] ? $params['religion'] : '';
                    $admission->caste_category = $params['caste_category'] ? $params['caste_category'] : '';
                    $admission->caste = $params['caste'] ? $params['caste'] : '';
                    $admission->first_language = $params['language'] ? $params['language'] : '';
                    $admission->father_name = $params['fname'] ? $params['fname'] : '';
                    $admission->f_occupation = $params['foccup'] ? $params['foccup'] : '';
                    $admission->f_designation = $params['fdesign'] ? $params['fdesign'] : '';
                    $admission->f_phone_no = $params['fphone'] ? $params['fphone'] : '';
                    $admission->f_phone_no_status = $params['fcontact'] ? $params['fcontact'] : '';
                    $admission->mother_name = $params['mname'] ? $params['mname'] : '';
                    $admission->m_occupation = $params['moccup'] ? $params['moccup'] : '';
                    $admission->m_designation = $params['mdesign'] ? $params['mdesign'] : '';
                    $admission->m_phone_no = $params['mphone'] ? $params['mphone'] : '';
                    $admission->m_phone_no_status = $params['mcontact'] ? $params['mcontact'] : '';
                    $admission->other_guardian_name = $params['gname'] ? $params['gname'] : '';
                    $admission->g_occupation = $params['goccup'] ? $params['goccup'] : '';
                    $admission->g_designation = $params['gdesign'] ? $params['gdesign'] : '';
                    $admission->g_phone = $params['gphone'] ? $params['gphone'] : '';
                    $admission->g_phone_no_status = $params['gcontact'] ? $params['gcontact'] : '';
                    $admission->person_school_fee = $params['feepayee'] ? $params['feepayee'] : '';
                    $admission->circumtances = $params['famcir'] ? $params['famcir'] : '';
                    $admission->sibling = $params['siblings'] ? $params['siblings'] : '';
                    $admission->blood_group = $params['blood_grp'] ? $params['blood_grp'] : '';
                    $admission->height = $params['height'] ? $params['height'] : '';
                    $admission->weight = $params['weight'] ? $params['weight'] : '';
                    $admission->allergic = $params['drugallergic'] ? $params['drugallergic'] : '';
                    $admission->chronic = $params['chronicill'] ? $params['chronicill'] : '';
                    $admission->family_doctor = $params['docDet'] ? $params['docDet'] : '';
                    $admission->previous_school_name = $params['prevSchoolName'] ? $params['prevSchoolName'] : '';
                    $admission->previous_school_state = $params['prevState'] ? $params['prevState'] : '';
                    $admission->previous_school_country = $params['pprevcountry'] ? $params['pprevcountry'] : '';
                    $admission->attended_from = $params['attfrom'] ? strtotime($params['attfrom']) : '';
                    $admission->attended_to = $params['attto'] ? strtotime($params['attto']) : '';
                    $admission->achievements = $params['achievements'] ? $params['achievements'] : '';
                    $admission->comments = $params['comments'] ? $params['comments'] : '';
                    $admission->other_details = $params['odet'] ? $params['odet'] : '';
                    $admission->f_education = $params['f_education'] ? $params['f_education'] : '';
                    $admission->m_education = $params['m_education'] ? $params['m_education'] : '';

                    $admission->family_income = $params['family_income'] ? $params['family_income'] : '';

                    $admission->created_by = $uid;
                    $admission->created_date = time();
                    $admission->modified_by = $uid;
                    $admission->modified_date = time();
                    $admission->transport = $params['transport'] ? $params['transport'] : '';
//                    print_r($admission);
//                    exit;
                    if ($admission->save()) {

                        if (isset($admission->application_no) && ($admission->application_no != 0)) {
                            $application_status_change = Application::findFirstByApplicationNo($admission->application_no);
                            $application_status_change->key_in_status = 'Completed';
                            $application_status_change->modified_by = $uid;
                            $application_status_change->modified_on = time();
                            $application_status_change->Student_Name = $admission->Student_Name;
                            $application_status_change->Gender = $admission->Gender;
                            $application_status_change->Date_of_Birth = $admission->Date_of_Birth;
                            $application_status_change->Address1 = $admission->Address1;
                            $application_status_change->Address2 = $admission->Address2;
                            $application_status_change->State = $admission->State;
                            $application_status_change->Country = $admission->Country;
                            $application_status_change->Pin = $admission->Pin;
                            $application_status_change->Phone = $admission->Phone;
                            $application_status_change->Email = $admission->Email;

                            if ($application_status_change->save()) {
                                $message['type'] = 'success';
                                $message['message'] = 'Student details updated successfully';
                                print_r(json_encode($message));
                                exit;
                            } else {
                                foreach ($application_status_change->getMessages() as $messages) {
                                    $error .= $messages . "\n";
                                }
                                $message['type'] = 'error';
                                $message['message'] = '' . $error . '';
                                print_r(json_encode($message));
                                exit;
                            }
                        } else {
				
			//change user mobile login number 
$data = array(
                                'login' => $admission->loginid, 
                                'phone' => $params['phone'], 
                                'appkey' => APPKEY,
                                'subdomain' => SUBDOMAIN,
                                'businesskey' => BUSINESSKEY 
                            );
                            $data_string = json_encode($data);   
                            $responseParam = IndexController::curlIt(USERAUTHAPI . 'changeLoginNumber', $data_string); 
//print_r( $responseParam );exit;              
$usrResponse = json_decode($responseParam); 
                            if (!$usrResponse->status) {
                                $this->view->servererror = 'Internal server error';
                            }
                            if ($usrResponse->status == 'ERROR') {
                                $this->view->servererror = $usrResponse->messages;
                            } else if ($usrResponse->status == 'SUCCESS' && $usrResponse->data->id > 0) {
                            $message['type'] = 'success';
                            $message['message'] = 'Student details updated successfully';
                            print_r(json_encode($message));
                            exit;
			}
                        }
                    } else {
                        foreach ($admission->getMessages() as $messages) {
                            $error .= $messages . "\n";
                        }
                        $message['type'] = 'error';
                        $message['message'] = '' . $error . '';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '' . $error . '';
            print_r(json_encode($message));
            exit;
        }
    }

    public function studentTcAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Student Profile | ");

        if ($this->request->isGet()) {

            $studentId = $this->request->get('studentid');

            $this->view->schoolinfo = ControllerBase::_getSchoolVariables();

            $this->view->stuinfo = StudentInfo::findFirstById($studentId);

//             print_r($stuinfo);
//             exit;
            $this->view->sturelievinfo = StudentRelievingMaster::findFirstByStudentId($studentId);
        }
    }

    //functions

    public function _getNonMandNodesForAssigning($id, $nodes = array()) {
        $fields = OrganizationalStructureMaster::find('parent_id=' . $id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
                // if ($field->mandatory_for_admission != 1) {
                if ($field->is_subordinate != 1) {
                    $nodes[$field->id] = $field->name;
                    // } else {
                    $nodes = $this->_getNonMandNodesForAssigning($field->id, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function _getMandNodesForAssigning($acdyrMas, $nodes = array()) {

        $nodes[$acdyrMas->id] = $acdyrMas->name;
        //  echo $acdyrMas->name;
        $fields = OrganizationalStructureMaster::find('parent_id=' . $acdyrMas->id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
                //if ($field->mandatory_for_admission == 1) {
                if ($field->is_subordinate != 1) {
//                    $nodes[$field->id] = $field->name;
                    $nodes = $this->_getMandNodesForAssigning($field, $nodes);
                }
            }
        endif;
        return $nodes;
    }

}
