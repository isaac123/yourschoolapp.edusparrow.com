<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class HomeWorkController extends ControllerBase {

    protected function initialize() {
//        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function addStudentHomeworkAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $identity = $this->auth->getIdentity();
        if (in_array('Staff', $identity['role_name'])) {
            $param['type'] = 'mainexam_mark';
            $getCombination = ControllerBase::loadCombinations($param);
            $this->view->combinations = $getCombination[1];
            $this->view->type = $getCombination[2];
            $this->view->errorMessage = $getCombination[0];
        } else {
            $this->view->errorMessage = 'Only teachers have access to this page!';
        }
    }

    public function loadSubtreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost() && $this->request->getPost('orgvalueid') != '') {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
            $this->view->org_value = $org_value = OrganizationalStructureValues::find('parent_id = ' . $orgvalueid
                            . ' GROUP BY  org_master_id ');
        }
    }

    public function loadHomeWorkSubjectAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_rating = 1;
        if ($this->request->isPost()) {
            $subject_masterid = $this->request->getPost('subject_masterid');
            $subject_master = GroupSubjectsTeachers::findfirst('id = ' . $subject_masterid);
            $this->view->subject_id = isset($subject_master->subject_id) ? $subject_master->subject_id : '';
        }
    }

    public function addClassteacherHomeworkAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $identity = $this->auth->getIdentity();
        if (in_array('Staff', $identity['role_name'])) {
            $param['type'] = 'homework';
            $getCombination = ControllerBase::loadCombinations($param);
            $this->view->combinations = $getCombination[1];
            $this->view->type = $getCombination[2];
            $this->view->errorMessage = $getCombination[0];
        } else {
            $this->view->errorMessage = 'Only teachers have access to this page!';
        }
    }

    public function saveHomeworkAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $identity = $this->auth->getIdentity();
            $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
            foreach ($this->request->getPost() as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'aggregate' && $value) {
                    $params['aggregateids'][] = $value;
                } else {
                    $params[$key] = $value;
                }
            }
            $subject_master = GroupSubjectsTeachers::findfirst('id = ' . $params['subject_masterid']);
            $classmaster = ClassroomMaster::findFirstById($subject_master->classroom_master_id);
            $subjids = ControllerBase::getGrpSubjMasPossiblities(explode('-', $classmaster->aggregated_nodes_id));
            if ($subject_master->subject_id > 0) {
                $queryParams[] = 'subjct_modules =' . $subject_master->subject_id;
            }
            if (count($subjids) > 0) {
                $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
            }

//            $query[] = ' subjct_modules = ' . $params['aggregateids'][count($params['aggregateids']) - 1];
            $queryParams[] = ' hmwrkdate >= "' . strtotime($params['date'] . ' 00:00:00')
                    . '" and hmwrkdate <= "' . strtotime($params['date'] . ' 23:59:59') . '"';
            $condtn = implode(' AND ', $queryParams);
//            echo $condtn;exit;
            $exists = HomeWorkTable::find(array($condtn,
                        'columns' => 'COUNT(*) as cnt'));
//            print_r($exists);exit;
            if ($exists[0]->cnt > 0) {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in"> Home work already added today.</div>';
                print_r(json_encode($message));
                exit;
            } else {
                $subjid = $params['aggregateids'][count($params['aggregateids']) - 1];
                $homeworkdet = new HomeWorkTable();
                $homeworkdet->grp_subject_teacher_id = $params['subject_masterid'];
                $homeworkdet->subjct_modules = $subjid;
                $homeworkdet->homework = $params['homeworkcontnt'];
                $homeworkdet->title = $params['title'];
                $homeworkdet->hmwrkdate = strtotime($params['date']);
                $homeworkdet->created_by = $uid;
                $homeworkdet->created_date = time();
//            $assignment->modified_by = $identity['id'];
//            $assignment->modified_date = time();
                if ($homeworkdet->save()) {
                    $message['type'] = 'success';
                    $message['ItemID'] = $homeworkdet->id;
                    $message['message'] = '<div class="alert alert-block alert-success fade in">Home Work Added Successfully</div>';
                    print_r(json_encode($message));
                    exit;
                } else {

                    foreach ($homeworkdet->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function editHomeworkAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->id = $id = $this->request->getPost('id');
            $this->view->homework_value = HomeWorkTable::findFirst('id = ' . $id);
        }
    }

    public function updateeditHomeworkAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $identity = $this->auth->getIdentity();
            foreach ($this->request->getPost() as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'aggregate' && $value) {
                    $params['aggregateids'][] = $value;
                } else {
                    $params[$key] = $value;
                }
            }
//            echo '<pre>';
//            print_r($params);
//            exit;


            $homeworkdet = HomeWorkTable::findFirstById($params['id']);
            $homeworkdet->homework = $params['homework'];
            $homeworkdet->hmwrkdate = strtotime($params['date']);
            $homeworkdet->modified_by = $identity['id'];
            $homeworkdet->modified_date = time();


            if ($homeworkdet->save()) {
                $message['type'] = 'success';
                $message['ItemID'] = $homeworkdet->id;
                $message['message'] = '<div class="alert alert-block alert-success fade in">Home Work Added Successfully</div>';
                print_r(json_encode($message));
                exit;
            } else {

                foreach ($homeworkdet->getMessages() as $messages) {
                    $error .= $messages;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function loadHomeWorkBYStudentAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->aggregate_key = $aggregate_key = $this->request->getPost('aggregate_key');
            $this->view->student = $student = $this->request->getPost('student');

            $this->view->searchdate = $searchdate = $this->request->getPost('searchdate') ? $this->request->getPost('searchdate') : '';
            $this->view->searchsubject = $searchsubject = $this->request->getPost('searchsubject') ? $this->request->getPost('searchsubject') : '';

            $get_Query_For_Subjct = ControllerBase::buildSubjectQuery($aggregate_key);

            $subdivval = GroupSubjectsTeachers::find(implode(' or ', $get_Query_For_Subjct));
            $expld_val = explode(',', $aggregate_key);
            $grpsubids = $subjIds = $id_val = array();
            echo '<pre>';
            foreach ($subdivval as $nodes) {
                $res = ControllerBase::getCommonIdsForKeys($nodes->aggregated_nodes_id);

                foreach ($res as $set) {
                    $arrayfdiff = array_diff(explode('-', $set), $expld_val);
                    if (count($arrayfdiff) == 0) {
                        $grpsubids[] = $nodes->id;
                        $subjIds[] = $nodes->subject_id;
                    }
                }
            }

            $expldval = explode(',', $aggregate_key);
            $currentQury = array();

            $currentQury[] = '( parent_id IN (' . implode(',', $expldval) . '))';

            $subjects_mas = OrganizationalStructureMaster::find(array(
                        'columns' => 'GROUP_CONCAT(id) as ids',
                        ' module = "Subject" and is_subordinate = 1 '
            ));
//            print_r($subjects_mas);
//            exit;
            $subjects = OrganizationalStructureValues::find('org_master_id IN (' . $subjects_mas[0]->ids . ') '
                            . ' and (' . implode(' or ', $currentQury) . ')');

            $this->view->orgvals = $subjects;
            $subj_Ids = array();
            foreach ($subjects as $nodes) {
                $subj_Ids[] = $nodes->id;
            }
            $subjids = array_unique($grpsubids);

            $queryParams = array();
            if ($searchdate == '') {

                $date = date('d-m-Y');
                $queryParams[] = 'hmwrkdate >= "' . strtotime($date . ' 00:00:00') . '" and hmwrkdate <= "' . strtotime($date . ' 23:59:59') . '"';
            } else {
                $queryParams[] = 'hmwrkdate >= "' . strtotime($searchdate . ' 00:00:00') . '" and hmwrkdate <= "' . strtotime($searchdate . ' 23:59:59') . '"';
            }

            if (count($subj_Ids) > 0) {
                $queryParams[] = 'subjct_modules IN(' . implode(',', $subj_Ids) . ')';
            }

            if (count($subjids) > 0) {
                $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
            }


            $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
            $this->view->homework_value = (count($subj_Ids) > 0) ? (HomeWorkTable::find($conditionvals)) : '';
            $this->view->subj_ids = $subj_ids = HomeWorkTable::find(array(
                        $conditionvals,
                        'columns' => 'GROUP_CONCAT(subjct_modules) as subjmodls'
            ));
        }
    }

    public function loadHomeWorkBYStudentDateAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {

            $params = $queryParams = $queryParams1 = array();
            $orderphql = array();
            foreach ($this->request->getPost() as $key => $value) {
                $params[$key] = $value;
            }
            $this->view->aggregate_key = isset($params['aggregate_key']) ? $params['aggregate_key'] : '';
            $aggregate_key = explode(',', $this->view->aggregate_key);
            $this->view->student = $student = isset($params['student']) ? $params['student'] : '';
            $this->view->searchdate = $searchdate = isset($params['searchdate']) ? $params['searchdate'] : date('d-m-Y');
            $this->view->searchsubject = $searchsubject = isset($params['searchsubject']) ? $params['searchsubject'] : '';
// print_r($aggregate_key);exit;
            $subjpids = ControllerBase::getAlSubjChildNodes($aggregate_key);
            $subjids = ControllerBase::getGrpSubjMasPossiblities($aggregate_key);
            $subjects = ControllerBase::getAllPossibleSubjects($subjids);
            $this->view->subjects = $subjects;
            $subj_Ids = array();
            foreach ($subjects as $nodes) {
                $subj_Ids[] = $nodes->id;
            }
            if (isset($searchdate) && $searchdate != '') {
                $queryParams[] = 'hmwrkdate >= "' . strtotime($searchdate . ' 00:00:00') . '" and hmwrkdate <= "' . strtotime($searchdate . ' 23:59:59') . '"';
            }
            if ($searchsubject && $searchsubject > 0) {
                $queryParams[] = 'subjct_modules = ' . $searchsubject;
            } else if (count($subj_Ids) > 0) {
                $queryParams[] = 'subjct_modules IN(' . implode(',', $subj_Ids) . ') ';
            }

            if (count($subjids) > 0) {
                $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
            }
            $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
//                print_r($queryParams);
//                exit;
            $this->view->homework_value = HomeWorkTable::find($conditionvals);
        }
    }

    public function loadHomeWorkBYClassAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {

            $params = $queryParams = $queryParams1 = array();
            $orderphql = array();
            foreach ($this->request->getPost() as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'aggregate' && $value) {
                    $params['aggregateids'][] = $value;
                } else {

                    $params[$key] = $value;
                }
            }

            $subjpids = ControllerBase::getAlSubjChildNodes($params['aggregateids']);
            $subjids = ControllerBase::getGrpSubjMasPossiblities($subjpids);
            $subjects = ControllerBase::getAllPossibleSubjects($subjids);
            $this->view->subjects = $subjects;
            $subj_Ids = array();
            foreach ($subjects as $nodes) {
                $subj_Ids[] = $nodes->id;
            }

            if (isset($params['hdate']) && $params['hdate'] != '') {
                $queryParams[] = 'hmwrkdate >= "' . strtotime($params['hdate'] . ' 00:00:00')
                        . '" and hmwrkdate <= "' . strtotime($params['hdate'] . ' 23:59:59') . '"';
            }
            if (count($subj_Ids) > 0) {
                $queryParams[] = 'subjct_modules IN(' . implode(',', $subj_Ids) . ')';
            }
            if (count($subjids) > 0) {
                $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
            }
            $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
//                print_r($conditionvals);
//                exit;
            $this->view->homework_value = (count($subj_Ids) > 0) ? HomeWorkTable::find($conditionvals) : 0;
        }
    }

    public function _getAllChildren($aids, $subjects_mas_ids, $subjids = array()) {
        $subjids[] = $aids;
        if (count($aids) > 0) {
            $nxt = OrganizationalStructureValues::find(array('columns' => 'group_concat(id) as ids',
                        'parent_id  IN ( ' . $aids . ') and org_master_id  IN (' . $subjects_mas_ids . ')'
            ));
        }

        if ($nxt[0]->ids != '') {
            $idsnxt = $nxt[0]->ids;
            $subjids = HomeWorkController::_getAllChildren($idsnxt, $subjects_mas_ids, $subjids);
        }

        return $subjids;
    }

    public function sendHomeWorkReportAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {

            $params = $queryParams = $queryParams1 = array();
            $orderphql = array();
            foreach ($this->request->getPost() as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'aggregate' && $value) {
                    $params['aggregateids'][] = $value;
                } else {

                    $params[$key] = $value;
                }
            }

            $subjpids = ControllerBase::getAlSubjChildNodes($params['aggregateids']);
            $subjids = ControllerBase::getGrpSubjMasPossiblities($subjpids);
            $subjects = ControllerBase::getAllPossibleSubjects($subjids);
            $this->view->subjects = $subjects;
            $subj_Ids = array();
            foreach ($subjects as $nodes) {
                $subj_Ids[] = $nodes->id;
            }
            if (isset($params['hdate']) && $params['hdate'] != '') {
                $queryParams[] = 'hmwrkdate >= "' . strtotime($params['hdate'] . ' 00:00:00')
                        . '" and hmwrkdate <= "' . strtotime($params['hdate'] . ' 23:59:59') . '"';
            }

            if (count($subj_Ids) > 0) {
                $queryParams[] = 'subjct_modules IN(' . implode(',', $subj_Ids) . ')';
            }
            if (count($subjids) > 0) {
                $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
            }
            $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
//                print_r($conditionvals);
//                exit;
            $this->view->homework_value = $homework_value = (count($subj_Ids) > 0) ? HomeWorkTable::find($conditionvals) : 0;

//                print_r($subj_Ids);exit;

            if ($homework_value != '' && count($homework_value) > 0) {
                $message_content = 'Home Work <br>' . " 
";
                $send_msg_content = 'Home Work <br>';
                foreach ($homework_value as $homework_val) {

                    $template = preg_replace("/[\r\n]+/", "\n", $homework_val->homework);
                    $template = preg_replace("/\s+/", ' ', $template);
                    $send_msg_content .= $template;

                    $orgvaldet = OrganizationalStructureValues::findFirstById($homework_val->subjct_modules);
//                    print_r($orgvaldet); exit;
                    $aggregate_keys = HomeWorkController::_getMandNodesForExam($orgvaldet);
                    $aggregate_keys = array_reverse($aggregate_keys);
                    $message_content .= implode('-', $aggregate_keys) . ' - ';
                    $message_content .= $homework_val->homework . ' <br>' . " 
";
                    $send_msg_content .= implode('-', $aggregate_keys) . ' - ';
                    $send_msg_content .= $template . ' <br>';
                }

                $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", aggregate_key)>0)', $params['aggregateids']);
//                echo 'test';
//                exit;
//            $params['aggregateids']
                $studet = StudentMapping::find(implode(' AND ', $subjQuery));
//            print_r(implode(' AND ', $subjQuery));
//            exit;
                $phoneno_arr = array();
                foreach ($studet as $studetval) {
                    $studata = StudentInfo::findFirstById($studetval->student_info_id);

                    $phoneno = $studata->Phone;

                    if ($studata->f_phone_no_status == 1)
                        $phoneno = $studata->f_phone_no;

                    if ($studata->m_phone_no_status == 1)
                        $phoneno = $studata->m_phone_no;

                    if ($studata->g_phone_no_status == 1)
                        $phoneno = $studata->g_phone;

                    $phoneno_arr[] = $phoneno;
                }
//            echo $message_content;
//            print_r($phoneno_arr);
//            exit;
                $phoneno_arr = array();
                $phoneno_arr[] = 9843258019;
                $phoneno_arr[] = 9843258019;
//            $phoneno_arr1[] = 9677770552;
//            $phoneno_arr1[] = 9095343963;

                $header = array();
                $header[] = 'Name';
                $header[] = 'Mobile No';
                $header[] = 'Message';
                $header[] = 'Date';
                $header[] = 'Status';
                $header[] = 'Smsid';
                $dateval = date('d-m-Y H:i:s');


                $data = preg_replace('/[ ]/', '_', $dateval);
                $data = preg_replace('/[:]/', '-', $dateval);
//         print_r($data);
//         exit; 

                $filename = 'HOME_WORK_REPORT_' . $data . '.csv';

                $fp = fopen(DOWNLOAD_DIR . $filename, 'a');

                $delimiter = ",";

                fputcsv($fp, $header, $delimiter);

                $number = implode(',', $phoneno_arr);
                $stu_data = array();
                $stu_data[] = 'HOME WORK';
                $stu_data[] = $number;
                $stu_data[] = $send_msg_content;
                $stu_data[] = date('d-m-Y');
//                echo $message_content;
//exit;
//                $return = $this->sendSMSByGateway($number, $message_content);
                $return = ControllerBase::sendSMSByGateway($number, $message_content);

                $total_msg = 0;
                $no_of_msgs = 0;

                if ($return == 0) {
                    $stu_data[] = 'Failed';
                } else {
                    $stu_data[] = 'Success';

                    //divide the message length by 160, 1 msg = 160 characters
                    $total_msg = (int) (strlen($message_content) / 160);
                    if ((strlen($message) % 160) != 0)
                        $total_msg++;
//                    $no_of_msgs = $no_of_msgs + $total_msg;
                }


                if (preg_match('/^[0-9]*$/', $return)) {
                    $stu_data[] = $return;
                } else {
                    $stu_data[] = '';
                }

                $fp = fopen(DOWNLOAD_DIR . $filename, 'a');
                fputcsv($fp, $stu_data, $delimiter);
                fclose($fp);
                $newtemplate = new SmsTemplates();
                $newtemplate->temp_content = $message_content;
                $newtemplate->temp_status = 'Approved';
                $newtemplate->tags = 'template';
                $newtemplate->is_active = 1;
                $newtemplate->save();

                $sentsms = new SentSms();
                $sentsms->template_id = $newtemplate->temp_id;
                $sentsms->time = time();
                $sentsms->number_of_recipients = count($phoneno_arr);
                $sentsms->report_file = '';
                $sentsms->number_of_failures = 0;
                $sentsms->number_of_messages = count($phoneno_arr); //count($phoneno_arr);
                $sentsms->report_file = $filename;
                $sentsms->responseid = $return;
                $sentsms->save();

                if ($return == 0) {
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">Home Work Sending Failed.</div>';
                    print_r(json_encode($message));
                    exit;
                } else {

                    $message['type'] = 'success';
                    $message['message'] = '<div class="alert alert-block alert-success fade in">Home Work Send Successfully.</div>';
                    print_r(json_encode($message));
                    exit;
                }
            } else {

                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-success fade in">Home Work Not Found.</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function sendSMSByGateway($number, $message) {

        $smsprovider = CountrySmsprovider::findFirst('domain = "' . SUBDOMAIN . '"');
//        if ($provider == $smsprovider->) { //country INDIA

        $error_description = array('101' => 'Invalid username/password',
            '102' => 'Sender not exist',
            '103' => 'Receiver not exist',
            '104' => 'Invalid route',
            '105' => 'Invalid message type',
            '106' => 'SMS content not exist',
            '107' => 'Transaction template mismatch',
            '108' => 'Low credits in the specified route',
            '109' => 'Account is not eligible for API',
            '110' => 'Promotional route will be working from 9am to 9pm only');

        $errorcodes = array(101, 102, 103, 104, 105, 106, 107, 108, 109, 110);

        $domain = "sms.edusparrow.com";
        $username = urlencode($smsprovider->username);
        $password = urlencode($smsprovider->password);
        $sender = urlencode($smsprovider->sender);
        $message = urlencode($message);
        $parameters = "uname=$username&password=$password&sender=$sender&receiver=$number&route=T&msgtype=1&sms=$message";
        $fp = fopen("http://$domain/httpapi/smsapi?$parameters", "r");

        $response = stream_get_contents($fp);

        fpassthru($fp);
        fclose($fp);

        $error = (in_array($response, $errorcodes));
        if ($error) {
            //process only when there is error
            //$errorcode = $response;
            $errordescription = $error_description[$response];

//                $sentsmsstatus = new SentSmsResults();
//                $sentsmsstatus->sms_id = $sentsms_id;
//                $sentsmsstatus->receiver_type = $receiver_type;
//                $sentsmsstatus->receiver_id = $receiver_id;
//                $sentsmsstatus->error_code = $response;
//                $sentsmsstatus->error_msg = $errordescription;
//                $sentsmsstatus->save();
            return 0;
        } else {
            return $response;
        }

//        if ($response == "") {
//            $sentsmsstatus = new SentSmsResults();
//            $sentsmsstatus->sms_id = $sentsms_id;
//            $sentsmsstatus->receiver_type = $receiver_type;
//            $sentsmsstatus->receiver_id = $receiver_id;
//            $sentsmsstatus->error_code = 0;
//            $sentsmsstatus->error_msg = "check domain, username and password";
//            $sentsmsstatus->save();
//            return 1;
//        } else {
//            
//        }
//        }
//end SMS
    }

    public function homeworkTblHdrAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Home Work | ");
        $this->assets->addCss('css/edustyle.css');
    }

    //view voucher list data
    public function loadHomeworkDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = array();

        $orderphql = array();

        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }


        if (isset($params['grpsubject_id']) && $params['grpsubject_id'] != '') {
            $queryParams[] = 'grp_subject_teacher_id IN(' . $params['grpsubject_id'] . ')';
        }
//print_r($params);
//exit;
        if (isset($params['subject_modules_id']) && $params['subject_modules_id'] != '') {
            $queryParams[] = 'subjct_modules IN(' . $params['subject_modules_id'] . ')';
        }
//        if (isset($params['subject_id']) && $params['subject_id'] != '') {
//            $queryParams[] = 'grp_subject_teacher_id =' . $params['subject_id'];
//        }

        if (isset($params['sSearch']) && $params['sSearch'] != ''):
            $queryParams1[] = 'id Like "%' . $params['sSearch'] . '%"';
//               $queryParams1[] = 'vouchertype  Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;

        $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $orderphql[] = $this->getSortColumnNameForHomework($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }

        $order = implode(',', $orderphql);
        $homeworks = HomeWorkTable::find(array(
                    $conditionvals,
                    "order" => $order,
                    "limit" => array(
                        "number" => $this->request->getPost('iDisplayLength'),
                        "offset" => $this->request->getPost('iDisplayStart')
                    )
        ));
        $tothomeworks = HomeWorkTable::find(array(
                    $conditionvals,
                    "columns" => "COUNT(*) as cnt",
        ));


        $rowEntries = $this->formatHomeworkTableData($homeworks);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($homeworks),
            "iTotalDisplayRecords" => $tothomeworks[0]->cnt,
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForHomework($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "id";
        }
        switch ($sortColumnIndex) {

            case 1:
                return "id";

            default:
                return "id";
        }
    }

    public function formatHomeworkTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();

                $row['date'] = ($items->hmwrkdate) ? date('d M Y', $items->hmwrkdate) : '';
                $row['title'] = ($items->title) ? $items->title : '';
                $row['homework'] = ($items->homework) ? $items->homework : '';
                $row['id'] = ($items->id) ? $items->id : '';
                $row['action'] = '<a class="row-edit" href="javascript:;" onclick="homeworkSettings.editHomework(' . $items->id . ')">
                                                            <span class="zmdi zmdi-edit"></span> Edit
                                                            </a> 
                                                         ';
                $row['actiond'] = ' <a class="row-delete" href="javascript:;"  onclick="homeworkSettings.deleteSelectedHomework(' . $items->id . ')">
                                                            <span class="zmdi zmdi-close"></span> Delete
                                                            </a>';
                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    public function stuSearchAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Application | ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs('js/appscripts/application.js');
//        $this->assets->addJs('js/appscripts/studentprofile.js');

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
        $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
    }

    public function deleteSelectedHomeworkAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $hmwrkid = $this->request->getPost('homeworkid');
            $homeworkdet = HomeWorkTable::findFirstById($hmwrkid);

            if ($homeworkdet->delete()) {
                $message['type'] = 'success';
                $message['ItemID'] = $homeworkdet->id;
                $message['message'] = '<div class="alert alert-block alert-success fade in">Home Work Deleted Successfully</div>';
                print_r(json_encode($message));
                exit;
            } else {

                foreach ($homeworkdet->getMessages() as $messages) {
                    $error .= $messages;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function _getMandNodesForExam($acdyrMas, $nodes = array()) {
        $fields = OrganizationalStructureValues::findFirstById($acdyrMas->parent_id);
        $query = 'id =' . $acdyrMas->org_master_id . ' AND module ="Subject"';
        $org_mas = OrganizationalStructureMaster::find(array(
                    $query,
                    "columns" => "COUNT(*) as cnt"
        ));
        if (isset($org_mas[0]->cnt) && $org_mas[0]->cnt == 1):

            $nodes[$acdyrMas->id] = $acdyrMas->name;
//                echo $acdyrMas->name;
            if (isset($fields->parent_id)):
//            echo $fields->parent_id;
                $nodes = HomeWorkController::_getMandNodesForExam($fields, $nodes);
            endif;
        endif;
//exit;
        return $nodes;
    }

    public function _getNonMandNodesForAssigning($id, $nodes = array()) {
        $fields = OrganizationalStructureMaster::find('parent_id=' . $id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
                // if ($field->mandatory_for_admission != 1) {
                if ($field->is_subordinate != 1) {
                    $nodes[$field->id] = $field->name;
                    // } else {
                    $nodes = $this->_getNonMandNodesForAssigning($field->id, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function _getMandNodesForAssigning($acdyrMas, $nodes = array()) {

        $nodes[$acdyrMas->id] = $acdyrMas->name;
        //  echo $acdyrMas->name;
        $fields = OrganizationalStructureMaster::find('parent_id=' . $acdyrMas->id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
                //if ($field->mandatory_for_admission == 1) {
                if ($field->is_subordinate != 1) {
//                    $nodes[$field->id] = $field->name;
                    $nodes = $this->_getMandNodesForAssigning($field, $nodes);
                }
            }
        endif;
        return $nodes;
    }

}
