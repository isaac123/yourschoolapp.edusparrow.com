<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class CommonController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function getSchoolListAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $schools = SchoolMaster::find('isactive =1');
        $list = $lists = array();
//        echo "<pre>";
        if (count($schools) > 0) {
            foreach ($schools as $svalue) {
                $list['id'] = $svalue->id;
                $list['name'] = $svalue->name;
                $lists[] = $list;
            }
            $message['type'] = 'success';
            $message['message'] = $lists;
            print_r(json_encode($message));
            exit;
        } else {
            $message['type'] = 'error';
            $message['message'] = 'Schools not yet added';
            print_r(json_encode($message));
            exit;
        }
    }

    public function getCountryListAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $country = CountryMaster::find();
        $list = $lists = array();
//        echo "<pre>";
        if (count($country) > 0) {
            foreach ($country as $svalue) {
                $list['id'] = $svalue->id;
                $list['name'] = $svalue->country_name;
                $lists[] = $list;
            }
            $message['type'] = 'success';
            $message['message'] = $lists;
            print_r(json_encode($message));
            exit;
        } else {
            $message['type'] = 'error';
            $message['message'] = 'Schools not yet added';
            print_r(json_encode($message));
            exit;
        }
    }

    public function getClassListAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $curentyr = ControllerBase::getCurrentAcademicYear();
        $grade = OrganizationalStructureMaster::findFirst('name = "Grade"');
        $classes = OrganizationalStructureValues::find(
                        "org_master_id = $grade->id and parent_id = $curentyr->id"
        );
        $list = $lists = array();
//        echo "<pre>";
        $list['id'] = 0;
        $list['name'] = 'Choose your grade';
        $lists[] = $list;
        if (count($classes) > 0) {
            foreach ($classes as $svalue) {
                $list['id'] = $svalue->id;
                $list['name'] = $svalue->name;
                $lists[] = $list;
            }
            $message['type'] = 'success';
            $message['message'] = $lists;
            print_r(json_encode($message));
            exit;
        } else {
            $message['type'] = 'error';
            $message['message'] = 'Schools not yet added';
            print_r(json_encode($message));
            exit;
        }
    }

    public function signupAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $identity = $this->auth->getIdentity();
        $uid = 1;
        try {
            if ($this->request->isPost()) {
                $getpostDta = $this->request->getJsonRawBody();
                $mobilenum = $getpostDta->phoneno;
                $uuid = $getpostDta->uuid;
                $device_type = $getpostDta->platform;
                $device_token = $getpostDta->device_token;
                $grade = $getpostDta->grade;
                $email = $mobilenum . '@' . SUBDOMAIN . '.edusparrow.com';
		
                $admissionNumPrfix = Settings::findFirstByVariableName('admission_prefix') ?
                        Settings::findFirstByVariableName('admission_prefix') : new Settings();
                $admissionNumSufix = Settings::findFirstByVariableName('admission_sufix') ?
                        Settings::findFirstByVariableName('admission_sufix') : new Settings();
                $prefix = $admissionNumPrfix ? $admissionNumPrfix->variableValue : '';
                $sufix = $admissionNumSufix ? $admissionNumSufix->variableValue : '';
                $stringLen = ControllerBase::getStudentPad('student');
                $admission_prefix = $prefix . $stringLen . $sufix;
 		$stumax = StudentInfo::find();
		$adminNumArr = array();
                foreach ($stumax as $stuAdmin) {
                    $adminNumArr[] = $stuAdmin->Admission_no; // ltrim ($stuAdmin->loginid, 's');;
                }
                $arraySorted = natsort($adminNumArr);
                $lastId = array_pop($adminNumArr);

                $asterikstr = str_replace("*", "", $admission_prefix, $asterikcount);
                $formartArr = explode('*', $admission_prefix);
                $nextAdminNumber = str_replace($formartArr, "", $lastId);
                $nextgennum = $nextAdminNumber + 1;
                $nextgennum = str_pad($nextgennum, $asterikcount, "0", STR_PAD_LEFT);

                $i = 0;
                $nextAdminNo = '';
                foreach ($formartArr as $formatStr):
                    if ($formatStr != '') {
                        $nextAdminNo .= $formatStr;
                    } else {
                        if ($i == 0)
                            $nextAdminNo .= $nextgennum;
                        $i = 1;
                    }
                endforeach;

                ##Student Login  
                $param = array(
                    'appkey' => APPKEY,
                    'subdomain' => SUBDOMAIN,
                    'businesskey' => BUSINESSKEY);
                $param['slogin'] = $mobilenum;
                $param['password'] = $mobilenum;
                $param['email'] = $email;
                $param['uuid'] = $uuid;
                $param['device_type'] = $device_type;
                $param['device_token'] = $device_token;
                $data_string = json_encode($param);
                $response = IndexController::curlIt(USERAUTHAPI . 'createStudentDeviceLogin', $data_string);
              //   print_r($response);;exit;
                $loginCreated = json_decode($response);
//          print_r($loginCreated);;
                if (!$loginCreated->status) {
                    $message['type'] = 'error';
                    $message['message'] = 'Internal server error';
                    print_r(json_encode($message));
                    exit;
                }
                ##Student LOGIN CREATION END
                if ($loginCreated->status == 'ERROR') {
                    $message['type'] = 'error';
                    $message['message'] = '' . $loginCreated->messages . '';
                    print_r(json_encode($message));
                    exit;
                } else if ($loginCreated->status == 'SUCCESS' && $loginCreated->data->id > 0) {

               
                    $nextAdminNo = $nextAdminNo;
                    ## Admit Student
                    $stuInfo = new StudentInfo();
                    $stuMapping = new StudentMapping();
                    $stuInfo->assign(array(
                        'Admission_no' => $nextAdminNo,
                        'Date_of_Joining' => time(),
                        'Student_Name' => $mobilenum,
                        'Phone' => $mobilenum,
                        'Email' => $email,
                        'loginid' => $loginCreated->data->stulogin,
                        'Gender' => '',
                        'Date_of_Birth' => '',
                        'parent_loginid' => '',
                        'Address1' => '',
                        'Address2' => '',
                        'application_no' => 0,
                    ));
                    $stuInfo->created_by = $uid;
                    $stuInfo->created_date = time();
                    $stuInfo->modified_by = $uid;
                    $stuInfo->modified_date = time();
                    if ($stuInfo->save()) {
                        $stu_status = 'Inclass';
                        $stuMapping->student_info_id = $stuInfo->id;
                        $agg = ControllerBase::getCommonIdsForKeys($grade->id);
                        foreach ($agg as $gvalue) {
                            $newagg[] = implode(',', explode('-', $gvalue));
                        }
                        $stuMapping->aggregate_key = implode(',', $newagg);
                        $stuMapping->status = $stu_status;

                        $stuAcademic = new StudentHistory();
                        $stuAcademic->student_info_id = $stuInfo->id;
                        $stuAcademic->aggregate_key = implode(',', $newagg);
                        $stuAcademic->status = $stu_status;
                        if (!$stuAcademic->save()) {
                            $error = '';
                            foreach ($stuAcademic->getMessages() as $messages) {
                                $error .= $messages;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '' . $error . '';
                            print_r(json_encode($message));
                            exit;
                        } else if (!$stuMapping->save()) {
                            $error = '';
                            foreach ($stuMapping->getMessages() as $messages) {
                                $error .= $messages;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '' . $error . '';
                            print_r(json_encode($message));
                            exit;
                        } else {
                            $message['type'] = 'success';
                            $message['message'] = $stuInfo->loginid;
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        $error = '';
                        foreach ($stuInfo->getMessages() as $message) {
                            $error .= $message;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '' . $error . '';
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    $error = '';
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-error">Login Creation failed!<br/>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '' . $error . '';
            print_r(json_encode($message));
            exit;
        }
    }

    public function admitStudentAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;

        try {
            if ($this->request->isPost()) {
                $application = Application::findFirstById($this->request->getPost('itemID'));
//                $admission_prefix = (Settings::findFirstByVariableName('admission_prefix')->variableValue);

                $admissionNumPrfix = Settings::findFirstByVariableName('admission_prefix') ?
                        Settings::findFirstByVariableName('admission_prefix') : new Settings();
                $admissionNumSufix = Settings::findFirstByVariableName('admission_sufix') ?
                        Settings::findFirstByVariableName('admission_sufix') : new Settings();
                $prefix = $admissionNumPrfix ? $admissionNumPrfix->variableValue : '';
                $sufix = $admissionNumSufix ? $admissionNumSufix->variableValue : '';
                $stringLen = ControllerBase::getStudentPad('student');
                $admission_prefix = $prefix . $stringLen . $sufix;


                $stumax = StudentInfo::find();
                $adminNumArr = array();
                foreach ($stumax as $stuAdmin) {
                    $adminNumArr[] = $stuAdmin->Admission_no; // ltrim ($stuAdmin->loginid, 's');;
                }
//                 print_r($adminNumArr);
                $arraySorted = natsort($adminNumArr);
//                print_r($adminNumArr);
                $lastId = array_pop($adminNumArr);

                $asterikstr = str_replace("*", "", $admission_prefix, $asterikcount);
                $formartArr = explode('*', $admission_prefix);
                $nextAdminNumber = str_replace($formartArr, "", $lastId);
                $nextgennum = $nextAdminNumber + 1;
                //echo 'prevnextgennum:'.$nextgennum;
                //$countOfZeros = abs(strlen($nextgennum) - $asterikcount);
                //$countOfZeros++;
                $nextgennum = str_pad($nextgennum, $asterikcount, "0", STR_PAD_LEFT);

                $i = 0;
                $nextAdminNo = '';
                foreach ($formartArr as $formatStr):
                    if ($formatStr != '') {
                        $nextAdminNo .= $formatStr;
                    } else {
                        if ($i == 0)
                            $nextAdminNo .= $nextgennum;
                        $i = 1;
                    }
                endforeach;

                //echo 'lastid:'.$lastId.',asterikstr:'.$asterikstr.'asterikcount:'.$asterikcount.'nextAdminNumber:'.$nextAdminNumber.'nextgennum:'.$nextgennum.'countOfZeros:'.$countOfZeros.'$nextAdminNo:'.$nextAdminNo;
//                print_r($formartArr);exit;
//                $admission_no = $admission_prefix . ($stumax ? ($stumax + 1) : 1);
                ##Student Login  
                $param = array(
                    'appkey' => APPKEY,
                    'subdomain' => SUBDOMAIN,
                    'businesskey' => BUSINESSKEY);
                $param['slogin'] = 's' . $nextAdminNo;
                $param['plogin'] = 'p' . $nextAdminNo;
                $param['password'] = date('d/m/Y', $application->Date_of_Birth);
                $param['email'] = $application->Email ? $application->Email : ('s' . $nextAdminNo . '@' . SUBDOMAIN . '.edusparrow.com');
                $data_string = json_encode($param);
                $response = IndexController::curlIt(USERAUTHAPI . 'createStudentLogin', $data_string);
//                   print_r($response);;exit;
                $loginCreated = json_decode($response);
//          print_r($loginCreated);;
                if (!$loginCreated->status) {
                    $message['type'] = 'error';
                    $message['message'] = 'Internal server error';
                    print_r(json_encode($message));
                    exit;
                }
                ##Student LOGIN CREATION END
                if ($loginCreated->status == 'ERROR') {
                    $message['type'] = 'error';
                    $message['message'] = '' . $loginCreated->messages . '';
                    print_r(json_encode($message));
                    exit;
                } else if ($loginCreated->status == 'SUCCESS' && $loginCreated->data->id > 0) {

                    ##MAIL LOGIN
                    $param = array(
                        'domain' => SUBDOMAIN,
                        'login' => 's' . $nextAdminNo,
                        'password' => date('d/m/Y', $application->Date_of_Birth));
                    $mail_data_string = json_encode($param);
//                      print_r($param); 
                    $response = IndexController::curlIt(MAILSERVICEAPI . 'createWebMailUser', $mail_data_string);
//                   print_r($response);;
                    $mailloginCreated = json_decode($response);
//                    print_r($mailloginCreated);
//                    exit;
                    if (!$mailloginCreated->status) {
                        $message['type'] = 'error';
                        $message['message'] = 'Internal server error';
                        print_r(json_encode($message));
                        exit;
                    }
                    if ($mailloginCreated->status == 'ERROR') {
                        $message['type'] = 'error';
                        $message['message'] = '' . $mailloginCreated->messages . '';
                        print_r(json_encode($message));
                        exit;
                    } else if ($mailloginCreated->status == 'SUCCESS' && $mailloginCreated->data->id) {

                        ## Admit Student
                        $stuInfo = new StudentInfo();
                        $stuMapping = new StudentMapping();
                        $stuInfo->assign(array(
                            'Admission_no' => $nextAdminNo,
                            'application_no' => $application->application_no,
                            'Student_Name' => $application->Student_Name,
                            'Gender' => $application->Gender,
                            'Date_of_Birth' => $application->Date_of_Birth,
                            'Date_of_Joining' => time(), //$application->Date_of_Joining,
                            'Address1' => $application->Address1,
                            'Address2' => $application->Address2,
                            'State' => $application->State,
                            'Country' => $application->Country,
                            'Pin' => $application->Pin,
                            'Phone' => $application->Phone,
                            'Email' => $application->Email ? $application->Email : ('s' . $nextAdminNo . '@' . SUBDOMAIN . '.edusparrow.com'),
                            'loginid' => $loginCreated->data->stulogin,
                            'parent_loginid' => $loginCreated->data->parentlogin,
                        ));

                        $stuInfo->created_by = $uid;
                        $stuInfo->created_date = time();
                        $stuInfo->modified_by = $uid;
                        $stuInfo->modified_date = time();
                        if ($stuInfo->save()) {
                            $stuMapping->student_info_id = $stuInfo->id;
                            $stuMapping->aggregate_key = $application->aggregate_key;

                            $admit_stu_val = Settings::findFirst('variableName ="admit_student"');

                            $stu_status = 'Admitted';
                            if ($admit_stu_val->variableValue == '1')
                                $stu_status = 'Inclass';

                            $stuMapping->status = $stu_status;

                            $stuAcademic = new StudentHistory();
                            $stuAcademic->student_info_id = $stuInfo->id;
                            $stuAcademic->aggregate_key = $application->aggregate_key;
                            $stuAcademic->status = $stu_status;
                            if (!$stuAcademic->save()) {
                                $error = '';
                                foreach ($stuAcademic->getMessages() as $messages) {
                                    $error .= $messages;
                                }
                                $message['type'] = 'error';
                                $message['message'] = '' . $error . '';
                                print_r(json_encode($message));
                                exit;
                            } else if (!$stuMapping->save()) {
                                $error = '';
                                foreach ($stuMapping->getMessages() as $messages) {
                                    $error .= $messages;
                                }
                                $message['type'] = 'error';
                                $message['message'] = '' . $error . '';
                                print_r(json_encode($message));
                                exit;
                            } else {

                                $appln = Application::findFirstById($this->request->getPost('itemID'));
                                $admit_stu_val = Settings::findFirst('variableName ="admit_student"');

                                $stu_status = 'Admitted';
                                if ($admit_stu_val->variableValue == '1')
                                    $stu_status = 'Inclass';

                                $appln->status = $stu_status;
                                $appln->key_in_status = 'In Progress';
                                $appln->modified_by = $uid;
                                $appln->modified_on = time();
                                $appln->Date_of_Joining = $stuInfo->Date_of_Joining;
                                $gender = ($stuInfo->Gender == '2') ? 'Male' : 'Female';
                                if ($appln->save()) {

                                    //calendar creation 
                                    /* $params['username'] = $stuInfo->loginid;
                                      $params['password'] = date('d/m/Y', $stuInfo->Date_of_Birth);
                                      $params['fullname'] = $stuInfo->Student_Name;
                                      $params['email_address'] = $stuInfo->Email;
                                      $params['rolenames'] = 'Student';
                                      $params['aggregateid'] = $stuMapping->aggregate_key;
                                      // print_r($params);
                                      $data_string = json_encode($params);
                                      $responseParam = IndexController::curlIt(CALENDARAPI . 'createUserPricipal', $data_string);
                                      $calloginCreated = json_decode($responseParam);
                                      // print_r($calloginCreated);
                                      if (!$calloginCreated->status) {
                                      $message['type'] = 'error';
                                      $message['message'] = 'Internal server error';
                                      print_r(json_encode($message));
                                      exit;
                                      } else if ($calloginCreated->status == 'ERROR') {
                                      $message['type'] = 'error';
                                      $message['message'] = '' . $loginCreated->messages . '';
                                      print_r(json_encode($message));
                                      exit;
                                      } else if ($calloginCreated->status == 'SUCCESS' && $calloginCreated->data->id > 0) { */
                                    $message['type'] = 'success';
                                    $message['message'] = $messageparam = '<div class="alert alert-block alert-success fade in">'
                                            . ' <b>Student Admitted successfully </b>'
                                            . "<table><tr><td> Student's Name </td><td>:</td><td> " . $application->Student_Name . "</td></tr>
                      <tr><td> Admission No  </td><td>:</td><td> " . $stuInfo->Admission_no . "</td></tr>
                      <tr><td> Gender  </td><td>:</td><td> " . $gender . "</td></tr>
                      <tr><td> Student's LoginId  </td><td>:</td><td> " . $stuInfo->loginid . "</td></tr>
                      <tr><td> Parents LoginId  </td><td>:</td><td> " . $stuInfo->parent_loginid . "</td></tr></table>
                      ";
                                    print_r(json_encode($message));
                                    exit;
//                                    }
                                } else {
                                    foreach ($appln->getMessages() as $message) {
                                        $error .= $message;
                                    }
                                    $message['type'] = 'error';
                                    $message['message'] = '' . $error . '';
                                    print_r(json_encode($message));
                                    exit;
                                }
                            }
                        } else {
                            $error = '';
                            foreach ($stuInfo->getMessages() as $message) {
                                $error .= $message;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '' . $error . '';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                    ##MAIL LOGIN CREATION END
                } else {
                    $error = '';
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-error">Login Creation failed!<br/>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '' . $error . '';
            print_r(json_encode($message));
            exit;
        }
    }

    public function _createLogin($param) {
        $user = new Users();
        $userRole = new UserRoles();
        $user->assign(array(
            'login' => $param['login'],
            'email' => $param['email'],
            'active' => 'Y',
            'password' => $this->security->hash($param['password'])
        ));
        if ($user->save()) {
            $userRole->role_name = $param['role'];
            $userRole->user = $user;
            if ($userRole->save()) {
                return $user->id;
            }
        }
        return "0";
    }

    public function subscribeAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $identity = $this->auth->getIdentity();
        $uid = 1;
        try {
            if ($this->request->isPost()) {
                $getpostDta = $this->request->getJsonRawBody();
                $mobilenum = $getpostDta->mobile_number;
                $user_name = $getpostDta->user_name;
                $selected_school = $getpostDta->selected_school;
                $entered_school = $getpostDta->entered_school;
                $country = $getpostDta->country;
                $state = $getpostDta->state;
                $zipCode = $getpostDta->zipCode;
                $uuid = $getpostDta->uuid;
                $device_type = $getpostDta->platform;
                $device_token = $getpostDta->device_token;
                $email = $mobilenum . '@' . SUBDOMAIN . '.edusparrow.com';
                if (!$selected_school) {
                    $schools = SchoolMaster::findFirst('name = "' . $entered_school . '"') ?
                            SchoolMaster::findFirst('name = "' . $entered_school . '"') : new SchoolMaster();
                    $schools->name = $entered_school;
                    $schools->board_type = '';
                    $schools->address = '';
                    $schools->state = $state;
                    $schools->country = $country->id;
                    $schools->zipcode = $zipCode;
                    $schools->isactive = 1;
                    if (!$schools->save()) {
                        $message['type'] = 'error';
                        $message['message'] = 'School not stored';
                        print_r(json_encode($message));
                        exit;
                    } else {
                        $selectedschl = $schools->id;
                    }
                } else {
                    $selectedschl = $selected_school->id;
                }

                $stuInfo = StudentInfo::findFirst('loginid LIKE "' . $mobilenum . '"');
                $alreadysubscribed = $stuInfo->subscription_id > 0 ? (UserSubscrption::findFirst('id = ' . $stuInfo->subscription_id)) : '';
                if ($alreadysubscribed) {
                    $alreadysubscribed->no_of_subscription = $alreadysubscribed->no_of_subscription - 1;
                    if (!$alreadysubscribed->save()) {
                        $error = '';
                        foreach ($alreadysubscribed->getMessages() as $messages) {
                            $error .= $messages;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '' . $error . '';
                        print_r(json_encode($message));
                        exit;
                    }
                }

                $user_subscription = UserSubscrption::findFirst('school_id = ' . $selectedschl) ? UserSubscrption::findFirst('school_id = ' . $selectedschl) : new UserSubscrption();
                $user_subscription->no_of_subscription = $user_subscription->no_of_subscription ? ($user_subscription->no_of_subscription + 1) : 1;
                $user_subscription->school_id = $selectedschl;


                if (!$user_subscription->save()) {
                    $error = '';
                    foreach ($user_subscription->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '' . $error . '';
                    print_r(json_encode($message));
                    exit;
                } else {
                    ## Admit Student
                    $stuInfo->State = $state;
                    $stuInfo->Country = $country->id;
                    $stuInfo->Pin = $zipCode;
                    $stuInfo->Phone = $mobilenum;
                    $stuInfo->school = $selectedschl;
                    $stuInfo->subscription_id = $user_subscription->id;
                    $stuInfo->modified_by = $uid;
                    $stuInfo->modified_date = time();

                    if (!$stuInfo->save()) {
                        $error = '';
                        foreach ($stuInfo->getMessages() as $message) {
                            $error .= $message;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '' . $error . '';
                        print_r(json_encode($message));
                        exit;
                    } else {
                        $message['type'] = 'success';
                        $message['message'] = $stuInfo->loginid;
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '' . $error . '';
            print_r(json_encode($message));
            exit;
        }
    }

    public function storelogAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $uid = 1;
        try {
            if ($this->request->isPost()) {
                $getpostDta = $this->request->getJsonRawBody();
                $login = $getpostDta->login;
                $stuInfo = StudentInfo::findFirst('loginid LIKE "' . $login . '"');
                if ($stuInfo) {
                    $SchoolLogs = SchoolLogs::findFirst('student_id =' . $stuInfo->id) ? SchoolLogs::findFirst('student_id =' . $stuInfo->id) : new SchoolLogs();
                    $SchoolLogs->student_id = $stuInfo->id;
                    $SchoolLogs->school_id = $stuInfo->school;
                    $SchoolLogs->phone_number = $stuInfo->Phone;
                    $SchoolLogs->is_accepted = 0;
                    $SchoolLogs->is_transfered = 0;
                    $SchoolLogs->created_by = 1;
                    $SchoolLogs->created_on = time();
                    $SchoolLogs->modified_by = 1;
                    $SchoolLogs->modified_on = time();
                    if ($SchoolLogs->save()) {
                        $message['type'] = 'success';
                        $message['message'] = $stuInfo->loginid;
                        print_r(json_encode($message));
                        exit;
                    } else {
                        $error = '';
                        foreach ($SchoolLogs->getMessages() as $messages) {
                            $error .= $messages;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '' . $error . '';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '' . $error . '';
            print_r(json_encode($message));
            exit;
        }
    }

    public function removelogAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $uid = 1;
        try {
            if ($this->request->isPost()) {
                $getpostDta = $this->request->getJsonRawBody();
                $login = $getpostDta->login;
                $stuInfo = StudentInfo::findFirst('loginid LIKE "' . $login . '"');
                if ($stuInfo) {
                    $SchoolLogs = SchoolLogs::findFirst('student_id =' . $stuInfo->id);
                    if ($SchoolLogs && $SchoolLogs->delete()) {
                        $message['type'] = 'success';
                        $message['message'] = $stuInfo->loginid;
                        print_r(json_encode($message));
                        exit;
                    } else {
                        $error = '';
                        foreach ($SchoolLogs->getMessages() as $messages) {
                            $error .= $messages;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '' . $error . '';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '' . $error . '';
            print_r(json_encode($message));
            exit;
        }
    }

    public function connectedRequestAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        try {
            if ($this->request->isGet()) {
                $login = $this->request->get('loginid');
                $stuInfo = StudentInfo::findFirst('loginid LIKE "' . $login . '"');
                if ($stuInfo) {
                    $SchoolLogs = SchoolLogs::findFirst('student_id =' . $stuInfo->id);
                    if ($SchoolLogs) {
                        $message['type'] = 'connecttoschlreq';
                        print_r(json_encode($message));
                        exit;
                    } else {
			if($stuInfo->subscription_id >0 ){
                        $message['type'] = 'subscribed';
                        print_r(json_encode($message));
                        exit;
			}else{
                        $message['type'] = 'yettoconnect';
                        print_r(json_encode($message));
                        exit;
			}
                    }
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '' . $error . '';
            print_r(json_encode($message));
            exit;
        }
    }

    public function commitlogAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $uid = 1;
        try {
            if ($this->request->isPost()) {
                $getpostDta = $this->request->getJsonRawBody();
                $login = $getpostDta->login;
                $stuInfo = StudentInfo::findFirst('loginid LIKE "' . $login . '"');
                if ($stuInfo) {
                    $SchoolLogs = SchoolLogs::findFirst('student_id =' . $stuInfo->id);
                    if ($SchoolLogs) {
                        $SchoolLogs->is_accepted = 1;
                        $SchoolLogs->is_transfered = 1;
                        $SchoolLogs->modified_on = time();
                        if ($SchoolLogs->save()) {
                            $message['type'] = 'success';
                            $message['message'] = $stuInfo->loginid;
                            print_r(json_encode($message));
                            exit;
                        } else {
                            $error = '';
                            foreach ($SchoolLogs->getMessages() as $messages) {
                                $error .= $messages;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '' . $error . '';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '' . $error . '';
            print_r(json_encode($message));
            exit;
        }
    }

}
