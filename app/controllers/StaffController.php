<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class StaffController extends ControllerBase {

    protected function initialize() {
//        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }
        $timeline = array();
        $i = 0;
        $staticon = array('w_bg_amber', 'w_bg_green', 'w_bg_teal', 'w_bg_lime', 'w_bg_yellow', 'w_bg_orange', 'w_bg_brown',
            'w_bg_red', 'w_bg_pink', 'w_bg_purple', 'w_bg_deep_purple', 'w_bg_indigo', 'w_bg_blue ', 'w_bg_light_blue', 'w_bg_cyan'
        );
        $identity = $this->auth->getIdentity();
        $userid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $this->view->searchStaffIdsList = $params['searchStaffIdsList'];
        if ($params['searchStaffIdsList']):
            $search_ids = explode('_', $params['searchStaffIdsList']);
        endif;
        if ($params['typesearch'] == 'Communicate' || $params['typesearch'] == 'All') {
            $announcement = count($search_ids) > 0 ? Announcement::find('created_by=' . $search_ids[1]) : Announcement::find();
            if (count($announcement) > 0) {
                foreach ($announcement as $announc) {
                    $stuquery = 'SELECT stfinfo.id,stfinfo.aggregate_key,stfinfo.loginid,stgen.photo, '
                            . '  stfinfo.status,stfinfo.Staff_Name,stfinfo.appointment_no, '
                            . ' stfinfo.Date_of_Birth,stfinfo.Date_of_Joining,stfinfo.Email,stfinfo.Gender  '
                            . ' FROM   StaffInfo stfinfo '
                            . ' LEFT JOIN StaffGeneralMaster stgen on stgen.staff_id = stfinfo.id'
                            . ' WHERE  stfinfo.id = ' . $announc->created_by
                            . ' ORDER BY stfinfo.Staff_Name ASC';
                    $stfdet = $this->modelsManager->executeQuery($stuquery);
                    $stfname = $stfdet[0];
                    $annlist = AnnouncementTolist::find('announcement_id=' . $announc->id);
                    $list = $acklist = array();
                    foreach ($annlist as $ann) {
                        $list[] = $ann->to;
                        if ($ann->status == 'Acknowledge'):
                            $acklist[] = $ann->to;
                        endif;
                    }
                    if ($params['statussearch'] == 'Open') {
                        if (in_array($identity['name'], $list) && !in_array($identity['name'], $acklist)) {
                            $timeline[$i]['topic'] = $announc->type;
                            $timeline[$i]['subtype'] = $announc->type;
                            $timeline[$i]['type'] = 'Announcement';
                            $timeline[$i]['id'] = $announc->id;
                            $timeline[$i]['message'] = $announc->message;
                            $timeline[$i]['datetime'] = date('Y-m-d H:i:s', $announc->date);
                            $timeline[$i]['createddt'] = date('Y-m-d H:i:s', $announc->created_date);
                            $timeline[$i]['by'] = $stfname->Staff_Name;
                            $timeline[$i]['icon'] = $stfname->photo ? $stfname->photo : (substr(ucfirst(trim($stfname->Staff_Name)), 0, 1));
                            $timeline[$i]['iconclr'] = $staticon[round($stfname->id % 15)];
                            $timeline[$i]['files'] = $announc->files;
                            $i++;
                        }
                    } elseif ($params['statussearch'] == 'Closed') {
                        if (in_array($identity['name'], $list) && in_array($identity['name'], $acklist)) {
                            $timeline[$i]['topic'] = $announc->type;
                            $timeline[$i]['subtype'] = $announc->type;
                            $timeline[$i]['type'] = 'Announcement';
                            $timeline[$i]['id'] = $announc->id;
                            $timeline[$i]['message'] = $announc->message;
                            $timeline[$i]['datetime'] = date('Y-m-d H:i:s', $announc->date);
                            $timeline[$i]['createddt'] = date('Y-m-d H:i:s', $announc->created_date);
                            $timeline[$i]['by'] = $stfname->Staff_Name;
                            $timeline[$i]['icon'] = $stfname->photo ? $stfname->photo : (substr(ucfirst(trim($stfname->Staff_Name)), 0, 1));
                            $timeline[$i]['iconclr'] = $staticon[round($stfname->id % 15)];
                            $timeline[$i]['files'] = $announc->files;
                            $i++;
                        }
                    }
                }
            }
        }
        if ($params['typesearch'] == 'Event' || $params['typesearch'] == 'All') {
            $events = count($search_ids) > 0 ? Events::find('created_by=' . $search_ids[1]) : Events::find();
            $evetyp = array("1" => "Seminars", "2" => "Workshops", "3" => "Competitions", "4" => "Events");
            if (count($events) > 0) {
                foreach ($events as $event) {
                    $stuquery = 'SELECT stfinfo.id,stfinfo.aggregate_key,stfinfo.loginid,stgen.photo, '
                            . '  stfinfo.status,stfinfo.Staff_Name,stfinfo.appointment_no, '
                            . ' stfinfo.Date_of_Birth,stfinfo.Date_of_Joining,stfinfo.Email,stfinfo.Gender  '
                            . ' FROM   StaffInfo stfinfo '
                            . ' LEFT JOIN StaffGeneralMaster stgen on stgen.staff_id = stfinfo.id'
                            . ' WHERE  stfinfo.id = ' . $event->created_by
                            . ' ORDER BY stfinfo.Staff_Name ASC';
                    $stfdet = $this->modelsManager->executeQuery($stuquery);
                    $stfname = $stfdet[0];
                    $eventlist = EventsList::find('event_id=' . $event->id);
                    $acceptlist = $acklist = array();
                    foreach ($eventlist as $l1) {
                        $listdet[] = $l1->events_to;
                        if ($l1->status == 'Accepted'):
                            $acceptlist[] = $l1->events_to;
                        endif;
                    }
                    if ($params['statussearch'] == 'Open') {
                        if (in_array($identity['name'], $listdet) && !in_array($identity['name'], $acceptlist)) {
                            $timeline[$i]['topic'] = $event->message;
                            $timeline[$i]['subtype'] = $evetyp[$event->type];
                            $timeline[$i]['type'] = 'Events';
                            $timeline[$i]['id'] = $event->id;
                            $timeline[$i]['message'] = $event->description;
                            $timeline[$i]['datetime'] = date('Y-m-d H:i:s', $event->from_date);
                            $timeline[$i]['todate'] = $event->to_date ? date('d M', $event->to_date) : '';
                            $timeline[$i]['createddt'] = date('Y-m-d H:i:s', $event->created_on);
                            $timeline[$i]['by'] = $stfname->Staff_Name;
                            $timeline[$i]['icon'] = $stfname->photo ? $stfname->photo : (substr(ucfirst(trim($stfname->Staff_Name)), 0, 1));
                            $timeline[$i]['iconclr'] = $staticon[round($stfname->id % 15)];
                            $timeline[$i]['files'] = $event->upload;
                            $i++;
                        }
                    } elseif ($params['statussearch'] == 'Closed') {
                        if (in_array($identity['name'], $listdet) && in_array($identity['name'], $acceptlist)) {
                            $timeline[$i]['topic'] = $event->message;
                            $timeline[$i]['subtype'] = $evetyp[$event->type];
                            $timeline[$i]['type'] = 'Events';
                            $timeline[$i]['id'] = $event->id;
                            $timeline[$i]['message'] = $event->description;
                            $timeline[$i]['datetime'] = date('Y-m-d H:i:s', $event->from_date);
                            $timeline[$i]['todate'] = $event->to_date ? date('d M', $event->to_date) : '';
                            $timeline[$i]['createddt'] = date('Y-m-d H:i:s', $event->created_on);
                            $timeline[$i]['by'] = $stfname->Staff_Name;
                            $timeline[$i]['icon'] = $stfname->photo ? $stfname->photo : (substr(ucfirst(trim($stfname->Staff_Name)), 0, 1));
                            $timeline[$i]['iconclr'] = $staticon[round($stfname->id % 15)];
                            $timeline[$i]['files'] = $event->upload;
                            $i++;
                        }
                    }
                }
            }
        }
        if ($params['typesearch'] == 'Task' || $params['typesearch'] == 'All') {
            $queryval = array();
            if ($identity['role_name'][0] == 'Staff') {
                $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                $type = 'staff';
                $queryval[] = '(task_from =' . $uid . ' or  task_to=' . $uid . ') and type = "staff" and comments is NULL';
            } else if ($identity['role_name'][0] == 'Student') {
                $uid = StudentInfo::findFirstByLoginid($identity['name'])->id;
                $type = 'student';
                $queryval[] = '(task_from =' . $uid . ' or  task_to=' . $uid . ') and type = "student" and comments is NULL';
            }
            if (count($search_ids) > 0):
                $queryval[] = "initiator_id = $search_ids[1]";
            endif;
            $conditionvals = count($queryval) > 0 ? implode(' and ', $queryval) : '';
            $taskItem = TaskActivities::find(array(
                        'conditions' => $conditionvals,
                        'columns' => "MAX(id) as id,task_id",
                        'order' => "id ASC",
                        'group' => "task_id"
            ));
            if (count($taskItem) > 0) {
                foreach ($taskItem as $task) {
                    $taskact = TaskActivities::findFirstById($task->id);
                    $tasklist = TaskMaster::findFirstById($task->task_id);
                    $stuquery = 'SELECT stfinfo.id,stfinfo.aggregate_key,stfinfo.loginid,stgen.photo, '
                            . '  stfinfo.status,stfinfo.Staff_Name,stfinfo.appointment_no, '
                            . ' stfinfo.Date_of_Birth,stfinfo.Date_of_Joining,stfinfo.Email,stfinfo.Gender  '
                            . ' FROM   StaffInfo stfinfo '
                            . ' LEFT JOIN StaffGeneralMaster stgen on stgen.staff_id = stfinfo.id'
                            . ' WHERE  stfinfo.id = ' . $taskact->task_from
                            . ' ORDER BY stfinfo.Staff_Name ASC';
                    $stfdet = $this->modelsManager->executeQuery($stuquery);
                    $stfname = $stfdet[0];
                    if ($params['statussearch'] == 'Open') {
                        if ($taskact->task_to == $uid && ($taskact->status != 'Rejected' && $taskact->status != 'Completed')) {
                            $timeline[$i]['topic'] = $tasklist->message;
                            $timeline[$i]['subtype'] = 'Task - ' . $tasklist->type;
                            $timeline[$i]['type'] = 'Task';
                            $timeline[$i]['id'] = $task->id;
                            $timeline[$i]['message'] = $tasklist->message;
                            $timeline[$i]['duedate'] = date('Y-m-d H:i:s', $tasklist->to_date);
                            $timeline[$i]['datetime'] = date('Y-m-d H:i:s', $tasklist->from_date);
                            $timeline[$i]['createddt'] = date('Y-m-d H:i:s', $tasklist->modified_on);
                            $timeline[$i]['by'] = $stfname->Staff_Name;
                            $timeline[$i]['icon'] = $stfname->photo ? $stfname->photo : (substr(ucfirst(trim($stfname->Staff_Name)), 0, 1));
                            $timeline[$i]['iconclr'] = $staticon[round($stfname->id % 15)];
                            $timeline[$i]['files'] = $tasklist->files;
                            $i++;
                        }
                    } elseif ($params['statussearch'] == 'Closed') {
                        if ($taskact->task_to == $uid && ($taskact->status == 'Rejected' || $taskact->status == 'Completed')) {
                            $timeline[$i]['topic'] = $tasklist->message;
                            $timeline[$i]['subtype'] = 'Task';
                            $timeline[$i]['type'] = 'Task';
                            $timeline[$i]['id'] = $task->id;
                            $timeline[$i]['message'] = $tasklist->message;
                            $timeline[$i]['duedate'] = date('Y-m-d H:i:s', $tasklist->to_date);
                            $timeline[$i]['datetime'] = date('Y-m-d H:i:s', $tasklist->from_date);
                            $timeline[$i]['createddt'] = date('Y-m-d H:i:s', $tasklist->created_on);
                            $timeline[$i]['by'] = $stfname->Staff_Name;
                            $timeline[$i]['icon'] = $stfname->photo ? $stfname->photo : (substr(ucfirst(trim($stfname->Staff_Name)), 0, 1));
                            $timeline[$i]['iconclr'] = $staticon[round($stfname->id % 15)];
                            $timeline[$i]['files'] = $tasklist->files;
                            $i++;
                        }
                    }
                }
            }
        }if ($params['typesearch'] == 'All') {
            if (count($search_ids) > 0):
                $query = " and requested_by = $search_ids[1]";
            endif;
            $application = 'SELECT appm.id as appmid,aptyp.approval_type,
            appm.Item_id,appm.approval_status,appm.requested_by,appm.requested_date,appm.approval_type_id
          FROM ApprovalMaster appm, ApprovalTypes aptyp WHERE '
                    . 'aptyp.id=appm.approval_type_id ' . $query;
            $applicationItem = $this->modelsManager->executeQuery($application);
            if (count($applicationItem) > 0) {
                foreach ($applicationItem as $items) {
                    $appitem = ApprovalItem::find('approval_master_id=' . $items->appmid);
                    $forwardto = $appitem->getLast()->forwaded_to;
                    $stuquery = 'SELECT stfinfo.id,stfinfo.aggregate_key,stfinfo.loginid,stgen.photo, '
                            . '  stfinfo.status,stfinfo.Staff_Name,stfinfo.appointment_no, '
                            . ' stfinfo.Date_of_Birth,stfinfo.Date_of_Joining,stfinfo.Email,stfinfo.Gender  '
                            . ' FROM   StaffInfo stfinfo '
                            . ' LEFT JOIN StaffGeneralMaster stgen on stgen.staff_id = stfinfo.id'
                            . ' WHERE  stfinfo.id = ' . $items->requested_by
                            . ' ORDER BY stfinfo.Staff_Name ASC';
                    $stfdet = $this->modelsManager->executeQuery($stuquery);
                    $stfname = $stfdet[0];

                    if ($items->approval_type == 'Application') {
                        $application = Application::findFirstById($items->Item_id);
                        $class = StaffController::getDepartment(explode(',', $application->aggregate_key));
                        $approvedname = $application->Student_Name . ' - ' . $class;
                    } elseif ($items->approval_type == 'Relieving(student)') {
                        $studrelieving = StudentRelievingMaster::findFirstById($items->Item_id);
                        $stud = StudentInfo::findFirstById($studrelieving->student_id);
                        $approvedname = $stud->Student_Name . ' - ' . StaffController::getDepartment(explode(',', $studrelieving->relieving_aggregate_key));
                    } elseif ($items->approval_type == 'Appointment' || $items->approval_type == 'Relieving(staff)') {
                        $staff = StaffInfo::findFirstById($items->Item_id);
                        $approvedname = $staff->Staff_Name . ' - ' . StaffController::getDepartment(explode(',', $staff->aggregate_key));
                    }

                    if ($params['statussearch'] == 'Open') {
                        if (($userid == $forwardto) && !(in_array($items->approval_status, array('Approved', 'Rejected')))) {
                            $appItem = ApprovalItem::find(array(
                                        'conditions' => "forwaded_to =$userid  and approval_master_id = $items->appmid",
                                        'columns' => "MAX(id) as id",
                                        'order' => "id DESC",
                                        'group' => "approval_master_id"
                            ));
                            $comments = ApprovalItem::findFirstById($appItem[0]->id)->comments;
                            $timeline[$i]['topic'] = $items->id . ',' . $items->appmid . ',' . $items->approval_type_id . ',' . $items->Item_id;
                            $timeline[$i]['type'] = 'Approval';
                            $timeline[$i]['subtype'] = 'Approval';
                            $timeline[$i]['item'] = $items->approval_type;
                            $timeline[$i]['id'] = $items->appmid;
                            $timeline[$i]['message'] = $approvedname . ' - ' . $comments;
                            $timeline[$i]['datetime'] = date('Y-m-d H:i:s', $items->requested_date);
                            $timeline[$i]['createddt'] = date('Y-m-d H:i:s', $items->requested_date);
                            $timeline[$i]['by'] = $stfname->Staff_Name;
                            $timeline[$i]['icon'] = $stfname->photo ? '<img src= "' . ((FILES_URI . $stfname->photo)) . '" />' : (substr(ucfirst(trim($stfname->Staff_Name)), 0, 1));
                            $timeline[$i]['iconclr'] = $staticon[round($stfname->id % 15)];
                            $i++;
                        }
                    } elseif ($params['statussearch'] == 'Closed') {
                        if (($userid == $forwardto) && (in_array($items->approval_status, array('Approved', 'Rejected')))) {
                            $appItem = ApprovalItem::find(array(
                                        'conditions' => "forwaded_to =$userid  and approval_master_id = $items->appmid",
                                        'columns' => "MAX(id) as id",
                                        'order' => "id DESC",
                                        'group' => "approval_master_id"
                            ));
                            $comments = ApprovalItem::findFirstById($appItem[0]->id)->comments;
                            $timeline[$i]['topic'] = $items->id . ',' . $items->appmid . ',' . $items->approval_type_id . ',' . $items->Item_id;
                            $timeline[$i]['type'] = 'Approval';
                            $timeline[$i]['subtype'] = 'Approval';
                            $timeline[$i]['id'] = $items->appmid;
                            $timeline[$i]['item'] = $items->approval_type;
                            $timeline[$i]['message'] = $approvedname . ' - ' . $comments;
                            $timeline[$i]['datetime'] = date('Y-m-d H:i', $items->requested_date);
                            $timeline[$i]['createddt'] = date('Y-m-d H:i', $items->requested_date);
                            $timeline[$i]['by'] = $stfname->Staff_Name;
                            $timeline[$i]['icon'] = $stfname->photo ? '<img src= "' . ((FILES_URI . $stfname->photo)) . '" />' : (substr(ucfirst(trim($stfname->Staff_Name)), 0, 1));

                            $timeline[$i]['iconclr'] = $staticon[round($stfname->id % 15)];
                            $i++;
                        }
                    }
                }
            }
        }
        if ($params['sort'] == 'Timeline_date') {
            $result = usort($timeline, 'StaffController::deadline_compare');
        } elseif ($params['sort'] == 'Date') {
            $result = usort($timeline, 'StaffController::date_compare');
        } elseif ($params['sort'] == 'Type') {
            $result = usort($timeline, 'StaffController::string_compare');
            $result = usort($timeline, 'StaffController::substring_compare');
        } else {
            $result = usort($timeline, 'StaffController::deadline_compare');
        }
        $this->view->count = count($timeline) > 0 ? count($timeline) : 0;
        $this->view->uid = $uid;
        $this->view->type = $type;
        $this->view->value = $timeline;
        $this->view->sort = $params['sort'];
        $this->view->typesearch = $params['typesearch'];
        $this->view->statussearch = $params['statussearch'];
        $this->view->inbxcnt = '( ' . (count($timeline)) . ' )';
    }

    public function dashboardAction() {
        $this->tag->prependTitle("My Activities | ");
        $this->assets->addCss('css/appstyles/dashboard-page.css');
        $this->assets->addJs('js/appscripts/timeline/timeline.js');
        $this->assets->addJs('js/appscripts/announcement/announcement.js');
        $this->assets->addJs('js/appscripts/event/event.js');
        $this->assets->addJs('js/appscripts/approval/approval.js');
        $this->assets->addJs('js/appscripts/application/manageApplication.js');
        $this->assets->addJs('js/appscripts/application/application.js');
        $this->assets->addJs('js/appscripts/approval-view/approvalview.js');
        $this->assets->addJs('js/appscripts/relieving/relieving.js');
        $this->assets->addJs('js/appscripts/appointment/appointment.js');
        $this->assets->addJs('js/appscripts/feepayment/feepayment.js');
        $this->assets->addJs('js/appscripts/voucher/voucher.js');
        $this->assets->addJs('js/appscripts/transport/transport.js');
        $identity = $this->auth->getIdentity();
        $userid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $events = Events::find();
        foreach ($events as $event) {
            $eventlist = EventsList::findFirst('event_id=' . $event->id);
            $listdet = explode(',', $eventlist->events_to);
            if (in_array($identity['name'], $listdet)) {
                $array['id'] = $event->id;
                $array['title'] = $event->title ? $event->title : substr($event->description, 0, 30);
                $array['value'] = $event->title ? substr($event->description, 0, 40) : ($event->type . ($event->location ? ' | ' . $event->location : ''));
                $array['date'] = date('Y-m-d', $event->from_date);
                $array['fromtime'] = date('H:i', $event->from_date);
                $array['totime'] = $event->to_date ? date('H:i', $event->to_date) : '';
                $calen[] = $array;
            }
        }
        $this->view->calendar = json_encode($calen);
    }

    public function date_compare($a, $b) {
        $t1 = strtotime($a['datetime']);
        $t2 = strtotime($b['datetime']);
        return $t2 - $t1;
    }

    public function deadline_compare($a, $b) {
        $t1 = strtotime($a['createddt']);
        $t2 = strtotime($b['createddt']);
        return $t2 - $t1;
    }

    public function string_compare($a, $b) {
        return strnatcmp($a['type'], $b['type']);
    }

    public function substring_compare($a, $b) {
        return strnatcmp($a['subtype'], $b['subtype']);
    }

    public function getDepartment($aggregate) {
        $class = array();
        foreach ($aggregate as $aggregateval) {
            $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
            $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
            $class[] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
        }
        return implode(' - ', $class);
    }

    public function outboxAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $params = $queryParams = array();
        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name']);
        if ($params['outbox_searchStaffList']):
            $search_ids = explode('_', $params['outbox_searchStaffList']);
        endif;
        $outbox = array();
        if (($params['outboxTypesearch'] == 'Communicate') || ($params['outboxTypesearch'] == 'All')) {
            $announcement = Announcement::find('created_by=' . $uid->id);
            if (count($announcement) > 0) {
                foreach ($announcement as $announc) {
                    $outbox[$i]['id'] = $announc->id;
                    $outbox[$i]['type'] = 'Announcement';
                    $outbox[$i]['subtype'] = $announc->type;
                    $outbox[$i]['datetime'] = date('Y-m-d H:i:s', $announc->modified_date);
                    $outbox[$i]['createddt'] = date('Y-m-d H:i:s', $announc->date);
                    $i++;
                }
            }
        }
        if (($params['outboxTypesearch'] == 'Event') || ($params['outboxTypesearch'] == 'All')) {
            $events = Events::find('created_by=' . $uid->id);
            $evtypArr = array("0" => "NA", "1" => "Seminars", "2" => "Workshops", "3" => "Competitions", "4" => "Events");
            if (count($events) > 0) {
                foreach ($events as $event) {
                    $outbox[$i]['id'] = $event->id;
                    $outbox[$i]['type'] = 'Events';
                    $outbox[$i]['subtype'] = $evtypArr[$event->type];
                    $outbox[$i]['datetime'] = date('Y-m-d H:i:s', $event->modified_on);
                    $outbox[$i]['createddt'] = date('Y-m-d H:i:s', $event->from_date);
                    $i++;
                }
            }
        }
        if (($params['outboxTypesearch'] == 'Task') || ($params['outboxTypesearch'] == 'All')) {
            $queryparams = ' initiator_id = ' . $uid->id . '  and comments is NULL';
            $taskItem = TaskActivities::find(array(
                        'conditions' => $queryparams,
                        'columns' => "MAX(id) as id,task_id",
                        'order' => "id ASC",
                        'group' => "task_id"
            ));
            if (count($taskItem) > 0) {
                foreach ($taskItem as $task) {
                    $taskact = TaskActivities::findFirstById($task->id);
                    $taskmas = TaskMaster::findFirstById($taskact->task_id);
                    $outbox[$i]['id'] = $task->id;
                    $outbox[$i]['type'] = 'Task';
                    $outbox[$i]['subtype'] = $taskmas->type;
                    $outbox[$i]['datetime'] = date('Y-m-d H:i:s', $taskact->created_date);
                    $outbox[$i]['createddt'] = date('Y-m-d H:i:s', $taskact->from_date);
                    $i++;
                }
            }
        }
        if ($params['sort'] == 'sortTimelineDate') {
            $result = usort($outbox, 'StaffController::date_compare');
        } elseif ($params['sort'] == 'sortDate') {
            $result = usort($outbox, 'StaffController::deadline_compare');
        } elseif ($params['sort'] == 'sortType') {
            $result = usort($outbox, 'StaffController::string_compare');
            $result = usort($outbox, 'StaffController::substring_compare');
        } else {
            $result = usort($outbox, 'StaffController::date_compare');
        }
        $this->view->statussearch = $params['outboxStatussearch'];
        $this->view->outbox = $outbox;
        $this->view->search_ids = $search_ids;
    }

    public function communicateScreenAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function taskHistoryAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $asscnt = $acccnt = $gavcnt = $cmpltcnt = $rjctcnt = 0;
        $identity = $this->auth->getIdentity();
        if ($identity['role_name'][0] == 'Staff') {
            $this->view->uid = $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
            $queryparams = ' type = "staff" and (comments is NULL)';
        } else if ($identity['role_name'][0] == 'Student') {
            $this->view->uid = $uid = StudentInfo::findFirstByLoginid($identity['name'])->id;
            $queryparams = ' type = "student" and (comments is NULL)';
        }
        $taskItem = TaskActivities::find(array(
                    'conditions' => $queryparams,
                    'columns' => "MAX(id) as id,task_id",
                    'order' => "id ASC",
                    'group' => "task_id"
        ));
        if (count($taskItem) > 0) {
            foreach ($taskItem as $value) {
                $asstask = TaskActivities::findFirstById($value->id);
                $tskmsg = TaskMaster::findFirstById($value->task_id);
                if (($asstask->status == 'Assigned' && $asstask->task_to == $uid && $asstask->action != 1) ||
                        ($asstask->status == 'Forwarded' && $asstask->task_to == $uid && $asstask->action != 1)) {
                    $asscnt++;
                }
                if (($asstask->status == 'Accepted' && $asstask->task_to == $uid && $asstask->action != 1)) {
                    $acccnt++;
                }
                if ($tskmsg->action != 1 && (($asstask->status == 'Assigned' && $asstask->initiator_id == $uid) ||
                        ($asstask->status == 'Forwarded' && $asstask->initiator_id == $uid) ||
                        ($asstask->status == 'Accepted' && $asstask->initiator_id == $uid))) {
                    $gavcnt++;
                }
                if (($asstask->status == 'Completed' && ($asstask->task_to == $uid || $asstask->initiator_id == $uid))) {
                    $cmpltcnt++;
                }
                if (($asstask->status == 'Rejected' && ($asstask->task_to == $uid || $asstask->initiator_id == $uid))) {
                    $rjctcnt++;
                }
            }
        }
        $this->view->revcls = $revcls = ($asscnt > 0) ? 'danger' : 'muted';
        $this->view->acccls = $acccls = ($acccnt > 0) ? 'danger' : 'muted';
        $this->view->gavcls = $gavcls = ($gavcnt > 0) ? 'danger' : 'muted';
        $this->view->taskItem = $taskItem;
        $this->view->asscnt = $asscnt;
        $this->view->acccnt = $acccnt;
        $this->view->gavcnt = $gavcnt;
    }

    public function inboxOutboxCntAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $arrst = array('Acknowledge', 'Declined', 'Completed', 'Rejected');
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name']);
        $announcement_out = Announcement::find('created_by=' . $uid->id . ' and status is NULL and status not in (' . implode(',', $arrst) . ')');
        $events_out = Events::find('created_by=' . $uid->id . ' and status is NULL and status not in (' . implode(',', $arrst) . ')');
        $queryparams = ' initiator_id = ' . $uid->id . ' and status NOT IN ("Completed","Rejected") and comments is NULL';
        $taskItem_out = TaskActivities::find(array(
                    'conditions' => $queryparams . ' and status not in (' . implode(',', $arrst) . ')',
                    'columns' => "MAX(id) as id,task_id",
                    'order' => "id ASC",
                    'group' => "task_id"
        ));

        $result['inbox'] = '( ' . (count($timeline)) . ' )';
        $result['outbox'] = '( ' . (count($announcement_out) + count($events_out) + count($taskItem_out)) . ' )';
        print_r(json_encode($result));
        exit;
    }

    public function loadCommentsDivAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->id = $id = $this->request->getPost('activityid');
        $this->view->type = $type = $this->request->getPost('type');
        $comments = '';
        if ($type == 'announcement' || $type == 'announc') {
            $this->view->comments = $comments = AnnouncementComments::find('master_id=' . $id . ' ORDER BY modified_date desc');
        }if ($type == 'task' || $type == 'viewtask') {
            $taskid = TaskActivities::findFirst('id=' . $id);
            $this->view->comments = $comments = TaskActivities::find('task_id=' . $taskid->task_id . ' and comments != "" ORDER BY created_date desc');
        }if ($type == 'events' || $type == 'viewevent') {
            $this->view->comments = $comments = EventsComments::find('master_id=' . $id . ' ORDER BY modified_date desc');
        }
    }

    public function saveStaffAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $form = new StaffProfileForm(null);
        try {
            if ($this->request->isPost()) {
                $err = 0;
                $errmsg = '';


                /* $this->flashSession->error($this->request->getPost('appointment_no'));
                  return $this->forward('staff/index'); */
                $postArr = $this->request->getPost();
                $fieldphoto = StaffProfileSettings::findFirst('column_name ="photo"');
                $addphoto1 = StaffProfileSettings::findFirst('column_name ="address_proof1"');
                $addphoto2 = StaffProfileSettings::findFirst('column_name ="address_proof2"');
                $addphoto3 = StaffProfileSettings::findFirst('column_name ="address_proof3"');
                // echo '<pre>';
//                print_r($postArr);
//                print_r($form->isValid($postArr)=== false);
//                
//                print_r($form->getMessages());
//                exit;
                if ($form->isValid($this->request->getPost()) == false) {


                    foreach ($form->getMessages() as $messages) {
                        $error .= $messages . '<br>';
                    }

                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                    /* $this->flashSession->error($error);
                      return $this->forward('staff/index'); */
                } else {

                    $staff = new StaffInfo();
                    $staff_add_info = new StaffGeneralMaster();


                    if ($this->request->hasFiles() == true && count($this->request->getUploadedFiles()) < 5) {
                        // Print the real file names and sizes
                        foreach ($this->request->getUploadedFiles() as $file) {
                            $ext = explode('/', $file->getType());
                            if ($ext[0] == 'image') {
                                $filename = md5(uniqid(rand(), true)) . '-' . strtolower($file->getname());
                                $file->moveTo(FILES_DIR . $filename);
                                $fieldname = $file->getKey();
                                $staff_add_info->{$fieldname} = $filename;
                            } else {
                                $errmsg .='Invalid file extension <br>';
                                $err = 1;
                                // return $this->forward('staff/index');
                            }
                        }//$fieldphoto->mandatory != 0 && empty($additionalDetails->photo)
                    } else if ((empty($staff_add_info->photo) && $fieldphoto->mandatory != 0 && $fieldphoto->hide_or_show != 0) ||
                            (empty($staff_add_info->address_proof1) && $addphoto1->mandatory != 0 && $addphoto1->hide_or_show != 0 ) ||
                            (empty($staff_add_info->address_proof2) && $addphoto2->mandatory != 0 && $addphoto2->hide_or_show != 0) ||
                            (empty($staff_add_info->address_proof3) && $addphoto3->mandatory != 0 && $addphoto3->hide_or_show != 0)) {
                        $errmsg .='Photo is required <br>';
                        $err = 1;
                        //return $this->forward('staff/index');
                    }
                    $acdmcyr = $this->request->getPost('academicYrId');
                    $deprtmnt = $this->request->getPost('divVal');
                    $cntry = $this->request->getPost('country');
                    $catgry = $this->request->getPost('category');
                    $natlity = $this->request->getPost('nationality');

                    if ($acdmcyr == '') {
                        $errmsg .='Please Select academic year <br>';
                        $err = 1;
                    }
                    if ($deprtmnt == '') {
                        $errmsg .='Please Select department <br>';
                        $err = 1;
                    }
                    if ($cntry == '') {
                        $errmsg .='Please Select country <br>';
                        $err = 1;
                    }
                    if ($catgry == '') {
                        $errmsg .='Please Select category <br>';
                        $err = 1;
                    }
                    if ($natlity == '') {
                        $errmsg .='Please Select nationality <br>';
                        $err = 1;
                    }
                    if (!preg_replace('/^[0-9]/', '', $this->request->getPost('pin'))) {
                        $errmsg .='Please ender valid pin code <br>';
                        $err = 1;
                    }

                    if ($err == 1) {

                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $errmsg . '</div>';
                        print_r(json_encode($message));
                        exit;
                        /* $this->flashSession->error($errmsg);
                          return $this->forward('staff/index'); */
                    }

                    $param['login'] = 'e' . $this->request->getPost('appointment_no');
                    $param['password'] = 'e' . date('d/m/Y', strtotime($this->request->getPost('dob')));
                    $param['role'] = 'Staff';
                    $param['email'] = $this->request->getPost('email');
                    $PloginCreated = $this->_createLogin($param);

                    if ($this->request->getPost('gender') == 1)
                        $gender = 'Female';
                    else
                        $gender = 'Male';
                    if ($PloginCreated != 0) {
                        $staff->assign(array(
                            'appointment_no' => $this->request->getPost('appointment_no'),
                            'academic_year_id' => $this->request->getPost('academicYrId'),
                            'Staff_Name' => $this->request->getPost('staffName'),
                            'Gender' => $this->request->getPost('gender'),
                            'Date_of_Birth' => $postArr['dob'] ? strtotime($this->request->getPost('dob')) : '',
                            'Date_of_Joining' => strtotime($this->request->getPost('doa')),
                            'Department' => $this->request->getPost('divVal'),
                            'Mobile_No' => $this->request->getPost('phone'),
                            'Email' => $this->request->getPost('email'),
                            'Address1' => $this->request->getPost('address1'),
                            'Address2' => $this->request->getPost('address2'),
                            'State' => $this->request->getPost('state'),
                            'Country' => $this->request->getPost('country'),
                            'Pin' => $this->request->getPost('pin'),
                            'Phone' => $this->request->getPost('phone'),
                            'loginid' => $param['login'],
                            'status' => 'Appointed'
                        ));

                        if ($staff->save()) {
                            $staff_add_info->staff = $staff;
//                            $staff_add_info->gender = $postArr['gender'] ? $this->request->getPost('gender') : '';
                            $staff_add_info->catg_id = $postArr['category'] ? $this->request->getPost('category') : '';
                            $staff_add_info->nationality = $postArr['nationality'] ? $this->request->getPost('nationality') : '';
                            $staff_add_info->qualification = $postArr['qualification'] ? $this->request->getPost('qualification') : '';
                            $staff_add_info->blood_group = $postArr['blood_grp'] ? $this->request->getPost('blood_grp') : '';
                            $staff_add_info->medical_details = $postArr['othrMedDet'] ? $this->request->getPost('othrMedDet') : '';
                            $staff_add_info->spouse_name = $postArr['sname'] ? $this->request->getPost('sname') : '';
                            $staff_add_info->s_occupation = $postArr['soccup'] ? $this->request->getPost('soccup') : '';
                            $staff_add_info->s_designation = $postArr['sdesign'] ? $this->request->getPost('sdesign') : '';
                            $staff_add_info->s_phoneno = $postArr['sphone'] ? $this->request->getPost('sphone') : '';
                            $staff_add_info->parent_name = $postArr['gname'] ? $this->request->getPost('gname') : '';
                            $staff_add_info->p_occupation = $postArr['goccup'] ? $this->request->getPost('goccup') : '';
                            $staff_add_info->p_designation = $postArr['gdesign'] ? $this->request->getPost('gdesign') : '';
                            $staff_add_info->p_phoneno = $postArr['gphone'] ? $this->request->getPost('gphone') : '';
                            $staff_add_info->classes_handling = $postArr['classHandl'] ? $this->request->getPost('classHandl') : '';
                            $staff_add_info->subjects_handling = $postArr['subjHandl'] ? $this->request->getPost('subjHandl') : '';
                            $staff_add_info->extra_curricular = $postArr['exActi'] ? $this->request->getPost('exActi') : '';
                            $staff_add_info->co_curricular = $postArr['coActi'] ? $this->request->getPost('coActi') : '';
                            $staff_add_info->additional_qualifications = $postArr['addQuali'] ? $this->request->getPost('addQuali') : '';
                            $staff_add_info->experience = $postArr['prevExp'] ? $this->request->getPost('prevExp') : '';
                            $staff_add_info->organization = $postArr['Org'] ? $this->request->getPost('Org') : '';
                            $staff_add_info->designation = $postArr['designation'] ? $this->request->getPost('designation') : '';
                            $staff_add_info->comments = $postArr['comments'] ? $this->request->getPost('comments') : '';
//                            $staff_add_info->Date_of_Birth = $postArr['dob'] ? strtotime($this->request->getPost('dob')) : '';
                            $identity = $this->auth->getIdentity();
                            $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                            $staff_add_info->created_by = $uid;
                            $staff_add_info->created_date = time();
                            $staff_add_info->modified_by = $uid;
                            $staff_add_info->modified_date = time();
                            if ($staff_add_info->save()) {
                                $message['type'] = 'success';
                                $message['message'] = '<div class="alert alert-block alert-success fade in"> Staff Appointed Successfully !<br><br>'
                                        . '<table class="table  table-hover general-table table-bordered "><tr><td>Name </td><td>' . $this->request->getPost('staffName') . '</td></tr>
                                          <tr><td>Department </td><td>' . DivisionValues::findFirstById($this->request->getPost('divVal'))->classname . '</td></tr>
                                          <tr><td>Login Id </td><td>' . $param['login'] . '</td></tr>
                                          </table> </div>';
                                // <a  href=' . $print . ' >Print</a>';

                                /* $this->flashSession->success($message['message']);
                                  return $this->forward('staff/index'); */

                                //	$message['type'] = 'success';
                                //$message['message'] = '<div class="alert alert-block alert-success fade in">' . $staff_add_info->getMessages() . '</div>';
                                print_r(json_encode($message));
                                exit;
                            } else {
                                /* $this->flashSession->error($staff_add_info->getMessages());
                                  return $this->forward('staff/index'); */

                                foreach ($staff_add_info->getMessages() as $messages) {
                                    $error .= $messages . '<br>';
                                }

                                $message['type'] = 'error';
                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                print_r(json_encode($message));
                                exit;
                            }
                        } else {

                            foreach ($staff->getMessages() as $messages) {
                                $error .= $messages . '<br>';
                            }

                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                            /*   $this->flashSession->error($staff->getMessages());
                              return $this->forward('staff/index'); */
                        }
                    }
                }
            }
        } catch (Exception $e) {

            foreach ($e->getMessages() as $messages) {
                $error .= $messages . '<br>';
            }

            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
            /* $this->flashSession->error($e->getMessages());
              return $this->forward('staff/index'); */
        }
    }

    public function editAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Appointment | ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs('js/appscripts/appointment.js');
        $this->view->staff = $staff = StaffInfo::findFirstByAppointmentNo($this->request->get('appointment_no'));
        $this->view->staff_add_info = $staff_add_info = count(StaffGeneralMaster::find('staff_id =' . $staff->id)) ?
                StaffGeneralMaster::findFirstByStaffId($staff->id) :
                new StaffGeneralMaster();
        $this->view->form = $form = new StaffProfileForm($this->view->staff, array('edit' => true));
        $this->view->appointment_no = $this->request->get('appointment_no');
    }

    public function updateStaffAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $staff = StaffInfo::findFirstByAppointmentNo($this->request->getPost('appointment_no'));
//        print_r($this->request->getPost('appointment_no'));exit;
        $staff_add_info = (StaffGeneralMaster::findFirst('staff_id =' . $staff->id)) ?
                StaffGeneralMaster::findFirstByStaffId($staff->id) :
                new StaffGeneralMaster();
        $form = new StaffProfileForm(null);
        $message = array();
        try {
            if ($this->request->isPost()) {
                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $messages) {
                        $error .= $messages . '<br>';
                    }

                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                } else {


                    $fieldphoto = StaffProfileSettings::findFirst('column_name ="photo"');
                    $addphoto1 = StaffProfileSettings::findFirst('column_name ="address_proof1"');
                    $addphoto2 = StaffProfileSettings::findFirst('column_name ="address_proof2"');
                    $addphoto3 = StaffProfileSettings::findFirst('column_name ="address_proof3"');

                    if ($this->request->hasFiles() == true && count($this->request->getUploadedFiles()) < 5) {
                        // Print the real file names and sizes
                        foreach ($this->request->getUploadedFiles() as $file) {
                            $ext = explode('/', $file->getType());
                            if ($ext[0] == 'image') {
                                $filename = md5(uniqid(rand(), true)) . '-' . strtolower($file->getname());
                                $file->moveTo(FILES_DIR . $filename);
                                $fieldname = $file->getKey();
                                $staff_add_info->{$fieldname} = $filename;
                            } else {

                                $message['type'] = 'error';
                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                print_r(json_encode($message));
                                exit;
                            }
                        }
                    } else if ((empty($staff_add_info->photo) && $fieldphoto->mandatory != 0 && $fieldphoto->hide_or_show != 0) ||
                            (empty($staff_add_info->address_proof1) && $addphoto1->mandatory != 0 && $addphoto1->hide_or_show != 0 ) ||
                            (empty($staff_add_info->address_proof2) && $addphoto2->mandatory != 0 && $addphoto2->hide_or_show != 0) ||
                            (empty($staff_add_info->address_proof3) && $addphoto3->mandatory != 0 && $addphoto3->hide_or_show != 0)) {


                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">Photo is required.</div>';
                        print_r(json_encode($message));
                        exit;
                    }


                    $staff->appointment_no = $this->request->getPost('appointment_no');
                    $staff->Staff_Name = $this->request->getPost('staffName');
                    $staff->Gender = $this->request->getPost('gender');
                    $staff->Date_of_Birth = strtotime($this->request->getPost('dob'));
                    $staff->Date_of_Joining = strtotime($this->request->getPost('doa'));
                    $staff->Mobile_No = $this->request->getPost('phone');
                    $staff->Email = $this->request->getPost('email');
                    $staff->Address1 = $this->request->getPost('address1');
                    $staff->Address2 = $this->request->getPost('address2');
                    $staff->State = $this->request->getPost('state');
                    $staff->Country = $this->request->getPost('country');
                    $staff->Pin = $this->request->getPost('pin');
                    $staff->Phone = $this->request->getPost('phone');
                    $staff->biometric_id = $this->request->getPost('biometric_id');
                    $staff->pf_no = $this->request->getPost('pf_no');
                    $staff->esi_no = $this->request->getPost('esi_no');
                    if ($staff->save()) {
                        $staff_add_info->staff = $staff;
//                        $staff_add_info->gender = $this->request->getPost('gender');
                        $staff_add_info->catg_id = $this->request->getPost('category');
                        $staff_add_info->nationality = $this->request->getPost('nationality');
                        $staff_add_info->qualification = $this->request->getPost('qualification');
                        $staff_add_info->blood_group = $this->request->getPost('blood_grp');
                        $staff_add_info->medical_details = $this->request->getPost('othrMedDet');
                        $staff_add_info->spouse_name = $this->request->getPost('sname');
                        $staff_add_info->s_occupation = $this->request->getPost('soccup');
                        $staff_add_info->s_designation = $this->request->getPost('sdesign');
                        $staff_add_info->s_phoneno = $this->request->getPost('sphone');
                        $staff_add_info->parent_name = $this->request->getPost('gname');
                        $staff_add_info->p_occupation = $this->request->getPost('goccup');
                        $staff_add_info->p_designation = $this->request->getPost('gdesign');
                        $staff_add_info->p_phoneno = $this->request->getPost('gphone');
                        $staff_add_info->classes_handling = $this->request->getPost('classHandl');
                        $staff_add_info->subjects_handling = $this->request->getPost('subjHandl');
                        $staff_add_info->extra_curricular = $this->request->getPost('exActi');
                        $staff_add_info->co_curricular = $this->request->getPost('coActi');
                        $staff_add_info->additional_qualifications = $this->request->getPost('addQuali');
                        $staff_add_info->experience = $this->request->getPost('prevExp');
                        $staff_add_info->organization = $this->request->getPost('Org');
                        $staff_add_info->designation = $this->request->getPost('designation');
                        $staff_add_info->comments = $this->request->getPost('comments');
//                        $staff_add_info->Date_of_Birth = strtotime($this->request->getPost('dob'));
                        $identity = $this->auth->getIdentity();
                        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                        $staff_add_info->created_by = $uid;
                        $staff_add_info->created_date = time();
                        $staff_add_info->modified_by = $uid;
                        $staff_add_info->modified_date = time();
                        if ($staff_add_info->save()) {

                            $message['type'] = 'success';
                            $message['message'] = '<div class="alert alert-block alert-success fade in">Staff Details Updated Successfully !</div>';
                            print_r(json_encode($message));
                            exit;
                        } else {
                            foreach ($staff_add_info->getMessages() as $messages) {
                                $error .= $messages . '<br>';
                            }

                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-success fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {

                        foreach ($staff->getMessages() as $messages) {
                            $error .= $messages . '<br>';
                        }

                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-success fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            foreach ($e->getMessages() as $messages) {
                $error .= $messages . '<br>';
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-success fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    /*
      public function newAppointmentAction() {
      $form = new StaffProfileForm();
      $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
      $message = array();
      print_r($this->request->getPost());
      print_r($this->request->getUploadedFiles());
      echo $this->request->hasFiles();
      echo count($this->request->getUploadedFiles());
      try {
      if ($this->request->isPost()) {
      if ($form->isValid($this->request->getPost()) != false) {
      $staff = new StaffInfo();
      $staff_add_info = new StudentGeneralMaster();
      if ($this->request->hasFiles() == true && count($this->request->getUploadedFiles()) < 5) {
      // Print the real file names and sizes
      foreach ($this->request->getUploadedFiles() as $file) {
      $ext = explode('/', $file->getType());
      if ($ext[0] == 'image') {
      $filename = md5(uniqid(rand(), true)) . '-' . strtolower($file->getname());
      $file->moveTo(FILES_DIR . $filename);
      $fieldname = $file->getKey();
      $staff_add_info->{$fieldname} = $filename;
      } else {
      $message['type'] = 'error';
      $message['message'] = '<div class="alert alert-block alert-danger fade in">Invalid file extension</div>';
      print_r(json_encode($message));
      exit;
      }
      }
      } else if ($this->request->hasFiles() == false) {
      $message['type'] = 'error';
      $message['message'] = '<div class="alert alert-block alert-danger fade in">Photo is required</div>';
      print_r(json_encode($message));
      exit;
      }

      $param['login'] = 'e' . $this->request->getPost('appointment_no');
      $param['password'] = 'e' . $this->request->getPost('appointment_no');
      $param['role'] = 'Staff';
      $param['email'] = $this->request->getPost('email');
      $PloginCreated = $this->_createLogin($param);
      if ($PloginCreated != 0) {
      $staff->assign(array(
      'Appointment_no' => $this->request->getPost('appointment_no'),
      'academic_year_id' => $this->request->getPost('academicYrId'),
      'Staff_Name' => $this->request->getPost('staffName'),
      'Date_of_Joining' => $this->request->getPost('doa'),
      'Department' => $this->request->getPost('divVal'),
      'Mobile_No' => $this->request->getPost('phone'),
      'Email' => $this->request->getPost('email'),
      'Address1' => $this->request->getPost('address1'),
      'Address2' => $this->request->getPost('address2'),
      'State' => $this->request->getPost('state'),
      'Country' => $this->request->getPost('country'),
      'Pin' => $this->request->getPost('pin'),
      'Phone' => $this->request->getPost('phone'),
      'loginid' => $param['login']
      ));
      if ($staff->save()) {
      $staff_add_info->staff = $staff;
      $staff_add_info->gender = $this->request->getPost('gender');
      $staff_add_info->catg_id = $this->request->getPost('category');
      $staff_add_info->nationality = $this->request->getPost('nationality');
      //                                $staff_add_info->photo= $this->request->getPost('application_no');
      //                                $staff_add_info->address_proof1= $this->request->getPost('application_no');
      //                                $staff_add_info->address_proof2= $this->request->getPost('application_no');
      //                                $staff_add_info->address_proof3= $this->request->getPost('application_no');
      $staff_add_info->qualification = $this->request->getPost('qualification');
      $staff_add_info->blood_group = $this->request->getPost('blood_grp');
      $staff_add_info->medical_details = $this->request->getPost('othrMedDet');
      $staff_add_info->spouse_name = $this->request->getPost('sname');
      $staff_add_info->s_occupation = $this->request->getPost('soccup');
      $staff_add_info->s_designation = $this->request->getPost('sdesign');
      $staff_add_info->s_phoneno = $this->request->getPost('sphone');
      $staff_add_info->parent_name = $this->request->getPost('gname');
      $staff_add_info->p_occupation = $this->request->getPost('goccup');
      $staff_add_info->p_designation = $this->request->getPost('gdesign');
      $staff_add_info->p_phoneno = $this->request->getPost('gphone');
      $staff_add_info->classes_handling = $this->request->getPost('classHandl');
      $staff_add_info->subjects_handling = $this->request->getPost('subjHandl');
      $staff_add_info->extra_curricular = $this->request->getPost('exActi');
      $staff_add_info->co_curricular = $this->request->getPost('coActi');
      $staff_add_info->additional_qualifications = $this->request->getPost('addQuali');
      $staff_add_info->experience = $this->request->getPost('prevExp');
      $staff_add_info->organization = $this->request->getPost('Org');
      $staff_add_info->designation = $this->request->getPost('designation');
      $staff_add_info->comments = $this->request->getPost('comments');
      $identity = $this->auth->getIdentity();
      $uid = $identity['id'];
      $staff_add_info->created_by = $uid;
      $staff_add_info->created_date = time();
      $staff_add_info->modified_by = $uid;
      $staff_add_info->modified_date = time();
      if ($staff_add_info->save()) {
      $message['type'] = 'success';
      $message['message'] = '<div class="alert alert-block alert-success fade in">Staff Appointed Successfully !</div>'
      . '<table><tr><td>Name </td><td>' . $this->request->getPost('staffName') . '</td></tr>
      <tr><td>Department </td><td>' . DivisionValues::findFirstById($this->request->getPost('divVal'))->classname . '</td></tr>
      <tr><td>Login Id </td><td>' . $param['login'] . '</td></tr>
      </table> <a  href=' . $print . ' >Print</a>';
      print_r(json_encode($message));
      exit;
      } else {
      foreach ($staff_add_info->getMessages() as $message) {
      $error .= $message;
      }
      $message['type'] = 'error';
      $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
      print_r(json_encode($message));
      exit;
      }
      } else {
      foreach ($staff->getMessages() as $message) {
      $error .= $message;
      }
      $message['type'] = 'error';
      $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
      print_r(json_encode($message));
      exit;
      }
      }
      } else {

      $error = '';
      foreach ($this->request->getPost() as $key => $val) {
      foreach ($form->getMessagesFor($key) as $messages) {
      $error .= $messages . "</br>";
      }
      }
      $message['type'] = 'error';
      $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
      print_r(json_encode($message));
      exit;
      }
      }
      } catch (Exception $e) {
      foreach ($e->getMessages() as $message) {
      $error .= $message;
      }
      $message['type'] = 'error';
      $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
      print_r(json_encode($message));
      exit;
      }
      }
     */

    public function getAllStaffAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->staffs = StaffInfo::find();
        $this->view->frmreset = $this->request->getPost('frmreset');

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');

        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                $nodes[$field->id] = $field->name;
            }
        endif;

        $this->view->nodes = $nodes;
        $this->view->mandnode = $nodes;
    }

    public function loadtableHeaderAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');

        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                //if ($field->mandatory_for_admission == 1) {
//                    $nodes[$field->id] = $field->name;
                $nodes[$field->id] = $field->name;
                //}
            }
        endif;

        $this->view->nodes = $nodes; //$this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $nodes; //$this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadtableDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();

        $aggregateval = '';
        foreach ($this->request->getPost() as $key => $value) {
            $IsSubdiv = explode('_', $key);
//            if ($IsSubdiv[0] == 'subDivVal') {
//                $params[$IsSubdiv[0]][] = $value;
//            } else {

            $params[$key] = $value;
            //}

            if ($IsSubdiv[0] == 'aggregate') {

                if ($value != '')
                    $aggregateval .= $value . ',';
            }
        }


        $department = $this->request->getPost('department');

        if (isset($params['appointment_no']) && $params['appointment_no'] != '') {
            $queryParams[] = "appointment_no='" . $params['appointment_no'] . "'";
        }

        if (isset($params['name']) && $params['name'] != '') {

            $queryParams[] = "Staff_Name LIKE '" . $params['name'] . "%'";
        }

        if (isset($params['loginid']) && $params['loginid'] != '') {

            $queryParams[] = "loginid = '" . $params['loginid'] . "'";
        }

        if (isset($params['status']) && $params['status'] != ''):
            $queryParams[] = 'status  Like "%' . $params['status'] . '%"';
        endif;

        if (isset($params['sSearch']) && $params['sSearch'] != ''):

            $psql_deprt = OrganizationalStructureValues::findFirst('name LIKE"%' . $params['sSearch'] . '%"');       //'SELECT id FROM OrganizationalStructureValues WHERE name LIKE"%' . $params['sSearch'] . '%"';

            if (isset($psql_deprt)) {
                $dep_id = isset($psql_deprt->id) ? $psql_deprt->id : '';
                if ($dep_id != '')
                    $queryParams1[] = 'aggregate_key Like "%' . $dep_id . '%"';
            }

            $queryParams1[] = 'appointment_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'Staff_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'status Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'loginid Like "%' . $params['sSearch'] . '%"';

            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;

        $queryParams[] = 'status IN("Appointed","Relieved","Relieving initiated")';

        $conditionvals = (count($queryParams) > 0) ? ' WHERE ' . implode(' and ', $queryParams) : '';

        $phql = "SELECT appointment_no,Staff_Name,Gender,status,
            		aggregate_key,loginid,Date_of_Joining,Date_of_Birth,id FROM StaffInfo" . $conditionvals;

//         echo $phql;
//         exit;
//         
        $phql2 = $phql;

        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $phql .= ' ORDER BY ' . $this->getSortColumnName($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }
        $phql .= ' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');

        $result = $this->modelsManager->executeQuery($phql);


        $result2 = $this->modelsManager->executeQuery($phql2);


        $rowEntries = $this->formatTableData($result);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($rowEntries),
            "iTotalDisplayRecords" => count($result2),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnName($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "appointment_no";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "appointment_no";
            case 1:
                return "Staff_Name";
            case 2:
                return "Gender";
            case 3:
                return "loginid";
            case 4:
                return "Date_of_Birth";
            case 5:
                return "Date_of_Joining";
            default:
                return "appointment_no";
        }
    }

    public function formatTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $row['appointment_no'] = $items->appointment_no;
                $staffgeninfo = StaffGeneralMaster::findFirstByStaffId($items->id);
                if ($staffgeninfo->photo) {
                    $row['Staff_Name'] = '<span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Staff_Name'] .= $this->tag->image(array(
                        "src" => $staffgeninfo->photo ? (FILES_URI . $staffgeninfo->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Staff_Name'] .= '</span>';
                } else {
                    $row['Staff_Name'] = ' <span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;" >' . substr($items->Staff_Name, 0, 1) . '</span>';
                }
                $row['Staff_Name'] .= '<span title="' . $items->Staff_Name . ' (' . $items->appointment_no . ') ' . '" class="name"> 
                                        ' . (strlen($items->Staff_Name) > 15 ? substr($items->Staff_Name, 0, 15) . "..." : $items->Staff_Name) . '</span>';

                //$row['Staff_Name'] .= '<span title="' . $items->appointment_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                      ' . $items->Staff_Name .'</span>';

                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';
                $row['stfflogin'] = $items->loginid;
                $row['email'] = $items->loginid ? $items->loginid . '@' . SUBDOMAIN . '.edusparrow.com' : '-';
                $row['dob'] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row['doj'] = (date('d-m-Y', $items->Date_of_Joining)) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row['id'] = $items->id;
//                print_r($items);exit;

                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';

                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }


                $row['assignsal'] = ' ';
                if ($items->status == 'Appointed') {
                    $row['Actions'] = '<span class="mini-stat-icon-action pink" title="Edit" onclick="appointmentSettings.loadStaffdetbyid(\'' . $items->appointment_no . '\')">
                                            <i class="fa fa-edit"></i></span>';

//                    $row['assignsal'] = '<a href="javascript:;" onclick="salarySettings.loadsalary(' . $items->id . ')"> 
//                                                <span class="fa fa-money"  title="Assign/Update Salary">                                         
//                                                </span>
//                                            </a>  ';
                } else if ($items->status == 'Approved') {
                    $row['Actions'] = '<button class="btn btn-primary " type="button" itemID ="' . $items->id . '"    
                                                    "  
                                                    id="stf_appoint">Appoint</button>';
                } else if ($items->status == 'Relieving initiated') {
                    $row['Actions'] = '<label class="btn btn-warning"                
                                        appType ="' . ( $items->status == 'Relieving initiated' ? 9 : 10) . '"  itemID ="' . $items->id . '"    
                                                    "  
                                                    id="stf_status">' . $items->status . '</label>';
                } else if ($items->status == 'Relieved') {
                    $row['Actions'] = '<label class="btn btn-primary"                
                                        appType ="' . ( $items->status == 'Relieving initiated' ? 9 : 10) . '"  itemID ="' . $items->id . '"    
                                                    "  
                                                    id="stf_status">' . $items->status . '</label>';
                }


                $row['Print'] = '-';

                if ($items->status == 'Appointed') {

                    $row['Print'] = '<a target="_blank" title="Appointment Lettrt" href="' . $this->url->get() . 'print/offerLetter?staffid=' . $items->id . '" >
                                 <span class="mini-stat-icon tar"><i class="fa fa-print"></i></span> </a>';
                }
//                $row['assignsal'] = '<a href="' . $this->url->get("salary/assign", array("staffId" => $items->id)) . '"> 
//                                                <span class="fa fa-money"  title="Assign/Update Salary">                                         
//                                                </span>
//                                            </a>  ';
                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    public function profileAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $staff_record = StaffInfo::findFirstById($this->request->get('staffId'));

        $this->view->input = $this->request->get('input') ? $this->request->get('input') : '';
        $staffId = $staff_record->id;
//        print_r($staffId);exit;
        $this->view->subdivexist = 0;
        $this->view->classteacherexist = 0;
        $this->view->subjectteacherexist = 0;
        if ($staff_record) {


            $this->view->staffinfo = $staff_record;
            $this->view->staff_name = $staff_record->Staff_Name;
            $this->view->appointment_no = $staff_record->appointment_no;
            $staffdivname = ''; //ControllerBase::get_division_name_staff();
            $staffsubdivname = ''; //ControllerBase::get_sub_division_name_staff();

            $studentdivname = ''; //ControllerBase::get_division_name_student();
            $studentsubdivname = ''; // ControllerBase::get_sub_division_name_student();

            $this->view->stfform = new StaffPersonalForm($staff_record, array('edit' => true));
            $this->view->changepassForm = new ChangePasswordForm();
            //Get the staff's division and subdivision
            // $divval = DivisionValues::findFirstById($staff_record->Department);
            //Current academic year
            // $cacdyr = AcademicYearMaster::findFirstByStatus('c');
            //echo '<div>'.$staff_record->Department.'</div>';

            $stf_mapdets = ($staff_record->aggregate_key) ? explode(',', $staff_record->aggregate_key) : '';
            $subdiv = array();
            $maping_values = '';

            if ($stf_mapdets != '') {

                foreach ($stf_mapdets as $stf_mapdet) {

                    $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stf_mapdet);

                    $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);

                    $subdiv[] = array(
                        'name' => $orgnztn_str_mas_det->name,
                        'value' => $orgnztn_str_det->name);
// $maping_values .= $orgnztn_str_mas_det->name . '-' . $orgnztn_str_det->name . ':';
                }
            }


            //Get the photo from StaffGeneralMaster
            $staff_profile = StaffGeneralMaster::findfirst('staff_id = ' . $staff_record->id);
            if ($staff_profile) {
                $photo_url = FILES_URI . $staff_profile->photo;
            } else if ($staff_record->Gender == '1') {
                $photo_url = "images/female_staff.png";
            } else if ($staff_record->Gender == '2') {
                $photo_url = "images/male_staff.png";
            } else {
                $photo_url = "images/User.png";
            }
            //Find the list of class/section/subject that the teacher teaches.
            $subject_teacher_records = GroupSubjectsTeachers::find('find_in_set(' . $staff_record->id . ', teachers_id) ');
            $subdivnamss = array();

            if (count($subject_teacher_records) > 0) {
                $this->view->subjectteacherexist = 1;
                $i = 0;

                foreach ($subject_teacher_records as $subject_teacher_record) {
                    $classmaster = ClassroomMaster::findFirstById($subject_teacher_record->classroom_master_id);
                    $ssubdivval = array();
                    $subdivvals = $classmaster->aggregated_nodes_id ?
                            explode('-', $classmaster->aggregated_nodes_id) : '-';
                    if ($subdivvals != '') {

                        foreach ($subdivvals as $stf_mapdet) {
                            $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stf_mapdet);
                            $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);

                            $ssubdivval[] = $orgnztn_str_mas_det->name . '-' . $orgnztn_str_det->name;
                        }

                        $ssubdivval[] = $subject_teacher_record->subject_id ?
                                OrganizationalStructureValues::findFirstById($subject_teacher_record->subject_id)->name : '-';

                        $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
                        $students = StudentMapping::find('status = "Inclass" and ' . implode(' or ', $res));

                        $ssubdivval[] = 'No Of Students - ' . count($students);

                        $subdivnamss[] = $ssubdivval;
                    }
                }
            } else {
                $this->view->subjectteacherexist = 0;
            }


            //Find the list of class/section/subject that the teacher teaches.
            $class_teacher_records = GroupClassTeachers::find('find_in_set(' . $staff_record->id . ', teachers_id) ');
            $subdiv_cls_mass = array();

            if (count($class_teacher_records) > 0) {
                $this->view->classteacherexist = 1;
                $i = 0;
                foreach ($class_teacher_records as $class_teacher_record) {
                    $clsroommmaster = ClassroomMaster::findFirstById($class_teacher_record->classroom_master_id);
                    $ssubdivval_cls = array();
                    $subdivvals_cls = $clsroommmaster->aggregated_nodes_id ?
                            explode('-', $clsroommmaster->aggregated_nodes_id) : '-';


                    if ($subdivvals_cls != '') {

                        foreach ($subdivvals_cls as $stf_mapdet) {
                            $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stf_mapdet);
                            $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);

                            $ssubdivval_cls[] = $orgnztn_str_mas_det->name . '-' . $orgnztn_str_det->name;
                        }


                        $subdiv_cls_mass[] = $ssubdivval_cls;
                    }
                }
            } else {
                $this->view->classteacherexist = 0;
            }

            //List of data passed to view
            $this->view->staffdivname = $staffdivname;
            $this->view->staffsubdivname = $staffsubdivname;
            $this->view->studentdivname = $studentdivname;
            $this->view->studentsubdivname = $studentsubdivname;
            $this->view->stfclsdet = $subdiv;
            $this->view->class_teacher = $subdiv_cls_mass;
            $this->view->department = $staff_record->Department;
            $this->view->subject_teacher_list = $subdivnamss;
            $user = 'staff';
            //$this->view->announcements = Announcement::find("to='staff'");
//            $this->view->announcements = Announcement::find('to LIKE "%,' . $user . ',%" OR to LIKE "%' . $user . ',%" OR to LIKE "%,' . $user . '%" OR to LIKE "' . $user . '" ORDER BY id DESC');
            //$announcements = Announcement::find("to='staff'");
            // $this->view->address = $staff_record->Address1 . ',</br>' . $staff_record->Address2 . ',</br>' .
            //$staff_record->State . ',</br>' . $staff_record->Country . ':' . $student_record->Pin;
            //$this->view->contact =  //No contact info stored in student_info, also, no field for city
            //$this->view->subjects = $subjects;
            $this->view->photo_url = $photo_url;



            //$this->view->announcements = Announcement::find('to="student"');
            //$this->view->subjects = SubDivisionSubjects::find('divValID =' . $staff_record->Division_Class . ' and subDivValId=' . $staff_record->Subdivision_section);
        }
    }

    public function loadAppointmentdetailsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $staff_record = StaffInfo::findFirstById($this->request->get('staffId'));
        $staffId = $staff_record->id;
//        print_r($staffId);exit;
        $this->view->subdivexist = 0;
        $this->view->classteacherexist = 0;
        $this->view->subjectteacherexist = 0;
        if ($staff_record) {


            $this->view->staffinfo = $staff_record;
            $this->view->staff_name = $staff_record->Staff_Name;
            $this->view->appointment_no = $staff_record->appointment_no;

            $stf_mapdets = ($staff_record->aggregate_key) ? explode(',', $staff_record->aggregate_key) : '';
            $subdiv = array();
            $maping_values = '';

            if ($stf_mapdets != '') {

                foreach ($stf_mapdets as $stf_mapdet) {

                    $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stf_mapdet);

                    $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);

                    $subdiv[] = array(
                        'name' => $orgnztn_str_mas_det->name,
                        'value' => $orgnztn_str_det->name);
// $maping_values .= $orgnztn_str_mas_det->name . '-' . $orgnztn_str_det->name . ':';
                }
            }

            //Get the photo from StaffGeneralMaster
            $staff_profile = StaffGeneralMaster::findfirst('staff_id = ' . $staff_record->id);
            if ($staff_profile) {
                $photo_url = FILES_URI . $staff_profile->photo;
            } else if ($staff_record->Gender == '1') {
                $photo_url = "images/female_staff.png";
            } else if ($staff_record->Gender == '2') {
                $photo_url = "images/male_staff.png";
            } else {
                $photo_url = "images/User.png";
            }
        }

        $this->view->staffdivname = $staffdivname;
        $this->view->staffsubdivname = $staffsubdivname;
        $this->view->studentdivname = $studentdivname;
        $this->view->studentsubdivname = $studentsubdivname;
        $this->view->stfclsdet = $subdiv;
        $this->view->class_teacher = $class_teacher;
        $this->view->department = $staff_record->Department;
        $this->view->subject_teacher_list = $subject_teacher_list;
        $user = 'staff';
        //$this->view->announcements = Announcement::find("to='staff'");
        $this->view->announcements = Announcement::find('to LIKE "%,' . $user . ',%" OR to LIKE "%' . $user . ',%" OR to LIKE "%,' . $user . '%" OR to LIKE "' . $user . '" ORDER BY id DESC');

        $this->view->photo_url = $photo_url;
    }

    /** Get All StaffList * */
    public function getAllStaffListAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->staffs = StaffInfo::find();


        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');

        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                //if ($field->mandatory_for_admission == 1) {
//                    $nodes[$field->id] = $field->name;
                $nodes[$field->id] = $field->name;
                //}
            }
        endif;

        $this->view->nodes = $nodes; //$this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $nodes; //$this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadStaffListtableHeaderAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');

        if (count($acdyrMas) > 0):
            foreach ($acdyrMas as $field) {
                //if ($field->mandatory_for_admission == 1) {
//                    $nodes[$field->id] = $field->name;
                $nodes[$field->id] = $field->name;
                //}
            }
        endif;

        $this->view->nodes = $nodes; //$this->_getNonMandNodesForAssigning($acdyrMas->id);
        $this->view->mandnode = $nodes; //$this->_getMandNodesForAssigning($acdyrMas);
    }

    public function loadStaffListtableDataAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();

        $aggregateval = '';
        foreach ($this->request->getPost() as $key => $value) {
            $IsSubdiv = explode('_', $key);
//            if ($IsSubdiv[0] == 'subDivVal') {
//                $params[$IsSubdiv[0]][] = $value;
//            } else {

            $params[$key] = $value;
            //}

            if ($IsSubdiv[0] == 'aggregate') {

                if ($value != '')
                    $aggregateval .= $value . ',';
            }
        }


        $department = $this->request->getPost('department');

        if (isset($params['appointment_no']) && $params['appointment_no'] != '') {
            $queryParams[] = "appointment_no='" . $params['appointment_no'] . "'";
        }

        if (isset($params['name']) && $params['name'] != '') {

            $queryParams[] = "Staff_Name LIKE '" . $params['name'] . "%'";
        }

        if (isset($params['loginid']) && $params['loginid'] != '') {

            $queryParams[] = "loginid = '" . $params['loginid'] . "'";
        }


        if (isset($params['sSearch']) && $params['sSearch'] != ''):

            $psql_deprt = OrganizationalStructureValues::findFirst('name LIKE"%' . $params['sSearch'] . '%"');       //'SELECT id FROM OrganizationalStructureValues WHERE name LIKE"%' . $params['sSearch'] . '%"';

            if (isset($psql_deprt)) {
                $dep_id = isset($psql_deprt->id) ? $psql_deprt->id : '';
                if ($dep_id != '')
                    $queryParams1[] = 'aggregate_key Like "%' . $dep_id . '%"';
            }

            $queryParams1[] = 'appointment_no Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'Staff_Name  Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'status Like "%' . $params['sSearch'] . '%"';
            $queryParams1[] = 'loginid Like "%' . $params['sSearch'] . '%"';
            $queryParams[] = count($queryParams1) > 0 ? '(' . implode(' OR ', $queryParams1) . ')' : '';
        endif;

        $queryParams[] = "status != 'NULL'";
        $conditionvals = (count($queryParams) > 0) ? ' WHERE ' . implode(' and ', $queryParams) : '';

        $phql = "SELECT appointment_no,Staff_Name,Gender,status,
            		aggregate_key,loginid,Date_of_Joining,Date_of_Birth,id FROM StaffInfo" . $conditionvals;

//         echo $phql;
//         exit;
//         
        $phql2 = $phql;

        for ($i = 0; $i < $this->request->getPost('iSortingCols'); $i++) {
            $phql .= ' ORDER BY ' . $this->getSortColumnNameForStaffList($this->request->getPost('iSortCol_' . $i)) . ' ' .
                    $this->request->getPost('sSortDir_' . $i);
        }
        $phql .= ' LIMIT ' . $this->request->getPost('iDisplayStart') . ',' . $this->request->getPost('iDisplayLength');

        $result = $this->modelsManager->executeQuery($phql);


        $result2 = $this->modelsManager->executeQuery($phql2);


        $rowEntries = $this->formatStaffListTableData($result);
        $tableData = array(
            "sEcho" => intval($this->request->getPost('sEcho')),
            "iTotalRecords" => count($rowEntries),
            "iTotalDisplayRecords" => count($result2),
            "aaData" => $rowEntries,
        );
        echo json_encode($tableData);
        exit;
    }

    public function getSortColumnNameForStaffList($sortColumnIndex) {
        if (!isset($sortColumnIndex)) {
            return "appointment_no";
        }
        switch ($sortColumnIndex) {

            case 0:
                return "appointment_no";
            case 1:
                return "Staff_Name";
            case 2:
                return "Gender";
            case 3:
                return "loginid";
            case 4:
                return "Date_of_Birth";
            case 5:
                return "Date_of_Joining";
            default:
                return "appointment_no";
        }
    }

    public function formatStaffListTableData($result) {
        $rowEntries = array();

        if (count($result) > 0) {
            foreach ($result as $items):
                $row = array();
                $row['appointment_no'] = $items->appointment_no;
                $row['Staff_Name'] = $items->Staff_Name;
                $row['gender'] = ($items->Gender == 1) ? 'Female' : 'Male';
                $row['stfflogin'] = $items->loginid;
                $row['dob'] = ($items->Date_of_Birth) ? date('d-m-Y', $items->Date_of_Birth) : '';
                $row['doj'] = (date('d-m-Y', $items->Date_of_Joining)) ? date('d-m-Y', $items->Date_of_Joining) : '';
                $row['id'] = $items->id;
//                print_r($items);exit;

                $aggregatevals = $items->aggregate_key ? explode(',', $items->aggregate_key) : '';

                if ($aggregatevals != '') {
                    foreach ($aggregatevals as $aggregateval) {

                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $row[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
                    }
                }


                $row['assignsal'] = ' ';
                if ($items->status == 'Appointed') {
                    $row['Actions'] = '<span class="label label-primary " 
                                                    id="stf_appoint">Appointed</span>';

//                    $row['assignsal'] = '<a href="javascript:;" onclick="salarySettings.loadsalary(' . $items->id . ')"> 
//                                                <span class="fa fa-money"  title="Assign/Update Salary">                                         
//                                                </span>
//                                            </a>  ';
                } else if ($items->status == 'Requested') {
                    $row['Actions'] = '<button class="btn btn-success " type="button" itemID ="' . $items->id . '"    
                                                  onclick="appointmentSettings.requestForwardDetails(this)"  
                                                    id="stf_appoint">Forward</button>';
                } else if ($items->status == 'Approved') {
                    $row['Actions'] = '<button class="btn btn-primary " type="button" itemID ="' . $items->id . '"    
                                                  onclick="appointmentSettings.appointStaff(this)"  
                                                    id="stf_appoint">Appoint</button>';
                } else {
                    $row['Actions'] = '<span class="label label-warning"                
                                        appType ="' . ( $items->status == 'Relieving initiated' ? 9 : 10) . '"  itemID ="' . $items->id . '"    
                                                   "  
                                                    id="stf_status">' . $items->status . '</span>';
                }


                $staffgeninfo = StaffGeneralMaster::findFirstByStaffId($items->id);
                if ($staffgeninfo->photo) {
                    $row['Staff_Name'] = '<span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon"  >';
                    $row['Staff_Name'] .= $this->tag->image(array(
                        "src" => $staffgeninfo->photo ? (FILES_URI . $staffgeninfo->photo) : ('images/user.jpg'),
                        'style' => 'border-radius:50%',
                        'width' => '40px',
                        'height' => '40px'));

                    $row['Staff_Name'] .= '</span>';
                } else {
                    $row['Staff_Name'] = ' <span title="' . $items->appointment_no . '" 
                                              class="mini-stat-icon tar " style="cursor:default;" >' . substr($items->Staff_Name, 0, 1) . '</span>';
                }
                // $row['Staff_Name'] .= '<span title="' . $items->appointment_no . '"  style="vertical-align: middle;position: relative;top: 5px;"> 
                //                       ' . $items->Staff_Name .'</span>';

                $row['Staff_Name'] .= '<span title="' . $items->Staff_Name . ' (' . $items->appointment_no . ') ' . '" class="name"> 
                                        ' . (strlen($items->Staff_Name) > 15 ? substr($items->Staff_Name, 0, 15) . "..." : $items->Staff_Name) . '</span>';




//                $row['assignsal'] = '<a href="' . $this->url->get("salary/assign", array("staffId" => $items->id)) . '"> 
//                                                <span class="fa fa-money"  title="Assign/Update Salary">                                         
//                                                </span>
//                                            </a>  ';
                $rowEntries[] = $row;
            endforeach;
        }

        return $rowEntries;
    }

    /** Get Staff Attendance Details * */
    public function getStaffAttendanceForMonthAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isGet()) {
            $params = $this->request->get();
            $staff_id = $params['staff_id'];
            $att_table_format = array();
            $staffloginid = StaffInfo::findFirstById($staff_id)->loginid;
            $start_date = $params['from'] / 1000;
            $end_date = $params['to'] / 1000;
            $att_table_format = array();
            $param['type'] = 'attendance';
            $obj = new Cassandra();
            $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);
            if ($res) {
                $buildquery = "select * from day_attendance where"
                        . " user_id = '" . $staffloginid . "'"
                        . " and date >= $start_date "
                        . " and date <= $end_date "
                        . " ALLOW FILTERING; ";
//                print_r($buildquery);exit;
                if ($result = $obj->query($buildquery)) {
                    for ($i = 0; $i < count($result); $i++):
                        $abbrevation = AttendanceSelectbox::findFirstById($result[$i]['value']);
                        $attPeriod = PeriodMaster::findFirstById($result[$i]['period'])->period;
                        $att_table_format['result'][] = array(
                            "id" => $result[$i]['period'] . '' . $result[$i]['date'],
                            "title" => 'Attendance value for period ' . $attPeriod . ' : ' . $abbrevation->attendancename,
                            "class" => "event-custom-" . str_replace("#", "", $abbrevation->color),
                            "start" => $result[$i]['date'] * 1000,
                            "end" => $result[$i]['date'] * 1000
                        );
                    endfor;
                }
            }
            $obj->close();
            $att_table_format['success'] = 1;
            echo json_encode($att_table_format);
            exit;
        }
    }

    public function ratingViewAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        // $this->assets->addJs('js/studenthomepage/rating.js');
        if ($this->request->isPost()) {

            $staffid = $this->view->staffid = $this->request->getPost("staffid");


            // $subjects = StaffRating::find('staff_id =' . $staffid);


            $staffrating = StaffRating::find('staff_id =' . $staffid);


            $rowArr = array();
            $ratingCat = array();
            foreach ($staffrating as $subjects) {


                $ratingCategorys = StaffRatingCategoryMaster::findFirst('id=' . $subjects->rating_category);

                $ratingValue = StaffRatingCategoryValues::findFirst('id= "' . $subjects->rating_value . '" and rating_category =' . $ratingCategorys->id);

                $rowArr[] = array(
                    'rating_catgrynam' => $ratingCategorys->category_name,
                    'rating_level_name' => $ratingValue->rating_level_name,
                    'rating_level_value' => $ratingValue->rating_level_value,
                    'ratingPoints' => (($ratingValue->rating_level_value / 100) * $ratingCategorys->category_weightage),
                    'ratingWeightage' => $ratingCategorys->category_weightage
                );
            }
//         echo '<pre>'; 
//          print_r(count($staffrating));
//          print_r($rowArr);
//          exit;
            //$new = array_column($yaxisuniqueArr, 'last_name', 'id');
//        echo '<pre>';
//        print_r($rowArr);
//        print_r($yaxisuniqueArr);
//        print_r($xaxisuniqueArr);
//        exit;
            $this->view->ratingArr = $rowArr;
        } else {
            $this->view->ratingArr = '';
        }
        // $this->view->yaxisuniqueArr = $yaxisuniqueArr;
        // $this->view->xaxisuniqueArr = $xaxisuniqueArr;
    }

    public function staffActivitiesAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $staff_record = StaffInfo::findFirstById($this->request->get('staffId'));
        $this->view->type = $this->request->get('type');
        if (isset($staff_record)) {
            $this->view->staffinfo = $staff_record;
            $this->view->attendanceHolidays = AttendanceDays::find('day_type = 1 and user_type = "staff"');
            $this->view->staff_att_legend = AttendanceSelectbox::find("attendance_for = 'staff'");
            $staffattendanceEve = array();
            foreach ($this->view->attendanceHolidays as $holiday) {
                $date = $holiday->date;
                $staffattendanceEve[date('d-m-Y', $date)] = 'holiday';
            }
            $this->view->staffattendanceEveJson = json_encode($staffattendanceEve);
        }
    }

    public function staffPayrollAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        //  $staff_record = StaffInfo::findFirstById($this->request->get('staffId'));

        if ($this->request->isPost()) {
            $month = $this->request->getPost('month');
            $staffid = $this->request->getPost('staffid');

            $salary_detect_arr = array();
            $salary_earning_arr = array();

            $staff_salry_permnthdet = StaffSalaryPermonth::findFirst('staffid =' . $staffid . ' and monthyear LIKE"%' . $month . '%"');


            $salary_detect_arr[] = 'Advance-' . $staff_salry_permnthdet->advance_deductions;
            $salary_detect_arr[] = 'Leave-' . $staff_salry_permnthdet->leave_deductions;
            $salary_detect_arr[] = 'Transport-' . $staff_salry_permnthdet->transport_deductions;
            $salary_detect_arr[] = 'Others-' . $staff_salry_permnthdet->other_deductions;


            $staff_assign_salrydets = StaffAssignedSalary::find('staffid =' . $staffid);

            if (isset($staff_assign_salrydets) && count($staff_assign_salrydets) > 0) {
                foreach ($staff_assign_salrydets as $staff_assign_salrydet) {

                    $expld_val = explode('_', $staff_assign_salrydet->paytype);

                    if ($expld_val[0] != 'Gross') {
                        $salary_earning_arr[] = $staff_assign_salrydet->paytype . '-' . $staff_assign_salrydet->payamount;
                    }
                }
            }

            $this->view->salary_detect_arr = $salary_detect_arr;
            $this->view->salary_earning_arr = $salary_earning_arr;
            $this->view->netpay = $staff_salry_permnthdet->netsalary;
            ;
        } else {
            $this->view->salary_detect_arr = '';
            $this->view->salary_earning_arr = '';
            $this->view->netpay = '';
        }
    }

    public function getStaffAdvanceDetAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {

            $staffid = $this->request->getPost('staffid');
//            print_r(implode(' and ', $queryParams));exit;
            $this->view->staffAdv = StaffAdvance::find('staffid = ' . $staffid);
            $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::find('mandatory_for_appointment =1');
        }
    }

}
