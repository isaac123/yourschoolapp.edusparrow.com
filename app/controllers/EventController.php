<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class EventController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
        $this->assets->addJs('js/appscripts/event/event.js');
    }

    public function selectEventUserAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $ids = $this->request->getPost('ids');
        if ($ids) {
            $userlist = explode(',', $ids);
            foreach ($userlist as $value) {
                $allIds = explode('_', $value);
                if ($allIds[0] == 'class') {
                    $classroom = ClassroomMaster::findFirstById($allIds[1]);
                    $allnames['id'] = 'class_' . $classroom->id;
                    $allnames['name'] = $classroom->name;
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'student') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'student_' . $studinfo->id;
                    $allnames['name'] = $studinfo->Student_Name;
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'parent') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'parent_' . $studinfo->id;
                    $allnames['name'] = $studinfo->Student_Name . "'s Parent";
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'staff') {
                    $stfinfo = StaffInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'staff_' . $stfinfo->id;
                    $allnames['name'] = $stfinfo->Staff_Name;
                    $all[] = $allnames;
                }if ($allIds[0] == 'node' || $allIds[0] == 'school') {
                    $orgval = OrganizationalStructureValues::findFirstById($allIds[1]);
                    $allnames['id'] = $allIds[0] . '_' . $orgval->id;
                    $allnames['name'] = $orgval->name;
                    $all[] = $allnames;
                }
            }
            $this->view->userids = json_encode($all);
        }
    }

    public function saveEventsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $params = $queryParams = array();
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        foreach ($this->request->getPost() as $key => $value) {
            $IsSubdiv = explode('_', $key);
            if ($IsSubdiv[0] == 'file' && $value) {
                $params['files'][] = $value;
            } else {
                $params[$key] = $value;
            }
        }
        if (!$params['event_text'] || !$params['eventUserIds'] || !$params['eventtype']) {
            $messages['type'] = 'error';
            $messages['message'] = 'Fill all Required Fields!';
            print_r(json_encode($messages));
            exit;
        }
        $st = strtotime($params['event_date']);
        $et = strtotime($params['event_todate']);
        if ($et && $st > $et) {
            $messages['type'] = 'error';
            $messages['message'] = 'Invalid date range';
            print_r(json_encode($messages));
            exit;
        }
        $events = $params['eventid'] ? Events::findFirstById($params['eventid']) : new Events();
        $events->title = $params['event_title'] ? $params['event_title'] : '';
        $events->description = $params['event_text'] ? $params['event_text'] : '';
        $events->from_date = $params['event_date'] ? strtotime($params['event_date']) : '';
        $events->to_date = $params['event_todate'] ? strtotime($params['event_todate']) : '';
        $events->location = $params['location'] ? $params['location'] : '';
        $events->type = $params['eventtype'] ? $params['eventtype'] : '';
        $events->selected_userlist = $params['eventUserIds'] ? $params['eventUserIds'] : '';
        $events->created_by = $uid;
        $events->created_on = time();
        $events->modified_by = $uid;
        $events->modified_on = time();
        if ($params['eventid']):
            $events->status = '';
        endif;
        if ($events->save()) {
            if ($params['files']) {
                foreach ($params['files'] as $file) {
                    $expld = explode('_', $file);
                    if (!is_dir(UPLOAD_DIR . 'event/event_' . $events->id)) {
                        if (!mkdir(UPLOAD_DIR . 'event/event_' . $events->id, 0777, true)) {
                            die('Failed to create folders...');
                        }
                    }
                    if (!is_file(UPLOAD_DIR . 'event/event_' . $events->id . '/' . $expld[0])) {
                        if (is_file(UPLOAD_DIR . 'temp/event/' . $expld[0])) {
                            copy(UPLOAD_DIR . 'temp/event/' . $expld[0], UPLOAD_DIR . 'event/event_' . $events->id . '/' . $expld[0]);
                        }
                    }
                }
                $events->upload = 'event_' . $events->id;
                if (is_dir(UPLOAD_DIR . 'temp/event')) {
                    ContentSharingController::deleteDir(UPLOAD_DIR . 'temp/event');
                }
                $events->save();
            }
            $userlist = explode(',', $events->selected_userlist);
            foreach ($userlist as $value) {
                $allIds = explode('_', $value);
                if ($allIds[0] == 'class') {
                    $classroom = ClassroomMaster::findFirstById($allIds[1]);
                    $res = ControllerBase::buildStudentQuery($classroom->aggregated_nodes_id);
                    $stuquery = 'SELECT stumap.id,stumap.student_info_id,stuinfo.loginid'
                            . ' FROM StudentMapping stumap LEFT JOIN'
                            . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" and '
                            . ' (' . implode(' or ', $res) . ')';
                    $students = $this->modelsManager->executeQuery($stuquery);
                    foreach ($students as $student) {
                        if (!$loginIds[$student->loginid])
                            $loginIds[$student->loginid] = $student->loginid . '_ClassEvents_class';
                    }
                }
                if ($allIds[0] == 'student') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $loginIds[$studinfo->loginid] = $studinfo->loginid . '_Events_student';
                }
                if ($allIds[0] == 'parent') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $loginIds[$studinfo->loginid] = $studinfo->loginid . '_Events_parent';
                }
                if ($allIds[0] == 'staff') {
                    $stfinfo = StaffInfo::findFirstById($allIds[1]);
                    $loginIds[$stfinfo->loginid] = $stfinfo->loginid . '_Events_staff';
                }
                if ($allIds[0] == 'node') {
                    $stuquery = 'SELECT stuinfo.loginid,stumap.id,stumap.student_info_id,stuinfo.Student_Name,stuinfo.id'
                            . ' FROM StudentMapping stumap INNER JOIN'
                            . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" '
                            . ' and FIND_IN_SET(' . $allIds[1] . ',stumap.aggregate_key)';
                    $node_studet = $this->modelsManager->executeQuery($stuquery);

                    if (count($node_studet) > 0) {
                        foreach ($node_studet as $stu) {
                            if (!$loginIds[$stu->loginid])
                                $$loginIds[$stu->loginid] = $stu->loginid . '_ClassEvents_node';
                        }
                    }
                    $staffs = StaffInfo::find('FIND_IN_SET(' . $allIds[1] . ',aggregate_key)');

                    if (count($staffs) > 0) {
                        foreach ($staffs as $staf) {
                            if (!$loginIds[$staf->loginid])
                                $loginIds[$staf->loginid] = $staf->loginid . '_ClassEvents_node';
                        }
                    }
                }
                if ($allIds[0] == 'school') {
                    $stuquery = 'SELECT stuinfo.loginid,stumap.id,stumap.student_info_id,stuinfo.Student_Name,stuinfo.id'
                            . ' FROM StudentMapping stumap INNER JOIN'
                            . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass" ';
                    $node_studet = $this->modelsManager->executeQuery($stuquery);
                    if (count($node_studet) > 0) {
                        foreach ($node_studet as $stu) {
                            if (!$loginIds[$stu->loginid])
                                $loginIds[$stu->loginid] = $stu->loginid . '_SchoolEvents_school';
                        }
                    }
                    $staffs = StaffInfo::find('status IN("Appointed","Relieving initiated")');
                    if (count($staffs) > 0) {
                        foreach ($staffs as $staf) {
                            if (!$loginIds[$staf->loginid])
                                $loginIds[$staf->loginid] = $staf->loginid . '_SchoolEvents_school';
                        }
                    }
                }
            }
            foreach ($loginIds as $eve_ids) {
                $details = array();
                $details = explode('_', $eve_ids);
                $eventlist = $params['eventid'] ?
                        (EventsList::findFirst('event_id=' . $params['eventid'] . ' and events_to = "' . $details[0] . '" and events_toname="' . $details[1] . '_' . $details[2] . '"') ?
                                EventsList::findFirst('event_id=' . $params['eventid'] . ' and events_to = "' . $details[0] . '" and events_toname="' . $details[1] . '_' . $details[2] . '"') : new EventsList()) : new EventsList();
                $eventlist->event_id = $events->id;
                $eventlist->events_to = $details[0];
                $eventlist->events_toname = $details[1] . '_' . $details[2];
                $eventlist->created_by = $uid;
                $eventlist->created_date = time();
                $eventlist->modified_by = $uid;
                $eventlist->modified_date = time();
                if ($params['eventid']):
                    $eventlist->status = '';
                endif;
                if (!$eventlist->save()) {
                    foreach ($eventlist->getMessages() as $msg) {
                        $error .= $msg . '<br>' . '<br>';
                    }
                    $messages['type'] = 'error';
                    $messages['message'] = $error;
                    print_r(json_encode($messages));
                    exit;
                }
            }
            $messages['type'] = 'success';
            $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
            $messages['message'] = ' Events Added Successfully';
            print_r(json_encode($messages));
            exit;
        } else {
            foreach ($events->getMessages() as $msg) {
                $error .= $msg . '<br>';
            }
            $messages['type'] = 'error';
            $messages['message'] = $error;
            print_r(json_encode($messages));
            exit;
        }
    }

    public function acceptEventsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $eventid = $this->request->getPost('event_id');
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $eventto = EventsList::findFirst('event_id=' . $eventid . ' and events_to="' . $identity['name'] . '"');
        $eventto->status = 'Accepted';
        if ($eventto->save()) {
            $events_item = EventsList::find('event_id=' . $eventid);
            $events = Events::findFirstById($eventid);
            $allloginIds = $acceptid = array();
            foreach ($events_item as $items) {
                $allloginIds[] = $items->events_to;
                if ($items->status == 'Accepted'):
                    $acceptid[] = $items->events_to;
                endif;
            }
            $valu1 = array_unique($allloginIds);
            $valu2 = array_unique($acceptid);
            $pending = array_diff($valu1, $valu2);
            if (count($pending) == 0) {
                $events->status = 'Accepted';
            }
            $events->modified_by = $uid;
            $events->modified_on = time();
            if ($events->save()) {
                $messages['type'] = 'success';
                $messages['time'] = AnnouncementController::time_elapsed_string(date('Y-m-d H:i:s', time()));
                $messages['message'] = 'Events Accepted Successfully';
                print_r(json_encode($messages));
                exit;
            } else {
                foreach ($events->getMessages() as $msg) {
                    $error .= $msg . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        } else {
            foreach ($eventto->getMessages() as $msg) {
                $error .= $msg;
            }
            $messages['type'] = 'error';
            $messages['message'] = $error;
            print_r(json_encode($messages));
            exit;
        }
    }

    public function viewEventsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->eventid = $eventid = $this->request->getPost('event_id');
        $this->view->edit = $edit = $this->request->getPost('edit');
        $this->view->event = $event = Events::findFirstById($eventid);
        $this->view->eventslist = $eventslist = EventsList::find('event_id=' . $eventid . ' ORDER BY modified_date DESC');
        $this->view->acc_list = $acc_list = EventsList::find('event_id=' . $eventid . ' and  status = "Accepted"  ORDER BY modified_date DESC');
        $this->view->event_comments = $event_comments = EventsComments::find('master_id = ' . $eventid . '  ORDER BY modified_date DESC');
        $this->view->identity = $identity = $this->auth->getIdentity();
        $this->view->uid = $uid = StaffInfo::findFirstByLoginid($identity['name']);
        $this->view->eventby = $eventby = StaffInfo::findFirstById($event->created_by)->Staff_Name;
    }

    public function advanceSettingsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }
        $all = array();
        $this->view->params = $params;
        $storeFolder = UPLOAD_DIR . 'temp/event';
        $ds = DIRECTORY_SEPARATOR;
        $files = scandir($storeFolder);
        $result = array();
        if (false !== $files) {
            foreach ($files as $file) {
                if ('.' != $file && '..' != $file) {
                    $obj['name'] = $file;
                    $obj['size'] = AnnouncementController::formatSizeUnits(filesize($storeFolder . $ds . $file));
                    $result[] = $obj;
                }
            }
        }
        $this->view->files = $result;
        if ($params['eventUserIds']) {
            $userlist = explode(',', $params['eventUserIds']);
            foreach ($userlist as $value) {
                $allIds = explode('_', $value);
                if ($allIds[0] == 'class') {
                    $classroom = ClassroomMaster::findFirstById($allIds[1]);
                    $allnames['id'] = 'class_' . $classroom->id;
                    $allnames['name'] = $classroom->name;
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'student') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'student_' . $studinfo->id;
                    $allnames['name'] = $studinfo->Student_Name;
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'parent') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'parent_' . $studinfo->id;
                    $allnames['name'] = $studinfo->Student_Name . "'s Parent";
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'staff') {
                    $stfinfo = StaffInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'staff_' . $stfinfo->id;
                    $allnames['name'] = $stfinfo->Staff_Name;
                    $all[] = $allnames;
                }if ($allIds[0] == 'node' || $allIds[0] == 'school') {
                    $orgval = OrganizationalStructureValues::findFirstById($allIds[1]);
                    $allnames['id'] = $allIds[0] . '_' . $orgval->id;
                    $allnames['name'] = $orgval->name;
                    $all[] = $allnames;
                }
            }
            $this->view->userids = json_encode($all);
        }
    }

    public function downloadeventFileAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $eventid = $this->request->getPost('event_id');
            $event = Events::findFirstById($eventid);
            $file = UPLOAD_DIR . $event->upload;
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . $event->upload . '";');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                exit;
            }
        }
    }

    public function saveCommentsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $id = $this->request->getPost('eventid');
        $comments = $this->request->getPost('comments');
        $type = $this->request->getPost('type');
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
    }

    public function editEventsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->eventid = $eventid = $this->request->getPost('event_id');
        $this->view->event = $event = Events::findFirstById($eventid);
        $all = array();
        if ($event->selected_userlist) {
            $userlist = explode(',', $event->selected_userlist);
            foreach ($userlist as $value) {
                $allIds = explode('_', $value);
                if ($allIds[0] == 'class') {
                    $classroom = ClassroomMaster::findFirstById($allIds[1]);
                    $allnames['id'] = 'class_' . $classroom->id;
                    $allnames['name'] = $classroom->name;
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'student') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'student_' . $studinfo->id;
                    $allnames['name'] = $studinfo->Student_Name;
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'parent') {
                    $studinfo = StudentInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'parent_' . $studinfo->id;
                    $allnames['name'] = $studinfo->Student_Name . "'s Parent";
                    $all[] = $allnames;
                }
                if ($allIds[0] == 'staff') {
                    $stfinfo = StaffInfo::findFirstById($allIds[1]);
                    $allnames['id'] = 'staff_' . $stfinfo->id;
                    $allnames['name'] = $stfinfo->Staff_Name;
                    $all[] = $allnames;
                } if ($allIds[0] == 'node' || $allIds[0] == 'school') {
                    $orgval = OrganizationalStructureValues::findFirstById($allIds[1]);
                    $allnames['id'] = $allIds[0] . '_' . $orgval->id;
                    $allnames['name'] = $orgval->name;
                    $all[] = $allnames;
                }
            }
            $this->view->userids = json_encode($all);
        }
        $result = array();
        $uploadedfile = $event->upload ? explode(',', $event->upload) : '';
        if ($uploadedfile) {
            $storeFolder = UPLOAD_DIR . 'event/event_' . $event->id;
            $ds = DIRECTORY_SEPARATOR;
            $files = scandir($storeFolder);
            $result = array();
            if (false !== $files) {
                foreach ($files as $file) {
                    if ('.' != $file && '..' != $file) {
                        $obj['name'] = $file;
                        $obj['size'] = AnnouncementController::formatSizeUnits(filesize($storeFolder . $ds . $file));
                        $result[] = $obj;
                    }
                }
            }
            $this->view->files = $result;
        }
    }

}
