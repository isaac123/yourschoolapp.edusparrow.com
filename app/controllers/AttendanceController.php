<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class AttendanceController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function staffAttendanceAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Attendance | ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs("js/attendance/attendance.js");
        $fields = OrganizationalStructureMaster::findFirst('module LIKE "Department" and cycle_node != 1');
        $assignedCount = OrganizationalStructureValues::find('org_master_id = ' . $fields->id);
        $combinations = array();
        if (count($assignedCount) > 0) {
            foreach ($assignedCount as $assignedDept) {
                $combinations[$assignedDept->id] = $assignedDept->name;
            }
            $this->view->combinations = $combinations;
        } else {
            $this->view->errorMessage = "No $fields->name added yet!";
        }
    }

    public function getStaffListAction() {
// Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Attendance | ");
        $this->assets->addCss('css/edustyle.css');
        if ($this->request->isPost()) {
            $UType = $this->request->getPost('UType');
            $this->view->cdate = strtotime($this->request->getPost('currentDate'));
            $this->view->staff = StaffInfo::find('status = "Appointed"');
            $this->view->noofperiods = AttendanceFrequency::findFirst("frequency_for = '$UType'")->frequency;

            $this->view->attdays = AttendanceDays::findFirst("date = '" . $this->view->cdate . "'"
                            . " and user_type='$UType'");
            $Period = $this->view->attdays ? AttendancePeriods::findFirst(array(
                        "att_days_id =" . $this->view->attdays->id,
                        "columns" => "group_concat(period_value) as periods"
                    )) : '';
            $this->view->attPeriod = $Period ? $Period->periods : '';
            $this->view->attValues = AttendanceSelectbox::find("attendance_for ='staff'");
        }
    }

    public function addOrUpdateSlotAction() {
// Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $message = array();
            try {
                $dayType = $this->request->getPost('dayValue');
                $currDate = strtotime($this->request->getPost('currentDate') . ' 00:00:00');
                $UType = $this->request->getPost('UType');
                $masterid = 0;
                $teacherType = $this->request->getPost('type');
                $teacherTypeID = ($teacherType == 'classTeacher') ? 1 : 0;
//                $current_acd_yr = Controllerbase::get_current_academic_year();
//                        (($UType == 'student') ? (($teacherType == 'classTeacher') ?
//                                        ClassTeachersMaster::findFirstById($masterid) :
//                                        SubDivisionSubjects::findFirstById($masterid)) : '');

                $attdays = ($UType == 'student') ? AttendanceDays::findFirst("date = '" . $currDate . "'"
                                . " and user_type='" . $UType . "'"
                                . " and permission_type = '$teacherTypeID'") :
                        AttendanceDays::findFirst("date = '" . $currDate . "'"
                                . " and user_type='" . $UType . "'");
                if (!$attdays) {
                    $attdays = new AttendanceDays();
                } else {

                    if ($dayType == 1) {

                        $attPeriod = AttendancePeriods::findFirst("att_days_id =" . $attdays->id) ?
                                AttendancePeriods::find("att_days_id =" . $attdays->id) : '';
                        $att_val = AttendanceValues::findFirst("att_days_id=" . $attdays->id) ?
                                AttendanceValues::find("att_days_id=" . $attdays->id) : '';
                        if ($att_val && !$att_val->delete()) {
                            $error = '';
                            foreach ($att_val->getMessages() as $messages) {
                                $error .= $messages . '\n';
                            }
                            $message['type'] = 'error';
                            $message['message'] = $error;
                            print_r(json_encode($message));
                            exit;
                        }
                        if ($attPeriod && !$attPeriod->delete()) {
                            $error = '';
                            foreach ($attPeriod->getMessages() as $messages) {
                                $error .= $messages . '\n';
                            }
                            $message['type'] = 'error';
                            $message['message'] = $error;
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                }

                $attdays->node_id = $masterid;
                $attdays->permission_type = ($UType == 'student') ? $teacherTypeID : '-1';
//                $attdays->academic_year_id = $current_acd_yr;
                $attdays->date = $currDate;
                $attdays->day_type = $dayType;
                $attdays->user_type = $UType;

                if ($attdays->save()) {
                    $message['type'] = 'success';
                    $message['message'] = date('d-m-Y', $currDate) . (($attdays->day_type == 1) ? ' is Holiday' : ' is Working') . ' Day';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($attdays->getMessages() as $messages) {
                        $error .= $messages . '\n';
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            } catch (Exception $ex) {
                $error = '';
                foreach ($ex->getMessages() as $messages) {
                    $error .= $messages . '\n';
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function addOrUpdatePeriodAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $message = array();
            try {
                $currDate = strtotime($this->request->getPost('currentDate') . ' 00:00:00');
                $UType = $this->request->getPost('UType');
//                $master_id = $UType == 'staff' ? 0 : $this->request->getPost('masterid');
                $teacherType = $this->request->getPost('type');
                $teacherTypeID = ($teacherType == 'classTeacher') ? 1 : 0;
                $periods = $this->request->getPost('periods');
                $period_value = $this->request->getPost('period_value');
                $master_id = 0;


                $attdays = ($UType == 'student') ?
                        AttendanceDays::findFirst("date = '" . $currDate . "'"
                                . " and user_type='" . $UType . "'"
                                . " and permission_type = '$teacherTypeID'") :
                        AttendanceDays::findFirst("date = '" . $currDate . "'"
                                . " and user_type='" . $UType . "'");
                if (!$attdays) {
                    $message['type'] = 'error';
                    $message['message'] = 'Day status is required!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    if ($attdays->day_type == '1') {
                        $message['type'] = 'error';
                        $message['message'] = 'Attendance cannot be marked.This day is holiday!';
                        print_r(json_encode($message));
                        exit;
                    }
                    $attPeriod = AttendancePeriods::findFirst("att_days_id =$attdays->id "
                                    . " and period_value =$periods");
                    if (!$attPeriod) {
                        $attPeriod = new AttendancePeriods();
                    }
                    $attPeriod->att_days_id = $attdays->id;
                    $attPeriod->period_value = $periods;
                    if ($period_value == 1) {
//                            print_r($attPeriod);exit;
//                            
                        //save period
                        if (!$attPeriod->save()) {
                            $error = '';
                            foreach ($attPeriod->getMessages() as $messages) {
                                $error .= $messages . "\n";
                            }
                            $message['type'] = 'error';
                            $message['message'] = $error;
                            print_r(json_encode($message));
                            exit;
                        } else {
                            if ($UType == 'staff') {
                                //check for Leave Applied B4
                                $levApprvd = LeaveRequest::findFirst("'" . $currDate . "'" .
                                                " BETWEEN  fromDate AND toDate  " .
                                                " and status LIKE 'Approved'");

                                if (($levApprvd)):

//                                foreach ($levApproved as $levApprvd):
//                            print_r($levApprvd->staffId);   
                                    $user = StaffInfo::findFirstById($levApprvd->staffId);
                                    $userId = $user->id;
                                    if ($userId > 0 && in_array($periods, explode(',', $levApprvd->frequency))):

                                        $attenValueExceeds = AttendanceSelectbox::findFirstById($levApprvd->leaveType);
                                        if ($attenValueExceeds->is_allowed_by_count == 1 && $UType == 'staff') {
                                            $start = date("1-m-Y", mktime(0, 0, 0, date("m", $currDate), date("d", $currDate), date("Y", $currDate)));
                                            $end = date("t-m-Y", mktime(0, 0, 0, date("m", $currDate), date("d", $currDate), date("Y", $currDate)));

                                            $sql = "SELECT count(*) as cnt from AttendanceDays  as attdays"
                                                    . " INNER JOIN AttendancePeriods as attper on attper.att_days_id  = attdays.id "
                                                    . " INNER JOIN AttendanceValues as attval on attval.att_period_id = attper.id and  attval.att_days_id= attdays.id "
                                                    . " where attdays.date BETWEEN  '" . strtotime($start) . "' "
                                                    . " AND  '" . strtotime($end) . "' "
                                                    . " AND attdays.user_type =  'staff' "
                                                    . " and attval.user_id =$userId "
                                                    . " and attval.value_id = $levApprvd->leaveType";
                                            $count_attval = $this->modelsManager->executeQuery($sql);
                                            if ($count_attval[0]->cnt <= $attenValueExceeds->allowed_count) {

                                                $att_val = AttendanceValues::findFirst(
                                                                "att_period_id='" . $attPeriod->id .
                                                                "' and att_days_id='" . $attdays->id .
                                                                "' and user_id=" . $userId) ?
                                                        AttendanceValues::findFirst(
                                                                "att_period_id='" . $attPeriod->id .
                                                                "' and att_days_id='" . $attdays->id .
                                                                "' and user_id=" . $userId) :
                                                        new AttendanceValues();
                                                $att_val->att_period_id = $attPeriod->id;
                                                $att_val->att_days_id = $attdays->id;
                                                $att_val->user_id = $userId;
                                                $att_val->value_id = $levApprvd->leaveType;
//                            print_r($att_val);exit;
                                                $defaultValue = AttendanceSelectbox::findFirst("attendance_for ='$UType' and default = 1");
                                                if (($defaultValue) && ($defaultValue->id == $levApprvd->leaveType)) {
                                                    if (!$att_val->delete()) {
                                                        $error = '';
                                                        foreach ($att_val->getMessages() as $messages) {
                                                            $error .= $messages . "\n";
                                                        }
                                                        $message['type'] = 'error';
                                                        $message['message'] = $error;
                                                        print_r(json_encode($message));
                                                        exit;
                                                    }
                                                } else {
                                                    if (!$att_val->save()) {
                                                        $error = '';
                                                        foreach ($att_val->getMessages() as $messages) {
                                                            $error .= $messages . "\n";
                                                        }
                                                        $message['type'] = 'error';
                                                        $message['message'] = $error;
                                                        print_r(json_encode($message));
                                                        exit;
                                                    }
                                                }
                                            }
                                        }
                                    endif;
//                                endforeach;
                                endif;
                            }
                            $message['type'] = 'success';
                            $message['message'] = 'Period attendance has been saved!';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        $att_val = AttendanceValues::find("att_period_id=" . $attPeriod->id);
                        if (count($att_val) > 0) {
                            if (!$att_val->delete()) {
                                $error = '';
                                foreach ($attPeriod->getMessages() as $messages) {
                                    $error .= $messages . "\n";
                                }
                                $message['type'] = 'error';
                                $message['message'] = $error;
                                print_r(json_encode($message));
                                exit;
                            }
                        }
                        if ($attPeriod->delete()) {
                            $message['type'] = 'success';
                            $message['message'] = 'Period attendance has been saved!';
                            print_r(json_encode($message));
                            exit;
                        } else {
                            $error = '';
                            foreach ($attPeriod->getMessages() as $messages) {
                                $error .= $messages . "\n";
                            }
                            $message['type'] = 'error';
                            $message['message'] = $error;
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                }
            } catch (Exception $ex) {
                $error = '';
                foreach ($ex->getMessages() as $messages) {
                    $error .= $messages . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function addOrUpdateAttnValOldAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $message = array();
            try {
                $currDate = strtotime($this->request->getPost('currentDate') . ' 00:00:00');
                $UType = $this->request->getPost('UType');
                $master_id = 0;
                $teacherType = $this->request->getPost('type');
                $periods = $this->request->getPost('periods');
                $userId = $this->request->getPost('userId');
                $attVal = $this->request->getPost('attVal');
                $teacherTypeID = ($teacherType == 'classTeacher') ? 1 : 0;
                $attenValueExceeds = AttendanceSelectbox::findFirstById($attVal);
                if ($attenValueExceeds->is_allowed_by_count == 1 && $UType == 'staff') {
                    $start = date("1-m-Y", mktime(0, 0, 0, date("m", $currDate), date("d", $currDate), date("Y", $currDate)));
                    $end = date("t-m-Y", mktime(0, 0, 0, date("m", $currDate), date("d", $currDate), date("Y", $currDate)));

                    $sql = "SELECT count(*) as cnt from AttendanceDays  as attdays"
                            . " INNER JOIN AttendancePeriods as attper on attper.att_days_id  = attdays.id "
                            . " INNER JOIN AttendanceValues as attval on attval.att_period_id = attper.id and  attval.att_days_id= attdays.id "
                            . " where attdays.date BETWEEN  '" . strtotime($start) . "' "
                            . " AND  '" . strtotime($end) . "' "
                            . " AND attdays.user_type =  'staff' "
                            . " and attval.user_id =$userId "
                            . " and attval.value_id = $attVal";
                    $count_attval = $this->modelsManager->executeQuery($sql);
                    if ($count_attval[0]->cnt >= $attenValueExceeds->allowed_count) {
                        $message['type'] = 'error';
                        $message['message'] = 'Attendance exceeds the maximum allowed limit';
                        print_r(json_encode($message));
                        exit;
                    }
                }
//                $master_id = ($UType == 'staff') ? $masterid :
//                        (($UType == 'student') ? (($teacherType == 'classTeacher') ?
//                                        ClassTeachersMaster::findFirstById($masterid) :
//                                        SubDivisionSubjects::findFirstById($masterid)) : '');

                $attdays = ($UType == 'student') ? AttendanceDays::findFirst("date = '" . $currDate . "'"
                                . " and user_type='" . $UType . "'"
                                . " and permission_type = '$teacherTypeID'") :
                        AttendanceDays::findFirst("date = '" . $currDate . "'"
                                . " and user_type='" . $UType . "'");
                if (!$attdays) {
                    $message['type'] = 'error';
                    $message['message'] = 'Day status is required!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    if ($attdays->day_type == '1') {
                        $message['type'] = 'error';
                        $message['message'] = 'Attendance can be marked.This day is holiday!';
                        print_r(json_encode($message));
                        exit;
                    }
                    $attPeriod = AttendancePeriods::findFirst("att_days_id =$attdays->id "
                                    . " and period_value =$periods");
                    if (!$attPeriod) {
                        $message['type'] = 'error';
                        $message['message'] = 'Period attendance is required!';
                        print_r(json_encode($message));
                        exit;
                    } else {
                        $att_val = AttendanceValues::findFirst(
                                        "att_period_id='" . $attPeriod->id .
                                        "' and att_days_id='" . $attdays->id .
                                        "' and user_id=" . $userId) ?
                                AttendanceValues::findFirst(
                                        "att_period_id='" . $attPeriod->id .
                                        "' and att_days_id='" . $attdays->id .
                                        "' and user_id=" . $userId) :
                                new AttendanceValues();
                        $att_val->att_period_id = $attPeriod->id;
                        $att_val->att_days_id = $attdays->id;
                        $att_val->user_id = $userId;
                        $att_val->value_id = $attVal;
                        $defaultValue = AttendanceSelectbox::findFirst("attendance_for ='$UType' and default = 1");
//                            print_r($defaultValue);exit;
                        if (($defaultValue) && ($defaultValue->id == $attVal)) {
                            if (!$att_val->delete()) {
                                $error = '';
                                foreach ($att_val->getMessages() as $messages) {
                                    $error .= $messages . "\n";
                                }
                                $message['type'] = 'error';
                                $message['message'] = $error;
                                print_r(json_encode($message));
                                exit;
                            }
                        } else {
                            if (!$att_val->save()) {
                                $error = '';
                                foreach ($att_val->getMessages() as $messages) {
                                    $error .= $messages . "\n";
                                }
                                $message['type'] = 'error';
                                $message['message'] = $error;
                                print_r(json_encode($message));
                                exit;
                            }
                        }
                    }
                }

                $message['type'] = 'success';
                $message['message'] = 'Student attendance has been saved!';
                print_r(json_encode($message));
                exit;
            } catch (Exception $ex) {
                $error = '';
                foreach ($ex->getMessages() as $messages) {
                    $error .= $messages . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function studentAttendanceAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $param['type'] = 'attendance';
        $getCombination = ControllerBase::loadCombinations($param);
//        echo '<pre>';print_r($getCombination);exit;
        $this->view->combinations = $getCombination[1];
        $this->view->type = $getCombination[2];
        $this->view->errorMessage = $getCombination[0];
    }

    public function getStudentListAction() {
// Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Attendance | ");
        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs("js/attendance/stuattendance.js");
        if ($this->request->isPost()) {
            $master_id = $this->request->getPost('masterid');
            $this->view->master_id = $this->request->getPost('masterid');
            $teacherType = $this->request->getPost('type');
            $this->view->cdate = strtotime($this->request->getPost('currentDate') . ' 00:00:00');
            $this->view->scdate = strtotime($this->request->getPost('currentDate') . ' 00:00:00');
            $this->view->ecdate = strtotime($this->request->getPost('currentDate') . ' 23:59:59');
//            print_r($this->view->cdate);
//            $this->view->currentYr = $current_acd_yr = Controllerbase::get_current_academic_year();
//            echo '<pre>';
            if ($teacherType == 'classTeacher') {
                $assignedclass = GroupClassTeachers::findFirstById($master_id);
                $classmaster = ClassroomMaster::findFirstById($assignedclass->classroom_master_id);
                $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//                $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
//                print_r('status = "Inclass" and ' . implode(' or ', $res));exit;

                $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                        . 'stumap.subordinate_key,stumap.status'
                        . ' FROM StudentMapping stumap LEFT JOIN'
                        . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                        . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                        . ' and (' . implode(' or ', $res) . ') '
                        . ' ORDER BY stuinfo.Student_Name ASC';

//                print_r($stuquery);exit;
                $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);

                $this->view->students = $students; // $student = StudentMapping::find('status = "Inclass" and ' . implode(' or ', $res));
                $this->view->noofperiods = AttendanceFrequency::findFirst("frequency_for = 'student'")->frequency;
                $this->view->attdays = AttendanceDays::findFirst("date = '" . $this->view->cdate . "'"
//                        "date >= '" . $this->view->scdate . "'"
//                                . " and date <='" . $this->view->ecdate . "'"
                                . " and user_type='student'"
                                . " and permission_type = 1");
//                print_r( $this->view->attdays);exit;
                $Period = $this->view->attdays ? AttendancePeriods::findFirst(array(
                            "att_days_id =" . $this->view->attdays->id,
                            "columns" => "group_concat(period_value) as periods"
                        )) : '';
                $this->view->attPeriod = $Period ? $Period->periods : '';
                $this->view->attValues = AttendanceSelectbox::find("attendance_for ='student'");
//                print_r($this->view->attPeriod);exit;
            }
//            else {
//                $master_id = SubDivisionSubjects::findFirstById($master_id);
//                $this->view->students = StudentInfo::find("Division_Class = $master_id->div_val_id "
//                                . "and Subdivision LIKE  '%$master_id->concat_subdivs%' ORDER BY Student_Name");
//                $this->view->noofperiods = AttendanceFrequency::findFirst("frequency_for = 'student'"
//                                . " and div_val_id = " . $master_id->div_val_id
//                                . " and academic_year_id = " . $current_acd_yr)->frequency;
//
//                $this->view->attdays = AttendanceDays::findFirst("date = '" . $this->view->cdate . "'"
////                        "date >= '" . $this->view->scdate . "'"
////                                . " and date <='" . $this->view->ecdate . "'"
//                                . " and user_type='student'"
//                                . " and master_id = $master_id->id"
//                                . " and permission_type = 0"
//                                . " and academic_year_id = $current_acd_yr ");
//                $Period = $this->view->attdays ? AttendancePeriods::findFirst(array(
//                            "att_days_id =" . $this->view->attdays->id,
//                            "columns" => "group_concat(period_value) as periods"
//                        )) : '';
//                $this->view->attPeriod = $Period ? $Period->periods : '';
//                $this->view->attValues = AttendanceSelectbox::find("attendance_for ='student'");
//            }
        }
    }

## reports

    public function loadMonthlyReportAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {

            $master_id = $this->request->getPost('masterid');
            $assignedclass = GroupClassTeachers::findFirstById($master_id);
            $classroommas = ClassroomMaster::findFirstById($assignedclass->classroom_master_id);
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//            $res = ControllerBase::buildStudentQuery($classroommas->aggregated_nodes_id);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status'
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                    . '  and (' . implode(' or ', $res)
                    . ') ORDER BY stuinfo.Student_Name ASC';
            $classStudents = $this->modelsManager->executeQuery($stuquery);
            $monthlyPercent = array();
            if (count($classStudents) > 0):
                foreach ($classStudents as $classStudent) {
                    $overallPercent = array();
                    $params['user_id'] = $classStudent->student_info_id;
                    $params['user_type'] = 'student';
//                    $params['master_id'] = $master_id;
//                    $params['academic_year_id'] = Controllerbase::get_current_academic_year();
                    $overallPercent = AttendanceController::getAttendancePercentMonthly($params);
                    $monthlyPercent[] = $overallPercent;
                }
            endif;
            $this->view->monthlyPer = $monthlyPercent;
        }
    }

    public function getAttendancePercentMonthly($params) {
        //academic_year_id	master_id	permission_type	date	day_type	user_type
        $user_id = $params['user_id'];
        $user_type = $params['user_type']; //staff or student
//        $master_id = $params['master_id']; //combination id (classCombination for student and deptId fro staff)
//        $academic_year_id = $params['academic_year_id'];
        $params['type'] = 'attendance';

//        $combinations = array($master_id);
        $per = array();
        $userInfo = ($user_type == 'student') ? StudentInfo::findFirstById($user_id) : StaffInfo::findFirstById($user_id);

        $getPermissionType = Permissions::findFirstByPermissionFor($params['type']);


        $phsql = "SELECT DISTINCT MONTH( FROM_UNIXTIME(  date ,  '%Y-%m-%d' ) ) as month ,
            YEAR( FROM_UNIXTIME(  date ,  '%Y-%m-%d' ) )  as year
                    FROM attendance_days
                    WHERE  user_type = '$user_type'  AND day_type = '2'
                    ORDER BY  date ASC ";
//        echo $phsql;
        $attendaceMonths = $this->db->fetchAll($phsql, Phalcon\Db::FETCH_ASSOC);
//        echo '<pre>';
        foreach ($attendaceMonths as $monthYear) {
            $stu_attendance = $class_attendance = 0;
            $days_in_month = cal_days_in_month(CAL_GREGORIAN, $monthYear['month'], $monthYear['year']);
            $start_date = date('01-m-Y', mktime(0, 0, 0, $monthYear['month'], 1, $monthYear['year']));
            $end_date = date('t-m-Y', mktime(0, 0, 0, $monthYear['month'], 1, $monthYear['year']));
            $startdt = (($userInfo->Date_of_Joining > strtotime($start_date) ) && ($userInfo->Date_of_Joining < strtotime($end_date))) ? $userInfo->Date_of_Joining : strtotime($start_date);

//            echo $days_in_month . ' - s' . $start_date . ' - e' . $end_date . ' - st' . date('Y-m-d',$startdt) . ' <br>';
            $enddt = strtotime($end_date);
            $attdaysQuery = '';
            $attendance_days = AttendanceDays::find("user_type LIKE  '$user_type' "
                            . " AND date >= '" . ($startdt) . "'"
                            . " AND date <= '" . ($enddt) . "'"
                            . $attdaysQuery);
//(count($combinations) > 0) ? ' AND master_id IN (' . implode(',', $combinations) . ')' :
            $attWorknQuery = '';
            $attendance_workingdays = AttendanceDays::find("user_type LIKE  '$user_type' "
                            . " AND date >= '" . ($startdt) . "'"
                            . " AND date <= '" . ($enddt) . "'"
                            . " AND day_type = '2' " . $attWorknQuery);


            $stu_att_totByVal = 0;

            $work_day = 0;
            foreach ($attendance_workingdays as $dayAtt) {
//                echo  date('Y-m-d',$dayAtt->date) . ' <br>';
                $stu_att_totByVal = 0;
                $periodAtt = AttendancePeriods::find('att_days_id = ' . $dayAtt->id);
                $countPeriods = count($periodAtt);
                $work_day = $countPeriods > 0 ? ($work_day + 1) : $work_day;
                foreach ($periodAtt as $eachPeriodAtt) {
                    $present = AttendanceSelectbox::findFirst(" attendance_for = '$user_type' ORDER BY default DESC");
                    $stu_att_values = AttendanceValues::findFirst("user_id= '" . $user_id . "'"
                                    . " AND att_period_id= '" . $eachPeriodAtt->id . "'"
                                    . " AND att_days_id= '" . $dayAtt->id . "'");

                    $student_att_props_val = AttendanceSelectbox::findFirst("attendance_for = '$user_type'"
                                    . ' and id= ' . $stu_att_values->value_id ? $stu_att_values->value_id : $present->id);
                    $stu_att_totByVal+=$student_att_props_val->attendancevalue;
                }
//                echo $countPeriods. '-';
                $stu_attendance += $stu_att_totByVal / ($countPeriods);
            }

            $percent = 0;
//            echo '<br>' . 'hai' . $stu_attendance . '-' . '-' . $work_day . '<br>';
            if (count($attendance_days) > 0) {
                if ($work_day != 0)
                    $percent = round($stu_attendance / $work_day * 100, 2);
            }
//                $per[$monthYear['year']][$monthYear['month']]

            $header[] = array(
                "text" => date('M - Y', $startdt),
                "month" => date('m', $startdt),
                "year" => date('Y', $startdt)
            );

            $per[] = array(
                'monthname' => date('M - Y', $startdt),
                'numofdays' => $days_in_month,
                'workingdays' => $work_day
            );
            if (strtotime(date('d-m-Y', $userInfo->Date_of_Joining)) <= strtotime(date('d-m-Y', $enddt))) {
                $per[count($per) - 1]['percent'] = $percent;
            } else {
                $per[count($per) - 1]['percent'] = 'NA';
            }
        }
        return array($header, $per);
//        exit
    }

    public function loadDailyReportAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $master_id = $this->request->getPost('masterid');
            $assignedclass = GroupClassTeachers::findFirstById($master_id);
            $classroommas = ClassroomMaster::findFirstById($assignedclass->classroom_master_id);
            $res = ControllerBase::buildStudentQuery($classroommas->aggregated_nodes_id);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status'
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                    . '  and (' . implode(' or ', $res)
                    . ') ORDER BY stuinfo.Student_Name ASC';
            $classStudents = $this->modelsManager->executeQuery($stuquery);
            $dailyPercent = array();
            if (count($classStudents) > 0):
                foreach ($classStudents as $classStudent) {
                    $overallPercent = array();
                    $params['user_id'] = $classStudent->student_info_id;
                    $params['user_type'] = 'student';
                    $params['month'] = $this->request->getPost('month');
                    $params['year'] = $this->request->getPost('year');
                    $monthPercent = AttendanceController::getAttendancePercentDaily($params);
                    $dailyPercent[] = $monthPercent;
                }
            endif;
            $this->view->daillyPer = $dailyPercent;
        }
    }

    public function getAttendancePercentDaily($params) {

        //academic_year_id	master_id	permission_type	date	day_type	user_type
        $user_id = $params['user_id']; //'82'; //
        $user_type = $params['user_type']; //staff or student 'student'; //
//        $master_id = $params['master_id']; //combination id (classCombination for student and deptId fro staff)'1'; //
//        $academic_year_id = $params['academic_year_id']; //'1'; //
        $params['type'] = 'attendance';
//        $params['month'] = '2';
//        $params['year'] = '2015';
        $selected_date = date('Y-m-01', mktime(0, 0, 0, $params['month'], 1, $params['year']));
//        echo $selected_date;exit;
//        $combinations = array($master_id);
        $per = array();
        $userInfo = ($user_type == 'student') ? StudentInfo::findFirstById($user_id) : StaffInfo::findFirstById($user_id);
        $getPermissionType = Permissions::findFirstByPermissionFor($params['type']);


        $phsql = "SELECT DISTINCT DAY( FROM_UNIXTIME( date , '%Y-%m-%d' ) ) as day , MONTH( FROM_UNIXTIME(  date ,  '%Y-%m-%d' ) ) as month ,
            YEAR( FROM_UNIXTIME(  date ,  '%Y-%m-%d' ) )  as year
                    FROM attendance_days
                    WHERE  user_type = '$user_type' 
                    and   day_type = '2'
                    and  MONTH( FROM_UNIXTIME( date , '%Y-%m-%d' ) ) = MONTH('$selected_date')
                    and  YEAR( FROM_UNIXTIME( date , '%Y-%m-%d' ) ) = YEAR('$selected_date')                    
                    ORDER BY  date ASC ";
//        echo $phsql;exit;
        $attendaceMonths = $this->db->fetchAll($phsql, Phalcon\Db::FETCH_ASSOC);
//        print_r(count($attendaceMonths));exit;
        //  echo '<pre>';
        foreach ($attendaceMonths as $monthYear) {
            $stu_attendance = $class_attendance = 0;
            $days_in_month = cal_days_in_month(CAL_GREGORIAN, $monthYear['month'], $monthYear['year']);
            $start_date = date('d-m-Y H:i:s', mktime(0, 0, 0, $monthYear['month'], $monthYear['day'], $monthYear['year']));
            $end_date = date('d-m-Y H:i:s', mktime(23, 59, 59, $monthYear['month'], $monthYear['day'], $monthYear['year']));
            $startdt = (($userInfo->Date_of_Joining > strtotime($start_date) ) && ($userInfo->Date_of_Joining < strtotime($end_date))) ? $userInfo->Date_of_Joining : strtotime($start_date);

            //echo $days_in_month . ' - s' . $start_date . ' - e' . $end_date . ' - st' . date('Y-m-d', $startdt) . ' <br>';
            $enddt = strtotime($end_date);
            //(count($combinations) > 0) ? ' AND master_id IN (' . implode(',', $combinations) . ')' : 
            $attdaysQuery = '';
            $attendance_days = AttendanceDays::find("user_type LIKE  '$user_type' "
                            . " AND date >= '" . ($startdt) . "'"
                            . " AND date <= '" . ($enddt) . "'"
                            . $attdaysQuery);
            //(count($combinations) > 0) ? ' AND master_id IN (' . implode(',', $combinations) . ')' : 
            $attWorknQuery = '';
            $attendance_workingdays = AttendanceDays::find("user_type LIKE  '$user_type' "
                            . " AND date >= '" . ($startdt) . "'"
                            . " AND date <= '" . ($enddt) . "'"
                            . " AND day_type = '2' " . $attWorknQuery);


            $stu_att_totByVal = 0;

            $work_day = 0;
            //echo count($attendance_workingdays) . ' <br>';
            foreach ($attendance_workingdays as $dayAtt) {
//                echo  $dayAtt->id. ' <br>';
                // echo date('Y-m-d', $dayAtt->date) . ' <br>';
                $stu_att_totByVal = 0;
                $periodAtt = AttendancePeriods::find('att_days_id = ' . $dayAtt->id);
                $countPeriods = count($periodAtt);
                $work_day = $countPeriods > 0 ? ($work_day + 1) : $work_day;
                foreach ($periodAtt as $eachPeriodAtt) {
                    $present = AttendanceSelectbox::findFirst(" attendance_for = '$user_type' ORDER BY default DESC");

//                echo "user_id= '" . $user_id . "'"
//                                    . " AND att_period_id= '" . $eachPeriodAtt->id . "'"
//                                    . " AND att_days_id= '" . $dayAtt->id . "'";
                    $stu_att_values = AttendanceValues::findFirst("user_id= '" . $user_id . "'"
                                    . " AND att_period_id= '" . $eachPeriodAtt->id . "'"
                                    . " AND att_days_id= '" . $dayAtt->id . "'");

                    $student_att_props_val = AttendanceSelectbox::findFirst("attendance_for = '$user_type'"
                                    . ' and id= ' . $stu_att_values->value_id ? $stu_att_values->value_id : $present->id);
                    $stu_att_totByVal+=$student_att_props_val->attendancevalue;
                }
//                echo $countPeriods. '-'.$stu_att_totByVal.'<br>';
                $stu_attendance += $stu_att_totByVal / ($countPeriods);
            }

            $percent = 0;
//            echo '<br>' . 'hai' . $stu_attendance . '-' . '-' . $work_day . '<br>';exit;
            if (count($attendance_days) > 0) {
                if ($work_day != 0)
                    $percent = round($stu_attendance / $work_day * 100, 2);
            }
//                $per[$monthYear['year']][$monthYear['month']]
            $header[]['text'] = date('d/m/Y', $startdt);
            $per[] = array(
                'monthname' => date('d/m/Y', $startdt),
                'numofdays' => $days_in_month,
                'workingdays' => $work_day
            );


            if (strtotime(date('d-m-Y', $userInfo->Date_of_Joining)) <= strtotime(date('d-m-Y', $enddt))) {
                $per[count($per) - 1]['percent'] = $percent;
            } else {
                $per[count($per) - 1]['percent'] = 'NA';
            }
//            echo  date('d M, Y', $startdt).'='.$stu_attendance.'<br>';
//        print_r($percent);
//        echo '<br>';
        }
//        print_r (array($header,$per));exit;
        return array($header, $per);
//        
    }

    public function indexAction() {
// no display
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Attendance | ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs("js/attendance/attendance.js");
//        $this->assets->addJs("js/attendancesettings/settings.js");
//        $this->assets->addJs("js/attendancesettings/staffsettings.js");
//        $this->assets->addJs('js/appscripts/approval.js');
//        $this->assets->addJs("js/attendance/attendance.js");
    }

    public function studentsAction() {
        $this->tag->prependTitle("Attendance | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs("js/attendance/attendance.js");
        $this->assets->addJs("js/search/search.js");
        $this->view->studentGroupSearchHtml = $this->view->getRender('search', 'studentGroupSearch', array('callBackFn' => 'attendanceSettings.loadStudentAttendance'));
    }

    public function studentListFormatAction() {

// Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $json = $this->request->getPost('stulistJson');
            $this->view->cdate = $this->request->getPost('currentDate');
            $stuarr = json_decode($json);
            $this->view->stuarr = $stuarr;

//Get the division and subdivision from the stuarr.
            if (count($stuarr) > 0) {
                $divvalid = $stuarr[0]->Division_Class;
                $subdivvalid = $stuarr[0]->Subdivision_section;
                $current_acd_yr = Controllerbase::get_current_academic_year();

//Get the info whether its working day or not for the given division and sub-division
                $this->view->attdays = AttendanceDays::findFirst("date = '" . $this->view->cdate . "' and user_type='student'" .
                                ' and div_val_id = ' . $divvalid . ' and sub_div_val_id = ' . $subdivvalid . 'and academic_year_id = ' . $current_acd_yr);
                $this->view->divvalid = $divvalid;
                $this->view->subdivvalid = $subdivvalid;
                $this->view->current_acd_yr = $current_acd_yr;
            }
        }
    }

    public function staffAction() {
        $this->tag->prependTitle("Attendance | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs("js/attendance/attendance.js");
        $this->view->staffGroupSearchHtml = $this->view->getRender('search', 'staffGroupSearch', array('callBackFn' => 'attendanceSettings.loadStaffAttendance'));
    }

    public function staffListFormatAction() {

// Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->cdate = $this->request->getPost('currentDate');
            $json = $this->request->getPost('stfListJson');
            $stuarr = json_decode($json);
            $this->view->stuarr = $stuarr;


//Get the division and subdivision from the stuarr.
            if (count($stuarr) > 0) {
                $divvalid = $stuarr[0]->Department;
                $subdivvalid = 0; //TODO: No subdivision for staff, making it 0
                $current_acd_yr = Controllerbase::get_current_academic_year();

//Get the info whether its working day or not for the given division
                $this->view->attdays = AttendanceDays::findFirst("date = '" . $this->view->cdate . "' and user_type='staff'" .
                                ' and div_val_id = ' . $divvalid . ' and sub_div_val_id = ' . $subdivvalid . 'and academic_year_id = ' . $current_acd_yr);
                $this->view->divvalid = $divvalid;
                $this->view->subdivvalid = $subdivvalid;
                $this->view->current_acd_yr = $current_acd_yr;
            }
        }
    }

    public function studentAttendanceOldAction() {
        $this->tag->prependTitle("Attendance | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs("js/attendance/attendance.js");
        $param['type'] = 'attendance';
        $getCombination = ControllerBase::loadCombinations($param);
        $this->view->combinations = $getCombination[1];
        $this->view->type = $getCombination[2];
        $this->view->errorMessage = $getCombination[0];
    }

    public function _toRoman($num) {
        $conv = array(10 => array('X', 'C', 'M'),
            5 => array('V', 'L', 'D'),
            1 => array('I', 'X', 'C'));
        $roman = '';

        if ($num < 0) {
            return '';
        }

        $num = (int) $num;

        $digit = (int) ($num / 1000);
        $num -= $digit * 1000;
        while ($digit > 0) {
            $roman .= 'M';
            $digit--;
        }

        for ($i = 2; $i >= 0; $i--) {
            $power = pow(10, $i);
            $digit = (int) ($num / $power);
            $num -= $digit * $power;

            if (($digit == 9) || ($digit == 4)) {
                $roman .= $conv[1][$i] . $conv[$digit + 1][$i];
            } else {
                if ($digit >= 5) {
                    $roman .= $conv[5][$i];
                    $digit -= 5;
                }

                while ($digit > 0) {
                    $roman .= $conv[1][$i];
                    $digit--;
                }
            }
        }

        return $roman;
    }

    public function makeAttendanceAction() {
// Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {

            $levId = $this->request->getPost('itemID');
            $levDet = LeaveRequest::findFirstById($levId);

            $stuid = $levDet->staffId;
            $attVal = $levDet->leaveType;
            $UType = 'staff';
            $sdates = $levDet->fromDate;
            $edates = $levDet->toDate;
// echo $sdates.$edates;
            $currDate = $sdates;

//                echo $currDate;
            while ($currDate <= $edates) {
                try {
                    if ($attVal != 3) {
                        $att_val = AttendanceValues::findFirst("date = '" . $currDate
                                        . "' and user_type='" . $UType . "' and user_id=" . $stuid) ?
                                AttendanceValues::findFirst("date = '" . $currDate
                                        . "' and user_type='" . $UType . "' and user_id=" . $stuid) :
                                new AttendanceValues();
                        $att_val->date = $currDate;
                        $att_val->user_type = $UType;
                        $att_val->user_id = $stuid;
                        $att_val->value_id = $attVal;

                        if (!$att_val->save()) {
                            foreach ($att_val->getMessages() as $message) {
                                $error .= $message;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        $att_val_del = AttendanceValues::findFirst("date = '" . $currDate
                                        . "' and user_type='" . $UType . "' and user_id=" . $stuid);
                        if (!$att_val_del->delete()) {

                            foreach ($att_val_del->getMessages() as $message) {
                                $error .= $message;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                } catch (Exception $ex) {
                    foreach ($ex->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }

//                echo date('d-m-Y',$currDate).'<br>';
                $currDate = strtotime('+1 days', $currDate);
//                echo $currDate;
//                echo date('d-m-Y',$currDate).'<br>';
//            exit;
            }

            $message['type'] = 'success';
            $message['message'] = '<div class="alert alert-block alert-success fade in">' . $this->request->getPost('status') . ' Successfully</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function loadMonthlyHdrAction() {
//        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function classroomAttForStuAction() {
// Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Attendance | ");
        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs("js/attendance/stuattendance.js");
        if ($this->request->isPost()) {
            $master_id = $this->request->getPost('masterid');
            $this->view->master_id = $this->request->getPost('masterid');
            $teacherType = $this->request->getPost('type');

            $this->view->cdate = $start = strtotime($this->request->getPost('currentDate') . ' 00:00:00');
            $this->view->scdate = strtotime($this->request->getPost('currentDate') . ' 00:00:00');
            $this->view->ecdate = strtotime($this->request->getPost('currentDate') . ' 23:59:59');

            if ($teacherType == 'classTeacher') {
                $assignedclass = GroupClassTeachers::findFirstById($master_id);
                $classmaster = ClassroomMaster::findFirstById($assignedclass->classroom_master_id);
                $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//                $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
                $periodres = ControllerBase::buildExamQuery($classmaster->aggregated_nodes_id);

//                print_r(" (" . implode(' or ', $periodres) . ") and user_type = 'student'");
//                exit;
                $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,stuinfo.Gender, '
                        . 'stumap.subordinate_key,stumap.status ,stuinfo.loginid,stuinfo.Date_of_Joining,stuinfo.Student_Name,'
                        . ' stuinfo.rollno,stuinfo.photo'
                        . ' FROM StudentMapping stumap LEFT JOIN'
                        . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                        . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                        . ' and (' . implode(' or ', $res)
                        . ' ORDER BY stuinfo.Student_Name ASC';

                $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);

                if (count($students) > 0) {
                    foreach ($students as $filteredst) {
                        $user_login[] = "'" . $filteredst->loginid . "'";
                    }
                }
                $obj = new Cassandra();
                $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);
                $attvalueforthePeriod = array();
                if ($res) {
                    $buildquery = "select * from day_attendance where"
                            . " user_id IN (" . implode(',', $user_login) . ")"
                            . " and date = $start ";
                    if ($result = $obj->query($buildquery)) {
                        for ($i = 0; $i < count($result); $i++):
                            $attvalueforthePeriod[$result[$i]['user_id']][$result[$i]['period']] = $result[$i]['value'];
                        endfor;
                    }
                }
                $obj->close();
                $this->view->attvaluestu = $attvalueforthePeriod;
                $this->view->noofperiods = PeriodMaster::find(" (" . implode(' or ', $periodres) . ") and user_type = 'student'");
                $this->view->attValues = AttendanceSelectbox::find("attendance_for ='student'");
            }
        }
    }

    public function addOrUpdateAttnValAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $identity = $this->auth->getIdentity();
            $attby = $identity['name'];
            $message = array();
            try {
                $master_id = 0;
                $UType = $this->request->getPost('UType');
                $teacherType = $this->request->getPost('type');
                $periods = $this->request->getPost('periods');
//                $periodmas = PeriodMaster::findFirstById($periods);
                $currDate = strtotime($this->request->getPost('currentDate') . ' 00:00:00');
                $userId = $this->request->getPost('userId');
                $attVal = $this->request->getPost('attVal');
                $teacherTypeID = ($teacherType == 'classTeacher') ? 1 : 0;
                $monthyear = date('m-Y', $currDate);
                $user = ($UType == 'staff') ? StaffInfo::findFirstById($userId) : StudentInfo::findFirstById($userId);
                $attenValueExceeds = AttendanceSelectbox::findFirstById($attVal);
                $obj = new Cassandra();
                $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);
                if ($res) {
                    if ($attenValueExceeds->is_allowed_by_count == 1 && $UType == 'staff') {
                        $buildquery = "select counter_value from month_attendance where"
                                . " user_id = '" . $user->loginid . "'"
                                . " and month ='$monthyear' "
                                . " and value ='$attVal'";
                        if ($result = $obj->query($buildquery)) {
                            if ($result[0]['counter_value'] >= $attenValueExceeds->allowed_count):
                                $message['type'] = 'error';
                                $message['message'] = 'Attendance exceeds the maximum allowed limit';
                                print_r(json_encode($message));
                                exit;
                            endif;
                        }
                    }
                    $update = 0;
                    $buildquerycheck = "select value from day_attendance where"
                            . " user_id = '" . $user->loginid . "'"
                            . " and date = $currDate"
                            . " and period='$periods'";
                    if ($result2 = $obj->query($buildquerycheck)) {
                        $update = 1;
                        $buildParticipantquery = " UPDATE month_attendance
                                                        SET counter_value = counter_value - 1
                                                        WHERE user_id='$user->loginid' AND month='$monthyear' AND value='" . $result2[0]['value'] . "';";
//                            echo $buildParticipantquery;exit;
                        if ($result2 = $obj->query($buildParticipantquery)) {
                            if ($result2[0]['result'] != 'success') {
                                $message['type'] = 'error';
                                $message['message'] = 'Problem in marking attendance';
                                print_r(json_encode($message));
                                exit;
                            }
                        }
                    }
                    $buildInsertquery = " INSERT INTO day_attendance (user_id,period,date,value,attendance_by) 
                                        values ('$user->loginid','$periods',$currDate,'$attVal','$attby');";
//                    print_r($buildInsertquery);exit;
                    if ($result1 = $obj->query($buildInsertquery)) {
                        if ($result1[0]['result'] == 'success') {
//                            if ($update == 0):
                            $buildParticipantquery = " UPDATE month_attendance
                                                        SET counter_value = counter_value + 1
                                                        WHERE user_id='$user->loginid' AND month='$monthyear' AND value='$attVal';";
//                            echo $buildParticipantquery;exit;
                            if ($result2 = $obj->query($buildParticipantquery)) {
                                if ($result2[0]['result'] != 'success') {
                                    $message['type'] = 'error';
                                    $message['message'] = 'Problem in marking attendance';
                                    print_r(json_encode($message));
                                    exit;
                                }
                            }
//                            endif;
                        } else {
                            $message['type'] = 'error';
                            $message['message'] = 'Problem in marking attendance';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                }
                $obj->close();

                $params['value_id'] = $attVal;
                $params['att_period_id'] = $periods;
                $params['att_date'] = $currDate;
                $params['att_act_id'] = $periods . '_' . $currDate;
                $params['user_id'][] = '"' . $user->loginid . '"';
                $objpram = (object) $params;
//                print_r($objpram);exit;
                $response = $this->notificationTrigger->attendanceNotification($objpram);
                print_r($response);
                exit;
//                $message['type'] = 'success';
//                $message['message'] = 'Student attendance has been saved!';
//                print_r(json_encode($message));
//                exit;
            } catch (Exception $ex) {
                $error = '';
                foreach ($ex->getMessages() as $messages) {
                    $error .= $messages . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function classroomAttForStfAction() {
// Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Attendance | ");
        $this->assets->addCss('css/edustyle.css');

        if ($this->request->isPost()) {
            $UType = $this->request->getPost('UType');
            $this->view->cdate = $start = strtotime($this->request->getPost('currentDate'));
            $master_id = $this->request->getPost('masterid');
            $this->view->staff = $filteredstf = StaffInfo::find('status = "Appointed" and find_in_set(' . $master_id . ',aggregate_key)');

            if (count($filteredstf) > 0) {
                foreach ($filteredstf as $filteredst) {
                    $user_login[] = "'" . $filteredst->loginid . "'";
                }
            }
            $attvalueforthePeriod = array();
            $obj = new Cassandra();
            $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);

            if ($res) {
                $buildquery = "select * from day_attendance where"
                        . " user_id IN (" . implode(',', $user_login) . ")"
                        . " and date = $start ";
                if ($result = $obj->query($buildquery)) {
                    for ($i = 0; $i < count($result); $i++):
                        $attvalueforthePeriod[$result[$i]['user_id']][$result[$i]['period']] = $result[$i]['value'];
                    endfor;
                }
            }
            $obj->close();
            $this->view->attvaluestaff = $attvalueforthePeriod;
            $this->view->noofperiods = PeriodMaster::find("(find_in_set('$master_id',  REPLACE(node_id ,  '-', ',' ))>0) and user_type = '$UType'");
            $this->view->attValues = AttendanceSelectbox::find("attendance_for ='staff'");
        }
    }

    public function addOrUpdateAttnValFullAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $identity = $this->auth->getIdentity();
            $attby = $identity['name'];
            $message = array();
//            print_r($this->request->getPost());exit;
            try {
                $master_id = 0;
                $currDate = strtotime($this->request->getPost('currentDate') . ' 00:00:00');
                $UType = $this->request->getPost('UType');
                $teacherType = $this->request->getPost('type');
                $periods = $this->request->getPost('periods');
                $attVal = $this->request->getPost('attVal');
                $teacherTypeID = ($teacherType == 'classTeacher') ? 1 : 0;
                $userIds = $this->request->getPost('userIds');
                $monthyear = date('m-Y', $currDate);
                $params = array();
                foreach ($userIds as $userId) {
                    $user = ($UType == 'staff') ? StaffInfo::findFirstById($userId) : StudentInfo::findFirstById($userId);
                    $attenValueExceeds = AttendanceSelectbox::findFirstById($attVal);

                    $obj = new Cassandra();
                    $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);
                    if ($res) {

                        if ($attenValueExceeds->is_allowed_by_count == 1 && $UType == 'staff') {
                            $buildquery = "select counter_value from month_attendance where"
                                    . " user_id = '" . $user->loginid . "'"
                                    . " and month ='$monthyear' "
                                    . " and value ='$attVal'";
                            if ($result = $obj->query($buildquery)) {
                                if ($result[0]['counter_value'] >= $attenValueExceeds->allowed_count):
                                    $message['type'] = 'error';
                                    $message['message'] = 'Attendance exceeds the maximum allowed limit';
                                    print_r(json_encode($message));
                                    exit;
                                endif;
                            }
                        }
                        $update = 0;
                        $buildquerycheck = "select value from day_attendance where"
                                . " user_id = '" . $user->loginid . "'"
                                . " and date = $currDate"
                                . " and period='$periods'";
                        if ($result2 = $obj->query($buildquerycheck)) {
                            $update = 1;
                            $buildParticipantquery = " UPDATE month_attendance
                                                        SET counter_value = counter_value - 1
                                                        WHERE user_id='$user->loginid' AND month='$monthyear' AND value='" . $result2[0]['value'] . "';";
                            //echo $buildParticipantquery;exit;
                            if ($result2 = $obj->query($buildParticipantquery)) {
                                if ($result2[0]['result'] != 'success') {
                                    $message['type'] = 'error';
                                    $message['message'] = 'Problem in marking attendance';
                                    print_r(json_encode($message));
                                    exit;
                                }
                            }
                        }
                        $buildInsertquery = " INSERT INTO day_attendance (user_id,period,date,value,attendance_by) 
                                        values ('$user->loginid','$periods',$currDate,'$attVal','$attby');";
//                    print_r($buildInsertquery);exit;
                        if ($result1 = $obj->query($buildInsertquery)) {
                            if ($result1[0]['result'] == 'success') {
//                                if ($update == 0):
                                $buildParticipantquery = " UPDATE month_attendance
                                                        SET counter_value = counter_value + 1
                                                        WHERE user_id='$user->loginid' AND month='$monthyear' AND value='$attVal';";
                                //echo $buildParticipantquery;exit;
                                if ($result2 = $obj->query($buildParticipantquery)) {
                                    if ($result2[0]['result'] != 'success') {
                                        $message['type'] = 'error';
                                        $message['message'] = 'Problem in marking attendance';
                                        print_r(json_encode($message));
                                        exit;
                                    }
                                }
//                                endif;
                            } else {
                                $message['type'] = 'error';
                                $message['message'] = 'Problem in marking attendance';
                                print_r(json_encode($message));
                                exit;
                            }
                        }
                    } else {
                        $message['type'] = 'Problem in connecting cassandra';
                        $message['message'] = $error;
                        print_r(json_encode($message));
                        exit;
                    }
                    $obj->close();
                    $params['value_id'] = $attVal;
                    $params['att_period_id'] = $periods;
                    $params['att_date'] = $currDate;
                    $params['att_act_id'] = $periods . '_' . $currDate;
                    $params['user_id'][] = '"' . $user->loginid . '"';
                }
                $objpram = (object) $params;
                $response = $this->notificationTrigger->attendanceNotification($objpram);
                print_r($response);
                exit;
//                $message['type'] = 'success';
//                $message['message'] = 'Student attendance has been saved!';
//                print_r(json_encode($message));
//                exit;
            } catch (Exception $ex) {
                $error = '';
                foreach ($ex->getMessages() as $messages) {
                    $error .= $messages . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function loadStaffAttReportAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function loadSubtreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost() && $this->request->getPost('orgvalueid') != '') {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
            $this->view->org_value = $org_value = OrganizationalStructureValues::find('parent_id = ' . $orgvalueid
                            . ' GROUP BY  org_master_id ');
        }
    }

    public function viewStaffAttendanceAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $params = $queryParams = array();
            foreach ($this->request->getPost() as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'aggregate' && $value) {
                    $params['aggregateids'][] = $value;
                } else {
                    $params[$key] = $value;
                }
            }
            $classroomname = ControllerBase::getNameForKeys(implode(',', $params['aggregateids']));
            $this->view->aggregateids = $gvnparams['aggregateids'] = implode(',', $params['aggregateids']);
            $this->view->reptyp = $gvnparams['reptyp'] = $params['reptyp'];
            $this->view->fromdate = $gvnparams['fromdate'] = $params['fdate'];
            $this->view->todate = $gvnparams['todate'] = $params['tdate'];
            $calres = $this->calculateStaffAttPercentMonthly($gvnparams);
            if (count($calres) > 0) {
                $this->view->classStudents = $calres[0];
                $this->view->monthlyPercent = $calres[1];
                $this->view->monthhead = $calres[4];
                $this->view->valhead = $calres[3];
                $this->view->legend = $calres[2];
                $this->view->result = $calres[5];
                $this->view->st = $calres[6];
                $this->view->et = $calres[7];
            } else {

                $this->view->error = '<div class="alert alert-danger">Attendance Not Yet Taken</div>';
            }
        }
    }

    public function calculateStaffAttPercentMonthly($gvnparams) {
        $aggregateids = $gvnparams['aggregateids'];
        $reptyp = $gvnparams['reptyp'];
        $newarr = array();
        $user_type = 'staff';
        $res = ControllerBase::buildStudentQuery($aggregateids);
        $stfquery = 'SELECT stf.aggregate_key,stf.loginid,stf.Staff_Name,stf.Date_of_Joining,'
                . ' stfmas.photo,stfmas.staff_id FROM StaffInfo stf LEFT JOIN'
                . ' StaffGeneralMaster stfmas ON stfmas.staff_id=stf.id WHERE stf.status = "Appointed"'
                . '  and (' . implode(' or ', $res)
                . ') ORDER BY stf.Staff_Name ASC';
        $classStudents = $this->modelsManager->executeQuery($stfquery);
        $monthlyPercent = $result = array();
        if (count($classStudents) > 0):
            $params['monthhead'] = array();
            $params['valhead'] = array();
            foreach ($classStudents as $classStudent) {
                $overallPercent = array();
                $params['user_id'] = $classStudent->loginid;
                $params['staff_id'] = $classStudent->staff_id;
                $params['doj'] = $classStudent->Date_of_Joining;
                $params['user_type'] = 'staff';
                $params['from_date'] = $gvnparams['fromdate'];
                $params['to_date'] = $gvnparams['todate'];
                $overallPercent = $this->getStaffAttendancePercentMonthly($params);
                $monthlyPercent = array_merge($monthlyPercent, $overallPercent[0]);
                $params['monthhead'] = ($overallPercent[1]);
                $params['valhead'] = ($overallPercent[2]);
                $params['legend'] = ($overallPercent[3]);
            }
        endif;
        $i = $j = 0;
        $mntharr = array_unique($params['monthhead']);
        usort($mntharr, 'ReportsController::month_compare');
//            print_r($mntharr);exit;
        if (count($mntharr) > 0) {
            $firstele = (count($mntharr) > 1) ? array_shift($mntharr) : $mntharr[0];
            $lastele = (count($mntharr) > 1) ? array_pop($mntharr) : $mntharr[0];
            $startdt = (date('Y-m-01 00:00:00', strtotime('01-' . $firstele)));
            $enddt = (date('Y-m-t 00:00:00', strtotime('01-' . $lastele)));
            $begin = new DateTime($startdt);
            $end = new DateTime($enddt);
            $interval = DateInterval::createFromDateString('1 month');
            $period = new DatePeriod($begin, $interval, $end);
            foreach ($period as $dt) {
                $monthhead[] = ($dt->format("m-Y"));
            }
            $valhead = array_unique($params['valhead']);
            if ($reptyp == 1) {
                $legend = array();
                $result['thead'][$i][$j]['text'] = 'Staff';
                $result['thead'][$i][$j]['rowspan'] = '';
                $result['thead'][$i][$j]['colspan'] = '';
                $j++;
                $result['thead'][$i][$j]['text'] = 'Department';
                $result['thead'][$i][$j]['rowspan'] = '';
                $result['thead'][$i][$j]['colspan'] = '';
                foreach ($classStudents as $classStudent) {
                    if (count($monthhead) > 0) {
                        foreach ($monthhead as $hvalue) {
                            $atttaken = $stu_att_totByVal = 0;
                            if (count($valhead) > 0) {
                                foreach ($valhead as $vvalue) {
                                    $student_att_props_val = AttendanceSelectbox::findFirst("attendance_for = '$user_type'"
                                                    . ' and attendanceid= "' . $vvalue . '"');
                                    $noofperiods = PeriodMaster::find('LOCATE(node_id,"' . str_replace(',', '-', $classStudent->aggregate_key) . '" )' . " and user_type = 'staff'");
                                    $counttaken = $monthlyPercent[$classStudent->loginid][$hvalue][$vvalue] ? ($monthlyPercent[$classStudent->loginid][$hvalue][$vvalue] / $noofperiods) : 0;
                                    $atttaken += ($counttaken / count($noofperiods));
                                    $stu_att_totByVal+= ($counttaken / count($noofperiods)) * $student_att_props_val->attendancevalue;
                                }
                            }
                            $monthlyPercent[$classStudent->loginid][$hvalue]['percent'] = $atttaken > 0 ? (round($stu_att_totByVal / $atttaken * 100, 2)) : '';
                        }
                    }
                }

                if (count($monthhead) > 0) {
                    foreach ($monthhead as $hvalue) {
                        $j++;
                        $monthhdr = explode('-', $hvalue);
                        $result['thead'][$i][$j]['text'] = '<a href="javascript:void(0)" onclick="attendanceSettings.loadStfDailyAttendance(this)"
                       class="monthExpand" month="' . $monthhdr['0'] . '" year="' . $monthhdr['1'] . '" 
                           aggregateids="' . $aggregateids . '" reptyp="1"  >
                        <span class="btn btn-round btn-info">' . date('M - Y', strtotime('01-' . $hvalue)) . '</span></a>';
                        $result['thead'][$i][$j]['rowspan'] = '';
                        $result['thead'][$i][$j]['colspan'] = '';
                    }

                    $j++;
                    $result['thead'][$i][$j]['text'] = 'Total';
                    $result['thead'][$i][$j]['rowspan'] = '';
                    $result['thead'][$i][$j]['colspan'] = '';
                }
            } else {

                foreach ($classStudents as $classStudent) {
                    $noofperiods = PeriodMaster::find('LOCATE(node_id,"' . str_replace(',', '-', $classStudent->aggregate_key) . '" )' . " and user_type = 'staff'");

                    if (count($monthhead) > 0) {
                        foreach ($monthhead as $hvalue) {
                            if (count($valhead) > 0) {
                                foreach ($valhead as $vvalue) {
                                    $atttaken = $stu_att_totByVal = 0;
                                    $counttaken = $monthlyPercent[$classStudent->loginid][$hvalue][$vvalue] ? ($monthlyPercent[$classStudent->loginid][$hvalue][$vvalue] ) : 0;
                                    $atttaken += ($counttaken / count($noofperiods));
//                                  print_r($atttaken); exit;
                                    $monthlyPercent[$classStudent->loginid][$hvalue][$vvalue] = ($atttaken > 0) ? $atttaken : '';
                                }
                            }
                        }
                    }
                }
                $legend = array_unique($params['legend']);
                $result['thead'][$i][$j]['text'] = 'Staff';
                $result['thead'][$i][$j]['rowspan'] = '2';
                $result['thead'][$i][$j]['colspan'] = '';
                $j++;
                $result['thead'][$i][$j]['text'] = 'Department';
                $result['thead'][$i][$j]['rowspan'] = '2';
                $result['thead'][$i][$j]['colspan'] = '';
                if (count($monthhead) > 0) {
                    foreach ($monthhead as $hvalue) {
                        $j++;
                        $monthhdr = explode('-', $hvalue);
                        $result['thead'][$i][$j]['text'] = '<a href="javascript:void(0)" onclick="attendanceSettings.loadStfDailyAttendance(this)"
                       class="monthExpand" month="' . $monthhdr['0'] . '" year="' . $monthhdr['1'] . '" 
                           aggregateids="' . $aggregateids . '" reptyp="0"  >
                        <span class="btn btn-round btn-info">' . date('M - Y', strtotime('01-' . $hvalue)) . '</span></a>';
                        $result['thead'][$i][$j]['rowspan'] = '';
                        $result['thead'][$i][$j]['colspan'] = count($valhead);
                    }
                    $j++;
                    $result['thead'][$i][$j]['text'] = 'Total';
                    $result['thead'][$i][$j]['rowspan'] = '';
                    $result['thead'][$i][$j]['colspan'] = count($valhead);
                }
                $i++;
                if (count($monthhead) > 0) {
                    foreach ($monthhead as $hvalue) {
                        if (count($valhead) > 0) {
                            foreach ($valhead as $vvalue) {
                                $j++;
                                $result['thead'][$i][$j]['text'] = $vvalue;
                                $result['thead'][$i][$j]['rowspan'] = '';
                                $result['thead'][$i][$j]['colspan'] = '';
                            }
                        }
                    }
                    if (count($valhead) > 0) {
                        foreach ($valhead as $vvalue) {
                            $j++;
                            $result['thead'][$i][$j]['text'] = $vvalue;
                            $result['thead'][$i][$j]['rowspan'] = '';
                            $result['thead'][$i][$j]['colspan'] = '';
                        }
                    }
                }
            }
            $newarr = array($classStudents, $monthlyPercent, $legend, $valhead, $monthhead, $result, $firstele, $lastele, '');
        }
        return $newarr;
    }

    public function loadStfDailyAttendanceAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->masterid = $aggregateids = $gnparam['aggregateids'] = $this->request->getPost('aggregateids');
            $this->view->reptyp = $reptyp = $gnparam['reptyp'] = $this->request->getPost('reptyp');
            $this->view->month = $month = $gnparam['month'] = $this->request->getPost('month');
            $this->view->year = $year = $gnparam['year'] = $this->request->getPost('year');
            $rescal = $this->calculateAttendancePercentDaily($gnparam);
            $this->view->classStudents = $rescal[0];
            $this->view->dailyPercent = $rescal[1];
            $this->view->monthhead = $rescal[4];
            $this->view->valhead = $rescal[3];
            $this->view->legend = $rescal[2];
            $this->view->result = $rescal[5];
        }
    }

    public function calculateAttendancePercentDaily($gnparam) {
        $aggregateids = $gnparam['aggregateids'];
        $reptyp = $gnparam['reptyp'];
        $month = $gnparam['month'];
        $year = $gnparam['year'];
        $user_type = 'staff';
        $newarr = array();
        $res = ControllerBase::buildStudentQuery($aggregateids);
        $stfquery = 'SELECT stf.aggregate_key,stf.loginid,stf.Staff_Name,stf.Date_of_Joining,'
                . ' stfmas.photo,stfmas.staff_id FROM StaffInfo stf LEFT JOIN'
                . ' StaffGeneralMaster stfmas ON stfmas.staff_id=stf.id WHERE stf.status = "Appointed"'
                . '  and (' . implode(' or ', $res)
                . ') ORDER BY stf.Staff_Name ASC';
        $classStudents = $this->modelsManager->executeQuery($stfquery);
        $dailyPercent = $result = array();
        if (count($classStudents) > 0):
            $params['monthhead'] = array();
            $params['valhead'] = array();
            foreach ($classStudents as $classStudent) {
                $overallPercent = array();
                $params['user_id'] = $classStudent->loginid;
                $params['staff_id'] = $classStudent->staff_id;
                $params['doj'] = $classStudent->Date_of_Joining;
                $params['user_type'] = 'staff';
                $params['month'] = $month;
                $params['year'] = $year;
                $overallPercent = ReportsController::getAttendancePercentDaily($params);
                $dailyPercent = array_merge($dailyPercent, $overallPercent[0]);
                $params['valhead'] = ($overallPercent[2]);
                $params['legend'] = ($overallPercent[3]);
            }
        endif;
        $i = $j = 0;

        $startdt = (date('Y-m-01 00:00:00', strtotime('01-' . $month . '-' . $year)));
        $enddt = (date('Y-m-01 00:00:00', strtotime('01-' . $month . '-' . $year . ' +1 month')));
        $begin = new DateTime($startdt);
        $end = new DateTime($enddt);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        foreach ($period as $dt) {
            $monthhead[] = strtotime($dt->format("Y-m-d H:i:s"));
        }
//            $monthhead = array_unique($params['monthhead']);
        $valhead = array_unique($params['valhead']);

        if ($reptyp == 1) {

            foreach ($classStudents as $classStudent) {
                $noofperiods = PeriodMaster::find('LOCATE(node_id,"' . str_replace(',', '-', $classStudent->aggregate_key) . '" )' . " and user_type = 'staff'");

                if (count($monthhead) > 0) {
                    foreach ($monthhead as $hvalue) {
                        $atttaken = $stu_att_totByVal = 0;
                        if (count($valhead) > 0) {
                            foreach ($valhead as $vvalue) {
                                $student_att_props_val = AttendanceSelectbox::findFirst("attendance_for = '$user_type'"
                                                . ' and attendanceid= "' . $vvalue . '"');
                                $counttaken = $dailyPercent[$classStudent->loginid][$hvalue][$vvalue] ? ($dailyPercent[$classStudent->loginid][$hvalue][$vvalue] ) : 0;
                                $atttaken += ($counttaken / count($noofperiods));
                                $stu_att_totByVal+= ($counttaken / count($noofperiods)) * $student_att_props_val->attendancevalue;
                            }
                        }
                        $dailyPercent[$classStudent->loginid][$hvalue]['percent'] = ($atttaken > 0) ? round(($stu_att_totByVal / ($atttaken * $noofperiods)) * 100, 2) : '';
                    }
                }
            }
            $legend = array();
            $result['thead'][$i][$j]['text'] = 'Staff&nbsp;&nbsp;&nbsp;&nbsp;';
            $result['thead'][$i][$j]['rowspan'] = '';
            $result['thead'][$i][$j]['colspan'] = '';
            $j++;
            $result['thead'][$i][$j]['text'] = 'Department&nbsp;&nbsp;&nbsp;&nbsp;';
            $result['thead'][$i][$j]['rowspan'] = '';
            $result['thead'][$i][$j]['colspan'] = '';
            if (count($monthhead) > 0) {
                foreach ($monthhead as $hvalue) {
                    $j++;
                    $result['thead'][$i][$j]['text'] = date('D d', $hvalue);
                    $result['thead'][$i][$j]['rowspan'] = '';
                    $result['thead'][$i][$j]['colspan'] = '';
                }
                $j++;
                $result['thead'][$i][$j]['text'] = 'Total';
                $result['thead'][$i][$j]['rowspan'] = '';
                $result['thead'][$i][$j]['colspan'] = '';
            }
        } else {
            foreach ($classStudents as $classStudent) {
                $noofperiods = PeriodMaster::find('LOCATE(node_id,"' . str_replace(',', '-', $classStudent->aggregate_key) . '" )' . " and user_type = 'staff'");
                if (count($monthhead) > 0) {
                    foreach ($monthhead as $hvalue) {
                        if (count($valhead) > 0) {
                            foreach ($valhead as $vvalue) {
                                $atttaken = $stu_att_totByVal = 0;
                                $counttaken = $dailyPercent[$classStudent->loginid][$hvalue][$vvalue] ? ($dailyPercent[$classStudent->loginid][$hvalue][$vvalue] ) : 0;
                                $atttaken += ($counttaken / count($noofperiods));
                                $dailyPercent[$classStudent->loginid][$hvalue][$vvalue] = ($atttaken > 0) ? $atttaken : '';
                            }
                        }
                    }
                }
            }
            $result['thead'][$i][$j]['text'] = 'Staff&nbsp;&nbsp;&nbsp;&nbsp;';
            $result['thead'][$i][$j]['rowspan'] = '2';
            $result['thead'][$i][$j]['colspan'] = '';
            $j++;
            $result['thead'][$i][$j]['text'] = 'Department&nbsp;&nbsp;&nbsp;&nbsp;';
            $result['thead'][$i][$j]['rowspan'] = '2';
            $result['thead'][$i][$j]['colspan'] = '';
            $legend = array_unique($params['legend']);
            if (count($monthhead) > 0) {
                foreach ($monthhead as $hvalue) {
                    $j++;
                    $result['thead'][$i][$j]['text'] = date('D d', $hvalue);
                    $result['thead'][$i][$j]['rowspan'] = '';
                    $result['thead'][$i][$j]['colspan'] = count($valhead);
                }
                $j++;
                $result['thead'][$i][$j]['text'] = 'Total';
                $result['thead'][$i][$j]['rowspan'] = '';
                $result['thead'][$i][$j]['colspan'] = count($valhead);
            }
            $i++;
            if (count($monthhead) > 0) {
                foreach ($monthhead as $hvalue) {
                    if (count($valhead) > 0) {
                        foreach ($valhead as $vvalue) {
                            $j++;
                            $result['thead'][$i][$j]['text'] = $vvalue;
                            $result['thead'][$i][$j]['rowspan'] = '';
                            $result['thead'][$i][$j]['colspan'] = '';
                        }
                    }
                }
                if (count($valhead) > 0) {
                    foreach ($valhead as $vvalue) {
                        $j++;
                        $result['thead'][$i][$j]['text'] = $vvalue;
                        $result['thead'][$i][$j]['rowspan'] = '';
                        $result['thead'][$i][$j]['colspan'] = '';
                    }
                }
            }
        }
        $newarr = array($classStudents, $dailyPercent, $legend, $valhead, $monthhead, $result);
        return $newarr;
    }

    public function getStaffAttendancePercentMonthly($params) {
        $user_id = $params['user_id'];
        $user_type = $params['user_type']; //staff or student
        $params['type'] = 'attendance';
        $per = $attvalueDays = array();
        $monthhead = $params['monthhead'];
        $valhead = $params['valhead'];
        $legend = $params['legend'];
        $start = new DateTime('01-' . $params['from_date']);
        $end = new DateTime('28-' . $params['to_date']);
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        foreach ($period as $dt) {
            $monthyr[] = $dt->format("m-Y");
        }
        $obj = new Cassandra();
        $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);
        if ($res) {
            if ($monthyr) {
                foreach ($monthyr as $value) {
                    $buildquery = "select * from month_attendance where"
                            . " user_id = '" . $user_id . "' "
                            . " and month = '$value' ";
                    if ($result = $obj->query($buildquery)) {
                        for ($i = 0; $i < count($result); $i++):
                            $abbrevation = AttendanceSelectbox::findFirstById($result[$i]['value']);
                            $monthhead[] = $result[$i]['month'];
                            $valhead[$abbrevation->id] = $abbrevation->attendanceid;
                            $legend[$abbrevation->id] = $abbrevation->attendanceid . ' - ' . $abbrevation->attendancename;
                            $attvalueDays[$user_id][$result[$i]['month']][$abbrevation->attendanceid] = $result[$i]['counter_value'];
                        endfor;
                    }
                }
            }
        }
        $obj->close();
        ksort($legend);
        ksort($valhead);
        return array($attvalueDays, $monthhead, $valhead, $legend);
    }

    public function subjectAttForStuAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Attendance | ");
        $this->assets->addCss('css/edustyle.css');
        if ($this->request->isPost()) {
            $master_id = $this->request->getPost('masterid');
            $dtgiven = $this->request->getPost('currentDate');
            $this->view->master_id = $this->request->getPost('masterid');
            $this->view->cdate = $start = strtotime($dtgiven.' 00:00:00'); // strtotime($this->request->getPost('currentDate') . ' 00:00:00');
            $assignedclass = GroupSubjectsTeachers::findFirstById($master_id);
            $classmaster = ClassroomMaster::findFirstById($assignedclass->classroom_master_id);
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
            $classroomid = $classmaster->id;

//            $participantsforquery = array();
//            $teachers = StaffInfo::find('id IN('.$assignedclass->teachers_id .')');
//            foreach($teachers as $teach){
//                $participantsforquery[] = "'" . $teach->loginid . "'";
//            }
//            if(count($participantsforquery)>0){
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,stuinfo.Gender, '
                    . 'stumap.subordinate_key,stumap.status ,stuinfo.loginid,stuinfo.Date_of_Joining,stuinfo.Student_Name,'
                    . ' stuinfo.rollno,stuinfo.photo'
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
                    . ' ORDER BY stuinfo.Student_Name ASC';
//echo $stuquery;exit;
            $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);

            if (count($students) > 0) {
                foreach ($students as $filteredst) {
                    $user_login[] = "'" . $filteredst->loginid . "'";
                }
            }
            $obj = new Cassandra();
            $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, CASSANDRASERVERPORT);
            $attvalueforthePeriod = array();
//            $buildquery = "select * from user_cal where user_id IN (" . implode(',',$participantsforquery) . ") ";
            $buildquery = "select * from classroom_cal where classroom_id = '" . $classroomid . "' ";
            if ($result = $obj->query($buildquery)) {
//            print_r($result);exit;
                $dateFilteresArr = array();
                for ($i = 0; $i < count($result); $i++) {
                    $endqried = strtotime(date('d-m-Y', $result[$i]["end"]).' 00:00:00');
                    $startqried =  strtotime( date('d-m-Y', $result[$i]["start"]).' 00:00:00');
                    $status = $result[$i]["status"];
                    $eveData = json_decode($result[$i]["event_data"]);
                    $subjectid = $assignedclass->subject_id;
                    if (($start >= $startqried && $start <= $endqried ) 
                            && $status == 'Confirmed' 
                            && $classroomid == $eveData->classroom 
                            && $subjectid == $eveData->subject) {
                        $dateFilteresArr[] = $eveData;
                    }   
                }
            }
 
                if ($res) {
                    $buildquery = "select * from day_attendance where"
                            . " user_id IN (" . implode(',', $user_login) . ")"
                            . " and date = $start ";
                    if ($result = $obj->query($buildquery)) {
                        for ($i = 0; $i < count($result); $i++):
                            $attvalueforthePeriod[$result[$i]['user_id']][$result[$i]['period']] = $result[$i]['value'];
                        endfor;
                    }
                }
                $obj->close();
            $this->view->attvaluestu = $attvalueforthePeriod;
//            print_r($attvalueforthePeriod);exit;
            $this->view->noofperiods = array_reverse($dateFilteresArr);
            $this->view->attValues = AttendanceSelectbox::find("attendance_for ='student'");
//            }
        }
    }

}
