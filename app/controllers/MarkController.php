<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class MarkController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function getStudentMarkListAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->master_id = $master_id = $this->request->getPost('masterid');
            $teacherType = $this->request->getPost('type');
            $this->view->mainExId = ($this->request->getPost('mainExId'));
            $SubDivSubjmaster = GroupSubjectsTeachers::findFirstById($master_id);
            $this->view->subject_id = $SubDivSubjmaster->subject_id;
            $param['type'] = 'mainexam_mark';
            $getCombination = ControllerBase::loadCombinations($param);
            $combinations = ($getCombination[1]);
            $teachSubj = array();
            foreach ($combinations as $key => $combi) {
                $teachSubj[$key] = GroupSubjectsTeachers::findFirst($key)->subject_id;
            }
            $this->view->iseditable = $teachSubj;

            if ($teacherType != 'classTeacher') {
//                $orgvaldet = OrganizationalStructureValues::findFirstById($SubDivSubjmaster->subject_id);
//                $subj_arr = $this->_getMandNodesForExam($orgvaldet);
//                $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", stumap.aggregate_key)>0)', array_keys($subj_arr));
                $classmaster = ClassroomMaster::findFirstById($SubDivSubjmaster->classroom_master_id);
                $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//                $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
                $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                        . 'stumap.subordinate_key,stumap.status'
                        . ' FROM StudentMapping stumap LEFT JOIN'
                        . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE'
                        . ' stumap.status = "Inclass" '
                        . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                        . ' and (' . implode(' or ', $res). ') '
//                        . ' and (' . implode(' and ', $subjQuery) . ')'
                        . '  ORDER BY stuinfo.Admission_no ASC';

                $this->view->students = $this->modelsManager->executeQuery($stuquery);
                $this->view->master_id = $SubDivSubjmaster->id;
                $this->view->node_id = array($classmaster->name);
            }
        }
    }

    public function _getMandNodesForExam($acdyrMas, $nodes = array()) {
        $fields = OrganizationalStructureValues::findFirstById($acdyrMas->parent_id);
        $iscycle = OrganizationalStructureMaster::findFirstById($acdyrMas->org_master_id);

        if (!in_array($iscycle->module, array("structural", "StaffCycleNode", "Ledger"))) {
            if ($iscycle->module != "Subject"):
                $nodes[$acdyrMas->id] = $acdyrMas->name;

            endif;
            if (isset($fields->parent_id)):
                $nodes = $this->_getMandNodesForExam($fields, $nodes);
            endif;
        }

        return $nodes;
    }

    public function find_childtreevaljson($aid, $html = '', $stucnt = array()) {
        $exist = OrganizationalStructureValues::find('parent_id =' . $aid);
        $nodeval = OrganizationalStructureValues::findFirstById($aid);
        $html .= '<td>' . $nodeval->name;
        $stucnt[] = $nodeval->id;
        $c ++;
        if (count($exist) > 0) {
            $html .= ' <table  border="0" class="table general-table" style="margin-bottom: 0 !important;">'
                    . '<thead>';
            $html .= '<tr> ';
            foreach ($exist as $chl) {
                $res = MarkController::find_childtreevaljson($chl->id, $html, $stucnt);
                $html = $res[0];
                $stucnt = $res[1];
            }
            $html .= '</tr>';
            $html .= '</table>';
        } else {
            $html .= '<span style="width:20px; height:20px; line-height:22px; font-size:13px;" class="mini-stat-icon tar"><i class="fa fa-edit lastchild" onclick="markSettings.loadStudentDiv();" ></i></span></td>';
        }
        return array($html, $stucnt);
    }

    public function find_childtreevalinput($rowexms, $studet) {
//       echo '<pre>'; print_r(array_reverse($rowexms));exit;
//        $rowexms = array_reverse($rowexms);
        if (count($rowexms) > 0) {
            foreach ($rowexms as $levl) {
                foreach ($levl as $chl) {

//        $nodeval = OrganizationalStructureValues::findFirstById($chl);
//            echo "student_id = $studet[0] and mainexam_id= $studet[1] and subject_id= $chl";
                    $marks = MainexamMarks::findFirst(array(
                                'columns' => '(marks+inherited_marks) as mark ,(outof+inherited_outof) as outof ',
                                "student_id = $studet[0] and mainexam_id= $studet[1] and subject_id= $chl and grp_subject_teacher_id= $studet[2]"
                    ));
                    $html .= '<td class="viewmark"><span class="label label-info">' . (isset($marks->mark) ? ($marks->mark ) : '') . '</span></td>';
                }
            }
        }
        return $html;
    }

    public function find_childtreevalinputforreport($rowexms, $studet) {

//       echo '<pre>'; print_r(array_reverse($rowexms));exit;
//        $rowexms = array_reverse($rowexms);
        if (count($rowexms) > 0) {
            foreach ($rowexms as $levl) {
                foreach ($levl as $chl) {
//        $nodeval = OrganizationalStructureValues::findFirstById($chl);
//            echo "student_id = $studet[0] and mainexam_id= $studet[1] and subject_id= $chl";
                    $marks = MainexamMarks::findFirst(array(
                                'columns' => '(marks+inherited_marks) as mark ,(outof+inherited_outof) as outof ',
                                "student_id = $studet[0] and mainexam_id= $studet[1] and subject_id= $chl and grp_subject_teacher_id IN($studet[2])"
                    ));
//                    $mname = OrganizationalStructureValues::findFirstById($chl)->name;
                    $html .= '<td class="viewmark"><span class="label label-info">' . (isset($marks->mark) ? ($marks->mark . '/' . $marks->outof ) : '') . '</span></td>';
                }
            }
        }
        return $html;
    }

    public function findSubratlistforreport($rowsubjects, $rowhead, $studet, $lastroe) {
        if (count($rowsubjects) > 0) {
            $column = $ncolumn = array();
            foreach ($rowsubjects as $key => $levl) {
                foreach ($levl as $chl) {
                    $clstest = RatingCategoryMaster::find();
                    if (count($clstest) > 0) {
                        $totweit = 0;
                        foreach ($clstest as $clst) {
                            $column[] = array(
                                'text' => $clst->category_name,
                                'rowspan' => '',
                                'colspan' => '',
                                'testid' => $clst->id,
                                'class' => 'notopborder',
                            );
                            $totweit +=$clst->category_weightage;
                            $ncolumn[] = array(
                                'text' => '<label class="label label-success" >' . $clst->category_weightage . ' pts</label>',
                                'rowspan' => '',
                                'colspan' => '',
                                'class' => 'nobottborder',
                            );
                        }

                        $column[] = array(
                            'text' => 'Total',
                            'rowspan' => '',
                            'colspan' => '',
                            'class' => 'notopborder',
                        );
                        $ncolumn[] = array(
                            'text' => '<label class="label label-success" >' . $totweit . ' pts</label>',
                            'rowspan' => '',
                            'colspan' => '',
                            'class' => 'nobottborder',
                        );
                    } else {
                        $column[] = array(
                            'text' => '',
                            'rowspan' => '',
                            'colspan' => '',
                            'class' => 'notopborder',
                        );
                        $ncolumn[] = array(
                            'text' => '',
                            'rowspan' => '',
                            'colspan' => '',
                            'class' => 'nobottborder',
                        );
                    }
                }
            }

            $rowhead[$lastroe] = $column;
            $rowhead[$lastroe + 1] = $ncolumn;
        }
        return $rowhead;
    }

    public function findAsslistforreport($rowsubjects, $rowhead, $studet, $lastroe) {
        if (count($rowsubjects) > 0) {
            $column = $ncolumn = array();
            foreach ($rowsubjects as $key => $levl) {
                foreach ($levl as $chl) {
                    $clststQury = array();
                    if (count($studet) > 0) {
                        $clststQury[] = " grp_subject_teacher_id IN (" . $studet . ")";
                    }
                    $clststQury[] = "  subjct_modules IN (" . $chl . ")";
                    $clstest = AssignmentsMaster::find(implode(' and ', $clststQury) . ' ORDER BY created_date');
                    if (count($clstest) > 0) {
                        foreach ($clstest as $clst) {
                            $column[] = array(
                                'text' => $clst->topic,
                                'rowspan' => '',
                                'colspan' => '',
                                'testid' => $clst->id,
                                'class' => 'notopborder',
                            );

                            $totmark = AssignmentMarks::findFirst('assignment_id = ' . $clst->id);
                            $ncolumn[] = array(
                                'text' => '<label class="label label-success" >' . $totmark->outof . '</label>',
                                'rowspan' => '',
                                'colspan' => '',
                                'class' => 'nobottborder',
                            );
                        }
                    } else {
                        $column[] = array(
                            'text' => '',
                            'rowspan' => '',
                            'colspan' => '',
                            'class' => 'notopborder',
                        );
                        $ncolumn[] = array(
                            'text' => '',
                            'rowspan' => '',
                            'colspan' => '',
                            'class' => 'nobottborder',
                        );
                    }
                }
            }
            $rowhead[$lastroe] = $column;
            $rowhead[$lastroe + 1] = $ncolumn;
        }
        return $rowhead;
    }

    public function find_evallisttforreport($rowsubjects, $rowhead, $evalheader, $lastroe) {
        if (count($rowsubjects) > 0) {
            $column = $ncolumn = array();
            foreach ($rowsubjects as $key => $levl) {
                foreach ($levl as $chl) {
                    if (isset($evalheader[$chl]) && count($evalheader[$chl]) > 0) {
                        foreach ($evalheader[$chl] as $key => $clst) {
                            $column[] = array(
                                'text' => $key,
                                'rowspan' => '',
                                'colspan' => '',
                                'class' => 'notopborder',
                            );
                            $ncolumn[] = array(
                                'text' => '<label class="label label-success" >' . $clst . '</label>',
                                'rowspan' => '',
                                'colspan' => '',
                                'class' => 'nobottborder',
                            );
                        }
                    } else {
                        $column[] = array(
                            'text' => '',
                            'rowspan' => '',
                            'colspan' => '',
                            'class' => 'notopborder',
                        );
                        $ncolumn[] = array(
                            'text' => '',
                            'rowspan' => '',
                            'colspan' => '',
                            'class' => 'nobottborder',
                        );
                    }
                }
            }
            $rowhead[$lastroe] = $column;
            $rowhead[$lastroe + 1] = $ncolumn;
        }
        return $rowhead;
    }

    public function find_classtestlistforreport($rowsubjects, $rowhead, $studet, $lastroe) {
        if (count($rowsubjects) > 0) {
            $column = $ncolumn = array();
            foreach ($rowsubjects as $key => $levl) {
                foreach ($levl as $chl) {
                    $clststQury = array();
                    if (count($studet) > 0) {
                        $clststQury[] = " grp_subject_teacher_id IN (" . $studet . ")";
                    }
                    $clststQury[] = "  subjct_modules IN (" . $chl . ")";
                    $clstest = ClassTest::find(implode(' and ', $clststQury) . ' ORDER BY created_date ');
                    if (count($clstest) > 0) {
                        foreach ($clstest as $clst) {

                            $column[] = array(
                                'text' => $clst->class_test_name,
                                'rowspan' => '',
                                'colspan' => '',
                                'testid' => $clst->class_test_id,
                                'class' => 'notopborder',
                            );

                            $totmark = ClassTestMarks::findFirst('class_test_id = ' . $clst->class_test_id);
                            $ncolumn[] = array(
                                'text' => '<label class="label label-success" >' . $totmark->outof . '</label>',
                                'rowspan' => '',
                                'colspan' => '',
                                'class' => 'nobottborder',
                            );
                        }
                    } else {
                        $column[] = array(
                            'text' => '',
                            'rowspan' => '',
                            'colspan' => '',
                            'class' => 'notopborder',
                        );
                        $ncolumn[] = array(
                            'text' => '',
                            'rowspan' => '',
                            'colspan' => '',
                            'class' => 'nobottborder',
                        );
                    }
                }
            }
            $rowhead[$lastroe] = $column;
            $rowhead[$lastroe + 1] = $ncolumn;
        }
        return $rowhead;
    }

    public function find_classtestmarkreport($rowexms, $studet) {

//       echo '<pre>'; print_r(array_reverse($rowexms));exit;
//        $rowexms = array_reverse($rowexms);
        if (count($rowexms) > 0) {
            foreach ($rowexms as $levl) {
                foreach ($levl as $chl) {
                    ClassTestMarks::findFirst('class_test_id = ' . $tst->class_test_id
                            . ' and student_id =' . $stu->student_info_id);
                    $marks = MainexamMarks::findFirst(array(
                                'columns' => '(marks+inherited_marks) as mark ,(outof+inherited_outof) as outof ',
                                "student_id = $studet[0] and mainexam_id= $studet[1] and subject_id= $chl and grp_subject_teacher_id IN($studet[2])"
                    ));
//                    $mname = OrganizationalStructureValues::findFirstById($chl)->name;
                    $html .= '<td class="viewmark"><span class="label label-info">' . (isset($marks->mark) ? ($marks->mark . '/' . $marks->outof ) : '') . '</span></td>';
                }
            }
        }
        return $html;
    }

    public function find_childtreevalinputold($aid, $html = '', $studet) {
        $exist = OrganizationalStructureValues::find('parent_id =' . $aid);
        $nodeval = OrganizationalStructureValues::findFirstById($aid);
        if (count($exist) > 0) {
            foreach ($exist as $chl) {
                $html = MarkController::find_childtreevalinput($chl->id, $html, $studet);
            }
        } else {
//            echo "student_id = $studet[0] and mainexam_id= $studet[1] and subject_id= $nodeval->id";
            $marks = MainexamMarks::findFirst(array(
                        'columns' => '(marks+inherited_marks) as mark ,(outof+inherited_outof) as outof ',
                        "student_id = $studet[0] and mainexam_id= $studet[1] and subject_id= $nodeval->id"
            ));
            $html .= '<td><span class="label label-info">' . $marks->mark . '</span></td>';
        }
        return $html;
    }

    public function get_rowvalinput($master_id, $mainExId, $aid, $html = '', $exmarr = array(), $c, $highest = 0, $exmids = array()) {


        $c = $c ? $c : 0;
        $exist = OrganizationalStructureValues::find('parent_id IN ( ' . implode(',', $aid) . ')');
        $edit = '';
        if (count($exist) > 0) {
            $html .="<tr>";
            foreach ($exist as $chl) {
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $colspan = MarkController::_getChildCount(array($chl->id));
                    $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                }
            }
            foreach ($exist as $chl) {
                $span = '';
                $colspan = array();
                $rowspan = 0;
                $roespn = '';
                $edit = '';
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $nxtchld = OrganizationalStructureValues::find('parent_id = ' . $chl->id);
                    if (count($nxtchld) == 0) {
                        $marks = MainexamMarks::findFirst(array(
                                    'columns' => '(outof+inherited_outof) as outof ',
                                    " mainexam_id= $mainExId and subject_id= $chl->id" .
                                    ' and grp_subject_teacher_id = ' . $master_id
                        ));

                        if ($c < $highest) {
                            $rowspan = ($highest - $c);
                        }
                        $roespn = ' rowspan = "' . $rowspan . '"';
                        $edit = '<i class="fa fa-edit lastchild" onclick="markSettings.loadStudentDiv(' . $chl->id . ');" ></i><br>'
                                . ' <span class="label label-success">' . $marks->outof . '</span>';
                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);
                        if (!in_array($chl->id, $exmids[$cntexm]))
                            $exmids[$cntexm] = $chl->id;
                    } else {
//                        $colspan = MarkController::_getChildCount(array($chl->id));
//                        $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
//                        $span = 'colspan="' . $colspan[0] . '"';

                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);
                        $colspan = MarkController::_getChildCountExm(array($chl->id), 0, 1, $exmids, $cntexm);
                        $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                        $span = 'colspan="' . $colspan[0] . '"';
                        $exmids = $colspan[2];
                    }
//               $spancnt += count($nxtchld);
                    $html .= '<td class="viewmark" ' . $span . $roespn . ' >' . $chl->name . $edit . '</td>';
                    $nextids[] = $chl->id;
                }
            }
            $html .="</tr>";
        }

        if (count($nextids) > 0) {
            $c = $c + 1;
            $res = MarkController::get_rowvalinput($master_id, $mainExId, $nextids, $html, $exmarr, $c, $highest);
            $html = $res[0];
            $exmarr = $res[1];
            $c = $res[2];
        }
        return array($html, $exmarr, $c, $highest);
    }

    public function gteSubjectsParents($aid, $exmsubjarr = array()) {
        $checkmas = OrganizationalStructureValues::findfirst('id = ' . $aid);
        $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $checkmas->org_master_id);
        if ($checksubjnode->module == "Subject" && $checkmas->parent_id > 0) {
            $nxtchld = OrganizationalStructureValues::findFirstById($checkmas->parent_id);
            $checkmassubjnode = OrganizationalStructureMaster::findfirst('id = ' . $nxtchld->org_master_id);
            if ($checkmassubjnode->module == "Subject") {
                $exmsubjarr = MarkController::gteSubjectsParents($checkmas->parent_id, $exmsubjarr);
            } else {
                $exmsubjarr[] = $aid;
            }
        } else {
            $exmsubjarr[] = $aid;
        }
        return $exmsubjarr;
    }

    public function gteSubjectsInOrder($aid, $exmsubjarr = array()) {
        $checkmas = OrganizationalStructureValues::findfirst('id = ' . $aid);
        $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $checkmas->org_master_id);
        if ($checksubjnode->module == "Subject") {
            $nxtchld = OrganizationalStructureValues::find('parent_id = ' . $aid);
            if (count($nxtchld) > 0) {
                foreach ($nxtchld as $dd) {
                    $exmsubjarr = MarkController::gteSubjectsInOrder($dd->id, $exmsubjarr);
                }
            } else {
                $exmsubjarr[] = $aid;
            }
        }
        return $exmsubjarr;
    }

    public function headerForEvaluation($evalheader, $aid, $row = array(), $exmarr = array(), $c, $highest = 0, $exmids = array()) {
        $c = $c ? $c : 0;
        $exist = OrganizationalStructureValues::find('id IN ( ' . implode(',', $aid) . ')');
        $column = array();
        $edit = '';
        if (count($exist) > 0) {
            foreach ($exist as $chl) {
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $colspan = MarkController::_getChildCount(array($chl->id));
                    $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                }
            }
            foreach ($exist as $chl) {
                $span = '';
                $colspan = array();
                $rowspan = 0;
                $roespn = '';
                $edit = '';
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $nxtchld = OrganizationalStructureValues::find('parent_id = ' . $chl->id);
                    if (count($nxtchld) == 0) {

                        if ($c < $highest) {
                            $rowspan = ($highest - $c);
                        }

                        $scolspan = MarkController::_getColspanEval($evalheader, $chl->id);
                        $span = count($scolspan);
                        $roespn = $rowspan;
                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);
                        if (!in_array($chl->id, $exmids[$cntexm]))
                            $exmids[$cntexm] = $chl->id;
                    } else {
                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);

                        $scolspan = MarkController::_getColspanEval($evalheader, $chl->id);
                        $colspan = MarkController::_getChildCountTst(array($chl->id), 0, 1, $exmids, $cntexm);
//                        print_r(($scolspan)); exit;
                        $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                        $span = count($scolspan);
                        $exmids = $colspan[2];
                    }

                    $icolumn['text'] = $chl->name;
                    $icolumn['rowspan'] = $roespn;
                    $icolumn['colspan'] = $span;
                    $icolumn['sid'] = $chl->id;
                    $column[] = $icolumn;
                }
            }

            $row[] = $column;
        }
        $nxtc = OrganizationalStructureValues::findFirst(array('columns' => 'GROUP_CONCAT(id) as chids', 'parent_id IN ( ' . implode(',', $aid) . ' )'));
        $nextids = $nxtc->chids ? explode(',', $nxtc->chids) : array();
        if (count($nextids) > 0) {
            $c = $c + 1;
            $res = MarkController::headerForEvaluation($evalheader, $nextids, $row, $exmarr, $c, $highest);
            $row = $res[0];
            $exmarr = $res[1];
            $c = $res[2];
        } else {
            
        }
        return array($row, $exmarr, $c, $highest);
    }

    public function get_rowvalinputforreport($mainExId, $aid, $html = '', $exmarr = array(), $c, $highest = 0, $exmids = array()) {
        $c = $c ? $c : 0;
        $exist = OrganizationalStructureValues::find('id IN ( ' . implode(',', $aid) . ')');
        $edit = '';
        if (count($exist) > 0) {
            $html .="<tr>";
            foreach ($exist as $chl) {
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $colspan = MarkController::_getChildCount(array($chl->id));
                    $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                }
            }
            foreach ($exist as $chl) {
                $span = '';
                $colspan = array();
                $rowspan = 0;
                $roespn = '';
                $edit = '';
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $nxtchld = OrganizationalStructureValues::find('parent_id = ' . $chl->id);
                    if (count($nxtchld) == 0) {
//                        $marks = MainexamMarks::findFirst(array(
//                                    'columns' => '(outof+inherited_outof) as outof ',
//                                    " mainexam_id= $mainExId and subject_id= $chl->id"
//                        ));

                        if ($c < $highest) {
                            $rowspan = ($highest - $c);
                        }
                        $roespn = ' rowspan = "' . $rowspan . '"';
//                        $edit = ' <br><span class="label label-success">' . $marks->outof . '</span>';
                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);
                        if (!in_array($chl->id, $exmids[$cntexm]))
                            $exmids[$cntexm] = $chl->id;
                    } else {
                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);

                        $scolspan = MarkController::_getColspan($chl->id);
                        $colspan = MarkController::_getChildCountExm(array($chl->id), 0, 1, $exmids, $cntexm);
//echo '<pre>';print_r($scolspan);exit;
                        $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                        $span = 'colspan="' . count($scolspan) . '"';
                        $exmids = $colspan[2];
                    }
//               $spancnt += count($nxtchld);
                    $html .= '<th ' . $span . $roespn . ' subjid = "' . $chl->id . '">' . $chl->name . $edit . '</th>';
                }
            }

            $html .="</tr>";
        }
        $nxtc = OrganizationalStructureValues::findFirst(array('columns' => 'GROUP_CONCAT(id) as chids', 'parent_id IN ( ' . implode(',', $aid) . ' )'));
        $nextids = $nxtc->chids ? explode(',', $nxtc->chids) : array();
        if (count($nextids) > 0) {
            $c = $c + 1;
            $res = MarkController::get_rowvalinputforreport($mainExId, $nextids, $html, $exmarr, $c, $highest);
            $html = $res[0];
            $exmarr = $res[1];
            $c = $res[2];
        }
        return array($html, $exmarr, $c, $highest);
    }

    public function loadHeaderForClasstest($masid, $aid, $row = array(), $exmarr = array(), $c, $highest = 0, $exmids = array()) {
        $c = $c ? $c : 0;
        $exist = OrganizationalStructureValues::find('id IN ( ' . implode(',', $aid) . ')');
        $column = array();
        $edit = '';
        if (count($exist) > 0) {
            foreach ($exist as $chl) {
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $colspan = MarkController::_getChildCount(array($chl->id));
                    $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                }
            }
            foreach ($exist as $chl) {
                $span = '';
                $colspan = array();
                $rowspan = 0;
                $roespn = '';
                $edit = '';
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $nxtchld = OrganizationalStructureValues::find('parent_id = ' . $chl->id);
                    if (count($nxtchld) == 0) {

                        if ($c < $highest) {
                            $rowspan = ($highest - $c);
                        }

                        $scolspan = MarkController::_getColspanClstst($masid, $chl->id);
                        $span = count($scolspan);
                        $roespn = $rowspan;
                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);
                        if (!in_array($chl->id, $exmids[$cntexm]))
                            $exmids[$cntexm] = $chl->id;
                    } else {
                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);

                        $scolspan = MarkController::_getColspanClstst($masid, $chl->id);
                        $colspan = MarkController::_getChildCountTst(array($chl->id), 0, 1, $exmids, $cntexm);
//                        print_r(($scolspan)); exit;
                        $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                        $span = count($scolspan);
                        $exmids = $colspan[2];
                    }

                    $icolumn['text'] = $chl->name;
                    $icolumn['rowspan'] = $roespn;
                    $icolumn['colspan'] = $span;
                    $icolumn['sid'] = $chl->id;
                    $column[] = $icolumn;
                }
            }

            $row[] = $column;
        }
        $nxtc = OrganizationalStructureValues::findFirst(array('columns' => 'GROUP_CONCAT(id) as chids', 'parent_id IN ( ' . implode(',', $aid) . ' )'));
        $nextids = $nxtc->chids ? explode(',', $nxtc->chids) : array();
        if (count($nextids) > 0) {
            $c = $c + 1;
            $res = MarkController::loadHeaderForClasstest($masid, $nextids, $row, $exmarr, $c, $highest);
            $row = $res[0];
            $exmarr = $res[1];
            $c = $res[2];
        } else {
            
        }
        return array($row, $exmarr, $c, $highest);
    }

    public function loadHeaderForAssignment($masid, $aid, $row = array(), $exmarr = array(), $c, $highest = 0, $exmids = array()) {
        $c = $c ? $c : 0;
        $exist = OrganizationalStructureValues::find('id IN ( ' . implode(',', $aid) . ')');
        $column = array();
        $edit = '';
        if (count($exist) > 0) {
            foreach ($exist as $chl) {
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $colspan = MarkController::_getChildCount(array($chl->id));
                    $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                }
            }
            foreach ($exist as $chl) {
                $span = '';
                $colspan = array();
                $rowspan = 0;
                $roespn = '';
                $edit = '';
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $nxtchld = OrganizationalStructureValues::find('parent_id = ' . $chl->id);
                    if (count($nxtchld) == 0) {

                        if ($c < $highest) {
                            $rowspan = ($highest - $c);
                        }

                        $scolspan = MarkController::_getColspanAss($masid, $chl->id);
                        $span = count($scolspan);
                        $roespn = $rowspan;
                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);
                        if (!in_array($chl->id, $exmids[$cntexm]))
                            $exmids[$cntexm] = $chl->id;
                    } else {
                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);

                        $scolspan = MarkController::_getColspanAss($masid, $chl->id);
                        $colspan = MarkController::_getChildCountTst(array($chl->id), 0, 1, $exmids, $cntexm);
//                        print_r(($scolspan)); exit;
                        $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                        $span = count($scolspan);
                        $exmids = $colspan[2];
                    }

                    $icolumn['text'] = $chl->name;
                    $icolumn['rowspan'] = $roespn;
                    $icolumn['colspan'] = $span;
                    $icolumn['sid'] = $chl->id;
                    $column[] = $icolumn;
                }
            }

            $row[] = $column;
        }
        $nxtc = OrganizationalStructureValues::findFirst(array('columns' => 'GROUP_CONCAT(id) as chids', 'parent_id IN ( ' . implode(',', $aid) . ' )'));
        $nextids = $nxtc->chids ? explode(',', $nxtc->chids) : array();
        if (count($nextids) > 0) {
            $c = $c + 1;
            $res = MarkController::loadHeaderForAssignment($masid, $nextids, $row, $exmarr, $c, $highest);
            $row = $res[0];
            $exmarr = $res[1];
            $c = $res[2];
        } else {
            
        }
        return array($row, $exmarr, $c, $highest);
    }

    public function loadHeaderForSubjRating($masid, $aid, $row = array(), $exmarr = array(), $c, $highest = 0, $exmids = array()) {
        $c = $c ? $c : 0;
        $exist = OrganizationalStructureValues::find('id IN ( ' . implode(',', $aid) . ')');
        $column = array();
        $edit = '';
        if (count($exist) > 0) {
            foreach ($exist as $chl) {
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $colspan = MarkController::_getChildCount(array($chl->id));
                    $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                }
            }
            foreach ($exist as $chl) {
                $span = '';
                $colspan = array();
                $rowspan = 0;
                $roespn = '';
                $edit = '';
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $nxtchld = OrganizationalStructureValues::find('parent_id = ' . $chl->id);
                    if (count($nxtchld) == 0) {

                        if ($c < $highest) {
                            $rowspan = ($highest - $c);
                        }

                        $scolspan = MarkController::_getColspanSubjrat($chl->id);
                        $span = count($scolspan);
                        $roespn = $rowspan;
                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);
                        if (!in_array($chl->id, $exmids[$cntexm]))
                            $exmids[$cntexm] = $chl->id;
                    } else {
                        $exmarr[$c][] = $chl->id;
                        $cntexm = count($exmids);

                        $scolspan = MarkController::_getColspanSubjrat($chl->id);
                        $colspan = MarkController::_getChildCountTst(array($chl->id), 0, 1, $exmids, $cntexm);
//                        print_r(($scolspan)); exit;
                        $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                        $span = count($scolspan);
                        $exmids = $colspan[2];
                    }

                    $icolumn['text'] = $chl->name;
                    $icolumn['rowspan'] = $roespn;
                    $icolumn['colspan'] = $span;
                    $icolumn['sid'] = $chl->id;
                    $column[] = $icolumn;
                }
            }

            $row[] = $column;
        }
        $nxtc = OrganizationalStructureValues::findFirst(array('columns' => 'GROUP_CONCAT(id) as chids', 'parent_id IN ( ' . implode(',', $aid) . ' )'));
        $nextids = $nxtc->chids ? explode(',', $nxtc->chids) : array();
        if (count($nextids) > 0) {
            $c = $c + 1;
            $res = MarkController::loadHeaderForSubjRating($masid, $nextids, $row, $exmarr, $c, $highest);
            $row = $res[0];
            $exmarr = $res[1];
            $c = $res[2];
        } else {
            
        }
        return array($row, $exmarr, $c, $highest);
    }

    public function get_rowvalinputforprint($mainExId, $aid, $html = '', $exmarr = array(), $c, $highest = 0, $i = 0) {
        $c = $c ? $c : 0;
        $exist = OrganizationalStructureValues::find('parent_id IN ( ' . implode(',', $aid) . ')');
        $edit = '';
        if (count($exist) > 0) {
            $html .="<tr>";
            foreach ($exist as $chl) {
                $colspan = MarkController::_getChildCount(array($chl->id));
                $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
            }

            foreach ($exist as $chl) {
                $span = '';
                $colspan = array();
                $rowspan = 0;
                $roespn = '';
                $edit = '';
                $checksubjnode = OrganizationalStructureMaster::findfirst('id = ' . $chl->org_master_id);
                if ($checksubjnode->module == "Subject") {
                    $nxtchld = OrganizationalStructureValues::find('parent_id = ' . $chl->id);
                    if (count($nxtchld) == 0) {
                        $marks = MainexamMarks::findFirst(array(
                                    'columns' => '(outof+inherited_outof) as outof ',
                                    " mainexam_id= $mainExId and subject_id= $chl->id"
                        ));

                        if ($c < $highest) {
                            $rowspan = ($highest - $c);
                        }
                        $roespn = ' rowspan = "' . $rowspan . '"';
                        $edit = ' <span >' . ( round($marks->outof) > 0 ? round($marks->outof) : '') . '</span>';
                        $exmarr[$c][] = $chl->id;
                    } else {
                        $colspan = MarkController::_getChildCount(array($chl->id));
                        $highest = ($highest < $colspan[1]) ? $colspan[1] : $highest;
                        $span = 'colspan="' . $colspan[0] . '"';
//               
                    }
                    if ($i == 0):
                        $html .="<td rowspan='" . ( $highest) . "' class='center'>Students</td>";
                        $i++;
                    endif;
//               $spancnt += count($nxtchld);
                    $html .= '<td ' . $span . $roespn . ' >' . $chl->name . $edit . '</td>';
                    $nextids[] = $chl->id;
                }
            }
            $html .="</tr>";
        }

        if (count($nextids) > 0) {
            $c = $c + 1;
            $res = MarkController::get_rowvalinputforprint($mainExId, $nextids, $html, $exmarr, $c, $highest, $i);
            $html = $res[0];
            $exmarr = $res[1];
            $c = $res[2];
        }
        return array($html, $exmarr, $c, $highest);
    }

    public function _getChildCount($aids, $colspan, $c = 1) {
        $colspan1 = 0;
        if (count($aids) > 0) {
            foreach ($aids as $chld) {
                $nxtchld = OrganizationalStructureValues::find('parent_id  = ' . $chld);
                if (count($nxtchld) > 0) {
                    $colspan1 = $colspan1 + (count($nxtchld));
                } else {
                    $colspan1 += 1;
                }
            }
            $nxt = OrganizationalStructureValues::find(array('columns' => 'group_concat(id) as ids',
                        'parent_id  IN ( ' . implode(',', $aids) . ')'
            ));
            if ($nxt[0]->ids != '') {
                $c ++;
                $colspan = ($colspan < $colspan1) ? $colspan1 : $colspan;
                $idsnxt = explode(',', $nxt[0]->ids);
                $res = MarkController::_getChildCount($idsnxt, $colspan, $c);
                $colspan = $res[0];
                $c = $res[1];
            }
        }
        return array($colspan, $c);
    }

    public function _getColspanSubjrat($aid, $restids) {
        $nxtchld = OrganizationalStructureValues::find('parent_id  = ' . $aid);
        if (count($nxtchld) > 0) {
            foreach ($nxtchld as $chl) {
                $restids = MarkController::_getColspanSubjrat($chl->id, $restids);
            }
        } else {
            $clstest = RatingCategoryMaster::find();
            if (count($clstest) > 0) {
                foreach ($clstest as $fvalue) {
                    $restids[] = $fvalue->id;
                }
                $restids[] = 'total';
            } else {
                $restids[] = '';
            }
        }
        return $restids;
    }

    public function _getColspanAss($masid, $aid, $restids) {
        $nxtchld = OrganizationalStructureValues::find('parent_id  = ' . $aid);
        if (count($nxtchld) > 0) {
            foreach ($nxtchld as $chl) {
                $restids = MarkController::_getColspanAss($masid, $chl->id, $restids);
            }
        } else {
            $clststQury = array();
            if (count($masid) > 0) {
                $clststQury[] = " grp_subject_teacher_id IN (" . $masid . ")";
            }
            $clststQury[] = "  subjct_modules IN (" . $aid . ")";
            $clstest = AssignmentsMaster::find(implode(' and ', $clststQury));
            if (count($clstest) > 0) {
                foreach ($clstest as $fvalue) {
                    $restids[] = $fvalue->id;
                }
            } else {
                $restids[] = '';
            }
        }
        return $restids;
    }

    public function _getColspanClstst($masid, $aid, $restids) {
        $nxtchld = OrganizationalStructureValues::find('parent_id  = ' . $aid);
        if (count($nxtchld) > 0) {
            foreach ($nxtchld as $chl) {
                $restids = MarkController::_getColspanClstst($masid, $chl->id, $restids);
            }
        } else {
            $clststQury = array();
            if (count($masid) > 0) {
                $clststQury[] = " grp_subject_teacher_id IN (" . $masid . ")";
            }
            $clststQury[] = "  subjct_modules IN (" . $aid . ")";
            $clstest = ClassTest::find(implode(' and ', $clststQury));
            if (count($clstest) > 0) {
                foreach ($clstest as $fvalue) {
                    $restids[] = $fvalue->class_test_id;
                }
            } else {
                $restids[] = '';
            }
        }
        return $restids;
    }

    public function _getColspanEval($evalheader, $aid, $restids) {
        $nxtchld = OrganizationalStructureValues::find('parent_id  = ' . $aid);
        if (count($nxtchld) > 0) {
            foreach ($nxtchld as $chl) {
                $restids = MarkController::_getColspanEval($evalheader, $chl->id, $restids);
            }
        } else {
            if (isset($evalheader[$aid]) && count($evalheader[$aid]) > 0) {
                foreach ($evalheader[$aid] as $key => $fvalue) {
                    $restids[] = $key;
                }
            } else {
                $restids[] = '';
            }
        }
        return $restids;
    }

    public function _getColspan($aid, $restids) {
        $nxtchld = OrganizationalStructureValues::find('parent_id  = ' . $aid);
        if (count($nxtchld) > 0) {
            foreach ($nxtchld as $chl) {
                $restids = MarkController::_getColspan($chl->id, $restids);
            }
        } else {
            $restids[] = $aid;
        }
        return $restids;
    }

    public function _getChildCountTst($aids, $colspan, $c = 1, $exmids, $cntexm) {
        $colspan1 = 0;
        if (count($aids) > 0) {
            foreach ($aids as $chld) {
                $nxtchld = OrganizationalStructureValues::find('parent_id  = ' . $chld);
                if (count($nxtchld) > 0) {
                    $colspan1 = $colspan1 + (count($nxtchld));
                } else {
                    $exmids[$cntexm][count($exmids[$cntexm])] = $chld;
                    $colspan1 += 1;
                }
            }

//            echo count($clstest).'~'.$colspan1.'~<br>';
            $nxt = OrganizationalStructureValues::findFirst(array('columns' => 'group_concat(id) as ids',
                        'parent_id  IN ( ' . implode(',', $aids) . ')'
            ));
            if ($nxt->ids != '') {
                $c ++;
                $colspan = ($colspan < $colspan1) ? $colspan1 : $colspan;
                $idsnxt = explode(',', $nxt->ids);
//print_r($colspan1);echo 'sas<br>';
                $res = MarkController::_getChildCountTst($idsnxt, $colspan, $c, $exmids[$cntexm], count($exmids[$cntexm]));
                $colspan = $res[0];
                $c = $res[1];
                $exmids[$cntexm] = ($res[2]);
            }
        }
        return array($colspan, $c, $exmids);
    }

    public function _getChildCountExm($aids, $colspan, $c = 1, $exmids, $cntexm) {
        $colspan1 = 0;
        if (count($aids) > 0) {
            foreach ($aids as $chld) {
                $nxtchld = OrganizationalStructureValues::find('parent_id  = ' . $chld);
                if (count($nxtchld) > 0) {
                    $colspan1 = $colspan1 + (count($nxtchld));
                } else {
                    $exmids[$cntexm][count($exmids[$cntexm])] = $chld;
                    $colspan1 += 1;
                }
            }
            $nxt = OrganizationalStructureValues::findFirst(array('columns' => 'group_concat(id) as ids',
                        'parent_id  IN ( ' . implode(',', $aids) . ')'
            ));
            if ($nxt->ids != '') {
                $c ++;
                $colspan = ($colspan < $colspan1) ? $colspan1 : $colspan;
                $idsnxt = explode(',', $nxt->ids);
//print_r($colspan1);echo 'sas<br>';
                $res = MarkController::_getChildCountExm($idsnxt, $colspan, $c, $exmids[$cntexm], count($exmids[$cntexm]));
                $colspan = $res[0];
                $c = $res[1];
                $exmids[$cntexm] = ($res[2]);
            }
        }
        return array($colspan, $c, $exmids);
    }

    public function find_childtreeval_htmlold($aid, $html, $c = 0, $stucnt = array()) {
//echo 'test';exit;
        $exist = OrganizationalStructureValues::find('parent_id =' . $aid);
        $nodeval = OrganizationalStructureValues::findFirstById($aid);
        $vcn = OrganizationalStructureValues::find('parent_id =' . $nodeval->parent_id);
        $valcnt = ($c > 0) ? (12 / count($vcn)) : 12;
        $c++;
        if (count($exist) > 0) {
            $html .= '<div class="col-md-' . $valcnt . '" style="border-top: 1px solid; text-align: center;">' . $nodeval->name;
            $html .= '<div class="col-md-12" style="border-top: 1px solid; text-align: center;">';
            foreach ($exist as $chl) {
                $resarr = MarkController::find_childtreeval_html($chl->id, $html, $c, $stucnt);
                $html = $resarr[0];
                $stucnt = $resarr[1];
            } $html .= '</div>';
            $html .= '</div>';
        } else {
            $stucnt[] = $nodeval->id; // ++;
            $html .= '<div class="col-md-' . $valcnt . '" style="border-top: 1px solid; text-align: center;">' . $nodeval->name . '</div>';
        }
        return array($html, $stucnt);
    }

    public function getStudentMarkListOldAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $master_id = $this->request->getPost('masterid');
            $teacherType = $this->request->getPost('type');
            $this->view->mainExId = ($this->request->getPost('mainExId'));
            $SubDivSubjmaster = GroupSubjectsTeachers::findFirstById($master_id);
            $this->view->subject_id = $SubDivSubjmaster->subject_id;
            $param['type'] = 'mainexam_mark';
            $getCombination = ControllerBase::loadCombinations($param);
            $combinations = ($getCombination[1]);
            $teachSubj = array();
            foreach ($combinations as $key => $combi) {
                $teachSubj[$key] = GroupClassTeachers::findFirst($key)->subject_id;
            }
            $this->view->iseditable = $teachSubj;
//            $sres = ControllerBase::buildExamQuery($SubDivSubjmaster->aggregated_nodes_id);
//            print_r(implode(' or ', $sres));exit;
            $getSubjects = SubjectsDivision::find("LOCATE(  node_id ,  '$SubDivSubjmaster->aggregated_nodes_id' ) ");
            foreach ($getSubjects as $subj) {
                $subjectList[$subj->id] = $subj;
            }
            $this->view->subjects = $subjectList;
            if ($teacherType != 'classTeacher') {
                $res = ControllerBase::buildStudentQuery($SubDivSubjmaster->aggregated_nodes_id);
                $this->view->students = StudentMapping::find('status = "Inclass" and ' . implode(' or ', $res));
                $this->view->master_id = $SubDivSubjmaster->id;
                $this->view->node_id = ControllerBase::getNameForKeys($SubDivSubjmaster->aggregated_nodes_id);
            }
        }
    }

    public function getStudentListAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $master_id = $this->request->getPost('masterid');
            $this->view->subsujectid = $subsujectid = $this->request->getPost('subsujectid');
            $teacherType = $this->request->getPost('type');
            $this->view->mainExId = $mainExId = ($this->request->getPost('mainExId'));
            $SubDivSubjmaster = GroupSubjectsTeachers::findFirstById($master_id);
            $this->view->subject_id = $SubDivSubjmaster->subject_id;

            $condition = "subject_id = $subsujectid AND mainexm_id = $mainExId" .
                    ' and grp_subject_teacher_id = ' . $master_id;

            $this->view->formuladet = $formuladet = FormulaTable::findFirst($condition);
            $this->view->title = OrganizationalStructureValues::findFirstById($subsujectid)->name;
            if ($teacherType != 'classTeacher') {
                $classmaster = ClassroomMaster::findFirstById($SubDivSubjmaster->classroom_master_id);
//                $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);

                $orgvaldet = OrganizationalStructureValues::findFirstById($SubDivSubjmaster->subject_id);
//                $orgvaldet = OrganizationalStructureValues::findFirstById($orgvaldet->parent_id);
                $subj_arr = $this->_getMandNodesForExam($orgvaldet);

                $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", aggregate_key)>0)', array_keys($subj_arr));


 $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//              
                $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                        . 'stumap.subordinate_key,stumap.status'
                        . ' FROM StudentMapping stumap LEFT JOIN'
                        . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                            . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
                        . ' and (' . implode(' and ', $subjQuery) . ') ORDER BY stuinfo.Admission_no ASC';

//                print_r($stuquery);exit;
                $this->view->students = $this->modelsManager->executeQuery($stuquery);

//                $this->view->students = StudentMapping::find('status = "Inclass" and (' . implode(' or ', $res)
//                                . ') and (' . implode(' and ', $subjQuery) . ')');
//                $this->view->students = StudentMapping::find('status = "Inclass" and ' . implode(' or ', $res));
                $this->view->master_id = $SubDivSubjmaster->id;
                $this->view->node_id = ControllerBase::getNameForKeys($classmaster->aggregated_nodes_id);
                $this->view->mainexams = Mainexam::find("LOCATE(  node_id ,  '$classmaster->aggregated_nodes_id' ) ");
            }
        }
    }

    public function updateAlStudentMarkAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $mainExId = $this->request->getPost('mainExId');
            $mainExOutof = $this->request->getPost('MainExTotalMark');
            $masterid = $this->request->getPost('masterid');
            $subsujectid = $this->request->getPost('subsujectid');
            $mainexRec = MainexamMarks::find('mainexam_id = ' . $mainExId .
                            ' and subject_id = ' . $subsujectid .
                            ' and grp_subject_teacher_id = ' . $masterid);
//            $clsTstRec = ClassTest::find('mainexam_id = ' . $mainExId);
            $student_id_arr = array();
            foreach ($mainexRec as $mainEx) {
                $student_id_arr[] = $mainEx->student_id;
            }

            $stuarr = array_unique($student_id_arr);
            //getallstudents under this exam
            $res = 1;
            foreach ($stuarr as $student_id) {
                $recursiveParam = array(
                    'stuId' => $student_id,
                    'x2' => $mainExId,
                    'obtainedMarks' => '',
                    'obtainedOutOf' => $mainExOutof,
                    'subject_id' => $subsujectid,
                    'clastst_combi_id' => $masterid
                );
                $res = $this->_calculateMarksToLinkedExamsNEW($recursiveParam);
            }
            if ($res != 1) {
                foreach ($res->getMessages() as $message) {
                    $error .= $message;
                }

                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
            if ($res == 1) {
                $message['type'] = 'success';
                $message['message'] = 'Successfully updated';
                print_r(json_encode($message));
            }
        }
    }

    public function updateStudentMarkAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        if ($this->request->isPost()) {

            $mainExId = $this->request->getPost('mainExId');
            $student_id = $this->request->getPost('StuId');
            $mark = $this->request->getPost('StuMainExMark');
            $mainExOutof = $this->request->getPost('MainExTotalMark');
            $subsujectid = $this->request->getPost('subsujectid');
            $masterid = $this->request->getPost('masterid');
//            echo floatval($mainExOutof) .'< '.floatval($mark);
//            echo (floatval($mainExOutof) < floatval($mark))?'y':'no' .'<br>';
            if (floatval($mainExOutof) < floatval($mark)) {
                $message['type'] = 'error';
                $message['message'] = 'Student Mark exceeds total marks';
                print_r(json_encode($message));
                exit;
            } else {
                $recursiveParam = array(
                    'stuId' => $student_id,
                    'x2' => $mainExId,
                    'obtainedMarks' => $mark,
                    'obtainedOutOf' => $mainExOutof,
                    'subject_id' => $subsujectid,
                    'clastst_combi_id' => $masterid
                );
                $res = $this->_calculateMarksToLinkedExamsNEW($recursiveParam);
            }

            if ($res == 1) {
                $message['type'] = 'success';
                $message['message'] = 'Successfully updated';
                print_r(json_encode($message));
            }
        }
    }

    public function updateClassTestMarkAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $test_id = $this->request->getPost('test_id');
            $student_id = $this->request->getPost('student_id');
            $mark = $this->request->getPost('mark');
            $total_mark = $this->request->getPost('total_mark');

            $record = ClassTestMarks::findFirst('class_test_id = ' . $test_id . ' and student_id = ' . $student_id) ?
                    ClassTestMarks::findFirst('class_test_id = ' . $test_id . ' and student_id = ' . $student_id) :
                    new ClassTestMarks();
            $linkedReccord = ClassTest::findFirstByClassTestId($test_id);
            $grpSubjTeach = GroupSubjectsTeachers::findFirstById($linkedReccord->grp_subject_teacher_id);
            $record->marks = $mark;
            $record->class_test_id = $test_id;
            $record->student_id = $student_id;
            $record->outof = $total_mark;
            if (!$record->save()) {
                foreach ($record->getMessages() as $messages) {
                    $error .= $messages . '<br>';

//                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
            } else {
//                echo 'test';exit;

                $clstestid = 'C_' . $test_id;
                $subject_id = $linkedReccord->subjct_modules;

                $condition = 'subject_id = ' . $subject_id . ' AND (FIND_IN_SET("' . $clstestid . '",exam_ids) > 0)';

//                echo $condition; exit;
                $formula = FormulaTable::findFirst($condition);
                $exam_ids = isset($formula->exam_ids) ? explode(',', $formula->exam_ids) : '';
                $student_id_arr = array();
                if ($exam_ids != '') {
                    // echo 'vv';exit;
                    foreach ($exam_ids as $exam_id) {
                        $expldval = explode('_', $exam_id);
                        if ($expldval[0] == 'M') {
                            $refmainmarks = MainexamMarks::find('mainexam_id = ' . $expldval[1] .
                                            ' and subject_id = ' . $subjectid .
                                            ' and grp_subject_teacher_id = ' . $master_id) ?
                                    MainexamMarks::find('mainexam_id = ' . $expldval[1] .
                                            ' and subject_id = ' . $subjectid .
                                            ' and grp_subject_teacher_id = ' . $master_id) :
                                    new MainexamMarks();

                            if ($refmainmarks) {
                                foreach ($refmainmarks as $refmainmark) {
                                    $student_id_arr[] = $refmainmark->student_id;
                                }
                            }
                        }
                        if ($expldval[0] == 'C') {
                            $reftest = ClassTestMarks::find('class_test_id = ' . $expldval[1]);
//                           echo 'in';      
                            if ($reftest) {
                                foreach ($reftest as $reftestmark) {
                                    $student_id_arr[] = $reftestmark->student_id;
                                }
                            }
                        }
                        if ($expldval[0] == 'A') {
                            $refassign = AssignmentMarks::find('assignment_id =' . $expldval[1]);
                            if ($refassign) {
                                foreach ($refassign as $refassignmark) {
                                    $student_id_arr[] = $refassignmark->student_id;
                                }
                            }
                        }
                    }

//             print_r($student_id_arr);
//            $clsTstRec = ClassTest::find('mainexam_id = ' . $mainExId);
//                foreach ($mainexRec as $mainEx) {
//                    $student_id_arr[] = $mainEx->student_id;
//                }
                    $stuarr = array_unique($student_id_arr);
//                 print_r($stuarr);
//                exit;
                    $res = 1;
                    foreach ($stuarr as $student_id) {
                        $recursiveParam = array(
                            'stuId' => $student_id,
                            'x2' => $formula->mainexm_id,
                            'obtainedMarks' => '',
                            'obtainedOutOf' => '',
                            'subject_id' => $subject_id,
                            'clastst_combi_id' => $grpSubjTeach->id
                        );

//                    print_r($recursiveParam);exit;

                        $res = MarkController::_calculateMarksToLinkedExamsNEW($recursiveParam);
                    }
                    if ($res == 1) {
                        $message['type'] = 'success';
                        $message['message'] = 'Successfully updated';
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    $message['type'] = 'success';
                    $message['message'] = 'Successfully updated';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function updateClassTestTotMarkAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $res = 1;
            $test_id = $this->request->getPost('test_id');
            $mark = $this->request->getPost('mark');
            $total_mark = $this->request->getPost('total_mark');
            $record = ClassTestMarks::find('class_test_id = ' . $test_id);
            $linkedReccord = ClassTest::findFirstByClassTestId($test_id);
            $grpSubjTeach = GroupSubjectsTeachers::findFirstById($linkedReccord->grp_subject_teacher_id);
            if (count($record) > 0) {
                foreach ($record as $stumark) {

                    $stumark->class_test_id = $test_id;
                    $stumark->outof = $total_mark;
                    if (!$stumark->save()) {
                        $err = 1;
                        foreach ($record->getMessages() as $message) {
                            $error .= $message;
                            echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        }
                    } else {

                        if ($linkedReccord && $linkedReccord->mainexam_id) {
                            $recursiveParam = array(
                                'stuId' => $stumark->student_id,
                                'x2' => $linkedReccord->mainexam_id,
                                'obtainedMarks' => '',
                                'subject_id' => $grpSubjTeach->subject_id,
                                'obtainedOutOf' => '',
                                'clastst_combi_id' => $grpSubjTeach->id
                            );
//                    print_r($recursiveParam);exit;
                            $res = $this->_calculateMarksToLinkedExamsNEW($recursiveParam);
                        }
                    }
                }
            }

            if ($res == 1) {
                $message['type'] = 'success';
                $message['message'] = 'Successfully updated';
                print_r(json_encode($message));
            }
        }
    }

    public function indexAction() {
        $this->tag->prependTitle("Marks | ");
        $this->assets->addCss('css/edustyle.css');
    }

    public function ClassTestMarkAction() {
        $this->tag->prependTitle("Class Test Mark| ");
        $this->assets->addJs("js/mark/mark.js");

        $cacdyr = ControllerBase::get_current_academic_year();
        $div = DivisionMaster::findfirst('divType = "student" and academicYrID=' . $cacdyr);
        $identity = $this->auth->getIdentity();
        if (in_array('Staff', $identity['role_name'])) {
            $this->view->staff = $staff = StaffInfo::findFirstByLoginid($identity['name']);
            $combinations = array();

            $assignedCount = SubjectTeachers::find('teacher_id = ' . $staff->id);
            if (count($assignedCount) > 0) {
                foreach ($assignedCount as $assignedclass) {
                    $getCombinations = SubDivisionSubjects::find('id=' . $assignedclass->subdivisions_subjects_id);
                    foreach ($getCombinations as $formCombination) {
                        $division = DivisionValues::findFirstById($formCombination->div_val_id)->classname;
                        $subdivname = array();
                        $subdivision = explode(',', $formCombination->concat_subdivs);
                        foreach ($subdivision as $svalue) {
                            $subdivname[] = SubDivisionValues::findFirstById($svalue)->name;
                        }
                        $subdivnames = implode(' - ', $subdivname);
                        $subjects = SubjectsDivision::findFirstById($formCombination->subject_id)->subjectName;
                        $combinations[$formCombination->id] = $division . ' - ' . $subdivnames . ' - ' . $subjects;
                    }
                }
                $this->view->combinations = $combinations;
                $this->view->divname = ControllerBase::get_division_name_student();
            } else {
                $this->view->errorMessage = 'No Subjects assigned for you yet!';
            }
        } else {
            $this->view->errorMessage = 'Only teachers have access to this page!';
        }
    }

    public function loadSectionsByClassAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_subdiv = 0;
        if ($this->request->isPost()) {
            $this->view->classId = $this->request->getPost('classId');

            $this->view->subdivname = ControllerBase::get_sub_division_name_student();
            $subdivvals = ControllerBase::get_sub_division_values_student($this->view->classId);

            if (count($subdivvals)) {
                $this->view->subdivVal = $subdivvals;
                $this->view->display_subdiv = 1;
            }
        }
    }

    public function loadSubjectsBySectionAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_subject = 0;
        if ($this->request->isPost()) {
            $classId = $this->request->getPost('class_id');

            $subdivid = $this->request->getPost('subDivVal');

            //fetch the subjects
            $subjects = ControllerBase::get_subjects($classId, $subdivid);

            if (count($subjects)) {
                $this->view->display_subject = 1;
                $this->view->subjects = $subjects;
            }
        }
    }

    public function loadClassTestAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display = 0;
        if ($this->request->isPost()) {

            $subject_masterid = $this->request->getPost('subject_masterid');

            //Get subject id from subject_masterid
            $subject = SubDivisionSubjects::findfirst('id = ' . $subject_masterid);

            $cacdyr = ControllerBase::get_current_academic_year();

//            //Get subject teachers
//            $subject_teachers = SubjectTeachers::find('subdivisions_subjects_id = ' . $subject_masterid . ' and academicYrId = ' . $cacdyr);
//
//
//            if (count($subject_teachers) == 1) {
//
//                $login = StaffInfo::findFirst('id = ' . $subject_teachers[0]->teacher_id)->loginid;
//                $teach_id = Users::findfirst('login = "' . $login . '"')->id;
//                //Load class test by subject id
//                $classtests = ClassTest::find('subject_master_id =' . $subject_id .
//                                'and academic_year_id =' . $cacdyr . ' and created_by = ' . $teach_id);
//            } else {
//                $subteacherids = array();
//                foreach ($subject_teachers as $subject_teacher) {
//                    $login = StaffInfo::findFirst('id = ' . $subject_teacher->teacher_id)->loginid;
//                    $teach_id = Users::findfirst('login = "' . $login . '"')->id;
//                    $subteacherids[] = $teach_id;
//                }
//                $subteacherid = implode(',', array_unique($subteacherids));
//                $classtests = ClassTest::find('subject_master_id =' . $subject_id .
//                                'and academic_year_id =' . $cacdyr . ' and created_by IN (' . $subteacherid . ')');
//            }

            $classtests = ClassTest::find('subject_master_id =' . $subject->subject_id .
                            'and academic_year_id =' . $cacdyr . ' and div_val_id = ' . $subject->div_val_id .
                            ' and concat_subdivs = "' . $subject->concat_subdivs . '"');
            if (count($classtests) > 0) {
                $this->view->display = 1;
                foreach ($classtests as $classtest) {
                    $test[$classtest->class_test_id] = $classtest->class_test_name;
                }
            }

            $this->view->classtests = $test;
            $this->view->divname = ControllerBase::get_division_name_student();
        }
    }

    public function updateMainExamMarkAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {

            $test_id = $this->request->getPost('test_id');
            $student_id = $this->request->getPost('student_id');
            $mark = $this->request->getPost('mark');
            $record = MainexamMarks::findFirst('mainexam_id = ' . $test_id . 'and student_id = ' . $student_id) ?
                    MainexamMarks::findFirst('mainexam_id = ' . $test_id . 'and student_id = ' . $student_id) :
                    new MainexamMarks();

            $linkedReccord = Mainexam::findFirstById($test_id);
            $record->inherited_marks = $record->inherited_marks ? $record->inherited_marks : 0;
            $record->mainexam_id = $test_id;
            $record->student_id = $student_id;
            $record->marks = $mark;

            if (!$record->save()) {
                foreach ($record->getMessages() as $message) {
                    $error .= $message;
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                }
            } else {

                if ($linkedReccord && $linkedReccord->mainexam_id) {
                    $recursiveParam = array(
                        'type' => 'main',
                        'stuId' => $student_id,
                        'x1' => $test_id,
                        'x2' => $linkedReccord->mainexam_id,
                        'obtainedMarks' => $record->inherited_marks + $mark
                    );

                    $this->_calculateMarksToLinkedExamsNEW($recursiveParam);
                }
            }
        }
    }

    public function _calculateMarksToLinkedExams($params) {
        try {
            $initType = $params['type']; //class test or mainexam
            $studentId = $params['stuId']; //student Id
            $examOneId = $params['x1']; //updated test Id
            $examTwoId = $params['x2']; // linked test Id
            $ObtainedMarks = $params['obtainedMarks']; //inherited+entered   
            $combiId = $params['CombiId'] ? $params['CombiId'] : ''; //combination master Id
            $obtainedOutOf = $params['obtainedOutOf'] ? $params['obtainedOutOf'] : 0; //inherited+entered   

            if ($initType == 'class') {
                $examOne = ClassTest::findFirstByClassTestId($examOneId); //updated test         
                $examOneMark = ClassTestMarks::findFirst('class_test_id = ' . $examOneId . ' and student_id = ' . $studentId) ?
                        ClassTestMarks::findFirst('class_test_id = ' . $examOneId . ' and student_id = ' . $studentId) :
                        new ClassTestMarks();
            } else {
                $examOne = Mainexam::findFirstById($examOneId); //updated test 
                $examOneMark = MainexamMarks::findFirst('mainexam_id = ' . $examOneId .
                                ' and student_id = ' . $studentId .
                                ' and combinationId = ' . $combiId) ?
                        MainexamMarks::findFirst('mainexam_id = ' . $examOneId .
                                ' and student_id = ' . $studentId .
                                ' and combinationId = ' . $combiId) :
                        new MainexamMarks();
            }
            $examTwo = Mainexam::findFirstById($examTwoId); // linked test
            $examTwoMark = MainexamMarks::findFirst('mainexam_id = ' . $examTwoId .
                            ' and student_id = ' . $studentId .
                            ' and combinationId = ' . $combiId) ?
                    MainexamMarks::findFirst('mainexam_id = ' . $examTwoId .
                            ' and student_id = ' . $studentId .
                            ' and combinationId = ' . $combiId) :
                    new MainexamMarks();
            $mainexoldMark = ((($examOneMark->marks ? $examOneMark->marks : 0) +
                    ($examOneMark->inherited_marks ? $examOneMark->inherited_marks : 0)) *
                    $examOne->mainexam_outof) / (($examOneMark->outof ? $examOneMark->outof : 0) +
                    ($examOneMark->inherited_outof ? $examOneMark->inherited_outof : 0));
            $oldCalMark = ($initType == 'class') ? ( ($examOneMark->outof ? ((($examOneMark->marks ? $examOneMark->marks : 0) *
                            $examOne->mainexam_outof) / ($examOneMark->outof ? $examOneMark->outof : 0)) : 0)) :
                    ($mainexoldMark ? $mainexoldMark : 0);

            $newCalMark = ($initType == 'class') ?
                    (($ObtainedMarks * $examOne->mainexam_outof) / $obtainedOutOf) :
                    (($ObtainedMarks * $examOne->mainexam_outof) / $obtainedOutOf);

            $mainoldoutOF = (($examOneMark->outof ? $examOneMark->outof : 0) +
                    ($examOneMark->inherited_outof ? $examOneMark->inherited_outof : 0));
            $OldOutOf = ($initType == 'class') ? ($examOneMark->outof ? $examOneMark->outof : 0) : $mainoldoutOF;
            $OldnheritedOutOf = ($OldOutOf != 0) ? $examOne->mainexam_outof : 0;
            $InheritedOutOf = $examOne->mainexam_outof;
//            echo $ObtainedMarks.' / '.$obtainedOutOf.'sO'.$oldCalMark.' / '.$OldnheritedOutOf.'sN'.$newCalMark.' / '.$InheritedOutOf.'<br>';
//            exit;
            if ($examTwo->mainexam_id) {

                $recursiveParam = array(
                    'type' => 'main',
                    'stuId' => $studentId,
                    'x1' => $examTwoId,
                    'x2' => $examTwo->mainexam_id,
                    'obtainedMarks' => (($examTwoMark->inherited_marks ? $examTwoMark->inherited_marks : 0) - $oldCalMark + $newCalMark),
                    'obtainedOutOf' => ($examTwoMark->inherited_outof ? $examTwoMark->inherited_outof : 0) - $OldnheritedOutOf + $InheritedOutOf,
                    'CombiId' => $combiId
                );
                $this->_calculateMarksToLinkedExams($recursiveParam);
                $examTwoMark->inherited_marks = (($examTwoMark->inherited_marks ? $examTwoMark->inherited_marks : 0) - $oldCalMark + $newCalMark);
                $examTwoMark->mainexam_id = $examTwoId;
                $examTwoMark->student_id = $studentId;
                $examTwoMark->marks = $examTwoMark->marks ? $examTwoMark->marks : 0;
                $examTwoMark->inherited_outof = ($examTwoMark->inherited_outof ? $examTwoMark->inherited_outof : 0) - $OldnheritedOutOf + $InheritedOutOf;
                $examTwoMark->outof = $examTwoMark->outof ? $examTwoMark->outof : 0;
                $examTwoMark->combinationId = $combiId;
                if (!$examTwoMark->save()) {
                    return $examTwoMark->getMessage();
                }
            } else {
                $examTwoMark->inherited_marks = ($examTwoMark->inherited_marks - $oldCalMark + $newCalMark);
                $examTwoMark->mainexam_id = $examTwoId;
                $examTwoMark->student_id = $studentId;
                $examTwoMark->marks = $examTwoMark->marks ? $examTwoMark->marks : 0;
                $examTwoMark->inherited_outof = ($examTwoMark->inherited_outof ? $examTwoMark->inherited_outof : 0) - $OldnheritedOutOf + $InheritedOutOf;
                $examTwoMark->outof = $examTwoMark->outof ? $examTwoMark->outof : 0;
                $examTwoMark->combinationId = $combiId;
                if (!$examTwoMark->save()) {
                    return $examTwoMark->getMessage();
                }
            }
            return true;
        } catch (Exception $ex) {

            return $ex->getMessage();
        }
    }

    public function addMainExamMarkAction() {
        $this->tag->prependTitle("Add Main Exam | ");
        $this->assets->addJs("js/mark/mark.js");
        $cacdyr = AcademicYearMaster::findFirstByStatus('c');
        $param['type'] = 'mainexam_mark';
        $getCombination = ControllerBase::loadCombinations($param);
        $this->view->combinations = $getCombination[1];
        $this->view->type = $getCombination[2];
        $this->view->errorMessage = $getCombination[0];
    }

    /* public function getExamListAction() {
      $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
      if ($this->request->isPost()) {
      $master_id = $this->request->getPost('masterid');
      $teacherType = $this->request->getPost('type');
      $current_acd_yr = Controllerbase::get_current_academic_year();
      if ($teacherType != 'classTeacher') {
      $master_id = SubDivisionSubjects::findFirstById($master_id);
      $this->view->mainexams = Mainexam::find('div_val_id =' . $master_id->div_val_id .
      ' and academic_year_id = ' . $current_acd_yr);
      }
      }
      } */

    public function loadMainExamAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_mainexams = 1;
        if ($this->request->isPost()) {
            $classId = $this->request->getPost('class_id');
            $subject_id = $this->request->getPost('subject_id');
            $cacdyr = ControllerBase::get_current_academic_year();


            $subdivid = $this->request->getPost('subDivVal');

            $records = Mainexam::find('div_val_id =' . $classId . ' and sub_div_val_id='
                            . $subdivid . ' and sub_div_subjects_id = ' . $subject_id . ' and academic_year_id = ' . $cacdyr);

            if (count($records) > 0) {
                foreach ($records as $record) {
                    $tests[$record->id] = $record->exam_name;
                }
                $this->view->tests = $tests;
            } else {
                $this->view->display_mainexams = 0;
            }
        }
    }

    public function loadMainExamStudentsAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_students = 1;
        if ($this->request->isPost()) {
            $classId = $this->request->getPost('class_id');
            $classname = $this->request->getPost('class_name');
            $subject_id = $this->request->getPost('subject_id');
            $cacdyr = AcademicYearMaster::findFirstByStatus('c')->id;


            $subdivid = $this->request->getPost('subDivVal');
            $subdivname = $this->request->getPost('subdiv_name');
            $test_id = $this->request->getPost('test_id');

            //Find the mainexams list which refers to the current main exam selected
            $refering_mainexams = Mainexam::find('mainexam_id = ' . $test_id);
            $count_refering_mainexams = count($refering_mainexams);
            $this->view->count_refering_mainexams = $count_refering_mainexams;

            if ($count_refering_mainexams > 0) {
                $this->view->refering_main_exam = 1;
                $i = 0;
                foreach ($refering_mainexams as $refering_exam) {

                    //For each such refering exam extract id, exam name and weightage
                    $referal_mainexam_metadata[$i++] = array("id" => $refering_exam->id,
                        "name" => $refering_exam->exam_name,
                        "weightage" => $refering_exam->mainexam_outof,
                        "outof" => $refering_exam->total_marks);
                    //echo '<div class="alert alert-block alert-danger fade in">' .implode(" ",$referal_metadata[$i-1]); '</div>';
                }
                $this->view->referal_mainexams = $referal_mainexam_metadata;
            }

            //Find the classtest list which refers to the current main exam selected
            $refering_classtests = ClassTest::find('mainexam_id = ' . $test_id);
            $count_refering_classtests = count($refering_classtests);
            $this->view->count_refering_classtests = $count_refering_classtests;

            if ($count_refering_classtests > 0) {
                $i = 0;
                foreach ($refering_classtests as $refering_exam) {

                    //For each such refering exam extract id, exam name and weightage
                    $referal_classtest_metadata[$i++] = array("id" => $refering_exam->class_test_id,
                        "name" => $refering_exam->class_test_name,
                        "weightage" => $refering_exam->mainexam_outof,
                        "outof" => $refering_exam->total_marks);
                    //echo '<div class="alert alert-block alert-danger fade in">' .implode(" ",$referal_classtest_metadata[$i-1]); '</div>';
                }
                $this->view->referal_classtests = $referal_classtest_metadata;
            }




            //Total marks
            $mainexam = Mainexam::findFirstById($test_id);
            $total_mark = $mainexam->total_marks;
            if (is_null($mainexam->mainexam_id)) {
                $this->view->message = "This Main Exam Is Not Contributing To Any Other Main Exam";
            } else {
                //echo '<div class="alert alert-block alert-danger fade in">'.$classtest->mainexam_id.'</div>';
                $mainexam_name = Mainexam::findFirstById($mainexam->mainexam_id)->exam_name;

                $this->view->message = "This Main Exam Is Contributing To Main Exam: '" . $mainexam_name . "' And The Weightage Assigned to '" .
                        $mainexam_name . "' Is " . $mainexam->mainexam_outof . " Marks";
            }

            //List of students
            $students = StudentInfo::find('Division_Class = "' . $classId . '" and Subdivision_section = "' .
                            $subdivid . '"');

            //echo '<div class="alert alert-block alert-danger fade in">'.count($students).'</div>';
            $referal_mainexam_marks = array();
            $referal_classtest_marks = array();
            $i = 0;
            if (count($students) > 0) {

                foreach ($students as $student) {
                    $displaymark = "";
                    if ($count_refering_mainexams > 0) {
                        $j = 0;
                        foreach ($referal_mainexam_metadata as $ref) {
                            $marks = MainexamMarks::find('mainexam_id = ' . $ref['id'] . 'and student_id = ' . $student->id);
                            if (count($marks) > 0) {
                                $referal_mainexam_marks[$j++] = (($marks[0]->marks + $marks[0]->inherited_marks) / $ref['outof']) * $ref['weightage'];
                            } else {
                                $referal_mainexam_marks[$j++] = "Yet To Be Evaluated";
                            }
                            //echo '<div class="alert alert-block alert-danger fade in">' .$referal_marks[$j-1].'</div>';
                        }
                    }

                    if ($count_refering_classtests > 0) {
                        $k = 0;
                        foreach ($referal_classtest_metadata as $ref) {
                            $marks = ClassTestMarks::find('class_test_id = ' . $ref['id'] . 'and student_id = ' . $student->id);

                            if (count($marks) > 0) {
                                //echo '<div class="alert alert-block alert-danger fade in">'. $ref['outof']." ".$ref['weightage'].'</div>';
                                $referal_classtest_marks[$k++] = (($marks[0]->marks + $marks[0]->inherited_marks) / $ref['outof']) * $ref['weightage'];
                            } else {
                                $referal_classtest_marks[$k++] = "Yet To Be Evaluated";
                            }
                            //echo '<div class="alert alert-block alert-danger fade in">' .$referal_classtest_marks[$k-1].'</div>';
                        }
                    }

                    $testmark = MainexamMarks::find('mainexam_id = ' . $test_id . 'and student_id = ' . $student->id);
                    if (count($testmark) > 0) {
                        $displaymark = $testmark[0]->marks;
                    }
                    $rows[$i++] = array($student->Student_Name, $student->id, $displaymark, $referal_mainexam_marks, $referal_classtest_marks);
                    //echo '<div class="alert alert-block alert-danger fade in">' .$student->id. '</div>';
                }

                $this->view->total_mark = $total_mark;
                $this->view->rows = $rows;
            } else {
                $this->view->display_students = 0;
            }
        }
    }

    public function viewMainExamMarkAction() {

        $this->tag->prependTitle("View Main Exam | ");
        $this->assets->addJs("js/mark/mark.js");


        $this->view->divname = ControllerBase::get_division_name_student();
        $this->view->div_val = ControllerBase::get_division_values_student(ControllerBase::get_current_academic_year());
    }

    public function viewMainExamStudentsAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_students = 1;
        if ($this->request->isPost()) {
            $classId = $this->request->getPost('class_id');
            $classname = $this->request->getPost('class_name');
            $subject_id = $this->request->getPost('subject_id');
            $cacdyr = AcademicYearMaster::findFirstByStatus('c')->id;


            $subdivid = $this->request->getPost('subDivVal');
            $subdivname = $this->request->getPost('subdiv_name');
            $test_id = $this->request->getPost('test_id');

            //Find the mainexams list which refers to the current main exam selected
            $refering_mainexams = Mainexam::find('mainexam_id = ' . $test_id);
            $count_refering_mainexams = count($refering_mainexams);
            $this->view->count_refering_mainexams = $count_refering_mainexams;

            if ($count_refering_mainexams > 0) {
                $this->view->refering_main_exam = 1;
                $i = 0;
                foreach ($refering_mainexams as $refering_exam) {

                    //For each such refering exam extract id, exam name and weightage
                    $referal_mainexam_metadata[$i++] = array("id" => $refering_exam->id,
                        "name" => $refering_exam->exam_name,
                        "weightage" => $refering_exam->mainexam_outof,
                        "outof" => $refering_exam->total_marks);
                    //echo '<div class="alert alert-block alert-danger fade in">' .implode(" ",$referal_metadata[$i-1]); '</div>';
                }
                $this->view->referal_mainexams = $referal_mainexam_metadata;
            }

            //Find the classtest list which refers to the current main exam selected
            $refering_classtests = ClassTest::find('mainexam_id = ' . $test_id);
            $count_refering_classtests = count($refering_classtests);
            $this->view->count_refering_classtests = $count_refering_classtests;

            if ($count_refering_classtests > 0) {
                $i = 0;
                foreach ($refering_classtests as $refering_exam) {

                    //For each such refering exam extract id, exam name and weightage
                    $referal_classtest_metadata[$i++] = array("id" => $refering_exam->class_test_id,
                        "name" => $refering_exam->class_test_name,
                        "weightage" => $refering_exam->mainexam_outof,
                        "outof" => $refering_exam->total_marks);
                    //echo '<div class="alert alert-block alert-danger fade in">' .implode(" ",$referal_classtest_metadata[$i-1]); '</div>';
                }
                $this->view->referal_classtests = $referal_classtest_metadata;
            }




            //Total marks
            $mainexam = Mainexam::findfirst('id = ' . $test_id);
            $total_mark = $mainexam->total_marks;
            if (is_null($mainexam->mainexam_id)) {
                $this->view->message = "This Main Exam Is Not Contributing To Any Other Main Exam";
            } else {
                //echo '<div class="alert alert-block alert-danger fade in">'.$classtest->mainexam_id.'</div>';
                $mainexam_name = Mainexam::findfirst('id = ' . $mainexam->mainexam_id)->exam_name;

                $this->view->message = "This Main Exam Is Contributing To Main Exam: '" . $mainexam_name . "' And The Weightage Assigned to '" .
                        $mainexam_name . "' Is " . $mainexam->mainexam_outof . " Marks";
            }

            //List of students
            $students = StudentInfo::find('Division_Class = "' . $classId . '" and Subdivision_section = "' .
                            $subdivid . '"');

            //echo '<div class="alert alert-block alert-danger fade in">'.count($students).'</div>';
            $referal_mainexam_marks = array();
            $referal_classtest_marks = array();
            $i = 0;
            if (count($students) > 0) {

                foreach ($students as $student) {
                    $displaymark = "Not Evaluated";
                    if ($count_refering_mainexams > 0) {
                        $j = 0;
                        foreach ($referal_mainexam_metadata as $ref) {
                            $marks = MainexamMarks::find('mainexam_id = ' . $ref['id'] . 'and student_id = ' . $student->id);
                            if (count($marks) > 0) {
                                $referal_mainexam_marks[$j++] = (($marks[0]->marks + $marks[0]->inherited_marks) / $ref['outof']) * $ref['weightage'];
                            } else {
                                $referal_mainexam_marks[$j++] = "Not Evaluated";
                            }
                            //echo '<div class="alert alert-block alert-danger fade in">' .$referal_marks[$j-1].'</div>';
                        }
                    }

                    if ($count_refering_classtests > 0) {
                        $k = 0;
                        foreach ($referal_classtest_metadata as $ref) {
                            $marks = ClassTestMarks::find('class_test_id = ' . $ref['id'] . 'and student_id = ' . $student->id);

                            if (count($marks) > 0) {
                                //echo '<div class="alert alert-block alert-danger fade in">'. $ref['outof']." ".$ref['weightage'].'</div>';
                                $referal_classtest_marks[$k++] = (($marks[0]->marks + $marks[0]->inherited_marks) / $ref['outof']) * $ref['weightage'];
                            } else {
                                $referal_classtest_marks[$k++] = "Not Evaluated";
                            }
                            //echo '<div class="alert alert-block alert-danger fade in">' .$referal_classtest_marks[$k-1].'</div>';
                        }
                    }

                    $testmark = MainexamMarks::find('mainexam_id = ' . $test_id . 'and student_id = ' . $student->id);
                    if (count($testmark) > 0) {
                        $displaymark = $testmark[0]->marks;
                    }
                    $rows[$i++] = array($student->Student_Name, $student->id, $displaymark, $referal_mainexam_marks, $referal_classtest_marks);
                    //echo '<div class="alert alert-block alert-danger fade in">' .$student->id. '</div>';
                }

                $this->view->total_mark = $total_mark;
                $this->view->rows = $rows;
            } else {
                $this->view->display_students = 0;
            }
        }
    }

    public function _calculateMarksToLinkedExamsNEW($params) {
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        try {
            $studentId = $params['stuId']; //student Id
            $examTwoId = intval($params['x2']); // linked test Id
            $combiId = $params['subject_id'] ? $params['subject_id'] : ''; //subject_id  
            $clastst_combi_id = $params['clastst_combi_id'] ? $params['clastst_combi_id'] : ''; //clastst_combi_id  
            $ObtainedMarks = $params['obtainedMarks']; //entered   
            $obtainedOutOf = $params['obtainedOutOf']; //entered  
//            $examTwo = Mainexam::findFirstById($examTwoId); // linked test

            $examTwoMark = MainexamMarks::findFirst('mainexam_id = ' . $examTwoId .
                            ' and student_id = ' . $studentId .
                            ' and subject_id = ' . $combiId .
                            ' and grp_subject_teacher_id = ' . $clastst_combi_id) ?
                    MainexamMarks::findFirst('mainexam_id = ' . $examTwoId .
                            ' and student_id = ' . $studentId .
                            ' and subject_id = ' . $combiId .
                            ' and grp_subject_teacher_id = ' . $clastst_combi_id) :
                    new MainexamMarks();

//        print_r(($obtainedOutOf >= 0 && $obtainedOutOf!='') ? $obtainedOutOf : ($examTwoMark->outof ? $examTwoMark->outof : 0) );exit;
//echo $obtainedOutOf .'-';
//echo ($examTwoMark->marks ? $examTwoMark->marks : 0);

            $mark = (($ObtainedMarks != '') && ($ObtainedMarks >= 0)) ? $ObtainedMarks : ($examTwoMark->marks ? $examTwoMark->marks : 0);
            $calcOutOf = ($obtainedOutOf >= 0 && $obtainedOutOf != '') ? $obtainedOutOf : ($examTwoMark->outof ? $examTwoMark->outof : 0);
//            echo $mark.'/'.$calcOutOf.'<br>';exit;
            if ((floatval($calcOutOf) < floatval($mark))) {
                $message['type'] = 'error';
                $message['message'] = 'Student Mark exceeds total marks';
                print_r(json_encode($message));
                exit;
            }

            $linkedmainIheritedTotal = 0;
            $linkedmainIheritedTOutOf = 0;
            $linkedmainrefOutOf = 0;

            $condition = "subject_id = $combiId "
                    . " AND mainexm_id = $examTwoId"
                    . ' and grp_subject_teacher_id = ' . $clastst_combi_id;
            $formula = FormulaTable::findFirst($condition);
            $exam_ids = $formula->exam_ids ? explode(',', $formula->exam_ids) : '';
            $merge_exam_val = array();

            foreach ($exam_ids as $exam_id) {
                $expldval = explode('_', $exam_id);
                if ($expldval[0] == 'M') {
                    $refmainmarks = MainexamMarks::findFirst('mainexam_id = ' . $expldval[1] .
                                    ' and student_id = ' . $studentId .
                                    ' and subject_id = ' . $combiId .
                                    ' and grp_subject_teacher_id = ' . $clastst_combi_id) ?
                            MainexamMarks::findFirst('mainexam_id = ' . $expldval[1] .
                                    ' and student_id = ' . $studentId .
                                    ' and subject_id = ' . $combiId .
                                    ' and grp_subject_teacher_id = ' . $clastst_combi_id) :
                            new MainexamMarks();

                    if ($refmainmarks->mainexam_marks_id > 0) {
                        $linkedmainIheritedTotal = (($refmainmarks->marks ? $refmainmarks->marks : 0) +
                                ($refmainmarks->inherited_marks ? $refmainmarks->inherited_marks : 0));
                        $linkedmainIheritedTOutOf = (($refmainmarks->outof ? $refmainmarks->outof : 0) +
                                ($refmainmarks->inherited_outof ? $refmainmarks->inherited_outof : 0));
                        $linkedmainrefOutOf = $mainrefex->mainexam_outof;

                        $merge_exam_val[] = $inhertMarkCalc = (($linkedmainIheritedTotal / $linkedmainIheritedTOutOf));
                    }
                }
                if ($expldval[0] == 'C') {
                    $reftest = ClassTest::findFirstByClassTestId($expldval[1]);
                    $reftestmarks = ClassTestMarks::findFirst('class_test_id = ' . $expldval[1] .
                                    ' and student_id = ' . $studentId) ?
                            ClassTestMarks::findFirst('class_test_id = ' . $expldval[1] .
                                    ' and student_id = ' . $studentId) :
                            new ClassTestMarks();
                    if ($reftestmarks->classtest_marks_id > 0) {
                        $linkedmainIheritedTotal = ($reftestmarks->marks ? $reftestmarks->marks : 0);
                        $linkedmainIheritedTOutOf = ($reftestmarks->outof ? $reftestmarks->outof : 0);
                        $linkedmainrefOutOf = $reftest->mainexam_outof;

                        $merge_exam_val[] = $inhertMarkCalc = (($linkedmainIheritedTotal / $linkedmainIheritedTOutOf));
                    }
                }
                if ($expldval[0] == 'A') {
                    $refassign = AssignmentsMaster::findFirstById($expldval[1]);
                    $refassmarks = AssignmentMarks::findFirst('assignment_id = ' . $expldval[1] .
                                    ' and student_id = ' . $studentId) ?
                            AssignmentMarks::findFirst('assignment_id = ' . $expldval[1] .
                                    ' and student_id = ' . $studentId) :
                            new AssignmentMarks();
                    if ($refassmarks->id > 0) {
                        $linkedmainIheritedTotal = ($refassmarks->marks ? $refassmarks->marks : 0);
                        $linkedmainIheritedTOutOf = ($refassmarks->outof ? $refassmarks->outof : 0);
                        $linkedmainrefOutOf = $refassign->mainexam_outof;
                        $merge_exam_val[] = $inhertMarkCalc = (($linkedmainIheritedTotal / $linkedmainIheritedTOutOf));
                    }
                }
            }

            $expld_val = explode('-', $formula->formula);
//            echo '<pre>';
//                    print_r($merge_exam_val );
//exit;
//            $merge_val =  array_merge($mainexm, $class_test, $assignment);
            if ($expld_val[0] == 'Max') {
                $ma = max($merge_exam_val);
                $inhertMarkCalc = $ma * $formula->outof;
            } else if ($expld_val[0] == 'Sum') {
                $ma = array_sum($merge_exam_val);
                $inhertMarkCalc = $ma * $formula->outof / count($merge_exam_val);
            } else if ($expld_val[0] == 'Avg') {
                $ma = array_sum($merge_exam_val);
                $inhertMarkCalc = $ma * $formula->outof / count($merge_exam_val);
            } else if ($expld_val[0] == 'Bestof') {
                $sorted = arsort($merge_exam_val);
                $ma = array_slice($merge_exam_val, 0, $expld_val[1]);
                $cnt = count($ma);
                $ma = array_sum($ma);
                $inhertMarkCalc = $ma * $formula->outof / $cnt;
            }
//            echo $ma;
//                    print_r($inhertMarkCalc );
//exit;
//  print_r($examTwoMark->marks  .'-test');
//exit;
            if (($inhertMarkCalc >= 0 || $examTwoMark->marks >= 0 || $mark >= 0 || $calcOutOf >= 0 || $examTwoMark->inherited_marks >= 0 || $examTwoMark->outof >= 0)) {
                $examTwoMark->mainexam_id = $examTwoId;
                $examTwoMark->student_id = $studentId;
                $examTwoMark->marks = $mark;
                $examTwoMark->outof = round($calcOutOf, 2);
                $examTwoMark->inherited_marks = $inhertMarkCalc ? $inhertMarkCalc : 0;
                $examTwoMark->inherited_outof = $formula->outof ? $formula->outof : 0;
                $examTwoMark->subject_id = $combiId;
                $examTwoMark->grp_subject_teacher_id = $clastst_combi_id;
                $examTwoMark->createdby = $uid;
                $examTwoMark->createdon = time();
                $examTwoMark->modifiedby = $uid;
                $examTwoMark->modifiedon = time();

//                echo $mark;
//                echo 'tru';exit;
//                print_r($examTwoMark->student_id.' : '.$examTwoMark->marks);
//                 print_r($examTwoMark->outof );
                if (!$examTwoMark->save()) {
//                print_r($examTwoMark);exit;
                    print_r($examTwoMark->getMessages());
                    exit;
                    return $examTwoMark->getMessages();
                } else {
                    return 1;
                }
            }
        } catch (Exception $ex) {

            return $ex;
        }
    }

    public function loadClassTestStudentsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_students = 1;
        if ($this->request->isPost()) {
            $this->view->test_id = $test_id = $this->request->getPost('test_id');
            $master_id = $this->request->getPost('subject_masterid');

            $classtest = ClassTest::findfirst('class_test_id = ' . $test_id);
            $grpSubjTeach = GroupSubjectsTeachers::findFirstById($master_id);

            $this->view->message = 'Update marks for classtest ' . $classtest->class_test_name . '<br>';
            $this->view->grp_subject_teacher_id = $master_id;

            $classmaster = ClassroomMaster::findFirstById($grpSubjTeach->classroom_master_id);
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);

//            $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
//         echo '<pre>';   print_r($res);exit;
//            $orgvaldet = OrganizationalStructureValues::findFirstById($grpSubjTeach->subject_id);
//            $subj_arr = $this->_getMandNodesForExam($orgvaldet);
//            $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", stumap.aggregate_key)>0)', array_keys($subj_arr));
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key, stumap.subordinate_key,stumap.status'
                    . ' ,stuinfo.Gender,stuinfo.photo,stuinfo.rollno,stuinfo.Student_Name '
                    . ' FROM StudentMapping stumap '
                    . ' LEFT JOIN StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id '
                    . ' WHERE stumap.status = "Inclass" '
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                    . ' and (' . implode(' or ', $res) . ') '
//                    . ' and (' . implode(' and ', $subjQuery) . ') '
                    . ' ORDER BY stuinfo.Admission_no ASC';

//               print_r($stuquery);exit;
            $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);

            $i = 0;
            if (count($students) > 0) {
                foreach ($students as $student) {
                    $displaymark = "";
                    $testmark = ClassTestMarks::findfirst('class_test_id = ' . $test_id . ' and student_id = ' . $student->id);
//                    $sturollno = StudentInfo::findfirst('id = ' . $student->student_info_id);
                    if ($testmark) {
                        $displaymark = $testmark->marks;
                        $out_of = $testmark->outof;
                    }
                    $gender = ($student->Gender == 1 ? 'Female' : 'Male');
                    $rows[$i++] = array(
                        $student->Student_Name,
                        $student->student_info_id,
                        $displaymark,
                        $out_of,
                        $student->rollno,
                        $student->photo,
                        $gender);
                }
                $this->view->rows = $rows;
            } else {
                $this->view->display_students = 0;
            }
        }
    }

    public function getClassTestListByCombinationAction($params) {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

//        $student_id = '82'; //$params['student_id'];//
//        $master_id = '1'; //$params['master_id'];//
//        $academicYrId = '1'; //$params['academic_year_id'];//
//        print_r($master_id);exit;
        if ($this->request->isPost()) {

            $master_id = $this->request->getPost('subject_masterid'); //'1'; //
            $this->view->master_id = $master_id;

            $classtestCombi = GroupSubjectsTeachers::findFirstById($master_id);

            $subjects_mas = OrganizationalStructureMaster::find(array(
                        'columns' => 'GROUP_CONCAT(id) as ids',
                        ' module = "Subject" and is_subordinate = 1 '
            ));
            $subidsall = HomeWorkController::_getAllChildren($classtestCombi->subject_id, $subjects_mas[0]->ids);

            $classmaster = ClassroomMaster::findFirstById($classtestCombi->classroom_master_id);

            $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);

            $orgvaldet = OrganizationalStructureValues::findFirstById($classtestCombi->subject_id);
            $subj_arr = $this->_getMandNodesForExam($orgvaldet);
            $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", stumap.aggregate_key)>0)', array_keys($subj_arr));
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key, stumap.subordinate_key,stumap.status'
                    . ' FROM StudentMapping stumap '
                    . ' LEFT JOIN StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id '
                    . ' WHERE stumap.status = "Inclass" '
                    . ' and (' . implode(' or ', $res) . ') '
                    . ' and (' . implode(' and ', $subjQuery) . ') '
                    . ' ORDER BY stuinfo.Admission_no ASC';

            $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);

            $i = 0;
            $cosolidatedMarks = $overaloutof = array();
            if (count($students) > 0) {
                foreach ($students as $student) {
                    $classTests = ClassTest::find('grp_subject_teacher_id =' . $master_id . ' AND subjct_modules IN (' . implode(',', $subidsall) . ')');
                    foreach ($classTests as $classTest) {
                        $clsTstMarks = ClassTestMarks::findFirst('class_test_id = ' . $classTest->class_test_id
                                        . ' and student_id = ' . $student->student_info_id);
                        $obtainedmark = ($clsTstMarks->marks) ? $clsTstMarks->marks : 0;
                        $obtainedOutOf = ($clsTstMarks->outof) ? $clsTstMarks->outof : 0;
                        $overaloutof[$classTest->class_test_id] = $obtainedOutOf > 0 ? $obtainedOutOf :
                                $overaloutof[$classTest->class_test_id];
                        $cosolidatedMarks[$student->id][$classTest->class_test_id] = array(
                            'ObtainedMarks' => $obtainedmark
                        );
                        $yaxis[$classTest->class_test_id] = array(
                            'testname' => $classTest->class_test_name,
                            'outoff' => $overaloutof[$classTest->class_test_id]);
                    }
                }

                $this->view->tests = $yaxis;
                $this->view->studentMarkList = $cosolidatedMarks;
            }
        }
    }

    public function getClassTestListByCombinationReportAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        $params = $queryParams = array();

        $aggregateval = '';
        //  $studata=$this->request->getPost('studata');

        $clsrptdata = json_decode($this->request->getPost('clsrptdata'));
        foreach ($clsrptdata as $key => $value) {
            $params[$key] = $value;
        }
        $master_id = $params['subject_masterid'];
        $test_id = $params['test_id'];

        $classtestCombi = GroupSubjectsTeachers::findFirstById($master_id);

        $subjects_mas = OrganizationalStructureMaster::find(array(
                    'columns' => 'GROUP_CONCAT(id) as ids',
                    ' module = "Subject" and is_subordinate = 1 '
        ));
        $subidsall = HomeWorkController::_getAllChildren($classtestCombi->subject_id, $subjects_mas[0]->ids);

        $classmaster = ClassroomMaster::findFirstById($classtestCombi->classroom_master_id);
        $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
//        $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
//        $orgvaldet = OrganizationalStructureValues::findFirstById($classtestCombi->subject_id);
//        $subj_arr = $this->_getMandNodesForExam($orgvaldet);
//        $subjQuery = preg_filter('/^([\d,])*/', '(find_in_set("$0", stumap.aggregate_key)>0)', array_keys($subj_arr));
        $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key, stumap.subordinate_key,stumap.status,'
                . ' stuinfo.Student_Name FROM StudentMapping stumap '
                . ' LEFT JOIN StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id '
                . ' WHERE stumap.status = "Inclass" '
                . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                . ' and (' . implode(' or ', $res) . ') '
//                . ' and (' . implode(' and ', $subjQuery) . ') '
                . ' ORDER BY stuinfo.Admission_no ASC';

        $this->view->students = $students = $this->modelsManager->executeQuery($stuquery);

        $header = $rowArr = $yaxis = array();

        $i = 0;
        $cosolidatedMarks = $overaloutof = array();
        if (count($students) > 0) {
            foreach ($students as $student) {
                $classTests = ClassTest::find('grp_subject_teacher_id =' . $master_id . ' AND subjct_modules IN (' . implode(',', $subidsall) . ')');
                foreach ($classTests as $classTest) {
                    $clsTstMarks = ClassTestMarks::findFirst('class_test_id = ' . $classTest->class_test_id
                                    . ' and student_id = ' . $student->student_info_id);
                    $obtainedmark = ($clsTstMarks->marks) ? $clsTstMarks->marks : 0;
                    $obtainedOutOf = ($clsTstMarks->outof) ? $clsTstMarks->outof : 0;
                    $overaloutof[$classTest->class_test_id] = $obtainedOutOf > 0 ? $obtainedOutOf :
                            $overaloutof[$classTest->class_test_id];
                    $cosolidatedMarks[$student->id][$classTest->class_test_id] = array(
                        'stuname' => $student->Student_Name,
                        'ObtainedMarks' => $obtainedmark
                    );
                    $yaxis[$classTest->class_test_id] = array(
                        'testname' => $classTest->class_test_name . '(' . $overaloutof[$classTest->class_test_id] . ')',
                        'outoff' => $overaloutof[$classTest->class_test_id]);
                }
            }

            $this->view->tests = $yaxis;
            $this->view->studentMarkList = $cosolidatedMarks;

            $aggregatevals = $students[0]->aggregate_key ? explode(',', $students[0]->aggregate_key) : '';
            $class_arr = array();
            $aggcnt = 0;
            if ($aggregatevals != '') {
                foreach ($aggregatevals as $aggregateval) {
                    if ($aggcnt != 0) {
                        $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
                        $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                        $class_arr[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_mas_det->name . ' : ' . $orgnztn_str_det->name : '-';
                    }
                    $aggcnt++;
                }
            }
        }




        $header[] = 'Student Name';


        foreach ($yaxis as $yaxisval) {
            $header[] = $yaxisval['testname'];
        }

        $orgvaldet = OrganizationalStructureValues::findFirstById($classtestCombi->subject_id);

        $subject_keys = ReportsController::_getMandNodesForSubject($orgvaldet);

        $subject_keys = array_reverse($subject_keys);
        $subject_head = array();

        foreach ($class_arr as $headerval) {
            $subject_head[] = $headerval;
        }
        $subject_head[] = 'Subject : ' . implode(' >> ', $subject_keys);
        $icnt = count($header) - count($subject_head);

        for ($ii = 0; $ii < $icnt; $ii++) {
            $subject_head[] = '';
        }



//            echo '<pre>';
//            print_r($header);
//print_r($subject_head);exit;

        $reportdata = array();
        foreach ($cosolidatedMarks as $stuarr) {
            $reportval = array();
            $stu_cnt = 0;
            foreach ($stuarr as $stuarrval) {
                if ($stu_cnt == 0) {
                    $reportval[] = $stuarrval['stuname'];

//                    $aggregatevals = $stuarrval['aggreegatekey'] ? explode(',', $stuarrval['aggreegatekey']) : '';
//                    $class_arr = array();
//                    $aggcnt = 0;
//                    if ($aggregatevals != '') {
//                        foreach ($aggregatevals as $aggregateval) {
//                            if ($aggcnt != 0) {
//                                $orgnztn_str_det = OrganizationalStructureValues::findFirstById($aggregateval);
//                                $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
//                                $class_arr[$orgnztn_str_mas_det->id] = $orgnztn_str_det->name ? $orgnztn_str_det->name : '-';
//                            }
//                            $aggcnt++;
//                        }
////                        array_shift($class_arr);
//                        $aggcntval = 0;
//                        foreach ($this->mandnode as $key => $mandnodeval) {
//                            if ($aggcntval != 0) {
//                                $reportval[] = isset($class_arr[$key]) ? $class_arr[$key] : '-';
//                            }
//                            $aggcntval++;
//                        }
//                    }
                }
                $reportval[] = $stuarrval['ObtainedMarks'];
                $stu_cnt++;
            }
            $reportdata[] = $reportval;
        }

//        echo '<pre>';
//        print_r($header);
//        print_r($reportdata);
////       print_r ( array($rowArr,$yaxis));
//        exit;


        $filename = 'Classtest_Students_List_' . date('d-m-Y') . '.csv';

        $counter = 0;
        $unlink_file = DOWNLOAD_DIR . 'Classtest_Students_List_' . date('d-m-Y') . '.csv';
        if (file_exists($unlink_file)) {
            unlink($unlink_file);
        }

        $fp = fopen(DOWNLOAD_DIR . $filename, 'a');

        $delimiter = ",";
        $emptyarr = array();
        fputcsv($fp, $subject_head, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $emptyarr, $delimiter);
        fputcsv($fp, $header, $delimiter);
        foreach ($reportdata as $reportsval) {
//            $fp = fopen(DOWNLOAD_DIR . $filename, 'a');
            fputcsv($fp, $reportsval, $delimiter);
        }
        fclose($fp);

        $file = DOWNLOAD_DIR . $filename;

        if (file_exists($file)) {

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }

}
