<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class ContentSharingController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function contentSharingFileUploadAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->hasFiles() == true && count($this->request->getUploadedFiles()) < 10) {
            $arr = array();
            foreach ($this->request->getUploadedFiles() as $file) {
                $ext = explode('.', $file->getName());
                $size = explode('.', $file->getSize());
                $type = explode('.', $file->getType());
                $filename = $ext[0] . '.' . $ext[1];
                $newdir = 'dummy';
                if (!is_dir(UPLOAD_DIR . $newdir)) {
                    if (!mkdir(UPLOAD_DIR . $newdir, 0777, true)) {
                        die('Failed to create folders...');
                    }
                }
                $file->moveTo(UPLOAD_DIR . $newdir . '/' . $filename);
                $isimg = explode('/', $type[0]);
                $filedet = array();
                $data['files'] = '';
                $filedet['name'] = $filename;
                $filedet['size'] = json_decode($size[0]);
                $filedet['type'] = $type[0];
                $filedet['url'] = $this->url->get(UPLOAD_URI . $newdir . '/' . $filename);
                $filedet['thumbnailUrl'] = $this->url->get(UPLOAD_URI . $newdir . '/' . $filename);
                $filedet['deleteUrl'] = $this->url->get('content-sharing/deleteFileUpload') . '?file=' . $filename . '&updir=' . $newdir;
                $filedet['deleteType'] = "Delete";
                $arr[] = $filedet;
                $data['files'] = $arr;
                print_r(json_encode($data));
                exit;
            }
        }
        if ($this->request->getPost()) {
            $masterid = $this->request->getPost('masterid');
            $sharedcontent = ContentSharing::find('grp_subject_teacher_id=' . $masterid);
            $subjmaster = GroupSubjectsTeachers::findFirstById($masterid);
            $classroom = ClassroomMaster::findFirstById($subjmaster->classroom_master_id);
            if (count($sharedcontent) > 0) {
                foreach ($sharedcontent as $value) {
                    $newdir = $value->grp_subject_teacher_id . '_Readings_' . $value->id;
                    $expld = $value->files ? explode(',', $value->files) : '';
                    if ($expld):
                        foreach ($expld as $det) {
                            $filefulldet = new SplFileInfo(UPLOAD_DIR . 'sharedcontent/' . $newdir . '/' . $det);
                            $filsize = $filefulldet->getSize();
                            $filext = $filefulldet->getExtension();
                            $filname = $filefulldet->getFilename();
                            $content = array();
                            $content['name'] = $value->title;
                            $content['description'] = $value->description;
                            $content['size'] = json_decode($filsize);
                            $content['subject'] = $classroom->name . ' (' . OrganizationalStructureValues::findFirstById($subjmaster->subject_id)->name . ')';
                            $content['type'] = $filext;
                            $content['url'] = $this->url->get(UPLOAD_URI . 'sharedcontent/' . $newdir . '/' . $filname);
                            $content['thumbnailUrl'] = $this->url->get(UPLOAD_URI . 'sharedcontent/' . $newdir . '/' . $filname);
                            $content['deleteUrl'] = $this->url->get('content-sharing/deleteFileUpload') . '?file=' . $filname . '&updir=' . 'sharedcontent/' . $newdir . '&id=' . $value->id;
                            $content['deleteType'] = "Delete";
                            $result[] = $content;
                        }
                    endif;
                }
                print_r(json_encode($result));
                exit;
            }
        }
    }

    public function saveReadingFilesAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $params = array();
        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }
        if (!$params['filename']) {
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">Upload File is Required!</div>';
            print_r(json_encode($message));
            exit;
        }
        $readings = new ContentSharing();
        $readings->grp_subject_teacher_id = $params['masterid'];
        $readings->title = $params['content_title'];
        $readings->description = $params['content_description'];
        $readings->files = implode(',', $params['filename']);
        $readings->created_by = $uid;
        $readings->created_on = time();
        $readings->modified_by = $uid;
        $readings->modified_on = time();
        if ($readings->save()) {
            $newdir = $params['masterid'] . '_Readings_' . $readings->id;
            if (!is_dir(UPLOAD_DIR . 'sharedcontent/' . $newdir)) {
                if (!mkdir(UPLOAD_DIR . 'sharedcontent/' . $newdir, 0777, true)) {
                    die('Failed to create folders...');
                }
            }
            foreach ($params['filename'] as $file) {
                if (is_file(UPLOAD_DIR . 'dummy/' . $file)) {
                    $filterfile[] = $file;
                    copy(UPLOAD_DIR . 'dummy/' . $file, UPLOAD_DIR . 'sharedcontent/' . $newdir . '/' . $file);
                }
            }
            $readings->files = implode(',', $filterfile);
            $readings->save();
            $this->deleteDir(UPLOAD_DIR . 'dummy');
            $message['type'] = 'success';
            $message['ItemID'] = $readings->id;
            $message['message'] = '<div class="alert alert-block alert-success fade in">Files Uploaded Sucessfully!!</div>';
            print_r(json_encode($message));
            exit;
        } else {
            $error = '';
            foreach ($readings->getMessages() as $msg) {
                $error .= $msg;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function deleteFileUploadAction($file) {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $expld = array();
        $name = $this->request->get('file');
        $updir = $this->request->get('updir');
        $type = $this->request->get('type');
        $id = $this->request->get('id');
        if ($id) {
            $sharedcontent = ContentSharing::findFirstById($id);
            $expld = explode(',', $sharedcontent->files);
            $remainfile = array_diff($expld, array($name));
            if ($remainfile) {
                $sharedcontent->files = implode(',', $remainfile);
                if ($sharedcontent->save()) {
                    unlink(UPLOAD_DIR . $updir . '/' . $name);
                    $del = "success";
                    print_r(json_encode($del));
                    exit;
                }
            } else {
                $this->deleteDir(UPLOAD_DIR . $updir);
                if ($sharedcontent->delete()) {
                    $del = "success";
                    print_r(json_encode($del));
                    exit;
                }
            }
        } else {
            unlink(UPLOAD_DIR . $updir . '/' . $name);
            $del = "success";
            print_r(json_encode($del));
            exit;
        }
    }

    public static function deleteDir($dirPath) {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

}
