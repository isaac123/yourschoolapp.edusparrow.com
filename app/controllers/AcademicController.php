<?php

class AcademicController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->tag->prependTitle("My Subject | ");
        $this->assets->addCss('css/appstyles/academic-page.css');
        $this->assets->addJs('js/appscripts/classroom/classroom.js');
        $this->assets->addJs("js/appscripts/exam/exam.js");
        $this->assets->addJs("js/appscripts/mark/mark.js");
        $this->assets->addJs("js/appscripts/assignment/assignment.js");
        $this->assets->addJs("js/appscripts/attendance/attendance.js");
        $this->assets->addJs("js/appscripts/rating/rating.js");
        $this->assets->addJs('js/appscripts/home-work/homework.js');
        $this->assets->addJs('js/appscripts/content-sharing/contentsharing.js');
        $this->view->masterid = $masterid = $this->dispatcher->getParam('masterid');
        $this->view->subjmaster= $subjmaster = GroupSubjectsTeachers::findFirstById($masterid);
        $this->view->content = $content = ContentSharing::find('grp_subject_teacher_id=' . $masterid);
        $this->view->classroom= ClassroomMaster::findFirstById($subjmaster->classroom_master_id);
    }
    
    public function classprogressAction() {
        $this->tag->prependTitle("My Class | ");
        $this->assets->addCss('css/appstyles/academic-page.css');
        $this->assets->addJs('js/appscripts/classroom/classroom.js');
        $this->assets->addJs("js/appscripts/attendance/attendance.js");
        $this->assets->addJs("js/appscripts/rating/rating.js");
        $this->view->masterid = $masterid = $this->dispatcher->getParam('masterid');
        $subjmaster = GroupClassTeachers::findFirstById($masterid);
        $this->view->classroom= ClassroomMaster::findFirstById($subjmaster->classroom_master_id);
        
    }

}

?>