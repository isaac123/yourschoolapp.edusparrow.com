<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class AssigningController extends ControllerBase {

    protected $bgclass = array(
        array('color' => "#AEC785", 'highlight' => "#97b06e"),
        array('color' => "#FDD752", 'highlight' => "#ecc641"),
        array('color' => "#3ad0c8", 'highlight' => "#1fb5ad"),
        array('color' => "#59ace2", 'highlight' => "#428bca"),
        array('color' => "#ff6c60", 'highlight' => "#f15e52"),
        array('color' => "#9589db", 'highlight' => "#8175c7"),
        array('color' => "#a9d86e", 'highlight' => "#9ecd63"),
        array('color' => "#fcb322", 'highlight' => "#f3aa19"),
        array('color' => "#cbcbcb", 'highlight' => "#BA2424"),
        array('color' => "#F2DEDE", 'highlight' => "#FCB1AE"));

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function addSubjectsByClassAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->form = new AddSubjectForm();
    }

    public function getSubjectsByCycleAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->cycleID = $this->request->getPost('cycleID');
            $this->view->subjects = SubjectsDivision::find('node_id LIKE "%-' . $this->view->cycleID . '-%" OR node_id LIKE "' . $this->view->cycleID . '-%" OR node_id LIKE "%-' . $this->view->cycleID . '" ORDER BY node_id');


            $acd_value = OrganizationalStructureValues::findfirst('id = ' . $this->view->cycleID);
            $results = array();
            ControllerBase::_getValue($acd_value->id, $acd_value->id, $acd_value->name, $results);

            $subjects = Array();

            foreach ($results as $result) {
                $sub = SubjectsDivision::find('node_id LIKE "' . $result['id'] . '"');
                if (count($sub) > 0) {
                    $subjects[] = array('name' => $result['name'],
                        'id' => $result['id'],
                        'subject' => $sub);
                }
            }
            $this->view->subjects = $subjects;


            $this->view->form = new AddSubjectForm();
            $this->view->acd_value = $acd_value->name;
            //$this->view->divname = ControllerBase::get_division_name_student();
        }
    }

    public function addSubjectAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $form = new AddSubjectForm();
        $message = array();
        // print_r($form->getMessages());
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) == false) {
                foreach ($form->getMessages() as $messages) {
                    $error .= $messages;
                }
                //echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            } else {
                $classID = $this->request->getPost('classID');
                $subjectName = $this->request->getPost('subj');
                $subjectCode = $this->request->getPost('code');
                $is_madatory = $this->request->getPost('mandatory');
                //$cacdyr = AcademicYearMaster::findFirstByStatus('c')->id;
                //$cacdyr = ControllerBase::getCycleNodeId($classID);
                $subject = new SubjectsDivision();
                $subject->assign(array(
                    'node_id' => $classID,
                    'subjectName' => $subjectName,
                    'subjectCode' => $subjectCode,
                    'is_mandatory' => $is_madatory
                ));

                $unique = SubjectsDivision::find('node_id = ' . $classID . ' and subjectName = "' . $subjectName . '"');
                if (count($unique) > 0) {
                    //echo '<div class="alert alert-block alert-danger fade in">Subject Exists already</div>';
                    $message['type'] = 'warning';
                    $message['message'] = 'Subject Exists already';
                    print_r(json_encode($message));
                    exit;
                }

                $unique = SubjectsDivision::find('node_id = ' . $classID . ' and subjectCode = "' . $subjectCode . '"');
                if (count($unique) > 0) {
                    //echo '<div class="alert alert-block alert-danger fade in">Subject Exists already</div>';
                    $message['type'] = 'warning';
                    $message['message'] = 'Subject Code Is Already Taken-up';
                    print_r(json_encode($message));
                    exit;
                }


                if ($subject->save()) {
                    // echo '<div class="alert alert-success">Subject Added Successfully</div>';
                    $message['type'] = 'success';
                    $message['message'] = 'Subject Added Successfully!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($subject->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    // echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function getSubjectByIdAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->subjId = $this->request->getPost('subjId');
            $this->view->subject = SubjectsDivision::findFirstById($this->view->subjId);
            $this->view->form = new AddSubjectForm();
        }
    }

    public function editSubjectByIdAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $form = new AddSubjectForm();
        $message = array();
        if ($this->request->isPost()) {
//        print_r($form->isValid($this->request->getPost()));
//        print_r($form->getMessages());
            if ($form->isValid($this->request->getPost()) == false) {
                foreach ($this->request->getPost() as $key => $val) {
                    foreach ($form->getMessagesFor($key) as $messages) {
                        $error .= $messages . "\n";
                    }
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            } else {
                $classID = $this->request->getPost('classID');
                $subjectName = $this->request->getPost('subj');
                $subjectCode = $this->request->getPost('code');
                $is_madatory = $this->request->getPost('mandatory');

                $subjID = $this->request->getPost('subjId');
                ;
                $cacdyr = ControllerBase::get_current_academic_year()->id;
                $subject = SubjectsDivision::findFirstById($subjID);
                $subject->assign(array(
                    'node_id' => $classID,
                    'subjectName' => $subjectName,
                    'subjectCode' => $subjectCode,
                    'is_mandatory' => $is_madatory,
                    'cycleNodeId' => $cacdyr
                ));
                $unique = SubjectsDivision::find('node_id = ' . $classID . ' and subjectName = "' . $subjectName . '" and id !=' . $subjID);
                if (count($unique) > 0) {
                    $message['type'] = 'warning';
                    $message['message'] = 'Subject Exists Already With The Name: ' . $subjectName;
                    print_r(json_encode($message));
                    exit;
                }

                $unique = SubjectsDivision::find('node_id = ' . $classID . ' and subjectCode = "' . $subjectCode . '" and id !=' . $subjID);
                if (count($unique) > 0) {
                    $message['type'] = 'warning';
                    $message['message'] = 'Subject Code Is Already Takenup';
                    print_r(json_encode($message));
                    exit;
                }
                if ($subject->save()) {
                    $message['type'] = 'success';
                    $message['message'] = 'Subject Updated Successfully!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($subject->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function assignSectionAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Assigning | ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs("js/assigning/assignSection.js");
//        $this->assets->addJs('js/appscripts/application.js');
//        $this->assets->addJs('js/spacetree/spacetreeutils.js');
    }

    public function loadSubtreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost() && $this->request->getPost('orgvalueid') != '') {
            $this->view->orgvalueid = $orgvalueid = $this->request->getPost('orgvalueid');
            $this->view->org_value = $org_value = OrganizationalStructureValues::find('parent_id = ' . $orgvalueid
                            . ' GROUP BY  org_master_id ');
//             $this->view->org_mas_value = $org_mas_value = OrganizationalStructureMaster::find('parent_id = '.$orgvalueid);
        }
    }

    public function loadStudentsAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $params = $queryParams = array();
            foreach ($this->request->getPost() as $key => $value) {
                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'aggregate' && $value != '') {
                    $params['aggregateids'][] = $value;
                } else {

                    $params[$key] = $value;
                }
            }
            $this->view->acdyrMas = $acdyrMas = OrganizationalStructureMaster::findFirst('mandatory_for_admission =1 and cycle_node =1');
            $this->view->nodes = $this->_getNonMandNodesForAssigning($acdyrMas->id);
            $this->view->mandnode = $this->_getMandNodesForAssigning($acdyrMas);
//            print_r(implode(',', $params['aggregateids']));
//            exit;
//            echo 'aggregate_key LIKE "' . implode(',', $params['aggregateids']) . '%"';exit;
            $this->view->academicyear = ControllerBase::get_current_academic_year();
            $this->view->aggregateid = implode(',', $params['aggregateids']); //$params['aggregateids'][count($params['aggregateids']) - 1];
            $this->view->students = StudentMapping::find('aggregate_key LIKE "' . implode(',', $params['aggregateids']) . '%" and status != "Relieved"');
        }
    }

    //assign subject and teacher
    public function assignSubjectandTeacherAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_subjectsdivision = 0;
    }

    //assign subject and teacher
    public function assignClassTeachersAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_subjectsdivision = 0;
    }

    public function loadGroupsSubjectTeachersAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->cycleID = $this->request->getPost('cycleID');
        }
    }

    public function loadGroupsClsTeachersAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->cycleID = $this->request->getPost('cycleID');
        }
    }

    public function loadGroupsClsTeachersForEditAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $groupClsTechID = $this->request->getPost('groupClsTechID');
            $this->view->record = GroupClassTeachers::findFirstById($groupClsTechID);
//            print_r($groupClsTechID);exit;
        }
    }

    public function checkrollnoexistsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        try {
            if ($this->request->isPost()) {
                $RollNo = $this->request->getPost('rollno');
                $stuid = $this->request->getPost('stuid');
                $params = $queryParams = array();
                foreach ($this->request->getPost('managgre') as $key => $value) {
                    $IsSubdiv = explode('_', $key);
                    if ($IsSubdiv[0] == 'aggregate' && $value) {
                        $params['aggregateids'][] = $value;
                    } else {
                        $params[$key] = $value;
                    }
                }
                $getCycleNodeMas = OrganizationalStructureMaster::findFirst('cycle_node=1');
                $getCycleNode = OrganizationalStructureValues::findFirst('id IN (' . implode(',', $params['aggregateids']) . ') and '
                                . 'org_master_id =' . $getCycleNodeMas->id);

                if ($getCycleNode) {
                    $psql = "Select * from StudentInfo s "
                            . " INNER JOIN StudentMapping m on s.id = m.student_info_id"
                            . " where s.rollno = '$RollNo'"
                            . " and s.id != " . $stuid
                            . " and find_in_set(" . $getCycleNode->id . ", m.aggregate_key)>0";

                    $extentry = $this->modelsManager->executeQuery($psql);
                    if (count($extentry)) {
                        $message['type'] = 'error';
                        $message['message'] = "<div class='alert alert-block alert-danger fade in'>Rollno already exists for this $getCycleNodeMas->name.</div>";
                        print_r(json_encode($message));
                        exit;
                    } else {
                        $message['type'] = 'success';
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    $message['type'] = 'error';
                    $message['message'] = "<div class='alert alert-block alert-danger fade in'>Invalid $getCycleNodeMas->name.</div>";
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {

            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function updateSubjTeachersAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        if ($this->request->isPost()) {
            $teacherIds = $this->request->getPost('teachers');
            $masterid = $this->request->getPost('masterid');
            $entry_exist = GroupSubjectsTeachers::findFirstById($masterid);
            if ($entry_exist) {

                $staff_id = array();
                $staff_app_nos = explode(',', $teacherIds);
                foreach ($staff_app_nos as $staff_app_no) {
                    $staff_no = explode('(', $staff_app_no);
                    $staff_number = rtrim($staff_no[1], ')');
                    $staff_id[] = StaffInfo::findfirst("loginid = '$staff_number'")->id;
                }

                $entry_exist->teachers_id = implode(',', $staff_id);
                if ($entry_exist->save()) {
                    $message['type'] = 'success';
                    $message['message'] = '<div class="alert alert-success">Subjects and Teachers updated Successfully</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($entry_exist->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            } else {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Invalid entry</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function saveSubjtsandTeachersAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        if ($this->request->isPost()) {

            $teacherIds = $this->request->getPost('staff');
            $aggregate_key = $this->request->getPost('aggregate_key');
            $subject_id = $this->request->getPost('subjID');
            $node_grp_name = $this->request->getPost('node_grp_name');

            if (!$subject_id) {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Select atleast one Subjects</div>';
                print_r(json_encode($message));
                exit;
            }
            if (!$teacherIds) {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Select atleast one Teachers</div>';
                print_r(json_encode($message));
                exit;
            }

            //process the teacher IDs.
            //Teacher_id will be of format aaaaa(EID),bbbbb(EID) etc.. extract the EID from the string and find staff id.

            $staff_id = array();
            $staff_app_nos = explode(',', $teacherIds);
            foreach ($staff_app_nos as $staff_app_no) {
                $staff_no = explode('(', $staff_app_no);
                $staff_number = rtrim($staff_no[1], ')');
                $staff_id[] = StaffInfo::findfirst("loginid = '$staff_number'")->id;
            }


            $entry_exist = '';
            $entry_exist_true = 0;
            $completeQuery = array();
//            foreach ($subjectQuery as $subjectclass) {
//                $subclsarr = explode('-', $subjectclass);
//                if (count($subclsarr) > 1):
//                    array_shift($subclsarr);
//                endif;
//                $currentQury = preg_filter('/^([\d])*/', 'find_in_set( "$0" ,REPLACE(aggregated_nodes_id, "-", "," ) )', $subclsarr);
//                $completeQuery = implode(' or ', $currentQury);
//                $entry_exist = GroupSubjectsTeachers::findfirst(
//                                'subject_id=' . $subject_id
//                                . ' AND (' . $completeQuery . ')');
//                if ($entry_exist) {
//
//                    $classIds = ControllerBase::getCommonIdsForKeys($entry_exist->aggregated_nodes_id);
//                    foreach ($classIds as $clsvalue) {
//                        $clsarr = explode('-', $clsvalue);
//                        $arraydif1= array_diff($clsarr, $subclsarr);
//                        $arraydif2= array_diff( $subclsarr, $clsarr);
//                        if (count($arraydif1) == 0 || count($arraydif2)==0):
//                            $entry_exist_true =1;
//                            $message['type'] = 'error';
//                            $message['message'] = '<div class="alert alert-success">Assignment Not Possible!</div>';
//                            print_r(json_encode($message));
//                            exit;
//                        endif;
//                    }
//                }
//            }
            $getnodename = GroupSubjectsTeachers::findFirst("aggregated_nodes_id LIKE '$aggregate_key'");
            $nodename = $getnodename ? $getnodename->name : $node_grp_name;
            if ($nodename == '') {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-danger">Classroom name is required!</div>';
                print_r(json_encode($message));
                exit;
            }
            if ($entry_exist_true == 0) {
                $subdivsubject = GroupSubjectsTeachers::findFirst("aggregated_nodes_id LIKE '$aggregate_key' and subject_id = $subject_id") ?
                        GroupSubjectsTeachers::findFirst("aggregated_nodes_id LIKE '$aggregate_key' and subject_id = $subject_id") :
                        new GroupSubjectsTeachers();
                $teach = $subdivsubject->teachers_id ? explode(',', $subdivsubject->teachers_id) : array();
                $teachfin = count($teach) > 0 ? array_unique(array_merge($teach, $staff_id)) : $staff_id;
                $subdivsubject->aggregated_nodes_id = $aggregate_key;
                $subdivsubject->teachers_id = implode(',', $teachfin);
                $subdivsubject->subject_id = $subject_id;
                $subdivsubject->name = $nodename;
                if ($subdivsubject->save()) {
                    $message['type'] = 'success';
                    $message['message'] = '<div class="alert alert-success">Subjects and Teachers Added Successfully</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($subdivsubject->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            } else {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-success">Assignment Not Possible!</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function saveClsTeachersAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        if ($this->request->isPost()) {

            $teacherIds = $this->request->getPost('staff');
            $aggregate_key = $this->request->getPost('aggregate_key');
            if (!$teacherIds) {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Select atleast one Teachers</div>';
                print_r(json_encode($message));
                exit;
            }

            //process the teacher IDs.
            //Teacher_id will be of format aaaaa(EID),bbbbb(EID) etc.. extract the EID from the string and find staff id.

            $staff_id = '';
            $staff_app_nos = explode(',', $teacherIds);
            foreach ($staff_app_nos as $staff_app_no) {
                $staff_no = explode('(', $staff_app_no);
                $staff_number = rtrim($staff_no[1], ')');
                $staff_id = $staff_id . ',' . StaffInfo::findfirst("loginid = '$staff_number'")->id;
            }

//            $getAllCombination = ControllerBase::
//            $pattern = '/[:]/';
////            POSITION("ou" IN "w3resource")
//            $Totarr = (preg_split($pattern, $node_id));
//            $currentQury = preg_filter('/^([\d-])*/', 'LOCATE(  node_id ,  "$0" )', $Totarr);
//echo'<pre>'; print_r($currentQury);print_r($subjectQuery);exit;


            $entry_exist = '';
            $entry_exist_true = 0;
            $completeQuery = array();
//            foreach ($subjectQuery as $subjectclass) {
//                $subclsarr = explode('-', $subjectclass);
//                if (count($subclsarr) > 1):
//                    array_shift($subclsarr);
//                endif;
//                $currentQury = preg_filter('/^([\d])*/', 'find_in_set( "$0" ,REPLACE(aggregated_nodes_id, "-", "," ) )', $subclsarr);
//                $completeQuery = implode(' or ', $currentQury);
//                $entry_exist = GroupClassTeachers::findfirst('  (' . $completeQuery . ')');
//                if ($entry_exist) {
//
//                    $classIds = ControllerBase::getCommonIdsForKeys($entry_exist->aggregated_nodes_id);
//                    foreach ($classIds as $clsvalue) {
//                        $clsarr = explode('-', $clsvalue);
//                        $arraydif1= array_diff($clsarr, $subclsarr);
//                        $arraydif2= array_diff( $subclsarr, $clsarr);
//                        if (count($arraydif1) == 0 || count($arraydif2)==0):
//                            $entry_exist_true =1;
//                            $message['type'] = 'error';
//                            $message['message'] = '<div class="alert alert-success">Assignment Not Possible!</div>';
//                            print_r(json_encode($message));
//                            exit;
//                        endif;
//                    }
//                }
//            }
            if ($entry_exist_true == 0) {
                $subdivsubject = new GroupClassTeachers();
                $subdivsubject->aggregated_nodes_id = $aggregate_key;
                $subdivsubject->teachers_id = ltrim($staff_id, ',');
//                print_r($subdivsubject);exit;
                if ($subdivsubject->save()) {
                    $message['type'] = 'success';
                    $message['message'] = '<div class="alert alert-success">Class Teachers Added Successfully</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($subdivsubject->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    exit;
                }
            } else {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-success">Assignment Not Possible!</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    function editSaveSubjtsandTeachersAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $error = '';
        try {
            if ($this->request->isPost()) {
                $subj_serialID = $this->request->getPost('subj_serid');
                $teacher = $this->request->getPost('teacherid');


                $subject_teachers = GroupSubjectsTeachers::findfirst('id = ' . $subj_serialID);
                $subject_teachers->teacher_id = $teacher;

                if (!($subject_teachers->save())) {
                    foreach ($subject_teachers->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $message['type'] = 'success';
                    $message['message'] = '<div class="alert alert-block alert-success fade in">Teachers edited Successfully</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {

            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function displayGroupSubjectTeachersAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_subjectsdivision = 0;
        if ($this->request->isPost()) {
            $this->view->cycle = $this->request->getPost('cycle');
            $GroupSubjectsTeachers = GroupSubjectsTeachers::find(' find_in_set(' . $this->view->cycle
                            . ',REPLACE(aggregated_nodes_id, "-", "," ) )>0 ORDER BY aggregated_nodes_id');
            $records = Array();
            foreach ($GroupSubjectsTeachers as $masterTeach) {
                $record = $masterTeach->aggregated_nodes_id;
                $teacher_name = $subject_name = $name_param = array();
//                $final_path = ControllerBase::getCommonIdsForKeys($record);
//                $nodegrp= implode('>>', $final_path);
                $name_param = ControllerBase::getNameForKeys($record);
                $subject_name = OrganizationalStructureValues::findfirst('id = ' . $masterTeach->subject_id)->name;
                $teacher_name[] = implode(' - ', preg_replace_callback('/^([\d-])*/', function($matches) {
                            return StaffInfo::findFirstById($matches[0])->Staff_Name;
                        }, explode(",", $masterTeach->teachers_id)));
                $row = array("id" => $masterTeach->id,
                    "pathname" => implode('<br>', $name_param),
                    "groups" => 'GROUP',
                    "subject_name" => $subject_name,
                    "teacher_name" => implode(' , ', $teacher_name));

                $records[$record][] = $row;
            }
//            echo '<pre>';
//            print_r($records);
//            exit;
            $this->view->subject_teachers_groups = $records;
        }
    }

    public function displayGroupClsTeachersAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_subjectsdivision = 0;
        if ($this->request->isPost()) {
            $this->view->cycle = $this->request->getPost('cycle');
            $records = Array();
            $GroupSubjectsTeachers = GroupClassTeachers::find(' find_in_set(' . $this->view->cycle . ',REPLACE(aggregated_nodes_id, "-", "," ) )>0 ORDER BY aggregated_nodes_id');
            foreach ($GroupSubjectsTeachers as $clsteachr) {
                $name_param = ControllerBase::getNameForKeys($clsteachr->aggregated_nodes_id);
                $teacher_name = Array();
                $teacher_det = explode(",", $clsteachr->teachers_id);
                foreach ($teacher_det as $teachrow) {
                    $teacher_name[] = StaffInfo::findfirst('id = ' . $teachrow)->Staff_Name;
                }
                $row = array("id" => $clsteachr->id,
                    "pathname" => $name_param,
                    "groups" => 'GROUP',
                    "teacher_name" => implode(', ', $teacher_name)
                );
                $records[] = $row;
            }
//            print_r($records);
//            exit;
            $this->view->subject_teachers_groups = $records;
        }
    }

    public function savesectionandrollnoAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $finalconcat_subdivs = '';
        if ($this->request->isPost()) {

//            foreach ($this->request->getPost('managgre') as $key => $value) {
//                $IsSubdiv = explode('_', $key);
//                if ($IsSubdiv[0] == 'aggregate' && $value != '') {
//                    $params['aggregateids'][] = $value;
//                } else {
//
//                    $params[$key] = $value;
//                }
//            }

            foreach ($this->request->getPost() as $key => $value) {
                if ($key == 'managgre') {

                    foreach ($this->request->getPost('managgre') as $key => $value) {
                        $IsSubdiv = explode('_', $key);
                        if ($IsSubdiv[0] == 'aggregate' && $value != '') {
                            $params['aggregateids'][] = $value;
                        } else {

                            $params[$key] = $value;
                        }
                    }
                } else {
                    $params[$key] = $value;
                }
            }
            $RollNo = $params['rollno'];
            $aggregateKey = $params['aggregateKey'];
            $studId = $params['stuid'];
            $aggregareArr = array();
//            $explagg = explode(':', $aggregateKey);
//            foreach ($explagg as $indval) {
//                $separr = explode('-', $indval);
//                $i = 0;
//                foreach ($separr as $indsepval) {
//                    if ((!in_array($indsepval, $aggregareArr)) && $i != 0)
//                        $aggregareArr[] = $indsepval;
//                    $i++;
//                }
//            }
            $old = explode(',', $aggregateKey);
            $newagg = array_filter(array_unique(array_merge($params['aggregateids'], $old)));
//            print_r($newagg);exit;
            $message = array();
            try {
                if ($studId) {
                    $stuinfo = StudentInfo::findFirstById($studId);
                    $stuacd = StudentMapping::findFirstByStudentInfoId($studId);
                    $stuhist = StudentHistory::findFirst('student_info_id=' . $studId . ' Order by id desc');
                    $newlygenerated = $newagg; // (implode(',', $params['aggregateids']) . ',' . $aggregateKey);
                    $stuinfo->rollno = $RollNo;
//                    $newaggre = $stuacd->aggregate_key . ',' . $newlygenerated;
//                    $newaggreARR = explode(',', $newlygenerated);
                    $newaggregateKey = implode(',', $newlygenerated);

                    $stuacd->aggregate_key = $newaggregateKey;
                    $stuacd->status = 'Inclass';

                    $stuhist->aggregate_key = $newaggregateKey;
                    $stuhist->status = 'Inclass';

                    if ($stuacd->save() && $stuinfo->save() && $stuhist->save()) {
//                        $dispatcherParams['studentId'] = $studId;
//                        $dispatcherParams['assignToAll'] = 0;
                        $message['type'] = 'success';
                        $message['message'] = '<div class="alert alert-block alert-success fade in">Saved Successfully</div>';

                        print_r(json_encode($message));
                        exit;
//                        $dispatcherParams['messageParams'] = $message;
//                        $title = $this->dispatcher->setParams($dispatcherParams);
                    } else {
                        echo 'sd';
                        foreach ($stuacd->getMessages() as $message) {
                            $error .= $message;
                        }
                        foreach ($stuinfo->getMessages() as $message) {
                            $error .= $message;
                        }foreach ($stuhist->getMessages() as $message) {
                            $error .= $message;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-success fade in">Error occured! please try again later</div>';
                    print_r(json_encode($message));
                    exit;
                }
            } catch (Exception $e) {
                foreach ($e->getMessages() as $message) {
                    $error .= $message;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function assignStafftoClassAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

//        $this->tag->prependTitle("Class Activity | ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs("js/assigning/assign.js");
    }

    public function spacetreeAction() {
        $this->tag->prependTitle("Space Tree | ");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $res = $this->_getValueTreeFor(3);
//        echo '<pre>';
//        print_r(json_encode($res));
//        exit;
    }

    public function _getNonMandNodesForAssigning($id, $nodes = array()) {
        $fields = OrganizationalStructureMaster::find('parent_id=' . $id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
                if ($field->mandatory_for_admission != 1 && $field->is_subordinate != 1) {
                    $nodes[$field->id] = $field->name;
                } else {
                    $nodes = AssigningController::_getNonMandNodesForAssigning($field->id, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function getTreeAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $cycle = $this->request->getPost('cycle');
            $res = $this->_getValueTreeFor($cycle);

            print_r(json_encode($res));
            exit;
        }
    }

    public function _getMandNodesForAssigning($acdyrMas, $nodes = array()) {

        $nodes[$acdyrMas->id] = $acdyrMas->name;
        $fields = OrganizationalStructureMaster::find('parent_id=' . $acdyrMas->id);
        if (count($fields) > 0):
            foreach ($fields as $field) {
                if ($field->mandatory_for_admission == 1) {
//                    $nodes[$field->id] = $field->name;
                    $nodes = AssigningController::_getMandNodesForAssigning($field, $nodes);
                }
            }
        endif;
        return $nodes;
    }

    public function getStudentCountAndSubjectsAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $response = array();

        if ($this->request->isPost()) {
            $node_id = $this->request->getPost('node_id');

            $pattern = '/[:]/';
            $Totarr = (preg_split($pattern, $node_id));
            foreach ($Totarr as $arr) {
                $valarr = explode('-', $arr);
//            $combinedarr= preg_filter('/^([\d-])*/', '( parent_id ="$0" )', $valarr);
                $currentQury[] = '( parent_id IN (' . implode(',', $valarr) . '))';
            }

//            print_r($currentQury);exit;
            //Fetch the common path first, say the node_id is 1-3-5:1-3-6:1-3-7, then
            // the first step is to fetch the common path which would be 1-3-
            $common_path = $this->_fetchCommonPath($node_id);
            $common = $common_path;

            //Once common path is fetched merge the node_id.
            //For the example taken above the result should be 1-3-5,6,7
            $nodes = explode(':', $node_id);

            if (count($nodes) > 1) {
                foreach ($nodes as $node) {
                    $node_split[] = explode('-', $node);
                }

                // Convert array to an array of string lengths
                $lengths = array_map('count', $node_split);
                $max = max($lengths);

                //Get the length for the common path
                $path_length = count($common_path);
                $common_path = implode('-', $common_path) . '-';

                for ($i = $path_length; $i < $max; $i++) {
                    $level = Array();
                    $j = 0;
                    foreach ($nodes as $node) {
                        $char = $node_split[$j++][$i];
                        if ($char != '') {
                            if (array_search($char, $level) === false) {
                                $level[] = $char;
                            }
                        }
                    }
                    //sort the item in same level
                    sort($level);
                    $level_str = implode(',', $level);
                    $common_path = $common_path . $level_str . '-';
                }
            }

            $aggregate = rtrim($common_path, '-');
            $samelevlnodes = explode(',', $aggregate);
//            print_r($samelevlnodes);
//            print_r($aggregate);
            $arraycommon = $samelevlnodes;
            if (count($samelevlnodes) > 0):
                $arraycommon = explode('-', $samelevlnodes[0]);
                if (count($samelevlnodes) > 1)
                    array_pop($arraycommon);
            endif;
            $this->view->aggregate = $aggregate;
//            print_r($currentQury);print_r($subjectQuery);exit;
//            print_r($samelevlnodes);print_r($aggregate);
//            print_r($arraycommon);
            //Aggregate is over
            //Find the common subjects
//            print_r($subjectQuery);
//            exit;
//            echo implode(' and ', $currentQury);exit;
//            $subjects = SubjectsDivision::find(implode(' or ', $currentQury));
//            $array_pop = array_pop($arraycommon);

            $commonQury = preg_filter('/^([\d])*/', '(parent_id= "$0")', $arraycommon);
            $subjects_mas = OrganizationalStructureMaster::find(array(
                        'columns' => 'GROUP_CONCAT(id) as ids',
                        ' module = "Subject" and is_subordinate = 1 '
            ));
//            print_r('org_master_id IN (' . $subjects_mas[0]->ids . ') '
//                            . ' and (' . implode(' or ', $currentQury) . ')');exit;
            $subjects = OrganizationalStructureValues::find('org_master_id IN (' . $subjects_mas[0]->ids . ') '
                            . ' and (' . implode(' or ', $currentQury) . ')');
            $this->view->subjects = $subjects;

            //TODO: $response student count
        }
    }

    public function getStudentCountAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $response = array();

        if ($this->request->isPost()) {
            $node_id = $this->request->getPost('node_id');

            //Fetch the common path first, say the node_id is 1-3-5:1-3-6:1-3-7, then
            // the first step is to fetch the common path which would be 1-3-
            $common_path = $this->_fetchCommonPath($node_id);
            $common = $common_path;

            //Once common path is fetched merge the node_id.
            //For the example taken above the result should be 1-3-5,6,7
            $nodes = explode(':', $node_id);

            if (count($nodes) > 1) {
                foreach ($nodes as $node) {
                    $node_split[] = explode('-', $node);
                }

                // Convert array to an array of string lengths
                $lengths = array_map('count', $node_split);
                $max = max($lengths);

                //Get the length for the common path
                $path_length = count($common_path);
                $common_path = implode('-', $common_path) . '-';

                for ($i = $path_length; $i < $max; $i++) {
                    $level = Array();
                    $j = 0;
                    foreach ($nodes as $node) {
                        $char = $node_split[$j++][$i];
                        if ($char != '') {
                            if (array_search($char, $level) === false) {
                                $level[] = $char;
                            }
                        }
                    }
                    //sort the item in same level
                    sort($level);
                    $level_str = implode(',', $level);
                    $common_path = $common_path . $level_str . '-';
                }
            }
            $aggregate = rtrim($common_path, '-');
            $samelevlnodes = explode(',', $aggregate);
            if (count($samelevlnodes) > 0):
                $arraycommon = explode('-', $samelevlnodes[0]);
                array_pop($arraycommon);
//            print_r();exit;
            endif;
            $this->view->aggregate = $aggregate;
        }
    }

    public function deleteClsTeachrByIdAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        try {
            if ($this->request->isPost()) {
                $clsteachId = $this->request->getPost('clsteachId');
                $clsTeachr = GroupClassTeachers::findFirstById($clsteachId);

                if ($clsTeachr->delete()) {
                    $message['type'] = 'success';
                    $message['message'] = 'Deleted Successfully';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($clsTeachr->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $messages) {
                $error .= $messages . "\n";
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        }
    }

    public function deleteSubTeacherByIdAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        try {
            if ($this->request->isPost()) {
                $clsteachId = $this->request->getPost('subjteachId');
                $clsTeachr = GroupSubjectsTeachers::findFirstById($clsteachId);

                if ($clsTeachr->delete()) {
                    $message['type'] = 'success';
                    $message['message'] = 'Deleted Successfully';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($clsTeachr->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $messages) {
                $error .= $messages . "\n";
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        }
    }

    public function addClassroomAction() {
        $this->view->onboard = 0;
        if ($this->request->get('onboard') == 'true') {
            $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
            $this->view->onboard = 1;
        } else {
            $this->tag->prependTitle("Classroom | ");
            $this->assets->addJs('js/appscripts/assigning/classroom.js');
            $this->assets->addJs('js/appscripts/spacetree/spacetreeutils.js');
            $this->assets->addJs("js/appscripts/assigning/assign.js");
        }
        $students = StudentInfo::findFirst();
        $staff = StaffInfo::findFirst('appointment_no != "NULL"');
        if (!$students) {
            $message = 'error_1';
            print_r(($message));
            exit;
        } else if (!$staff) {
            $message = 'error_2';
            print_r(($message));
            exit;
        }
        $this->view->classrooms = $classrooms = ClassroomMaster::find();
        $cycleNode = ControllerBase::get_current_academic_year();
        $this->view->cycleID = $cycleNode->id;
    }

    public function openValuesAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->classmaterid = $classmaterid = $this->request->getPost('classmasid');
            $this->view->classmaster = $classmaster = ClassroomMaster::findFirstById($classmaterid);
            $this->view->nodenames = $nodenames = OrganizationalStructureValues::findFirst(array(
                        'columns' => 'group_concat(name) names',
                        'id IN (' . str_replace('-', ',', $classmaster->aggregated_nodes_id) . ') '
            ));
            $name = ControllerBase::getNameForKeys($classmaster->aggregated_nodes_id);
            foreach ($name as $n) {
                $a = explode('>>', $n);
                $b = array_shift($a);
                $c[] = implode(' and ', $a);
            }
            $this->view->classname = $classname = implode('  (OR) ', $c);
            $this->view->subjteac = $subjteac = GroupSubjectsTeachers::find('classroom_master_id = ' . $classmaterid);
            $this->view->clsteac = $clsteac = GroupClassTeachers::findFirst('classroom_master_id = ' . $classmaterid);
            $pattern = '/[,-]/';
            $Totarr = (preg_split($pattern, $classmaster->aggregated_nodes_id));
            $subjpids = ControllerBase::getAlSubjChildNodes($Totarr);
            $subjects = ControllerBase::getAllPossibleSubjectsold($subjpids);
            $this->view->subjects = $subjects;
        }
    }

    public function loadStafStuAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->masterid = $masterid = $this->request->getPost('classmasterid');
            $this->view->classmaster = $classmaster = ClassroomMaster::findFirstById($masterid);
            $this->view->classgroupStu = $classgroupStu = ClassgroupStudents::findFirst("classroom_master_id = $masterid");
            $stuquery = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
//            print_r($stuquery);exit;
            $this->view->students = $students = StudentMapping::find('status = "Inclass" and ' . implode(' or ', $stuquery));
//            $this->view->staff = $staff =  StaffInfo::find('id IN ('.$master->teachers_id .')');
        }
    }

    public function addOrEditClassroomAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        try {
            if ($this->request->isPost()) {
                $actions = $this->request->getPost('actions');
                if ($actions == 'add') {
                    $newnodevalue = $this->request->getPost('newnodevalue');
                    $aggregate_key = $this->request->getPost('aggregate_key');
                    $orgValues = ClassroomMaster::findFirst("name= '$newnodevalue' and aggregated_nodes_id LIKE '$aggregate_key'");
                    if ($orgValues) {
                        $messages['type'] = 'error';
                        $messages['message'] = 'Node name already exist';
                        print_r(json_encode($messages));
                        exit;
                    } else {
                        $orgValues = new ClassroomMaster();
                        $orgValues->name = $newnodevalue;
                        $orgValues->aggregated_nodes_id = $aggregate_key;
                        $stuids = array();

                        if ($orgValues->save()) {
                            $classroomid = $orgValues->id;
                            $stuquery = ControllerBase::buildStudentQuery($orgValues->aggregated_nodes_id);
                            $students = StudentMapping::find('status = "Inclass" and ' . implode(' or ', $stuquery));
                            foreach ($students as $stu) {
                                $stuids[] = $stu->student_info_id;
                            }
                            if (count($stuids) > 0) {
                                $stuClassgrp = ClassgroupStudents::findFirst("classroom_master_id = $classroomid") ?
                                        ClassgroupStudents::findFirst("classroom_master_id = $classroomid") :
                                        new ClassgroupStudents();
                                $stuClassgrp->classroom_master_id = $classroomid;
                                $stuClassgrp->students_id = implode(',', $stuids);
//                print_r($stuClassgrp); exit;
                                if ($stuClassgrp->save()) {

                                    $messages['type'] = 'success';
                                    $messages['id'] = $orgValues->id;
                                    $messages['message'] = 'saved successfully';
                                    print_r(json_encode($messages));
                                    exit;
                                } else {
                                    $error = '';
                                    foreach ($stuClassgrp->getMessages() as $messages) {
                                        $error .= $messages;
                                    }
                                    echo $error;
                                    exit;
                                }
                            } else {

                                $messages['type'] = 'success';
                                $messages['id'] = $orgValues->id;
                                $messages['message'] = 'saved successfully';
                                print_r(json_encode($messages));
                                exit;
                            }
                        } else {
                            $error = '';
                            foreach ($orgValues->getMessages() as $message) {
                                $error .= $message;
                            }
                            $messages['type'] = 'error';
                            $messages['message'] = $error;
                            print_r(json_encode($messages));
                            exit;
                        }
                    }
                }
                if ($actions == 'edit') {
                    $nodevalue = $this->request->getPost('nodevalue');
                    $nodeid = $this->request->getPost('nodeid');
                    $orgValues = ClassroomMaster::findFirstById($nodeid);
                    if (!$orgValues) {
                        $messages['type'] = 'error';
                        $messages['message'] = 'Invalid node';
                        print_r(json_encode($messages));
                        exit;
                    } else {
                        $orgValues->name = $nodevalue;
                        if ($orgValues->save()) {
                            $messages['type'] = 'success';
                            $messages['id'] = $orgValues->id;
                            $messages['message'] = 'Updated successfully';
                            print_r(json_encode($messages));
                            exit;
                        } else {
                            $error = '';
                            foreach ($orgValues->getMessages() as $message) {
                                $error .= $message;
                            }
                            $messages['type'] = 'error';
                            $messages['message'] = $error;
                            print_r(json_encode($messages));
                            exit;
                        }
                    }
                }
                if ($actions == 'delete') {
                    $nodeid = $this->request->getPost('nodeid');
                    $stuClassgrp = ClassgroupStudents::findFirst("classroom_master_id = $nodeid");
                    $orgValues = ClassroomMaster::findFirstById($nodeid);
                    if (!$orgValues) {
                        $messages['type'] = 'error';
                        $messages['message'] = 'Invalid node';
                        print_r(json_encode($messages));
                        exit;
                    } else {
                        if ($stuClassgrp  && $stuClassgrp->delete() && $orgValues->delete()) {
                            $messages['type'] = 'success';
                            $messages['message'] = 'Deleted successfully';
                            print_r(json_encode($messages));
                            exit;
                        } else {
                            $error = '';
                            foreach ($orgValues->getMessages() as $message) {
                                $error .= $message;
                            }
                            $messages['type'] = 'error';
                            $messages['message'] = $error;
                            print_r(json_encode($messages));
                            exit;
                        }
                    }
                }
            }
        } catch (Exception $e) {

            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function addStaffInchargeConfirmAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        if ($this->request->isPost()) {

            $teacherIds = $this->request->getPost('staff_list');
            $classroomid = $this->request->getPost('classroomid');
//            $classmaster = ClassroomMaster::findFirstById($classroomid);
            if (!$teacherIds) {
                $message['type'] = 'error';
//                $message['message'] = '<div class="alert alert-block alert-danger fade in">Select atleast one Teachers</div>';
                $message['message'] = 'Select atleast one Teachers';
                print_r(json_encode($message));
                exit;
            }

            //process the teacher IDs.
            //Teacher_id will be of format aaaaa(EID),bbbbb(EID) etc.. extract the EID from the string and find staff id.

            $staff_id = array();
            $staff_app_nos = explode(',', $teacherIds);
            foreach ($staff_app_nos as $staff_app_no) {
                $staff_no = explode('(', $staff_app_no);
                $staff_number = rtrim($staff_no[1], ')');
                $staff_id[] = StaffInfo::findfirst("loginid = '$staff_number'")->id;
            }

            $entry_exist_true = 0;
            if ($entry_exist_true == 0) {
                $subdivsubject = GroupClassTeachers::findFirst("classroom_master_id = '$classroomid'") ?
                        GroupClassTeachers::findFirst("classroom_master_id = '$classroomid'") :
                        new GroupClassTeachers();
                $teach = $subdivsubject->teachers_id ? explode(',', $subdivsubject->teachers_id) : array();
                $teachfin = count($teach) > 0 ? array_unique(array_merge($teach, $staff_id)) : $staff_id;
                new GroupClassTeachers();
                $subdivsubject->classroom_master_id = $classroomid;
                $subdivsubject->teachers_id = implode(',', $teachfin);
                if ($subdivsubject->save()) {
                    $message['type'] = 'success';
//                    $message['message'] = '<div class="alert alert-success">Class Teachers Added Successfully</div>';
                    $message['message'] = 'Class Teachers Added Successfully';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($subdivsubject->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    echo $error;
                    exit;
                }
            } else {
                $message['type'] = 'error';
                $message['message'] = 'Assignment Not Possible!';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function addSubjTeachConfirmAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        if ($this->request->isPost()) {
            $teacherIds = $this->request->getPost('staff_list');
            $subjclasid = $this->request->getPost('subjclasid');
            $subject_id = $this->request->getPost('subjID');
            $action = $this->request->getPost('actions');
            if (!$subject_id) {
                $message['type'] = 'error';
                $message['message'] = 'Select atleast one Subjects';
                print_r(json_encode($message));
                exit;
            }
            if (!$teacherIds) {
                $message['type'] = 'error';
                $message['message'] = 'Select atleast one Teachers';
                print_r(json_encode($message));
                exit;
            }
            $staff_id = array();
            $staff_app_nos = explode(',', $teacherIds);
            foreach ($staff_app_nos as $staff_app_no) {
                $staff_no = explode('(', $staff_app_no);
                $staff_number = rtrim($staff_no[1], ')');
                $staff_id[] = StaffInfo::findfirst("loginid = '$staff_number'")->id;
            }
            $classmaster = ClassroomMaster::findFirstById($subjclasid);
            $entry_exist_true = 0;
            if ($action != 'edit') {
                $stuquery = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
                $students = StudentMapping::find(array(
                            'columns' => ' DISTINCT aggregate_key as aggregate_key',
                            'status = "Inclass" and ' . implode(' or ', $stuquery)
                ));
                foreach ($students as $stu) {
                    $subjrr = array();
                    $subjids = ControllerBase::getGrpSubjMasPossiblities(explode(',', $stu->aggregate_key));
                    $subjectsid = count($subjids) > 0 ? GroupSubjectsTeachers::find('id IN (' . implode(',', $subjids) . ')') : '';
                    if (count($subjectsid) > 0) {
                        foreach ($subjectsid as $svalue) {
                            $subjrr[] = $svalue->subject_id;
                        }
                    }
                    if (in_array($subject_id, $subjrr)) {
                        $entry_exist_true = 1;
                        $message['type'] = 'error';
                        $message['message'] = ' Selected Subject Already assigned';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
            if ($entry_exist_true == 0) {
                $subdivsubject = GroupSubjectsTeachers::findFirst("classroom_master_id = '$subjclasid' and subject_id = $subject_id") ?
                        GroupSubjectsTeachers::findFirst("classroom_master_id = '$subjclasid' and subject_id = $subject_id") :
                        new GroupSubjectsTeachers();
                $teach = $subdivsubject->teachers_id ? explode(',', $subdivsubject->teachers_id) : array();
                $teachfin = count($teach) > 0 ? array_unique(array_merge($teach, $staff_id)) : $staff_id;
                $subdivsubject->classroom_master_id = $subjclasid;
                $subdivsubject->teachers_id = implode(',', $teachfin);
                $subdivsubject->subject_id = $subject_id;
                if ($subdivsubject->save()) {
                    $message['type'] = 'success';
                    $message['message'] = 'Subjects and Teachers Added Successfully';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($subdivsubject->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    echo $error;
                    exit;
                }
            } else {
                $message['type'] = 'error';
                $message['message'] = 'Assignment Not Possible!';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function removeClassTeachersAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $teacherid = $this->request->getPost('teacherid');
            $classmasterid = $this->request->getPost('classmasterid');
            $clsteac = GroupClassTeachers::findFirst('classroom_master_id = ' . $classmasterid);
            $staff = explode(',', $clsteac->teachers_id);
            $teachid[] = $teacherid;
            $remain_id = array_diff($staff, $teachid);
            if (count($remain_id) > 0) {
                $clsteac->teachers_id = implode(',', $remain_id);
                if ($clsteac->save()) {
                    $message['type'] = 'success';
                    $message['message'] = 'Selected Staff Removed Successfully';
                    print_r(json_encode($message));
                    exit;
                } else {

                    foreach ($clsteac->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            } else {
                if ($clsteac->delete()) {
                    $message['type'] = 'success';
                    $message['message'] = 'Selected Staff Removed Successfully';
                    print_r(json_encode($message));
                    exit;
                } else {

                    foreach ($clsteac->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function removeSubjectTeachersAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $teacherid = $this->request->getPost('teacherid');
            $classmasterid = $this->request->getPost('classmasterid');
            $subjectid = $this->request->getPost('subject_id');
            $subteac = GroupSubjectsTeachers::findFirst('classroom_master_id = ' . $classmasterid . ' and subject_id = ' . $subjectid);
            $staff = explode(',', $subteac->teachers_id);
            if ($teacherid) {
                $teachid[] = $teacherid;
                $remain_id = array_diff($staff, $teachid);
                $subteac->teachers_id = implode(',', $remain_id);
                if (count($remain_id) > 0) {
                    $subteac->teachers_id = implode(',', $remain_id);
                    if ($subteac->save()) {
                        $message['type'] = 'success';
                        $message['message'] = 'Selected Staff Removed Successfully';
                        print_r(json_encode($message));
                        exit;
                    } else {

                        foreach ($subteac->getMessages() as $messages) {
                            $error .= $messages;
                        }
                        $message['type'] = 'error';
                        $message['message'] = $error;
                        print_r(json_encode($message));
                        exit;
                    }
                }
            } else {
                if ($subteac->delete()) {
                    $message['type'] = 'success';
                    $message['message'] = 'Selected Staff Removed Successfully';
                    print_r(json_encode($message));
                    exit;
                } else {

                    foreach ($subteac->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function addnewClassroomAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $cycleNode = ControllerBase::get_current_academic_year();
	$this->view->selectednode = $selectednode = $this->request->getPost('selectednode');
        $this->view->cycleID = $cycleNode->id;
    }

    public function allClassroomAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->currectacdyr = $currectacdyr = ControllerBase::getCurrentAcademicYear();
        $organizationalMas = OrganizationalStructureMaster::findFirst('upload_flag =1');
        $this->view->gradeList = $gradeList = OrganizationalStructureValues::find('org_master_id = ' . $organizationalMas->id . ' and parent_id = ' . $currectacdyr->id);
       
    }

    public function studentAssigningAction() {

        $this->tag->prependTitle("Student Assigning | ");
        $this->view->mastersubor = OrganizationalStructureMaster::find(array('columns' => ' distinct module, nodefont',
                    'is_subordinate=1'));
    }

    public function valueTreeJsonAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $rootid = OrganizationalStructureValues::findFirstByParentId(0)->id;
        $masterids = OrganizationalStructureMaster::findFirst(array('columns' => ' group_concat(id) as masids',
                    'is_subordinate=0'));
        $mas[] = $masterids->masids;
        $output = $this->find_childtreevaljson($rootid, implode(',', $mas));
        // echo'<pre>'; print_r($output);exit;
        echo json_encode($output);
        exit;
    }

    public function getChildNode($aid, $rcpathname = array()) {
        $name = OrganizationalStructureValues::findfirst('id = ' . $aid);
        $iscycle = OrganizationalStructureMaster::findFirstById($name->org_master_id);
        if (in_array($iscycle->module, array("structural", "StudentCycleNode", "StaffCycleNode"))) {
            $rcpathname[] = $name->id; //"(find_in_set($name->id, aggregate_key)>0)";
            $exist = OrganizationalStructureValues::find('parent_id =' . $aid);
            if (count($exist) > 0) {
                foreach ($exist as $chl) {
                    $rcpathname = AssigningController::getChildNode($chl->id, $rcpathname);
                }
            }
        }
        return $rcpathname;
    }

    public function find_childtreevaljson($aid, $masids) {
        $exist = OrganizationalStructureValues::find('parent_id =' . $aid);
        $nodeval = OrganizationalStructureValues::findFirst('id =' . $aid);
        $alchild = AssigningController::getChildNode($aid, array());
        $master = OrganizationalStructureMaster::findFirstById($nodeval->org_master_id);
        $mas = (strlen($master->name) > 5 ? substr($master->name, 0, 5) . "..." : $master->name);
        $chldnm = (strlen($nodeval->name) > 10 ? substr($nodeval->name, 0, 10) . "..." : $nodeval->name);
        $narr = preg_filter('/^([\d])*/', '(find_in_set("$0", aggregate_key)>0)', $alchild);
        $qry = $master->module == "structural" ? implode(' or ', $narr) : "find_in_set($aid, aggregate_key)>0";
        $stucount = (count($alchild) > 0 || $master->module != "structural" ) ? StudentMapping::findFirst(array('columns' => 'count(*) as cnt', $qry)) : '0';
        $stfcount = (count($alchild) > 0 || $master->module != "structural" ) ? StaffInfo::findFirst(array('columns' => 'count(*) as cnt', $qry)) : '0';
        if (count($exist) > 0) {

            $choutput['name'] = $mas . ': ' . $chldnm;
//            $choutput['alchild'] = implode(' or ',$alchild) ;
            $choutput['stucount'] = $stucount->cnt + $stfcount->cnt;
            $choutput['fullname'] = $master->name . ': ' . $nodeval->name;
            $choutput['nodeid'] = $nodeval->id;
            $choutput['groupnode'] = $master->name;
            $choutput['font'] = $master->nodefont;
            $choutput['content'] = '<div class="col-md-7"> '
                    . '<span class="mini-stat-icon orange"><i class="fa fa-child"></i></span>'
                    . '<div class="mini-stat-info"> <span>' . $stucount->cnt . '</span> Student</div>'
                    . ' </div><div class="col-md-5" style=" padding-left: 0px;">'
                    . '<span class="mini-stat-icon tar"><i class="fa fa-user"></i></span>'
                    . '<div class="mini-stat-info"> <span>' . $stfcount->cnt . '</span> Staff</div>'
                    . ' </div>';
            $choutput['fillcolor'] = $master->module == 'Cyclenode' ? $this->bgclass[round($nodeval->id % 10)]['highlight'] : $master->color;
            if ($master->promotion_status == 'above cycle node' || ($master->promotion_status == 'cycle node' && ($nodeval->status == 'C' ))) {
                $choutput['expandnodes'] = 1;
            }
            foreach ($exist as $chl) {
                $nodevalch = OrganizationalStructureValues::findFirst('id =' . $chl->id . ' and org_master_id IN (' . $masids . ')');
                if ($nodevalch) {
                    $choutput["children"][] = $this->find_childtreevaljson($chl->id, $masids);
                }
            }
        } else {
            $choutput['name'] = $mas . ': ' . $chldnm;
            $choutput['stucount'] = $stucount->cnt + $stfcount->cnt;
            $choutput['fullname'] = $master->name . ': ' . $nodeval->name;
            $choutput['font'] = $master->nodefont;
            $choutput['groupnode'] = $master->name;
            $choutput['nodeid'] = $nodeval->id;
            $choutput['fillcolor'] = '#ddd';
            $choutput['content'] = '<div class="col-md-7"> '
                    . '<span class="mini-stat-icon orange"><i class="fa fa-child"></i></span>'
                    . '<div class="mini-stat-info"> <span>' . $stucount->cnt . '</span> Student</div>'
                    . ' </div><div class="col-md-5" style=" padding-left: 0px;">'
                    . '<span class="mini-stat-icon tar"><i class="fa fa-user"></i></span>'
                    . '<div class="mini-stat-info"> <span>' . $stfcount->cnt . '</span> Staff</div>'
                    . ' </div>';
            if ($master->promotion_status == 'above cycle node' || ($master->promotion_status == 'cycle node' && ($nodeval->status == 'C' ))) {
                $choutput['expandnodes'] = 1;
            }
        }
        return $choutput;
    }

    public function listAllStfStuAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if ($this->request->isPost()) {
            $nodeid = $this->request->getPost('nodeid');
            $nodeval = OrganizationalStructureValues::findFirst('id =' . $nodeid);
            $master = OrganizationalStructureMaster::findFirstById($nodeval->org_master_id);
            $alchild = AssigningController::getChildNode($nodeid);
//        print_r(implode(',', $alchild));exit;
            $qry = $master->module == "structural" ? implode(' or ', $alchild) : "find_in_set($nodeid, aggregate_key)>0";
            $stu = (count($alchild) > 0 || $master->module != "structural" ) ? StudentHistory::find($qry) : '0';
            $this->view->students = $stu;
            $this->view->stufromnode = $nodeid;
            $this->view->nodetitle = ControllerBase::getNameForKeysFullSearch($nodeid);
        }
    }

    public function listAllStfAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if ($this->request->isPost()) {
            $nodeid = $this->request->getPost('nodeid');
            $nodeval = OrganizationalStructureValues::findFirst('id =' . $nodeid);
            $master = OrganizationalStructureMaster::findFirstById($nodeval->org_master_id);
            $alchild = AssigningController::getChildNode($nodeid);
//        print_r(implode(',', $alchild));exit;
            $qry = $master->module == "structural" ? implode(' or ', $alchild) : "find_in_set($nodeid, aggregate_key)>0";
            $stf = (count($alchild) > 0 || $master->module != "structural" ) ? StaffInfo::find($qry) : '0';
            $this->view->staff = $stf;
            $this->view->stufromnode = $nodeid;
            $this->view->nodetitle = ControllerBase::getNameForKeysFullSearch($nodeid);
        }
    }

    public function copyNodeToStuAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $selectedstu = $this->request->getPost('loginid');
            $nodeid = $this->request->getPost('nodeid');
            $stufromnode = $this->request->getPost('stufromnode');
            $node = OrganizationalStructureValues::findFirstById($nodeid);
            $nodemas = OrganizationalStructureMaster::findFirstById($node->org_master_id);
            if ($nodemas->module == "structural") {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Students cannot be moved/copied above the cycle node </div>';
                print_r(json_encode($message));
                exit;
            } else {
                $i = 0;
                foreach ($selectedstu as $stu) {
                    $studentMap = StudentMapping::findFirst('student_info_id = ' . $stu . ' and find_in_set(' . $stufromnode . ', aggregate_key)');
                    $studentHis = StudentHistory::findFirst('student_info_id = ' . $stu . ' and find_in_set(' . $stufromnode . ', aggregate_key)');
                    $getCycleRoot = ControllerBase::getCommonIdsForKeys($nodeid);
                    $array1 = explode('-', $getCycleRoot[0]);
                    $array2 = explode(',', $studentHis->aggregate_key);
                    $arraymerged = array_unique(array_merge($array1, $array2));
                    $studentHis->aggregate_key = implode(',', $arraymerged);
                    if ($studentMap) {
                        $studentMap->aggregate_key = implode(',', $arraymerged);
                        if (!$studentMap->save()) {
                            foreach ($studentMap->getMessages() as $messages) {
                                $error .= $messages;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                    if (!$studentHis->save()) {
                        foreach ($studentHis->getMessages() as $messages) {
                            $error .= $messages;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                    $i++;
//                    print_r($studentMap);
//                    exit;
                }
                if ($i > 0) {

                    $message['type'] = 'success';
                    $message['message'] = '<div class="alert alert-block alert-success fade in">Successfully saved!</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function moveNodeToStuAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $selectedstu = $this->request->getPost('loginid');
            $nodeid = $this->request->getPost('nodeid');
            $stufromnode = $this->request->getPost('stufromnode');
            $node = OrganizationalStructureValues::findFirstById($nodeid);
            $nodemas = OrganizationalStructureMaster::findFirstById($node->org_master_id);

            $snode = OrganizationalStructureValues::findFirstById($stufromnode);
            $snodemas = OrganizationalStructureMaster::findFirstById($snode->org_master_id);
            if ($nodemas->module == "structural" || $snodemas->module == "structural") {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Students cannot be moved/copied above the cycle node </div>';
                print_r(json_encode($message));
                exit;
            } else {
                $i = 0;
                foreach ($selectedstu as $stu) {
                    $studentMap = StudentMapping::findFirst('student_info_id = ' . $stu . ' and find_in_set(' . $stufromnode . ', aggregate_key)');
                    $studentHis = StudentHistory::findFirst('student_info_id = ' . $stu . ' and find_in_set(' . $stufromnode . ', aggregate_key)');
                    $getCycleRoot = ControllerBase::getCommonIdsForKeys($nodeid);
                    $array1 = explode('-', $getCycleRoot[0]);
                    $arraymerged = array_unique($array1);
                    $studentHis->aggregate_key = implode(',', $arraymerged);
                    if ($studentMap) {
                        $studentMap->aggregate_key = implode(',', $arraymerged);
                        if (!$studentMap->save()) {
                            foreach ($studentMap->getMessages() as $messages) {
                                $error .= $messages;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                    if (!$studentHis->save()) {
                        foreach ($studentHis->getMessages() as $messages) {
                            $error .= $messages;
                        }
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                    $i++;
//                    print_r($studentMap);
//                    exit;
                }
                if ($i > 0) {

                    $message['type'] = 'success';
                    $message['message'] = '<div class="alert alert-block alert-success fade in">Successfully moved!</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function copyNodeToStfAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $selectedstu = $this->request->getPost('loginid');
            $nodeid = $this->request->getPost('nodeid');
            $stufromnode = $this->request->getPost('stufromnode');
            $node = OrganizationalStructureValues::findFirstById($nodeid);
            $nodemas = OrganizationalStructureMaster::findFirstById($node->org_master_id);
            if ($nodemas->module == "group") {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Students cannot be moved/copied below the cycle node </div>';
                print_r(json_encode($message));
                exit;
            } else {
                $i = 0;
                foreach ($selectedstu as $stu) {
                    $staff = StaffInfo::findFirstById($stu);
                    $getCycleRoot = ControllerBase::getCommonIdsForKeys($nodeid);
                    $array1 = explode('-', $getCycleRoot[0]);
                    $array2 = explode(',', $staff->aggregate_key);
                    $arraymerged = array_unique(array_merge($array1, $array2));
                    $staff->aggregate_key = implode(',', $arraymerged);
                    if ($staff) {
                        if (!$staff->save()) {
                            foreach ($staff->getMessages() as $messages) {
                                $error .= $messages;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                    $i++;
                }
                if ($i > 0) {

                    $message['type'] = 'success';
                    $message['message'] = '<div class="alert alert-block alert-success fade in">Successfully saved!</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function moveNodeToStfAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $selectedstu = $this->request->getPost('loginid');
            $nodeid = $this->request->getPost('nodeid');
            $stufromnode = $this->request->getPost('stufromnode');
            $node = OrganizationalStructureValues::findFirstById($nodeid);
            $nodemas = OrganizationalStructureMaster::findFirstById($node->org_master_id);

            $snode = OrganizationalStructureValues::findFirstById($stufromnode);
            $snodemas = OrganizationalStructureMaster::findFirstById($snode->org_master_id);
            if ($nodemas->module == "group" || $snodemas->module == "group") {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Students cannot be moved/copied below the cycle node </div>';
                print_r(json_encode($message));
                exit;
            } else {
                $i = 0;
                foreach ($selectedstu as $stu) {
                    $staff = StaffInfo::findFirstById($stu);
                    $getCycleRoot = ControllerBase::getCommonIdsForKeys($nodeid);
                    $array1 = explode('-', $getCycleRoot[0]);
                    $arraymerged = array_unique($array1);
                    $staff->aggregate_key = implode(',', $arraymerged);
                    if ($staff) {
                        if (!$staff->save()) {
                            foreach ($staff->getMessages() as $messages) {
                                $error .= $messages;
                            }
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                    $i++;
                }
                if ($i > 0) {
                    $message['type'] = 'success';
                    $message['message'] = '<div class="alert alert-block alert-success fade in">Successfully moved!</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function loadSubjTeacherAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->classmaterid = $classmaterid = $this->request->getPost('classmasterid');
            $this->view->classmaster = $classmaster = ClassroomMaster::findFirstById($classmaterid);
            $pattern = '/[,-]/';
            $Totarr = (preg_split($pattern, $classmaster->aggregated_nodes_id));
            $subjpids = ControllerBase::getAlSubjChildNodes($Totarr);
            $subjects = ControllerBase::getAllPossibleSubjectsold($subjpids);
            $this->view->subjects = $subjects;
        }
    }

    public function loadSubjTeacherOnlyAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->classmaterid = $classmaterid = $this->request->getPost('classmasterid');
            $this->view->subid = $subid = $this->request->getPost('subid');
        }
    }

    public function loadStaffInchargeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->classmaterid = $classmaterid = $this->request->getPost('classmasterid');
        }
    }

    public function loadStuFromExcelAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            if ($this->request->hasFiles() == true) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $ext = explode('.', $file->getName());
                    if (in_array($ext[1], array('xlsx', 'xls'))) {
                        $filename = 'Student_excel_add' . '_' . date('Y-m-d-h-i-s') . '.' . $ext[1];
                        $file->moveTo(UPLOAD_DIR . $filename);
                    } else {
                        $message['type'] = 'error';
                        $message['message'] = "<div class='alert alert-error'>Invalid file extension</div>";
                        print_r(json_encode($message));
                        exit;
                    }
                }
                $user = $this->request->getPost('usertyp');
                if (file_exists(UPLOAD_DIR . $filename)) {
                    $objPHPExcel = PHPExcel_IOFactory::load(UPLOAD_DIR . $filename);
                    $maxCol = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
                    $maxRow = $objPHPExcel->getActiveSheet()->getHighestDataRow();
                    $row = $objPHPExcel->getActiveSheet()->rangeToArray('A' . 1 . ':' . $maxCol . 1);
                    $header = ($row[0]);

                    $headercheck = array("Name", "Gender", "Date_of_Birth", "Date_of_Joining");
                    $ardiff = array_diff($header, $headercheck);

                    $i = 0;
                    $error = $validation_log = '';
                    $inv_dupli_row = array();
//                    echo'hai';
//                    print_r(($headercheck));
//                    print_r(($header));
//                    print_r(($ardiff));
//                    echo'hai';
//                    exit;
                    if (count($ardiff) == 0) {
                        $j = 0;
                        for ($i = 2; $i <= $maxRow; $i++) {
                            $j++;
                            $row = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $i . ':' . $maxCol . $i);
                            $row = $row[0];
                            $isempty = implode("", $row);
                            if ($isempty) {
                                //validation begins
                                if (($row[0] == '') || ($row[1] == '') || ($row[2] == '') || ($row[3] == '')) {
                                    $error.= "Missing Value in " . ($i) . ' row.<br/>';
                                }

                                /* Staff Name Validation  */
                                if (!preg_match('/^[a-z][a-z ]*$/i', $row[0])) {
                                    $error.= "Invalid  Student Name in " . ($i) . ' row. (<i> Hint : Only alphabets and space are allowed!</i>)<br/>';
                                }

                                /*  Gender Validation  */
                                if (!in_array($row[1], array('Female', 'Male'))) {
                                    $error.= "Invalid  Gender in " . ($i) . ' row.<br/>';
                                }

                                /*  Birth Date Validation  */
                                if (!preg_match("/^([1-9]|0[1-9]|[1-2][0-9]|3[0-1])\/([1-9]|0[1-9]|1[0-2])\/[0-9]{4}$/", $row[2])) {
                                    $error.= "Invalid  Date of Birth in " . ($i) . ' row.(<i> Hint : date format is dd/mm/yyyy </i>)<br/>';
                                }
                                /*  Appointment Date Validation  */
                                if (!preg_match("/^([1-9]|0[1-9]|[1-2][0-9]|3[0-1])\/([1-9]|0[1-9]|1[0-2])\/[0-9]{4}$/", $row[3])) {
                                    $error.= "Invalid  Date of Admission in " . ($i) . ' row.(<i> Hint : date format is dd/mm/yyyy </i>)<br/>';
                                }

                                $birthDt = date_timestamp_get(date_create_from_format('d/m/Y', $row[2]));
                                $AppDt = date_timestamp_get(date_create_from_format('d/m/Y', $row[3]));
                                if ($birthDt > $AppDt) {
                                    $error.= "Invalid  Birth/Admission Date in " . ($i) . ' row.(<i> Hint : Birth date greater than Admission date </i>)<br/>';
                                }
                                if ($birthDt > time() || $AppDt > time()) {
                                    $error.= "Invalid  Birth/Admission Date in " . ($i) . ' row.(<i> Hint : Birth date or Admission date is future date </i>)<br/>';
                                }
                            }
                        }
//                print_r($error);exit;
                        if ($error != '') {

                            $message['type'] = 'error';
                            $message['message'] = "<div class='alert alert-error'>Aborted!<br/>" . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                        if ($j > 0) {

                            try {
                                $j = 0;
                                $html = '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading " role="tab" id="headingOne" style=" background-color: #1fb5ac">
            <h4 class="panel-title">
                <a role="button"  data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    ' . ucfirst($user) . '
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" style=" max-height: 386px;">
            <div class="stu-list " > 
                <ul id="draggable"  class="event-list">';
                                for ($i = 2; $i <= $maxRow; $i++) {
//                                echo $i;
                                    $param = $Pparam = array();
                                    $row = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $i . ':' . $maxCol . $i);
                                    $row = $row[0];
                                    $isempty = implode("", $row);
                                    if ($isempty) {
                                        $j++;
                                        $html .="<li class=' ui-state-selected'  
                                stuname ='$row[0]'
                                gender ='$row[1]'
                                dob ='$row[2]'
                                doj ='$row[3]'
                                 >"
                                                . "  <span   class='mini-stat-icon tar stu-icon' >" . ( substr($row[0], 0, 1)) . "</span> " .
                                                "<span title= '$row[0]' class='stu-name'>" .
                                                (strlen($row[0]) > 15 ? substr($row[0], 0, 15) . "..." : $row[0])
                                                . "</span><a href='#' class='event-select event-close' ><i class='fa fa-check'></i></a>  </li>";
                                    }
                                }
                                $html .='</ul>  </div>  </div> </div><br> </div>';
                                $html .= '
<script>

    var body = d3.selectAll("rect");

    d3.selectAll("rect").on("mouseover", function (d, i) {
        DragDropManager.droppable = d;
    });
    d3.selectAll("rect").on("mouseout", function (e) {
        DragDropManager.droppable = null;
    });

    $("#collapseOne").niceScroll();
    var selectedClass = "ui-state-selected",
            clickDelay = 600,
            // click time (milliseconds)
            lastClick, diffClick,
            btnclass = "event-select"; // timestamps

    $("#draggable li")
            // Script to deferentiate a click from a mousedown for drag event
            .bind("mousedown mouseup", function (e) {
                if (e.type == "mousedown") {
                    lastClick = e.timeStamp; // get mousedown time
                } else {
                    diffClick = e.timeStamp - lastClick;
                    if (diffClick < clickDelay) {
                        // add selected class to group draggable objects
                        $(this).toggleClass(selectedClass);
                        $(this).find(".event-close").toggleClass(btnclass);
                    }
                }
            })
            .draggable({
                cursor: "move",
                cursorAt: {top: -2, left: 2}, opacity: 0.7,
                helper: function (event) {
                    $ele = $("<span class=\'label label-info\'> " + $("." + selectedClass).length + " ' . ucfirst($user) . '(s) selected</span>");
                    return $ele;
                },
                revertDuration: 10,
                start: function (e, ui) {
                },
                stop: function (e, ui) {
                    // reset group positions
                    $("." + selectedClass).css({
                        top: 0,
                        left: 0
                    });
                    body.style("cursor", function () {
                        return "";
                    });
                    matches = DragDropManager.draggedMatchesTarget();
                    if (matches) {
                        var selectedStuFul = {};
                        var selectedStuNew = [];
                        $("." + selectedClass).each(function () {
                            var selectedStu = {};
                            selectedStu.nodeid = DragDropManager.droppable.nodeid;
                            selectedStu.stuname = $(this).attr("stuname");
                            selectedStu.gender = $(this).attr("gender");
                            selectedStu.dob = $(this).attr("dob");
                            selectedStu.doj = $(this).attr("doj");
                            selectedStuNew.push(selectedStu)
                        })
                        selectedStuFul.selectedStuNew = selectedStuNew;
                            selectedStuFul.user = "' . $user . '";
                        $("#formModal .formBox-title").html("Adding Student");
                        $("#formModal .formBox-body").html("You are trying to add "+ ($("." + selectedClass).length) + " ' . ucfirst($user) . '(s) in <b> " + DragDropManager.droppable.fullname + "</b><br><br>"
                             + "1. Add - This action will admit selected student in selected node.<br>");
                        $("#formModal").modal("show")
                        $("#formModal .modal-footer").html(" ");
                        $("#formModal .modal-footer").append("<button class=\'btn btn-success\' type=\'button\' id=\'confirm_save\'>Add</button>");
                        $("#formModal .modal-footer").append(" <button data-dismiss=\'modal\' class=\'btn btn-default\' type=\'button\'  id=\'confirm_close\'>Cancel</button>");
                        $("#confirm_save").unbind("click").click(function () {
                            $("#formModal").modal("hide");
                            assignStudentSettings.addUser(selectedStuFul)

                        });

                    } else if (!DragDropManager.draggedMatchesTarget())
                        return;
                },
                drag: function (e, ui) {
                    // set selected group position to main dragged object
                    // this works because the position is relative to the starting position
                    $("." + selectedClass).css({
                        top: ui.position.top,
                        left: ui.position.left,
//                        "z-index": 9999
                    });
                    body.style("cursor", function () {
                        matches = DragDropManager.draggedMatchesTarget();
                        return (matches) ? "copy" : "move";
                    });
                }
            });


</script>';
                                if ($j > 0) {
                                    print_r($html);
                                    exit;
                                } else {
                                    $message['type'] = 'error';
                                    $message['message'] = "<div class='alert alert-error'>Import failed!<br/>Empty file</div>";
                                    print_r(json_encode($message));
                                    exit;
                                }
                            } catch (Exception $ex) {
                                $message['type'] = 'error';
                                $message['message'] = "<div class='alert alert-error'>Import failed!<br/>" . $ex . '</div>';
                                print_r(json_encode($message));
                                exit;
                            }
                        } else {

                            $message['type'] = 'error';
                            $message['message'] = "<div class='alert alert-error'>Validation failed!<br/>Empty file</div>";
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        $message['type'] = 'error';
                        $message['message'] = "<div class='alert alert-error'> Invalid File. Import failed! (<i> Hint : Column header mismatch</i>)<br/></div>";
                        print_r(json_encode($message));
                        exit;
                    }
                } else {
                    $message['type'] = 'error';
                    $message['message'] = "<div class='alert alert-error'> File not found</div>";
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function addStuToNodeAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        try {
            if ($this->request->isPost()) {
//                print_r();exit;
                $userlist = $this->request->getPost('selectedStuNew');
                $stucnt = 0;
                foreach ($userlist as $stu) {
                    $dob = $stu['dob'];
                    $doj = $stu['doj'];
                    $gender = $stu['gender'];
                    $stuname = $stu['stuname'];
                    $nodeid = $stu['nodeid'];
                    $getCycleRoot = ControllerBase::getCommonIdsForKeys($nodeid);
                    $array1 = explode('-', $getCycleRoot[0]);
                    $arraymerged = array_unique($array1);
                    $aggregate_key = implode(',', $arraymerged);

//                    print_r($aggregate_key);exit;
//                    $admission_prefix = (Settings::findFirstByVariableName('admission_prefix')->variableValue);

                    $admissionNumPrfix = Settings::findFirstByVariableName('admission_prefix') ?
                            Settings::findFirstByVariableName('admission_prefix') : new Settings();
                    $admissionNumSufix = Settings::findFirstByVariableName('admission_sufix') ?
                            Settings::findFirstByVariableName('admission_sufix') : new Settings();
                    $prefix = $admissionNumPrfix ? $admissionNumPrfix->variableValue : '';
                    $sufix = $admissionNumSufix ? $admissionNumSufix->variableValue : '';
                    $stringLen = ControllerBase::getStudentPad('student');
                    $admission_prefix = $prefix . $stringLen. $sufix;
                    $stumax = StudentInfo::find();
                    $adminNumArr = array();
                    foreach ($stumax as $stuAdmin) {
                        $adminNumArr[] = $stuAdmin->Admission_no; // ltrim ($stuAdmin->loginid, 's');;
                    }
//                 print_r($adminNumArr);
                    $arraySorted = natsort($adminNumArr);
//                print_r($adminNumArr);
                    $lastId = array_pop($adminNumArr);

                    $asterikstr = str_replace("*", "", $admission_prefix, $asterikcount);
                    $formartArr = explode('*', $admission_prefix);
                    $nextAdminNumber = str_replace($formartArr, "", $lastId);
                    $nextgennum = $nextAdminNumber + 1;

                    $nextgennum = str_pad($nextgennum, $asterikcount, "0", STR_PAD_LEFT);

                    $i = 0;
                    $nextAdminNo = '';
                    foreach ($formartArr as $formatStr):
                        if ($formatStr != '') {
                            $nextAdminNo .= $formatStr;
                        } else {
                            if ($i == 0)
                                $nextAdminNo .= $nextgennum;
                            $i = 1;
                        }
                    endforeach;
                    ##Student Login  
                    $param = array(
                        'appkey' => APPKEY,
                        'subdomain' => SUBDOMAIN,
                        'businesskey' => BUSINESSKEY);
                    $param['slogin'] = 's' . $nextAdminNo;
                    $param['plogin'] = 'p' . $nextAdminNo;
                    $param['password'] = date('d/m/Y', $dob);
                    $param['email'] = 's' . $nextAdminNo . '@' . SUBDOMAIN . '.edusparrow.com';
                    $data_string = json_encode($param);
                    $response = IndexController::curlIt(USERAUTHAPI . 'createStudentLogin', $data_string);
//                   print_r($response);;exit;
                    $loginCreated = json_decode($response);
//          print_r($loginCreated);;
                    if (!$loginCreated->status) {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                    ##Student LOGIN CREATION END
                    if ($loginCreated->status == 'ERROR') {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $loginCreated->messages . '</div>';
                        print_r(json_encode($message));
                        exit;
                    } else if ($loginCreated->status == 'SUCCESS' && $loginCreated->data->id > 0) {

                        ##MAIL LOGIN
                        $param = array(
                            'domain' => SUBDOMAIN,
                            'login' => 's' . $nextAdminNo,
                            'password' => date('d/m/Y', $dob));
                        $mail_data_string = json_encode($param);
//                      print_r($param); 
                        $response = IndexController::curlIt(MAILSERVICEAPI . 'createWebMailUser', $mail_data_string);
//                   print_r($response);;
                        $mailloginCreated = json_decode($response);
//                    print_r($mailloginCreated);
//                    exit;
                        if (!$mailloginCreated->status) {
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                        if ($mailloginCreated->status == 'ERROR') {
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $mailloginCreated->messages . '</div>';
                            print_r(json_encode($message));
                            exit;
                        } else if ($mailloginCreated->status == 'SUCCESS' && $mailloginCreated->data->id) {

                            ## Admit Student
                            $stuInfo = new StudentInfo();
                            $stuMapping = new StudentMapping();
                            $stuInfo->assign(array(
                                'Admission_no' => $nextAdminNo,
                                'application_no' => 0,
                                'Student_Name' => $stuname,
                                'Gender' => $gender,
                                'Date_of_Birth' => $dob,
                                'Date_of_Joining' => $doj, //$application->Date_of_Joining,
                                'Email' => 's' . $nextAdminNo . '@' . SUBDOMAIN . '.edusparrow.com',
                                'loginid' => $loginCreated->data->stulogin,
                                'parent_loginid' => $loginCreated->data->parentlogin,
                            ));

                            $stuInfo->created_by = $uid;
                            $stuInfo->created_date = time();
                            $stuInfo->modified_by = $uid;
                            $stuInfo->modified_date = time();
                            if ($stuInfo->save()) {
                                $stuMapping->student_info_id = $stuInfo->id;
                                $stuMapping->aggregate_key = $aggregate_key;

                                $admit_stu_val = Settings::findFirst('variableName ="admit_student"');

                                $stu_status = 'Admitted';
                                if ($admit_stu_val->variableValue == '1')
                                    $stu_status = 'Inclass';

                                $stuMapping->status = $stu_status;

                                $stuAcademic = new StudentHistory();
                                $stuAcademic->student_info_id = $stuInfo->id;
                                $stuAcademic->aggregate_key = $aggregate_key;
                                $stuAcademic->status = $stu_status;
                                if (!$stuAcademic->save()) {
                                    $error = '';
                                    foreach ($stuAcademic->getMessages() as $messages) {
                                        $error .= $messages;
                                    }
                                    $message['type'] = 'error';
                                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                    print_r(json_encode($message));
                                    exit;
                                } else if (!$stuMapping->save()) {
                                    $error = '';
                                    foreach ($stuMapping->getMessages() as $messages) {
                                        $error .= $messages;
                                    }
                                    $message['type'] = 'error';
                                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                    print_r(json_encode($message));
                                    exit;
                                } else {
                                    $admit_stu_val = Settings::findFirst('variableName ="admit_student"');
                                    $stu_status = 'Admitted';
                                    if ($admit_stu_val->variableValue == '1')
                                        $stu_status = 'Inclass';
                                    $gender = ($stuInfo->Gender == '2') ? 'Male' : 'Female';

                                    /* $params['username'] = $stuInfo->loginid;
                                      $params['password'] = date('d/m/Y', $stuInfo->Date_of_Birth);
                                      $params['fullname'] = $stuInfo->Student_Name;
                                      $params['email_address'] = $stuInfo->Email;
                                      $params['rolenames'] = 'Student';
                                      $params['aggregateid'] = $stuMapping->aggregate_key;
                                      // print_r($params);
                                      $data_string = json_encode($params);
                                      $responseParam = IndexController::curlIt(CALENDARAPI . 'createUserPricipal', $data_string);
                                      $calloginCreated = json_decode($responseParam);
                                      //                             print_r($calloginCreated);
                                      if (!$calloginCreated->status) {
                                      $message['type'] = 'error';
                                      $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                                      print_r(json_encode($message));
                                      exit;
                                      } else if ($calloginCreated->status == 'ERROR') {
                                      $message['type'] = 'error';
                                      $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $loginCreated->messages . '</div>';
                                      print_r(json_encode($message));
                                      exit;
                                      } else if ($calloginCreated->status == 'SUCCESS' && $calloginCreated->data->id > 0) { */
                                    $message['type'] = 'success';
                                    $stucnt++;
//                                    }
                                }
                            } else {
                                $error = '';
                                foreach ($stuInfo->getMessages() as $message) {
                                    $error .= $message;
                                }
                                $message['type'] = 'error';
                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                print_r(json_encode($message));
                                exit;
                            }
                        }
                        ##MAIL LOGIN CREATION END
                    } else {
                        $error = '';
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-error">Login Creation failed!<br/>';
                        print_r(json_encode($message));
                        exit;
                    }
                }

                if ($stucnt > 0) {
                    $message['type'] = 'success';
                    $message['message'] = "<div class='alert alert-success'>Completed  !<br/>Total Records [ ($stucnt) ]Inserted! </div>";
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function addStfToNodeAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        try {
            if ($this->request->isPost()) {
//                print_r();exit;
                $userlist = $this->request->getPost('selectedStuNew');
                $stucnt = 0;
                foreach ($userlist as $stu) {
                    $dob = $stu['dob'];
                    $doj = $stu['doj'];
                    $gender = $stu['gender'];
                    $stuname = $stu['stuname'];
                    $nodeid = $stu['nodeid'];
                    $getCycleRoot = ControllerBase::getCommonIdsForKeysFullSearch($nodeid);
                    $array1 = explode('-', $getCycleRoot[0]);
                    $arraymerged = array_unique($array1);
                    $aggregate_key = implode(',', $arraymerged);

//                    print_r($aggregate_key);exit;
//                    $appointment_prefix = (Settings::findFirstByVariableName('appointment_prefix')->variableValue);

                    $admissionNumPrfix = Settings::findFirstByVariableName('appointment_prefix') ?
                            Settings::findFirstByVariableName('appointment_prefix') : new Settings();
                    $admissionNumSufix = Settings::findFirstByVariableName('appointment_sufix') ?
                            Settings::findFirstByVariableName('appointment_sufix') : new Settings();
                    $prefix = $admissionNumPrfix ? $admissionNumPrfix->variableValue : '';
                    $sufix = $admissionNumSufix ? $admissionNumSufix->variableValue : '';
                    
                    $stringLen = ControllerBase::getStudentPad('staff');
                    $appointment_prefix = $prefix . $stringLen. $sufix;
                    $stumax = StaffInfo::find();
                    $adminNumArr = array();

                    foreach ($stumax as $stuAdmin) {
                        $adminNumArr[] = $stuAdmin->appointment_no;
                    }

                    $arraySorted = natsort(array_reverse($adminNumArr));
                    $lastId = array_pop($adminNumArr);
                    $asterikcount = 0;
                    $asterikstr = str_replace("*", "", $appointment_prefix, $asterikcount);
                    $formartArr = explode('*', $appointment_prefix);
                    $nextAdminNumber = str_replace($formartArr, "", $lastId);
                    $nextgennum = $nextAdminNumber + 1;
                    $countOfZeros = abs(strlen($nextgennum) - $asterikcount);
                    $nextgennum = str_pad($nextgennum, $asterikcount, "0", STR_PAD_LEFT);
                    $i = 0;
                    $nextAdminNo = '';
                    foreach ($formartArr as $formatStr):
                        if ($formatStr != '') {
                            $nextAdminNo .= $formatStr;
                        } else {
                            if ($i == 0)
                                $nextAdminNo .= $nextgennum;
                            $i = 1;
                        }
                    endforeach;
                    $appointment_no = $nextAdminNo;

                    $param = array(
                        'appkey' => APPKEY,
                        'subdomain' => SUBDOMAIN,
                        'businesskey' => BUSINESSKEY);
                    $param['slogin'] = 'e' . $appointment_no;
                    $param['password'] = $dob;
                    $param['email'] = 'e' . $appointment_no . '@' . SUBDOMAIN . '.edusparrow.com';
                    $data_string = json_encode($param);
                    $response = IndexController::curlIt(USERAUTHAPI . 'createStaffLogin', $data_string);
//                   print_r($response);;
                    $loginCreated = json_decode($response);
//                print_r($loginCreated);
                    if (!$loginCreated->status) {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                        print_r(json_encode($message));
                        exit;
                    } else if ($loginCreated->status == 'ERROR') {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $loginCreated->messages . '</div>';
                        print_r(json_encode($message));
                        exit;
                    } else if ($loginCreated->status == 'SUCCESS' && $loginCreated->data->id > 0) {
                        ##MAIL LOGIN
                        $param = array(
                            'domain' => SUBDOMAIN,
                            'login' => 'e' . $appointment_no,
                            'password' => $dob);
                        $mail_data_string = json_encode($param);
//                      print_r($param); 
                        $response = IndexController::curlIt(MAILSERVICEAPI . 'createWebMailUser', $mail_data_string);
//                   print_r($response);;
                        $mailloginCreated = json_decode($response);
//                    print_r($mailloginCreated);
//                    exit;
                        if (!$mailloginCreated->status) {
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                        if ($mailloginCreated->status == 'ERROR') {
                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $mailloginCreated->messages . '</div>';
                            print_r(json_encode($message));
                            exit;
                        } else if ($mailloginCreated->status == 'SUCCESS' && $mailloginCreated->data->id) {


                            $staff = new StaffInfo();
                            $staff->assign(array(
                                'appointment_no' => $appointment_no,
                                'aggregate_key' => $aggregate_key,
                                'Staff_Name' => $stuname,
                                'Gender' => $gender,
                                'Date_of_Birth' => $dob,
                                'Date_of_Joining' => $doj,
                                'Email' => 'e' . $appointment_no . '@' . SUBDOMAIN . '.edusparrow.com',
                                'loginid' => $loginCreated->data->stflogin,
                                'status' => 'Appointed'
                            ));

                            if ($staff->save()) {

                                /* $params['username'] = $staff->loginid;
                                  $params['password'] = date('d/m/Y', $staff->Date_of_Birth);
                                  $params['fullname'] = $staff->Staff_Name;
                                  $params['email_address'] = $staff->Email;
                                  $params['rolenames'] = 'Staff';
                                  $params['aggregateid'] = $staff->aggregate_key;
                                  //                            print_r($params);
                                  // print_r(CALENDARAPI . 'createUserPricipal');
                                  $data_string = json_encode($params);
                                  $responseParam = IndexController::curlIt(CALENDARAPI . 'createUserPricipal', $data_string);
                                  //                             print_r($responseParam);exit;
                                  $calloginCreated = json_decode($responseParam);
                                  if (!$calloginCreated->status) {
                                  $message['type'] = 'error';
                                  $message['message'] = '<div class="alert alert-block alert-danger fade in">Internal server error</div>';
                                  print_r(json_encode($message));
                                  exit;
                                  } else if ($calloginCreated->status == 'ERROR') {
                                  $message['type'] = 'error';
                                  $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $loginCreated->messages . '</div>';
                                  print_r(json_encode($message));
                                  exit;
                                  } else if ($calloginCreated->status == 'SUCCESS' && $calloginCreated->data->id > 0) { */
                                $stucnt++;
//                                }
                            } else {

                                foreach ($staff->getMessages() as $messages) {
                                    $error .= $messages . '<br>';
                                }

                                $message['type'] = 'error';
                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                print_r(json_encode($message));
                                exit;
                            }
                        }
                    }
                }

                if ($stucnt > 0) {
                    $message['type'] = 'success';
                    $message['message'] = "<div class='alert alert-success'>Completed  !<br/>Total Records [ ($stucnt) ]Inserted! </div>";
                    print_r(json_encode($message));
                    exit;
                }
            }
        } catch (Exception $e) {
            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function updateClassgroupStudentsAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        if ($this->request->isPost()) {

            $stuids = $this->request->getPost('sid');
            $classroomid = $this->request->getPost('classroomid');
//            print_r($stuids); exit;
            $classmaster = ClassroomMaster::findFirstById($classroomid);
            if (!$classmaster) {
                $message['type'] = 'error';
                $message['message'] = 'Invalid class group';
                print_r(json_encode($message));
                exit;
            }
            $stuids = count($stuids) > 0 ? array_unique($stuids) : '';
            if ($stuids && count($stuids) > 0) {
                $stuClassgrp = ClassgroupStudents::findFirst("classroom_master_id = $classroomid") ?
                        ClassgroupStudents::findFirst("classroom_master_id = $classroomid") :
                        new ClassgroupStudents();
                $stuClassgrp->classroom_master_id = $classroomid;
                $stuClassgrp->students_id = implode(',', $stuids);
//                print_r($stuClassgrp); exit;
                if ($stuClassgrp->save()) {
                    $message['type'] = 'success';
                    $message['message'] = 'Class group students updated successfully';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($stuClassgrp->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    echo $error;
                    exit;
                }
            } else {
                $message['type'] = 'error';
                $message['message'] = 'Select atleast one student';
                print_r(json_encode($message));
                exit;
            }
        }
    }

}
