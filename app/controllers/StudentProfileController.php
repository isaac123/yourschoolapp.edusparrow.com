<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class StudentProfileController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Student Profile | ");
//
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs('js/studenthomepage/dashboard.js');
//        $this->assets->addJs('js/studenthomepage/rating.js');
//
//        $this->assets->addJs('js/studenthomepage/highcharts.js');
//        $this->assets->addJs('js/studenthomepage/exporting.js');
//        $this->assets->addJs('js/studenthomepage/chartStyle.js');
//        $this->assets->addJs('js/video/video.js');
        $this->view->triggerClick = $this->request->get('action') ? $this->request->get('action') : 'overviewtop_section';

        $this->assets->addJs("js/underscore/underscore-min.js");
        //echo '<pre>';
        //Get the identity
        $identity = $this->auth->getIdentity();

        //Get the identity
        if ($this->request->isGet()) {
            $studentId = $this->request->get('studentId');
        }
        //Extract the login name for identity and use it to get details of the student

        $this->view->student_record = $student_record = StudentInfo::findFirstById($studentId);
        if ($student_record) {

            //From the controller base get the division and subdivision names
            $divname = ControllerBase::get_division_name_student();
            //Get the student's division and subdivision
            $divval = DivisionValues::findfirst('id = ' . $student_record->Division_Class);
            $subDivValId = $student_record->Subdivision;
            $subdivval = $student_record->Subdivision ?
                    SubDivisionValues::find('id IN ( ' . $student_record->Subdivision . ')') : '';

            $subdiv = array();
            foreach ($subdivval as $subdivNV) {
                $subdivname = SubDivisionMaster::findFirstById($subdivNV->subDivID)->subdivname;
                $subdiv[] = array(
                    'name' => $subdivname,
                    'value' => $subdivNV->name);
            }

            $this->view->subdiv = $subdiv;
            //Get the photo from StudentGeneralMaster
            $student_profile = StudentGeneralMaster::findfirst('student_id = ' . $student_record->id);
            if ($student_profile) {
                $photo_url = FILES_URI . $student_profile->photo;
            } else if ($student_record->Gender == '1') {
                $photo_url = "images/girl_user.png";
            } else if ($student_record->Gender == '2') {
                $photo_url = "images/boy_user.png";
            } else {
                $photo_url = "images/User.png";
            }
            $this->view->cacdyr = AcademicYearMaster::findFirstByStatus('c');

            $havingQuery = $subDivValId ? "(LENGTH('$subDivValId') - LENGTH( REPLACE('$subDivValId',concat_subdivs , '' ) ))/ LENGTH(concat_subdivs )>0
                    OR
                    (LENGTH('$subDivValId') - LENGTH( REPLACE('$subDivValId',concat_subdivs ,'' ) )) / LENGTH(concat_subdivs ) IS NULL" : '';
            $having = $havingQuery ? 'having' : '';
            //Get subjects for the student
            $getSubjects = SubDivisionSubjects::find(array(
                        'div_val_id =' . $student_record->Division_Class,
                        $having => $havingQuery));
//            print_r($student_record->Subdivision);exit;
            $combiarr = $subjMaster = array();
            foreach ($getSubjects as $formCombination) {
                $combiarr[$formCombination->subject_id] = $subjects = SubjectsDivision::findFirstById($formCombination->subject_id)->subjectName;
                $subjMaster[] = $formCombination->id;
            }

            //List of data passed to view
            $this->view->divname = $divname;
            $this->view->divval = $divval->classname;

            $this->view->address = $student_record->Address1 . ',</br>' . $student_record->Address2 . ',</br>' .
                    $student_record->State . ',</br>' . $student_record->Country . ':' . $student_record->Pin;

            $this->view->student_name = $student_record->Student_Name;
            $this->view->Admission_no = $student_record->Admission_no;
            $this->view->subjects = $combiarr;
            $this->view->subjMaster = $subjMaster;
            $this->view->photo_url = $photo_url;
            $this->view->student_name = $student_record->Student_Name;
            $this->view->Admission_no = $student_record->Admission_no;
            $this->view->student_id = $student_record->id;
            $this->view->divValId = $student_record->Division_Class;
            $this->view->subDivisions = $subdiv;
            $this->view->academicYrId = $this->view->cacdyr->id;

            $this->view->announcements = Announcement::find('to="' . ((in_array("Parent", $identity['role_name'])) ? 'parent' : 'student') . '"');

            $attquery = (count($subjMaster) > 0) ? 'subject_master_id IN (' . implode(',', $subjMaster) . ')' : '';
            $this->view->assignments = (count($subjMaster) > 0) ? AssignmentsMaster::find($attquery) : '';
            $this->view->changepassForm = new ChangePasswordForm();
            $param['type'] = 'attendance';

            $combinations = array();
            $getPermissionType = Permissions::findFirstByPermissionFor($param['type']);
            /* $masQuery = ($student_record->Subdivision) ? " and concat_subdivs Like '%$student_record->Subdivision%'" : '';
              $getCombinations = ($getPermissionType->permission == '1') ?
              ClassTeachersMaster::find("div_val_id = " . $student_record->Division_Class .
              $attenQuery) :
              SubDivisionSubjects::find("div_val_id = " . $student_record->Division_Class .
              $attenQuery); */
            $getCombinations = ($getPermissionType->permission == '1') ? ClassTeachersMaster::find(array(
                        'div_val_id =' . $student_record->Division_Class,
                        $having => $havingQuery
                    )) :
                    SubDivisionSubjects::find(array(
                        'div_val_id =' . $student_record->Division_Class,
                        $having => $havingQuery));
            foreach ($getCombinations as $formCombination) {
                $combinations[] = $formCombination->id;
            }
            $attdenceQuery = (count($combinations) > 0) ? ' and master_id IN (' . implode(',', $combinations) . ')' : '';
            $this->view->attendanceHolidays = AttendanceDays::find('day_type = 1 and user_type = "student"' .
                            ' and  permission_type  = ' . $getPermissionType->permission .
                            ' and academic_year_id = ' . $this->view->academicYrId . $attdenceQuery);

            $this->view->student_att_legend = AttendanceSelectbox::find("attendance_for = 'student'");
            $stuattendanceEve = array();
            foreach ($this->view->attendanceHolidays as $holiday) {
                $date = $holiday->date;
                $stuattendanceEve[date('d-m-Y', $date)] = 'holiday';
            }
            $this->view->stuattendanceEveJson = json_encode($stuattendanceEve);
            $this->view->personalform = new StudentPersonalForm($student_record, array('edit' => true));
        }
    }

    public function keyInStudentDataAction() {

        $this->tag->prependTitle("Student Profile | ");
        $this->assets->addCss('css/edustyle.css');
        $this->assets->addJs('js/cycle/studentcycle.js');


        $this->view->application = Application::findFirstByApplicationNo($this->request->get('application_no'));
        $student = StudentInfo::findFirstByApplicationNo($this->request->get('application_no'));
        $this->view->additionalDetails = StudentGeneralMaster::findFirstByStudentId($this->view->student->id);

        $this->view->student = $student;

        if ($this->request->isGet()) {
            $this->view->form = new StudentProfileForm($this->view->student, array('edit' => true));
        }
        $hideOrShowFields = StudentProfileSettings::find("hide_or_show = 1");
        foreach ($hideOrShowFields as $hideOrShow) {
            $fieldArr[$hideOrShow->id] = $hideOrShow->form_name;
        }
//        $this->view->fieldArr = $fieldArr;
        $message = array();

        try {
            if ($this->request->isPost()) {
                $sform = new StudentProfileForm(null);
                $postArr = $this->request->getPost();
                $fieldphoto = StudentProfileSettings::findFirst('column_name ="photo"');
                $fieldfamphoto = StudentProfileSettings::findFirst('column_name ="family_photo"');
//                print_r($sform->getMessages());
//                print_r($this->request->getPost());
//                
//echo $postArr['transport']? $this->request->getPost('transport'):'21';exit;
                if ($sform->isValid($postArr) == false) {
                    foreach ($sform->getMessages() as $messages) {
                        $error .= $messages . "<br>";
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                    /* $this->flashSession->error($error);
                      //                                $this->flashSession->error($form->getMessages());
                      return $this->response->redirect('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no')); */
                } else {

                    $errmsg = '';
                    $application = Application::findFirstByApplicationNo($this->request->get('application_no'));
                    $admission = StudentInfo::findFirstByApplicationNo($this->request->get('application_no'));
                    $additionalDetails = count(StudentGeneralMaster::find('student_id =' . $admission->id)) ?
                            StudentGeneralMaster::findFirstByStudentId($admission->id) :
                            new StudentGeneralMaster();

                    if ($this->request->hasFiles() == true) {
                        // Print the real file names and sizes
                        foreach ($this->request->getUploadedFiles() as $file) {
                            $ext = explode('/', $file->getType());
                            if ($ext[0] == 'image') {
                                $filename = md5(uniqid(rand(), true)) . '-' . strtolower($file->getname());
                                $file->moveTo(FILES_DIR . $filename);
                                $fieldname = $file->getKey();
                                $additionalDetails->{$fieldname} = $filename;
                            } else {
                                $errmsg .='Invalid file extension' . "<br>";
                                /*  $this->flashSession->error('Invalid file extension');
                                  return $this->forward('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no')); */
                                /* $message['type'] = 'error';
                                  $message['message'] = '<div class="alert alert-block alert-danger fade in">Invalid file extension</div>';
                                  print_r(json_encode($message));
                                  exit; */
                            }
                        }
                    } else {
                        // $this->flashSession->error('Photo is required');

                        if (($fieldphoto->hide_or_show != 0 && $fieldphoto->mandatory != 0 && empty($additionalDetails->photo)) ||
                                ($fieldfamphoto->hide_or_show != 0 && $fieldphoto->mandatory != 0 && empty($additionalDetails->family_photo))) {

                            $errmsg .='Photo is required' . "<br>";
                            /* $this->flashSession->error('Photo is required');
                              return $this->forward('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no'));* */
                        }
                    }



                    //$this->flashSession->error(ctype_alpha("hello") ? "true" : "false");
                    //	$this->flashSession->error($stuname);
                    //exit;
                    if ($this->request->getPost('fcontact') == 0)
                        $errmsg .= 'Please Select Father contact number.' . "<br>";
                    if ($this->request->getPost('mcontact') == 0)
                        $errmsg .= 'Please Select Mother contact number.' . "<br>";
                    if ($this->request->getPost('gcontact') == 0)
                        $errmsg .= 'Please Select Guardian contact number.' . "<br>";

                    if ($this->request->getPost('fcontact') == 1) {
                        if (($this->request->getPost('mcontact') == 1) || ($this->request->getPost('gcontact') == 1)) {

                            $errmsg .= 'Please Select only one primary contact number. ' . "<br>";
                        }
                    } else if ($this->request->getPost('mcontact') == 1) {
                        if (($this->request->getPost('fcontact') == 1) || ($this->request->getPost('gcontact') == 1)) {

                            $errmsg .= 'Please Select only one primary contact number.' . "<br>";
                        }
                    } else if ($this->request->getPost('gcontact') == 1) {
                        if (($this->request->getPost('mcontact') == 1) || ($this->request->getPost('fcontact') == 1)) {

                            $errmsg .= 'Please Select only one primary contact number.' . "<br>";
                        }
                    } else {

                        $errmsg .= 'Please Select at-least one primary contact number.' . "<br>";
                    }

                    if ($errmsg != '') {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $errmsg . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                    /* if (!ctype_alpha($stuname)) {
                      $this->flashSession->error('Alphabeds Only allowed in Student Name Field');
                      return $this->forward('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no'));
                      } else if (!ctype_alpha($this->request->getPost('gname'))) {
                      $this->flashSession->error('Alphabeds Only allowed in Guardian Name Field');
                      return $this->forward('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no'));
                      } else if (!ctype_alpha($mname)) {
                      $this->flashSession->error('Alphabeds Only allowed in Mother Name Field');
                      return $this->forward('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no'));
                      } else if (!ctype_alpha($fname)) {
                      $this->flashSession->error('Alphabeds Only allowed in Father Name Field');
                      return $this->forward('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no'));
                      } else if ($height < 0) {
                      $this->flashSession->error('Please enter real number in student height field.');
                      return $this->forward('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no'));
                      } else if ($weight < 0) {
                      $this->flashSession->error('Please enter real number in student weight field.');
                      return $this->forward('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no'));
                      } */

                    $application->application_no = $this->request->getPost('application_no');
                    $application->academic_year_id = $this->request->getPost('academicYrId');
                    $application->Student_Name = $this->request->getPost('studentName');
                    $application->Gender = $this->request->getPost('gender');
                    $application->Date_of_Birth = strtotime($this->request->getPost('dob'));
                    //$application->status = 'Issued';
                    $application->Date_of_Joining = strtotime($this->request->getPost('doj'));
                    $application->Address1 = $this->request->getPost('address1');
                    $application->Address2 = $this->request->getPost('address2');
                    $application->State = $this->request->getPost('state');
                    $application->Country = $this->request->getPost('country');
                    $application->Pin = $this->request->getPost('pin');
                    $application->Phone = $this->request->getPost('phone');
                    $application->Email = $this->request->getPost('email');
                    $application->key_in_status = 'Completed';
                    //$application->Division_Class = $this->request->getPost('divVal');
                    //$application->Joining_division = $this->request->getPost('divVal');
//                    $admission->Admission_no = $this->request->getPost('admission_no');
                    $admission->application_no = $application->application_no;
                    $admission->Student_Name = $application->Student_Name;
                    $admission->Gender = $application->Gender;
                    $admission->Date_of_Birth = $application->Date_of_Birth;
                    $admission->Date_of_Joining = $application->Date_of_Joining;
                    $admission->Address1 = $application->Address1;
                    $admission->Address2 = $application->Address2;
                    $admission->State = $application->State;
                    $admission->Country = $application->Country;
                    $admission->Pin = $application->Pin;
                    $admission->Phone = $application->Phone;
                    $admission->Email = $application->Email;
                    // $admission->Division_Class = $application->Division_Class;
                    // $admission->Joining_division = $application->Joining_division;
                    //                         $admission->Subdivision_section = $application->Subdivision_section;
                    //                         $admission->Subdivision_group = $application->Subdivision_group;


                    $additionalDetails->place_of_birth = $postArr['pob'] ? $this->request->getPost('pob') : '';
                    $additionalDetails->nationality = $postArr['nationality'] ? $this->request->getPost('nationality') : '';
                    $additionalDetails->academic_year_id = $this->request->getPost('academicYrId');
                    $additionalDetails->religion = $postArr['religion'] ? $this->request->getPost('religion') : '';
                    $additionalDetails->caste_category = $postArr['caste_cat'] ? $this->request->getPost('caste_cat') : '';
                    $additionalDetails->caste = $postArr['caste'] ? $this->request->getPost('caste') : '';
                    $additionalDetails->first_language = $postArr['language'] ? $this->request->getPost('language') : '';
                    $additionalDetails->father_name = $postArr['fname'] ? $this->request->getPost('fname') : '';
                    $additionalDetails->f_occupation = $postArr['foccup'] ? $this->request->getPost('foccup') : '';
                    $additionalDetails->f_designation = $postArr['fdesign'] ? $this->request->getPost('fdesign') : '';
                    $additionalDetails->f_phone_no = $postArr['fphone'] ? $this->request->getPost('fphone') : '';
                    $additionalDetails->f_phone_no_status = $postArr['fcontact'] ? $this->request->getPost('fcontact') : '';
                    $additionalDetails->mother_name = $postArr['mname'] ? $this->request->getPost('mname') : '';
                    $additionalDetails->m_occupation = $postArr['moccup'] ? $this->request->getPost('moccup') : '';
                    $additionalDetails->m_designation = $postArr['mdesign'] ? $this->request->getPost('mdesign') : '';
                    $additionalDetails->m_phone_no = $postArr['mphone'] ? $this->request->getPost('mphone') : '';
                    $additionalDetails->m_phone_no_status = $postArr['mcontact'] ? $this->request->getPost('mcontact') : '';
                    $additionalDetails->other_guardian_name = $postArr['gname'] ? $this->request->getPost('gname') : '';
                    $additionalDetails->g_occupation = $postArr['goccup'] ? $this->request->getPost('goccup') : '';
                    $additionalDetails->g_designation = $postArr['gdesign'] ? $this->request->getPost('gdesign') : '';
                    $additionalDetails->g_phone = $postArr['gphone'] ? $this->request->getPost('gphone') : '';
                    $additionalDetails->g_phone_no_status = $postArr['gcontact'] ? $this->request->getPost('gcontact') : '';
                    $additionalDetails->person_school_fee = $postArr['feepayee'] ? $this->request->getPost('feepayee') : '';
                    $additionalDetails->circumtances = $postArr['famcir'] ? $this->request->getPost('famcir') : '';
                    $additionalDetails->sibling = $postArr['siblings'] ? $this->request->getPost('siblings') : '';
                    $additionalDetails->blood_group = $postArr['blood_grp'] ? $this->request->getPost('blood_grp') : '';
                    $additionalDetails->height = $postArr['height'] ? $this->request->getPost('height') : '';
                    $additionalDetails->weight = $postArr['weight'] ? $this->request->getPost('weight') : '';
                    $additionalDetails->allergic = $postArr['drugallergic'] ? $this->request->getPost('drugallergic') : '';
                    $additionalDetails->chronic = $postArr['chronicill'] ? $this->request->getPost('chronicill') : '';
                    $additionalDetails->family_doctor = $postArr['docDet'] ? $this->request->getPost('docDet') : '';
                    $additionalDetails->previous_school_name = $postArr['prevSchoolName'] ? $this->request->getPost('prevSchoolName') : '';
                    $additionalDetails->previous_school_state = $postArr['prevState'] ? $this->request->getPost('prevState') : '';
                    $additionalDetails->previous_school_country = $postArr['pprevcountry'] ? $this->request->getPost('pprevcountry') : '';
                    $additionalDetails->attended_from = $postArr['attfrom'] ? strtotime($this->request->getPost('attfrom')) : '';
                    $additionalDetails->attended_to = $postArr['attto'] ? strtotime($this->request->getPost('attto')) : '';
                    $additionalDetails->achievements = $postArr['achievements'] ? $this->request->getPost('achievements') : '';
                    $additionalDetails->comments = $postArr['comments'] ? $this->request->getPost('comments') : '';
                    $additionalDetails->other_details = $postArr['odet'] ? $this->request->getPost('odet') : '';
//                    $additionalDetails->rollno = $postArr['rollno'] ? $this->request->getPost('rollno') : '';
                    $identity = $this->auth->getIdentity();
                    $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                    $additionalDetails->created_by = $uid;
                    $additionalDetails->created_date = time();
                    $additionalDetails->modified_by = $uid;
                    $additionalDetails->modified_date = time();
                    $additionalDetails->transport = $postArr['transport'] ? $this->request->getPost('transport') : '';
//                    print_r($additionalDetails);exit;
                    if ($application->save()) {
                        if ($admission->save()) {
                            $additionalDetails->student = $admission;
                            if ($additionalDetails->save()) {

                                $message['type'] = 'success';
                                $message['message'] = 'Student details saved successfully';

                                print_r(json_encode($message));
                                exit;
                                /* $this->flashSession->success($message['message']);
                                  $this->view->data = 'save';
                                  return $this->response->redirect('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no')); */
                                //  sleep(10);
                                //return $this->response->redirect('application/getAllApplications');
                                //print_r(json_encode($message));
                                // exit;
                            } else {
                                /* $this->flashSession->error($additionalDetails->getMessages());
                                  return $this->response->redirect('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no')); */
                                // return $this->forward('student-profile/keyInStudentData?application_no='.$this->request->get('application_no'));
                                foreach ($additionalDetails->getMessages() as $messages) {
                                    $error .= $messages . "<br>";
                                }
                                $message['type'] = 'error';
                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                print_r(json_encode($message));
                                exit;
                            }
                        } else {
                            /* $this->flashSession->error($admission->getMessages());
                              return $this->response->redirect('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no'));* */
                            foreach ($admission->getMessages() as $messages) {
                                $error .= $messages . "</br>";
                            }
                            $message['type'] = 'error';
                            $message['message'] = $error;
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        /* $this->flashSession->error($application->getMessages());
                          return $this->response->redirect('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no')); */
                        foreach ($application->getMessages() as $messages) {
                            $error .= $messages . "<br>";
                        }
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        } catch (Exception $e) {
            /* $this->flashSession->error($e->getMessages());
              return $this->response->redirect('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no')); */
            foreach ($e->getMessages() as $messages) {
                $error .= $messages;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function editStudentProfileAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Student Profile | ");
//        $this->assets->addCss('css/edustyle.css');
//        $this->assets->addJs('js/appscripts/studentprofile.js');
        //$this->view->application = Application::findFirstByApplicationNo($this->request->get('application_no'));
        $student = StudentInfo::findFirstById($this->request->get('student_id'));
        $this->view->student_id = $this->request->get('student_id');
        $academicdet = StudentAcademicMaster::findFirst('student_id=' . $this->request->get('student_id'));
        $this->view->academic = StudentAcademicMaster::findFirst('student_id=' . $this->request->get('student_id'));
        $this->view->subclassval = SubDivisionValues::findFirst("subDivID LIKE'%" . $academicdet->subDivValId . "%' and divValID=" . $academicdet->divValId);

        $this->view->additionalDetails = StudentGeneralMaster::findFirstByStudentId($this->request->get('student_id'));

        $this->view->student = $student;

        if ($this->request->isGet()) {
            $this->view->form = new StudentProfileForm($this->view->student, array('edit' => true));
        }
        $hideOrShowFields = StudentProfileSettings::find("hide_or_show = 1");
        foreach ($hideOrShowFields as $hideOrShow) {
            $fieldArr[$hideOrShow->id] = $hideOrShow->form_name;
        }
//        $this->view->fieldArr = $fieldArr;
    }

    public function updatestudentAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();

        try {
            if ($this->request->isPost()) {

                // print_r($this->request->getUploadedFiles()); 			  	   
                //exit;
                $sform = new StudentProfileForm(null);
                $postArr = $this->request->getPost();
                $fieldphoto = StudentProfileSettings::findFirst('column_name ="photo"');
                $fieldfamphoto = StudentProfileSettings::findFirst('column_name ="family_photo"');
//                print_r($sform->getMessages());
//                print_r($this->request->getPost());
//                
//echo $postArr['transport']? $this->request->getPost('transport'):'21';exit;
                if ($sform->isValid($postArr) == false) {

                    foreach ($sform->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }

                    $message['type'] = 'error';
                    $message['message'] = '' . $error . '';
                    print_r(json_encode($message));
                    exit;

                    /* $this->flashSession->error($error);
                      //                                $this->flashSession->error($form->getMessages());
                      return $this->response->redirect('student-profile/editStudentProfile?student_id=' . $this->request->get('student_id')); */
                } else {

                    $errmessage = '';
                    $this->view->data = 'post';
                    // $application = Application::findFirstByApplicationNo($this->request->get('student_id'));
                    $admission = StudentInfo::findFirstById($this->request->getPost('student_id'));
                    //  $admission = StudentInfo::findFirstById($this->request->get('student_id'));
                    $additionalDetails = count(StudentGeneralMaster::find('student_id =' . $admission->id)) ?
                            StudentGeneralMaster::findFirstByStudentId($admission->id) :
                            new StudentGeneralMaster();

                    if ($this->request->hasFiles() == true) {


                        // Print the real file names and sizes
                        foreach ($this->request->getUploadedFiles() as $file) {
                            $ext = explode('/', $file->getType());
                            //	echo $ext .'<br>';
                            if ($ext[0] == 'image') {
                                $filename = md5(uniqid(rand(), true)) . '-' . strtolower($file->getname());
                                $file->moveTo(FILES_DIR . $filename);

                                $fieldname = $file->getKey();
                                $additionalDetails->{$fieldname} = $filename;
                            } else {


                                $errmessage .= 'Invalid file extension ' . "\n";

                                /* $this->flashSession->error('Invalid file extension');
                                  return $this->forward('student-profile/editStudentProfile?student_id=' . $this->request->get('student_id')); */
                                /* $message['type'] = 'error';
                                  $message['message'] = '<div class="alert alert-block alert-danger fade in">Invalid file extension</div>';
                                  print_r(json_encode($message));
                                  exit; */
                            }
                        }
                    } else {
                        // $this->flashSession->error('Photo is required');

                        if (($fieldphoto->hide_or_show != 0 && $fieldphoto->mandatory != 0 && empty($additionalDetails->photo)) ||
                                ($fieldfamphoto->hide_or_show != 0 && $fieldphoto->mandatory != 0 && empty($additionalDetails->family_photo))) {


                            $errmessage .='Photo is required ' . "\n";

                            /* 								  $this->flashSession->error('Photo is required');
                              return $this->forward('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no')); */
                        }
                    }


                    $fname = $this->request->getPost('fname');
                    $mname = $this->request->getPost('mname');
                    $gname = $this->request->getPost('gname');
                    $stuname = $this->request->getPost('studentName');
                    $height = $this->request->getPost('height');
                    $weight = $this->request->getPost('weight');
                    //$this->flashSession->error(ctype_alpha("hello") ? "true" : "false");
                    //	$this->flashSession->error($stuname);
                    //exit;



                    /*                 $fcontact = $this->request->getPost('fcontact');

                      if ($this->request->getPost('fcontact') == 0)
                      $errmessage .= 'Please Select Father contact number.' . "\n";
                      if ($this->request->getPost('mcontact') == 0)
                      $errmessage .= 'Please Select Mother contact number.' . "\n";
                      if ($this->request->getPost('gcontact') == 0)
                      $errmessage .= 'Please Select Guardian contact number.' . "\n";

                      if ($this->request->getPost('fcontact') == 1) {
                      if (($this->request->getPost('mcontact') == 1) || ($this->request->getPost('gcontact') == 1)) {

                      $errmessage .= 'Please Select only one primary contact number. ' . "\n";
                      }
                      } else if ($this->request->getPost('mcontact') == 1) {
                      if (($this->request->getPost('fcontact') == 1) || ($this->request->getPost('gcontact') == 1)) {

                      $errmessage .= 'Please Select only one primary contact number.' . "\n";
                      }
                      } else if ($this->request->getPost('gcontact') == 1) {
                      if (($this->request->getPost('mcontact') == 1) || ($this->request->getPost('fcontact') == 1)) {

                      $errmessage .= 'Please Select only one primary contact number.' . "\n";
                      }
                      } else {

                      $errmessage .= 'Please Select at-least one primary contact number.' . "\n";
                      }
                     */

                    if ($errmessage != '') {
                        $message['type'] = 'error';
                        $message['message'] = $errmessage;
                        print_r(json_encode($message));
                        exit;
                    }


                    /*  $application->application_no = $this->request->getPost('application_no');
                      $application->academic_year_id = $this->request->getPost('academicYrId');
                      $application->Student_Name = $this->request->getPost('studentName');
                      $application->Gender = $this->request->getPost('gender');
                      $application->Date_of_Birth = strtotime($this->request->getPost('dob'));
                      //$application->status = 'Issued';
                      $application->Date_of_Joining = strtotime($this->request->getPost('doj'));
                      $application->Address1 = $this->request->getPost('address1');
                      $application->Address2 = $this->request->getPost('address2');
                      $application->State = $this->request->getPost('state');
                      $application->Country = $this->request->getPost('country');
                      $application->Pin = $this->request->getPost('pin');
                      $application->Phone = $this->request->getPost('phone');
                      $application->Email = $this->request->getPost('email');
                      $application->key_in_status = 'Completed';
                      //$application->Division_Class = $this->request->getPost('divVal');
                      //$application->Joining_division = $this->request->getPost('divVal'); */


                    /* echo '<br> ad no-' . $this->request->getPost('admission_no');
                      echo '<br> ap no-' . $this->request->getPost('application_no');
                      echo '<br> stu nam-' . $this->request->getPost('studentName');
                      echo  '<br>gnd-' . $this->request->getPost('gender');
                      echo '<br>dob-' . strtotime($this->request->getPost('dob'));
                      echo '<br>doj-' . strtotime($this->request->getPost('doj'));
                      echo '<br>ad1-' . $this->request->getPost('address1');
                      echo '<br>ad2-' . $this->request->getPost('address2');
                      echo '<br>st-' . $this->request->getPost('state');
                      echo '<br>cnt-'. $this->request->getPost('country');
                      echo '<br>pin-'. $this->request->getPost('pin');
                      echo '<br>phn-'. $this->request->getPost('phone');
                      echo '<br>eml-'.$this->request->getPost('email'); */


                    $admission->Admission_no = $this->request->getPost('admission_no');
                    $admission->application_no = $this->request->getPost('application_no');
                    $admission->Student_Name = $this->request->getPost('studentName');
                    $admission->Gender = $this->request->getPost('gender');
                    $admission->Date_of_Birth = strtotime($this->request->getPost('dob'));
                    $admission->Date_of_Joining = strtotime($this->request->getPost('doj'));
                    $admission->Address1 = $this->request->getPost('address1');
                    $admission->Address2 = $this->request->getPost('address2');
                    $admission->State = $this->request->getPost('state');
                    $admission->Country = $this->request->getPost('country');
                    $admission->Pin = $this->request->getPost('pin');
                    $admission->Phone = $this->request->getPost('phone');
                    $admission->Email = $this->request->getPost('email');
                    // $admission->Division_Class = $application->Division_Class;
                    // $admission->Joining_division = $application->Joining_division;
                    //                         $admission->Subdivision_section = $application->Subdivision_section;
                    //                         $admission->Subdivision_group = $application->Subdivision_group;


                    $additionalDetails->place_of_birth = $postArr['pob'] ? $this->request->getPost('pob') : '';
                    $additionalDetails->nationality = $postArr['nationality'] ? $this->request->getPost('nationality') : '';
                    $additionalDetails->academic_year_id = $this->request->getPost('academicYrId');
                    $additionalDetails->religion = $postArr['religion'] ? $this->request->getPost('religion') : '';
                    $additionalDetails->caste_category = $postArr['caste_cat'] ? $this->request->getPost('caste_cat') : '';
                    $additionalDetails->caste = $postArr['caste'] ? $this->request->getPost('caste') : '';
                    $additionalDetails->first_language = $postArr['language'] ? $this->request->getPost('language') : '';
                    $additionalDetails->father_name = $postArr['fname'] ? $this->request->getPost('fname') : '';
                    $additionalDetails->f_occupation = $postArr['foccup'] ? $this->request->getPost('foccup') : '';
                    $additionalDetails->f_designation = $postArr['fdesign'] ? $this->request->getPost('fdesign') : '';
                    $additionalDetails->f_phone_no = $postArr['fphone'] ? $this->request->getPost('fphone') : '';
                    $additionalDetails->f_phone_no_status = $postArr['fcontact'] ? $this->request->getPost('fcontact') : '';
                    $additionalDetails->mother_name = $postArr['mname'] ? $this->request->getPost('mname') : '';
                    $additionalDetails->m_occupation = $postArr['moccup'] ? $this->request->getPost('moccup') : '';
                    $additionalDetails->m_designation = $postArr['mdesign'] ? $this->request->getPost('mdesign') : '';
                    $additionalDetails->m_phone_no = $postArr['mphone'] ? $this->request->getPost('mphone') : '';
                    $additionalDetails->m_phone_no_status = $postArr['mcontact'] ? $this->request->getPost('mcontact') : '';
                    $additionalDetails->other_guardian_name = $postArr['gname'] ? $this->request->getPost('gname') : '';
                    $additionalDetails->g_occupation = $postArr['goccup'] ? $this->request->getPost('goccup') : '';
                    $additionalDetails->g_designation = $postArr['gdesign'] ? $this->request->getPost('gdesign') : '';
                    $additionalDetails->g_phone = $postArr['gphone'] ? $this->request->getPost('gphone') : '';
                    $additionalDetails->g_phone_no_status = $postArr['gcontact'] ? $this->request->getPost('gcontact') : '';
                    $additionalDetails->person_school_fee = $postArr['feepayee'] ? $this->request->getPost('feepayee') : '';
                    $additionalDetails->circumtances = $postArr['famcir'] ? $this->request->getPost('famcir') : '';
                    $additionalDetails->sibling = $postArr['siblings'] ? $this->request->getPost('siblings') : '';
                    $additionalDetails->blood_group = $postArr['blood_grp'] ? $this->request->getPost('blood_grp') : '';
                    $additionalDetails->height = $postArr['height'] ? $this->request->getPost('height') : '';
                    $additionalDetails->weight = $postArr['weight'] ? $this->request->getPost('weight') : '';
                    $additionalDetails->allergic = $postArr['drugallergic'] ? $this->request->getPost('drugallergic') : '';
                    $additionalDetails->chronic = $postArr['chronicill'] ? $this->request->getPost('chronicill') : '';
                    $additionalDetails->family_doctor = $postArr['docDet'] ? $this->request->getPost('docDet') : '';
                    $additionalDetails->previous_school_name = $postArr['prevSchoolName'] ? $this->request->getPost('prevSchoolName') : '';
                    $additionalDetails->previous_school_state = $postArr['prevState'] ? $this->request->getPost('prevState') : '';
                    $additionalDetails->previous_school_country = $postArr['pprevcountry'] ? $this->request->getPost('pprevcountry') : '';
                    $additionalDetails->attended_from = $postArr['attfrom'] ? strtotime($this->request->getPost('attfrom')) : '';
                    $additionalDetails->attended_to = $postArr['attto'] ? strtotime($this->request->getPost('attto')) : '';
                    $additionalDetails->achievements = $postArr['achievements'] ? $this->request->getPost('achievements') : '';
                    $additionalDetails->comments = $postArr['comments'] ? $this->request->getPost('comments') : '';
                    $additionalDetails->other_details = $postArr['odet'] ? $this->request->getPost('odet') : '';
//                    $additionalDetails->rollno = $postArr['rollno'] ? $this->request->getPost('rollno') : '';
                    $identity = $this->auth->getIdentity();
                    $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
                    $additionalDetails->created_by = $uid;
                    $additionalDetails->created_date = time();
                    $additionalDetails->modified_by = $uid;
                    $additionalDetails->modified_date = time();
                    $additionalDetails->transport = $postArr['transport'] ? $this->request->getPost('transport') : '';
//                    print_r($additionalDetails);exit;
                    // if ($application->save()) {
                    if ($admission->save()) {
                        $additionalDetails->student = $admission;
                        if ($additionalDetails->save()) {

                            $message['type'] = 'success';
                            $message['message'] = 'Student details updated successfully';
                            //$this->flashSession->success($message['message']);
                            //return $this->response->redirect('student-profile/editStudentProfile?student_id=' . $this->request->get('student_id'));

                            print_r(json_encode($message));
                            exit;
                        } else {
                            // $this->flashSession->error($additionalDetails->getMessages());
                            //return $this->response->redirect('student-profile/editStudentProfile?student_id=' . $this->request->get('student_id'));
                            // return $this->forward('student-profile/keyInStudentData?application_no='.$this->request->get('application_no'));
                            foreach ($additionalDetails->getMessages() as $messages) {
                                $error .= $messages . "\n";
                            }
                            $message['type'] = 'error';
                            $message['message'] = '' . $error . '';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        //$this->flashSession->error($admission->getMessages());
                        //return $this->response->redirect('student-profile/editStudentProfile?student_id=' . $this->request->get('student_id'));
                        foreach ($admission->getMessages() as $messages) {
                            $error .= $messages . "\n";
                        }
                        $message['type'] = 'error';
                        $message['message'] = '' . $error . '';
                        print_r(json_encode($message));
                        exit;
                    }
                    /* } else {
                      $this->flashSession->error($application->getMessages());
                      return $this->response->redirect('student-profile/keyInStudentData?application_no=' . $this->request->get('application_no'));
                      foreach ($application->getMessages() as $message) {
                      $error .= $message;
                      }
                      $message['type'] = 'error';
                      $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                      print_r(json_encode($message));
                      exit;
                      } */
                }
            }
        } catch (Exception $e) {
            //$this->flashSession->error($e->getMessages());
            //return $this->response->redirect('student-profile/editStudentProfile?student_id=' . $this->request->get('student_id'));
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '' . $error . '';
            print_r(json_encode($message));
            exit;
        }
    }

    public function studentGeneralProfileAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->tag->prependTitle("Student Profile | ");

        $studentId = $this->request->get('studentId');
        $this->view->student_record = $student_record = StudentInfo::findFirstById($studentId);
        if ($student_record) {
//            $divname = ControllerBase::get_division_name_student();
//            //Get the student's division and subdivision
//            $divval = DivisionValues::findfirst('id = ' . $student_record->Division_Class);
//            $subDivValId = $student_record->Subdivision;
//            $subdivval = $student_record->Subdivision ?
//                    SubDivisionValues::find('id IN ( ' . $student_record->Subdivision . ')') : '';

            $subdiv = array();


            $stulist_mapping_list = StudentMapping::findFirst('student_info_id =' . $student_record->id);

            $stu_mapdets = ($stulist_mapping_list->aggregate_key) ? explode(',', $stulist_mapping_list->aggregate_key) : '';
            $maping_values = '';
            if ($stu_mapdets != '') {

                foreach ($stu_mapdets as $stu_mapdet) {


                    $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stu_mapdet);



                    $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);

                    // $single_stu_arr[$orgnztn_str_mas_det->name] = $orgnztn_str_det->name;
                    // $maping_values  .= $orgnztn_str_mas_det->name .'-'. $orgnztn_str_det->name .''; //[$studet->id]

                    $subdiv[] = array(
                        'name' => $orgnztn_str_mas_det->name,
                        'value' => $orgnztn_str_det->name);
                    //$maping_values .= $orgnztn_str_mas_det->name . '-' . $orgnztn_str_det->name . ':';
                }
            }


            $this->view->subdiv = $subdiv;
            //Get the photo from StudentGeneralMaster
            // $student_profile = StudentGeneralMaster::findfirst('student_id = ' . $student_record->id);
            if ($student_record->photo) {
                $photo_url = FILES_URI . $student_record->photo;
            } else if ($student_record->Gender == '1') {
                $photo_url = "images/girl_user.png";
            } else if ($student_record->Gender == '2') {
                $photo_url = "images/boy_user.png";
            } else {
                $photo_url = "images/User.png";
            }
            // $this->view->cacdyr = AcademicYearMaster::findFirstByStatus('c');   //List of data passed to view
            //  $this->view->divname = $divname;
            // $this->view->divval = $divval->classname;

            $this->view->address = $student_record->Address1 . ',</br>' . $student_record->Address2 . ',</br>' .
                    $student_record->State . ',</br>' . $student_record->Country . ':' . $student_record->Pin;
            $this->view->stuGen = $student_record;
            $this->view->student_name = $student_record->Student_Name;
            $this->view->Admission_no = $student_record->Admission_no;
            $this->view->photo_url = $photo_url;
            $this->view->student_id = $student_record->id;
            // $this->view->divValId = $student_record->Division_Class;
            $this->view->subDivisions = $subdiv;
            // $this->view->academicYrId = $this->view->cacdyr->id;
        }
    }

    public function academicAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $studentId = $this->request->get('studentId');
        //Extract the login name for identity and use it to get details of the student

        $this->view->triggerClick = $this->request->get('action') ? $this->request->get('action') : 'attendance_section';  //'overviewtop_section';
        $this->view->student_record = $student_record = StudentInfo::findFirstById($studentId);
        if ($student_record) {
            $subdiv = array();
            $stulist_mapping_list = StudentMapping::findFirst('student_info_id =' . $student_record->id);

            $stu_mapdets = ($stulist_mapping_list->aggregate_key) ? explode(',', $stulist_mapping_list->aggregate_key) : '';
            $maping_values = '';
            if ($stu_mapdets != '') {

                foreach ($stu_mapdets as $stu_mapdet) {
                    $orgnztn_str_det = OrganizationalStructureValues::findFirstById($stu_mapdet);
                    $orgnztn_str_mas_det = OrganizationalStructureMaster::findFirstById($orgnztn_str_det->org_master_id);
                    $subdiv[] = array(
                        'name' => $orgnztn_str_mas_det->name,
                        'value' => $orgnztn_str_det->name);
                }
            }


            $this->view->subdiv = $subdiv;
//            $currentQury = preg_filter('/^([\d])*/', '(FIND_IN_SET("$0" , REPLACE( aggregated_nodes_id, "-", "," ) ) >0)', $stu_mapdets);
//            $subdivval = GroupSubjectsTeachers::find(implode(' OR ', $currentQury));
//            $grpsubids = $subjIds = $combiarr = array();
//            echo '<pre>';
//            foreach ($subdivval as $nodes) {
//                $res = ControllerBase::getCommonIdsForKeys($nodes->aggregated_nodes_id);
//                foreach ($res as $set) {
//                    $arrayfdiff = array_diff($stu_mapdets, explode('-', $set));
//                    if (count($arrayfdiff) == 0) {
//                        $grpsubids[] = $nodes->id;
//                        $subjIds[] = $nodes->subject_id;
//                        $combiarr[$nodes->subject_id] = OrganizationalStructureValues::findFirstById($nodes->subject_id)->name;
//                    }
//                }
//            }
            if ($student_record->photo) {
                $photo_url = FILES_URI . $student_record->photo;
            } else if ($student_record->Gender == '1') {
                $photo_url = "images/girl_user.png";
            } else if ($student_record->Gender == '2') {
                $photo_url = "images/boy_user.png";
            } else {
                $photo_url = "images/User.png";
            }


            $this->view->address = $student_record->Address1 . ',</br>' . $student_record->Address2 . ',</br>' .
                    $student_record->State . ',</br>' . $student_record->Country . ':' . $student_record->Pin;

            $this->view->student_name = $student_record->Student_Name;
            $this->view->Admission_no = $student_record->Admission_no;
            $this->view->aggregate_key = $stulist_mapping_list->aggregate_key;
            $this->view->subjects = $combiarr;
            $this->view->subjMaster = '11'; // $subjMaster;
            $this->view->photo_url = $photo_url;
            $this->view->student_name = $student_record->Student_Name;
            $this->view->Admission_no = $student_record->Admission_no;
            $this->view->student_id = $student_record->id;
            $this->view->subDivisions = $subdiv;
            $identity = $this->auth->getIdentity();
            
            
            
//            $currentQury = array();
//            $currentQury[] = '( parent_id IN (' . implode(',', $stu_mapdets) . '))';
//            $subjects_mas = OrganizationalStructureMaster::find(array(
//                        'columns' => 'GROUP_CONCAT(id) as ids',
//                        ' module = "Subject" and is_subordinate = 1 '
//            ));
//            $subjects = OrganizationalStructureValues::find('org_master_id IN (' . $subjects_mas[0]->ids . ') '
//                            . ' and (' . implode(' or ', $currentQury) . ')');
//            $this->view->orgvals = $subjects;
//            $subj_Ids = array();
//            foreach ($subjects as $nodes) {
//                $subj_Ids[] = $nodes->id;
//            }
//            if (count($subj_Ids) > 0) {
//                $sub_qry = '( subject_id  IN (' . implode(',', $subj_Ids) . '))';
//                $subjects_mas_grup = GroupSubjectsTeachers::find(array(
//                            $sub_qry,
//                            'columns' => 'GROUP_CONCAT(id) as ids'
//                ));
//            }
//            
//
//            $attquery = (count($subj_Ids) > 0) ? 'grp_subject_teacher_id IN (' . $subjects_mas_grup[0]->ids . ')' : '';
//            $subjidMaster = array();

            $this->view->assignments = (count($subj_Ids) > 0) ? AssignmentsMaster::find($attquery) : '';

            
            $this->view->changepassForm = new ChangePasswordForm();
            $this->view->attendanceHolidays = AttendanceDays::find('day_type = 1 and user_type = "student"');
            $this->view->student_att_legend = AttendanceSelectbox::find("attendance_for = 'student'");
            $stuattendanceEve = array();
            foreach ($this->view->attendanceHolidays as $holiday) {
                $date = $holiday->date;
                $stuattendanceEve[date('d-m-Y', $date)] = 'holiday';
            }
            $this->view->stuattendanceEveJson = json_encode($stuattendanceEve);

            $this->view->personalform = new StudentPersonalForm($student_record, array('edit' => true));
        }
    }

    public function loadAssignmentAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if ($this->request->isPost()) {
            $identity = $this->auth->getIdentity();

            $params = $queryParams = $queryParams1 = array();
            $orderphql = array();
            foreach ($this->request->getPost() as $key => $value) {
                $params[$key] = $value;
            }

            $this->view->subjectid = $subjectid = isset($params['subjectid']) ? $params['subjectid'] : '';
            $this->view->assignmentdate =$assignmentdate = isset($params['assignmentdate']) ? $params['assignmentdate'] : date('d-m-Y');
            $this->view->stuid = $stuid = isset($params['studentId']) ? $params['studentId'] : '';
            $student_record = StudentMapping::findFirst('student_info_id =' . $stuid);
            $aggregate_key = explode(',', $student_record->aggregate_key);
            
            $subjpids = ControllerBase::getAlSubjChildNodes($aggregate_key);
            $subjids = ControllerBase::getGrpSubjMasPossiblities($aggregate_key);
            $subjects = ControllerBase::getAllPossibleSubjects($subjids);
            $this->view->subjects = $subjects;
             if (isset($assignmentdate) && $assignmentdate != '') {
                $queryParams[] = 'submission_date >= "' . strtotime($assignmentdate . ' 00:00:00') . '" and submission_date <= "' . strtotime($assignmentdate . ' 23:59:59') . '"';
            }
            if ($subjectid && $subjectid > 0) {
	 $subjagg = ControllerBase::getAllSubjectAndSubModules(array($subjectid));
        if (count($subjagg)>0):
             $queryParams[]  = 'subjct_modules IN(' . implode(',', $subjagg) . ')';
        endif;

            } 

            if (count($subjids) > 0) {
                $queryParams[] = 'grp_subject_teacher_id IN(' . implode(',', $subjids) . ')';
            }
            $conditionvals = (count($queryParams) > 0) ? implode(' and ', $queryParams) : '';
              //  print_r($conditionvals);
              //  exit;
            $this->view->assignments = $assignments = AssignmentsMaster::find($conditionvals);
        }
    }

    public function loadAssignmentoldAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        if ($this->request->isPost()) {
            $identity = $this->auth->getIdentity();
            $subjectid = $this->request->getPost('subjectid');
            $assignmentdate = $this->request->getPost('assignmentdate');
            $stuid = $this->request->getPost('stuid');

            $attquery = '';
            if ($subjectid != '') {
                $attquery = 'subject_id =' . $subjectid . ' AND ';
            }
            if ((in_array("Staff", $identity['role_name'])) || (in_array("superadmin", $identity['role_name']))) {
                $student_record = StudentMapping::findFirst('student_info_id =' . $stuid);
            } else {
                $student_det = (in_array("Parent", $identity['role_name'])) ?
                        StudentInfo::findFirstByParentLoginid($identity['name']) :
                        StudentInfo::findFirstByLoginid($identity['name']);


                $student_record = StudentMapping::findFirst('student_info_id =' . $student_det->id);
            }

            $stu_mapdets = ($student_record->aggregate_key) ? explode(',', $student_record->aggregate_key) : '';

            $currentQury = preg_filter('/^([\d])*/', '(FIND_IN_SET("$0" , REPLACE( aggregated_nodes_id, "-", "," ) ) >0)', $stu_mapdets);
//            print_r(implode(' OR ', $currentQury));exit;
            $subdivval = GroupSubjectsTeachers::find($attquery . '(' . implode(' OR ', $currentQury) . ')');

//            print_r($subdivval);exit;
            $grpsubids = $subjIds = $combiarr = array();
            echo '<pre>';
            if (count($subdivval) > 0) {
                foreach ($subdivval as $nodes) {
                    $res = ControllerBase::getCommonIdsForKeys($nodes->aggregated_nodes_id);
                    foreach ($res as $set) {
                        $arrayfdiff = array_diff($stu_mapdets, explode('-', $set));
                        if (count($arrayfdiff) == 0) {
                            $grpsubids[] = $nodes->id;
                            $subjIds[] = $nodes->subject_id;
                            $combiarr[$nodes->subject_id] = SubjectsDivision::findFirstById($nodes->subject_id)->subjectName;
                        }
                    }
                }
            }


            $currentQury = array();

            $currentQury[] = '( parent_id IN (' . implode(',', $stu_mapdets) . '))';

            $subjects_mas = OrganizationalStructureMaster::find(array(
                        'columns' => 'GROUP_CONCAT(id) as ids',
                        ' module = "Subject" and is_subordinate = 1 '
            ));
            $subjects = OrganizationalStructureValues::find('org_master_id IN (' . $subjects_mas[0]->ids . ') '
                            . ' and (' . implode(' or ', $currentQury) . ')');

//            print_r($subjects);exit;
            $this->view->orgvals = $subjects;

            $subj_Ids = array();
            if ($subjectid == '') {
                foreach ($subjects as $nodes) {
                    $subj_Ids[] = $nodes->id;
                }
            }

            if ($subjectid != '') {
                $subj_Ids[] = $subjectid;
            }

            if (count($subj_Ids) > 0) {
                $sub_qry = '( subject_id  IN (' . implode(',', $subj_Ids) . '))';
                $subjects_mas_grup = GroupSubjectsTeachers::find(array(
                            $sub_qry,
                            'columns' => 'GROUP_CONCAT(id) as ids'
                ));
            }
            $assign_query = array();

//echo 'tesy';exit;
            if ($assignmentdate != '') {
                $start_date = strtotime($assignmentdate . " 00:00:00");
                $end_date = strtotime(date('d-m-Y', $start_date) . " 23:59:59 ");
                $assign_query[] = '( submission_date >= "' . $start_date . '" AND submission_date <= "' . $end_date . '" )';
            }

            if (count($subj_Ids) > 0) {
                $assign_query[] = (count($subj_Ids) > 0) ? 'grp_subject_teacher_id IN (' . $subjects_mas_grup[0]->ids . ')' : '';
            }
//            print_r(implode(' and ', $assign_query));
//            exit;

            $this->view->assignments = $assignments = (count($assign_query) > 0) ? AssignmentsMaster::find(implode(' and ', $assign_query)) : 0;
            //
//            print_r(count($assign_query));
//            exit;

            $this->view->stuid = $stuid;
            $this->view->subjects = $combiarr;
        }
    }

}
