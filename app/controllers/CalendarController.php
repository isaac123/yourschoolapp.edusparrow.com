<?php

use  Phalcon\Mvc\View; 

class CalendarController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->tag->prependTitle("Calendar | ");
        $this->assets
                ->collection('caldavcss')
                ->addCss('js/caldav/css/jquery-ui.custom.css')
                ->addCss('js/caldav/css/spectrum.custom.css')
                ->addCss('js/caldav/css/fullcalendar.css')
                ->addCss('js/caldav/css/default.css')
                ->addCss('js/caldav/css/default_integration.css');


        $this->view->identity = $this->auth->getIdentity();
        $this->assets
                ->addJs('js/caldav/js/cache_handler.js')
                ->addJs('js/caldav/lib/jquery-2.1.3.min.js')
                ->addJs('js/caldav/lib/jquery.browser.js')
                ->addJs('js/caldav/lib/jquery.autosize.js')
                ->addJs('js/caldav/lib/jquery-ui-1.11.4.custom.js')
                ->addJs('js/caldav/lib/jquery.quicksearch.js')
                ->addJs('js/caldav/lib/jquery.placeholder-1.1.9.js')
                ->addJs('js/caldav/lib/jshash-2.2_sha256.js')
                ->addJs('js/caldav/lib/spectrum.js')
                ->addJs('js/caldav/lib/fullcalendar.js')
                ->addJs('js/caldav/js/config.js')
                ->addJs('js/caldav/js/common.js')
                ->addJs('js/caldav/js/webdav_protocol.js')
                ->addJs('js/caldav/js/localization.js')
                ->addJs('js/caldav/js/interface_normal.js')
                ->addJs('js/caldav/js/vcalendar_rfc_regex.js')
                ->addJs('js/caldav/js/resource.js')
                ->addJs('js/caldav/js/vcalendar.js')
                ->addJs('js/caldav/js/vtodo.js')
                ->addJs('js/caldav/js/data_process.js')
                ->addJs('js/caldav/js/main.js')
                ->addJs('js/caldav/js/forms.js')
                ->addJs('js/caldav/js/timezones.js');

        $this->assets->addJs('js/academiccalendar/academiccalendar.js');
    }

    public function classroomAction() {
        $this->tag->prependTitle("Calendar | ");
        $this->assets
                ->collection('caldavcss')
                ->addCss('js/caldav/css/jquery-ui.custom.css')
                ->addCss('js/caldav/css/spectrum.custom.css')
                ->addCss('js/caldav/css/fullcalendar.css')
                ->addCss('js/caldav/css/default.css')
                ->addCss('js/caldav/css/default_integration.css');


        $this->view->identity = $this->auth->getIdentity();
        $this->assets
                ->addJs('js/caldav/js/cache_handler.js')
                ->addJs('js/caldav/lib/jquery-2.1.3.min.js')
                ->addJs('js/caldav/js/calendar.js')
                ->addJs('js/caldav/lib/jquery.browser.js')
                ->addJs('js/caldav/lib/jquery.autosize.js')
                ->addJs('js/caldav/lib/jquery-ui-1.11.4.custom.js')
                ->addJs('js/caldav/lib/jquery.quicksearch.js')
                ->addJs('js/caldav/lib/jquery.placeholder-1.1.9.js')
                ->addJs('js/caldav/lib/jshash-2.2_sha256.js')
                ->addJs('js/caldav/lib/spectrum.js')
                ->addJs('js/caldav/lib/fullcalendar.js')
                ->addJs('js/caldav/js/config.js')
                ->addJs('js/caldav/js/common.js')
                ->addJs('js/caldav/js/webdav_protocol.js')
                ->addJs('js/caldav/js/localization.js')
                ->addJs('js/caldav/js/interface.js')
                ->addJs('js/caldav/js/vcalendar_rfc_regex.js')
                ->addJs('js/caldav/js/resource.js')
                ->addJs('js/caldav/js/vcalendar.js')
                ->addJs('js/caldav/js/vtodo.js')
                ->addJs('js/caldav/js/data_process.js')
                ->addJs('js/caldav/js/main.js')
                ->addJs('js/caldav/js/forms.js')
                ->addJs('js/caldav/js/timezones.js')
                ->addJs('js/assigning/classroom.js');

        $this->assets->addJs('js/academiccalendar/academiccalendar.js');
    }

    public function getSubjectsForClassroomAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            if ($this->request->getPost('classroomid') != '') {

                $calid = $this->view->calid = $this->request->getPost('classroomid');
                $classmaster = ClassroomMaster::findFirstById($calid);
                /** get All Students ** */
                $stuquery = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
                $this->view->students = $students = StudentMapping::find('status = "Inclass" and ' . implode(' or ', $stuquery));
                /** get All subjects ** */
                $getPossSubj = GroupSubjectsTeachers::find('classroom_master_id = ' . $calid);
                foreach ($getPossSubj as $svalue) {
                    $subjects[] = OrganizationalStructureValues::findFirstById($svalue->subject_id);
                }
                $this->view->subjects = $subjects;
            }
        }
    }

    public function loadStafStuAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->calid = $calid = $this->request->getPost('classroomid');
            $classmaster = ClassroomMaster::findFirstById($calid);
            /** get All Students ** */
//            $res = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
            $classroomstu = ClassgroupStudents::findFirst('classroom_master_id = ' . $classmaster->id);
            $stuquery = 'SELECT stumap.id,stumap.student_info_id,stumap.aggregate_key,'
                    . 'stumap.subordinate_key,stumap.status,stuinfo.Student_Name,stuinfo.photo,stuinfo.Admission_no'
                    . ' FROM StudentMapping stumap LEFT JOIN'
                    . ' StudentInfo stuinfo ON stuinfo.id=stumap.student_info_id WHERE stumap.status = "Inclass"'
                    . ' and stuinfo.id IN (' . $classroomstu->students_id . ') '
//                    . ' and (' . implode(' or ', $res)  . ') '
                    . ' ORDER BY stuinfo.Student_Name ASC';
            $this->view->students = $classStudents = $this->modelsManager->executeQuery($stuquery);
        }
    }

    public function classroomSchedulingAction() {
        $this->tag->prependTitle("Classroom Scheduling | ");
        $this->assets
                ->addJs('js/appscripts/fullcalendar/fullcalendar.min.js')
                ->addJs('js/appscripts/fullcalendar/external-dragging-calendar.js')
                ->addJs('js/appscripts/fullcalendar/cal.js');
        $this->assets
                ->addCss('js/appscripts/fullcalendar/bootstrap-fullcalendar.css');
    }

    public function calendarFeedCassandraAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $classroomid = $this->request->getPost('classroomid');

            $obj = new Cassandra();
            $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, '9042');
            if ($res) {
                $buildquery = "select event_data from classroom_cal where classroom_id = '" . $classroomid . "'";
                if ($result = $obj->query($buildquery)) {
                    echo "[";
                    for ($i = 0; $i < count($result) - 1; $i++)
                        echo $result[$i]["event_data"] . ",";
                    echo $result[$i]["event_data"];
                    echo "]";
                }
            }
            $obj->close();
            exit;
        }
    }

    public function editEventPopupAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->event_id = $this->request->getPost('id');
            $this->view->subjectid = $this->request->getPost('subjectid');
            $this->view->classroomid = $classroomid = $this->request->getPost('classroomid');
            $this->view->title = $this->request->getPost('title');
            $this->view->status = $this->request->getPost('status');
            $this->view->mandatory = $this->request->getPost('mandatory');
            $this->view->Attendees = $this->request->getPost('Attendees');
            $this->view->start = strtotime($this->request->getPost('start'));
            $this->view->end = strtotime($this->request->getPost('end'));
            $this->view->location = $this->request->getPost('location');
            $this->view->loc_name = ClassroomLocation::find('classroom_master_id =' . $classroomid);
        }
    }

    public function addEventAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {

            foreach ($this->request->getPost() as $key => $vl) {
                $params[$key] = $vl;
            }
            $classroomid = $params['classroomid'];
            $subjectid = $params['subjectid'];
            $paticipants = $params['paticipants'];
            $start = strtotime($params['start']);
            $duration = $params['duration'];
            $mandatory = $params['mandatory'];
            $status = $params['status'];
            $forceadd = $params['forceadd'];
            $end = strtotime(date('Y-m-d H:i:s', $start) . "+$duration minutes");
            $message = array();
            $i = 0;

            $tempusers = explode(',', $paticipants);
            $participantsforquery = "'" . implode("','", $tempusers) . "'";
            $subjname = OrganizationalStructureValues::findFirstById($subjectid)->name;
            $obj = new Cassandra();
            $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, '9042');
            if ($res) {
                // checking for event clash
                if (!isset($forceadd) && $forceadd == 0) {
                    $buildquery = "select * from user_cal where user_id IN (" . $participantsforquery . ") ";
                    $error = 0;
                    //echo $buildquery;exit;
                    if ($result = $obj->query($buildquery)) {
                        $collapseUsers = '';
                        $collapseEvents = array();
                        $userconflict = 0;
//                        for ($i = 0; $i < count($result); $i++) {
//                            if (($result[$i]["end"] > $start && $result[$i]["start"] < $end)) {
                        for ($i = 0; $i < count($result); $i++) {
                            $endqried = date('d-m-Y', $result[$i]["end"]);
                            $startqried = date('d-m-Y', $result[$i]["start"]);
                            $endgiven = date('d-m-Y', $end);
                            $startgiven = date('d-m-Y', $start);
                            if (($endqried == $startgiven && $startqried == $endgiven) &&
                                    ($result[$i]["end"] > $start && $result[$i]["start"] < $end)) {
                                $userconflict++;
                                $studentid = explode('_', $result[$i]["user_id"]);
                                if ($studentid[0] == 'Student') {
                                    $stuInfo = StudentInfo::findFirstById($studentid[1]);
                                    if ($stuInfo->photo) {
                                        $collapseUsers .="<img src='" . $stuInfo->photo ? $this->url->get(FILES_URI . $stuInfo->photo) : $this->url->get('images/user.jpg') . "' "
                                                . " title='$stuInfo->Admission_no'"
                                                . " style 'border-radius:50%'"
                                                . "  width = '40px' "
                                                . " height = '40px' />";
                                    } else {
                                        $collapseUsers .= "<span title= '$stuInfo->Admission_no' class='mini-stat-icon tar ' style='cursor:default;' >" . substr($stuInfo->Student_Name, 0, 1) . "</span>";
                                    }

                                    $collapseUsers .= '<span title= "' . $stuInfo->Student_Name . '(' . $stuInfo->Admission_no . ')' . '" style="vertical-align: middle;position: relative;top:
                                            10px;cursor:default;word-break: break-all;text-transform: uppercase !important;">' .
                                            strlen($stuInfo->Student_Name) > 15 ? substr($stuInfo->Student_Name, 0, 15) . "..." : $stuInfo->Student_Name . '(' . $stuInfo->Admission_no . ')'
                                            . ' </span>';
                                } else {
                                    $stf = StaffInfo::findFirstById($studentid[1]);
                                    $stfInfo = StaffGeneralMaster::findFirstByStaffId($studentid[1]);
                                    if ($stfInfo->photo) {
                                        $collapseUsers .="<img src='" . $stfInfo->photo ? $this->url->get(FILES_URI . $stfInfo->photo) : $this->url->get('images/user.jpg') . "' "
                                                . " title='$stuInfo->Admission_no'"
                                                . " style 'border-radius:50%'"
                                                . "  width = '40px' "
                                                . " height = '40px' />";
                                    } else {
                                        $collapseUsers .= "<span title= '$stf->appointment_no' class='mini-stat-icon tar ' style='cursor:default;' >" . substr($stf->Staff_Name, 0, 1) . "</span>";
                                    }

                                    $collapseUsers .= '<span title= "' . $stf->Staff_Name . '(' . $stf->appointment_no . ')' . '" style="vertical-align: middle;position: relative;top:
                                            10px;cursor:default;word-break: break-all;text-transform: uppercase !important;">' .
                                            strlen($stf->Staff_Name) > 15 ? substr($stf->Staff_Name, 0, 15) . "..." : $stf->Staff_Name . '(' . $stf->appointment_no . ')'
                                            . ' </span>';
                                }


                                $collapseUsers .= " <br><br><br>";

                                $evedata = json_decode($result[$i]["event_data"]);
                                $collapseEvents[$result[$i]["event_id"]] = '<span class="label label-primary">' . $evedata->title . "</span> scheduled from " . date('d-m-Y H:i:s', $result[$i]["start"]) . " to " . date('d-m-Y H:i:s', $result[$i]["end"]);
                                $error = 1;
                            }
                        }
                    }

                    if ($error == 1) {

                        $message['type'] = 'error';
                        $message['title'] = 'Events affects availablity';
                        $message['message'] = 'Event you are trying to schedule clashes with events for  ' . $userconflict . '  users'
                                . ' <a class="" role="button" data-toggle="collapse" 
                          href="#collapseUsers" aria-expanded="false" aria-controls="collapseUsers">
                          <span>View Users</span> 
                          </a> /<a class="" role="button" data-toggle="collapse" 
                          href="#collapseEvents" aria-expanded="false" aria-controls="collapseEvents">
                            <span>View events</span>
                          </a>';
                        $message['message'] .= '<div class="collapse" id="collapseUsers"> <div class="well">';
                        $message['message'] .= $collapseUsers;
                        $message['message'] .= '</div> </div>';
                        $message['message'] .= '<div class="collapse" id="collapseEvents"> <div class="well">';
                        $message['message'] .= implode('<br><br><br>', $collapseEvents);
                        $message['message'] .= '</div> </div>';
                        print_r(json_encode($message));
                        exit;
                    }
                }
                $nextid = microtime();

                $event_data['id'] = $nextid;
                $event_data['title'] = "$subjname";
                $event_data['start'] = $start;
                $event_data['end'] = $end;
                $event_data['subject'] = $subjectid;
                $event_data['classroom'] = $classroomid;
                $event_data['Attendees'] = $paticipants;
                $event_data['status'] = $status;
                $event_data['mandatory'] = $mandatory;
                $evedatajson = json_encode($event_data);

                $buildInsertquery = " INSERT INTO classroom_cal (classroom_id,event_id,start,end,event_data,occurance,status,mandatory) 
values ('$classroomid','$nextid',$start,$end,'$evedatajson',1 ,'$status','$mandatory');";

                //echo $buildInsertquery;  exit;

                if ($result1 = $obj->query($buildInsertquery)) {
                    if ($result1[0]['result'] == 'success') {
                        foreach ($tempusers as $paticipant):
                            $userid = $paticipant;
                            $buildParticipantquery = " INSERT INTO user_cal (user_id,event_id,start,end,event_data,occurance,status,mandatory) 
values ('$userid','$nextid',$start,$end, '$evedatajson',1 ,'$status','$mandatory');";
                            //echo $buildParticipantquery;exit;
                            if ($result2 = $obj->query($buildParticipantquery)) {
                                if ($result2[0]['result'] != 'success') {
                                    $message['type'] = 'error';
                                    $message['title'] = 'Error - add event';
                                    $message['message'] = 'Problem in creating event';
                                    print_r(json_encode($message));
                                    exit;
                                }
                            }
                            $i++;
                        endforeach;
                    } else {
                        $message['type'] = 'error';
                        $message['title'] = 'Error - add event';
                        $message['message'] = 'Problem in creating event';
                        print_r(json_encode($message));
                        exit;
                    }
                }
                if ($i > 0) {
                    $message['type'] = 'success';
                    $message['title'] = 'Add Event';
                    $message['message'] = 'Event Successfully Created';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function editEventAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {


            foreach ($this->request->getPost() as $key => $vl) {
                $params[$key] = $vl;
            }
            $classroomid = $params['classroomid'];
            $subjectid = $params['subjectid'];
            $eventid = $params['eventid'];
            $phpdate = $params['phpdate'];
            $mandatory = $params['mandatory'];
            $status = $params['status'];
            $forceadd = $params['forceadd'];
            $locationdet = $params['locationdet'];
            $start = strtotime($params['start']);//$phpdate == 1 ? strtotime($params['start']) : $params['start'];
            $end = strtotime($params['end']);//$phpdate == 1 ? strtotime($params['end']) : $params['end'];
            $message = array();
            $i = 0;
            $subjname = OrganizationalStructureValues::findFirstById($subjectid)->name;

            $obj = new Cassandra();
            $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, '9042');
            if ($res) {
                $classmaster = ClassroomMaster::findFirstById($classroomid);
                $stuquery = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
                $students = StudentMapping::find('status = "Inclass" and ' . implode(' or ', $stuquery));
                $staff = GroupSubjectsTeachers::find("classroom_master_id =$classroomid  and subject_id = $subjectid");

                if (count($staff) > 0):
                    foreach ($staff as $stf) {
                        $splitteach = explode(',', $stf->teachers_id);
                        $part = preg_filter('/^([\d])*/', "'Staff_$0'", $splitteach);
                    }
                endif;
                if (count($students) > 0):
                    foreach ($students as $stu) {
                        $part[] = "'Student_" . $stu->id . "'";
                    }
                endif;
//                print_r($part);exit;
                $participantsforquery[] = implode(',', $part);
                // checking for event clash
                $buildquery = "select * from user_cal where user_id IN (" . implode(',', $participantsforquery) . "); ";
                $error = 0;
// print_r($buildquery);exit;
                if (!isset($forceadd) && $forceadd == 0) {
                    if ($result = $obj->query($buildquery)) {
//                        echo 'in'. count($result);
                        $collapseUsers = '';
                        $collapseEvents = array();
                        $userconflict = 0;
                        $collapseUsers .= '<ul class="to-do-list ui-sortable" id="sortable-todo" style="">';
                        for ($i = 0; $i < count($result); $i++) {
                            $endqried = date('d-m-Y', $result[$i]["end"]);
                            $startqried = date('d-m-Y', $result[$i]["start"]);
                            $endgiven = date('d-m-Y', $end);
                            $startgiven = date('d-m-Y', $start);
//                            echo $result[$i]["end"].' > '.$start.' && '.$result[$i]["start"].' < '.$end.' && '.$result[$i]["event_id"].' != '.$eventid;
//                            echo '<br>i) '.($result[$i]["end"] > $start && $result[$i]["start"] < $end).'<br>ii) ' ;
//                            echo ($result[$i]["event_id"] != $eventid).'<br>iii) ';
//                            echo ($endqried==$startgiven && $startqried == $endgiven).'<br>';
//                            echo $endqried.'=='.$startgiven.' && '.$startqried.' == '.$endgiven.'<br>';

                            if (($endqried == $startgiven && $startqried == $endgiven)
                                    && ($result[$i]["end"] > $start && $result[$i]["start"] < $end) && $result[$i]["event_id"] != $eventid) {
                                $userconflict++;
                                $attend = $result[$i]["user_id"];
                                $team = explode('_', $attend);
                                $user = $team[0] == 'Student' ? StudentInfo::findFirstById($team[1]) : StaffInfo::findFirstById($team[1]);
                                $username = $team[0] == 'Student' ? $user->Student_Name : $user->Staff_Name;
                                $uniqueid = $team[0] == 'Student' ? $user->Admission_no : $user->Appointment_no;
                                $stucla = $team[0] == 'Student' ? 'tar' : 'orange-theme';
                                $collapseUsers .= '<li class="clearfix" stuId = "<?= $attend ?>"  stuname ="<?= $username ?>">';
                                if ($user->photo) {
                                    $collapseUsers .= '<span title="' . $uniqueid . '" class="mini-stat-icon" style="cursor:default;">
                                                <img src="' . ($user->photo ? (FILES_URI . $user->photo) : ('images/user.jpg')) . '" 
                                                    title = "' . $uniqueid . '"
                                                     style = "border-radius:50%",
                                                    width = "40px",
                                                    height = "40px"/>
                                            </span>';
                                } else {
                                    $collapseUsers .= ' <span title= "' . $uniqueid . '" 
                                                 class="mini-stat-icon ' . $stucla . ' style="cursor:default;" >
                                                 ' . substr($username, 0, 1) . '</span>';
                                }
                                $collapseUsers .= ' <span title= "' . $uniqueid . '"
                                                 style="vertical-align: middle;position: relative;top: 10px;cursor:default;word-break: break-all;text-transform: uppercase !important;">
                                            ' . strlen($username) > 20 ? substr($username, 0, 20) . '...' : $username . '</span>
                                    </li>';
//                                $collapseUsers .= " <br><br><br>";
                                $evedata = json_decode($result[$i]["event_data"]);
                                $collapseEvents[$result[$i]["event_id"]] = '<span class="label label-primary">' . $evedata->title . "</span> scheduled from " . date('d-m-Y H:i:s', $result[$i]["start"]) . " to " . date('d-m-Y H:i:s', $result[$i]["end"]);
                                $error = 1;
                                //show in a pop up all clashes by participant
                            }
                        }

                        $collapseUsers .= ' </ul>';
                    }

//                            exit;
                    if ($error == 1) {

                        $message['type'] = 'error';
                        $message['title'] = 'Events affects availablity';
                        $message['message'] = 'Event you are trying to schedule clashes with events for ' . $userconflict . ' users'
                                . ' <a class="" role="button" data-toggle="collapse" 
                          href="#collapseUsers" aria-expanded="false" aria-controls="collapseUsers">
                          <span>View Users</span> 
                          </a> /<a class="" role="button" data-toggle="collapse" 
                          href="#collapseEvents" aria-expanded="false" aria-controls="collapseEvents">
                            <span>View events</span>
                          </a>';
                        $message['message'] .= '<div class="collapse" id="collapseUsers"> ';
                        $message['message'] .= $collapseUsers;
                        $message['message'] .= '</div>';
                        $message['message'] .= '<div class="collapse" id="collapseEvents"> <div class="well">';
                        $message['message'] .= implode('<br><br><br>', $collapseEvents);
                        $message['message'] .= '</div> </div>';
                        print_r(json_encode($message));
                        exit;
                    }
                }
//    else {
                $parti = str_replace("'", '', implode(',', $participantsforquery));
                $updtfields[] = "start = $start , end = $end ";
                $event_data['id'] = $eventid;
                $event_data['title'] = "$subjname";
                $event_data['start'] = $start;
                $event_data['end'] = $end;
                $event_data['subject'] = $subjectid;
                $event_data['classroom'] = $classroomid;
                $event_data['Attendees'] = $parti;
                $event_data['location'] = $locationdet;
                $event_data['location_name'] = ClassroomLocation::findFirstById($locationdet)->location_name;
                if (isset($status)) {
                    $updtfields[] = " status = '$status'";
                    $event_data['status'] = $status;
                }
                if (isset($mandatory)) {
                    $updtfields[] = " mandatory = '$mandatory'";
                    $event_data['mandatory'] = $mandatory;
                }
                $updtfields[] = " event_data = '" . json_encode($event_data) . "'";

                $buildUpdateuery = " UPDATE classroom_cal SET  " . implode(' , ', $updtfields)
                        . " WHERE event_id = '$eventid' and classroom_id = '$classroomid'; ";
//      echo $buildUpdateuery;// exit;
                if ($result1 = $obj->query($buildUpdateuery)) {
                    if ($result1[0]['result'] == 'success') {
                        foreach ($part as $paticipant):
                            $userid = $paticipant;
                            $buildParticipantUdptquery = "UPDATE user_cal SET " . implode(' , ', $updtfields)
                                    . " WHERE event_id = '$eventid' and user_id = $userid;";
//                            echo $buildParticipantUdptquery;exit;
                            if ($result2 = $obj->query($buildParticipantUdptquery)) {
                                if ($result2[0]['result'] != 'success') {
                                    $message['type'] = 'error';
                                    $message['title'] = 'Error - Update event';
                                    $message['message'] = 'Problem in creating event';
                                    print_r(json_encode($message));
                                    exit;
                                }
                            }
                            $i++;
                        endforeach;
                    }
                }
//    }


                if ($i > 0) {
                    $message['type'] = 'success';
                    $message['title'] = 'Update Event';
                    $message['message'] = 'Event Successfully Saved';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function locationsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->classroomid = $classroomid = $this->request->getPost('classroomid');
            $this->view->locations = $locations = ClassroomLocation::find('classroom_master_id = ' . $classroomid);
        }
    }

    public function addLocationsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->classroomid = $this->request->getPost('classroomid');
        }
    }

    public function saveClassroomLocationAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }
        $exists = ClassroomLocation::findFirst('classroom_master_id = ' . $params['classroomid'] . " and location_name LIKE '$params[location_name]'");
        if ($exists) {
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">  Location Name Exist.</div>';
            print_r(json_encode($message));
            exit;
        } else {
            $location = new ClassroomLocation();
            $location->classroom_master_id = $params['classroomid'];
            $location->location_name = $params['location_name'];
            $location->created_by = $uid;
            $location->created_on = time();
            $location->modified_by = $uid;
            $location->modified_on = time();
            if ($location->save()) {
                $message['type'] = 'success';
                $message['ItemID'] = $location->id;
                $message['message'] = '<div class="alert alert-block alert-success fade in">Location Details saved Successfully</div>';
                print_r(json_encode($message));
                exit;
            } else {
                $error = '';
                foreach ($location->getMessages() as $msg) {
                    $error .= $msg;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function deleteEventAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            foreach ($this->request->getPost() as $key => $vl) {
                $params[$key] = $vl;
            }
            $eventid = $params['eventid'];
            $classroomid = $params['classroomid'];
            $subjectid = $params['subjectid'];
            $obj = new Cassandra();
            $res = $obj->connect(CASSANDRASERVERIP, '', '', SUBDOMAIN, '9042');
            if ($res) {
                $classmaster = ClassroomMaster::findFirstById($classroomid);
                $stuquery = ControllerBase::buildStudentQuery($classmaster->aggregated_nodes_id);
                $students = StudentMapping::find('status = "Inclass" and ' . implode(' or ', $stuquery));
                $staff = GroupSubjectsTeachers::find("classroom_master_id =$classroomid  and subject_id = $subjectid");

                if (count($staff) > 0):
                    foreach ($staff as $stf) {
                        $splitteach = explode(',', $stf->teachers_id);
                        $part = preg_filter('/^([\d])*/', "'Staff_$0'", $splitteach);
                    }
                endif;
                if (count($students) > 0):
                    foreach ($students as $stu) {
                        $part[] = "'Student_" . $stu->id . "'";
                    }
                endif;
                $buildUpdateuery = " DELETE FROM classroom_cal "
                        . " WHERE event_id = '$eventid' and classroom_id = '$classroomid'; ";
                //echo $buildUpdateuery; exit;
                if ($result1 = $obj->query($buildUpdateuery)) {
                    if ($result1[0]['result'] == 'success') {
                        foreach ($part as $paticipant):
                            $userid = $paticipant;
                            $buildParticipantUdptquery = "DELETE FROM user_cal"
                                    . " WHERE event_id = '$eventid' and user_id = $userid;";
                            // echo $buildParticipantUdptquery;exit;
                            if ($result2 = $obj->query($buildParticipantUdptquery)) {
                                if ($result2[0]['result'] != 'success') {
                                    $message['type'] = 'error';
                                    $message['title'] = 'Error - deleting event';
                                    $message['message'] = 'Problem in deleting event';
                                    print_r(json_encode($message));
                                    exit;
                                }
                            }
                            $i++;
                        endforeach;

                        if ($i > 0) {
                            $message['type'] = 'success';
                            $message['title'] = 'Delete Event';
                            $message['message'] = 'Event Successfully Removed';
                            print_r(json_encode($message));
                            exit;
                        }
                    } else {
                        $message['type'] = 'error';
                        $message['title'] = 'Error - deleting event';
                        $message['message'] = 'Problem in deleting event';
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }
        }
    }

}
