<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class ExamController extends ControllerBase {

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    public function indexAction() {
        $this->tag->prependTitle("Exam | ");
        $this->assets->addCss('css/edustyle.css');
    }

    public function addExamoldAction() {
        // print_r($this->request->getPost());
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
//        $this->tag->prependTitle("Exam | ");
//        $this->assets->addJs("js/exam/exam.js");
        $this->view->examTypeActive = ($this->request->getPost('examType')) ? $this->request->getPost('examType') : '1';
        if ($this->request->getPost('examType') == '2' && $this->request->getPost('exam_text') != '') {
            $phql = 'SELECT m.id,m.exam_name,m.div_val_id,m.mark_record_start_date ,m.mark_record_end_date,m.mainexam_id,
                            m.mainexam_outof,ma.all_class                       
                            FROM Mainexam m
                            Left JOIN MainexamApplicability ma ON ma.mainexam_name = m.exam_name
                            WHERE m.id =' . $this->request->getPost('examId') .
                    ' GROUP BY m.exam_name';

            $result = $this->modelsManager->executeQuery($phql);
            foreach ($result as $value) {
                // print_r($value);
                $this->view->form = new AddExamForm($value, array(
                    'edit' => $this->view->examTypeActive
                ));
                $this->view->mainexamchecked = ($value->mainexam_id) ? 'checked' : '';
                $this->view->alclasschecked = ($value->all_class) ? '' : 'checked';
//            $this->view->mainExamId =  ($value->mainexam_id)?$value->mainexam_id:'';
//            $this->view->mainExamOutOf =  ($value-> mainexam_outof)?$value-> mainexam_outof:'';
                $this->view->mainexamVal = $value;
            }
        } else {
            $this->view->form = new AddExamForm(null, array(
                'edit' => $this->view->examTypeActive
            ));
        }
        $this->view->display_mainexam = 1;

        $mainexam_applicable_to_all_class = MainexamApplicability::find('all_class = 1');

        //echo '<div class="alert alert-block alert-danger fade in">'.count($mainexam_applicable_to_all_class).'</div>';

        $j = 0;
        if (count($mainexam_applicable_to_all_class) > 0) {
            foreach ($mainexam_applicable_to_all_class as $record) {
                //echo '<div class="alert alert-block alert-danger fade in">'.$record->mainexam_name.'</div>';
                $mainexams[$record->mainexam_name] = $record->mainexam_name;
            }
            $this->view->mainexams = $mainexams;
        } else {
            $this->view->display_mainexam = 0;
        }

        $this->view->division_name = ControllerBase::get_division_name_student();
        $this->view->subdivision_name = ControllerBase::get_sub_division_name_student();
        $this->view->division_values = ControllerBase::get_division_values_student(ControllerBase::get_current_academic_year());
        $this->view->mainexamList = Mainexam::find();
    }

    public function add_main_examAction() {

        $this->tag->prependTitle("Exam | ");
//        $this->assets->addJs("js/exam/exam.js");
        $identity = $this->auth->getIdentity();
        $message = array();
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $form = new AddExamForm();
        try {
            if ($this->request->isPost()) {
                if ($form->isValid($this->request->getPost()) == false) {
                    $error = "";
                    foreach ($form->getMessages() as $message) {
                        $error .= $message;
                    }

                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $linked = $this->request->getPost('link_check_box');

                    if ($linked == 'true') {
                        $linked_exam_name = $this->request->getPost('mainexam');
                        $weightage = $this->request->getPost('weightage');
                        //echo '<div class="alert alert-block alert-danger fade in">'.$linked_exam_name.$weightage.'</div>';
                    }
                    $examName = ($this->request->getPost('examType') == 1) ? $this->request->getPost('exam_text') :
                            Mainexam::findFirstById($this->request->getPost('exam_type_id'))->exam_name;

                    $record_start = $this->request->getPost('mark_record_start');
                    $mark_record_start = strtotime($record_start);
                    $record_end = $this->request->getPost('mark_record_end');
                    $divValId = $this->request->getPost('divVal');
                    $mark_record_end = strtotime($record_end);
//                    $total_mark = $this->request->getPost('totalmarks');
                    $cacdyr = AcademicYearMaster::findFirstByStatus('c')->id;

                    $div = DivisionMaster::findFirst('divType = "student" and academicYrID=' . $cacdyr);
                    $divVals = DivisionValues::find('divID = ' . $div->id);

                    $duplicate = 0;
                    $i = 0;
                    $linked_exam_not_found = 0;
                    $weighage_error = 0;
                    $records = array();
                    if ($divValId != 0) {
                        $unique = Mainexam::find('exam_name = "' . $examName . '"' .
                                        ' and div_val_id = ' . $divValId . ' and academic_year_id = ' . $cacdyr);
                        if ($linked == 'true') {
                            $link_mainexam_records = Mainexam::find('exam_name = "' . $linked_exam_name . '"' .
                                            ' and div_val_id = ' . $divValId . ' and academic_year_id = ' . $cacdyr);
//                                            print_r('exam_name = "' . $linked_exam_name .'"'.
//                                                            ' and div_val_id = ' . $divValId. ' and academic_year_id = ' . $cacdyr);exit;
                            if (count($link_mainexam_records) > 0) {
                                $mainexam_id = $link_mainexam_records[0]->id;
                                $records[$i] = array(
                                    'div_val_id' => $divValId,
                                    'academic_year_id' => $cacdyr,
                                    'exam_name' => $this->request->getPost('exam_text'),
                                    'mainexam_id' => $mainexam_id,
                                    'mainexam_outof' => $weightage,
                                    'mark_record_start_date' => $mark_record_start,
                                    'mark_record_end_date' => $mark_record_end,
                                    'created_by' => $identity['id'],
                                    'created_date' => time(),
                                    'modified_by' => $identity['id'],
                                    'modified_date' => time(),
                                );

                                if ($this->request->getPost('examType') == 2) {
                                    $records[$i]['id'] = $this->request->getPost('exam_type_id');
                                }
                                $availweitage = $link_mainexam_records[0]->available_outof +
                                        ((count($unique) > 0 && $unique[0]->mainexam_outof ) ? $unique[0]->mainexam_outof : 0) - $weightage;

//                                                        echo '<pre>'.$weightage.'<br>'.$unique[0]->mainexam_outof.'<br>'
//                                                                .$link_mainexam_records[0]->available_outof.'<br>'.$availweitage.'<br>';
//                                                        echo $availweitage;exit;
                                $linkrecords[$i] = array(
                                    'available_outof' => $availweitage
                                );
                                $i++;
                            } else {
                                $linked_exam_not_found = 1;
                            }
                        } else {
                            $records[$i] = array(
                                'div_val_id' => $divValId,
                                'academic_year_id' => $cacdyr,
                                'exam_name' => $this->request->getPost('exam_text'),
                                'mark_record_start_date' => $mark_record_start,
                                'mark_record_end_date' => $mark_record_end,
                                'created_by' => $identity['id'],
                                'created_date' => time(),
                                'modified_by' => $identity['id'],
                                'modified_date' => time(),);
                            if ($this->request->getPost('examType') == 2) {
                                $records[$i]['id'] = $this->request->getPost('exam_type_id');
                            }
                            $i++;
                        }
                        if (count($unique) > 0 && ($this->request->getPost('examType') == '1')) {
                            $duplicate++;
                        }
                    } else {
                        foreach ($divVals as $divVal) {
                            $unique = Mainexam::find('exam_name = "' . $examName . '"' .
                                            ' and div_val_id = ' . $divVal->id . ' and academic_year_id = ' . $cacdyr);
                            if ($linked == 'true') {
                                $link_mainexam_records = Mainexam::find('exam_name = "' . $linked_exam_name . '"' .
                                                ' and div_val_id = ' . $divVal->id . ' and academic_year_id = ' . $cacdyr);

                                if (count($link_mainexam_records) > 0) {
                                    $mainexam_id = $link_mainexam_records[0]->id;
                                    $records[$i] = array(
                                        'div_val_id' => $divVal->id,
                                        'academic_year_id' => $cacdyr,
                                        'exam_name' => $this->request->getPost('exam_text'),
                                        'mainexam_id' => $mainexam_id,
                                        'mainexam_outof' => $weightage,
                                        'mark_record_start_date' => $mark_record_start,
                                        'mark_record_end_date' => $mark_record_end,
                                        'created_by' => $identity['id'],
                                        'created_date' => time(),
                                        'modified_by' => $identity['id'],
                                        'modified_date' => time(),
                                    );
                                    if ($this->request->getPost('examType') == 2) {
                                        $records[$i]['id'] = $this->request->getPost('exam_type_id');
                                    }
                                    $availweitage = $link_mainexam_records[0]->available_outof +
                                            ((count($unique) > 0 && $unique[0]->mainexam_outof ) ? $unique[0]->mainexam_outof : 0) - $weightage;

//                                                        echo '<pre>'.$weightage.'<br>'.$unique[0]->mainexam_outof.'<br>'
//                                                                .$link_mainexam_records[0]->available_outof.'<br>'.$availweitage.'<br>';
//                                                        echo $availweitage;exit;
                                    $linkrecords[$i] = array(
                                        'available_outof' => $availweitage
                                    );
                                    $i++;
                                } else {
                                    $linked_exam_not_found = 1;
                                }
                            } else {
                                $records[$i] = array(
                                    'div_val_id' => $divVal->id,
                                    'academic_year_id' => $cacdyr,
                                    'exam_name' => $this->request->getPost('exam_text'),
                                    'mark_record_start_date' => $mark_record_start,
                                    'mark_record_end_date' => $mark_record_end,
                                    'created_by' => $identity['id'],
                                    'created_date' => time(),
                                    'modified_by' => $identity['id'],
                                    'modified_date' => time(),);

                                if ($this->request->getPost('examType') == 2) {
                                    $records[$i]['id'] = $this->request->getPost('exam_type_id');
                                }
                                $i++;
                            }
                            if (count($unique) > 0 && ($this->request->getPost('examType') == '1')) {
                                $duplicate++;
                            }
                        }
                    }


                    if ($duplicate > 0) {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">Exam Exists already!Try again with a different name</div>';
                        print_r(json_encode($message));
                        exit;
                    } else if ($linked_exam_not_found) {
                        $message['type'] = 'error';
                        $message['message'] = '<div class="alert alert-block alert-danger fade in">Linked exam not found. Main exam not added.Try again</div>';
                        print_r(json_encode($message));
                        exit;
                    } else {

//                            print_r($records);exit;
//                            print_r($linkrecords);exit;
                        $failed = 0;
                        for ($i = 0; $i < count($records); $i++) {

//                            $mainexam = Mainexam::findFirst('exam_name = "' . $records[$i]['exam_name'] . '"' .
//                                            ' and div_val_id = ' . $records[$i]['div_val_id'] .
//                                            ' and academic_year_id = ' . $records[$i]['academic_year_id']) ?
//                                    Mainexam::findFirst('exam_name = "' . $records[$i]['exam_name'] . '"' .
//                                            ' and div_val_id = ' . $records[$i]['div_val_id'] .
//                                            ' and academic_year_id = ' . $records[$i]['academic_year_id']) :
//                                    new Mainexam();
                            $mainexam = $records[$i]['id'] ?
                                    Mainexam::findFirstById($records[$i]['id']) :
                                    ( Mainexam::findFirst('exam_name = "' . $records[$i]['exam_name'] . '"' .
                                            ' and div_val_id = ' . $records[$i]['div_val_id'] .
                                            ' and academic_year_id = ' . $records[$i]['academic_year_id']) ?
                                            Mainexam::findFirst('exam_name = "' . $records[$i]['exam_name'] . '"' .
                                                    ' and div_val_id = ' . $records[$i]['div_val_id'] .
                                                    ' and academic_year_id = ' . $records[$i]['academic_year_id']) :
                                            new Mainexam());

                            $mainexam->assign($records[$i]);
                            if (!($mainexam->save())) {
                                $failed++;
                            } else {
                                if ($linked == 'true') {
                                    $linkedrecords = Mainexam::findFirstById($records[$i]['mainexam_id']);
                                    $linkedrecords->assign($linkrecords[$i]);
                                    if (!($linkedrecords->save())) {
                                        $failed++;
                                    } else {
                                        $subjects = SubjectsDivision::find('divValId = ' . $records[$i]['div_val_id']);
                                        foreach ($subjects as $subj) {
                                            $mainexRec = MainexamMarks::find('mainexam_id = ' . $records[$i]['mainexam_id'] .
                                                            ' and combinationId = ' . $subj->id);
                                            $student_id_arr = array();
                                            foreach ($mainexRec as $mainEx) {
                                                $student_id_arr[] = $mainEx->student_id;
                                            }
                                            $stuarr = array_unique($student_id_arr);
                                            //getallstudents under this exam
                                            $res = 1;
                                            foreach ($stuarr as $student_id) {
                                                $recursiveParam = array(
                                                    'stuId' => $student_id,
                                                    'x2' => $records[$i]['mainexam_id'],
                                                    'obtainedMarks' => '',
                                                    'obtainedOutOf' => '',
                                                    'CombiId' => $subj->id
                                                );
                                                $res = MarkController::_calculateMarksToLinkedExamsNEW($recursiveParam);
                                            }
                                        }
                                    }
                                }
                            }
                        }
//                            print_r($failed  . $divValId);exit;
                        if ($failed == 0 && $divValId == 0) {

                            $mainexam_applic = new MainexamApplicability();
                            $mainexam_applic->assign(array('mainexam_name' => $examName, 'all_class' => 1));
//                            print_r($mainexam_applic);exit;
                            if (!($mainexam_applic->save())) {
                                $error = '';
                                foreach ($mainexam_applic->getMessages() as $message) {
                                    $error .= $message;
                                }

                                $message['type'] = 'error';
                                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                                print_r(json_encode($message));
                                exit;
                            }

                            $message['type'] = 'success';
                            $message['message'] = '<div class="alert alert-success">Exam Added Successfully to all classes</div>';
                            print_r(json_encode($message));
                            exit;
                        } else if ($failed != 0) {
                            $error = '';
                            foreach ($mainexam->getMessages() as $message) {
                                $error .= $message;
                            }

                            $message['type'] = 'error';
                            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                            print_r(json_encode($message));
                            exit;
                        } else {

                            $message['type'] = 'success';
                            $message['message'] = '<div class="alert alert-success">Exam Added Successfully</div>';
                            print_r(json_encode($message));
                            exit;
                        }
                    }
                }
            }
        } catch (Exception $e) {

            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    public function addMainExamLoadSubjectsAction() {

        $this->tag->prependTitle("Exam | ");
//        $this->assets->addJs("js/exam/exam.js");
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_subject = 1;
        $divVal = $this->request->getPost('divVal');
        $subdivval = $this->request->getPost('subdivId');
        $formname = $this->request->getPost('formname');

        //echo '<div class="alert alert-block alert-danger fade in">' . $divVal .$subdivval. '</div>';

        $records = SubDivisionSubjects::find('divValID =' . $divVal . ' and subDivValId=' . $subdivval);

        if (count($records) > 0) {
            foreach ($records as $record) {

                $subject_name = SubjectsDivision::findfirst('id = ' . $record->subject_id)->subjectName;
                $subjects[$record->subject_id] = $subject_name;
            }
            $this->view->subjects = $subjects;
            $this->view->formname = $formname;
        } else {
            $this->view->display_subject = 0;
        }
    }

    public function addMainExamLinkAction() {

        $this->tag->prependTitle("Exam | ");
//        $this->assets->addJs("js/exam/exam.js");
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_mainexam = 1;
        $divVal = $this->request->getPost('divVal');

        $formname = $this->request->getPost('formname');
        $subdivval = $this->request->getPost('subdivId_' . $formname);
        $subid = $this->request->getPost('subid');
        $cacdyr = ControllerBase::get_current_academic_year();

        //echo '<div class="alert alert-block alert-danger fade in">' . $divVal .$subdivval. '</div>';

        $records = Mainexam::find('div_val_id =' . $divVal . ' and sub_div_val_id=' . $subdivval . ' and sub_div_subjects_id = ' .
                        $subid . ' and academic_year_id = ' . $cacdyr);

        if (count($records) > 0) {
            foreach ($records as $record) {

                $mainexams[$record->id] = $record->exam_name;
            }
            $this->view->mainexams = $mainexams;
            $this->view->formname = $formname;
        } else {
            $this->view->display_mainexam = 0;
        }
    }

    public function addMainExamByClassAction() {

        $this->tag->prependTitle("Exam | ");
//        $this->assets->addJs("js/exam/exam.js");
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $form = new AddExamForm();

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) == false) {
                $error = "";
                foreach ($form->getMessages() as $message) {
                    $error .= $message;
                }
                echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                exit;
            } else {
                $linked = $this->request->getPost('linked');
                if ($linked == 1) {
                    $linked_exam_name = $this->request->getPost('linkmainexam');
                    $weightage = $this->request->getPost('linkweightage');
                    //echo '<div class="alert alert-block alert-danger fade in">'.$linked_exam_name.$weightage.'</div>';
                }

                $examName = $this->request->getPost('exam_text');
                $mark_record_start = $this->request->getPost('mark_record_start');
                $mark_record_end = $this->request->getPost('mark_record_end');
                $divVal = $this->request->getPost('divVal');
                $formname = $this->request->getPost('formname');
//                $subdivval = $this->request->getPost('subdivId_' . $formname);
//                $subid = $this->request->getPost('subid_' . $formname);
                $cacdyr = ControllerBase::get_current_academic_year();
//                $total_mark = $this->request->getPost('totalmarks');
                //echo '<div class="alert alert-block alert-danger fade in">'.$examName." ".$mark_record_start." ".
                //$mark_record_end." ".$divVal." ".$subdivval." ".$subid. '</div>';


                $unique = Mainexam::find('exam_name = "' . $examName . '"' .
                                ' and div_val_id = ' . $divVal .
                                ' and academic_year_id = ' . $cacdyr);
                $weighage_error = 0;
                $linked_exam_not_found = 0;
                if ($linked == 1) {
                    $link_mainexam_records = Mainexam::find('id =' . $linked_exam_name);

                    if (count($link_mainexam_records) > 0) {
                        $mainexam_id = $link_mainexam_records[0]->id;
                        if ($weightage <= $link_mainexam_records[0]->total_marks) {
                            $record = array(
                                'div_val_id' => $divVal,
//                                'sub_div_val_id' => $subdivval,
                                'academic_year_id' => $cacdyr,
//                                'sub_div_subjects_id' => $subid,
                                'exam_name' => $examName,
//                                'total_marks' => $total_mark,
                                'mark_record_start_date' => $mark_record_start,
                                'mark_record_end_date' => $mark_record_end,
                                'mainexam_id' => $mainexam_id,
//                                'available_outof' => (count($unique) > 0 && $unique[0]->available_outof) ?
//                                        $unique[0]->available_outof : '',
                                'mainexam_outof' => $weightage);
//                            $availweitage = $link_mainexam_records[0]->available_outof +
//                                    ((count($unique) > 0 && $unique[0]->mainexam_outof ) ? $unique[0]->mainexam_outof : 0) - $weightage;
//                                                    
                            $linkrecords = array(
//                                'available_outof' => $availweitage
                            );
                        } else {
                            $weighage_error = 1;
                        }
                    } else {
                        $linked_exam_not_found = 1;
                    }
                } else {
                    $record = array(
                        'div_val_id' => $divVal,
//                        'sub_div_val_id' => $subdivval,
                        'academic_year_id' => $cacdyr,
//                        'sub_div_subjects_id' => $subid,
                        'exam_name' => $examName,
//                        'total_marks' => $total_mark,
                        'mark_record_start_date' => $mark_record_start,
//                        'available_outof' => (count($unique) > 0 && $unique[0]->available_outof) ?
//                                $unique[0]->available_outof : $total_mark,
                        'mark_record_end_date' => $mark_record_end);
                }


                if (count($unique) > 0) {
                    echo '<div class="alert alert-block alert-danger fade in">Exam Exists already!Try a different name</div>';
                    exit;
                } else if ($weighage_error) {
                    echo '<div class="alert alert-block alert-danger fade in">Entered Weightage Exceeds Total Mark of Selected Main Exam. Try again.</div>';
                    exit;
                } else if ($linked_exam_not_found) {
                    echo '<div class="alert alert-block alert-danger fade in">Linked exam not found. Main exam not added.Try again</div>';
                    exit;
                } else {

                    $mainexam = new Mainexam();
                    $mainexam->assign($record);

                    if ($mainexam->save()) {

//                        if ($linked == 1) {
//                            $linkedrecords = Mainexam::findFirstById($record['mainexam_id']);
//                            $linkedrecords->assign($linkrecords);
//                            if (($linkedrecords->save())) {
//                                echo '<div class="alert alert-success">Exam Added Successfully to all classes and sections</div>';
//                                exit;
//                            }
//                        }
                    } else {
                        $error = '';
                        foreach ($mainexam->getMessages() as $message) {
                            $error .= $message;
                        }
                        echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                        exit;
                    }
                }
            }
        }
    }

    public function viewMainExamAction() {
        $this->tag->prependTitle("View Main Exam | ");
        $this->view->form = new ViewMainExamForm();
        $this->assets->addJs("js/exam/viewexam.js");
    }

    public function viewMainExamByAcdYearAction() {
        $this->tag->prependTitle("View Main Exam | ");
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $this->view->display_table = 1;
        if ($this->request->isPost()) {


            $cacdyr = $this->request->getPost('academic_year_name');

            //For staff, the below call lists only the division which the staff is authorized to view
            $div_vals = ControllerBase::get_division_values_student($cacdyr);
            foreach ($div_vals as $div_val) {
                $div_vals_id[] = $div_val->id;
            }
            $div_vals_id_string = implode(',', $div_vals_id);
            //echo '<div class="alert alert-block alert-danger fade in">' .$div_vals_id_string. '</div>';
            //fetch the records
            $records = Mainexam::find('academic_year_id=' . $cacdyr . ' and div_val_id IN (' .
                            $div_vals_id_string . ') GROUP BY div_val_id, sub_div_val_id, exam_name');



            //echo '<div class="alert alert-block alert-danger fade in">' .count($records). '</div>';
            //echo '<div class="alert alert-block alert-danger fade in">' . $classId . $subdivid . $cacdyr.$no_of_subjects.'</div>';
            $i = 0;
            if (count($records) > 0) {

                $divs = ControllerBase::get_division_values_student($cacdyr);
                foreach ($records as $record) {

                    //Fetch the class name
                    $class_name = DivisionValues::findfirst('id = ' . $record->div_val_id)->classname;
                    $section_name = SubDivisionValues::findfirst('id = ' . $record->sub_div_val_id)->name;
                    //echo '<div class="alert alert-block alert-danger fade in">' .$record->sub_div_subjects_id. '</div>';
                    //echo '<div class="alert alert-block alert-danger fade in">' .$subject_id. '</div>';
                    $exam_name = $record->exam_name;
                    $mark_record_start = $record->mark_record_start_date;
                    $mark_record_end = $record->mark_record_end_date;
                    if (is_null($record->mainexam_id)) {
                        $linked_exam_name = "NA";
                        $weightage = "NA";
                    } else {
                        $linked = Mainexam::findfirst('id = ' . $record->mainexam_id);
                        $linked_exam_name = $linked->exam_name;
                        $weightage = $record->mainexam_outof;
                    }

                    //echo '<div class="alert alert-block alert-danger fade in">' .$record->mainexam_id. '</div>';
                    //echo '<div class="alert alert-block alert-danger fade in">' . $class_name .$section_name.$subject_name.$record->class_test_name. '</div>';
                    $rows[$i++] = array($exam_name,
                        $class_name,
                        $section_name,
                        date('m-d-Y', $mark_record_start),
                        date('m-d-Y', $mark_record_end),
                        $linked_exam_name,
                        $weightage);
                }
                $this->view->rows = $rows;
                $this->view->divs = $divs;
                $this->view->div_name = ControllerBase::get_division_name_student();
                $this->view->subdiv_name = ControllerBase::get_sub_division_name_student();

                //echo '<div class="alert alert-block alert-danger fade in">' .$this->view->div_name.$this->view->subdiv_name. '</div>';                
            } else {
                $this->view->display_table = 0;
            }
        }
    }

    public function viewMainExamByClassAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_table = 1;
        if ($this->request->isPost()) {
            $classId = $this->request->getPost('classID');

            $cacdyr = $this->request->getPost('acd_id');

            //fetch the records           
            $records = Mainexam::find('academic_year_id=' . $cacdyr . ' and div_val_id = ' . $classId . ' GROUP BY div_val_id, sub_div_val_id, exam_name');

            //echo '<div class="alert alert-block alert-danger fade in">' .count($records). '</div>';
            //echo '<div class="alert alert-block alert-danger fade in">' . $classId . $subdivid . $cacdyr.$no_of_subjects.'</div>';
            $i = 0;
            if (count($records) > 0) {

                $divs = ControllerBase::get_division_values_student($cacdyr);
                $class_name = DivisionValues::findfirst('id = ' . $classId)->classname;
                foreach ($records as $record) {

                    $section_name = SubDivisionValues::findfirst('id = ' . $record->sub_div_val_id)->name;
                    //echo '<div class="alert alert-block alert-danger fade in">' .$record->sub_div_subjects_id. '</div>';
                    //echo '<div class="alert alert-block alert-danger fade in">' .$subject_id. '</div>';
                    $exam_name = $record->exam_name;
                    $mark_record_start = $record->mark_record_start_date;
                    $mark_record_end = $record->mark_record_end_date;

                    if (is_null($record->mainexam_id)) {
                        $linked_exam_name = "NA";
                        $weightage = "NA";
                    } else {
                        $linked = Mainexam::findfirst('id = ' . $record->mainexam_id);
                        $linked_exam_name = $linked->exam_name;
                        $weightage = $record->mainexam_outof;
                        //echo '<div class="alert alert-block alert-danger fade in">' .$linked_exam_name." ".$weightage. '</div>';
                    }

                    //echo '<div class="alert alert-block alert-danger fade in">' .$record->mainexam_id. '</div>';
                    //echo '<div class="alert alert-block alert-danger fade in">' . $class_name .$section_name.$subject_name.$record->class_test_name. '</div>';
                    $rows[$i++] = array($exam_name,
                        $class_name,
                        $section_name,
                        date('m-d-Y', $mark_record_start),
                        date('m-d-Y', $mark_record_end),
                        $linked_exam_name,
                        $weightage);
                }

                $this->view->class_id = $classId;
                $this->view->rows = $rows;
                $this->view->divs = $divs;
                $this->view->div_name = ControllerBase::get_division_name_student();
                $this->view->subdiv_name = ControllerBase::get_sub_division_name_student();
            } else {
                $this->view->display_table = 0;
            }
        }
    }

    public function ClassTestAction() {
        $this->tag->prependTitle("Add Class Test | ");
        $this->assets->addJs("js/exam/exam.js");

        $cacdyr = ControllerBase::get_current_academic_year();
        $div = DivisionMaster::findfirst('divType = "student" and academicYrID=' . $cacdyr);
        $identity = $this->auth->getIdentity();
        if (in_array('Staff', $identity['role_name'])) {
            $this->view->staff = $staff = StaffInfo::findFirstByLoginid($identity['name']);
            $combinations = array();

            $assignedCount = SubjectTeachers::find('teacher_id = ' . $staff->id);
            if (count($assignedCount) > 0) {
                foreach ($assignedCount as $assignedclass) {
                    $getCombinations = SubDivisionSubjects::find('id=' . $assignedclass->subdivisions_subjects_id);
                    foreach ($getCombinations as $formCombination) {
                        $division = DivisionValues::findFirstById($formCombination->div_val_id)->classname;
                        $subdivname = array();
                        $subdivision = explode(',', $formCombination->concat_subdivs);
                        foreach ($subdivision as $svalue) {
                            $subdivname[] = SubDivisionValues::findFirstById($svalue)->name;
                        }
                        $subdivnames = implode(' - ', $subdivname);
                        $subjects = SubjectsDivision::findFirstById($formCombination->subject_id)->subjectName;
                        $combinations[$formCombination->id] = $division . ' - ' . $subdivnames . ' - ' . $subjects;
                    }
                }
                $this->view->combinations = $combinations;
                $this->view->divname = ControllerBase::get_division_name_student();
            } else {
                $this->view->errorMessage = 'No Subjects assigned for you yet!';
            }
        } else {
            $this->view->errorMessage = 'Only teachers have access to this page!';
        }
    }

    public function loadSubdivisionSubjectsAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_subdivsubjects = 0;
        if ($this->request->isPost()) {
            $this->view->classId = $this->request->getPost('classId');

            $this->view->subdiv_name = ControllerBase::get_sub_division_name_student();
            $subdivvals = ControllerBase::get_sub_division_values_student($this->view->classId);
//            echo'<pre>';
//            print_r(ControllerBase::get_subdivision_subjects($this->view->classId));
//            exit;
            $subdivvals = ControllerBase::get_subdivision_subjects($this->view->classId);
            if (count($subdivvals)) {
                $this->view->subdivvals = $subdivvals;
                $this->view->display_subdivsubjects = 1;
            }
        }
    }

    public function getSectionByClassAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display_subdiv = 0;
        if ($this->request->isPost()) {
            $this->view->classId = $this->request->getPost('classId');

            $this->view->subdiv_name = ControllerBase::get_sub_division_name_student();
            $subdivvals = ControllerBase::get_sub_division_values_student($this->view->classId);

            if (count($subdivvals)) {
                $this->view->subdivvals = $subdivvals;
                $this->view->display_subdiv = 1;
            }
        }
    }

    public function getSubjectsByClassAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display = 0;
        if ($this->request->isPost()) {
            $this->view->classId = $this->request->getPost('class_id');

            $this->view->subdivid = $this->request->getPost('subDivVal');

            $subjects = ControllerBase::get_subjects($this->view->classId, $this->view->subdivid);

            if (count($subjects)) {
                $this->view->display = 1;
                $this->view->subjects = $subjects;
            }
            //echo '<div class="alert alert-block alert-danger fade in">' .$linked_exam_name." ".$weightage. '</div>';
        }
    }

    public function addTestByClassAction() {

        // No rendering
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $subject_masterid = $this->request->getPost('subject_masterid');

            $test_name = strtoupper($this->request->getPost('test_name'));

            $cacdyr = ControllerBase::get_current_academic_year();

            $identity = $this->auth->getIdentity();

            //echo '<div class="alert alert-block alert-danger fade in">' . $classId . $subdivid . $cacdyr.$no_of_subjects.'</div>';
            //Get subject id from subject_masterid
            $subject = SubDivisionSubjects::findfirst('id = ' . $subject_masterid);

            $unique = ClassTest::find('subject_master_id =' . $subject->subject_id .
                            'and academic_year_id =' . $cacdyr . ' and class_test_name = "' . $test_name . '"');

            //echo '<div class="alert alert-block alert-danger fade in">' . $classId . $subdivid . $cacdyr . $subject_id . '</div>';

            if (count($unique) > 0) {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Class Test Name Exists already!Try a different name</div>';
                print_r(json_encode($message));
                exit;
            } else {

                $classtest = new ClassTest();
                $classtest->subject_master_id = $subject->subject_id;
                $classtest->academic_year_id = $cacdyr;
                $classtest->class_test_name = $test_name;
                $classtest->div_val_id = $subject->div_val_id;
                $classtest->concat_subdivs = $subject->concat_subdivs;
                $classtest->created_by = $identity['id'];
                $classtest->created_date = time();
                if (($classtest->save())) {
                    $message['type'] = 'success';
                    $message['message'] = '<div >Class Test Added Successfully!!</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($classtest->getMessages() as $msg) {
                        $error .= $msg;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function editTestByClassAction() {

        // No rendering
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {
            $subject_masterid = $this->request->getPost('subject_masterid');

            $test_name = strtoupper($this->request->getPost('test_name'));

            $test_id = strtoupper($this->request->getPost('test_id'));

            $cacdyr = ControllerBase::get_current_academic_year();

            $identity = $this->auth->getIdentity();

            //echo '<div class="alert alert-block alert-danger fade in">' . $classId . $subdivid . $cacdyr.$no_of_subjects.'</div>';
            $subject_id = SubDivisionSubjects::findfirst('id = ' . $subject_masterid)->subject_id;

            $unique = ClassTest::find('subject_master_id =' . $subject_id . ' and class_test_id != ' . $test_id .
                            'and academic_year_id =' . $cacdyr . ' and class_test_name = "' . $test_name . '"');
            //echo '<div class="alert alert-block alert-danger fade in">' . $classId . $subdivid . $cacdyr . $subject_id . '</div>';

            if (count($unique) > 0) {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Class Test Name Exists already!Try a different name</div>';
                print_r(json_encode($message));
                exit;
            } else {

                $classtest = ClassTest::findfirst('class_test_id = ' . $test_id);
                $linkedrecord = $classtest->mainexam_id;
                $linkedRecordSubj = $classtest->subject_master_id;
                $res = 1;
                $classtest->subject_master_id = $subject_id;
                $classtest->academic_year_id = $cacdyr;
                $classtest->class_test_name = $test_name;
                $classtest->mainexam_id = NULL;
                $classtest->mainexam_outof = NULL;
                $classtest->modified_by = $identity['id'];
                $classtest->modified_date = time();

                if (!$classtest->save()) {
                    $error = '';
                    foreach ($classtest->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }

                if ($linkedrecord > 0) {
                    $student_id_arr = array();
                    $testmarks = ClassTestMarks::find('class_test_id = ' . $test_id);
                    if (count($testmarks) > 0):
                        foreach ($testmarks as $tstm) {
                            $student_id_arr[] = $tstm->student_id;
                        }
                    endif;

                    $stuarr = array_unique($student_id_arr);
                    foreach ($stuarr as $student_id) {
                        $res = 0;
                        $recursiveParam = array(
                            'type' => 'main',
                            'stuId' => $student_id,
                            'x2' => $linkedrecord,
                            'obtainedMarks' => '',
                            'CombiId' => $linkedRecordSubj,
                            'obtainedOutOf' => '',
                        );

                        $res = MarkController::_calculateMarksToLinkedExamsNEW($recursiveParam);
                    }
                }
                if ($res == 1) {

                    $message['type'] = 'success';
                    $message['message'] = '<div >Class Test Edited Successfully!!</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function viewSectionByClassAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->classId = $this->request->getPost('classId');
        }
    }

    public function loadAvailWeightAction() {

        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $linkname = $this->request->getPost('linkname');
        $enteredId = $this->request->getPost('enteredId');

        $enteredOF = $enteredId ? Mainexam::findFirstById($enteredId)->mainexam_outof : 0;
        $weightage = Mainexam::findFirstByExamName($linkname)->available_outof + $enteredOF;

        echo $weightage;
    }

    public function loadExamByClassoldAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $divVal = $this->request->getPost('divVal');
        $this->view->display_mainexam = 1;
        if ($this->request->getPost('examType') == '2' && $this->request->getPost('exam_type_id') != '') {
            $phql = 'SELECT m.id,m.exam_name,m.mark_record_start_date ,m.mark_record_end_date,m.mainexam_id,
                            m.mainexam_outof,ma.all_class                       
                            FROM Mainexam m
                            Left JOIN MainexamApplicability ma ON ma.mainexam_name = m.exam_name
                            WHERE m.id =' . $this->request->getPost('exam_type_id') .
                    ' GROUP BY m.exam_name';

            $result = $this->modelsManager->executeQuery($phql);
            foreach ($result as $value) {
                $this->view->mainexamVal = $value;
            }
        }
        $whrequery = '';
        if ($divVal == 0) {
            $whrequery = ($this->request->getPost('examType') != '2') ? '' : ((!$this->request->getPost('exam_type_id')) ? '' :
                            (" and mainexam_name !='" . Mainexam::findFirstById($this->request->getPost('exam_type_id'))->exam_name . "'"));
            $mainexam_applicable_to_all_class = MainexamApplicability::find('all_class = 1' . $whrequery);

            $j = 0;
            if (count($mainexam_applicable_to_all_class) > 0) {
                foreach ($mainexam_applicable_to_all_class as $record) {
                    //echo '<div class="alert alert-block alert-danger fade in">'.$record->mainexam_name.'</div>';
                    $mainexams[$record->mainexam_name] = $record->mainexam_name;
                }
                $this->view->mainexams = $mainexams;
            } else {
                $this->view->display_mainexam = 0;
            }
        } else {
            $whrequery = ($this->request->getPost('examType') == '2' && !$this->request->getPost('exam_type_id')) ? "" : " and id!='" . $this->request->getPost('exam_text') . "'";
            $mainexams_div = Mainexam::find('div_val_id = ' . $divVal . $whrequery);
            $j = 0;
            if (count($mainexams_div) > 0) {
                foreach ($mainexams_div as $record) {
                    //echo '<div class="alert alert-block alert-danger fade in">'.$record->mainexam_name.'</div>';
                    $mainexams[$record->exam_name] = $record->exam_name;
                }
                $this->view->mainexams = $mainexams;
            } else {
                $this->view->display_mainexam = 0;
            }
        }
    }

    public function listCombinationAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $param['type'] = 'mainexam_mark';
        $getCombination = ControllerBase::loadCombinations($param);
//        print_r($getCombination);exit;
        $this->view->combinations = $getCombination[1];
        $this->view->type = $getCombination[2];
        $this->view->errorMessage = $getCombination[0];
    }

    public function listClassTestCombinationAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $param['type'] = 'mainexam_mark';
        $getCombination = ControllerBase::loadCombinations($param);
//        print_r($getCombination);exit;
        $this->view->combinations = $getCombination[1];
        $this->view->type = $getCombination[2];
        $this->view->errorMessage = $getCombination[0];
    }

    public function getExamListAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $master_id = $this->request->getPost('masterid');
            $this->view->master_id = $master_id;
            //echo $this->view->master_id;
            //exit;
            $teacherType = $this->request->getPost('type');
            if ($teacherType != 'classTeacher') {
                $assignedclass = GroupSubjectsTeachers::findFirstById($master_id);
                $classmaster = ClassroomMaster::findFirstById($assignedclass->classroom_master_id);
                $res = ControllerBase::buildExamQuery($classmaster->aggregated_nodes_id);
//        print_r($res);exit;
                $this->view->mainexams = Mainexam::find(implode(' or ', $res));
            }
            $this->view->combinations = $classmaster->name; //ControllerBase::getNameForKeys($classmaster->aggregated_nodes_id);
        }
    }

    public function listClassTestCombinationoldAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $param['type'] = 'mainexam_mark';
        $getCombination = ControllerBase::loadCombinations($param);
        $this->view->combinations = $getCombination[1];
        $this->view->type = $getCombination[2];
        $this->view->errorMessage = $getCombination[0];
    }

    public function editExamAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->examTypeActive = '2';
        $phql = 'SELECT m.id,m.exam_name,m.div_val_id,m.mark_record_start_date ,m.mark_record_end_date,m.mainexam_id,
                            m.mainexam_outof,ma.all_class                       
                            FROM Mainexam m
                            Left JOIN MainexamApplicability ma ON ma.mainexam_name = m.exam_name
                            WHERE m.id =' . $this->request->getPost('examId') .
                ' GROUP BY m.exam_name';

        $result = $this->modelsManager->executeQuery($phql);
        foreach ($result as $value) {
            $this->view->form = new AddExamForm($value, array(
                'edit' => $this->view->examTypeActive,
                'examid' => $this->request->getPost('examId')
            ));
            $this->view->mainexamchecked = ($value->mainexam_id) ? 'checked' : '';
            $this->view->alclasschecked = ($value->all_class) ? '' : 'checked';
            $this->view->mainexamVal = $value;
        }
        $this->view->display_mainexam = 1;
        $mainexam_applicable_to_all_class = MainexamApplicability::find('all_class = 1');
        $j = 0;
        if (count($mainexam_applicable_to_all_class) > 0) {
            foreach ($mainexam_applicable_to_all_class as $record) {
                $mainexams[$record->mainexam_name] = $record->mainexam_name;
            }
            $this->view->mainexams = $mainexams;
        } else {
            $this->view->display_mainexam = 0;
        }

        $this->view->division_name = ControllerBase::get_division_name_student();
        $this->view->subdivision_name = ControllerBase::get_sub_division_name_student();
        $this->view->division_values = ControllerBase::get_division_values_student(ControllerBase::get_current_academic_year());
        $this->view->mainexamList = Mainexam::find();
    }

    public function listAdminCombinationAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $assignedCount = GroupSubjectsTeachers::find();
        if (count($assignedCount) > 0) {
            foreach ($assignedCount as $assignedclass) {
                $getCombinations = SubjectsDivision::findFirst('id=' . $assignedclass->subject_id);
                $pattern = '/[-,]/';
                $Totarr = (preg_split($pattern, $assignedclass->aggregated_nodes_id));
                $name_param = array();
                $rcpathname = '';
//                echo '<pre>';
                foreach ($Totarr as $rcvalue) {
                    $res = AssigningController::buildTree($rcpathname, $rcvalue);
                    $inp = array_reverse($res);
                    $searchword = implode('>>', $inp);
//                    echo $searchword.'asdasdasd';
                    if (count($name_param) < 1) {
                        $name_param[] = implode('>>', $inp);
                    }
                    foreach ($name_param as $k => $v) {
//                        echo preg_match("/\b$searchword\b/i", $v);
                        if (preg_match("/\b$v\b/i", $searchword)) {
                            $name_param[$k] = $searchword;
                            break;
                        } else {
                            $name_param[] = implode('>>', $inp);
                            break;
                        }
                    }
                }
                $combinations[$assignedclass->id]['name'] = $name_param;
                $combinations[$assignedclass->id]['subject'] = $getCombinations->subjectName;
            }

            $type = 'subjectTeacher';
        } else {
            $errorMessage = 'No Class assigned for you yet!';
        }

//           print_r($combinations);exit;
//        $getCombination = $combinations;
        $this->view->combinations = $combinations;
        $this->view->type = 'admin';
        $this->view->errorMessage = '';
    }

    public function addExamByClassAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function getExamByCycleAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $this->view->cycleID = $this->request->getPost('cycleID');
            $this->view->subjects = Mainexam::find('node_id LIKE "%-' . $this->view->cycleID
                            . '-%" OR node_id LIKE "' . $this->view->cycleID . '-%" OR node_id LIKE "%-'
                            . $this->view->cycleID . '" ORDER BY node_id');
            $allmains = Mainexam::find();
            $subjects = array();
            foreach ($allmains as $mainex) {
                $nodes = explode('-', $mainex->node_id);
                array_walk($nodes, function (&$item, $key) {
                    $item = OrganizationalStructureValues::findFirstById($item)->name;
                });
                $node_name = implode('>>', $nodes);
                $subjects[$node_name][] = array(
                    'id' => $mainex->id,
                    'node_id' => $mainex->node_id,
                    'exam_name' => $mainex->exam_name,
                    'exam_code' => $mainex->examCode,
                    'mark_record_start_date' => $mainex->mark_record_start_date,
                    'mark_record_end_date' => $mainex->mark_record_end_date,
                    'C_mainexam_id' => Mainexam::findFirstById($mainex->mainexam_id),
                    'mainexam_outof' => $mainex->mainexam_outof
                );
            }
            $this->view->subjects = $subjects;


            $this->view->form = new AddExamForm();
//            $this->view->acd_value = $acd_value->name;
        }
    }

    public function spacetreeAction() {
        $this->tag->prependTitle("Space Tree | ");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $res = $this->_getValueTreeFor(3);
//        echo '<pre>';
//        print_r(json_encode($res));
//        exit;
    }

    public function addExamAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $form = new AddExamForm();
        $identity = $this->auth->getIdentity();
        $res = 1;
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        $message = array();
        // print_r($form->getMessages());
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) == false) {
                foreach ($form->getMessages() as $messages) {
                    $error .= $messages . "<br>";
                }
                //echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            } else {

                $params = array();
                foreach ($this->request->getPost() as $key => $value) {
                    $params[$key] = $value;
                }
                $classID = $params['classID'];
                $subjectName = $params['exname'];
                $subjectCode = $params['excode'];
                $examid = $params['examid'];
                $query[] = 'node_id = ' . $classID . ' and examCode = "' . $subjectCode . '"';
                if (isset($examid)) {
                    $query[] = "id != '$examid'";
                    $subject = Mainexam::findFirstById($examid);
                } else {
                    $subject = new Mainexam();
                }
                $subject->node_id = $params['classID'];
                $subject->exam_name = $params['exname'];
                $subject->examCode = $params['excode'];

                $subject->mark_record_start_date = strtotime($params['mark_record_start']);
                $subject->mark_record_end_date = strtotime($params['mark_record_end']);
                $subject->contribute_outof = $params['contribute_outof'];
                $subject->formula = $params['formula'];
                $subject->created_by = $uid;
                $subject->created_date = time();
                $subject->modified_by = $uid;
                $subject->modified_date = time();
//                print_r($subject);
//                exit;
                $unique = Mainexam::findFirst(implode(' and ', $query));
                if (($unique)) {
                    $message['type'] = 'error';
                    $message['message'] = 'Exam code Exists already';
                    print_r(json_encode($message));
                    exit;
                }
                if ($subject->save()) {


                    // echo '<div class="alert alert-success">Subject Added Successfully</div>';
                    $message['type'] = 'success';
                    $message['message'] = 'Exam saved successfully!';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($subject->getMessages() as $messages) {
                        $error .= $messages . "<br>";
                    }
                    // echo '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function loadExamByClassAction() {
        // Shows only the view related to the action
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $params = array();
        foreach ($this->request->getPost() as $key => $value) {
            $params[$key] = $value;
        }
        $node_id = $params['classID'];
        $examid = $params['examid'];
//        echo "node_id LIKE '$node_id%'"; exit;
        $query[] = "node_id LIKE '$node_id%'";
        if (isset($examid)):
            $query[] = "id != '$examid'";
        endif;
        $mainexams = Mainexam::find(implode(' and ', $query));
        $this->view->mainexams = $mainexams;
        $this->view->mainexamVal = Mainexam::findFirstById($examid);
        $this->view->display_mainexam = count($mainexams) > 0 ? 1 : '';
    }

    public function addExamFormAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->classId = $this->request->getPost('classID');
        $this->view->form = new AddExamForm();
    }

    public function editExamFormAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $examid = $this->request->getPost('examid');
        $this->view->mainexam = Mainexam::findFirstById($examid);

//       $this->view->main_exms = $main_exms = Mainexam::find('mainexam_id =' . $examid);
//       $this->view->class_test = $class_test = ClassTest::find('mainexam_id =' . $examid);
//       $this->view->assignments = $assignments = AssignmentsMaster::find('mainexam_id =' . $examid);

        $this->view->form = new AddExamForm();
    }

    public function loadoverallclasstestsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->viewclasstest = 0;
        if ($this->request->isPost()) {
            $this->view->subject_master_id = $subject_master_id = $this->request->getPost('subject_master_id');
            $subject = GroupSubjectsTeachers::findfirst('id = ' . $subject_master_id);
            $subjects_mas = OrganizationalStructureMaster::find(array(
                        'columns' => 'GROUP_CONCAT(id) as ids',
                        ' module = "Subject" and is_subordinate = 1 '
            ));
            $classmaster = ClassroomMaster::findFirstById($subject->classroom_master_id);
            $this->view->combinations = $classmaster->name;
            $subidsall = HomeWorkController::_getAllChildren($subject->subject_id, $subjects_mas[0]->ids);
            $queryParams[] = 'grp_subject_teacher_id IN(' . $subject_master_id . ')';
            if ($subject->subject_id > 0) {
                $queryParams[] = 'subjct_modules IN (' . implode(',', $subidsall) . ')';
            }
//            echo implode(' and ', $queryParams);exit;
            $classtests = ClassTest::find(implode(' and ', $queryParams));
            if (count($classtests) > 0) {
                foreach ($classtests as $classtest) {

                    $combinations = ControllerBase::getCommonIdsForKeys($classtest->subjct_modules);
                    $inquery = array();
                    foreach ($combinations as $orvalue) {
                        $narr = explode('-', $orvalue);
                        $splicedarr = array_slice($narr, array_search($subject->subject_id, $narr) + 1);
                        $namerepl = count($splicedarr) > 0 ? $splicedarr : (array_slice($narr, array_search($subject->subject_id, $narr)));
                        $inquery[] = implode(' - ', preg_replace_callback('/^([\d-])*/', function($matches) {
                                    return OrganizationalStructureValues::findFirstById($matches[0])->name;
                                }, $namerepl));
                    }
//                     echo '<pre>';print_r(($narr));exit;
                    $rows[] = array("subject" => $inquery,
                        "subject_id" => $subject_master_id,
                        "test_name" => $classtest->class_test_name,
                        "out_of" => $classtest->mainexam_outof,
                        "class_test_id" => $classtest->class_test_id);
                }
                $this->view->viewclasstest = 1;
                $this->view->rows = $rows;
            }
        }
    }

    public function getMainExamBySubjectsAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $this->view->display = 0;
        if ($this->request->isPost()) {
            $this->view->subject_masterid = $subject_masterid = $this->request->getPost('subject_masterid');
            $this->view->subject = $subject = GroupSubjectsTeachers::findfirst('id = ' . $subject_masterid);
            $records = Mainexam::find("LOCATE(  node_id ,  '$subject->aggregated_nodes_id' ) ");

            if (count($records) > 0) {
                $this->view->display = 1;
                foreach ($records as $record) {
                    $mainexams[$record->id] = $record->exam_name;
                }
            }

            $this->view->mainexams = $mainexams;
        }
    }

    public function addTestByClassLinkedMainExamAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        if ($this->request->isPost()) {
            $params = array();
            foreach ($this->request->getPost() as $key => $value) {
//                $params[$key] = $value;

                $IsSubdiv = explode('_', $key);
                if ($IsSubdiv[0] == 'aggregate' && $value != '') {
                    $params['aggregateids'][] = $value;
                } else {

                    $params[$key] = $value;
                }
            }
            $subject_masterid = $params['subject_masterid'];
            $test_name = $params['test_name'];
            $aggregate_keys = $params['aggregateids'][count($params['aggregateids']) - 1]; //isset($params['aggregateids']) ? implode('-', $params['aggregateids']) : '';
            //Get subject id from subject_masterid
            $subject = GroupSubjectsTeachers::findfirst('id = ' . $subject_masterid);
            $unique = ClassTest::findfirst('grp_subject_teacher_id =' . $subject->id .
                            ' and class_test_name = "' . $test_name . '"');
            if ($unique) {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Class Test Name Exists already!Try a different name</div>';
                print_r(json_encode($message));
                exit;
            } else {

                $classtest = new ClassTest();
                $record = array(
                    'grp_subject_teacher_id' => $subject->id,
                    'class_test_name' => $test_name,
                    'subjct_modules' => $aggregate_keys,
                    'mainexam_id' => '',
                    'mainexam_outof' => '',
                    'created_by' => $uid,
                    'created_date' => time()
                );
                $classtest->assign($record);
                if ($classtest->save()) {
                    $message['type'] = 'success';
                    $message['message'] = '<div>Class Test Added Successfully!!</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($classtest->getMessages() as $messages) {
                        $error .= $messages;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function editClassTestAction() {

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $test_id = $this->request->getPost('test_id');
            $test = ClassTest::findfirst('class_test_id = ' . $test_id);
            $subject = GroupSubjectsTeachers::findfirst('id = ' . $test->grp_subject_teacher_id);
            $records = Mainexam::find("LOCATE(  node_id ,  '$subject->aggregated_nodes_id' ) ");

            if (count($records) > 0) {
                $this->view->display = 1;
                foreach ($records as $record) {
                    $mainexams[$record->id] = $record->exam_name;
                }
            }

            $this->view->mainexams = $mainexams;
            $this->view->test = $test;
        }
    }

    public function editTestByClassLinkedMainExamAction() {

        // No rendering
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $identity = $this->auth->getIdentity();
        $uid = StaffInfo::findFirstByLoginid($identity['name'])->id;
        if ($this->request->isPost()) {
            $params = array();
            foreach ($this->request->getPost() as $key => $value) {
                $params[$key] = $value;
            }
            $subject_masterid = $params['subject_masterid'];
            $test_name = strtoupper($params['test_name']);
            $test_id = strtoupper($params['test_id']);
            $subject = GroupSubjectsTeachers::findfirst('id = ' . $subject_masterid);
            $unique = ClassTest::findfirst('grp_subject_teacher_id =' . $subject->id . ' and class_test_id != ' . $test_id .
                            ' and class_test_name = "' . $test_name . '"');
            if ($unique) {
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">Class Test Name Exists already!Try a different name</div>';
                print_r(json_encode($message));
                exit;
            } else {

                $classtest = ClassTest::findfirst('class_test_id = ' . $test_id);
                $record = array(
                    'grp_subject_teacher_id' => $subject_masterid,
                    'class_test_name' => $test_name,
                    'mainexam_id' => '',
                    'mainexam_outof' => '',
                    'modified_by' => $uid,
                    'modified_date' => time()
                );
                $classtest->assign($record);
                if ($classtest->save()) {
                    $message['type'] = 'success';
                    $message['message'] = '<div>Class Test Added Successfully!!</div>';
                    print_r(json_encode($message));
                    exit;
                } else {
                    $error = '';
                    foreach ($classtest->getMessages() as $message) {
                        $error .= $message;
                    }
                    $message['type'] = 'error';
                    $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                    print_r(json_encode($message));
                    exit;
                }
            }
        }
    }

    public function checkLinkageForTestAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        if ($this->request->isPost()) {
            $examId = $this->request->getPost('testId');
            $test = ClassTest::findFirstByClassTestId($examId);
            if ($test->mainexam_id > 0) {
                $message['type'] = 'warning';
                $message['message'] = " Deleting this test will lead to following action \n"
                        . " 1. Linkage to exam will be deleted\n."
                        . " 2. Marks added to students for this test will be deleted";
                print_r(json_encode($message));
                exit;
            } else {
                $mainexMark = ClassTestMarks::findFirstByClassTestId($examId);
                if ($mainexMark) {
                    $message['type'] = 'warning';
                    $message['message'] = "Marks added to students for this test will also be delted. "
                            . "\n Do you really wanna delete this exam?";
                    print_r(json_encode($message));
                    exit;
                }
            }

            $message['type'] = 'success';
            $message['message'] = "No action performed yet and can be delete!";
            print_r(json_encode($message));
            exit;
        }
    }

    public function removeClassTestAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $this->view->viewclasstest = 0;

        if ($this->request->isPost()) {
            $test_id = $this->request->getPost('test_id');
            $test = ClassTest::findfirst('class_test_id = ' . $test_id);
            $linkedid = $test->mainexam_id;
            $res = 1;
            $testMarks = ClassTestMarks::find('class_test_id = ' . $test_id);
            if (!$testMarks->delete()) {
                $error = '';
                foreach ($testMarks->getMessages() as $message) {
                    $error .= $message;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            }
            if ($linkedid > 0) {
                $subjects = GroupSubjectsTeachers::findFirstById($test->grp_subject_teacher_id);
                $mainexRec = MainexamMarks::find('mainexam_id = ' . $test->mainexam_id .
                                ' and subject_id = ' . $subjects->subject_id);
                $student_id_arr = array();
                foreach ($mainexRec as $mainEx) {
                    $student_id_arr[] = $mainEx->student_id;
                }
                $stuarr = array_unique($student_id_arr);
                //getallstudents under this exam
                foreach ($stuarr as $student_id) {

                    $res = 0;
                    $recursiveParam = array(
                        'stuId' => $student_id,
                        'x2' => $test->mainexam_id,
                        'obtainedMarks' => '',
                        'obtainedOutOf' => '',
                        'subject_id' => $subjects->subject_id,
                        'clastst_combi_id' => $subjects->id
                    );
//                        print_r($recursiveParam);
//                        exit;
                    $res = MarkController::_calculateMarksToLinkedExamsNEW($recursiveParam);
                }

//                }
            }
            if (!$test->delete()) {

                $error = '';
                foreach ($test->getMessages() as $msg) {
                    $error .= $msg;
                }
                $message['type'] = 'error';
                $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
                print_r(json_encode($message));
                exit;
            } else if ($res == 1) {
                $message['type'] = 'success';
                $message['message'] = "Test Deleted Successfully!";
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function checkLinkageAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        if ($this->request->isPost()) {
            $examId = $this->request->getPost('examId');
            $mainexam = Mainexam::findFirstById($examId);
            if ($mainexam->mainexam_id > 0) {
                $message['type'] = 'warning';
                $message['message'] = " Deleting this exam will lead to following action \n"
                        . " 1. Linkage to another exam will be deleted\n."
                        . " 2. Marks added to students for this exam will be deleted";
                print_r(json_encode($message));
                exit;
            } else {
                $mainexamLink = Mainexam::findFirstByMainexamId($examId);
                $classtestLink = ClassTest::findFirstByMainexamId($examId);
                $AssLink = AssignmentsMaster::findFirstByMainexamId($examId);
                if ($mainexamLink || $classtestLink || $AssLink) {
                    $message['type'] = 'warning';
                    $message['message'] = " Deleting this exam will lead to following action \n"
                            . " 1. Contributing marks Linkage to another exam will be deleted\n."
                            . " 2. Marks added to students for this exam will be deleted";
                    print_r(json_encode($message));
                    exit;
                } else {
                    $mainexMark = MainexamMarks::findFirstByMainexamId($examId);
                    if ($mainexMark) {
                        $message['type'] = 'warning';
                        $message['message'] = "Marks added to students for this exam will also be delted. \n Do you really wanna delete this exam?";
                        print_r(json_encode($message));
                        exit;
                    }
                }
            }

            $message['type'] = 'success';
            $message['message'] = "No action performed yet and can be delete!";
            print_r(json_encode($message));
            exit;
        }
    }

    public function confirmDeleteExamAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $message = array();
        if ($this->request->isPost()) {
            $examId = $this->request->getPost('examId');
            $mainexam = Mainexam::findFirstById($examId);
            $res = 1;
            $mainexMark = MainexamMarks::find('mainexam_id = ' . $examId);
            if (!$mainexMark->delete()) {
                $error = '';
                foreach ($mainexMark->getMessages() as $messages) {
                    $error .= $messages . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }


            if (!$mainexam->delete()) {
                $error = '';
                foreach ($mainexam->getMessages() as $messages) {
                    $error .= $messages . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
            if ($res == 1) {
                $message['type'] = 'success';
                $message['message'] = "Exam Deleted Successfully!";
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function loadFormulaFormAction() {
        $this->view->setRenderLevel(VIEW::LEVEL_ACTION_VIEW);

        if ($this->request->isPost()) {

            $this->view->examid = $examid = $this->request->getPost('examid');
            $this->view->subjectid = $subjectid = $this->request->getPost('subjectid');
            $this->view->master_id = $master_id = $this->request->getPost('masterid');

            $condition = "subject_id = $subjectid AND mainexm_id = $examid" .
                    ' and grp_subject_teacher_id = ' . $master_id;

            $this->view->formula_tabl = $formula_tabl = (FormulaTable::findFirst(array(
                        $condition,
                        'columns' => ' COUNT(*) as cnt'
                    ))->cnt > 0) ?
                    FormulaTable::findFirst($condition) : '';
            $this->view->expld_val = $expld_val = ($formula_tabl != '') ? explode(',', $formula_tabl->exam_ids) : '';

            $mainexam_det = Mainexam::findFirstById($examid);
            $expldval = $mainexam_det->node_id ? explode('-', $mainexam_det->node_id) : '';
            $currentQury = preg_filter('/^([\d])*/', '(FIND_IN_SET("$0" , REPLACE(node_id, "-", "," ) ) >0)', $expldval);
            $currentQury[] = 'id != ' . $examid;
            $this->view->main_exms = $main_exms = Mainexam::find(implode(' AND ', $currentQury));

            $testcondn = 'subjct_modules = ' . $subjectid . ' and grp_subject_teacher_id = ' . $master_id;
            $this->view->class_test = $class_test = ClassTest::find($testcondn);
            $this->view->assignments = $assignments = AssignmentsMaster::find($testcondn);
//print_r(count($assignments));exit;
        }
    }

    public function addFormulaAction() {
        $this->view->setRenderLevel(VIEW::LEVEL_NO_RENDER);
        $message = array();

        if ($this->request->isPost()) {

            $outof = $this->request->getPost('contribute_outof');
            $formula = $this->request->getPost('formula');
            $examid = $this->request->getPost('exam_id');
            $subjectid = $this->request->getPost('subject_id');
            $exam_ids = $this->request->getPost('exam_ids');
            $master_id = $this->request->getPost('masterid');

            $condition = "subject_id = $subjectid AND mainexm_id = $examid" .
                    ' and grp_subject_teacher_id = ' . $master_id;

            $formula_tabl = (FormulaTable::findFirst(array(
                        $condition,
                        'columns' => ' COUNT(*) as cnt'
                    ))->cnt > 0) ?
                    FormulaTable::findFirst($condition) :
                    new FormulaTable();
//echo 'test';
//exit;
            $formula_tabl->outof = $outof;
            $formula_tabl->formula = $formula;
            $formula_tabl->subject_id = $subjectid;
            $formula_tabl->mainexm_id = $examid;
            $formula_tabl->exam_ids = $exam_ids;
            $formula_tabl->grp_subject_teacher_id = $master_id;

//            print_r($formula_tabl);
//            exit;
            if (!$formula_tabl->save()) {
                $error = '';
                foreach ($formula_tabl->getMessages() as $messages) {
                    $error .= $messages . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            } else {

//                $condition = "subject_id = $combiId AND mainexm_id = $examTwoId";
                $formula = FormulaTable::findFirst($condition);
                $exam_ids = $formula->exam_ids ? explode(',', $formula->exam_ids) : '';
                $student_id_arr = array();

                foreach ($exam_ids as $exam_id) {
                    $expldval = explode('_', $exam_id);
                    if ($expldval[0] == 'M') {
                        $refmainmarks = MainexamMarks::find('mainexam_id = ' . $expldval[1] .
                                        ' and subject_id = ' . $subjectid .
                                        ' and grp_subject_teacher_id = ' . $master_id) ?
                                MainexamMarks::find('mainexam_id = ' . $expldval[1] .
                                        ' and subject_id = ' . $subjectid .
                                        ' and grp_subject_teacher_id = ' . $master_id) :
                                new MainexamMarks();

                        if ($refmainmarks) {
                            foreach ($refmainmarks as $refmainmark) {
                                $student_id_arr[] = $refmainmark->student_id;
                            }
                        }
                    }
                    if ($expldval[0] == 'C') {
                        $reftest = ClassTestMarks::find('class_test_id = ' . $expldval[1]);
//                           echo 'in';      
                        if ($reftest) {
                            foreach ($reftest as $reftestmark) {
                                $student_id_arr[] = $reftestmark->student_id;
                            }
                        }
                    }
                    if ($expldval[0] == 'A') {
                        $refassign = AssignmentMarks::find('assignment_id =' . $expldval[1]);
                        if ($refassign) {
                            foreach ($refassign as $refassignmark) {
                                $student_id_arr[] = $refassignmark->student_id;
                            }
                        }
                    }
                }

//             print_r($student_id_arr);
//            $clsTstRec = ClassTest::find('mainexam_id = ' . $mainExId);
//                foreach ($mainexRec as $mainEx) {
//                    $student_id_arr[] = $mainEx->student_id;
//                }
                $stuarr = array_unique($student_id_arr);
//                 print_r($stuarr);
//                exit;
                $res = 1;
                foreach ($stuarr as $student_id) {
                    $recursiveParam = array(
                        'stuId' => $student_id,
                        'x2' => $examid,
                        'obtainedMarks' => '',
                        'obtainedOutOf' => '',
                        'subject_id' => $subjectid,
                        'clastst_combi_id' => $master_id
                    );

//                    print_r($recursiveParam);exit;

                    $res = MarkController::_calculateMarksToLinkedExamsNEW($recursiveParam);
                }

                if ($res != 1) {
                    foreach ($res->getMessages() as $message) {
                        $error .= $message;
                    }

                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                }
                if ($res == 1) {
                    $message['type'] = 'success';
                    $message['message'] = 'Successfully updated';
                    print_r(json_encode($message));
                }
//                $message['type'] = 'success';
//                $message['message'] = 'Formula added successfully.';
//                print_r(json_encode($message));
//                exit;
            }
        }
    }

    public function resetFormulaAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $mainExId = $this->request->getPost('exam_id');
            $masterid = $this->request->getPost('masterid');
            $subsujectid = $this->request->getPost('subject_id');

            $condition = "subject_id = $subsujectid AND mainexm_id = $mainExId" .
                    ' and grp_subject_teacher_id = ' . $masterid;
            $formula_tabl = FormulaTable::findFirst($condition);
            if ($formula_tabl && !$formula_tabl->delete()) {
                foreach ($formula_tabl->getMessages() as $message) {
                    $error .= $message;
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
            $mainexRec = MainexamMarks::find('mainexam_id = ' . $mainExId .
                            ' and subject_id = ' . $subsujectid .
                            ' and grp_subject_teacher_id = ' . $masterid);
            $student_id_arr = array();
            foreach ($mainexRec as $mainEx) {
                $student_id_arr[] = $mainEx->student_id;
            }

            $stuarr = array_unique($student_id_arr);
            //getallstudents under this exam
            $res = 1;
            foreach ($stuarr as $student_id) {
                $recursiveParam = array(
                    'stuId' => $student_id,
                    'x2' => $mainExId,
                    'obtainedMarks' => '',
                    'obtainedOutOf' => '',
                    'subject_id' => $subsujectid,
                    'clastst_combi_id' => $masterid
                );
                $res = MarkController::_calculateMarksToLinkedExamsNEW($recursiveParam);
            }
            if ($res == 1) {
                $message['type'] = 'success';
                $message['message'] = 'Successfully updated';
                print_r(json_encode($message));
            } else {
                foreach ($res->getMessages() as $message) {
                    $error .= $message;
                }

                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            }
        }
    }

}
