<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class OrganizationalStructureController extends ControllerBase {

    protected $bgclass = array(
        array('color' => "#AEC785", 'highlight' => "#97b06e"),
        array('color' => "#FDD752", 'highlight' => "#ecc641"),
        array('color' => "#3ad0c8", 'highlight' => "#1fb5ad"),
        array('color' => "#59ace2", 'highlight' => "#428bca"),
        array('color' => "#ff6c60", 'highlight' => "#f15e52"),
        array('color' => "#9589db", 'highlight' => "#8175c7"),
        array('color' => "#a9d86e", 'highlight' => "#9ecd63"),
        array('color' => "#fcb322", 'highlight' => "#f3aa19"),
        array('color' => "#cbcbcb", 'highlight' => "#BA2424"),
        array('color' => "#F2DEDE", 'highlight' => "#FCB1AE"));

    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
        $this->view->setTemplateAfter('private');
    }

    /* Organizational Structure master tree - Settings screen
     * One root node (like business, ..) need to be added in database
     * jquery tree plugin is used - customized for our use
     * CURD functionalities has been included
     * CURD callbacks are written in orgmaster.js
     * jquery-1.8.1.min.js plugin is required
     *  */

    public function indexAction() {
        $this->tag->prependTitle("Organizational structure | ");
//        $this->assets
//                ->collection('treeJs')
//                ->addCss('css/treecss/style.css')
//                ->addJs('js/jquery-1.8.1.min.js')
//                ->addJs('js/orgmaster/orgmaster.js')
//                ->addJs('js/jquery-ui.js')
//                ->addJs('js/jquery.tree.js');
//        $rootid = OrganizationalStructureMaster::findFirstByParentId(0)->id;
//        $reslt = $this->find_childtree($rootid, '');
//        $output.=$reslt;
//        $this->view->mastertree = $output;
//
//        foreach ($this->request->get() as $key => $vl) {
//            $params[$key] = $vl;
//        }
//        $view = isset($params['view']) ? $params['view'] : '';
//        $this->view->back = $back = 1;
//        if (empty($view)) {
//            $this->view->back = $back = 0;
//        }
    }

    /* Organizational Structure master tree - Recursive callback
     * $aid - id of OrganizationalStructureMaster
     * $choutput - returns the tree nodes
     * Tree structure built using this function
     *  */

    public function find_childtree($aid, $choutput) {
        $exist = OrganizationalStructureMaster::find('parent_id =' . $aid);
//        print_r(count($exist));exit;
        $attribute = $choutput ? '' : 'class="tree"';
        $nodeval = OrganizationalStructureMaster::findFirstById($aid);
        $choutput.=$choutput ? '' : '<ul ' . $attribute . ' > ';
        if (count($exist) > 0) {
            $choutput.='<li> <div id="' . $aid . '" cycleval = "'
                    . $nodeval->cycle_node . '" manadmission = "'
                    . $nodeval->mandatory_for_admission . '">'
                    . $nodeval->name .
                    '</div> <ul>';
            foreach ($exist as $chl) {
                $choutput = OrganizationalStructureController::find_childtree($chl->id, $choutput);
            }
            $choutput.='</li></ul>';
        } else {
            $choutput.='<li> <div id="' . $aid . '" cycleval = "'
                    . $nodeval->cycle_node . '" manadmission = "'
                    . $nodeval->mandatory_for_admission . '">' . $nodeval->name . '</div> </li>';
        }


        return $choutput;
    }

    /* Organizational Structure master tree - CURD
     * action - add, edit, drag, delete
     * cyclenode - Is node is cycle node? (form input)
     * manforadmission - Is mandatory for admission? (form input)
     * parentid - ID od parent node (form input)
     * id - Id of current node (form input)
     * html - Name of current node (form input)
     * Success and error messages are handeld accordingly
     *  */

    public function updatetreeAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
//      print_r($this->request->getPost());exit;
        try {
            if ($this->request->isPost()) {
                $action = $this->request->getPost('action');
                $cycle_node = $this->request->getPost('cyclenode') == 'false' ? '2' : '1';
                $mandatory_for_admission = $this->request->getPost('manforadmission') == 'false' ? '2' : '1';
                $editid = ($action == "add") ? '' : $this->request->getPost('id');

                if ($action == "drag") {
                    $dragid = $this->request->getPost('id');
                    $parentid = $this->request->getPost('parentid');
                    $newnode = (OrganizationalStructureMaster::findFirstById($editid) ? OrganizationalStructureMaster::findFirstById($editid) :
                                    new OrganizationalStructureMaster());
                    $newnode->parent_id = $parentid;
                    if ($newnode->save()) {
                        $messages['type'] = 'success';
                        $messages['message'] = 'Saved successfully';
                        print_r(json_encode($messages));
                        exit;
                    } else {
                        $error = '';
                        foreach ($newnode->getMessages() as $message) {
                            $error .= $message;
                        }
                        $messages['type'] = 'error';
                        $messages['message'] = $error;
                        print_r(json_encode($messages));
                        exit;
                    }
                } else if ($action == "add" || $action == "edit") {

                    // Only one node can be cyclic 
                    $cyclenodeexists = OrganizationalStructureMaster::findFirstByCycleNode('1');
                    if ($cyclenodeexists && $cycle_node == 1) {
                        $messages['type'] = 'error';
                        $messages['message'] = 'Cycle Node already exists';
                        print_r(json_encode($messages));
                        exit;
                    }
                    $newnode = ($action == "add") ? new OrganizationalStructureMaster() :
                            (OrganizationalStructureMaster::findFirstById($editid) ? OrganizationalStructureMaster::findFirstById($editid) :
                                    new OrganizationalStructureMaster());
                    $newnode->name = $this->request->getPost('html');
                    $newnode->parent_id = ($action == "add") ? $this->request->getPost('parentid') : $newnode->parent_id;
                    $newnode->cycle_node = $cycle_node;
                    $newnode->mandatory_for_admission = $mandatory_for_admission;

                    if ($newnode->save()) {
                        $messages['type'] = 'success';
                        $messages['message'] = 'Saved successfully';
                        print_r(json_encode($messages));
                        exit;
                    } else {
                        $error = '';
                        foreach ($newnode->getMessages() as $message) {
                            $error .= $message;
                        }
                        $messages['type'] = 'error';
                        $messages['message'] = $error;
                        print_r(json_encode($messages));
                        exit;
                    }
                } else if ($action == "delete") {
                    $deleteids = explode(',', $this->request->getPost('id'));
                    // Recurrsive delete is handled
                    foreach ($deleteids as $deleteid) {
                        $newnode = OrganizationalStructureMaster::findFirstById($deleteid) ?
                                OrganizationalStructureMaster::findFirstById($deleteid) :
                                new OrganizationalStructureMaster();

                        if (!$newnode->delete()) {
                            $error = '';
                            foreach ($newnode->getMessages() as $message) {
                                $error .= $message;
                            }
                            $messages['type'] = 'error';
                            $messages['message'] = $error;
                            print_r(json_encode($messages));
                            exit;
                        }
                    }
                }
            }
        } catch (Exception $e) {

            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message;
            }
            $message['type'] = 'error';
            $message['message'] = '<div class="alert alert-block alert-danger fade in">' . $error . '</div>';
            print_r(json_encode($message));
            exit;
        }
    }

    /* Organizational Structure Value - CURD
     *  View callback - openValuesAction is used
     */

    public function addValuesAction() {
        $this->tag->prependTitle("Organizational structure | ");
        $this->assets->addJs('js/orgmaster/orgmaster.js');

        foreach ($this->request->get() as $key => $vl) {
            $params[$key] = $vl;
        }
        $view = isset($params['view']) ? $params['view'] : '';
        $this->view->back = $back = 1;
        if (empty($view)) {
            $this->view->back = $back = 0;
        }
    }

    /* Organizational Structure Value - View callback
     * Based on Id and Organizational master id ,
     * returns the view of its child node &  values
     * CURD functionalities has been handled
     * Delete option not yet implemented
     */

    public function openValuesAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $org_value = '';
        $org_master = 0;
        if ($this->request->isPost()) {
            $this->view->prev_org_value = $org_value = $this->request->getPost('org_value');
            $this->view->prev_org_master = $org_master = $this->request->getPost('org_master');
        }
        $this->view->root = $root = OrganizationalStructureMaster::find('parent_id =' . $org_master);
        $this->view->nodevalue = $node = $org_value ? OrganizationalStructureValues::findFirst('id = ' . $org_value . ' and org_master_id = ' . $org_master) : '';
    }

    /* Organizational Structure Value - CURD callback
     * actions = add, edit
     * parent_id - Id of the Parent node (like business 'Trust' is the parent of school 'Qmis')
     * org_value - Organizational structure master Id (like for 'Qmis' Org master 'school')
     * newnodevalue - name of the Org value
     * status - Current/ Next (applicable only for Cycle node)
     */

    public function updateOrgValuesAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
//        print_r($this->request->getPost());
//        exit;
        try {
            if ($this->request->isPost()) {
                $transactionManager = new TransactionManager();
                $transactionService = $transactionManager->setDbService('db');
                $transaction = $transactionService->get();
                $actions = $this->request->getPost('actions');
                $parent_id = $this->request->getPost('parent_id');
                $org_value = $this->request->getPost('org_value');
                $newnodevalue = $this->request->getPost('newnodevalue');
                $rangefrom = strtotime($this->request->getPost('range_from'));
                $rangeto = strtotime($this->request->getPost('range_to'));
                $selpar = OrganizationalStructureValues::findFirstById($parent_id);
                $optionsforchild = $selpar->status;
                $parentOrgCycle = OrganizationalStructureMaster::findFirstById($org_value)->cycle_node;
                $parentNode = OrganizationalStructureMaster::findFirstById($org_value);
                $gvnsta = $this->request->getPost('status');
                $options = $gvnsta ? $gvnsta : ((!$optionsforchild || $parentOrgCycle == '1') ?
                                ($this->request->getPost('options') == 'current' ? 'C' : 'N') : $optionsforchild);
                $orgmasFeeCheck = OrganizationalStructureMaster::findFirstById($org_value);
//echo $optionsforchild.':'.$parentOrgCycle.':'.$options;exit;
                if ($actions == 'add') {

                    if ($options == 'C' && $parentOrgCycle == '1') {
                        $checkForsameMasterNodeStatus = OrganizationalStructureValues::findFirst(
                                        " org_master_id = $org_value "
                                        . " and parent_id = $parent_id"
                                        . " and status ='C'"
                                        . " and name != '$newnodevalue' ");
                        if ($checkForsameMasterNodeStatus) {
                            $messages['type'] = 'error';
                            $messages['message'] = "Only one cycle node can set to current status";
                            print_r(json_encode($messages));
                            exit;
                        }
                    }

                    $orgValues = OrganizationalStructureValues::findFirst(
                                    "name= '$newnodevalue' "
                                    . " and parent_id = $parent_id"
                                    . " and org_master_id = $org_value ");
                    if ($orgValues) {
                        $messages['type'] = 'error';
                        $messages['message'] = 'Node name already exist';
                        print_r(json_encode($messages));
                        exit;
                    } else {
                        $orgValues = new OrganizationalStructureValues();

                        $orgMasValues = OrganizationalStructureMaster::findFirstById($org_value);
                        if ($orgMasValues->cycle_node == '1') {
                            if (!$rangefrom || !$rangeto) {
                                $messages['type'] = 'error';
                                $messages['message'] = 'Date is required';
                                print_r(json_encode($messages));
                                exit;
                            }
                            $othercyclenodes = OrganizationalStructureValues::findFirst(
                                            "name != '$newnodevalue' "
                                            . " and org_master_id = $org_value"
                                            . " and parent_id = $parent_id"
                                            . " and (((rangefrom between '$rangefrom' and '$rangeto') "
                                            . "         OR (rangeto between '$rangefrom' and '$rangeto' ))"
                                            . "     OR (('$rangefrom' between rangefrom and rangeto)"
                                            . "         OR ('$rangeto' between rangefrom and rangeto )))");
                            if ($othercyclenodes) {
                                $messages['type'] = 'error';
                                $messages['message'] = 'Date range already assigned to another node';
                                print_r(json_encode($messages));
                                exit;
                            }
                            $orgValues->rangefrom = ($rangefrom);
                            $orgValues->rangeto = ($rangeto);
                        }
                        $orgValues->org_master_id = $org_value;
                        $orgValues->parent_id = $parent_id;
                        $orgValues->name = $newnodevalue;
                        $orgValues->status = $options;

                        $orgValues->orgname = $parentNode->name;
                        $orgValues->nodecolor = $parentNode->color ? $parentNode->color : ( '#a9d86e');
                        $orgValues->nodefont = $parentNode->nodefont ? $parentNode->nodefont : ('bank');

                        $orgValues->setTransaction($transaction);
//  print_r($orgValues);exit;
                        if ($orgmasFeeCheck->module == 'Ledger' && $orgmasFeeCheck->cycle_node != 1) {
                            $ledgernames = OrganizationalStructureController::_getParentNamesLedger($parent_id);
                            array_pop($ledgernames);
                            if (count($ledgernames) > 0) {
                                $inp = array_reverse($ledgernames);
                                $feeorgids = ControllerBase::getFeeOrgMasIds();
                                $parfeeroot = ControllerBase::getFeeRootNode($parent_id)->parent_id;
                                $feelednode = 0;
//                                print_r($ledgernames); exit;

                                foreach ($inp as $inpvalue) {
                                    $lidp = OrganizationalStructureValues::findFirst("name LIKE '" . strtoupper($inpvalue) . "'"
                                                    . " and org_master_id IN (" . implode(',', $feeorgids) . ")"
                                                    . " and parent_id = $parfeeroot");
                                    if ($lidp) {
                                        $parentldid = $parfeeroot->id;
                                        $feelednode = 1;
                                    }
                                }
                            }
                            if ($feelednode == 1) {

                                $messages['type'] = 'error';
                                $messages['message'] = "Parent ledger is linked with fee node and can't be created.";
                                print_r(json_encode($messages));
                                exit;
                            }
                        }
//                        print_r($actualstructural);exit;
                        if ($orgValues->save()) {//(1 == 1) {//
                            if ($orgmasFeeCheck->module == 'Fee') {
                                $recname = strtoupper($newnodevalue);
                                $resfeeled = ControllerBase::FeeAndLedgerTree($parent_id);
//                                 print_r($resfeeled);exit;
                                $parentldid = $resfeeled[1];
                                $orgids = $resfeeled[3];
                                $param['ledgername'] = $recname;
                                $param['lid'] = $parentldid;
                                $lid = LedgerController::saveNewLedger($param);
                                if ($lid > 0) {
                                    $transaction->commit();
                                    $messages['type'] = 'success';
                                    $messages['message'] = 'Fee node and its corresponding ledgers are created!';
                                    print_r(json_encode($messages));
                                    exit;
                                }
                            } else {
                                $transaction->commit();
                                $messages['type'] = 'success';
                                $messages['message'] = 'Saved successfully';
                                print_r(json_encode($messages));
                                exit;
                            }
                        } else {
                            $error = '';
                            $transaction->rollback("Problem in adding!");
                            foreach ($orgValues->getMessages() as $message) {
                                $error .= $message . '<br>';
                            }
                            $messages['type'] = 'error';
                            $messages['message'] = $error;
                            print_r(json_encode($messages));
                            exit;
                        }
                    }
                }
                if ($actions == 'edit') {

                    $nodeid = $this->request->getPost('nodeid');
                    $orgValues = OrganizationalStructureValues::findFirstById($nodeid);
                    $orgMasValues = OrganizationalStructureMaster::findFirstById($orgValues->org_master_id);
                    $nodevalue = $this->request->getPost('nodevalue');
                    if ($options == 'C' && $parentOrgCycle == '1') {
                        $checkForsameMasterNodeStatus = OrganizationalStructureValues::findFirst(
                                        " org_master_id = $org_value "
                                        . " and parent_id = $parent_id"
                                        . " and status ='C'"
                                        . " and id != $orgValues->id ");
                        if ($checkForsameMasterNodeStatus) {
                            $messages['type'] = 'error';
                            $messages['message'] = "Only one cycle node can set to current status";
                            print_r(json_encode($messages));
                            exit;
                        }
                    }
                    if ($orgMasValues->cycle_node == '1') {
                        if (!$rangefrom || !$rangeto) {
                            $messages['type'] = 'error';
                            $messages['message'] = 'Date is required';
                            print_r(json_encode($messages));
                            exit;
                        }
                        $othercyclenodes = OrganizationalStructureValues::findFirst("id != $orgValues->id "
                                        . " and org_master_id = $org_value"
                                        . " and parent_id = $parent_id"
                                        . " and (((rangefrom between '$rangefrom' and '$rangeto') "
                                        . "         OR (rangeto between '$rangefrom' and '$rangeto' ))"
                                        . "     OR (('$rangefrom' between rangefrom and rangeto)"
                                        . "         OR ('$rangeto' between rangefrom and rangeto )))");

                        if ($othercyclenodes) {
                            $messages['type'] = 'error';
                            $messages['message'] = 'Date range already assigned to another node';
                            print_r(json_encode($messages));
                            exit;
                        }
                        $orgValues->rangefrom = ($rangefrom);
                        $orgValues->rangeto = ($rangeto);
                    }
                    $orgnameexs = OrganizationalStructureValues::findFirst(
                                    " name= '$nodevalue' "
                                    . " and parent_id = $parent_id"
                                    . " and org_master_id = $org_value "
                                    . " and id != $orgValues->id ");
                    if ($orgnameexs) {
                        $messages['type'] = 'error';
                        $messages['message'] = 'Node name already exist';
                        print_r(json_encode($messages));
                        exit;
                    }
                    $existingpar = $orgValues->parent_id;
                    $orgValues->name = $nodevalue;
                    $orgValues->status = $options;
                    $orgValues->parent_id = $parent_id;
                    $orgValues->org_master_id = $org_value;
                    $orgValues->orgname = $parentNode->name;
                    $orgValues->nodecolor = $parentNode->color ? $parentNode->color : ( '#a9d86e');
                    $orgValues->nodefont = $parentNode->nodefont ? $parentNode->nodefont : ('bank');
                    $orgValues->setTransaction($transaction);
//                    print_r($orgValues);  exit;
                    if (!$orgValues) {
                        $messages['type'] = 'error';
                        $messages['message'] = 'Invalid node';
                        print_r(json_encode($messages));
                        exit;
                    } else {
                        $editnmode = 1; // cannot be edited


                        $orgmasForParent = OrganizationalStructureMaster::findFirstById($selpar->org_master_id);
                        if ($orgmasForParent->module == 'Ledger' && $orgmasForParent->cycle_node != 1) {
                            $ledgernames = OrganizationalStructureController::_getParentNamesLedger($parent_id);
                            array_pop($ledgernames);
                            if (count($ledgernames) > 0) {
                                $inp = array_reverse($ledgernames);
                                $feeorgids = ControllerBase::getFeeOrgMasIds();
                                $parfeeroot = ControllerBase::getFeeRootNode($parent_id)->parent_id;
                                $feelednode = 0;
//                                print_r($ledgernames); exit;

                                foreach ($inp as $inpvalue) {
                                    $lidp = OrganizationalStructureValues::findFirst("name LIKE '" . strtoupper($inpvalue) . "'"
                                                    . " and org_master_id IN (" . implode(',', $feeorgids) . ")"
                                                    . " and parent_id = $parfeeroot");
                                    if ($lidp) {
                                        $parentldid = $parfeeroot->id;
                                        $feelednode = 1;
                                    }
                                }
                            }
                            if ($feelednode == 1) {

                                $messages['type'] = 'error';
                                $messages['message'] = "Parent ledger is linked with fee node and can't be moved.";
                                print_r(json_encode($messages));
                                exit;
                            }
                        }
                        if ($orgmasFeeCheck->module == 'Ledger' && $orgmasFeeCheck->cycle_node != 1) {
                            $ledgernames = OrganizationalStructureController::_getParentNamesLedger($orgValues->id);
                            array_pop($ledgernames);
                            if (count($ledgernames) > 0) {
                                $inp = array_reverse($ledgernames);
                                $feeorgids = ControllerBase::getFeeOrgMasIds();
                                $parfeeroot = ControllerBase::getFeeRootNode($orgValues->id)->parent_id;
                                $feelednode = 0;
//                                print_r($ledgernames); exit;

                                foreach ($inp as $inpvalue) {
                                    $lidp = OrganizationalStructureValues::findFirst("name LIKE '" . strtoupper($inpvalue) . "'"
                                                    . " and org_master_id IN (" . implode(',', $feeorgids) . ")"
                                                    . " and parent_id = $parfeeroot");
                                    if ($lidp) {
                                        $parentldid = $parfeeroot->id;
                                        $feelednode = 1;
                                    }
                                }
                            }
                            if ($feelednode == 1) {

                                $messages['type'] = 'error';
                                $messages['message'] = "Parent ledger is linked with fee node and can't be created.";
                                print_r(json_encode($messages));
                                exit;
                            }

                            $editnmode = 1; //can be edited
                        } else if ($orgmasFeeCheck->module == 'Fee') {

                            $resfeeled = ControllerBase::FeeAndLedgerTree($parent_id);
                            $parentldid = $resfeeled[1];
                            $orgids = $resfeeled[3];
                            $arryfeledger = $resfeeled[2];
                            $totledger = implode(',', $arryfeledger);
                            $ledgerVoucher = LedgerVoucher::findFirst("creditledger IN ($totledger) or debitledger IN ($totledger) ");
                            if ($ledgerVoucher) {
                                $feemaster = FeeMaster::findFirst('(find_in_set( "' . $orgValues->id . '" ,REPLACE(node_id, "-", "," ) ) '
                                                . ' or find_in_set( "' . $orgValues->id . '" ,REPLACE(fee_node, "-", "," ) ))'
                                                . ' and paid_amt > 0');
                                if ($feemaster) {

                                    $editnmode = 0;
                                    $messages['type'] = 'error';
                                    $messages['message'] = "This node has transaction and can't be edited ";
                                    print_r(json_encode($messages));
                                    exit;
                                } else {
                                    //create a new node against fee node
                                    $editnmode = 2; //can be edited with fee ledger creation
                                }
                            } else {
                                $ledgerid = $parentldid;
                                $editnmode = 3; //can be edited with fee ledger edition
//                                $messages['type'] = 'warning';
//                                $messages['message'] = "New node -  Create a new ledger for this fee node <br>"
//                                        . " Rename existing -  Existing ledger related to this fee node will be renamed";
//                                print_r(json_encode($messages));
//                                exit;
                            }
                        }
                        if (in_array($editnmode, array('1', '2', '3'))) {
                            if ($orgValues->save()) {
                                if ($editnmode == 2) {
                                    if ($orgmasFeeCheck->module == 'Fee') {
                                        $recname = strtoupper($nodevalue);
                                        $resfeeled = ControllerBase::FeeAndLedgerTree($parent_id);
                                        $parentldid = $resfeeled[1];
                                        $orgids = $resfeeled[3];
                                        $arryfeledger = $resfeeled[2];
                                        $param['ledgername'] = $recname;
                                        $param['lid'] = $parentldid;
                                        $lid = LedgerController::saveNewLedger($param);
                                        if ($lid > 0) {
                                            $transaction->commit();
                                            $messages['type'] = 'success';
                                            $messages['message'] = 'Fee node and its corresponding ledgers are created!';
                                            print_r(json_encode($messages));
                                            exit;
                                        }
                                    } else {
                                        $transaction->commit();
                                        $messages['type'] = 'success';
                                        $messages['message'] = 'Saved successfully';
                                        print_r(json_encode($messages));
                                        exit;
                                    }
                                } else if ($editnmode == 3) {
                                    $resfeeled = ControllerBase::FeeAndLedgerTree($parent_id);
                                    $parentldid = $resfeeled[1];
                                    $orgids = $resfeeled[3];
                                    $arryfeledger = $resfeeled[2];

                                    $editledger = OrganizationalStructureValues::findFirstById($ledgerid);
                                    $editledger->name = $nodevalue;
                                    $editledger->parent_id = $parentldid;
                                    if ($editledger->save()) {
                                        $transaction->commit();
                                        $messages['type'] = 'success';
                                        $messages['message'] = 'Saved successfully';
                                        print_r(json_encode($messages));
                                        exit;
                                    } else {
                                        $error = '';
                                        foreach ($editledger->getMessages() as $message) {
                                            $error .= $message . '<br>';
                                        }
                                        $messages['type'] = 'error';
                                        $messages['message'] = $error;
                                        print_r(json_encode($messages));
                                        exit;
                                    }
                                } else {
                                    $transaction->commit();
                                    $messages['type'] = 'success';
                                    $messages['message'] = 'Saved successfully';
                                    print_r(json_encode($messages));
                                    exit;
                                }
                            } else {
                                $error = '';
                                foreach ($orgValues->getMessages() as $message) {
                                    $error .= $message . '<br>';
                                }
                                $messages['type'] = 'error';
                                $messages['message'] = $error;
                                print_r(json_encode($messages));
                                exit;
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {

            $error = '';
            foreach ($e->getMessages() as $message) {
                $error .= $message . '<br>';
            }
            $message['type'] = 'error';
            $message['message'] = $error;
            print_r(json_encode($message));
            exit;
        }
    }

    /* Organizational Structure value - Recursive callback for breadcrum
     * $nodevalue - id of OrganizationalStructureValue (currently clicked node)
     *  */

    public function _findBreadcrum($nodevalue) {
        $orgroute = OrganizationalStructureValues::findFirstById($nodevalue->parent_id);
        if ($orgroute) {
            $out.= OrganizationalStructureController::_findBreadcrum($orgroute);
        }
        $out.= '<li class="active "><a class="btn  btn-info" ondblclick="orgmasterSettings.openValue(this )" data-toggle="tooltip" 
             org-value ="' . $nodevalue->id . '" org-master="' . $nodevalue->org_master_id . '"
                 tooltip-title="Double click on the value buttons to go forward/backward" >' . $nodevalue->name . '</a></li>';
        return $out;
    }

    /* Organizational Structure value - Internal service 
     * $nodeid - id of OrganizationalStructureValue (currently clicked node)
     * Returns the complete array of required node  its child values
     *  */

    public function _getValueTreeFor($nodeid) {

        $exist = OrganizationalStructureValues::find('parent_id =' . $nodeid);
        $nodeval = OrganizationalStructureValues::findFirstById($nodeid);
        $masternode = OrganizationalStructureMaster::findFirstById($nodeval->org_master_id);
        $choutput = array();
        if (count($exist) > 0) {
            $nodearr = array();
            $nodearr['id'] = $nodeval->id;
            $nodearr['status'] = $nodeval->status == 'C' ? 'Current' : 'Next';
            $nodearr['name'] = $nodeval->name;
            $nodearr['master_name'] = $masternode->name;
            $nodearr['mandatory_for_admission'] = $masternode->mandatory_for_admission == 1 ? 'Yes' : 'No';
            $nodearr['cyclenode'] = $masternode->cycle_node == 1 ? 'Yes' : 'No';
            foreach ($exist as $chl) {
                $nodearr['child'][] = OrganizationalStructureController::_getValueTreeFor($chl->id);
            }
            $choutput = $nodearr;
        } else {
            $nodearr = array();
            $nodearr['id'] = $nodeval->id;
            $nodearr['status'] = $nodeval->status == 'C' ? 'Current' : 'Next';
            $nodearr['name'] = $nodeval->name;
            $nodearr['master_name'] = $masternode->name;
            $nodearr['mandatory_for_admission'] = $masternode->mandatory_for_admission == 1 ? 'Yes' : 'No';
            $nodearr['cyclenode'] = $masternode->cycle_node == 1 ? 'Yes' : 'No';
            $choutput = $nodearr;
        }

        return $choutput;
    }

    public function getCycleNodeAction() {
        $res = OrganizationalStructureController::_getValueTreeFor(3);
        echo '<pre>';
        print_r($res);
    }

    public function valueTreeAction() {
        $this->tag->prependTitle("Organizational structure | ");
        $this->assets
                ->collection('treeJs')
                ->addCss('css/treecss/style.css')
                ->addJs('js/jquery-1.8.1.min.js')
                ->addJs('js/orgmaster/orgmaster.js')
                ->addJs('js/jquery-ui.js')
                ->addJs('js/jquery.tree.js');

        $rootid = OrganizationalStructureValues::findFirstByParentId(0)->id;
        $reslt = OrganizationalStructureController::find_childtreeval($rootid, '');
        $output.=$reslt;

        $this->view->mastertree = $output;
    }

    public function valueTreeD3Action() {

        $this->view->onboard = 0;
        if ($this->request->get('onboard') == 'true') {
            $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
            $this->view->onboard = 1;
        } else {
            $this->tag->prependTitle("Organizational structure | ");
            $this->assets->addJs('js/orgmaster/orgmaster.js');
        }
        $this->view->mastersubor = OrganizationalStructureMaster::find(array('columns' => ' distinct module, nodefont',
                    'is_subordinate=1'));
    }

    public function getToBeexpandedIndexAction($exparray, $aggregatenodes = array(), $faggregatenodes = array()) {
        foreach ($exparray as $key => $par) {
            if ($par['expandnodes'] == 1) {
                $aggregatenodes[] = $key;
                $faggregatenodes[] = implode('-', $aggregatenodes);
                if (isset($par['children'])) {
                    $res = OrganizationalStructureController::getToBeexpandedIndexAction($par['children'], $aggregatenodes, $faggregatenodes);
                    $faggregatenodes = $res[1];
                    array_pop($res[0]);
                    $aggregatenodes = $res[0];
                }
            }
        }

        return array($aggregatenodes, $faggregatenodes);
    }

    public function valueTreeJsonAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $selectedmasterids = '';
        if ($this->request->isGet()) {

            $params = $queryParams = array();
            foreach ($this->request->get() as $key => $value) {
                $params[$key] = $value;
            }

            if (isset($params['module'])) {
                $module = $params['module'];
                $selectedmasterids = OrganizationalStructureMaster::findFirst(array('columns' => ' group_concat(id) as masids',
                            'module ="' . $module . '"'));

                $mas[] = $selectedmasterids->masids;
            }
        }

//        $rootid = OrganizationalStructureValues::findFirstByParentId(0)->id;
        $rootq = 'Select o.id , o.name, m.name as orgname, m.nodefont, m.color as nodecolor '
                . ' FROM OrganizationalStructureValues o '
                . ' INNER JOIN OrganizationalStructureMaster m on m.id=o.org_master_id '
                . ' WHERE o.parent_id =0';
        $root = $this->modelsManager->executeQuery($rootq);
        $masterids = OrganizationalStructureMaster::findFirst(array('columns' => ' group_concat(id) as masids',
                    'is_subordinate=0 '));
        $mas[] = $masterids->masids;
        // print_r($mas);exit;
        $output = OrganizationalStructureController::find_childtreevaljson($root[0], implode(',', $mas));
        // $output['expandnodes'] = implode(',', $aggregatenodes[1]);
//        echo'<pre>';  print_r($output);  exit;
        echo json_encode($output);
        exit;
    }

    public function find_childtreevaljson($anode, $masids) {

//        $sql = 'Select o.id , o.name, m.name as orgname, m.nodefont, m.color '
//                . ' FROM OrganizationalStructureValues o '
//                . ' INNER JOIN OrganizationalStructureMaster m on m.id=o.org_master_id '
//                . ' WHERE o.parent_id =' . $anode->id . ' and o.org_master_id IN (' . $masids . ')';

        $sql = 'Select o.id , o.name, o.nodefont, o.nodecolor, o.orgname'
                . ' FROM OrganizationalStructureValues o '
                . ' WHERE o.parent_id =' . $anode->id . ' and o.org_master_id IN (' . $masids . ')';
        $exist = $this->modelsManager->executeQuery($sql);
        $masname = $anode->orgname;
        $mas = (strlen($masname) > 5 ? substr($masname, 0, 5) . "..." : $masname);
        $chldnm = (strlen($anode->name) > 10 ? substr($anode->name, 0, 10) . "..." : $anode->name);
        if (count($exist) > 0) {
            $choutput['name'] = $mas . ': ' . $chldnm;
            $choutput['nodeid'] = $anode->id;
            $choutput['groupnode'] = $masname;
            $choutput['font'] = $anode->nodefont;
            $choutput['fillcolor'] = $anode->nodecolor; //$this->bgclass[round($nodeval->id % 10)]['highlight'];
            foreach ($exist as $chl) {
                $choutput["children"][] = OrganizationalStructureController::find_childtreevaljson($chl, $masids);
            }
        } else {
            $choutput['name'] = $mas . ': ' . $chldnm;
            $choutput['font'] = $anode->nodefont;
            $choutput['groupnode'] = $masname;
            $choutput['nodeid'] = $anode->id;
            $choutput['fillcolor'] = '#ddd';
        }
        return $choutput;
    }

    public function find_childtreeval($aid, $choutput) {
        $exist = OrganizationalStructureValues::find('parent_id =' . $aid);
//        print_r(count($exist));exit;
        $attribute = $choutput ? '' : 'class="tree"';
        $nodeval = OrganizationalStructureValues::findFirstById($aid);
        $choutput.=$choutput ? '' : '<ul ' . $attribute . ' > ';
        if (count($exist) > 0) {
            $choutput.='<li> <div id="' . $aid . '">'
                    . $nodeval->name .
                    '</div> <ul>';
            foreach ($exist as $chl) {
                $choutput = OrganizationalStructureController::find_childtreeval($chl->id, $choutput);
            }
            $choutput.='</li></ul>';
        } else {
            $choutput.='<li> <div id="' . $aid . '" >' . $nodeval->name . '</div> </li>';
        }


        return $choutput;
    }

    public function _getParentNames($aid, $feenames = array()) {
        $feenode = OrganizationalStructureValues::findFirstById($aid);
        $iscycle = OrganizationalStructureMaster::findFirstById($feenode->org_master_id);
        if ($iscycle->module == "Fee") {
            $feenames[$feenode->name] = $feenode->name;
            if ($feenode->parent_id > 0) {
                $feenames = OrganizationalStructureController::_getParentNames($feenode->parent_id, $feenames);
            }
        }
        return $feenames;
    }

    public function _getParentNamesLedger($aid, $feenames = array()) {
        $feenode = OrganizationalStructureValues::findFirstById($aid);
        $iscycle = OrganizationalStructureMaster::findFirstById($feenode->org_master_id);
        if ($iscycle->module == "Ledger") {
            $feenames[$feenode->name] = $feenode->name;
            if ($feenode->parent_id > 0 && $feenode->name != 'Indirect Income') {
                $feenames = OrganizationalStructureController::_getParentNamesLedger($feenode->parent_id, $feenames);
            }
        }
        return $feenames;
    }

    public function _getParentIdsLedger($aid, $feenames = array()) {
        $feenode = OrganizationalStructureValues::findFirstById($aid);
        $iscycle = OrganizationalStructureMaster::findFirstById($feenode->org_master_id);
        if ($iscycle->module == "Ledger") {
            $feenames[$feenode->id] = $feenode->id;
            if ($feenode->parent_id > 0) {
                $feenames = OrganizationalStructureController::_getParentIdsLedger($feenode->parent_id, $feenames);
            }
        }
        return $feenames;
    }

//    public function _getLegerIdsforNames($name, $ledgerids = array()) {
//         $lidp = LedgerMaster::findFirst("ledgername LIKE '" . strtoupper($name) . "'");
//          $ledgerids[$lidp->lid] = $lidp->lid;
//    }

    public function dynamictreeAction() {
        
    }

    public function valueTreeJsonforMasterAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $rootid = OrganizationalStructureMaster::findFirstByParentId(0)->id;
        $masterids = OrganizationalStructureMaster::findFirst(array('columns' => ' group_concat(id) as masids',
                    'is_subordinate=0'));
        $mas[] = $masterids->masids;
        $output = OrganizationalStructureController::find_masterchildtreevaljson($rootid, implode(',', $mas));

        //$aggregatenodes = $this->getToBeexpandedIndexAction($output['children']);
        // $output['expandnodes'] = implode(',', $aggregatenodes[1]);
        // echo'<pre>'; print_r($output);exit;
        echo json_encode($output);
        exit;
    }

    public function find_masterchildtreevaljson($aid, $masids) {
        $exist = OrganizationalStructureMaster::find('parent_id =' . $aid);
//        $nodeval = OrganizationalStructureValues::findFirst('id =' . $aid);
        $nodeval = OrganizationalStructureMaster::findFirstById($aid);
        if (count($exist) > 0) {
            $choutput['name'] = (strlen($nodeval->name) > 15 ? substr($nodeval->name, 0, 10) . "..." : $nodeval->name);
            $choutput['font'] = $nodeval->nodefont;
            $choutput['nodeid'] = $nodeval->id;
            $choutput['fillcolor'] = $this->bgclass[round($nodeval->id % 10)]['color'];
            //$choutput['expandnodes'] = 1;
            foreach ($exist as $chl) {
                $nodevalch = OrganizationalStructureMaster::findFirst('id =' . $chl->id);
                if ($nodevalch) {
                    $choutput["children"][] = OrganizationalStructureController::find_masterchildtreevaljson($chl->id, $masids);
                }
            }
        } else {
            $choutput['name'] = (strlen($nodeval->name) > 15 ? substr($nodeval->name, 0, 15) . "..." : $nodeval->name);
            $choutput['font'] = $nodeval->nodefont;
            $choutput['nodeid'] = $nodeval->id;
            $choutput['fillcolor'] = '#ddd'; // $this->bgclass[round($nodeval->parent_id % 10)]['color'];
            // $choutput['expandnodes'] = 1;
//         if ($master->promotion_status == 'above cycle node' || ($master->promotion_status == 'cycle node' && ($nodeval->status == 'C' ))) {
//                $choutput['expandnodes'] = 1;
//            }
        }
        return $choutput;
    }

    public function editValueTreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $valid = $this->request->getPost('valueid');
            $orgvaldet = OrganizationalStructureValues::findFirstById($valid);
            $this->view->orgvaldet = $orgvaldet;
        }
    }

    public function viewValueTreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $valid = $this->request->getPost('valueid');
            $orgvaldet = OrganizationalStructureValues::findFirstById($valid);

            $this->view->orgvaldet = $orgvaldet;
        }
    }

    public function addValueTreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $valid = $this->request->getPost('valueid');
            $orgvaldet = OrganizationalStructureValues::findFirstById($valid);
            $this->view->orgmasdata = $orgmasdata = OrganizationalStructureMaster::find('parent_id=' . $orgvaldet->org_master_id);
            $this->view->orgvaldet = $orgvaldet;
        }
    }

    public function loadDefinitionAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $valid = $this->request->getPost('valueid');
            $orgvaldet = OrganizationalStructureValues::findFirstById($valid);
            $this->view->orgmasdata = $orgmasdata = OrganizationalStructureMaster::find('parent_id=' . $orgvaldet->org_master_id);
            $this->view->orgvaldet = $orgvaldet;
        }
    }

    public function editMasterTreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $masid = $this->request->getPost('valueid');
            $orgmasdet = OrganizationalStructureMaster::findFirstById($masid);

            $this->view->orgmasdet = $orgmasdet;
        }
    }

    public function updateMasTreeValueAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $params = $data = array();
            foreach ($this->request->getPost() as $key => $value) {
                $params[$key] = $value;
            }
            $module = isset($params['module']) ? $params['module'] :
                    (isset($params['module_name']) ? $params['module_name'] : '');

            $orgmas_id = isset($params['org_value']) ? $params['org_value'] : '';

            $orgmasdet = $orgmas_id ? OrganizationalStructureMaster::findFirstById($orgmas_id) :
                    new OrganizationalStructureMaster();
            $parent = OrganizationalStructureMaster::findFirstById($params['parentnodeid']);
            $orgmasdet->name = $params['nodevalue'];
            $orgmasdet->parent_id = $parent ? $parent->id : 0;
            $orgmasdet->cycle_node = $params['cyclenode'];
            $orgmasdet->mandatory_for_admission = $params['admissionmandatory'];
            $orgmasdet->mandatory_for_appointment = $params['appointmentmandatory'];
            $orgmasdet->promotion_status = '';
            $orgmasdet->module = $module;
            $orgmasdet->is_subordinate = $params['subordinatemandatory'] ? $params['subordinatemandatory'] : $params['subordinatemandatory'];

            $orgmasdet->color = $orgmasdet->color ? $orgmasdet->color : ($parent ? $parent->color : '#a9d86e');
            $orgmasdet->nodefont = $orgmasdet->nodefont ? $orgmasdet->nodefont : ($parent ? $parent->nodefont : 'bank');
//            print_r($orgmasdet);
//            exit;

            if (!$orgmasdet->save()) {
//                echo 'test';exit;
                $error = '';
                foreach ($orgmasdet->getMessages() as $messages) {
                    $error .= $messages . "\n";
                }
                $message['type'] = 'error';
                $message['message'] = $error;
                print_r(json_encode($message));
                exit;
            } else {
                $valuestree = OrganizationalStructureValues::find('org_master_id= ' . $orgmasdet->id);
                foreach ($valuestree as $valnde) {
                    $valnde->orgname = $orgmasdet->name;
                    $valnde->nodefont = $orgmasdet->nodefont;
                    $valnde->nodecolor = $orgmasdet->color;
                    if (!$valnde->save()) {
                        foreach ($valnde->getMessages() as $messages) {
                            $error .= $messages . "\n";
                        }
                        $message['type'] = 'error';
                        $message['message'] = $error;
                        print_r(json_encode($message));
                        exit;
                    }
                }
                $message['type'] = 'success';
                $message['message'] = 'Saved Successfully.';
                print_r(json_encode($message));
            }
        }
    }

    public function viewMasterTreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $masid = $this->request->getPost('valueid');
            $orgmasdet = OrganizationalStructureMaster::findFirstById($masid);

            $this->view->orgmasdet = $orgmasdet;
        }
    }

    public function addnewMasterTreeAction() {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        if ($this->request->isPost()) {
            $masid = $this->request->getPost('valueid');
            $orgmasdet = OrganizationalStructureMaster::findFirstById($masid);

            $this->view->orgmasdet = $orgmasdet;
        }
    }

    public function valueTreeGroupJsonAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $selectedmasterids = '';
        $params = $queryParams = array();
        if ($this->request->isGet()) {
            foreach ($this->request->get() as $key => $value) {
                $params[$key] = $value;
            }
        }

        if (isset($params['module'])) {
            $module = $params['module'];
            $selectedmasterids = OrganizationalStructureMaster::findFirst(array('columns' => ' group_concat(id) as masids',
                        'module ="' . $module . '"'));

            $mas[] = $selectedmasterids->masids;
        }

        $masterids = OrganizationalStructureMaster::findFirst(array('columns' => ' group_concat(id) as masids', 'is_subordinate=0'));
        $mas[] = $masterids->masids;
        $masid = implode(',', $mas);
//---------------
        $supernodeid = (isset($params['supernodeid']) && $params['supernodeid'] != '') ? $params['supernodeid'] : '';
        $rootid = (isset($params['rootid']) && $params['rootid'] != '') ? OrganizationalStructureValues::find('parent_id =' . $params['rootid']) : OrganizationalStructureValues::find('parent_id =0');

        if ($supernodeid || count($exist) > 1) { //ctn1
            $org_grup_arr = array();
            foreach ($rootid as $chl) { //loop1
                if ($chl->org_master_id == $supernodeid) {
                    $output["children"][] = OrganizationalStructureController::group_childtreevaljson($chl->id, $masid);
                } else if (!in_array($chl->org_master_id, $org_grup_arr) && in_array($chl->org_master_id, explode(',', $masid))) {
                    $org_grup_arr[] = $chl->org_master_id;
                    $output["children"][] = OrganizationalStructureController::group_childtreenodejson($chl->org_master_id, $chl->parent_id, $masid);
                }
            }//loop1 end
        } else {
            $output = OrganizationalStructureController::group_childtreevaljson($rootid[0]->id, $masid);
        }
        /* if((isset($params['rootid'])&&$params['rootid']!='')){
          $output1['children'][]=$output;
          }else{
          $output1=$output;
          } */


//print_r($output);exit;
        echo json_encode($output);
        exit;
    }

    public function group_childtreevaljson($aid, $masids) {
        $nodeval = OrganizationalStructureValues::findFirstById($aid);
        $gropnode = OrganizationalStructureMaster::findFirst('id =' . $nodeval->org_master_id);
        $exist = OrganizationalStructureValues::find('parent_id =' . $aid);
        $nodevalcnt = OrganizationalStructureValues::findFirst(array('columns' => 'count(*) as cnt', 'org_master_id=' . $aid));
        $choutput['name'] = $nodeval->name;
        $choutput['font'] = $gropnode->nodefont;
        $choutput['nodeid'] = $nodeval->id;
        $choutput['fillcolor'] = $gropnode->color;
        $choutput['nodetype'] = 'Valuenode';
        if (count($exist) > 0) {
            $nodevalch = OrganizationalStructureMaster::find('parent_id =' . $gropnode->id . ' and id  IN (' . $masids . ')');
            foreach ($nodevalch as $chl) {
                $choutput["children"][] = OrganizationalStructureController::group_childtreenodejson($chl->id, $nodeval->id, $masids);
            }
        }
        return $choutput;
    }

    public function group_childtreenodejson($aid, $parentid = 0, $masids) {
        $choutput = array();
        $exist = OrganizationalStructureMaster::find('parent_id =' . $aid);
        $nodeval = OrganizationalStructureMaster::findFirstById($aid);
        $qry[] = 'org_master_id=' . $aid;
        if ($parentid > 0) {
            $qry[] = 'parent_id=' . $parentid;
        }
//print_r(implode(' and ', $qry));exit;
        $nodevalcnt = OrganizationalStructureValues::findFirst(array('columns' => 'count(*) as cnt', implode(' and ', $qry)));
        $choutput['name'] = $nodeval->name . ' (' . $nodevalcnt->cnt . ' )';
        $choutput['font'] = $nodeval->nodefont;
        $choutput['nodeid'] = $nodeval->id;
        $choutput['fillcolor'] = $nodeval->color;
        $choutput['nodetype'] = 'Supernode';
        if (count($exist) > 0) {
            foreach ($exist as $chl) {
                $nodevalch = OrganizationalStructureMaster::findFirst('id =' . $chl->id . ' and id  IN (' . $masids . ')');
                if ($nodevalch) {
                    $choutput["children"][] = OrganizationalStructureController::group_childtreenodejson($chl->id, 0, $masids);
                }
            }
        }
        return $choutput;
    }

    public function deleteValueNodeAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $valid = $this->request->getPost('valueid');
            $orgdet = OrganizationalStructureValues::findFirstById($valid);
            $orgmasFeeCheck = OrganizationalStructureMaster::findFirstById($orgdet->org_master_id);
            if ($orgmasFeeCheck->module == 'structural' && $orgmasFeeCheck->parent_id == 0) {
                $messages['type'] = 'error';
                $messages['message'] = "Root node cannot be deleted.";
                print_r(json_encode($messages));
                exit;
            }

            if ($orgmasFeeCheck->module == 'Ledger' && $orgmasFeeCheck->cycle_node != 1) {
                $ledgernames = OrganizationalStructureController::_getParentNamesLedger($orgdet->id);
                array_pop($ledgernames);
                if (count($ledgernames) > 0) {
                    $inp = array_reverse($ledgernames);
                    $feeorgids = ControllerBase::getFeeOrgMasIds();
                    $parfeeroot = ControllerBase::getFeeRootNode($orgdet->id)->parent_id;
                    $feelednode = 0;
//                                print_r($ledgernames); exit;

                    foreach ($inp as $inpvalue) {
                        $lidp = OrganizationalStructureValues::findFirst("name LIKE '" . strtoupper($inpvalue) . "'"
                                        . " and org_master_id IN (" . implode(',', $feeorgids) . ")"
                                        . " and parent_id = $parfeeroot");
                        if ($lidp) {
                            $parentldid = $parfeeroot->id;
                            $feelednode = 1;
                        }
                    }
                }
                if ($feelednode == 1) {

                    $messages['type'] = 'error';
                    $messages['message'] = "Parent ledger is linked with fee node and can't be deleted.";
                    print_r(json_encode($messages));
                    exit;
                }
            }
            if ($orgmasFeeCheck->module == 'Fee') {
                $resfeeled = ControllerBase::FeeAndLedgerTree($orgdet->id);
                $parentldid = $resfeeled[1];
                $orgids = $resfeeled[3];
                $arryfeledger = $resfeeled[2];
                $totledger = implode(',', $arryfeledger);
                $ledgerVoucher = LedgerVoucher::findFirst("creditledger IN ($totledger) or debitledger IN ($totledger) ");
                if (!$ledgerVoucher) {
                    $voudel = OrganizationalStructureValues::findFirstById($parentldid);
//                    print_r( $orgdet->id);exit;
                    if (!$voudel->delete()) {
                        $error = '';
                        foreach ($voudel->getMessages() as $message) {
                            $error .= $message . '<br>';
                        }
                        $messages['type'] = 'error';
                        $messages['message'] = $error;
                        print_r(json_encode($messages));
                        exit;
                    }
                }
            }
            if ($orgdet->delete()) {

                $messages['type'] = 'success';
                $messages['message'] = 'Node deleted successfully';
                print_r(json_encode($messages));
                exit;
            } else {
                $error = '';
                foreach ($orgdet->getMessages() as $message) {
                    $error .= $message . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        }
    }

    public function deleteMasterNodeAction() {

        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        if ($this->request->isPost()) {
            $valid = $this->request->getPost('valueid');
            $orgdet = OrganizationalStructureMaster::findFirstById($valid);
            if ($orgdet->delete()) {
                $messages['type'] = 'success';
                $messages['message'] = 'Node deleted successfully';
                print_r(json_encode($messages));
                exit;
            } else {
                $error = '';
                foreach ($orgdet->getMessages() as $message) {
                    $error .= $message . '<br>';
                }
                $messages['type'] = 'error';
                $messages['message'] = $error;
                print_r(json_encode($messages));
                exit;
            }
        }
    }

    public function importXMLTemplateAction() {
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $insTyp = 'School';
        $country = 'India';
        if ($this->request->isPost()) {
            $insTyp = $this->request->getPost('instype');
            $country = $this->request->getPost('country');
        }
        $structURl = ORGTEMPL_DIR . $insTyp . '/' . $country . '/org.xml';
        $valueURl = ORGTEMPL_DIR . $insTyp . '/' . $country . '/orgVal.xml';
//        echo '<pre>';
        libxml_use_internal_errors(true);
        $sxe = simplexml_load_file($structURl);
        if (!$sxe) {
            print_r(libxml_get_errors());
        } else {
//            print_r($sxe); exit;
            $addedres = OrganizationalStructureController::recursiveLoadXml($sxe);
            if ($addedres) {
                $message['type'] = 'success';
                $message['message'] = 'Added Successfully';
                print_r(json_encode($message));
                exit;
            }
        }
    }

    public function recursiveLoadXml($sxe, $parent_id = 0) {
        foreach ($sxe->children() as $nodekey => $nodevalue) {
            if (!empty($nodevalue)) {
//                    echo  '<br>';
                foreach ($nodevalue->attributes() as $key => $attr) {
                    $attr = (array) $attr;
                    $module = $attr[0];
                }
                $params = array();
                foreach ((array) $nodevalue as $nkey => $nattr) {
                    if ($nkey != "childnode") {
                        $params[$nkey] = (string) $nattr;
                    }
                }
                $nextp = new stdClass();
                $nextp = (object) $params;
//                print_r($nextp);
//                $id = '';
                $id = $nextp->id;
                $name = $nextp->name;
                $cycle_node = (strtolower($nextp->cycle_node) == 'yes') ? '1' : '2';
                $mandatory_for_admission = (strtolower($nextp->mandatory_for_admission) == 'yes') ? '1' : '2';
                $mandatory_for_appointment = (strtolower($nextp->mandatory_for_appointment) == 'yes') ? '1' : '2';
                $promotion_status = '';
                $module = $module;
                $is_subordinate = (strtolower($nextp->is_subordinate) == 'yes') ? '1' : '0';
                $nodefont = $nextp->icon;
                $color = $nextp->color;
                
                $upload_flag= isset($nextp->uploadFlag)?$nextp->uploadFlag:'0';
                $basic_level= isset($nextp->basicLevel)?$nextp->basicLevel:'0';
                $secondary_level=isset($nextp->secondaryLevel)?$nextp->secondaryLevel:'0';
                $getSubjects= isset($nextp->getSubjects)?$nextp->getSubjects:'0';
                $specific= isset($nextp->specific)?$nextp->specific:'0';
                $default= isset($nextp->default)?$nextp->default:'0';

                //ADDING NODE
                $orgmasdet = OrganizationalStructureMaster::findFirstById($id) ? OrganizationalStructureMaster::findFirstById($id) :
                        (OrganizationalStructureMaster::findFirst("name = '$name' and parent_id= $parent_id ") ?
                                OrganizationalStructureMaster::findFirst("name = '$name' and parent_id= $parent_id ") :
                                new OrganizationalStructureMaster()
                        );
                $orgmasdet->id = $id;
                $orgmasdet->name = $name;
                $orgmasdet->parent_id = $parent_id;
                $orgmasdet->cycle_node = $cycle_node;
                $orgmasdet->mandatory_for_admission = $mandatory_for_admission;
                $orgmasdet->mandatory_for_appointment = $mandatory_for_appointment;
                $orgmasdet->promotion_status = $promotion_status;
                $orgmasdet->module = $module;
                $orgmasdet->is_subordinate = $is_subordinate;
                $orgmasdet->color = $color;
                $orgmasdet->nodefont = $nodefont;
                
                //default stuct
                
                $orgmasdet->upload_flag = $upload_flag;
                $orgmasdet->basic_level = $basic_level;
                $orgmasdet->secondary_level = $secondary_level;
                $orgmasdet->getSubjects = $getSubjects;
                $orgmasdet->specific = $specific;
//                $orgmasdet->default = $default;

//            print_r($orgmasdet); exit;
                if (!$orgmasdet->save()) {
                    $error = '';
                    foreach ($orgmasdet->getMessages() as $messages) {
                        $error .= $messages . "\n";
                    }
                    $message['type'] = 'error';
                    $message['message'] = $error;
                    print_r(json_encode($message));
                    exit;
                } else {
                    $valuestree = OrganizationalStructureValues::find('org_master_id= ' . $orgmasdet->id);
                    if (count($valuestree) > 0) {
                        foreach ($valuestree as $valnde) {
                            $valnde->orgname = $orgmasdet->name;
                            $valnde->nodefont = $orgmasdet->nodefont;
                            $valnde->nodecolor = $orgmasdet->color;
                            if (!$valnde->save()) {
                                foreach ($valnde->getMessages() as $messages) {
                                    $error .= $messages . "\n";
                                }
                                $message['type'] = 'error';
                                $message['message'] = $error;
                                print_r(json_encode($message));
                                exit;
                            }
                        }
                    }
                }
                if (count($nodevalue->childnode) > 0) {
//                    echo $orgmasdet->id.'>';
                    OrganizationalStructureController::recursiveLoadXml($nodevalue->childnode, $orgmasdet->id);
                }
            }
        }
        return true;
    }

    public function recursiveLoadValXml($sxe, $parent_id = 0) {
        foreach ($sxe->children() as $nodekey => $nodevalue) {
            $org_master_id = '';
            if (!empty($nodevalue)) {
//                    echo  '<br>';
                foreach ($nodevalue->attributes() as $key => $attr) {
                    $attr = (array) $attr;
                    $org_master_id = $attr[0];
                }
            }
            foreach ($nodevalue->children() as $zkey => $zvalue) {

//                print_r($zvalue);  exit;
                if (!empty($zvalue)) {
                    foreach ((array) $zvalue as $nkey => $nattr) {
                        if ($nkey != "childnode") {
                            $params[$nkey] = (string) $nattr;
                        }
                    }
                    $nextp = (object) $params;
//                    print_r($nextp); exit;
                    $name = $nextp->name;
                    $status = (strtolower($nextp->status) == 'current') ? 'C' : 'N';
                    $rangeFrom = $nextp->rangeFrom ? strtotime($nextp->rangeFrom) : '';
                    $rangeTo = $nextp->rangeTo ? strtotime($nextp->rangeTo) : '';
                    $valuestree = OrganizationalStructureMaster::findFirstById($org_master_id);
                    $nodefont = $valuestree->nodefont;
                    $nodecolor = $valuestree->color;
                    $orgname = $valuestree->name;
                    //ADDING NODE
                    $orgmasdet = OrganizationalStructureValues::findFirstById($id) ? OrganizationalStructureMaster::findFirstById($id) :
                            (OrganizationalStructureValues::findFirst("name = '$name' and parent_id= $parent_id and org_master_id = $org_master_id") ?
                                    OrganizationalStructureValues::findFirst("name = '$name' and parent_id= $parent_id  and org_master_id = $org_master_id") :
                                    new OrganizationalStructureValues()
                            );
                    $orgmasdet->name = $name;
                    $orgmasdet->status = $status;
                    $orgmasdet->rangefrom = $rangeFrom;
                    $orgmasdet->rangeto = $rangeTo;
                    $orgmasdet->nodefont = $nodefont;
                    $orgmasdet->nodecolor = $nodecolor;
                    $orgmasdet->orgname = $orgname;
                    $orgmasdet->org_master_id = $org_master_id;
                    $orgmasdet->parent_id = $parent_id;

//                    print_r($orgmasdet); exit;
                    if (!$orgmasdet->save()) {
                        $error = '';
                        foreach ($orgmasdet->getMessages() as $messages) {
                            $error .= $messages . "\n";
                        }
                        $message['type'] = 'error';
                        $message['message'] = $error;
                        print_r(json_encode($message));
                        exit;
                    }
                    if (count($zvalue->childnode) > 0) {
//                    echo $orgmasdet->id.'>';
//                         print_r($zvalue->childnode); exit;
                        OrganizationalStructureController::recursiveLoadValXml($zvalue->childnode, $orgmasdet->id);
                    }
                }
            }
        }
        return true;
    }

}
