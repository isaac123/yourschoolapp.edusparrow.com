<?php

use \Phalcon\Tag as Tag,
    \Phalcon\Mvc\Model\Criteria,
    Phalcon\Http\Request\File,
    Phalcon\Mvc\View;

class ErrorpageController extends ControllerBase {


    protected function initialize() {
        $this->tag->setTitle("Edu Sparrow");
    } 
    public function show404Action() {
        $this->tag->prependTitle("404 Error | ");
    }
    public function show500Action() {
        $this->tag->prependTitle("500 Error | ");
    }
    public function accessdeniedAction() {
        $this->tag->prependTitle("403 Error | ");
    }
}