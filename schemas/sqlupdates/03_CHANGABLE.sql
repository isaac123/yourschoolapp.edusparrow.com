

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `variableName`, `variableValue`, `variableDescription`) VALUES
(1, 'SA_access_token', '4vcZS0lwVAYjV6Ltd44P6zk55L6Te', 'Super Admin Creation access token'),
(2, 'SA_token_isActive', 'No', 'Super Admin Creation access token is active? Yes/No'),
(3, 'SA_access_email', 'contact@queenmira.com', 'Super Admin Default Email Id'),
(4, 'settings_completed', 'yes', 'Basic School Settings Completed');

-- --------------------------------------------------------


--
-- Dumping data for table `staff_info`
--

INSERT INTO `staff_info` (`id`, `appointment_no`, `aggregate_key`, `loginid`, `Staff_Name`, `Gender`, `Date_of_Birth`, `Date_of_Joining`, `Mobile_No`, `Email`, `Address1`, `Address2`, `State`, `Country`, `Pin`, `Phone`, `status`, `relieve_status`) VALUES

(NULL, 'NULL', 'NULL', 'Siteadmin', 'Siteadmin', '', '', '', '9677770552', 'lourdespradeep@gmail.com', '30A, HAK road,
Chinna Chokkikulam, ', 'Madurai', 'TamilNadu', 'India', '625002', '04524217055', 'NULL', NULL),

(NULL, 'NULL', 'NULL', 'Superadmin', 'Superadmin', '', '', '', '', '', '', '', '', '', '', '', 'NULL', NULL);

-- --------------------------------------------------------

