-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 08, 2016 at 05:43 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `queentrans`
--

-- --------------------------------------------------------

--
-- Table structure for table `staff_profile_settings`
--

CREATE TABLE IF NOT EXISTS `staff_profile_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `column_name` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `form_name` varchar(255) NOT NULL,
  `help_text` varchar(500) NOT NULL,
  `field_format` varchar(100) NOT NULL,
  `default_value` tinyint(1) NOT NULL,
  `hide_or_show` tinyint(1) NOT NULL,
  `mandatory` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` varchar(50) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_on` varchar(50) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `staff_profile_settings`
--

INSERT INTO `staff_profile_settings` (`id`, `parent_id`, `column_name`, `field_name`, `form_name`, `help_text`, `field_format`, `default_value`, `hide_or_show`, `mandatory`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(1, 0, 'NULL', 'STAFF APPOINTMENT DETAILS', 'NULL', 'Header Section', 'Fieldtest', 1, 1, 1, 1, '1419841259', 1, '1419841259'),
(2, 0, 'NULL', 'STAFF PERSONAL DETAILS', 'NULL', 'Header Section', 'Fieldtest', 1, 1, 1, 1, '1419841259', 1, '1419841259'),
(3, 0, 'NULL', 'STAFF SPOUSE DETAILS', 'NULL', 'Header Section', 'Fieldtest', 1, 1, 1, 1, '1419841259', 1, '1419841259'),
(4, 0, 'NULL', 'STAFF PARENT/GUARDIAN DETAILS', 'NULL', 'Header Section', 'Fieldtest', 1, 1, 1, 1, '1419841259', 1, '1419841259'),
(5, 0, 'NULL', 'STAFF MEDICAL DETAILS', 'NULL', 'Header Section', 'Fieldtest', 1, 1, 1, 1, '1419841259', 1, '1419841259'),
(6, 0, 'NULL', 'STAFF OTHER DETAILS', 'NULL', 'Header Section', 'Fieldtest', 1, 1, 1, 1, '1419841259', 1, '1419841259'),
(7, 1, 'catg_id', 'Category', 'category', 'Choice between Teaching staff and Non-Teaching staff', 'Select', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(10, 2, 'photo', 'Photo', 'photo', 'Photo', 'File', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(11, 2, 'address_proof1', 'Address Proof 1', 'address_proof1', 'Address Proof 1', 'File', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(12, 2, 'address_proof2', 'Address Proof 2', 'address_proof2', 'Address Proof 2', 'File', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(13, 2, 'address_proof3', 'Address Proof 3', 'address_proof3', 'Address Proof 3', 'File', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(14, 2, 'qualification', 'Qualification', 'qualification', 'Qualification', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(15, 2, 'nationality', 'Nationality', 'nationality', 'Nationality', 'Select', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(16, 3, 'spouse_name', 'Spouse Name', 'sname', 'Spouse Name', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(17, 3, 's_occupation', 'Spouse Occupation', 'soccup', 'Spouse Occupation', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(18, 3, 's_designation', 'Spouse Designation', 'sdesign', 'Spouse Designation', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(19, 3, 's_phoneno', 'Spouse Phone', 'sphone', 'Spouse Phone', 'Numeric', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(20, 4, 'parent_name', 'Parent/Guardian Name', 'gname', 'Parent/Guardian Name', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(21, 4, 'p_occupation', 'Parent/Guardian Occupation', 'goccup', 'Parent/Guardian Occupation', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(22, 4, 'p_designation', 'Parent/Guardian Designation', 'gdesign', 'Parent/Guardian Designation', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(23, 4, 'p_phoneno', 'Parent/Guardian Phone', 'gphone', 'Parent/Guardian Phone', 'Numeric', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(24, 5, 'blood_group', 'Blood Group', 'blood_grp', 'Blood Group', 'Select', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(25, 5, 'medical_details', 'Other details', 'othrMedDet', 'Any other Medical details', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(26, 6, 'classes_handling', 'Classes Handling (Interests)', 'classHandl', 'Classes Handling (Interests)', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(27, 6, 'subjects_handling', 'Subjects Handling (Interests)', 'subjHandl', 'Subjects Handling (Interests)', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(28, 6, 'experience', 'Previous Work Experience (years)', 'prevExp', 'Previous Work Experience (years)', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(29, 6, 'organization', 'Organization', 'Org', 'Organization', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(30, 6, 'designation', 'Designation', 'designation', 'Designation', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(31, 6, 'comments', 'Comments', 'comments', 'Comments', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(32, 6, 'extra_curricular', 'Extra curricular Activities', 'exActi', 'Extra curricular Activities', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(33, 6, 'co_curricular', 'Co-curricular Activities', 'coActi', 'Co-curricular Activities', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(34, 6, 'additional_qualifications', 'Additional Qualifications', 'addQuali', 'Additional Qualifications', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(35, 6, 'biometric_id', 'BioMetric Id', 'biometric_id', 'BioMetric Id', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(36, 6, 'pf_no', 'PF No', 'pf_no', 'PF No', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(37, 6, 'esi_no', 'ESI No', 'esi_no', 'ESI No', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
