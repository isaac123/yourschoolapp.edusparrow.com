
--
-- Table structure for table `attendance_selectbox`
--

CREATE TABLE IF NOT EXISTS `attendance_selectbox` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `attendance_for` varchar(25) NOT NULL COMMENT 'to staff/stud',
  `attendanceid` varchar(50) DEFAULT NULL COMMENT 'val for att calculation',
  `attendancename` varchar(50) NOT NULL,
  `attendancevalue` float NOT NULL,
  `color` varchar(10) DEFAULT NULL,
  `allowed_leave_typ` int(11) DEFAULT NULL,
  `allowed_count` int(11) DEFAULT NULL,
  `duration_month` int(11) DEFAULT NULL,
  `school_cal_formula` varchar(255) DEFAULT NULL,
  `is_allowed_by_count` tinyint(1) DEFAULT '0',
  `default` tinyint(1) NOT NULL,
  `punish` int(11) DEFAULT NULL COMMENT 'is salary needs to be deducted?',
  `punish_limit` int(11) DEFAULT NULL COMMENT 'limit above which salary has to be deducted',
  `punish_group` int(11) DEFAULT NULL COMMENT 'this variable is to group the count',
  `punish_type` int(11) DEFAULT NULL COMMENT '0 represents instance and 1 represent day',
  `deduct_salary` float DEFAULT NULL,
  `show_in_payslip` int(11) DEFAULT NULL COMMENT '1 means show in payslip, 0 is to hide',
  `substitute` int(11) DEFAULT NULL COMMENT 'substitute attendance value when show is payslip is 0',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `attendance_selectbox`
--

INSERT INTO `attendance_selectbox` (`id`, `attendance_for`, `attendanceid`, `attendancename`, `attendancevalue`, `color`, `allowed_leave_typ`, `allowed_count`, `duration_month`, `school_cal_formula`, `is_allowed_by_count`, `default`, `punish`, `punish_limit`, `punish_group`, `punish_type`, `deduct_salary`, `show_in_payslip`, `substitute`) VALUES
(1, 'staff', 'P', 'Present', 1, '#5ac727', 0, NULL, NULL, '', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'student', 'P', 'Present', 1, '#73ba78', 0, NULL, NULL, '', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'student', 'A', 'Absent', 0, '#e60e0e', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'student', 'L', 'Leave', 0, '#00ffd1', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


