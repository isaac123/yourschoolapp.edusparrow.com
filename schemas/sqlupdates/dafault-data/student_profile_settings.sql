-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 08, 2016 at 05:44 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `queentrans`
--

-- --------------------------------------------------------

--
-- Table structure for table `student_profile_settings`
--

CREATE TABLE IF NOT EXISTS `student_profile_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `column_name` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `form_name` varchar(255) NOT NULL,
  `help_text` varchar(500) NOT NULL,
  `field_format` varchar(100) NOT NULL,
  `default_value` tinyint(1) NOT NULL,
  `hide_or_show` tinyint(1) NOT NULL,
  `mandatory` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` varchar(50) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_on` varchar(50) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `student_profile_settings`
--

INSERT INTO `student_profile_settings` (`id`, `parent_id`, `column_name`, `field_name`, `form_name`, `help_text`, `field_format`, `default_value`, `hide_or_show`, `mandatory`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(1, 0, '', 'Student Admission Details', '', 'Header Section', 'Fieldtest', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(2, 0, '', 'Personal Details', '', 'Header Section', 'Fieldtest', 1, 1, 1, 1, '1419841259', 1, '1419841259'),
(3, 0, '', 'Parental Details', '', 'Header Section', 'Fieldtest', 1, 1, 1, 1, '1419841259', 1, '1419841259'),
(4, 0, '', 'Medical Details', '', 'Header Section', 'Text', 1, 1, 1, 1, '1419841259', 1, '1419841259'),
(5, 0, '', 'Previous School details', '', 'Header Section', 'Fieldtest', 1, 1, 1, 1, '1419841259', 1, '1419841259'),
(6, 1, 'transport', 'Transport Facility', 'transport', 'Choose between options if Transport facility provided', 'Select', 1, 1, 1, 1, '1419841259', 1, '1419841259'),
(7, 2, 'place_of_birth', 'Place of Birth', 'pob', 'Birth place of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(8, 2, 'nationality', 'Nationality', 'nationality', 'Nationality of student', 'Select', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(9, 2, 'religion', 'Religion', 'religion', 'Religion of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(10, 2, 'caste_category', 'Caste Category', 'caste_category', 'Caste category of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(11, 2, 'caste', 'Caste', 'caste', 'caste of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(12, 2, 'first_language', 'First Language', 'language', 'First Language of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(13, 2, 'photo', 'Photo', 'photo', 'Photo of student', 'File', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(14, 2, 'family_photo', 'Family Photo', 'family_photo', 'Family Photo of student', 'File', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(15, 3, 'father_name', 'Father Name', 'fname', 'Father Name of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(16, 3, 'f_occupation', 'Father Occupation', 'foccup', 'Father Occupation of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(17, 3, 'f_designation', 'Father Designation', 'fdesign', 'Father Designation of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(18, 3, 'f_phone_no', 'Father''s phone', 'fphone', 'Father''s phone', 'Numeric', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(19, 3, 'f_phone_no_status', 'Father''s phone Contact', 'fcontact', 'Is Father''s phone primary or secondary ?', 'Select', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(20, 3, 'f_education', 'Father  Education', 'f_education', 'Father  Education', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(21, 3, 'mother_name', 'Mother Name', 'mname', 'Mother Name of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(22, 3, 'm_occupation', 'Mother Occupation', 'moccup', 'Mother Occupation of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(23, 3, 'm_designation', 'Mother Designation', 'mdesign', 'Mother Designation of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(24, 3, 'm_phone_no', 'Mother''s phone', 'mphone', 'Mother''s phone', 'Numeric', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(25, 3, 'm_phone_no_status', 'Mother''s phone Contact', 'mcontact', 'Is Mother''s phone primary or secondary ?', 'Select', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(26, 3, 'm_education', 'Mother Education', 'm_education', 'Mother Education', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(27, 3, 'other_guardian_name', 'Guardian Name', 'gname', 'Guardian Name of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(28, 3, 'g_occupation', 'Guardian Occupation', 'goccup', 'Guardian Occupation of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(29, 3, 'g_designation', 'Guardian Designation', 'gdesign', 'Guardian Designation of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(30, 3, 'g_phone_no', 'Guardian''s phone', 'gphone', 'Guardian''s phone', 'Numeric', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(31, 3, 'g_phone_no_status', 'Guardian''s phone Contact', 'gcontact', 'Is Guardian''s phone primary or secondary ?', 'Select', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(32, 3, 'family_income', 'Family Income', 'family_income', 'Family Income Details', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(33, 3, 'person_school_fee', 'person/company paying school fee', 'feepayee', 'The person/company paying school fee', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(34, 3, 'circumtances', 'family circumstances', 'famcir', 'family circumstances', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(35, 3, 'sibling', 'Siblings', 'siblings', 'siblings of student', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(36, 4, 'blood_group', 'Blood Group', 'blood_grp', 'Blood Group of student', 'Select', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(37, 4, 'height', 'Height', 'height', 'Height of student', 'Numeric', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(38, 4, 'weight', 'Weight', 'weight', 'Weight of student', 'Numeric', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(39, 4, 'allergic', 'Allergic to any drug?', 'drugallergic', 'Allergic to any drug? ', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(40, 4, 'chronic', 'Chronic illness', 'chronicill', 'Chronic illness if any', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(41, 4, 'family_doctor', 'Family doctor details', 'docDet', 'Family doctor details of student', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(42, 5, 'previous_school_name', 'Previous School Name', 'prevSchoolName', 'Previous School Name of student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(43, 5, 'previous_school_country', 'Previous school country', 'pprevcountry', 'Previous School Country of Student', 'Select', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(44, 5, 'previous_school_state', 'Previous school state', 'prevState', 'Previous School State Of Student', 'Text', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(45, 5, 'attended_from', 'Attended From', 'attfrom', 'Previous schoolAttended From Date', 'Date Picker', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(46, 5, 'attended_to', 'Attended To', 'attto', 'Previous school Attended Till date', 'Date Picker', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(47, 5, 'achievements', 'Achievements', 'achievements', 'Achievements of student', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(48, 5, 'comments', 'Comments', 'comments', 'Comments', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(49, 5, 'other_details', 'Other details', 'odet', 'Other details', 'TextArea', 1, 1, 0, 1, '1419841259', 1, '1419841259'),
(50, 2, 'other_photos', 'Other Documents', 'other_photos', 'Other Documents of student', 'File', 1, 1, 0, 1, '1419841259', 1, '1419841259');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
