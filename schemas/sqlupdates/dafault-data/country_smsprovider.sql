
--
-- Table structure for table `country_smsprovider`
--

CREATE TABLE IF NOT EXISTS `country_smsprovider` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `country_id` int(1) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `sender` varchar(100) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `token` varchar(255) NOT NULL,
  `api` varchar(2000) NOT NULL,
  `reportapi` varchar(2000) NOT NULL,
  `xmlreportapi` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `country_smsprovider`
--

INSERT INTO `country_smsprovider` (`id`, `country_id`, `domain`, `sender`, `username`, `password`, `token`, `api`, `reportapi`, `xmlreportapi`) VALUES
(2, 101, 'edusparrow', 'EDUSPA', 'NATIVE', 'india123', '728614951006376c08519cda53623364', 'http://sms.smsblaster.in/httpapi/httpapi?token=$token&sender=$sender&number=$number&route=2&type=$msgtypeid&sms=$message', 'http://sms.smsblaster.in/httpapi/httpdlr?token=$token&messageid=$messageid', 'http://sms.smsblaster.in/xmlapi/dlrapi?xml=$xml'),
(3, 101, 'staging', 'EDUSPA', 'NATIVE', 'india123', '728614951006376c08519cda53623364', 'http://sms.smsblaster.in/httpapi/httpapi?token=$token&sender=$sender&number=$number&route=2&type=$msgtypeid&sms=$message', 'http://sms.smsblaster.in/httpapi/httpdlr?token=$token&messageid=$messageid', 'http://sms.smsblaster.in/xmlapi/dlrapi?xml=$xml'),
(4, 101, 'queen', 'QNMIRA', 'NATIVE', 'india123', '728614951006376c08519cda53623364', 'http://sms.smsblaster.in/httpapi/httpapi?token=$token&sender=$sender&number=$number&route=2&type=$msgtypeid&sms=$message', 'http://sms.smsblaster.in/httpapi/httpdlr?token=$token&messageid=$messageid', 'http://sms.smsblaster.in/xmlapi/dlrapi?xml=$xml'),
(5, 101, 'fmhss', 'FATIMA', 'NATIVE', 'india123', '728614951006376c08519cda53623364', 'http://sms.smsblaster.in/httpapi/httpapi?token=$token&sender=$sender&number=$number&route=2&type=$msgtypeid&sms=$message', 'http://sms.smsblaster.in/httpapi/httpdlr?token=$token&messageid=$messageid', 'http://sms.smsblaster.in/xmlapi/dlrapi?xml=$xml'),
(6, 101, 'kvms', 'KVMSNP', 'NATIVE', 'india123', '728614951006376c08519cda53623364', 'http://sms.smsblaster.in/httpapi/httpapi?token=$token&sender=$sender&number=$number&route=2&type=$msgtypeid&sms=$message', 'http://sms.smsblaster.in/httpapi/httpdlr?token=$token&messageid=$messageid', 'http://sms.smsblaster.in/xmlapi/dlrapi?xml=$xml'),
(7, 101, 'sakthividyalaya', 'SAKTHI', 'NATIVE', 'india123', '728614951006376c08519cda53623364', 'http://sms.smsblaster.in/httpapi/httpapi?token=$token&sender=$sender&number=$number&route=2&type=$msgtypeid&sms=$message', 'http://sms.smsblaster.in/httpapi/httpdlr?token=$token&messageid=$messageid', 'http://sms.smsblaster.in/xmlapi/dlrapi?xml=$xml'),
(8, 101, 'scgmis', 'SCGMIS', 'NATIVE', 'india123', '728614951006376c08519cda53623364', 'http://sms.smsblaster.in/httpapi/httpapi?token=$token&sender=$sender&number=$number&route=2&type=$msgtypeid&sms=$message', 'http://sms.smsblaster.in/httpapi/httpdlr?token=$token&messageid=$messageid', 'http://sms.smsblaster.in/xmlapi/dlrapi?xml=$xml'),
(9, 101, 'snmhss', 'SNMHSS', 'NATIVE', 'india123', '728614951006376c08519cda53623364', 'http://sms.smsblaster.in/httpapi/httpapi?token=$token&sender=$sender&number=$number&route=2&type=$msgtypeid&sms=$message', 'http://sms.smsblaster.in/httpapi/httpdlr?token=$token&messageid=$messageid', 'http://sms.smsblaster.in/xmlapi/dlrapi?xml=$xml'),
(10, 101, 'svvm', 'SVVMSC', 'NATIVE', 'india123', '728614951006376c08519cda53623364', 'http://sms.smsblaster.in/httpapi/httpapi?token=$token&sender=$sender&number=$number&route=2&type=$msgtypeid&sms=$message', 'http://sms.smsblaster.in/httpapi/httpdlr?token=$token&messageid=$messageid', 'http://sms.smsblaster.in/xmlapi/dlrapi?xml=$xml');

