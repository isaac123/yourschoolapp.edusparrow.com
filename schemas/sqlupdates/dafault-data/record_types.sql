
--
-- Table structure for table `record_types`
--

CREATE TABLE IF NOT EXISTS `record_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `record_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `record_types`
--

INSERT INTO `record_types` (`id`, `record_name`) VALUES
(1, 'PAYMENT VOUCHER'),
(2, 'SUSPENSE VOUCHER'),
(3, 'RECEIPT VOUCHER'),
(4, 'JOURNAL VOUCHER'),
(5, 'SALARY VOUCHER'),
(6, 'ADVANCE'),
(7, 'APPLICATION FEES'),
(8, 'FEE'),
(9, 'CONTRA VOUCHER');

