
--
-- Table structure for table `approval_types`
--

CREATE TABLE IF NOT EXISTS `approval_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `approval_type` varchar(50) NOT NULL,
  `is_required` int(11) NOT NULL,
  `is_default_by_role` varchar(50) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `approval_types`
--

INSERT INTO `approval_types` (`id`, `approval_type`, `is_required`, `is_default_by_role`) VALUES
(1, 'Leave', 1, NULL),
(2, 'Application', 1, NULL),
(3, 'Fee cancel', 1, NULL),
(5, 'Journal Voucher', 1, NULL),
(6, 'Payment Voucher', 1, NULL),
(7, 'Suspense Voucher', 1, NULL),
(8, 'Relieving(student)', 1, NULL),
(9, 'Relieving(staff)', 1, NULL),
(10, 'Appointment', 1, NULL),
(11, 'Transport', 0, NULL),
(12, 'Advance', 1, NULL),
(13, 'Salary Voucher', 1, NULL),
(14, 'Concession', 1, NULL),
(15, 'Transport Cancel', 0, NULL),
(16, 'Application Fees', 1, NULL),
(17, 'Fee Assign', 0, NULL),
(18, 'StockAdjustment', 1, NULL),
(19, 'Contra Voucher', 1, NULL);
